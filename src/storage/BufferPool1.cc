/*******************************************************************************
 * Copyright (c) 2015 Michail Basios and Stratis Viglas.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michail Basios - initial API and implementation
 *     Stratis Viglas - initial API and implementation
 *******************************************************************************/

#include <fcntl.h>
#include <boost/multi_index_container.hpp>

#include <iostream>
#include <string>
#include <iterator>

#include "../utils/helpers.hh"
#include "../utils/types.hh"
#include "../storage/File.hh"
#include "../storage/MemPage.hh"
#include "../storage/BufferPool.hh"
#include "../utils/global.hh"

namespace deceve {
namespace storage {

// int misses = 0, hits = 0;

BufferPool::BufferPool(const unsigned long pool_size_b,
                       StorageManager* const sm_ptr)
    : pool_size(pool_size_b / PAGE_SIZE_PERSISTENT),
      used_pages(0),
      primary_used_pages(0),
      auxiliary_used_pages(0),
      intermediate_used_pages(0),
      clock(0),
      sm(sm_ptr),
      pg_index(),
      auxiliary_pg_index(),
      intermediate_pg_index(),
      pin_counts(),
      primary_references(0),
      primary_hits(0),
      auxiliary_references(0),
      auxiliary_hits(0),
      intermediate_references(0),
      intermediate_hits(0),
      sliderCount( pool_size),
      primaryWrites(0),
      intermediateWrites(0),
      auxiliaryWrites(0),
      region_pool_size(pool_size_b / PAGE_SIZE_PERSISTENT),
      print_counter(0) {
  // pool is the memory-page-aligned buffer
  realpool = new char[pool_size_b + PAGE_SIZE_PERSISTENT];
  memset(realpool, 0, pool_size_b + PAGE_SIZE_PERSISTENT);

  pool = (char*)(((ptr_type)realpool + PAGE_SIZE_PERSISTENT) -
                 ((ptr_type)realpool % PAGE_SIZE_PERSISTENT));

  // realregionpool = new char[pool_size_b + PAGE_SIZE];
  // regionpool = (char *) (((ptr_type) realregionpool + PAGE_SIZE)
  // - ((ptr_type) realregionpool % PAGE_SIZE));

  numberOfAvailablePrimaryPages = sliderCount;
  //  std::cout << "numberOfAvailablePrimaryPages is:"
  //            << numberOfAvailablePrimaryPages << "\n";

  //  std::cout << "POOL_SIZE: " << pool_size << std::endl;
  algorithmId = 0;

  srand(time(NULL));

  pg_index.clear();
  auxiliary_pg_index.clear();
  intermediate_pg_index.clear();
  free_page_index.clear();
  free_intermediate_page_index.clear();

  for (auto it = indexMapTkIndex.begin(); it != indexMapTkIndex.end(); ++it) {
    it->second->clear();
  }
}

BufferPool::~BufferPool() {
  //  std::cout << "BufferPool Destructor Start"
  //            << "\n";
  //  std::cout << "Delete Primary List"
  //            << "\n";
  mpi_by_time::const_iterator it = pg_index.get<Timestamp>().begin();
  for (; it != pg_index.get<Timestamp>().end(); ++it) {
    // COUT<<page(it).getFilepos().filename<<"
    // "<<page(it).getFilepos().offset<<"\n";
    assert(!page(it).isPinned() && "Pou pas re Karamhtrooooo...");
    if (page(it).isDirty()) {
      sm->writePage(page(it));
    }
    delete &page(it);
  }
  std::cout << "Delete Auxiliary List"
            << "\n";
  it = auxiliary_pg_index.get<Timestamp>().begin();
  for (; it != auxiliary_pg_index.get<Timestamp>().end(); ++it) {
    assert(!page(it).isPinned() && "Pou pas re Karamhtrooooo...");
    if (page(it).isDirty()) {
      sm->writePage(page(it));
    }
    delete &page(it);
  }

  std::cout << "Delete Intermediate List"
            << "\n";
  it = intermediate_pg_index.get<Timestamp>().begin();
  for (; it != intermediate_pg_index.get<Timestamp>().end(); ++it) {
    assert(!page(it).isPinned() && "Pou pas re Karamhtrooooo...");
    if (page(it).isDirty()) {
      sm->writePage(page(it));
    }
    delete &page(it);
  }

  std::cout << "Free List"
            << "\n";
  it = free_page_index.get<Timestamp>().begin();
  for (; it != free_page_index.get<Timestamp>().end(); ++it) {
    delete &page(it);
  }
  std::cout << "Intermediate Free List"
            << "\n";
  it = free_intermediate_page_index.get<Timestamp>().begin();
  for (; it != free_intermediate_page_index.get<Timestamp>().end(); ++it) {
    delete &page(it);
  }
  std::cout << "Clear indexes" << std::endl;
  pg_index.clear();
  std::cout << "Clear pg_index" << std::endl;
  auxiliary_pg_index.clear();
  std::cout << "Clear auxiliary_pg_index" << std::endl;
  intermediate_pg_index.clear();
  std::cout << "Clear intermediate_pg_index" << std::endl;
  free_page_index.clear();
  std::cout << "Clear free_intermediate_page_index" << std::endl;
  free_intermediate_page_index.clear();
  std::cout << "Clear free_intermediate_page_index" << std::endl;
  delete[] realpool;

  std::cout << "BufferPool is empty." << std::endl;
  // delete[] realregionpool;

  // COUT << "Hits :\t " << hits << "\n";
  // COUT << "Misses:\t" << misses << "\n";
}

// MemPageIndex* BufferPool::findIndex(const FilePos& filepos) {
//  switch (sm->getFileMode(filepos.filename)) {
//    case PRIMARY:
//      return &pg_index;
//    case INTERMEDIATE:
//      return &intermediate_pg_index;
//    case FREE:
//      return &free_page_index;
//    case FREE_INTERMEDIATE:
//      return &free_intermediate_page_index;
//    case AUXILIARY:
//      return &auxiliary_pg_index;
//  }
//  return &pg_index;
//}

MemPageIndex* BufferPool::findIndex(const FilePos& filepos) {
  FileMode mode = sm->getFileMode(filepos.filename);
  if (mode == PRIMARY) {
    return &pg_index;
  } else if (mode == INTERMEDIATE) {
    return &intermediate_pg_index;
  } else if (mode == AUXILIARY) {
    return &auxiliary_pg_index;
  } else if (mode == FREE) {
    return &free_page_index;
  } else if (mode == FREE_INTERMEDIATE) {
    return &free_intermediate_page_index;
  }
  return &pg_index;
  //  switch (sm->getFileMode(filepos.filename)) {
  //    case PRIMARY:
  //      return &pg_index;
  //    case INTERMEDIATE:
  //      return &intermediate_pg_index;
  //    case FREE:
  //      return &free_page_index;
  //    case FREE_INTERMEDIATE:
  //      return &free_intermediate_page_index;
  //    case AUXILIARY:
  //      return &auxiliary_pg_index;
  //  }
  //  return &pg_index;
}

// MemPageIndex* BufferPool::findIndex(const std::string& filename) {
//  switch (sm->getFileMode(filename)) {
//    case PRIMARY:
//      return &pg_index;
//    case INTERMEDIATE:
//      return &intermediate_pg_index;
//    case FREE:
//      return &free_page_index;
//    case FREE_INTERMEDIATE:
//      return &free_intermediate_page_index;
//    case AUXILIARY:
//      return &auxiliary_pg_index;
//  }
//  return &pg_index;
//}

MemPageIndex* BufferPool::findIndex(const std::string& filename) {
  FileMode mode = sm->getFileMode(filename);

  if (mode == PRIMARY) {
    return &pg_index;
  } else if (mode == INTERMEDIATE) {
    return &intermediate_pg_index;
  } else if (mode == AUXILIARY) {
    return &auxiliary_pg_index;
  } else if (mode == FREE) {
    return &free_page_index;
  } else if (mode == FREE_INTERMEDIATE) {
    return &free_intermediate_page_index;
  }
  return &pg_index;
}

// MemPageIndex* BufferPool::findIndex(const FileMode& mode) {
//  switch (mode) {
//    case PRIMARY:
//      return &pg_index;
//    case INTERMEDIATE:
//      return &intermediate_pg_index;
//    case FREE:
//      return &free_page_index;
//    case FREE_INTERMEDIATE:
//      return &free_intermediate_page_index;
//    case AUXILIARY:
//      return &auxiliary_pg_index;
//  }
//  return &pg_index;
//}

MemPageIndex* BufferPool::findIndex(const FileMode& mode) {
  if (mode == PRIMARY) {
    return &pg_index;
  } else if (mode == INTERMEDIATE) {
    return &intermediate_pg_index;
  } else if (mode == AUXILIARY) {
    return &auxiliary_pg_index;
  } else if (mode == FREE) {
    return &free_page_index;
  } else if (mode == FREE_INTERMEDIATE) {
    return &free_intermediate_page_index;
  }
  return &pg_index;
}

void BufferPool::updateTimestamp(MemPageIndex* index,
                                 mpi_by_filepos::const_iterator it) {
  // COUT << "updateTimestamp" << "\n";
  assert(it != index->get<FilePos>().end() &&
         "Page does not exist in the buffer pool.");
  Timestamp nts = getNextTime();
  page(it).setTimestamp(nts);
  index->modify(it, newTimestamp(nts));
}

// Pins and releases
void BufferPool::pinPage(const FilePos& pageid) {
  // std::lock_guard < std::mutex > lock_pinning(sm->readerPinReleaseMutex);

  // COUT << "pinPage" << "\n";
  FileMode fmode = sm->getFileMode(pageid.filename);
  MemPageIndex* index = findIndex(fmode);
  auto it = index->get<FilePos>().find(pageid);
  assert(it != index->get<FilePos>().end() &&
         "On page pinning: Page does not exist in the buffer pool.");
  page(it).addPin();
}

// Pins and releases
void BufferPool::pinPage(const FilePos& pageid, FileMode fmode) {
  // COUT << "pinPage" << "\n";
  // COUT<<"fmode: "<<fmode<<" pagid: "<<pageid.filename<<"
  // "<<pageid.offset<<std::endl;
  // std::cout<<"pin page: "<<pageid.filename<<" "<<pageid.offset<<"\n";
  // FileMode ffmode = sm->getFileMode(pageid.filename);

  //#ifdef CLEANTEST
  //  (void)fmode;
  //  MemPageIndex* index = &pg_index;
  //  auto it = index->get<FilePos>().find(pageid);
  //  if (it == index->get<FilePos>().end()) {
  //    index = &intermediate_pg_index;
  //    it = index->get<FilePos>().find(pageid);
  //  }
  //  assert(it != index->get<FilePos>().end() &&
  //         "On page pinning: Page does not exist in the buffer pool.");
  //  page(it).addPin();
  //
  //#elif CLEANTEST2
  //  if (fmode == sto::PRIMARY) {
  //    MemPageIndex* index = &pg_index;
  //    auto it = index->get<FilePos>().find(pageid);
  //    if (it == index->get<FilePos>().end()) {
  //      index = &internal_intermediate_index;
  //      it = index->get<FilePos>().find(pageid);
  //    }
  //    assert(it != index->get<FilePos>().end() &&
  //           "On page pinning: Page does not exist in the buffer pool.");
  //    page(it).addPin();
  //  } else {
  //    MemPageIndex* index = findIndex(fmode);
  //    auto it = index->get<FilePos>().find(pageid);
  //    if (it == index->get<FilePos>().end()) {
  //      std::cout << "PIN PROBLEM: " << pageid.filename << " " <<
  //      pageid.offset
  //                << " "
  //                << "fmode " << fmode << std::endl;
  //      return;
  //    }
  //    assert(it != index->get<FilePos>().end() &&
  //           "On page pinning: Page does not exist in the buffer pool.");
  //    page(it).addPin();
  //  }
  //#else
  //  MemPageIndex* index = findIndex(fmode);
  //  auto it = index->get<FilePos>().find(pageid);
  //  if (it == index->get<FilePos>().end()) {
  //    std::cout << "PIN PROBLEM: " << pageid.filename << " " << pageid.offset
  //              << " "
  //              << "fmode " << fmode << std::endl;
  //    return;
  //  }
  //  assert(it != index->get<FilePos>().end() &&
  //         "On page pinning: Page does not exist in the buffer pool.");
  //  page(it).addPin();
  //#endif

  if (getAlgorithm() == 2) {
    (void)fmode;
    MemPageIndex* index = &pg_index;
    auto it = index->get<FilePos>().find(pageid);
    if (it == index->get<FilePos>().end()) {
      index = &intermediate_pg_index;
      it = index->get<FilePos>().find(pageid);
    }
    assert(it != index->get<FilePos>().end() &&
           "On page pinning: Page does not exist in the buffer pool.");
    page(it).addPin();

  } else if (getAlgorithm() == 3) {
    if (fmode == sto::PRIMARY) {
      MemPageIndex* index = &pg_index;
      auto it = index->get<FilePos>().find(pageid);
      if (it == index->get<FilePos>().end()) {
        index = &internal_intermediate_index;
        it = index->get<FilePos>().find(pageid);
      }
      assert(it != index->get<FilePos>().end() &&
             "On page pinning: Page does not exist in the buffer pool.");
      page(it).addPin();
    } else {
      MemPageIndex* index = findIndex(fmode);
      auto it = index->get<FilePos>().find(pageid);
      if (it == index->get<FilePos>().end()) {
        std::cout << "PIN PROBLEM: " << pageid.filename << " " << pageid.offset
                  << " "
                  << "fmode " << fmode << std::endl;
        return;
      }
      assert(it != index->get<FilePos>().end() &&
             "On page pinning: Page does not exist in the buffer pool.");
      page(it).addPin();
    }
  } else {
    MemPageIndex* index = findIndex(fmode);
    auto it = index->get<FilePos>().find(pageid);
    if (it == index->get<FilePos>().end()) {
      std::cout << "PIN PROBLEM: " << pageid.filename << " " << pageid.offset
                << " "
                << "fmode " << fmode << std::endl;
      return;
    }
    assert(it != index->get<FilePos>().end() &&
           "On page pinning: Page does not exist in the buffer pool.");
    page(it).addPin();
  }
}

void BufferPool::releasePage(const FilePos& pageid) {
  // std::lock_guard < std::mutex > lock_pinning(sm->readerPinReleaseMutex);

  FileMode fmode = sm->getFileMode(pageid.filename);
  MemPageIndex* index = findIndex(fmode);
  auto it = index->get<FilePos>().find(pageid);

  // my_assert_release(it != index->get<FilePos>().end(), pageid.filename,
  // pageid.offset);
  assert(it != index->get<FilePos>().end() &&
         "On page release: Page does not exist in the buffer pool. ");
  page(it).removePin();
}

void BufferPool::releasePage(const FilePos& pageid, FileMode fmode) {
  // std::lock_guard < std::mutex > lock_pinning(sm->readerPinReleaseMutex);
  // COUT<<"release page: "<<pageid.filename<<" "<<pageid.offset<<"\n";
  //#ifdef CLEANTEST
  //  (void)fmode;
  //  MemPageIndex* index = &pg_index;
  //  auto it = index->get<FilePos>().find(pageid);
  //  if (it == index->get<FilePos>().end()) {
  //    index = &intermediate_pg_index;
  //    it = index->get<FilePos>().find(pageid);
  //  }
  //  assert(it != index->get<FilePos>().end() &&
  //         "On page release: Page does not exist in the buffer pool. ");
  //  page(it).removePin();
  //#elif CLEANTEST2
  //  if (fmode == sto::PRIMARY) {
  //    MemPageIndex* index = &pg_index;
  //    auto it = index->get<FilePos>().find(pageid);
  //    if (it == index->get<FilePos>().end()) {
  //      index = &internal_intermediate_index;
  //      it = index->get<FilePos>().find(pageid);
  //    }
  //    assert(it != index->get<FilePos>().end() &&
  //           "On page release: Page does not exist in the buffer pool. ");
  //    page(it).removePin();
  //  } else {
  //    MemPageIndex* index = findIndex(fmode);
  //    auto it = index->get<FilePos>().find(pageid);
  //    if (it == index->get<FilePos>().end()) {
  //      std::cout << "RELEASE PROBLEM: " << pageid.filename << " "
  //                << pageid.offset << " "
  //                << "fmode " << fmode << std::endl;
  //      return;
  //    }
  //    assert(it != index->get<FilePos>().end() &&
  //           "On page release: Page does not exist in the buffer pool. ");
  //    page(it).removePin();
  //  }
  //#else
  //  MemPageIndex* index = findIndex(fmode);
  //  auto it = index->get<FilePos>().find(pageid);
  //  if (it == index->get<FilePos>().end()) {
  //    std::cout << "RELEASE PROBLEM: " << pageid.filename << " " <<
  //    pageid.offset
  //              << " "
  //              << "fmode " << fmode << std::endl;
  //    return;
  //  }
  //  assert(it != index->get<FilePos>().end() &&
  //         "On page release: Page does not exist in the buffer pool. ");
  //  page(it).removePin();
  //#endif
  //

  if (getAlgorithm() == 2) {
    (void)fmode;
    MemPageIndex* index = &pg_index;
    auto it = index->get<FilePos>().find(pageid);
    if (it == index->get<FilePos>().end()) {
      index = &intermediate_pg_index;
      it = index->get<FilePos>().find(pageid);
    }
    assert(it != index->get<FilePos>().end() &&
           "On page release: Page does not exist in the buffer pool. ");
    page(it).removePin();
  } else if (getAlgorithm() == 3) {
    if (fmode == sto::PRIMARY) {
      MemPageIndex* index = &pg_index;
      auto it = index->get<FilePos>().find(pageid);
      if (it == index->get<FilePos>().end()) {
        index = &internal_intermediate_index;
        it = index->get<FilePos>().find(pageid);
      }
      assert(it != index->get<FilePos>().end() &&
             "On page release: Page does not exist in the buffer pool. ");
      page(it).removePin();
    } else {
      MemPageIndex* index = findIndex(fmode);
      auto it = index->get<FilePos>().find(pageid);
      if (it == index->get<FilePos>().end()) {
        std::cout << "RELEASE PROBLEM: " << pageid.filename << " "
                  << pageid.offset << " "
                  << "fmode " << fmode << std::endl;
        return;
      }
      assert(it != index->get<FilePos>().end() &&
             "On page release: Page does not exist in the buffer pool. ");
      page(it).removePin();
    }
  } else {
    MemPageIndex* index = findIndex(fmode);
    auto it = index->get<FilePos>().find(pageid);
    if (it == index->get<FilePos>().end()) {
      std::cout << "RELEASE PROBLEM: " << pageid.filename << " "
                << pageid.offset << " "
                << "fmode " << fmode << std::endl;
      return;
    }
    assert(it != index->get<FilePos>().end() &&
           "On page release: Page does not exist in the buffer pool. ");
    page(it).removePin();
  }
}

bool BufferPool::isInIndex(const FilePos& pageid, FileMode fmode) {
  MemPageIndex* index = findIndex(fmode);
  auto it = index->get<FilePos>().find(pageid);

  if (it != index->get<FilePos>().end()) {
    return true;
  }
  return false;
}

void BufferPool::incrementPins(const FilePos& fn) {
  // std::lock_guard < std::mutex > lock(pinCounterMutex);
  auto it = pin_counts.find(fn);
  if (!(it == pin_counts.end())) {
    (it->second)++;
  } else {
    pin_counts.insert(std::make_pair(fn, 1));
  }
}

void BufferPool::decrementPins(const FilePos& fn) {
  // std::lock_guard < std::mutex > lock(pinCounterMutex);

  auto it = pin_counts.find(fn);
  if (!(it == pin_counts.end())) {
    (it->second)--;
    if (it->second == 0) {
      pin_counts.erase(it);
    }
  }
}

// Statistics
// void BufferPool::incrementReferences(const FileMode& mode) {
//  switch (mode) {
//    case PRIMARY:
//      primary_references++;
//      break;
//    case INTERMEDIATE:
//      intermediate_references++;
//      break;
//    case AUXILIARY:
//      auxiliary_references++;
//      break;
//    case FREE:
//      break;
//    case FREE_INTERMEDIATE:
//      break;
//  }
//}

void BufferPool::incrementReferences(const FileMode& mode) {
  if (mode == PRIMARY) {
    primary_references++;
  } else if (mode == INTERMEDIATE) {
    intermediate_references++;
  } else if (mode == AUXILIARY) {
    auxiliary_references++;
  }
}

// void BufferPool::incrementHits(FileMode mode) {
//  switch (mode) {
//    case PRIMARY:
//      primary_hits++;
//      break;
//    case INTERMEDIATE:
//      intermediate_hits++;
//      break;
//    case AUXILIARY:
//      auxiliary_hits++;
//      break;
//    case FREE:
//      break;
//    case FREE_INTERMEDIATE:
//      break;
//  }
//}

void BufferPool::incrementHits(FileMode mode) {
  if (mode == PRIMARY) {
    primary_hits++;
  } else if (mode == INTERMEDIATE) {
    intermediate_hits++;
  } else if (mode == AUXILIARY) {
    auxiliary_hits++;
  }
}

// double BufferPool::hitRatio(FileMode fm) {
//  switch (fm) {
//    case PRIMARY:
//      return (primary_references > 0)
//                 ? (static_cast<double>(primary_hits) /
//                    static_cast<double>(primary_references))
//                 : 0;
//    case INTERMEDIATE:
//      return (intermediate_references > 0)
//                 ? (static_cast<double>(intermediate_hits) /
//                    static_cast<double>(intermediate_references))
//                 : 0;
//    case AUXILIARY:
//      return (auxiliary_references > 0)
//                 ? (static_cast<double>(auxiliary_hits) /
//                    static_cast<double>(auxiliary_references))
//                 : 0;
//    case FREE:
//      return 0.0;
//    case FREE_INTERMEDIATE:
//      return 0.0;
//  }
//  return 0.0;
//}

double BufferPool::hitRatio(FileMode fm) {
  if (fm == PRIMARY) {
    return (primary_references > 0) ? (static_cast<double>(primary_hits) /
                                       static_cast<double>(primary_references))
                                    : 0;
  } else if (fm == INTERMEDIATE) {
    return (intermediate_references > 0)
               ? (static_cast<double>(intermediate_hits) /
                  static_cast<double>(intermediate_references))
               : 0;
  } else if (fm == AUXILIARY) {
    return (auxiliary_references > 0)
               ? (static_cast<double>(auxiliary_hits) /
                  static_cast<double>(auxiliary_references))
               : 0;
  } else {
    return 0.0;
  }
}

void BufferPool::invalidateFilePages(const std::string& filename) {
// When a file is removed from the system (with sm.removeFile) we set all
// its buffer-pool pages to false.

#ifdef CLEANTEST

  FileMode mode = sm->getFileMode(filename);
  // COUT<<"Mode: "<<mode<<std::endl;
  MemPageIndex* index = &pg_index;

  mpi_by_time::const_iterator it = index->get<Timestamp>().begin();

  while (it != index->get<Timestamp>().end()) {
    if (page(it).getFilepos().filename == filename) {
      //      if (mode == PRIMARY ||
      //          mode == AUXILIARY) {
      changeIndex(&pg_index, page(it++).getFilepos(), &free_page_index);
      //      } else if (mode == INTERMEDIATE) {
      //        changeIndex(page(it++).getFilepos(),
      //                    FREE_INTERMEDIATE);
      //      }
    } else {
      ++it;
    }
  }

  // COUT<<"Mode: "<<mode<<std::endl;
  index = &intermediate_pg_index;

  it = index->get<Timestamp>().begin();

  while (it != index->get<Timestamp>().end()) {
    if (page(it).getFilepos().filename == filename) {
      //      if (mode == PRIMARY ||
      //          mode == AUXILIARY) {
      changeIndex(&intermediate_pg_index, page(it++).getFilepos(),
                  &free_page_index);
      //      } else if (mode == INTERMEDIATE) {
      //        changeIndex(page(it++).getFilepos(),
      //                    FREE_INTERMEDIATE);
      //      }
    } else {
      ++it;
    }
  }

#elif CLEANTEST2

  FileMode mode = sm->getFileMode(filename);

  if (mode == sto::PRIMARY) {
    MemPageIndex* index = &pg_index;

    mpi_by_time::const_iterator it = index->get<Timestamp>().begin();

    while (it != index->get<Timestamp>().end()) {
      if (page(it).getFilepos().filename == filename) {
        changeIndex(&pg_index, page(it++).getFilepos(), &free_page_index);
      } else {
        ++it;
      }
    }
    index = &internal_intermediate_index;
    it = index->get<Timestamp>().begin();

    while (it != index->get<Timestamp>().end()) {
      if (page(it).getFilepos().filename == filename) {
        changeIndex(&internal_intermediate_index, page(it++).getFilepos(),
                    &free_page_index);
      } else {
        ++it;
      }
    }
  } else {
    MemPageIndex* index = findIndex(mode);

    mpi_by_time::const_iterator it = index->get<Timestamp>().begin();

    while (it != index->get<Timestamp>().end()) {
      if (page(it).getFilepos().filename == filename) {
        if (mode == AUXILIARY) {
          changeIndex(index, page(it++).getFilepos(), &free_page_index);
        } else if (mode == INTERMEDIATE) {
          changeIndex(index, page(it++).getFilepos(), &free_page_index);
        }
      } else {
        ++it;
      }
    }
  }

#else

  FileMode mode = sm->getFileMode(filename);
  //  std::cout << "File Mode: " << mode << " filename: " << filename <<
  //  std::endl;
  MemPageIndex* index = findIndex(mode);

  mpi_by_time::const_iterator it = index->get<Timestamp>().begin();

  while (it != index->get<Timestamp>().end()) {
    if (page(it).getFilepos().filename == filename) {
      if (mode == PRIMARY || mode == AUXILIARY) {
        //        std::cout << "PRIMATY OR AUXILIARY Invalidate: " <<
        //        page(it).getFilepos().filename << " " <<
        //        page(it).getFilepos().offset
        //            << std::endl;

        changeIndex(index, page(it++).getFilepos(), &free_page_index);

        //        changeIndex(page(it++).getFilepos(), FREE);
      } else if (mode == INTERMEDIATE) {
        //        std::cout << "Invalidate: " << page(it).getFilepos().filename
        //        << " " << page(it).getFilepos().offset
        //                   << std::endl;

        changeIndex(index, page(it++).getFilepos(), &free_page_index);
        //        changeIndex(page(it++).getFilepos(),
        //                    FREE_INTERMEDIATE);
      }
    } else {
      ++it;
    }
  }

#endif
}

void BufferPool::invalidateFilePages(const std::string& filename,
                                     FileMode mode) {
// When a file is removed from the system (with sm.removeFile) we set all
// its buffer-pool pages to false.

#ifdef CLEANTEST
  MemPageIndex* index = &pg_index;
  mpi_by_time::const_iterator it = index->get<Timestamp>().begin();
  while (it != index->get<Timestamp>().end()) {
    if (page(it).getFilepos().filename == filename) {
      changeIndex(&pg_index, page(it++).getFilepos(), &free_page_index);

      //      if (mode == PRIMARY ||
      //          mode == AUXILIARY) {
      //        changeIndex(&pg_index, page(it++).getFilepos(),
      //        FREE);
      //      } else if (mode == INTERMEDIATE) {
      //        changeIndex(&pg_index, page(it++).getFilepos(),
      //                    FREE_INTERMEDIATE);
      //      }

    } else {
      ++it;
    }
  }

  index = &intermediate_pg_index;
  it = index->get<Timestamp>().begin();
  while (it != index->get<Timestamp>().end()) {
    if (page(it).getFilepos().filename == filename) {
      changeIndex(&intermediate_pg_index, page(it++).getFilepos(),
                  &free_page_index);

      //      if (mode == PRIMARY ||
      //          mode == AUXILIARY) {
      //        changeIndex(&intermediate_pg_index, page(it++).getFilepos(),
      //                    FREE);
      //      } else if (mode == INTERMEDIATE) {
      //        changeIndex(&intermediate_pg_index, page(it++).getFilepos(),
      //                    FREE_INTERMEDIATE);
      //      }

    } else {
      ++it;
    }
  }

#elif CLEANTEST2
  if (mode == sto::PRIMARY) {
    MemPageIndex* index = &pg_index;
    mpi_by_time::const_iterator it = index->get<Timestamp>().begin();
    while (it != index->get<Timestamp>().end()) {
      if (page(it).getFilepos().filename == filename) {
        changeIndex(&pg_index, page(it++).getFilepos(), &free_page_index);
      } else {
        ++it;
      }
    }

    index = &internal_intermediate_index;
    it = index->get<Timestamp>().begin();
    while (it != index->get<Timestamp>().end()) {
      if (page(it).getFilepos().filename == filename) {
        changeIndex(&internal_intermediate_index, page(it++).getFilepos(),
                    &free_page_index);
      } else {
        ++it;
      }
    }
  } else {
    MemPageIndex* index = findIndex(mode);
    mpi_by_time::const_iterator it = index->get<Timestamp>().begin();

    while (it != index->get<Timestamp>().end()) {
      if (page(it).getFilepos().filename == filename) {
        if (mode == AUXILIARY) {
          changeIndex(index, page(it++).getFilepos(), &free_page_index);

          //        changeIndex(page(it++).getFilepos(), FREE);
        } else if (mode == INTERMEDIATE) {
          changeIndex(index, page(it++).getFilepos(), &free_page_index);
          //        changeIndex(page(it++).getFilepos(),
          //                    FREE_INTERMEDIATE);
        }
      } else {
        ++it;
      }
    }
  }

#else
  MemPageIndex* index = findIndex(mode);
  mpi_by_time::const_iterator it = index->get<Timestamp>().begin();

  while (it != index->get<Timestamp>().end()) {
    if (page(it).getFilepos().filename == filename) {
      if (mode == PRIMARY || mode == AUXILIARY) {
        changeIndex(index, page(it++).getFilepos(), &free_page_index);

        //        changeIndex(page(it++).getFilepos(), FREE);
      } else if (mode == INTERMEDIATE) {
        changeIndex(index, page(it++).getFilepos(), &free_page_index);
        //        changeIndex(page(it++).getFilepos(),
        //                    FREE_INTERMEDIATE);
      }
    } else {
      ++it;
    }
  }
#endif
}

void BufferPool::invalidateFilePages(const table_key& tk) {
  // When a file is removed from the system (with sm.removeFile) we set all
  // its
  // buffer-pool pages to false.

  //  COUT<< "InvalidateTKpages: " << tk << "\n";
  // std::lock_guard < std::mutex > lock(invalidateFilePagesMutex);

  auto it1 = indexMapTkIndex.find(tk);

  if (it1 == indexMapTkIndex.end()) return;

  MemPageIndex* index = it1->second;

  auto it = index->get<Timestamp>().begin();
  //  std::cout << "INVALIDATE PERSISTENT FILEPAGES" << std::endl;
  while (it != index->get<Timestamp>().end()) {
    MemPage& pg = page(it);
    //    std::cout << "Page: " << pg << std::endl;
    // Decrease cost saved from the in-memory dirty pages
    if (pg.isDirty()) {
      uint64_t cost = sm->getStorageWriteDelay();
      decreaseCostIntermediateCache(cost);
      decreaseCostOfRegion(cost);
    } else {
      uint64_t cost = sm->getStorageReadDelay();
      decreaseCostIntermediateCache(cost);
      decreaseCostOfRegion(cost);
    }
    //    decreaseCounterOfIntermediateDirtyPages();

    index->get<Timestamp>().erase(it++);
  }

  indexMapTkIndex.erase(it1);
}

void BufferPool::changeFileNameOnBufferPoolPages(const std::string& filename,
                                                 const std::string& n) {
  //  COUT << "changeFileNameOnBufferPoolPages"
  //       << "\n";

  // std::lock_guard < std::mutex >
  // lock(changeFileNameOnBufferPoolPagesMutex);

  // This can be done by using index->modify(it, newFilename(newName));
  // When a file is removed from the system (with sm.removeFile) we set all
  // its
  // buffer-pool pages to false.
  FileMode fmode = sm->getFileMode(filename);

  MemPageIndex* index = findIndex(fmode);

  mpi_by_time::const_iterator it = index->get<Timestamp>().begin();

  if (fmode == PRIMARY) {
    while (it != index->get<Timestamp>().end()) {
      if (page(it).getFilepos().filename == filename) {
        FilePos filepos(n, page(it).getOffset());
        MemPage* mp =
            new MemPage(filepos, page(it).getData(), page(it).getTimestamp());
        mp->setDirty(page(it).isDirty());
        mp->setPin(page(it).isPinned());
        MemPageP changedPage = MemPageP(mp);
        index->get<Timestamp>().erase(it++);
        index->get<Timestamp>().insert(changedPage);
      } else {
        ++it;
      }
    }
  } else if (fmode == INTERMEDIATE) {
    while (it != index->get<Timestamp>().end()) {
      if (page(it).getFilepos().filename == filename) {
        FilePos filepos(n, page(it).getOffset());
        MemPage* mp =
            new MemPage(filepos, page(it).getData(), page(it).getTimestamp());
        mp->setDirty(page(it).isDirty());
        mp->setPin(page(it).isPinned());
        MemPageP changedPage = MemPageP(mp);
        index->get<Timestamp>().erase(it++);
        index->get<Timestamp>().insert(changedPage);
      } else {
        ++it;
      }
    }
  } else if (fmode == AUXILIARY) {
    // std::lock_guard < std::mutex > lock(auxiliaryPgIndexMutex);

    while (it != index->get<Timestamp>().end()) {
      if (page(it).getFilepos().filename == filename) {
        FilePos filepos(n, page(it).getOffset());
        MemPage* mp =
            new MemPage(filepos, page(it).getData(), page(it).getTimestamp());
        mp->setDirty(page(it).isDirty());
        mp->setPin(page(it).isPinned());
        MemPageP changedPage = MemPageP(mp);
        index->get<Timestamp>().erase(it++);
        index->get<Timestamp>().insert(changedPage);
      } else {
        ++it;
      }
    }

  } else if (fmode == FREE) {
    // std::lock_guard < std::mutex > lock(freePgIndexMutex);

    while (it != index->get<Timestamp>().end()) {
      if (page(it).getFilepos().filename == filename) {
        FilePos filepos(n, page(it).getOffset());
        MemPage* mp =
            new MemPage(filepos, page(it).getData(), page(it).getTimestamp());
        mp->setDirty(page(it).isDirty());
        mp->setPin(page(it).isPinned());
        MemPageP changedPage = MemPageP(mp);
        index->get<Timestamp>().erase(it++);
        index->get<Timestamp>().insert(changedPage);
      } else {
        ++it;
      }
    }
  } else if (fmode == FREE_INTERMEDIATE) {
    while (it != index->get<Timestamp>().end()) {
      if (page(it).getFilepos().filename == filename) {
        FilePos filepos(n, page(it).getOffset());
        MemPage* mp =
            new MemPage(filepos, page(it).getData(), page(it).getTimestamp());
        mp->setDirty(page(it).isDirty());
        mp->setPin(page(it).isPinned());
        MemPageP changedPage = MemPageP(mp);
        index->get<Timestamp>().erase(it++);
        index->get<Timestamp>().insert(changedPage);
      } else {
        ++it;
      }
    }
  }
}

// add pages to be removed to free indexes
void BufferPool::changeIndex(const FilePos& fp, const FileMode& newMode) {
  // std::lock_guard < std::mutex > lock(changeIndexMutex);

  //  std::lock_guard<std::mutex> lock(sm->bufferManagerOperationMutex);

  MemPageIndex* index = findIndex(fp);
  MemPageIndex* newIndex = findIndex(newMode);
  mpi_by_filepos::iterator it = index->get<FilePos>().find(fp);
  MemPage& pg = page(it);
  MemPageP newpage = MemPageP(&pg);
  newIndex->insert(newpage);
  index->erase(pg.getFilepos());
}

// add pages to be removed to free indexes
void BufferPool::changeIndex(MemPageIndex* fromIndex, const FilePos& fp,
                             const FileMode& newMode) {
  // std::lock_guard < std::mutex > lock(changeIndexMutex);

  MemPageIndex* index = fromIndex;
  MemPageIndex* newIndex = findIndex(newMode);
  mpi_by_filepos::iterator it = index->get<FilePos>().find(fp);
  MemPage& pg = page(it);
  MemPageP newpage = MemPageP(&pg);

  newIndex->insert(newpage);

  index->erase(pg.getFilepos());
}

void BufferPool::changeIndex(MemPageIndex* fromIndex, const FilePos& fp,
                             MemPageIndex* toIndex) {
  MemPageIndex* index = fromIndex;
  MemPageIndex* newIndex = toIndex;
  mpi_by_filepos::iterator it = index->get<FilePos>().find(fp);
  MemPage& pg = page(it);
  MemPageP newpage = MemPageP(&pg);
  newIndex->insert(newpage);
  index->erase(pg.getFilepos());
}

size_t BufferPool::getPrimaryPinCount() {
  size_t counter = 0;

  for (auto it = pg_index.begin(); it != pg_index.end(); ++it) {
    if (page(it).isPinned()) counter++;
  }
  return counter;
}

size_t BufferPool::getSizeOfFile(table_key tk) {
  auto it = sm->getIterOfPersistedFileCatalog(tk);

  // remove sorted version of the file
  if (tk.version == SORT && it != sm->getIteratorPersistedCatalogEnd()) {
    std::string filename = it->second.full_output_path;

    //    std::cout << "getSizeOfFile tk@ " << filename << "\n";

    return sm->getFileSizeFromCatalog(filename) /
           deceve::storage::PAGE_SIZE_PERSISTENT;
  }

  if (tk.version == HASHJOIN && it != sm->getIteratorPersistedCatalogEnd()) {
    // size_t sum = 0;

    std::set<std::string> filenames;

    size_t totalSize = 0;

    for (size_t i = 0; i < it->second.num_files; i++) {
      std::stringstream filename;
      filename << it->second.output_name << it->second.full_output_path
               << "_persistent." << i;
      filenames.insert(filename.str());
      totalSize += sm->getFileSizeFromCatalog(filename.str());
      // std::cout << "partialSize of: " << filename.str() << " is: " <<
      // totalSize << "\n";
    }

    //  std::cout << "totalSize: " << totalSize << "\n";
    return totalSize / deceve::storage::PAGE_SIZE_PERSISTENT;
  }

  return 0;
}

// Print Functions
std::ostream& operator<<(std::ostream& os, const BufferPool& bp) {
  mpi_by_time::const_iterator it = bp.pg_index.get<Timestamp>().begin();
  for (; it != bp.pg_index.get<Timestamp>().end(); ++it) os << page(it);
  mpi_by_time::const_iterator iit =
      bp.intermediate_pg_index.get<Timestamp>().begin();
  for (; iit != bp.intermediate_pg_index.get<Timestamp>().end(); ++iit)
    os << page(iit);
  return os;
}

void BufferPool::printPages(const FileMode& mode) {
  MemPageIndex* index = findIndex(mode);

  auto it = index->get<FilePos>().begin();
  printMode(mode);
  int counter = 0;
  for (; it != index->get<FilePos>().end(); ++it) {
    std::cerr << page(it);
    std::cerr << " mode: " << sm->getFileMode(page(it).getFilepos().filename)
              << "\n";
    counter++;
  }
  std::cerr << "Number of pages is: " << counter << "\n";

  return;
}

void BufferPool::printPages(MemPageIndex* index) {
  mpi_by_time::const_iterator it = index->get<Timestamp>().begin();
  int counter = 0;
  for (; it != index->get<Timestamp>().end(); ++it) {
    COUT << page(it);
    COUT << " mode: " << sm->getFileMode(page(it).getFilepos().filename)
         << "\n";
    counter++;
  }
  COUT << "Number of pages is: " << counter << "\n";

  return;
}

void BufferPool::printPagesByTime(const FileMode& mode) {
  MemPageIndex* index = findIndex(mode);

  mpi_by_time::const_iterator it = index->get<Timestamp>().begin();
  int counter = 0;
  for (; it != index->get<Timestamp>().end(); ++it) {
    std::cout << page(it);
    std::cout << " mode: " << sm->getFileMode(page(it).getFilepos().filename)
              << "\n";
    counter++;
  }
  COUT << "Number of pages is: " << counter << "\n";

  return;
}

void BufferPool::printPagesByDirtiness(const FileMode& mode) {
  MemPageIndex* index = findIndex(mode);

  mpi_by_dirtiness::const_iterator it = index->get<Dirtiness>().begin();

  for (; it != index->get<Dirtiness>().end(); ++it) {
    COUT << page(it);
    COUT << " mode: " << sm->getFileMode(page(it).getFilepos().filename)
         << "\n";
  }
}

void BufferPool::printHitRatio() {
  COUT << "hitPrimaryRatio: " << hitRatio(PRIMARY) << "\n";
  COUT << "hitAuxiliaryRatio: " << hitRatio(AUXILIARY) << "\n";
  COUT << "hitIntermediateRatio: " << hitRatio(INTERMEDIATE) << "\n";
  COUT << "primary_references: " << primary_references << "\n";
  COUT << "primary_hits: " << primary_hits << "\n";
  COUT << "intermediate_references: " << intermediate_references << "\n";
  COUT << "intermediate_hits: " << intermediate_hits << "\n";
  COUT << "auxiliary_references: " << auxiliary_references << "\n";
  COUT << "auxiliary_hits: " << auxiliary_hits << "\n";
  COUT << "Primary Hit/Ratio: " << getPrimaryHitRatio() << "\n";
  COUT << "Intermediate Hit/Ratio: " << getIntermediateHitRatio() << "\n";
  COUT << "Auxiliary Hit/Ratio: " << getAuxiliaryHitRatio() << "\n";
  COUT << "primaryWrites: " << primaryWrites << "\n";
  COUT << "intermediateWrites: " << intermediateWrites << "\n";
  COUT << "physical writes: " << deceve::bama::writes << "\n";
  COUT << "physical reads: " << deceve::bama::reads << "\n";
  COUT << "Final SliderCount: " << sliderCount << "\n";
}
void BufferPool::printHitRatio(std::ofstream* f) {
  std::ofstream& file = *f;
  file << "physical writes: " << sm->getPhysicalWritesNumber() << "\n";
  file << "physical reads: " << sm->getPhysicalReadsNumber() << "\n";
  file << "hitPrimaryRatio: " << getPrimaryHitRatio() << "\n";
  file << "hitIntermediateRatio: " << getIntermediateHitRatio() << "\n";
  file << "hitAuxiliaryRatio: " << getAuxiliaryHitRatio() << "\n";
  file << "primary_references: " << primary_references << "\n";
  file << "primary_hits: " << primary_hits << "\n";
  file << "auxiliary_references: " << auxiliary_references << "\n";
  file << "auxiliary_hits: " << auxiliary_hits << "\n";
  file << "intermediate_references: " << intermediate_references << "\n";
  file << "intermediate_hits: " << intermediate_hits << "\n";
  file << "primaryWrites: " << primaryWrites << "\n";
  file << "intermediateWrites: " << intermediateWrites << "\n";
  file << "auxiliaryWrites: " << auxiliaryWrites << "\n";
}

void BufferPool::printMode(FileMode mode) {
  if (mode == PRIMARY) {
    COUT << "PRIMARY Mode"
         << "\n";
  } else if (mode == AUXILIARY) {
    COUT << "AUXILIARY Mode"
         << "\n";
  } else if (mode == INTERMEDIATE) {
    COUT << "INTERMEDIATE Mode"
         << "\n";
  } else if (mode == FREE) {
    COUT << "FREE Mode"
         << "\n";
  }
}

// Other Page replacement algorithms

char* BufferPool::evictPageWithLRU(FileMode mode) {
  std::pair<char*, bool> freePageTuple = getAFreePage(mode);
  if (freePageTuple.second) return freePageTuple.first;

  //### GET FROM INTERMEDIATE TEMPORAL ###
  std::pair<char*, bool> freePrimaryPageTuple;

  freePrimaryPageTuple = getIntermediatePage(PERMANENT);
  if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

  //### GET FROM PRIMARY ##
  freePrimaryPageTuple = getPrimaryPage();
  if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

  freePrimaryPageTuple = getIntermediatePage(TEMPORAL);
  if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

  freePrimaryPageTuple = getAuxiliaryPage();
  if (freePrimaryPageTuple.second) {
    return freePrimaryPageTuple.first;
  } else {
    std::cout << "Auxiliary page NULL: " << std::endl;
  }

  assert(false && "no free page available");
  return nullptr;
}

void BufferPool::movePage(MemPageIndex* indexFirst, MemPageIndex* indexSecond) {
  mpi_by_time::const_iterator primaryIt1 = findCandidate(
      indexFirst->get<Timestamp>().begin(), indexFirst->get<Timestamp>().end());
  MemPage& fetched_primary_page = page(primaryIt1);
  MemPageP primary_memory_page = MemPageP(&fetched_primary_page);
  //  if (counterOfPrints % 10000 == 0) {
  //    std::cout << "--------------------------" << std::endl;
  //    std::cout << "fetched_primary_page page: \n" << fetched_primary_page
  //              << std::endl;
  //    std::cout << "primary_memory_page: \n" << primary_memory_page.ref
  //              << std::endl;
  //  }
  indexSecond->insert(primary_memory_page);
  indexFirst->erase(fetched_primary_page.getFilepos());
  //  if (counterOfPrints % 10000 == 0) {
  //    std::cout << "fetched_primary_page page: \n" << fetched_primary_page
  //              << std::endl;
  //    std::cout << "primary_memory_page: \n" << primary_memory_page.ref
  //              << std::endl;
  //    std::cout << "--------------------------" << std::endl;
  //  }
}

char* BufferPool::evictPageByCostLRU(FileMode mode) {
  MemPageIndex *index, *index1, *index2, *freeIndex1, *freeIndex2;

  double newSlider = static_cast<double>(primary_references) /
                     (static_cast<double>(primary_references) +
                      static_cast<double>(intermediate_references));

  sliderCount = (unsigned long)(static_cast<double>(pool_size) *
                                static_cast<double>(newSlider));

  if (mode == PRIMARY) {
    if (free_page_index.size() + pg_index.size() >= sliderCount) {
      index1 = &pg_index;
      index2 = &intermediate_pg_index;
      freeIndex1 = &free_page_index;
      freeIndex2 = &free_intermediate_page_index;
    } else {
      index1 = &intermediate_pg_index;
      index2 = &pg_index;
      freeIndex1 = &free_intermediate_page_index;
      freeIndex2 = &free_page_index;
    }
  } else {
    if (free_intermediate_page_index.size() + intermediate_pg_index.size() >=
        pool_size - sliderCount) {
      index1 = &intermediate_pg_index;
      index2 = &pg_index;
      freeIndex1 = &free_intermediate_page_index;
      freeIndex2 = &free_page_index;
    } else {
      index1 = &pg_index;
      index2 = &intermediate_pg_index;
      freeIndex1 = &free_page_index;
      freeIndex2 = &free_intermediate_page_index;
    }
  }

  index = index1;

  mpi_by_time::const_iterator primaryIt;

  if (freeIndex1->size() != 0) {
    primaryIt = freeIndex1->get<Timestamp>().begin();
    MemPage& pg = page(primaryIt);
    char* freepage = pg.getData();
    freeIndex1->erase(pg.getFilepos());
    delete &pg;
    return freepage;
  }

  if (freeIndex2->size() != 0) {
    primaryIt = freeIndex2->get<Timestamp>().begin();
    MemPage& pg = page(primaryIt);
    char* freepage = pg.getData();
    freeIndex2->erase(pg.getFilepos());
    delete &pg;
    return freepage;
  }

  primaryIt = findCandidate(index->get<Timestamp>().begin(),
                            index->get<Timestamp>().end());

  mpi_by_time::const_iterator intermediateIt;

  intermediateIt = findCandidate(index2->get<Timestamp>().begin(),
                                 index2->get<Timestamp>().end());

  assert((primaryIt != index->get<Timestamp>().end() ||
          intermediateIt != index2->get<Timestamp>().end()) &&
         "Cannot evict page; all pages pinned");

  MemPage& pg = page(primaryIt);
  MemPage& pg1 = page(intermediateIt);

  if (primaryIt != index->get<Timestamp>().end() && !pg.isDirty()) {
    char* freepage = pg.getData();
    index->erase(pg.getFilepos());
    delete &pg;
    return freepage;
  }

  if (intermediateIt != index2->get<Timestamp>().end() && !pg1.isDirty()) {
    char* freepage = pg1.getData();
    index2->erase(pg1.getFilepos());
    delete &pg1;
    return freepage;
  }

  if (primaryIt != index->get<Timestamp>().end() && pg.isDirty()) {
    char* freepage = pg.getData();
    sm->writePage(pg);
    index->erase(pg.getFilepos());
    delete &pg;
    return freepage;
  }

  char* freepage = pg1.getData();
  sm->writePage(pg1);
  index2->erase(pg1.getFilepos());
  delete &pg1;
  return freepage;
}

char* BufferPool::evictPageByCost(FileMode mode) {
  MemPageIndex *index, *index1, *index2, *freeIndex1, *freeIndex2;

  double newSlider = static_cast<double>(primary_references) /
                     (static_cast<double>(primary_references) +
                      static_cast<double>(intermediate_references));

  sliderCount = (unsigned long)(static_cast<double>(pool_size) *
                                static_cast<double>(newSlider));

  if (mode == PRIMARY) {
    if (free_page_index.size() + pg_index.size() >= sliderCount) {
      index1 = &pg_index;
      index2 = &intermediate_pg_index;
      freeIndex1 = &free_page_index;
      freeIndex2 = &free_intermediate_page_index;
    } else {
      // COUT<< "PRIMARY: Adapt to slider: " << sliderCount << "\n";
      index1 = &intermediate_pg_index;
      index2 = &pg_index;
      freeIndex1 = &free_intermediate_page_index;
      freeIndex2 = &free_page_index;
    }
  } else {
    // COUT << "REQUEST INTERMEDIATE" << "\n";
    if (free_intermediate_page_index.size() + intermediate_pg_index.size() >=
        pool_size - sliderCount) {
      index1 = &intermediate_pg_index;
      index2 = &pg_index;
      freeIndex1 = &free_intermediate_page_index;
      freeIndex2 = &free_page_index;
    } else {
      //  COUT << "INTERMEDIATE: Adapt to slider: " << sliderCount
      //<< "\n";
      index1 = &pg_index;
      index2 = &intermediate_pg_index;
      freeIndex1 = &free_page_index;
      freeIndex2 = &free_intermediate_page_index;
    }
  }

  index = index1;
  mpi_by_time::const_iterator primaryIt;

  if (freeIndex1->size() != 0) {
    primaryIt = freeIndex1->get<Timestamp>().begin();
    MemPage& pg = page(primaryIt);
    char* freepage = pg.getData();
    freeIndex1->erase(pg.getFilepos());
    delete &pg;
    return freepage;
  }

  if (freeIndex2->size() != 0) {
    primaryIt = freeIndex2->get<Timestamp>().begin();
    MemPage& pg = page(primaryIt);
    char* freepage = pg.getData();
    freeIndex2->erase(pg.getFilepos());
    delete &pg;
    return freepage;
  }

  std::pair<bool, mpi_by_dirtiness::const_iterator> pair =
      findCandidate(index, false);

  if (!pair.first) {
    index = index2;
    pair = findCandidate(index, false);
    if (!pair.first) {
      index = index1;
      //  COUT<< "Case3: Get dirty Page from INDEX: slideCount"
      //  << sliderCount << " " << "\n";
      pair = findCandidate(index, true);
      if (!pair.first) {
        //  COUT<< "Case4: Get Dirty Page from OTHER INDEX: slideCount"
        //  << sliderCount << " " << "\n";
        index = index2;
        pair = findCandidate(index, true);
        assert(pair.first && "Cannot evict page; all pages pinned");
      }
    }
  }

  MemPage& pg = page(pair.second);
  char* freepage = pg.getData();
  if (pg.isDirty()) {
    if (mode == PRIMARY) {
      primaryWrites++;
    } else {
      intermediateWrites++;
    }
    sm->writePage(pg);
  }
  index->erase(pg.getFilepos());
  delete &pg;

  return freepage;
}

char* BufferPool::evictPageSillySRU(FileMode mode) {
  MemPageIndex *index, *index1, *index2, *freeIndex1, *freeIndex2;

  // int value = 0;

  double newSlider = static_cast<double>(primary_references) /
                     (static_cast<double>(primary_references) +
                      static_cast<double>(intermediate_references));

  sliderCount = (unsigned long)(static_cast<double>(pool_size) *
                                static_cast<double>(newSlider));

  if (mode == PRIMARY) {
    if (free_page_index.size() + pg_index.size() >= sliderCount) {
      index1 = &pg_index;
      index2 = &intermediate_pg_index;
      freeIndex1 = &free_page_index;
      freeIndex2 = &free_intermediate_page_index;
    } else {
      index1 = &intermediate_pg_index;
      index2 = &pg_index;
      freeIndex1 = &free_intermediate_page_index;
      freeIndex2 = &free_page_index;
    }
  } else {
    if (free_intermediate_page_index.size() + intermediate_pg_index.size() >=
        pool_size - sliderCount) {
      index1 = &intermediate_pg_index;
      index2 = &pg_index;
      freeIndex1 = &free_intermediate_page_index;
      freeIndex2 = &free_page_index;
    } else {
      index1 = &pg_index;
      index2 = &intermediate_pg_index;
      freeIndex1 = &free_page_index;
      freeIndex2 = &free_intermediate_page_index;
    }
  }

  index = index1;

  mpi_by_time::const_iterator primaryIt;

  if (freeIndex1->size() != 0) {
    primaryIt = freeIndex1->get<Timestamp>().begin();
    MemPage& pg = page(primaryIt);
    char* freepage = pg.getData();
    freeIndex1->erase(pg.getFilepos());
    delete &pg;
    return freepage;
  }

  if (freeIndex2->size() != 0) {
    primaryIt = freeIndex2->get<Timestamp>().begin();
    MemPage& pg = page(primaryIt);
    char* freepage = pg.getData();
    freeIndex2->erase(pg.getFilepos());
    delete &pg;
    return freepage;
  }

  primaryIt = findCandidate(index->get<Timestamp>().begin(),
                            index->get<Timestamp>().end());

  mpi_by_time::const_iterator intermediateIt;

  intermediateIt = findCandidate(index2->get<Timestamp>().begin(),
                                 index2->get<Timestamp>().end());

  assert((primaryIt != index->get<Timestamp>().end() ||
          intermediateIt != index2->get<Timestamp>().end()) &&
         "Cannot evict page; all pages pinned");

  MemPage& pg = page(primaryIt);
  MemPage& pg1 = page(intermediateIt);

  if (primaryIt != index->get<Timestamp>().end() && !pg.isDirty()) {
    char* freepage = pg.getData();
    index->erase(pg.getFilepos());
    delete &pg;
    return freepage;
  }

  if (intermediateIt != index2->get<Timestamp>().end() && !pg1.isDirty()) {
    char* freepage = pg1.getData();
    index2->erase(pg1.getFilepos());
    delete &pg1;
    return freepage;
  }

  if (primaryIt != index->get<Timestamp>().end() && pg.isDirty()) {
    char* freepage = pg.getData();
    sm->writePage(pg);
    index->erase(pg.getFilepos());
    delete &pg;
    return freepage;
  }

  //  if (intermediateIt != index2->get<Timestamp>().end() && pg1.isDirty()) {
  char* freepage = pg1.getData();
  sm->writePage(pg1);
  index2->erase(pg1.getFilepos());
  delete &pg1;
  return freepage;
  //  }
}

char* BufferPool::evictPageByType() {
  // COUT << "evictPageByTypeevictPageByType" << "\n";
  mpi_by_time::const_iterator it;

  MemPageIndex* index = &pg_index;
  FileMode mode = PRIMARY;
  it = findCandidate(index->get<Timestamp>().begin(),
                     index->get<Timestamp>().end());

  if (it == index->get<Timestamp>().end()) {
    index = &auxiliary_pg_index;
    it = findCandidate(index->get<Timestamp>().begin(),
                       index->get<Timestamp>().end());
    mode = AUXILIARY;
    if (it == index->get<Timestamp>().end()) {
      index = &intermediate_pg_index;
      it = findCandidate(index->get<Timestamp>().begin(),
                         index->get<Timestamp>().end());
      mode = INTERMEDIATE;
    }
  }

  assert(it != index->get<Timestamp>().end() &&
         "Cannot evict page; all pages pinned");

  if (mode == PRIMARY) {
    MemPage& pg = page(it);
    char* freepage = pg.getData();
    if (pg.isDirty()) {
      sm->writePage(pg);
    }

    // std::lock_guard < std::mutex > lock(pgIndexMutex);
    index->erase(pg.getFilepos());
    delete &pg;
    return freepage;
  } else if (mode == INTERMEDIATE) {
    MemPage& pg = page(it);
    char* freepage = pg.getData();
    if (pg.isDirty()) {
      sm->writePage(pg);
    }

    index->erase(pg.getFilepos());
    delete &pg;
    return freepage;
  } else {
    MemPage& pg = page(it);
    char* freepage = pg.getData();
    if (pg.isDirty()) {
      sm->writePage(pg);
    }

    index->erase(pg.getFilepos());
    delete &pg;
    return freepage;
  }
}

char* BufferPool::evictPageByType(FileMode mode) {
  mpi_by_time::const_iterator it;
  MemPageIndex* index;
  if (mode == PRIMARY) {
    index = &pg_index;
    it = findCandidate(index->get<Timestamp>().begin(),
                       index->get<Timestamp>().end());
    if (it == index->get<Timestamp>().end()) {
      index = &auxiliary_pg_index;
      it = findCandidate(index->get<Timestamp>().begin(),
                         index->get<Timestamp>().end());
      if (it == index->get<Timestamp>().end()) {
        index = &intermediate_pg_index;
        it = findCandidate(index->get<Timestamp>().begin(),
                           index->get<Timestamp>().end());
      }
    }
  } else if (mode == INTERMEDIATE) {
    index = &intermediate_pg_index;
    it = findCandidate(index->get<Timestamp>().begin(),
                       index->get<Timestamp>().end());
    if (it == index->get<Timestamp>().end()) {
      index = &auxiliary_pg_index;
      it = findCandidate(index->get<Timestamp>().begin(),
                         index->get<Timestamp>().end());
      if (it == index->get<Timestamp>().end()) {
        index = &pg_index;
        it = findCandidate(index->get<Timestamp>().begin(),
                           index->get<Timestamp>().end());
      }
    }
  } else {
    index = &pg_index;
    it = findCandidate(index->get<Timestamp>().begin(),
                       index->get<Timestamp>().end());
    if (it == index->get<Timestamp>().end()) {
      index = &auxiliary_pg_index;
      it = findCandidate(index->get<Timestamp>().begin(),
                         index->get<Timestamp>().end());
      if (it == index->get<Timestamp>().end()) {
        index = &intermediate_pg_index;
        it = findCandidate(index->get<Timestamp>().begin(),
                           index->get<Timestamp>().end());
      }
    }
  }
  assert(it != index->get<Timestamp>().end() &&
         "Cannot evict page; all pages pinned");
  MemPage& pg = page(it);
  char* freepage = pg.getData();
  if (pg.isDirty()) {
    sm->writePage(pg);
  }
  index->erase(pg.getFilepos());
  delete &pg;
  return freepage;
}

char* BufferPool::evictPageCleanFirst() {
  std::pair<char*, bool> freePageTuple = getAFreePage(PRIMARY);
  if (freePageTuple.second) return freePageTuple.first;

  MemPageIndex* index = &pg_index;

  std::pair<bool, mpi_by_dirtiness::const_iterator> pair =
      findCandidate(index, false);
  if (!pair.first) {
    index = &intermediate_pg_index;
    pair = findCandidate(index, false);
    if (!pair.first) {
      index = &pg_index;
      pair = findCandidate(index, true);
      if (!pair.first) {
        index = &intermediate_pg_index;
        pair = findCandidate(index, true);
        assert(pair.first && "Cannot evict page; all pages are pinned");
      }
    }
  }

  MemPage& pg = page(pair.second);
  char* freepage = pg.getData();
  if (pg.isDirty()) {
    sm->writePage(pg);
  }
  index->erase(pg.getFilepos());
  delete &pg;
  return freepage;
}

char* BufferPool::evictPageByTypeCleanFirst() {
  MemPageIndex* index = &pg_index;

  std::pair<bool, mpi_by_dirtiness::const_iterator> pair =
      findCandidate(index, false);
  if (!pair.first) {
    pair = findCandidate(index, true);
    if (!pair.first) {
      index = &auxiliary_pg_index;
      pair = findCandidate(index, false);
      if (!pair.first) {
        pair = findCandidate(index, true);
        if (!pair.first) {
          index = &intermediate_pg_index;
          pair = findCandidate(index, false);
          if (!pair.first) {
            pair = findCandidate(index, true);
            assert(pair.first && "Cannot evict page; all pages pinned");
          }
        }
      }
    }
  }

  MemPage& pg = page(pair.second);
  char* freepage = pg.getData();
  if (pg.isDirty()) {
    sm->writePage(pg, PRIMARY);
  }
  index->erase(pg.getFilepos());
  delete &pg;
  return freepage;
}

char* BufferPool::evictLRUPage() {
  // COUT<< "pg_index.size() " << pg_index.size() << "\n";

  mpi_by_time::const_iterator pgit = pg_index.get<Timestamp>().begin();
  while (pgit != pg_index.get<Timestamp>().end() && page(pgit).isPinned())
    pgit++;

  // COUT << "auxiliary_pg_index.size() " << auxiliary_pg_index.size() <<
  // "\n";

  mpi_by_time::const_iterator auit =
      auxiliary_pg_index.get<Timestamp>().begin();
  while (pgit != pg_index.get<Timestamp>().end() && page(pgit).isPinned())
    auit++;

  // COUT << "intermediate_pg_index.size() " << intermediate_pg_index.size()
  //<< "\n";

  mpi_by_time::const_iterator init =
      intermediate_pg_index.get<Timestamp>().begin();
  while (pgit != intermediate_pg_index.get<Timestamp>().end() &&
         page(init).isPinned())
    init++;

  assert((pgit != pg_index.get<Timestamp>().end() ||
          auit != auxiliary_pg_index.get<Timestamp>().end() ||
          init != intermediate_pg_index.get<Timestamp>().end()) &&
         "Cannot evict LRU page. All pages are pinned.");

  // COUT << "Pass require" << "\n";

  mpi_by_time::const_iterator it;
  if (pgit == pg_index.get<Timestamp>().end()) {
    if (auit == auxiliary_pg_index.get<Timestamp>().end()) {
      it = init;
    } else if (init == intermediate_pg_index.get<Timestamp>().end()) {
      it = auit;
    } else if (page(auit).getTimestamp() < page(init).getTimestamp()) {
      it = auit;
    } else {
      it = init;
    }
  } else {
    if (auit == auxiliary_pg_index.get<Timestamp>().end()) {
      if (init == intermediate_pg_index.get<Timestamp>().end()) {
        it = pgit;
      } else if (page(pgit).getTimestamp() < page(init).getTimestamp()) {
        it = pgit;
      } else {
        it = init;
      }
    } else {
      if (init == intermediate_pg_index.get<Timestamp>().end()) {
        if (page(pgit).getTimestamp() < page(auit).getTimestamp()) {
          it = pgit;
        } else {
          it = auit;
        }
      } else {
        if (page(pgit).getTimestamp() < page(auit).getTimestamp()) {
          if (page(pgit).getTimestamp() < page(init).getTimestamp()) {
            it = pgit;
          } else {
            it = init;
          }
        } else if (page(auit).getTimestamp() < page(init).getTimestamp()) {
          it = auit;
        } else {
          it = init;
        }
      }
    }
  }
  MemPage& pg = page(it);
  char* freepage = pg.getData();

  if (pg.isDirty()) {
    sm->writePage(pg);
  }
  pg_index.erase(pg.getFilepos());
  delete &pg;

  return freepage;
}
}  // namespace  storage
}  // namespace  deceve
