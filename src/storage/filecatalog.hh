/*
 * filecatalog.hh
 *
 *  Created on: Nov 18, 2014
 *      Author: maba18
 */

#ifndef FILECATALOG_HH_
#define FILECATALOG_HH_

namespace deceve {
namespace storage {

const static size_t name_size = 50;

class DataRelationInfo {

	DataRelationInfo() :
			record_size(0), attribute_count(0), num_pages(0), num_records(0) {
		::memset(table_name, 0, name_size + 1);
	}

	DataRelationInfo(char *rel_name) {
		::memcpy(this, rel_name, DataRelationInfo::size());
	}

	static unsigned int size() {
		return (name_size + 1) + 4 * sizeof(int);
	}

private:
	int record_size;
	int attribute_count;
	int num_pages;
	int num_records;
	char table_name[name_size];

};

}
}

#endif /* FILECATALOG_HH_ */
