#ifndef __HEAP_HH__
#define __HEAP_HH__

#include "../utils/require.hh"
#include <iostream>

namespace deceve {

template <typename R, size_t S = 1024>
class Heap {
private:
    R array[S];
    off_t top;

public:
    typedef R element_type;
    
    Heap(): top(0) {}
    ~Heap() {}

public:
    const off_t capacity() const { return S; }
    const off_t length() const { return top; }
    void set_length(off_t l, bool hfy = true) { top = l; if (hfy) heapify(0); }
    
    void dump(R* a, size_t s) {
        require(s < S, "more elements than the heap can take.");
        for (unsigned int i = 0; i < s; i++) array[i] = a[i];
        top = s;
    }
    
    void build() {
        for (unsigned int i = S/2; i > 0; i--) heapify(i);
    }
    
    void add(const R& r) {
        require(top < S, "heap is full.");
        array[top++] = r;
        unsigned int idx = top-1;
        unsigned int prnt = parent(idx);
        while (prnt >= 0 && idx > 0 && array[prnt] > array[idx]) {
            swap(array[idx], array[prnt]);
            idx = prnt;
            prnt = parent(idx);
        }
    }

    void append(const R& r) {
        array[top] = r;
    }
    
    void set_tail(const R& r) {
        //   require(top < S, "heap is full.");
        //array[top++] = r;
        array[top] = r;
    }

    R get_top() { return array[0]; }
    void set_top(const R& r) { array[0] = r; }
    
    R remove() {
        require(top > 0, "heap is empty.");
        R elem = array[0];

        if (top > 1) {
            unsigned int idx = 0;

            array[idx] = array[top-1];
            while (((array[idx] < array[lchild(idx)] && lchild(idx) < top-1)
                    || (array[idx] < array[rchild(idx)] && rchild(idx) < top-1))
                   && idx < top-1) {
                unsigned int sw = (array[lchild(idx)] > array[rchild(idx)]
                                   ? lchild(idx) : rchild(idx));
                swap(array[idx], array[sw]);
                idx = sw;
            }
        }
        top--;
        
        return elem;
    }

    void heapify(unsigned int i) { heapify(i, length()); }
    
    void heapify(unsigned int i, unsigned int end) {
        unsigned int max = ((lchild(i) < end && array[lchild(i)] > array[i])
                            ? lchild(i) : i);
        if (rchild(i) < end && array[i] > array[max])
            max = rchild(i);
        if (max != i) {
            swap(array[i], array[max]);
            heapify(max, end);
        }
    }

private:
    unsigned int lchild(unsigned int i) const { return 2*i+1; }
    unsigned int rchild(unsigned int i) const { return 2*i+2; }
    unsigned int parent(unsigned int i) const { return (i-1)/2; }
    void swap(R& x, R& y) { R t = x; x = y; y = t; }

public:
    std::ostream& toTree(std::ostream& os,
                         unsigned int index = 0,
                         unsigned int level = 0) const {
        for (unsigned int i = 0; i < level; i++) os << " ";
        os << array[index] << "\n";
        if (lchild(index) < top) toTree(os, lchild(index), level+1);
        if (rchild(index) < top) toTree(os, rchild(index), level+1);
        return os;
    }
};



template <typename R, size_t S = 1024>
class dual_heap {
private:
    R array[S];
    off_t curr_length;

public:
    typedef enum { current, next } heap_spec;    
    typedef R element_type;
    
    dual_heap(): curr_length(0) {}
    ~dual_heap() {}

public:
    const off_t capacity() const { return S; }
    const off_t current_length() const { return curr_length; }
    const off_t next_length() const { return S - curr_length; }

    /*
    void dump(heap_spec which, R* a, size_t s) {
        off_t top = 0;
        off_t limit = current_length;
        if (which == next) { top = current_length; limit = next_length(); }
        
        require(s < limit, "more elements than the heap can take.");
        for (unsigned int i = 0; i < s; i++) array[top+i] = a[i];
    }
    
    void build(heap_spec which) {
        off_t start = 0;
        off_t length = current_length/2;
        if (which == next) { top = current_length; start = S/2; }
        for (off_t i = end/2; i > 0; i--) heapify(start+i);
    }
    */    
    
    void add(heap_spec which, const R& r) {
        if (which == current) {
            //array[0] = r;
            array[curr_length-1] = r;
            heapify_current(0);
            //heapify(0, curr_length);
        }
        else {
            array[curr_length] = r;
            //heapify(curr_length, S);
            heapify_next(0);
        }
    }

    void add(const R& r) {
        require(curr_length < S, "heap is full.");
        array[curr_length++] = r;
        unsigned int idx = curr_length-1;
        unsigned int prnt = parent(idx);
        while (prnt >= 0 && idx > 0 && array[prnt] > array[idx]) {
            swap(array[idx], array[prnt]);
            idx = prnt;
            prnt = parent(idx);
        }
    }

    /*
    void append(const R& r) {
        array[top] = r;
    }
    
    void set_tail(const R& r) {
        //   require(top < S, "heap is full.");
        //array[top++] = r;
        array[top] = r;
    }

    R get_top() { return array[0]; }
    void set_top(const R& r) { array[0] = r; }
    */

    // always remove from the current heap
    R remove() {
        require(curr_length > 0, "heap is empty.");
        R elem = array[0];

        if (curr_length > 1) {
            unsigned int idx = 0;
            array[idx] = array[curr_length-1];
            curr_length--;
            heapify_current(0);
            /*
            array[idx] = array[curr_length-1];
            while (((array[idx] < array[lchild(idx)]
                     && lchild(idx) < curr_length-1)
                    ||
                    (array[idx] < array[rchild(idx)]
                     && rchild(idx) < curr_length-1))
                   && idx < curr_length-1) {
                unsigned int sw = (array[lchild(idx)] > array[rchild(idx)]
                                   ? lchild(idx) : rchild(idx));
                swap(array[idx], array[sw]);
                idx = sw;
            }
            */
        }
        else curr_length--;
        
        return elem;
    }

    //void heapify(unsigned int i) { heapify(i, length()); }

    void heapify(heap_spec which) {
        if (which == current) heapify_current(0); else heapify_next(0);
    }

    void heapify_current(off_t i) {
        off_t max = ((lchild(i) < curr_length
                             && array[lchild(i)] > array[i])
                            ? lchild(i) : i);
        if (rchild(i) < curr_length && array[i] > array[max])
            max = rchild(i);
        if (max != i) {
            swap(array[i], array[max]);
            heapify_current(max);//, curr_length);
        }
    }

    void heapify_next(off_t i) {
        off_t max = ((curr_length + lchild(i) < S
                             && array[curr_length + lchild(i)]
                             > array[curr_length + i])
                            ? lchild(i) : i);
        if (curr_length + rchild(i) < S
            && array[curr_length + i] > array[curr_length + max])
            max = rchild(i);
        if (max != i) {
            swap(array[curr_length + i], array[curr_length + max]);
            heapify_next(max);
        }        
    }
    
    void heapify(unsigned int start, unsigned int end) {
        unsigned int max = ((lchild(start) < end
                             && array[lchild(start)] > array[start])
                            ? lchild(start) : start);
        if (rchild(start) < end && array[start] > array[max])
            max = rchild(start);
        if (max != start) {
            swap(array[start], array[max]);
            heapify(max, end);
        }
    }

    void swap() { curr_length = S; }    

private:
    unsigned int lchild(unsigned int i) const { return 2*i+1; }
    unsigned int rchild(unsigned int i) const { return 2*i+2; }
    unsigned int parent(unsigned int i) const { return (i-1)/2; }
    void swap(R& x, R& y) { R t = x; x = y; y = t; }

public:
    std::ostream& toTree(std::ostream& os,
                         unsigned int index = 0,
                         unsigned int level = 0) const {
        for (unsigned int i = 0; i < level; i++) os << " ";
        os << array[index] << "\n";
        if (lchild(index) < curr_length) toTree(os, lchild(index), level+1);
        if (rchild(index) < curr_length) toTree(os, rchild(index), level+1);
        return os;
    }
};

};

#endif //:~ __HEAP_HH__
