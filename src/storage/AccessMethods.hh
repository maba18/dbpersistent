#ifndef ACCESSMETHODS_HH
#define ACCESSMETHODS_HH

#include "../storage/BufferManager.hh"

namespace deceve { namespace storage {

class Scanner {
public:
  Scanner(BufferManager *bm, const std::string& s)
    : bufferManager(bm), filename(s), pagesNo(0), currentOffset(0)
  {};
  ~Scanner() {}

  void open();
  bool hasNext();
  const char* nextPage();
  void close();
private:
  BufferManager* bufferManager;
  std::string filename;
  offset_t pagesNo;
  offset_t currentOffset;
};

}}

#endif
