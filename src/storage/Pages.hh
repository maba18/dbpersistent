#ifndef PAGE_HH
#define PAGE_HH

#include "../utils/helpers.hh"
#include "../utils/types.hh"
#include <typeinfo>	// std::bad_cast
#include <cstring>	// memcpy

using namespace std;

namespace deceve { namespace storage {

template <typename K, typename P>
struct Record: public boost::totally_ordered<Record<K, P> > {
    K key;
    P payload;
	
    bool operator==(const Record<K, P>& r) const { 
	return key == r.key;
    }
    bool operator<(const Record<K, P>& r) const {
	return key < r.key;
    }
    friend std::ostream& operator<<(std::ostream& os,
				    const Record<K, P>& r) {
	os << "<" << r.key << ", " << r.payload << ">";
	return os;
    }		
};
	
template <typename K, typename P>
Record<K, P> makeRecord(const K& k, const P& p) {
    Record<K, P> rec;
    rec.key = k;
    rec.payload = p;
    return rec;
}
	
template <typename K, typename P>
Record<K, P> makeLookupRecord(const K& k) {
    Record<K, P> rec; 
    rec.key = k; 
    return rec;
}


class PageBase {
public:
    PageBase() {}
    virtual ~PageBase() {}
};
	
template <typename H, typename R>
class PackedPage : public PageBase {
private:
    static const unsigned int dataSize =
	PAGE_SIZE_PERSISTENT - sizeof(H) - sizeof(unsigned int) - sizeof(void*);
    static const unsigned int alloc = dataSize / sizeof(R);
    H header;
    union {
    	R records[alloc];
	char padding[dataSize - (dataSize % sizeof(void *))];
    } data;
    unsigned int numRecs;
    
// In dataSize declaration, sizeof(void*) is the size of the pointer
// to the table of virtual functions (since PageBase has a virtual destructor). 
// In the declaration of padding sizeof(void *) is used to make sure
// that the actual size of the union will be sizeof(void*)-aligned,
// but not greater than dataSize. Thus, enough space will be left for
// numRecords, both on a 32-bit machine, as well as on a 64-bit one.
    
public:
    PackedPage(): PageBase(), header(), numRecs(0) {
/*	std::cout << "Page starts at\t\t" << (unsigned long) 0 << "\n";
	std::cout << "header starts at\t" << (unsigned long) &header - (unsigned long) this << "\n";
	std::cout << "data starts at\t\t" << (unsigned long) &data - (unsigned long) this << "\n";
	std::cout << "numRecs starts at\t" << (unsigned long) &numRecs - (unsigned long) this << "\n";
	std::cout << "header =  " << sizeof(header) << "\n";
	std::cout << "record =  " << sizeof(R) << "\n";
	std::cout << "data   =  " << sizeof(data) << "\n";
	std::cout << "alloc  =  " << alloc << "\n";
	std::cout <<  sizeof(*this) << "\n"; */
	//std::cout << "alloc " << alloc << "\n";
    }
    ~PackedPage() {}
    
    void setHeader(const H& h) { header = h; }
    const H getHeader() const { return header; }
    H& getHeader() { return header; }

    size_t sizeOfData() { return sizeof(data); }
    unsigned int getAlloc() const { return alloc; }
    unsigned int numRecords() const { return numRecs; }
    unsigned int capacity() const { return alloc; }
    void clear() { numRecs = 0; memset(&data, 0, sizeof(data));}
    const R getRecord(unsigned int i) const { 
	require(i < numRecs, "Array index out of bounds.1");
	return data.records[i]; 
    }
    R& getRecord(unsigned int i) {
	if (i >= numRecs)
	    std::cout << "i:\t" << i << "numRecs:\t" << numRecs << "\n";
	require(i < numRecs, "Array index out of bounds.2");
	return data.records[i];
    }
    const R operator[](unsigned int i) const { 
	require(i < numRecs, "Array index out of bounds.3");
	return data.records[i]; 
    }
    R& operator[](unsigned int i) { 
	require(i < numRecs, "Array index out of bounds.4");
	return data.records[i]; 
    }
		
    virtual bool addRecord(const R& r) {
	if (numRecs == alloc) return false;
	data.records[numRecs++] = r;
	return true;
    }
		
    virtual bool addRecord(unsigned int i, const R& r) {
	if (numRecs == alloc) return false;
	memmove((char*) &data.records[i+1], (char*) &data.records[i],
	       (numRecs-i)*sizeof(R));
	data.records[i] = r;
	numRecs++;
	return true;
    }
		
    virtual void setRecord(unsigned int i, const R&r) {
	require(i < numRecs, "Array index out of bounds.5");
	data.records[i] = r;
    }
		
    bool hasRoom() const { return numRecs < alloc; }
    
    bool removeRecord(unsigned int i) {
	require(i < numRecs, "Array index out of bounds.6");
	memmove((char*) &data.records[i], (char*) &data.records[i+1],
	       (numRecs-i-1)*sizeof(R));
	numRecs--;
	return true;
    }
    
    class iterator;
    friend class iterator;
    class iterator: public
    std::iterator<std::forward_iterator_tag, R> {
    private:
	PackedPage& page;
	unsigned int idx;
    public:
	friend class PackedPage;
	iterator(PackedPage& p): page(p), idx(0) {}
	iterator(PackedPage& p, bool): page(p), idx(p.numRecords()) {}
	unsigned int index() { return idx; }
	R& current() { return page.getRecord(idx); }
	R& operator*() { return current(); }
	R* operator->() { return &current(); }
	iterator& operator++() { ++idx; return *this; }
	iterator& operator++(int) { idx++; return *this; }
	bool operator==(const iterator& it) { return idx == it.idx; }
	bool operator!=(const iterator& it) { return idx != it.idx; }
    };
		
    iterator begin() { return iterator(*this); }
    iterator end() { return iterator(*this, true); }
    iterator find(const R& r) {
	if (numRecords() == 0) return end();
	for (iterator it = begin(); it != end(); it++) {
	    if (*it == r) return it;
	}
	return end();
    }
    void update(iterator& it, const R& r) {
	std::cout << "Hello, mpagasako..." << "\n";
	setRecord(it.idx, r);
    }
    bool insert(iterator& it, const R& r) { return addRecord(it.idx, r); }
    bool remove(iterator& it) { return removeRecord(it.idx); }
};


template <typename H, typename R>
class OrderedPage: public PackedPage<H, R> {
    public:
    typedef typename PackedPage<H, R>::iterator iterator;
    
    OrderedPage(): PackedPage<H, R>() {}
    ~OrderedPage() {}
		
    bool addRecord(const R& r) {
	if (! this->hasRoom()) return false;
	unsigned int i = 0;
	while (i < this->numRecords() && this->getRecord(i) < r) i++;
	return PackedPage<H, R>::addRecord(i, r);
    }
		
    bool insert(iterator& it, const R& r) { 
	return addRecord(it.index(), r); 
    }
    bool addRecord(unsigned int idx, const R& r) {
	bool pred = true;
	if (idx == 0 && this->numRecords() > 1) 
	    pred = this->getRecord(idx+1) >= r;
	else if (idx == this->numRecords()-1)
		pred = this->getRecord(idx) <= r;
	else if (idx < this->numRecords())
	    pred = this->getRecord(idx-1) <= r
		&& this->getRecord(idx) >= r;
	
	require(pred, "Record is out of order.");
	return PackedPage<H, R>::addRecord(idx, r);
    }
    void setRecord(unsigned int idx, const R& r) {
	bool pred = true;
	if (idx == 0 && this->numRecords() > 1) 
	    pred = this->getRecord(idx+1) >= r;
	else if (idx == this->numRecords()-1)
	    pred = this->getRecord(idx) <= r;
	else if (idx < this->numRecords())
	    pred = this->getRecord(idx-1) <= r
		&& this->getRecord(idx) >= r;
			
	require(pred, "Record is out of order.");
	PackedPage<H, R>::setRecord(idx, r);
    }
		
    iterator find(const R& r) {
	if (this->numRecords() == 0) return this->end();
	for (iterator it = this->begin(); it != this->end();
	     it++) {
	    if (*it == r) return it;
	}
	
	return this->end();
    }
};
	
template <typename H, typename R>
class SetOrderedPage: public OrderedPage<H, R> {
public:
    typedef typename OrderedPage<H, R>::iterator iterator;
    
    SetOrderedPage(): OrderedPage<H, R>() {}
    ~SetOrderedPage() {}

    bool addRecord(const R& r) {
	//if (! this->hasRoom()) return false;
	iterator it = this->find(r);
	if (it != this->end()) {
	    this->update(it, r);
	    return true;
	}
	//return OrderedPage<H, R>::insert(it, r);
	return OrderedPage<H, R>::addRecord(r);
    }
    
    void setRecord(unsigned int idx, const R& r) {
	bool pred = true;
	if (idx == 0 && this->numRecords() > 1) 
	    pred = this->getRecord(idx+1) >= r;
	else if (idx == this->numRecords()-1)
	    pred = this->getRecord(idx) <= r;
	else if (idx < this->numRecords())
	    pred = this->getRecord(idx-1) <= r
		&& this->getRecord(idx) >= r;
	
	require(pred, "Record is out of order.");
	OrderedPage<H, R>::setRecord(idx, r);
    }
    
};

}}

#endif	// PAGE_HH //:~
