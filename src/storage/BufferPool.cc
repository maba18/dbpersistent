/*******************************************************************************
 * Copyright (c) 2015 Michail Basios and Stratis Viglas.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michail Basios - initial API and implementation
 *     Stratis Viglas - initial API and implementation
 *******************************************************************************/
// BufferPool header
#include "../storage/BufferPool.hh"
// C headers
#include <fcntl.h>
#include <boost/multi_index_container.hpp>
// C++ headers
#include <string>
#include <iostream>
#include <iterator>
// Other Headers
#include "../utils/helpers.hh"
#include "../utils/types.hh"
#include "../storage/File.hh"
#include "../storage/MemPage.hh"
#include "../utils/global.hh"

namespace deceve {
namespace storage {

MemPage &BufferPool::fetchPage(const std::string &fname, offset_t offt,
                               FileMode fmode) {
  return fetchPage(FilePos(fname, offt), fmode);
}

MemPage &BufferPool::fetchPage(const FilePos &filepos, FileMode fmode) {
  if (fmode == INTERMEDIATE) {
    if (flagNumberForIntermediatePages <= 0) {
      flagNumberForIntermediatePages = 0;
    } else {
      flagNumberForIntermediatePages--;
    }
  }

  //#ifdef CLEANTEST
  //  return fetchPageClean(filepos, fmode);
  //#elif CLEANTEST2
  //  return fetchPageClean2(filepos, fmode);
  //#else
  //  return fetchPageDefault(filepos, fmode);
  //#endif

  if (getAlgorithm() == 2) {
    return fetchPageClean(filepos, fmode);
  } else if (getAlgorithm() == 3) {
    return fetchPageClean2(filepos, fmode);
  } else {
    return fetchPageDefault(filepos, fmode);
  }
}

MemPage &BufferPool::fetchPageDefault(const FilePos &filepos, FileMode fmode) {
  counterOfPrints++;
  incrementReferences(fmode);

  // startTimer = rdtsc();

  MemPageIndex *index = findIndex(fmode);
  mpi_by_filepos::const_iterator it = index->get<FilePos>().find(filepos);

  if (it != index->get<FilePos>().end()) {
    MemPage &fetched_page = page(it);
    updateTimestamp(index, it);
    incrementHits(fmode);
    return fetched_page;
  } else {
    MemPage &fetched_page = physicalReadPage(filepos, fmode);
    return fetched_page;
  }
}

// fetch a page by using a two-list clean replacement policy
// no additional indexes are used
MemPage &BufferPool::fetchPageClean(const FilePos &filepos, FileMode fmode) {
  counterOfPrints++;
  incrementReferences(fmode);
  return fetchFromPrimaryMode(filepos, fmode, &pg_index,
                              &intermediate_pg_index);
}

// TODO clean2-new
// fetch a page by using a two-list clean replacement policy
// other indexes (intermediate, auxiliary) are used as well
MemPage &BufferPool::fetchPageClean2(const FilePos &filepos, FileMode fmode) {
  counterOfPrints++;
  incrementReferences(fmode);

  // check if page is in any of the two indexes of primary mode
  if (fmode == sto::PRIMARY) {
    return fetchFromPrimaryMode(filepos, fmode, &pg_index,
                                &internal_intermediate_index);
  }
  // startTimer = rdtsc();
  MemPageIndex *index = findIndex(fmode);
  mpi_by_filepos::const_iterator it = index->get<FilePos>().find(filepos);

  if (it != index->get<FilePos>().end()) {
    MemPage &fetched_page = page(it);
    // @HOT do you really need an update to the timestamp here?
    //    updateTimestamp(index, it);
    incrementHits(fmode);
    return fetched_page;
  } else {
    MemPage &fetched_page = physicalReadPage(filepos, fmode);
    return fetched_page;
  }
}

MemPage &BufferPool::fetchFromPrimaryMode(const FilePos &filepos,
                                          FileMode fmode,
                                          MemPageIndex *primaryIndex,
                                          MemPageIndex *intermediateIndex) {
#ifdef PROFILING
  timerFetchFromPrimaryMode.start();
#endif

  MemPageIndex *index = primaryIndex;
  mpi_by_filepos::const_iterator it = index->get<FilePos>().find(filepos);

#ifdef PROFILING
  timerFetchFromPrimaryMode.end(0);
#endif

  // Look in the first index
  if (it != index->get<FilePos>().end()) {
#ifdef PROFILING
    timerFetchFromPrimaryMode.start();
#endif

    incrementHits(fmode);
    MemPage &fetched_page = page(it);
    updateTimestamp(index, it);

#ifdef PROFILING
    timerFetchFromPrimaryMode.end(1);
#endif

    return fetched_page;
  } else {
// Look at the second index

#ifdef PROFILING
    timerFetchFromPrimaryMode.start();
#endif

    index = intermediateIndex;
    it = index->get<FilePos>().find(filepos);

    if (it != index->get<FilePos>().end()) {
      incrementHits(fmode);
      MemPage &fetched_intermediate_page = page(it);
      // updateTimestamp(index, it);
      // return fetched_intermediate_page;
      fetched_intermediate_page.setTimestamp(getNextTime());
      //  Move page from second to the first lru queue
      MemPageP intermediate_memory_page = MemPageP(&fetched_intermediate_page);
      primaryIndex->insert(intermediate_memory_page);
      intermediateIndex->erase(fetched_intermediate_page.getFilepos());
      movePage(primaryIndex, intermediateIndex);

#ifdef PROFILING
      timerFetchFromPrimaryMode.end(2);
#endif

      return intermediate_memory_page.ref;
    } else {
#ifdef PROFILING
      timerFetchFromPrimaryMode.start();
#endif
      MemPage &fetched_page = physicalReadPage(filepos, fmode);
#ifdef PROFILING
      timerFetchFromPrimaryMode.end(3);
#endif
      return fetched_page;
    }
  }
}

MemPage &BufferPool::physicalReadPage(const FilePos &filepos, FileMode fmode) {
  //#ifdef CLEANTEST
  //  return physicalReadPageClean(filepos, fmode);
  //#elif CLEANTEST2
  //  return physicalReadPageClean2(filepos, fmode);
  //#else
  //  return physicalReadPageDefault(filepos, fmode);
  //#endif

  if (getAlgorithm() == 2) {
    return physicalReadPageClean(filepos, fmode);
  } else if (getAlgorithm() == 3) {
    return physicalReadPageClean2(filepos, fmode);
  } else {
    return physicalReadPageDefault(filepos, fmode);
  }
}

MemPage &BufferPool::physicalReadPageDefault(const FilePos &filepos,
                                             FileMode fmode) {
  // 1) Request a page from the bufferpool
  char *pool_offset = NULL;
  pool_offset = requestPage(fmode);

  assert(pool_offset != NULL &&
         "Buffer full of pinned pages. Rollback required...");

  // 2) Read page from the persistent memory
  sm->readPage(filepos.filename, filepos.offset, pool_offset, fmode);
  MemPageP newpage = MemPageP(new MemPage(filepos, pool_offset, getNextTime()));

  if (fmode == PRIMARY) {
    findIndex(fmode)->insert(newpage);
  } else if (fmode == INTERMEDIATE) {
    findIndex(fmode)->insert(newpage);
    if (algorithmId == 3 || algorithmId == 5 || algorithmId == 6 ||
        algorithmId == 7 || algorithmId == 8 || algorithmId == 10 ||
        algorithmId == 15) {
      //  table_key intermediateTK =
      // sm->getNameFromConverter(filepos.filename);
      bool check = true;
      auto itConverter = sm->nameConverter.find(filepos.filename);
      if (itConverter == sm->nameConverter.end()) {
        // std::cout << "FILENAME ERROR: " << filepos.filename <<
        // std::endl;
        check = false;
      }
      if (check) {
        table_key intermediateTK = itConverter->second;

        if (indexMapTkIndex.find(intermediateTK) == indexMapTkIndex.end()) {
          indexMapTkIndex.insert(intermediateTK, new MemPageIndex);
          indexMapTkIndex.find(intermediateTK)->second->insert(newpage);
        } else {
          indexMapTkIndex.find(intermediateTK)->second->insert(newpage);
        }
      }
    }
    //   endTimer = rdtsc();
    //   sm->increaseCounter3(endTimer - startTimer);
  } else if (fmode == FREE) {
    findIndex(fmode)->insert(newpage);
  } else if (fmode == FREE_INTERMEDIATE) {
    findIndex(fmode)->insert(newpage);
  } else if (fmode == AUXILIARY) {
    findIndex(fmode)->insert(newpage);
  }
  return newpage.ref;
}

MemPage &BufferPool::physicalReadPageClean(const FilePos &filepos,
                                           FileMode fmode) {
#ifdef PROFILING
  timerPhysicalReadPageClean.start();
#endif

  // 1) Request a page from the bufferpool
  char *pool_offset = NULL;
  pool_offset = requestPage(fmode);

#ifdef PROFILING
  timerPhysicalReadPageClean.end(0);
#endif

  assert(pool_offset != NULL &&
         "Buffer full of pinned pages. Rollback required...");

#ifdef PROFILING
  timerPhysicalReadPageClean.start();
#endif
  // 2) Read page from the persistent memory
  sm->readPage(filepos.filename, filepos.offset, pool_offset, fmode);

#ifdef PROFILING
  timerPhysicalReadPageClean.end(1);
#endif

#ifdef PROFILING
  timerPhysicalReadPageClean.start();
#endif

  MemPageP newpage = MemPageP(new MemPage(filepos, pool_offset, getNextTime()));

#ifdef PROFILING
  timerPhysicalReadPageClean.end(2);
#endif

#ifdef PROFILING
  timerPhysicalReadPageClean.start();
#endif
  // 3) Write page to proper index refering to the bufferpool
  // If clean with two lists move a page from primary
  // Index to the second index
  pg_index.insert(newpage);
  if (pg_index.size() >= pool_size / 2) {
    movePage(&pg_index, &intermediate_pg_index);
  }

#ifdef PROFILING
  timerPhysicalReadPageClean.end(3);
#endif

  return newpage.ref;
}

MemPage &BufferPool::physicalReadPageClean2(const FilePos &filepos,
                                            FileMode fmode) {
  // 1) Request a page from the bufferpool
  char *pool_offset = NULL;
  pool_offset = requestPage(fmode);

  assert(pool_offset != NULL &&
         "Buffer full of pinned pages. Rollback required...");

  // 2) Read page from the persistent memory
  sm->readPage(filepos.filename, filepos.offset, pool_offset, fmode);
  MemPageP newpage = MemPageP(new MemPage(filepos, pool_offset, getNextTime()));

  // 3) Write page to proper index refering to the bufferpool
  // If clean with two lists move a page from primary
  // Index to the second index

  if (fmode == PRIMARY) {
    pg_index.insert(newpage);
    if (pg_index.size() >= pool_size / 2) {
      movePage(&pg_index, &internal_intermediate_index);
    }
  } else if (fmode == INTERMEDIATE) {
    findIndex(fmode)->insert(newpage);
    if (algorithmId == 3 || algorithmId == 5 || algorithmId == 6 ||
        algorithmId == 7 || algorithmId == 8 || algorithmId == 10 ||
        algorithmId == 15) {
      //  table_key intermediateTK =
      // sm->getNameFromConverter(filepos.filename);
      bool check = true;
      auto itConverter = sm->nameConverter.find(filepos.filename);
      if (itConverter == sm->nameConverter.end()) {
        // std::cout << "FILENAME ERROR: " << filepos.filename <<
        // std::endl;
        check = false;
      }
      if (check) {
        table_key intermediateTK = itConverter->second;

        if (indexMapTkIndex.find(intermediateTK) == indexMapTkIndex.end()) {
          indexMapTkIndex.insert(intermediateTK, new MemPageIndex);
          indexMapTkIndex.find(intermediateTK)->second->insert(newpage);
        } else {
          indexMapTkIndex.find(intermediateTK)->second->insert(newpage);
        }
      }
    }
    //   endTimer = rdtsc();
    //   sm->increaseCounter3(endTimer - startTimer);
  } else if (fmode == FREE) {
    findIndex(fmode)->insert(newpage);
  } else if (fmode == FREE_INTERMEDIATE) {
    findIndex(fmode)->insert(newpage);
  } else if (fmode == AUXILIARY) {
    findIndex(fmode)->insert(newpage);
  }

  return newpage.ref;
}

char *BufferPool::requestPage(const FileMode &mode) {
  if (used_pages.load() == pool_size) {
    switch (algorithmId) {
      case 0:
        return evictPageByCostLRU(mode);
      case 1:
        return evictPageSillyLRU(mode);  // Basic LRU implementation
      case 2:
        return evictPageCleanFirstLRU2Lists(mode);  // Clean lru with two lists
      case 3:
        return evictPageCleanFirstLRU2ListsClever(mode);
      //        return evictPageByCost(mode);
      case 4:
        return evictPageSillyCRU(mode);  // Basic clean first
      case 5:
        return evictPageByFileCostClean(mode);
      case 6:
        return evictPageByRanking(mode);  // Ranked replacement
      case 7:
        return evictPageByRankingClean(mode);
      case 8:
        return evictPageByRankingRandomClean(mode);
      case 9:
        return evictPageCleanFirst();
      case 10:
        return evictPageWithLRU(mode);
      case 15:
        return evictPageByRankingRandom(mode);  // Adaptive Ranked Replacement
      default:
        return evictPageSillyLRU(mode);
    }
  } else {
    used_pages++;
    return &pool[(used_pages.load()) * PAGE_SIZE_PERSISTENT];
  }
}

// char *BufferPool::evictPage() {
// #ifdef EVICT_BY_TYPE
//  return evictPageByType();
// #elif EVICT_CLEAN_FIRST
//  return evictPageCleanFirst();
// #elif EVICT_BY_TYPE_CLEAN_FIRST
//  return evictPageByTypeCleanFirst();
// #else
//  return evictLRUPage();
// #endif
//}

char *BufferPool::evictPageSillyLRU(FileMode mode) {
  //   u_int64_t startTimer;
  //   u_int64_t endTimer;

  MemPageIndex *index1 = nullptr;
  //  startTimer = rdtsc();
  //################### Check for a free page #######################
  std::pair<char *, bool> freePageTuple = getAFreePage(mode);
  //  endTimer = rdtsc();
  //  sm->increaseOtherTimer(endTimer - startTimer);

  if (freePageTuple.second) return freePageTuple.first;
  //################### Prioritize indexes #######################

  index1 = &pg_index;

  //  startTimer = rdtsc();
  mpi_by_time::const_iterator primaryIt;
  primaryIt = findCandidate(index1->get<Timestamp>().begin(),
                            index1->get<Timestamp>().end());
  //  endTimer = rdtsc();
  //  sm->increaseLruTimer(endTimer - startTimer);

  if (primaryIt != index1->get<Timestamp>().end()) {
    MemPage &pg = page(primaryIt);
    char *freepage = pg.getData();

    if (pg.isDirty()) {
      //      startTimer = rdtsc();
      sm->writePage(pg, PRIMARY);
      //      endTimer = rdtsc();
      //      sm->increaseCleanTimer(endTimer - startTimer);
    }
    //  startTimer = rdtsc();
    index1->erase(pg.getFilepos());

    //    index1->erase(index->key_extractor()(*primaryIt));
    //    c.key_extractor()(*it)
    delete &pg;
    //   endTimer = rdtsc();
    //   sm->increaseOtherTimer2(endTimer - startTimer);
    return freepage;
  }
  assert((primaryIt != index1->get<Timestamp>().end()) &&
         "Cannot evict page; all pages pinned");
  return nullptr;
}

char *BufferPool::evictPageSillyCRU(FileMode mode) {
  //################### Check for a free page #######################
  // Find Cost
  //  u_int64_t startTimer;
  // u_int64_t endTimer;

  // startTimer = rdtsc();
  std::pair<char *, bool> freePageTuple = getAFreePage(mode);
  // endTimer = rdtsc();
  // sm->increaseOtherTimer(endTimer - startTimer);
  if (freePageTuple.second) return freePageTuple.first;

  // startTimer = rdtsc();
  //################### Prioritize indexes #######################
  std::pair<bool, mpi_by_dirtiness::const_iterator> pair =
      findCandidate(&pg_index, false);
  if (!pair.first) {
    pair = findCandidate(&pg_index, true);
  }
  //  endTimer = rdtsc();
  //  sm->increaseLruTimer(endTimer - startTimer);

  MemPage &pg = page(pair.second);
  char *freepage = pg.getData();
  // Assert Cost
  //  assert(pair.first && "Cannot evict page; all pages pinned");

  // Write page cost
  if (pg.isDirty()) {
    //   startTimer = rdtsc();
    sm->writePage(pg, PRIMARY);
    //  endTimer = rdtsc();
    //  sm->increaseCleanTimer(endTimer - startTimer);
  }

  // Erase Cost
  //  startTimer = rdtsc();
  pg_index.erase(pg.getFilepos());
  delete &pg;
  //  endTimer = rdtsc();
  //  sm->increaseOtherTimer2(endTimer - startTimer);
  return freepage;
}

char *BufferPool::evictPageCleanFirstLRU2Lists(FileMode mode) {
//################### Check for a free page #######################

#ifdef PROFILING
  timerEvictPageTwoLists.start();
#endif
  std::pair<char *, bool> freePageTuple = getAFreePage(mode);
  if (freePageTuple.second) {
    return freePageTuple.first;
#ifdef PROFILING
    timerEvictPageTwoLists.end(0);
#endif
  }

//################### Find from which category to get page first
//########### GET SECOND QUEUE ################
#ifdef PROFILING
  timerEvictPageTwoLists.start();
#endif
  std::pair<char *, bool> freePageFromSecondQueue =
      getCleanPageOrDirtyFromIndex(&intermediate_pg_index);
  if (freePageFromSecondQueue.second) {
#ifdef PROFILING
    timerEvictPageTwoLists.end(1);
#endif
    return freePageFromSecondQueue.first;
  }

#ifdef PROFILING
  timerEvictPageTwoLists.start();
#endif
  std::pair<char *, bool> freePageFromFirstQueue = getPageFromIndex(&pg_index);
  if (freePageFromFirstQueue.second) {
#ifdef PROFILING
    timerEvictPageTwoLists.end(2);
#endif
    return freePageFromFirstQueue.first;
  }

  assert(false && "no free page available");
  return nullptr;
}

char *BufferPool::evictPageCleanFirstLRU2ListsClever(FileMode mode) {
  if (algorithm3Case == 1) {
    // DRAM
    return evictPageCleanFirstLRU2ListsCleverCase1(mode);
  } else if (algorithm3Case == 2) {
    // PERSISTENT
    return evictPageCleanFirstLRU2ListsCleverCase2(mode);
  }

//################### Check for a free page #######################
#ifdef PROFILING
  timerEvictPageTwoListsClever.start();
#endif
  std::pair<char *, bool> freePageTuple = getAFreePage(mode);
  if (freePageTuple.second) {
    return freePageTuple.first;
  }
#ifdef PROFILING
  timerEvictPageTwoListsClever.end(0);
#endif

  //### GET FROM INTERMEDIATE TEMPORAL ###
  std::pair<char *, bool> freePrimaryPageTuple;

#ifdef PROFILING
  timerEvictPageTwoListsClever.start();
#endif
  freePrimaryPageTuple = getCleanIntermediatePageWithFlag(PERMANENT, false);
  if (freePrimaryPageTuple.second) {
#ifdef PROFILING
    timerEvictPageTwoListsClever.end(1);
#endif
    return freePrimaryPageTuple.first;
  }

//## GET FROM INTERMEDIATE TEMPORAL ##
#ifdef PROFILING
  timerEvictPageTwoListsClever.start();
#endif
  freePrimaryPageTuple = getCleanIntermediatePageWithFlag(TEMPORAL, false);
  if (freePrimaryPageTuple.second) {
#ifdef PROFILING
    timerEvictPageTwoListsClever.end(2);
#endif
    return freePrimaryPageTuple.first;
  }

//  //### GET FROM PRIMARY ##
//  freePrimaryPageTuple = getCleanPrimaryPageWithFlag(false);
//  if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

//################### Find from which category to get page first
//########### GET SECOND QUEUE ################
#ifdef PROFILING
  timerEvictPageTwoListsClever.start();
#endif
  std::pair<char *, bool> freePageFromSecondQueue =
      getCleanPageOrDirtyFromIndex(&internal_intermediate_index);
  if (freePageFromSecondQueue.second) {
#ifdef PROFILING
    timerEvictPageTwoListsClever.end(1);
#endif
    return freePageFromSecondQueue.first;
  }

#ifdef PROFILING
  timerEvictPageTwoListsClever.start();
#endif
  std::pair<char *, bool> freePageFromFirstQueue = getPageFromIndex(&pg_index);
  if (freePageFromFirstQueue.second) {
#ifdef PROFILING
    timerEvictPageTwoListsClever.end(2);
#endif
    return freePageFromFirstQueue.first;
  }

// GET SOME DIRTY PAGE
#ifdef PROFILING
  timerEvictPageTwoListsClever.start();
#endif
  freePrimaryPageTuple = getCleanIntermediatePageWithFlag(PERMANENT, true);
  if (freePrimaryPageTuple.second) {
#ifdef PROFILING
    timerEvictPageTwoListsClever.end(6);
#endif
    return freePrimaryPageTuple.first;
  }

//## GET FROM INTERMEDIATE TEMPORAL ##
#ifdef PROFILING
  timerEvictPageTwoListsClever.start();
#endif
  freePrimaryPageTuple = getCleanIntermediatePageWithFlag(TEMPORAL, true);
  if (freePrimaryPageTuple.second) {
#ifdef PROFILING
    timerEvictPageTwoListsClever.end(7);
#endif
    return freePrimaryPageTuple.first;
  }

#ifdef PROFILING
  timerEvictPageTwoListsClever.start();
#endif
  //### GET FROM AUXILIARY ##
  freePrimaryPageTuple = getAuxiliaryPage();
  if (freePrimaryPageTuple.second) {
#ifdef PROFILING
    timerEvictPageTwoListsClever.end(3);
#endif
    return freePrimaryPageTuple.first;
  }

  //  std::cout << "Auxiliary page NULL: " << std::endl;

  assert(false && "no free page available");
  return nullptr;
}

// Store Data structures in DRAM
char *BufferPool::evictPageCleanFirstLRU2ListsCleverCase1(FileMode mode) {
  //################### Check for a free page #######################
  std::pair<char *, bool> freePageTuple = getAFreePage(mode);
  if (freePageTuple.second) {
    return freePageTuple.first;
  }

  //### GET FROM INTERMEDIATE TEMPORAL ###
  std::pair<char *, bool> freePrimaryPageTuple;

  //  //### GET FROM PRIMARY ##
  //  freePrimaryPageTuple = getCleanPrimaryPageWithFlag(false);
  //  if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

  //################### Find from which category to get page first
  //########### GET SECOND QUEUE ################
  std::pair<char *, bool> freePageFromSecondQueue =
      getCleanPageOrDirtyFromIndex(&internal_intermediate_index);
  if (freePageFromSecondQueue.second) {
    return freePageFromSecondQueue.first;
  }

  std::pair<char *, bool> freePageFromFirstQueue = getPageFromIndex(&pg_index);
  if (freePageFromFirstQueue.second) {
    return freePageFromFirstQueue.first;
  }

  //  //### GET FROM AUXILIARY ##
  //  freePrimaryPageTuple = getAuxiliaryPage();
  //  if (freePrimaryPageTuple.second) {
  //    return freePrimaryPageTuple.first;
  //  }

  freePrimaryPageTuple = getCleanIntermediatePageWithFlag(PERMANENT, false);
  if (freePrimaryPageTuple.second) {
    return freePrimaryPageTuple.first;
  }

  //## GET FROM INTERMEDIATE TEMPORAL ##
  freePrimaryPageTuple = getCleanIntermediatePageWithFlag(TEMPORAL, false);
  if (freePrimaryPageTuple.second) {
    return freePrimaryPageTuple.first;
  }

  // GET SOME DIRTY PAGE
  freePrimaryPageTuple = getCleanIntermediatePageWithFlag(PERMANENT, true);
  if (freePrimaryPageTuple.second) {
    return freePrimaryPageTuple.first;
  }

  //## GET FROM INTERMEDIATE TEMPORAL ##
  freePrimaryPageTuple = getCleanIntermediatePageWithFlag(TEMPORAL, true);
  if (freePrimaryPageTuple.second) {
    return freePrimaryPageTuple.first;
  }

  //  std::cout << "Auxiliary page NULL: " << std::endl;

  assert(false && "no free page available");
  return nullptr;
}

char *BufferPool::evictPageCleanFirstLRU2ListsCleverCase2(FileMode mode) {
  //################### Check for a free page #######################
  std::pair<char *, bool> freePageTuple = getAFreePage(mode);
  if (freePageTuple.second) {
    return freePageTuple.first;
  }

  //### GET FROM INTERMEDIATE TEMPORAL ###
  std::pair<char *, bool> freePrimaryPageTuple;

  freePrimaryPageTuple = getCleanIntermediatePageWithFlag(PERMANENT, false);
  if (freePrimaryPageTuple.second) {
    return freePrimaryPageTuple.first;
  }

  //## GET FROM INTERMEDIATE TEMPORAL ##
  freePrimaryPageTuple = getCleanIntermediatePageWithFlag(TEMPORAL, false);
  if (freePrimaryPageTuple.second) {
    return freePrimaryPageTuple.first;
  }

  // GET SOME DIRTY PAGE
  freePrimaryPageTuple = getCleanIntermediatePageWithFlag(PERMANENT, true);
  if (freePrimaryPageTuple.second) {
    return freePrimaryPageTuple.first;
  }

  //## GET FROM INTERMEDIATE TEMPORAL ##
  freePrimaryPageTuple = getCleanIntermediatePageWithFlag(TEMPORAL, true);
  if (freePrimaryPageTuple.second) {
    return freePrimaryPageTuple.first;
  }

  //  //### GET FROM PRIMARY ##
  //  freePrimaryPageTuple = getCleanPrimaryPageWithFlag(false);
  //  if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

  //################### Find from which category to get page first
  //########### GET SECOND QUEUE ################
  std::pair<char *, bool> freePageFromSecondQueue =
      getCleanPageOrDirtyFromIndex(&internal_intermediate_index);
  if (freePageFromSecondQueue.second) {
    return freePageFromSecondQueue.first;
  }

  std::pair<char *, bool> freePageFromFirstQueue = getPageFromIndex(&pg_index);
  if (freePageFromFirstQueue.second) {
    return freePageFromFirstQueue.first;
  }

  //  //### GET FROM AUXILIARY ##
  //  freePrimaryPageTuple = getAuxiliaryPage();
  //  if (freePrimaryPageTuple.second) {
  //    return freePrimaryPageTuple.first;
  //  }

  //  std::cout << "Auxiliary page NULL: " << std::endl;

  assert(false && "no free page available");
  return nullptr;
}

char *BufferPool::evictPageByRankingRandom(FileMode mode) {
  //################### Adapt Slider #######################
  //  updateSliderValue();

  //  if ((counterOfPrints % 10000) == 0) {
  //    std::cout << "ASK PAGE WITH MODE: " << mode << std::endl;
  //    std::cout << "REGION COST: " << costOfRegion
  //              << " PREVIOUS REGION COST: " << previousCostOfRegion <<
  //              std::endl;
  //    std::cout << "FLAGNumberForIntermediatePages "
  //              << flagNumberForIntermediatePages << std::endl;
  //  }
  //################### Check for a free page #######################
  std::pair<char *, bool> freePageTuple = getAFreePage(mode);
  if (freePageTuple.second) {
    //    if ((counterOfPrints % 10000) == 0)
    //      std::cout << "get FREE: cost of region: " << costOfRegion <<
    //      std::endl;
    return freePageTuple.first;
  }

  //########################### GET FROM INTERMEDIATE PERMANENT
  //########################################
  std::pair<char *, bool> freePrimaryPageTuple;

  freePrimaryPageTuple = getCleanIntermediatePage(PERMANENT);
  if (freePrimaryPageTuple.second) {
    //    if ((counterOfPrints % 10000) == 0)
    //      std::cout << "get a PERMANENT: cost of region: " << costOfRegion
    //                << std::endl;
    return freePrimaryPageTuple.first;
  }

  // std::cout << "intermediateDirtyPages: " << intermediateDirtyPages
  //           << std::endl;

  if (costOfRegion >= 0) {
    // free pages
    //    if ((counterOfPrints % 10000) == 0) {
    //      std::cout << "GET PRIMARY FIRST: cost of region: " <<
    //      costOfRegion
    //                << std::endl;
    //    }
    freePrimaryPageTuple = getCleanPrimaryPage();
    if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;
    //    if ((counterOfPrints % 10000) == 0) {
    //      std::cout << "NO FREE PRIMARY AVAILABLE: ASK TEMPORAL, cost of "
    //                   "region:"
    //                << costOfRegion << std::endl;
    //    }
    freePrimaryPageTuple = getCleanIntermediatePage(TEMPORAL);
    if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;
  } else {
    //    if ((counterOfPrints % 10000) == 0) {
    //      std::cout << "GET TEMPORAL FIRST: cost of region: " <<
    //      costOfRegion
    //                << std::endl;
    //    }

    freePrimaryPageTuple = getCleanIntermediatePage(TEMPORAL);
    if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;
    //
    //    if ((counterOfPrints % 10000) == 0) {
    //      std::cout << "NO FREE TEMPORAL AVAILABLE: ASK PRIMARY" <<
    //      std::endl;
    //    }
    freePrimaryPageTuple = getCleanPrimaryPage();
    if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;
  }

  freePrimaryPageTuple = getAuxiliaryPage();
  if (freePrimaryPageTuple.second) {
    return freePrimaryPageTuple.first;
  } else {
    std::cout << "Auxiliary pape NULL: " << std::endl;
  }

  ////########################### GET FROM PRIMARY
  ///#############################################
  //  std::pair<char *, bool> freePrimaryPageTuple = getPrimaryPage();
  //  if (freePrimaryPageTuple.second)
  //    return freePrimaryPageTuple.first;
  //
  ////########################### GET FROM INTERMEDIATE TEMPORAL
  ///########################################
  //  std::pair<char *, bool> freeIntermediatePageTuple = getIntermediatePage(
  //       TEMPORAL);
  //  if (freeIntermediatePageTuple.second)
  //    return freeIntermediatePageTuple.first;

  assert(false && "no free page available");
  return nullptr;
}

char *BufferPool::evictPageByRankingRandomClean(FileMode mode) {
  //################### Adapt Slider #######################

  std::pair<char *, bool> freePageTuple = getAFreePage(mode);
  if (freePageTuple.second) return freePageTuple.first;

  //### GET FROM INTERMEDIATE TEMPORAL ###
  std::pair<char *, bool> freePrimaryPageTuple;

  freePrimaryPageTuple = getCleanIntermediatePageWithFlag(PERMANENT, false);
  if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

  if (costOfRegion >= 0) {
    //### GET FROM PRIMARY ##
    freePrimaryPageTuple = getCleanPrimaryPageWithFlag(false);
    if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

    //## GET FROM INTERMEDIATE TEMPORAL ##
    freePrimaryPageTuple = getCleanIntermediatePageWithFlag(TEMPORAL, false);
    if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;
  } else {
    //## GET FROM INTERMEDIATE TEMPORAL ##
    freePrimaryPageTuple = getCleanIntermediatePageWithFlag(TEMPORAL, false);
    if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

    //### GET FROM PRIMARY ##
    freePrimaryPageTuple = getCleanPrimaryPageWithFlag(false);
    if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;
  }

  freePrimaryPageTuple = getCleanIntermediatePageWithFlag(PERMANENT, true);
  if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

  if (costOfRegion >= 0) {
    freePrimaryPageTuple = getCleanIntermediatePageWithFlag(TEMPORAL, true);
    if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

    //### GET FROM PRIMARY ##
    freePrimaryPageTuple = getCleanPrimaryPageWithFlag(true);
    if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

    //### GET FROM INTERMEDIATE ##
    freePrimaryPageTuple = getAuxiliaryPage();
    if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

  } else {
    //### GET FROM PRIMARY ##
    freePrimaryPageTuple = getCleanPrimaryPageWithFlag(true);
    if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

    freePrimaryPageTuple = getAuxiliaryPage();
    if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

    //### GET FROM INTERMEDIATE ##
    freePrimaryPageTuple = getCleanIntermediatePageWithFlag(TEMPORAL, true);
    if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;
  }

  std::cout << "Auxiliary page NULL: " << std::endl;

  assert(false && "no free page available");
  return nullptr;
}

char *BufferPool::evictPageByRanking(FileMode mode) {
  //################### Adapt Slider #######################
  updateSliderValue();

  //################### Check for a free page #######################
  std::pair<char *, bool> freePageTuple = getAFreePage(mode);
  if (freePageTuple.second) return freePageTuple.first;

  //########################### GET FROM INTERMEDIATE PERMANENT
  //########################################
  std::pair<char *, bool> freePrimaryPageTuple;

  freePrimaryPageTuple = getCleanPrimaryPage();
  if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

  freePrimaryPageTuple = getCleanIntermediatePage(PERMANENT);
  if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

  // if (intermediateDirtyPages > 0) {
  // free pages
  freePrimaryPageTuple = getCleanIntermediatePage(TEMPORAL);
  if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

  freePrimaryPageTuple = getAuxiliaryPage();
  if (freePrimaryPageTuple.second) {
    return freePrimaryPageTuple.first;
  } else {
    std::cout << "Auxiliary page NULL: " << std::endl;
  }

  ////########################### GET FROM PRIMARY
  ///#############################################
  //  std::pair<char *, bool> freePrimaryPageTuple = getPrimaryPage();
  //  if (freePrimaryPageTuple.second)
  //    return freePrimaryPageTuple.first;
  //
  ////########################### GET FROM INTERMEDIATE TEMPORAL
  ///########################################
  //  std::pair<char *, bool> freeIntermediatePageTuple = getIntermediatePage(
  //       TEMPORAL);
  //  if (freeIntermediatePageTuple.second)
  //    return freeIntermediatePageTuple.first;

  assert(false && "no free page available");
  return nullptr;
}

std::pair<char *, bool> BufferPool::getPrimaryPage() {
  // COUT<< "Get Primary page" << "\n";

  mpi_by_time::const_iterator primaryIt = findCandidate(
      pg_index.get<Timestamp>().begin(), pg_index.get<Timestamp>().end());

  if (primaryIt != pg_index.get<Timestamp>().end()) {
    MemPage &pg = page(primaryIt);

    char *freepage = pg.getData();

    bool isDirty = pg.isDirty();

    decreaseIntermediateCosts(isDirty, PRIMARY);

    if (pg.isDirty()) {
      sm->writePage(pg, PRIMARY);
    }

    pg_index.erase(pg.getFilepos());
    delete &pg;
    return std::make_pair(freepage, true);
  } else {
    return std::make_pair(nullptr, false);
  }
}

std::pair<char *, bool> BufferPool::getAuxiliaryPage() {
  // COUT<< "Get Primary page" << "\n";
  mpi_by_time::const_iterator auxiliaryIt =
      findCandidate(auxiliary_pg_index.get<Timestamp>().begin(),
                    auxiliary_pg_index.get<Timestamp>().end());
  if (auxiliaryIt != auxiliary_pg_index.get<Timestamp>().end()) {
    MemPage &pg = page(auxiliaryIt);

    //    if ((counterOfPrints % 10000) == 0) {
    //      std::cout << "Return auxiliary page: " << pg << std::endl;
    //    }

    char *freepage = pg.getData();
    if (pg.isDirty()) {
      sm->writePage(pg, AUXILIARY);
    }
    auxiliary_pg_index.erase(pg.getFilepos());
    delete &pg;
    return std::make_pair(freepage, true);
  } else {
    return std::make_pair(nullptr, false);
  }
}

std::pair<char *, bool> BufferPool::getIntermediatePage(
    FILE_CLASSIFIER fileClassifier) {
  //########################### GET FROM INTERMEDIATE
  //#############################################

  table_key *leastSignificantTk;
  boost::ptr_map<table_key, MemPageIndex>::iterator indexIt;
  //  double intermediateCacheSize = intermediate_pg_index.size()
  //      + free_intermediate_page_index.size();

  std::vector<scoreTuple> *tmpScores;

  if (fileClassifier == PERMANENT) {
    tmpScores = &permanentScores;
  } else if (fileClassifier == TEMPORAL) {
    tmpScores = &temporalScores;
  } else {
    return std::make_pair(nullptr, false);
  }

  if (fileClassifier == PERMANENT) {
    auto beginIterator = tmpScores->rbegin();
    auto endIterator = tmpScores->rend();

    // std::cout<<"--------------- getIntermediatePage Permanent
    // ------------------------ \n";
    for (auto it = beginIterator; it != endIterator; ++it) {
      leastSignificantTk = &(it->tk);
      indexIt = indexMapTkIndex.find(*leastSignificantTk);
      if (indexIt == indexMapTkIndex.end()) continue;
      double currentSizeInIndex = indexIt->second->size();
      if (currentSizeInIndex == 0) continue;

      MemPageIndex *index = indexIt->second;

      mpi_by_time::const_iterator tempIt = findCandidate(
          index->get<Timestamp>().begin(), index->get<Timestamp>().end());
      if (tempIt != index->get<Timestamp>().end()) {
        MemPage &pg = page(tempIt);
        char *freepage = pg.getData();
        if (pg.isDirty()) {
          sm->writePage(pg, INTERMEDIATE);
        }
        intermediate_pg_index.erase(pg.getFilepos());
        index->erase(pg.getFilepos());
        delete &pg;
        return std::make_pair(freepage, true);
      }
    }

  } else if (fileClassifier == TEMPORAL) {
    auto beginIterator = tmpScores->begin();
    auto endIterator = tmpScores->end();

    // std::cout<<"--------------- getIntermediatePage Permanent
    // ------------------------ \n";
    for (auto it = beginIterator; it != endIterator; ++it) {
      leastSignificantTk = &(it->tk);
      indexIt = indexMapTkIndex.find(*leastSignificantTk);
      if (indexIt == indexMapTkIndex.end()) continue;
      double currentSizeInIndex = indexIt->second->size();
      if (currentSizeInIndex == 0) continue;

      MemPageIndex *index = indexIt->second;

      mpi_by_time::const_iterator tempIt = findCandidate(
          index->get<Timestamp>().begin(), index->get<Timestamp>().end());
      if (tempIt != index->get<Timestamp>().end()) {
        MemPage &pg = page(tempIt);
        char *freepage = pg.getData();

        bool isDirty = pg.isDirty();
        //        if (print_counter % 100000 == 0) {
        //          std::cout << "TEMPORAL DIRTY VALUE IS: " << isDirty
        //          <<
        //          std::endl;
        //        }
        decreaseIntermediateCosts(isDirty, INTERMEDIATE);

        if (pg.isDirty()) {
          sm->writePage(pg, INTERMEDIATE);
          //          if (counterOfPrints % 1000 == 0) {
          //            std::cout << "Get a temporal intemediate page:
          //            Cost of
          //            region"
          //                      << costOfRegion << std::endl;
          //          }
          decreaseTotalNumberOfDirtyIntermediatePages();
          //          if (counterOfPrints % 1000 == 0) {
          //            std::cout <<
          //            "totalNumberOfDirtyIntermediatePages: "
          //                      <<
          //                      totalNumberOfDirtyIntermediatePages
          //                      <<
          //       :               std::endl;
          //          }
        }

        intermediate_pg_index.erase(pg.getFilepos());
        index->erase(pg.getFilepos());
        delete &pg;
        return std::make_pair(freepage, true);
      }
    }
  }

  return std::make_pair(nullptr, false);
}

char *BufferPool::evictPageByFileCostClean(FileMode mode) {
  //################### Adapt Slider #######################
  updateSliderValue();

  //################### Check for a free page #######################
  std::pair<char *, bool> freePageTuple = getAFreePage(mode);
  if (freePageTuple.second) return freePageTuple.first;

  //################### Find from which category to get page first
  //#######################
  int flagAlgorithm = getFlagAlgorithm(mode);

  if (flagAlgorithm == 0) {
    //########################### GET FROM PRIMARY
    //#############################################
    std::pair<char *, bool> freePrimaryPageTuple = getCleanPrimaryPage();
    if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

    //########################### GET FROM INTERMEDIATE
    //########################################
    std::pair<char *, bool> freeIntermediatePageTuple =
        getCleanIntermediateBasicPage();
    if (freeIntermediatePageTuple.second)
      return freeIntermediatePageTuple.first;

  } else if (flagAlgorithm == 1) {
    //########################### GET FROM INTERMEDIATE
    //########################################
    std::pair<char *, bool> freeIntermediatePageTuple =
        getCleanIntermediateBasicPage();
    if (freeIntermediatePageTuple.second)
      return freeIntermediatePageTuple.first;

    //########################### GET FROM PRIMARY
    //#############################################
    std::pair<char *, bool> freePrimaryPageTuple = getCleanPrimaryPage();
    if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

  } else if (flagAlgorithm == 2) {
    //########################### GET FROM INTERMEDIATE
    //########################################
    // std::cout << "Get from Intermediate \n";
    std::pair<char *, bool> freeIntermediatePageTuple =
        getCleanIntermediateBasicPage();
    if (freeIntermediatePageTuple.second)
      return freeIntermediatePageTuple.first;

    //########################### GET FROM PRIMARY
    //#############################################
    // std::cout << "Get from Primary \n";
    std::pair<char *, bool> freePrimaryPageTuple = getCleanPrimaryPage();
    if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

  } else {
    //########################### GET FROM PRIMARY
    //#############################################
    std::pair<char *, bool> freePrimaryPageTuple = getCleanPrimaryPage();
    if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

    //########################### GET FROM INTERMEDIATE
    //########################################
    std::pair<char *, bool> freeIntermediatePageTuple =
        getCleanIntermediateBasicPage();
    if (freeIntermediatePageTuple.second)
      return freeIntermediatePageTuple.first;
  }

  assert(false && "No free page available");
  return nullptr;
}

char *BufferPool::evictPageByRankingClean(FileMode mode) {
  //################### Adapt Slider #######################
  //  updateSliderValue();
  //################### Check for a free page #######################
  std::pair<char *, bool> freePageTuple = getAFreePage(mode);
  if (freePageTuple.second) return freePageTuple.first;

  //### GET FROM INTERMEDIATE TEMPORAL ###
  std::pair<char *, bool> freePrimaryPageTuple;

  //### GET FROM PRIMARY ##
  freePrimaryPageTuple = getCleanPrimaryPageWithFlag(false);
  if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

  //### GET FROM PRIMARY ##
  freePrimaryPageTuple = getCleanPrimaryPageWithFlag(true);
  if (freePrimaryPageTuple.second) {
    return freePrimaryPageTuple.first;
  }

  //### GET FROM AUXILIARY ##
  freePrimaryPageTuple = getAuxiliaryPage();
  if (freePrimaryPageTuple.second) {
    return freePrimaryPageTuple.first;
  }

  freePrimaryPageTuple = getCleanIntermediatePageWithFlag(PERMANENT, false);
  if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

  // GET SOME DIRTY PAGE
  freePrimaryPageTuple = getCleanIntermediatePageWithFlag(PERMANENT, true);
  if (freePrimaryPageTuple.second) {
    return freePrimaryPageTuple.first;
  }

  //## GET FROM INTERMEDIATE TEMPORAL ##
  freePrimaryPageTuple = getCleanIntermediatePageWithFlag(TEMPORAL, false);
  if (freePrimaryPageTuple.second) return freePrimaryPageTuple.first;

  //## GET FROM INTERMEDIATE TEMPORAL ##
  freePrimaryPageTuple = getCleanIntermediatePageWithFlag(TEMPORAL, true);
  if (freePrimaryPageTuple.second) {
    return freePrimaryPageTuple.first;
  }

  std::cout << "Auxiliary page NULL: " << std::endl;

  assert(false && "no free page available");
  return nullptr;
}

char *BufferPool::cflru(FileMode mode) {
  //################### Adapt Slider #######################
  updateSliderValue();

  //################### Check for a free page #######################
  std::pair<char *, bool> freePageTuple = getAFreePage(mode);
  if (freePageTuple.second) return freePageTuple.first;

  //########################### GET FROM INTERMEDIATE TEMPORAL
  //########################################

  MemPageIndex *index = &intermediate_pg_index;

  std::pair<bool, mpi_by_dirtiness::const_iterator> pair =
      findCandidate(index, false);
  if (!pair.first) {
    pair = findCandidate(index, true);
  }

  char *freepage = nullptr;

  if (pair.first) {
    MemPage &pg = page(pair.second);
    freepage = pg.getData();
    if (pg.isDirty()) sm->writePage(pg, INTERMEDIATE);
    intermediate_pg_index.erase(pg.getFilepos());
    delete &pg;
    return freepage;
  }

  assert(pair.first && "cflru: no free intermediate_index page available");
  return nullptr;
}

std::pair<bool, mpi_by_dirtiness::const_iterator> BufferPool::findCandidate(
    MemPageIndex *i, bool v) {
  // COUT << "findCandidate" << "\n";
  // std::lock_guard < std::mutex > lock(findCandidateMutex);

  // u_int64_t startTimer = rdtsc();
  // u_int64_t endTimer;

  bool found = false;
  mpi_by_dirtiness::const_iterator it;

  // COUT << "FIND1" << "\n";
  std::pair<mpi_by_dirtiness::const_iterator, mpi_by_dirtiness::const_iterator>
      pair = i->get<Dirtiness>().equal_range(v);
  it = pair.first;

  if (it == pair.second) {
    // COUT << "Den vrethike kapoio dirty page" << "\n";
    return std::make_pair(false, it);
    // endTimer = rdtsc();
    //  sm->increaseCleanTimer(endTimer - startTimer);
  }
  while (it != pair.second) {
    if (page(it).isPinned()) {
      it++;
    } else {
      found = true;
      break;
    }
  }
  // endTimer = rdtsc();
  // sm->increaseCleanTimer(endTimer - startTimer);
  return std::make_pair(found, it);
}

mpi_by_time::const_iterator BufferPool::findCandidateTwoLists(MemPageIndex *i) {
  //  mpi_by_time::const_iterator primaryIt;
  //
  //
  //  if (freeIndex1->size() != 0) {
  //    //    std::lock_guard<std::mutex> lock(freePgIndexMutex);
  //    primaryIt = freeIndex1->get<Timestamp>().begin();
  //    MemPage &pg = page(primaryIt);
  //    char *freepage = pg.getData();
  //    freeIndex1->erase(pg.getFilepos());
  //    delete &pg;
  //    return std::make_pair(freepage, true);
  //  }

  mpi_by_time::const_iterator primaryIt;

  //  size_t indexSize = i->get<Timestamp>().size() / 2;
  //  size_t counter = 0;

  // find the most LRU clean page from the first half of the list
  for (primaryIt = i->get<Timestamp>().begin();
       primaryIt != i->get<Timestamp>().end(); ++primaryIt) {
    //  if (counter < indexSize) {
    MemPage &pg = page(primaryIt);
    if (!pg.isPinned()) {
      return primaryIt;
    }
    //    }
    //    counter++;
  }
  // find the most LRU dirty page from the first half of the list
  for (primaryIt = i->get<Timestamp>().begin();
       primaryIt != i->get<Timestamp>().end(); ++primaryIt) {
    MemPage &pg = page(primaryIt);
    if (pg.isDirty() && !pg.isPinned()) {
      return primaryIt;
    }
  }

  return primaryIt;
}

/*
 * ######################################################
 *         Utility functions for BufferPool
 * ######################################################
 */
FileMode BufferPool::getIndexMode(FileMode mode) {
  if (mode == PRIMARY) {
    if (free_page_index.size() + pg_index.size() >= sliderCount) {
      return PRIMARY;
    } else {
      return INTERMEDIATE;
    }
  } else {
    if (free_intermediate_page_index.size() + intermediate_pg_index.size() >=
        pool_size - sliderCount) {
      return INTERMEDIATE;
    } else {
      return PRIMARY;
    }
  }
  return INTERMEDIATE;
}

int BufferPool::getFlagAlgorithm(FileMode mode) {
  int flagAlgorithm = 0;
  if (mode == PRIMARY) {
    if (free_page_index.size() + pg_index.size() >= sliderCount) {
      flagAlgorithm = 0;
    } else {
      flagAlgorithm = 1;
    }
  } else {
    if (free_intermediate_page_index.size() + intermediate_pg_index.size() >=
        pool_size - sliderCount) {
      flagAlgorithm = 2;
    } else {
      flagAlgorithm = 3;
    }
  }
  return flagAlgorithm;
}

MemPageIndex *BufferPool::prioritiseIndexes(FileMode mode, MemPageIndex *index1,
                                            MemPageIndex *&index2) {
  if (mode == PRIMARY) {
    if (free_page_index.size() + pg_index.size() >= sliderCount) {
      index1 = &pg_index;
      index2 = &intermediate_pg_index;
    } else {
      index1 = &intermediate_pg_index;
      index2 = &pg_index;
    }
  } else {
    if (free_intermediate_page_index.size() + intermediate_pg_index.size() >=
        pool_size - sliderCount) {
      index1 = &intermediate_pg_index;
      index2 = &pg_index;
    } else {
      index1 = &pg_index;
      index2 = &intermediate_pg_index;
    }
  }
  return index1;
}

/*
 * ######################################################
 *     Functions that are used for the slider sliding
 * ######################################################
 */
void BufferPool::updateSliderValue() {
  //  double newSlider = static_cast<double>(primary_references) /
  //                     (static_cast<double>(primary_references) +
  //                      static_cast<double>(intermediate_references));
  //  std::cout << "newSliderValue" << newSlider << std::endl;
  //    sliderCount = (unsigned long) (((double) (pool_size) * (double)
  //    (newSlider)));
}

void BufferPool::decreaseIntermediateCosts(bool isDirty, FileMode mode) {
  //  if (counterOfPrints % 10000 == 0) {
  //    std::cout << "### DecreaseIntermediateCosts for mode: " << mode
  //              << " and dirty: " << isDirty << std::endl;
  //  }
  double localCost = functionWithRatio();
  //  if ((counterOfPrints) % 10000 == 0) {
  //    std::cout << "localCost: " << localCost << std::endl;
  //    std::cout << "totalNumberOfDirtyIntermediatePages: "
  //              << totalNumberOfDirtyIntermediatePages << std::endl;
  //    std::cout << "pool_size: " << pool_size << std::endl;
  //  }

  if (isDirty) {
    double cost = static_cast<double>(sm->getStorageWriteDelay());
    if (flagNumberForIntermediatePages > 0) {
      costOfRegion = previousCostOfRegion;
    } else {
      if (mode == PRIMARY) {
        decreaseCostIntermediateCache(cost * localCost);
        decreaseCostOfRegion(cost * localCost);
      } else if (mode == INTERMEDIATE) {
        decreaseCostIntermediateCache(cost);
        decreaseCostOfRegion(cost);
      }
    }
    //    (costSavedIntermediateCache > 0)
    //        ? (costSavedIntermediateCache =
    //               (costSavedIntermediateCache -
    //               sm->getStorageWriteDelay()))
    //        : (costSavedIntermediateCache = 0);
  } else if (mode == PRIMARY) {
    double cost = static_cast<double>(sm->getStorageReadDelay());
    if (flagNumberForIntermediatePages > 0) {
      costOfRegion = previousCostOfRegion;
    } else {
      decreaseCostIntermediateCache(cost * localCost);
      decreaseCostOfRegion(cost * localCost);
    }

    //    (costSavedIntermediateCache > 0)
    //        ? (costSavedIntermediateCache =
    //               costSavedIntermediateCache - sm->getStorageReadDelay())
    //        : (costSavedIntermediateCache = 0);
  }
  //  if (counterOfPrints % 10000 == 0) {
  //    std::cout << "%%%%%%%% costOfIntermediateDirtyPages: "
  //              << costSavedIntermediateCache << std::endl;
  //    std::cout << "cost of REGION: " << costOfRegion << std::endl;
  //  }
}

/*
 * ######################################################
 *  Functions for getting pages from different indexes
 * ######################################################
 */

std::pair<char *, bool> BufferPool::getAFreePage(FileMode mode) {
  MemPageIndex *freeIndex1, *freeIndex2;
  (void)mode;

  freeIndex1 = &free_page_index;
  freeIndex2 = &free_intermediate_page_index;

  mpi_by_time::const_iterator primaryIt;

  if (freeIndex1->size() != 0) {
    //    std::lock_guard<std::mutex> lock(freePgIndexMutex);
    primaryIt = freeIndex1->get<Timestamp>().begin();
    MemPage &pg = page(primaryIt);
    char *freepage = pg.getData();
    freeIndex1->erase(pg.getFilepos());
    delete &pg;
    return std::make_pair(freepage, true);
  }

  if (freeIndex2->size() != 0) {
    //    std::lock_guard<std::mutex> lock(freeIntermediateIndexMutex);
    primaryIt = freeIndex2->get<Timestamp>().begin();
    MemPage &pg = page(primaryIt);
    char *freepage = pg.getData();
    freeIndex2->erase(pg.getFilepos());
    delete &pg;
    return std::make_pair(freepage, true);
  }
  return std::make_pair(nullptr, false);
}

std::pair<char *, bool> BufferPool::getIntermediateBasicPage() {
  std::pair<char *, bool> freeIntermediatePageTuple =
      getIntermediatePage(PERMANENT);

  if (freeIntermediatePageTuple.second) return freeIntermediatePageTuple;

  return getIntermediatePage(TEMPORAL);
}

std::pair<char *, bool> BufferPool::getCleanPrimaryPage() {
  MemPageIndex *tempIndex = &pg_index;
  std::pair<bool, mpi_by_dirtiness::const_iterator> pair =
      findCandidate(tempIndex, false);

  if (!pair.first) {
    pair = findCandidate(tempIndex, true);
  }

  if (pair.first) {
    MemPage &pg = page(pair.second);
    char *freepage = pg.getData();
    if (pg.isDirty()) sm->writePage(pg, PRIMARY);
    tempIndex->erase(pg.getFilepos());
    delete &pg;
    return std::make_pair(freepage, true);
  } else {
    return std::make_pair(nullptr, false);
  }
}

std::pair<char *, bool> BufferPool::getCleanPrimaryPageWithFlag(bool isDirty) {
  MemPageIndex *tempIndex = &pg_index;
  std::pair<bool, mpi_by_dirtiness::const_iterator> pair =
      findCandidate(tempIndex, isDirty);

  if (pair.first) {
    MemPage &pg = page(pair.second);
    char *freepage = pg.getData();
    if (pg.isDirty()) sm->writePage(pg, PRIMARY);
    tempIndex->erase(pg.getFilepos());
    delete &pg;
    return std::make_pair(freepage, true);
  } else {
    return std::make_pair(nullptr, false);
  }
}

std::pair<char *, bool> BufferPool::getPageFromIndex(MemPageIndex *index) {
  // COUT<< "Get Primary page" << "\n";
  MemPageIndex *tempIndex = index;

  mpi_by_time::const_iterator primaryIt = findCandidate(
      tempIndex->get<Timestamp>().begin(), tempIndex->get<Timestamp>().end());
  if (primaryIt != tempIndex->get<Timestamp>().end()) {
    MemPage &pg = page(primaryIt);
    char *freepage = pg.getData();
    //    bool isDirty = pg.isDirty();
    if (pg.isDirty()) {
      sm->writePage(pg, PRIMARY);
    }
    tempIndex->erase(pg.getFilepos());
    delete &pg;
    return std::make_pair(freepage, true);
  } else {
    return std::make_pair(nullptr, false);
  }
}

std::pair<char *, bool> BufferPool::getCleanPageOrDirtyFromIndex(
    MemPageIndex *index) {
  MemPageIndex *tempIndex = index;
  std::pair<bool, mpi_by_dirtiness::const_iterator> pair =
      findCandidate(tempIndex, false);

  if (!pair.first) {
    pair = findCandidate(tempIndex, true);
  }
  if (pair.first) {
    MemPage &pg = page(pair.second);
    char *freepage = pg.getData();
    if (pg.isDirty()) {
      sm->writePage(pg, PRIMARY);
    }
    tempIndex->erase(pg.getFilepos());
    delete &pg;
    return std::make_pair(freepage, true);
  } else {
    return std::make_pair(nullptr, false);
  }
}

std::pair<char *, bool> BufferPool::getCleanIntermediateBasicPage() {
  std::pair<char *, bool> freeIntermediatePageTuple =
      getCleanIntermediatePage(PERMANENT);

  if (freeIntermediatePageTuple.second) return freeIntermediatePageTuple;

  return getCleanIntermediatePage(TEMPORAL);
}

std::pair<char *, bool> BufferPool::getCleanIntermediatePage(

    FILE_CLASSIFIER fileClassifier) {
  // COUT<< "Get Intermediate page" << "\n";

  //########################### GET FROM INTERMEDIATE
  //#############################################

  table_key leastSignificantTk;

  boost::ptr_map<table_key, MemPageIndex>::iterator it1;

  //  double intermediateCacheSize = intermediate_pg_index.size()
  //
  //  + free_intermediate_page_index.size();

  //    if (counter == sm->futureOrderedQueries.size()) {

  std::vector<scoreTuple> *tmpScores;

  if (fileClassifier == PERMANENT) {
    tmpScores = &permanentScores;

  } else if (fileClassifier == TEMPORAL) {
    tmpScores = &temporalScores;

  } else {
    return std::make_pair(nullptr, false);
  }

  for (auto it = tmpScores->begin(); it != tmpScores->end(); ++it) {
    //    double idealFileSizeInIndex = ceil(
    //
    //    (it->fileSize / (sm->totalFileSizes))
    //
    //    * (double) (intermediateCacheSize));

    leastSignificantTk = it->tk;

    // COUT << "leastSignificantTk: " << leastSignificantTk << "\n";

    it1 = indexMapTkIndex.find(leastSignificantTk);

    if (it1 == indexMapTkIndex.end()) continue;

    // COUT << "Size: " << it1->second->size() << "\n";

    //    if (it1->second->size() == 0)

    double currentSizeInIndex = it1->second->size();

    //    if (currentSizeInIndex == 0

    //        || currentSizeInIndex < idealFileSizeInIndex - 1)

    if (currentSizeInIndex == 0) continue;

    MemPageIndex *index = it1->second;

    std::pair<bool, mpi_by_dirtiness::const_iterator> pair = findCandidate(

        index, false);

    if (!pair.first) {
      continue;

      //      pair = findCandidate(index, true);
    }

    if (pair.first) {
      MemPage &pg = page(pair.second);

      char *freepage = pg.getData();

      if (pg.isDirty()) {
        sm->writePage(pg, INTERMEDIATE);
      }

      intermediate_pg_index.erase(pg.getFilepos());

      index->erase(pg.getFilepos());

      delete &pg;

      return std::make_pair(freepage, true);
    }
  }

  for (auto it = tmpScores->begin(); it != tmpScores->end(); ++it) {
    //    double idealFileSizeInIndex = ceil(
    //
    //    (it->fileSize / (sm->totalFileSizes))
    //
    //    * (double) (intermediateCacheSize));

    leastSignificantTk = it->tk;

    // COUT << "leastSignificantTk: " << leastSignificantTk << "\n";

    it1 = indexMapTkIndex.find(leastSignificantTk);

    if (it1 == indexMapTkIndex.end()) continue;

    // COUT << "Size: " << it1->second->size() << "\n";

    //    if (it1->second->size() == 0)

    double currentSizeInIndex = it1->second->size();

    //    if (currentSizeInIndex == 0

    //        || currentSizeInIndex < idealFileSizeInIndex - 1)

    if (currentSizeInIndex == 0) continue;

    MemPageIndex *index = it1->second;

    std::pair<bool, mpi_by_dirtiness::const_iterator> pair = findCandidate(

        index, true);

    if (!pair.first) {
      continue;

      //      pair = findCandidate(index, true);
    }

    if (pair.first) {
      MemPage &pg = page(pair.second);

      char *freepage = pg.getData();

      if (pg.isDirty()) {
        sm->writePage(pg, INTERMEDIATE);
      }

      intermediate_pg_index.erase(pg.getFilepos());

      index->erase(pg.getFilepos());

      delete &pg;

      return std::make_pair(freepage, true);
    }
  }

  return std::make_pair(nullptr, false);
}

std::pair<char *, bool> BufferPool::getCleanIntermediatePageWithFlag(

    FILE_CLASSIFIER fileClassifier, bool isDirty) {
  // COUT<< "Get Intermediate page" << "\n";

  //########################### GET FROM getCleanIntermediatePage INTERMEDIATE
  //#############################################
  table_key leastSignificantTk;

  boost::ptr_map<table_key, MemPageIndex>::iterator it1;

  std::vector<scoreTuple> *tmpScores;

  if (fileClassifier == PERMANENT) {
    tmpScores = &permanentScores;

  } else if (fileClassifier == TEMPORAL) {
    tmpScores = &temporalScores;

  } else {
    return std::make_pair(nullptr, false);
  }

  for (auto it = tmpScores->begin(); it != tmpScores->end(); ++it) {
    leastSignificantTk = it->tk;

    it1 = indexMapTkIndex.find(leastSignificantTk);

    if (it1 == indexMapTkIndex.end()) continue;

    double currentSizeInIndex = it1->second->size();

    if (currentSizeInIndex == 0) continue;

    MemPageIndex *index = it1->second;

    std::pair<bool, mpi_by_dirtiness::const_iterator> pair =
        findCandidate(index, isDirty);
    if (!pair.first) {
      continue;
    }

    if (pair.first) {
      MemPage &pg = page(pair.second);

      char *freepage = pg.getData();

      if (pg.isDirty()) {
        sm->writePage(pg, INTERMEDIATE);
      }

      intermediate_pg_index.erase(pg.getFilepos());

      index->erase(pg.getFilepos());

      delete &pg;

      return std::make_pair(freepage, true);
    }
  }

  return std::make_pair(nullptr, false);
}

// Utility functions for calculating the exact number of dirty pages in the
// bufferpool

void BufferPool::printDirtyPagesForAllDataStructures() {
#ifdef MULTI_THREADED
  std::lock_guard<std::mutex> lock(sm->bufferManagerOperationMutex);
#endif
  printAllDirtyPagesDSTreeMap();
  return;

  std::ofstream dirtyStatisticsFile;

  std::stringstream ss;
  ss << "../visualisation/dirtyStatistics" << uniqueDirtyId << ".txt";
  uniqueDirtyId++;

  dirtyStatisticsFile.open(ss.str(), ios::out);

  size_t primarySize = pg_index.size();
  dirtyStatisticsFile << "primary"
                      << "," << primarySize << std::endl;
  size_t intermediateSize = intermediate_pg_index.size();
  dirtyStatisticsFile << "intermediate"
                      << "," << intermediateSize << std::endl;
  size_t auxiliarySize = auxiliary_pg_index.size();
  dirtyStatisticsFile << "auxiliary"
                      << "," << auxiliarySize << std::endl;
  size_t freeSize =
      free_page_index.size() + free_intermediate_page_index.size();
  dirtyStatisticsFile << "free"
                      << "," << freeSize << std::endl;

  dirtyStatisticsFile.close();
}
/**
 * Calculates statistics for clean and dirty pages for each category and exports
 * in .json files.
 * The goal is to generate a nice tree-map graph with the current state of the
 * buffer-pool
 */
void BufferPool::printAllDirtyPagesDSTreeMap() {
  std::ofstream dirtyStatisticsFile;

  std::stringstream ss;
  ss << "../visualisation/pattern/stats" << uniqueDirtyId << ".json";
  uniqueDirtyId++;

  dirtyStatisticsFile.open(ss.str(), ios::out);

  std::pair<std::string, std::string> psPrimary =
      printPagesPerDataStructure(&pg_index, "primary");
  std::pair<std::string, std::string> psIntermediate =
      printPagesPerDataStructure(&intermediate_pg_index, "intermediate");
  std::pair<std::string, std::string> psAuxiliary =
      printPagesPerDataStructure(&auxiliary_pg_index, "auxiliary");
  std::pair<std::string, std::string> psFree =
      printPagesPerDataStructure(&free_page_index, "free");
  std::pair<std::string, std::string> psFreeIntermediate =
      printPagesPerDataStructure(&free_intermediate_page_index,
                                 "free_intermediate");

  dirtyStatisticsFile << printParentHeader();
  dirtyStatisticsFile << printParent("Primary", psPrimary.first,
                                     psPrimary.second);

  if (getAlgorithm() == 2) {
    dirtyStatisticsFile << printParent("Intermediate", psIntermediate.first,
                                       psIntermediate.second);

  } else if (getAlgorithm() == 3) {
    std::cout << "CLEANTEST2 MODE " << std::endl;
    std::pair<std::string, std::string> psIntermediate1 =
        printPagesPerDataStructure(&internal_intermediate_index,
                                   "intermediate_internal");
    dirtyStatisticsFile << printParent(
        "Internal_Primary", psIntermediate1.first, psIntermediate1.second);

    std::cout << "MIKE: " << psIntermediate1.first << " "
              << psIntermediate1.second << std::endl;
    std::cout << "internal_intermediate_index size: "
              << internal_intermediate_index.size() << std::endl;
  }
  //  else {
  //#ifdef CLEANTEST
  //  dirtyStatisticsFile << printParent("Intermediate", psIntermediate.first,
  //                                     psIntermediate.second);
  //#else
  //
  //#ifdef CLEANTEST2
  //  std::cout << "CLEANTEST2 MODE " << std::endl;
  //  std::pair<std::string, std::string> psIntermediate1 =
  //      printPagesPerDataStructure(&internal_intermediate_index,
  //                                 "intermediate_internal");
  //  dirtyStatisticsFile << printParent("Internal_Primary",
  //  psIntermediate1.first,
  //                                     psIntermediate1.second);
  //
  //  std::cout << "MIKE: " << psIntermediate1.first << " "
  //            << psIntermediate1.second << std::endl;
  //  std::cout << "internal_intermediate_index size: "
  //            << internal_intermediate_index.size() << std::endl;
  //
  //#endif

  if (getAlgorithm() == 3) {
    std::pair<std::string, std::string> psPermanent =
        printPagesPerFileClassifier(PERMANENT);
    std::pair<std::string, std::string> psTemporal =
        printPagesPerFileClassifier(TEMPORAL);

    dirtyStatisticsFile << "{" << std::endl;
    dirtyStatisticsFile << " \"name\" : \"Intermediate\", " << std::endl;
    dirtyStatisticsFile << " \"children\" : [" << std::endl;

    dirtyStatisticsFile << printParent("Permanent", psPermanent.first,
                                       psPermanent.second);
    dirtyStatisticsFile << printParentLast("Temporal", psTemporal.first,
                                           psTemporal.second);

    dirtyStatisticsFile << "]" << std::endl;
    dirtyStatisticsFile << "}" << std::endl;
    dirtyStatisticsFile << "," << std::endl;
  }
  //  }
  //#endif

  //  dirtyStatisticsFile << printParent("Temporal", psTemporal.first,
  //  psTemporal.second);
  dirtyStatisticsFile << printParent("Auxiliary", psAuxiliary.first,
                                     psAuxiliary.second);
  dirtyStatisticsFile << printParent("Free", psFree.first, psFree.second);
  dirtyStatisticsFile << printParentLast(
      "FreeIntermediate", psFreeIntermediate.first, psFreeIntermediate.second);
  dirtyStatisticsFile << printFooter();

  size_t d_p = dirtyPagesForIndex(&pg_index);
  size_t d_internal_p = dirtyPagesForIndex(&internal_intermediate_index);
  size_t d_i = dirtyPagesForIndex(&intermediate_pg_index);
  size_t d_a = dirtyPagesForIndex(&auxiliary_pg_index);
  size_t d_f = dirtyPagesForIndex(&free_page_index);
  size_t d_f2 = dirtyPagesForIndex(&free_intermediate_page_index);

  size_t totalDirtyPages = 0;

  //#ifdef CLEANTEST
  //  totalDirtyPages = d_p + d_i;
  //#elif CLEANTEST2
  //  totalDirtyPages = d_p + d_internal_p + d_i + d_a;
  //#elif LRUTEST
  //  totalDirtyPages = d_p;
  //#else
  //  totalDirtyPages = d_p + d_i + d_a;
  //#endif

  if (getAlgorithm() == 2) {
    totalDirtyPages = d_p + d_i;
  } else if (getAlgorithm() == 3) {
    totalDirtyPages = d_p + d_internal_p + d_i + d_a;
  } else if (getAlgorithm() == 1 || getAlgorithm() == 4) {
    totalDirtyPages = d_p;
  } else {
    totalDirtyPages = d_p + d_i + d_a;
  }

  //  size_t freePages = d_f + d_f2;
  //  size_t totalDirtyPagesPermanent = dirtyPagesForCategory(PERMANENT);
  //  size_t totalDirtyPagesTemporal = dirtyPagesForCategory(TEMPORAL);

  //  std::cout << "printDirtyPagesForAllDataStructuresTreeMap: d_i: " << d_i
  //            << std::endl;
  //  std::cout << "printDirtyPagesForAllDataStructuresTreeMap: "
  //               "getTotalNumberOfDirtyIntermediatePages : "
  //            << getTotalNumberOfDirtyIntermediatePages() << std::endl;
  //  std::cout
  //      << "printDirtyPagesAllDataStructuresTreeMap: totalDirtyPages  (sum) :
  //      "
  //      << totalDirtyPages << std::endl;
  //  std::cout << "DSTreeMap: totalFreeDirtyPages(sum)  : " << freePages
  //            << std::endl;
  //  std::cout << "DSTreeMap: totalDirtyPagesPermanent : "
  //            << totalDirtyPagesPermanent << std::endl;
  //  std::cout << "DSTreeMap: totalDirtyPagesTemporal : "
  //            << totalDirtyPagesTemporal << std::endl;
  //  std::cout << "DSTreeMap: totalPagesAll " << dirtyPagesAll() << std::endl;

  dirtyStatisticsFile.close();
}

size_t BufferPool::dirtyPagesAll() {
  //
  //#ifdef CLEANTEST
  //  return dirtyPagesForIndex(&pg_index) +
  //         dirtyPagesForIndex(&intermediate_pg_index);
  //#elif CLEANTEST2
  //  return dirtyPagesForIndex(&pg_index) +
  //         dirtyPagesForIndex(&internal_intermediate_index) +
  //         dirtyPagesForIndex(&intermediate_pg_index) +
  //         dirtyPagesForIndex(&auxiliary_pg_index);
  //#elif LRUTEST
  //  return dirtyPagesForIndex(&pg_index);
  //#else
  //  return dirtyPagesForIndex(&pg_index) +
  //         dirtyPagesForIndex(&intermediate_pg_index) +
  //         dirtyPagesForIndex(&auxiliary_pg_index);
  //#endif
  //

  if (getAlgorithm() == 2) {
    return dirtyPagesForIndex(&pg_index) +
           dirtyPagesForIndex(&intermediate_pg_index);
  } else if (getAlgorithm() == 3) {
    return dirtyPagesForIndex(&pg_index) +
           dirtyPagesForIndex(&internal_intermediate_index) +
           dirtyPagesForIndex(&intermediate_pg_index) +
           dirtyPagesForIndex(&auxiliary_pg_index);
  } else if (getAlgorithm() == 1 || getAlgorithm() == 4) {
    return dirtyPagesForIndex(&pg_index);
  } else {
    return dirtyPagesForIndex(&pg_index) +
           dirtyPagesForIndex(&intermediate_pg_index) +
           dirtyPagesForIndex(&auxiliary_pg_index);
  }

  return 0;
}

size_t BufferPool::dirtyPagesForIndex(MemPageIndex *index) {
  return index->get<Dirtiness>().count(true);
}
size_t BufferPool::dirtyPagesForIndex(MemPageIndex *index,
                                      const std::string &fileName) {
  size_t counter = 0;
  for (auto it = index->get<Timestamp>().begin();
       it != index->get<Timestamp>().end(); ++it) {
    if (page(it).isDirty() && page(it).getFilepos().filename == fileName) {
      counter++;
    }
  }
  return counter;
}

size_t BufferPool::dirtyPagesIntermediate() {
  //#ifdef CLEANTEST
  //  return dirtyPagesForCategoryLruOrClean(deceve::storage::TEMPORAL) +
  //         dirtyPagesForCategoryLruOrClean(deceve::storage::PERMANENT);
  //#elif LRUTEST
  //  return dirtyPagesForCategoryLruOrClean(deceve::storage::TEMPORAL) +
  //         dirtyPagesForCategoryLruOrClean(deceve::storage::PERMANENT);
  //#else
  //  return dirtyPagesForIndex(&intermediate_pg_index);
  //#endif

  if (getAlgorithm() == 2) {
    return dirtyPagesForCategoryLruOrClean(deceve::storage::TEMPORAL) +
           dirtyPagesForCategoryLruOrClean(deceve::storage::PERMANENT);
  } else if (getAlgorithm() == 1 || getAlgorithm() == 4) {
    return dirtyPagesForCategoryLruOrClean(deceve::storage::TEMPORAL) +
           dirtyPagesForCategoryLruOrClean(deceve::storage::PERMANENT);
  } else {
    return dirtyPagesForIndex(&intermediate_pg_index);
  }
}
size_t BufferPool::dirtyPagesForCategory(FILE_CLASSIFIER fileClassifier) {
  //#ifdef CLEANTEST
  //  return dirtyPagesForCategoryLruOrClean(fileClassifier);
  //#elif LRUTEST
  //  return dirtyPagesForCategoryLruOrClean(fileClassifier);
  //#else
  //  return dirtyPagesForCategoryRanking(fileClassifier);
  //#endif

  if (getAlgorithm() == 2) {
    return dirtyPagesForCategoryLruOrClean(fileClassifier);
  } else if (getAlgorithm() == 1 || getAlgorithm() == 4) {
    return dirtyPagesForCategoryLruOrClean(fileClassifier);
  } else {
    return dirtyPagesForCategoryRanking(fileClassifier);
  }
}

size_t BufferPool::dirtyPagesForCategoryLruOrClean(
    FILE_CLASSIFIER fileClassifier) {
  std::vector<scoreTuple> *tmpScores;
  if (fileClassifier == PERMANENT) {
    tmpScores = &permanentScores;
  } else if (fileClassifier == TEMPORAL) {
    tmpScores = &temporalScores;
  } else {
    return 0;
  }

  size_t totalSum = 0;
  for (auto dsIt = tmpScores->begin(); dsIt != tmpScores->end(); ++dsIt) {
    totalSum += dirtyPagesForDS(dsIt->tk);
  }

  return totalSum;
}

size_t BufferPool::dirtyPagesForCategoryRanking(
    FILE_CLASSIFIER fileClassifier) {
  std::vector<scoreTuple> *tmpScores;
  if (fileClassifier == PERMANENT) {
    tmpScores = &permanentScores;
  } else if (fileClassifier == TEMPORAL) {
    tmpScores = &temporalScores;
  } else {
    return 0;
  }

  size_t totalSum = 0;
  boost::ptr_map<table_key, MemPageIndex>::iterator indexIterator;
  table_key tmpKey;
  for (auto dsIt = tmpScores->begin(); dsIt != tmpScores->end(); ++dsIt) {
    tmpKey = dsIt->tk;
    indexIterator = indexMapTkIndex.find(tmpKey);
    if (indexIterator == indexMapTkIndex.end()) {
      continue;
    }
    double currentSizeInIndex = indexIterator->second->size();
    if (currentSizeInIndex == 0) {
      continue;
    }
    MemPageIndex *index = indexIterator->second;
    totalSum += dirtyPagesForIndex(index);

    std::cout << "totalSum: " << totalSum << std::endl;
  }

  return totalSum;
}

size_t BufferPool::dirtyPagesForDS(const table_key &tk) {
  auto it = sm->getIterOfPersistedFileCatalog(tk);
  if (it == sm->getIteratorPersistedCatalogEnd()) {
    return 0;
  }
  if (tk.table_name == NATION || tk.table_name == REGION ||
      tk.table_name == SUPPLIER || tk.table_name == OTHER_TABLENAME ||
      tk.field == OTHER_FIELD) {
    return 0;
  }

  // remove sorted version of the file
  if (tk.version == SORT) {
    std::string filename = it->second.full_output_path;
    return dirtyPagesForFile(INTERMEDIATE, filename);
  }

  if (tk.version == HASHJOIN) {
    size_t sum = 0;
    std::unordered_set<std::string> filenames;
    for (size_t i = 0; i < it->second.num_files; i++) {
      std::stringstream filename;
      filename << it->second.output_name << it->second.full_output_path
               << "_persistent." << i;
      filenames.insert(filename.str());
    }
    if (filenames.size() != 0) {
      sum = dirtyPagesForMultipleFiles(INTERMEDIATE, filenames);
    }
    filenames.erase(filenames.begin(), filenames.end());
    return sum;
  }
  return 0;
}

size_t BufferPool::dirtyPagesForMultipleFiles(
    const FileMode &mode, const std::unordered_set<std::string> &filenames) {
  std::lock_guard<std::mutex> flocker(sm->dirtyLocker);
  size_t total = 0;
  for (const auto &it : filenames) {
    size_t amount = sm->getNumberOfDirtyPagesFromSM(it, mode);
    total += amount;
  }
  return total;
}

size_t BufferPool::dirtyPagesForFile(const FileMode &mode,
                                     const std::string &fn) {
  //#ifdef CLEANTEST
  //  (void)mode;
  //  size_t counter = 0;
  //  counter += dirtyPagesForIndex(&pg_index, fn);
  //  counter += dirtyPagesForIndex(&intermediate_pg_index, fn);
  //  return counter;
  //#elif CLEANTEST2
  //  size_t counter = 0;
  //  if (mode == sto::PRIMARY) {
  //    counter += dirtyPagesForIndex(&pg_index, fn);
  //    counter += dirtyPagesForIndex(&internal_intermediate_index, fn);
  //    return counter;
  //  } else if (mode == sto::INTERMEDIATE) {
  //    return dirtyPagesForIndex(&intermediate_pg_index, fn);
  //  } else if (mode == sto::AUXILIARY) {
  //    return dirtyPagesForIndex(&auxiliary_pg_index, fn);
  //  }
  //  return counter;
  //
  //#elif LRUTEST
  //  (void)mode;
  //  return dirtyPagesForIndex(&pg_index, fn);
  //#else
  //  return sm->getNumberOfDirtyPagesFromSM(fn, mode);
  //#endif

  if (getAlgorithm() == 2) {
    (void)mode;
    size_t counter = 0;
    counter += dirtyPagesForIndex(&pg_index, fn);
    counter += dirtyPagesForIndex(&intermediate_pg_index, fn);
    return counter;
  } else if (getAlgorithm() == 3) {
    size_t counter = 0;
    if (mode == sto::PRIMARY) {
      counter += dirtyPagesForIndex(&pg_index, fn);
      counter += dirtyPagesForIndex(&internal_intermediate_index, fn);
      return counter;
    } else if (mode == sto::INTERMEDIATE) {
      return dirtyPagesForIndex(&intermediate_pg_index, fn);
    } else if (mode == sto::AUXILIARY) {
      return dirtyPagesForIndex(&auxiliary_pg_index, fn);
    }
    return counter;
  } else if (getAlgorithm() == 1 || getAlgorithm() == 4) {
    (void)mode;
    return dirtyPagesForIndex(&pg_index, fn);
  } else {
    return sm->getNumberOfDirtyPagesFromSM(fn, mode);
  }
}

/**
 *  Function that prints the current state of the bufferpool into a json file
 *  that can be visually represented by a treemap-chart
 */
std::string BufferPool::printParentHeader() {
  std::stringstream ss;
  ss << "{" << std::endl;
  ss << "\"name\": \"bufferpool\"," << std::endl;
  ss << "\"children\": [" << std::endl;
  ss << "{" << std::endl;
  ss << "\"name\": \"analytics\"," << std::endl;
  ss << "\"children\": [" << std::endl;

  return ss.str();
}

std::string BufferPool::printParent(const std::string &name,
                                    const std::string &kid1,
                                    const std::string &kid2) {
  return printParentLast(name, kid1, kid2) + ",";
}

std::string BufferPool::printParentLast(const std::string &name,
                                        const std::string &kid1,
                                        const std::string &kid2) {
  std::stringstream ss;
  ss << "{" << std::endl;
  ss << "\"name\" : \"" << name << "\"," << std::endl;
  ss << "\"children\": [" << std::endl;
  ss << "{\"name\" : \"" << name << "Dirty\", \"children\": [ " << kid1
     << "] }," << std::endl;
  ss << "{\"name\" : \"" << name << "Clean\", \"children\": [ " << kid2 << "]}"
     << std::endl;
  ss << "]" << std::endl;
  ss << "}";
  return ss.str();
}

std::string BufferPool::printKid(const std::string &name, int numOfDirty,
                                 int numOfClean) {
  int dirtyNum = numOfDirty < 10 ? 0 : numOfDirty;
  int cleanNum = numOfClean < 10 ? 0 : numOfClean;
  std::stringstream ss;
  ss << "{" << std::endl;
  ss << "\"name\" : \"" << name << "\"," << std::endl;
  ss << "\"children\": [" << std::endl;
  ss << "{\"name\" : \"" << name << "_Dirty\", \"size\": " << dirtyNum << "},"
     << std::endl;
  ss << "{\"name\" : \"" << name << "_Clean\", \"size\": " << cleanNum << "}"
     << std::endl;
  ss << "]" << std::endl;
  ss << "},";
  return ss.str();
}

std::string BufferPool::printLastKid(const std::string &name, int numOfDirty,
                                     int numOfClean) {
  int dirtyNum = numOfDirty < 10 ? 0 : numOfDirty;
  int cleanNum = numOfClean < 10 ? 0 : numOfClean;
  std::stringstream ss;
  ss << "{" << std::endl;
  ss << "\"name\" : \"" << name << "\"," << std::endl;
  ss << "\"children\": [" << std::endl;
  ss << "{\"name\" : \"" << name << "Dirty\", \"size\": " << dirtyNum << "},"
     << std::endl;
  ss << "{\"name\" : \"" << name << "Clean\", \"size\": " << cleanNum << "}"
     << std::endl;
  ss << "]" << std::endl;
  ss << "}";
  return ss.str();
}

std::string BufferPool::printUniqueKidLast(const std::string &filename,
                                           int count,
                                           const std::string &dirtiness) {
  int numOfItems = count < 10 ? 0 : count;
  std::stringstream ss;

  ss << "{\"name\" : \"" << filename << "_" << dirtiness
     << "\", \"size\": " << numOfItems << "}";

  return ss.str();
}

std::string BufferPool::printUniqueKid(const std::string &filename, int count,
                                       const std::string &dirtiness) {
  return printUniqueKidLast(filename, count, dirtiness) + ", \n";
}

std::string BufferPool::printFooter() {
  std::stringstream ss;
  ss << "]" << std::endl;
  ss << "}" << std::endl;
  ss << "]" << std::endl;
  ss << "}" << std::endl;
  return ss.str();
}

std::string BufferPool::splitFilename(const std::string &str) {
  std::size_t found = str.find_last_of(".");
  return str.substr(0, found);
}

std::pair<std::string, std::string> BufferPool::printPagesPerDataStructure(
    MemPageIndex *index, const std::string &mode) {
  std::map<std::string, std::pair<int, int> > primaryStats;

  for (auto primaryIter = index->get<Timestamp>().begin();
       primaryIter != index->get<Timestamp>().end(); ++primaryIter) {
    MemPage &pg = page(primaryIter);
    std::string fileNamePrefix = splitFilename(pg.getFilename());
    std::map<std::string, std::pair<int, int> >::iterator statsIter =
        primaryStats.find(fileNamePrefix);
    if (statsIter == primaryStats.end()) {
      if (pg.isDirty()) {
        primaryStats.insert(
            std::make_pair(fileNamePrefix, std::make_pair(1, 0)));
      } else {
        primaryStats.insert(
            std::make_pair(fileNamePrefix, std::make_pair(0, 1)));
      }
    } else {
      if (pg.isDirty()) {
        int dirtyCounter = statsIter->second.first + 1;
        std::pair<int, int> p =
            std::make_pair(dirtyCounter, statsIter->second.second);
        primaryStats[fileNamePrefix] = p;
      } else {
        int cleanCounter = statsIter->second.second + 1;
        std::pair<int, int> p =
            std::make_pair(statsIter->second.first, cleanCounter);
        primaryStats[fileNamePrefix] = p;
      }
    }
  }

  for (auto s : primaryStats) {
    std::cout << mode << "-00000000- " << s.first << " " << s.second.first
              << " " << s.second.second << std::endl;
  }

  std::map<std::string, int> dirtyMap;
  std::map<std::string, int> cleanMap;

  for (auto it = primaryStats.begin(); it != primaryStats.end(); ++it) {
    if (it->second.first > 10) {
      dirtyMap.insert(std::make_pair(it->first, it->second.first));
    }
    if (it->second.second > 10) {
      cleanMap.insert(std::make_pair(it->first, it->second.second));
    }
  }

  std::stringstream ssDirty;
  std::stringstream ssClean;
  int lastDirtyIndex = dirtyMap.size() - 1;
  int lastCleanIndex = cleanMap.size() - 1;

  int counter = 0;
  for (auto it = dirtyMap.begin(); it != dirtyMap.end(); ++it) {
    if (counter == lastDirtyIndex) {
      ssDirty << printUniqueKidLast(it->first, it->second, "Dirty");
    } else {
      ssDirty << printUniqueKid(it->first, it->second, "Dirty");
    }
    counter++;
  }

  counter = 0;
  for (auto it = cleanMap.begin(); it != cleanMap.end(); ++it) {
    if (counter == lastCleanIndex) {
      ssClean << printUniqueKidLast(it->first, it->second, "Clean");
    } else {
      ssClean << printUniqueKid(it->first, it->second, "Clean");
    }
    counter++;
  }

  std::cout << ssDirty.str();
  std::cout << ssClean.str();

  return std::make_pair(ssDirty.str(), ssClean.str());
}

std::pair<std::string, std::string> BufferPool::printPagesPerFileClassifier(
    FILE_CLASSIFIER fileClassifier) {
  std::vector<scoreTuple> *tmpScores;
  std::string fileClassifierString = "none";
  if (fileClassifier == PERMANENT) {
    tmpScores = &permanentScores;
    fileClassifierString = "PERM_";
  } else if (fileClassifier == TEMPORAL) {
    tmpScores = &temporalScores;
    fileClassifierString = "TEMP_";
  } else {
    return std::make_pair("0", "0");
  }

  boost::ptr_map<table_key, MemPageIndex>::iterator indexIterator;
  table_key tmpKey;

  std::map<std::string, int> dirtyMap;
  std::map<std::string, int> cleanMap;

  for (auto dsIt = tmpScores->begin(); dsIt != tmpScores->end(); ++dsIt) {
    tmpKey = dsIt->tk;
    std::cout << "tmpKey: " << tmpKey << std::endl;
    indexIterator = indexMapTkIndex.find(tmpKey);
    if (indexIterator == indexMapTkIndex.end()) {
      continue;
    }
    double currentSizeInIndex = indexIterator->second->size();
    if (currentSizeInIndex == 0) {
      continue;
    }
    MemPageIndex *index = indexIterator->second;
    size_t tmpDirtyPages = dirtyPagesForIndex(index);
    size_t tmpCleanPages = index->size() - tmpDirtyPages;

    dirtyMap.insert(std::make_pair(
        fileClassifierString + tmpKey.printFriendly(), tmpDirtyPages));
    cleanMap.insert(std::make_pair(
        fileClassifierString + tmpKey.printFriendly(), tmpCleanPages));
  }

  std::stringstream ssDirty;
  std::stringstream ssClean;
  int lastDirtyIndex = dirtyMap.size() - 1;
  int lastCleanIndex = cleanMap.size() - 1;

  int counter = 0;
  for (auto it = dirtyMap.begin(); it != dirtyMap.end(); ++it) {
    if (counter == lastDirtyIndex) {
      ssDirty << printUniqueKidLast(it->first, it->second, "Dirty");
    } else {
      ssDirty << printUniqueKid(it->first, it->second, "Dirty");
    }
    counter++;
  }

  counter = 0;
  for (auto it = cleanMap.begin(); it != cleanMap.end(); ++it) {
    if (counter == lastCleanIndex) {
      ssClean << printUniqueKidLast(it->first, it->second, "Clean");
    } else {
      ssClean << printUniqueKid(it->first, it->second, "Clean");
    }
    counter++;
  }

  std::cout << "Here: " << ssDirty.str() << std::endl;
  std::cout << "Here: " << ssClean.str() << std::endl;

  return std::make_pair(ssDirty.str(), ssClean.str());
}

}  // namespace  storage
}  // namespace  deceve
