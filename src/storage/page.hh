#ifndef __PAGE_HH__
#define __PAGE_HH__

#include "../utils/defs.hh"
#include "../utils/types.hh"
#include "../utils/require.hh"
#include <boost/operators.hpp>
#include <iostream>

namespace deceve { namespace bama {

/*
template <typename K, typename P>
struct Record: public boost::totally_ordered<Record<K, P> > {
    K key;
    P payload;

    bool operator==(const Record<K, P>& r) const { return key == r.key; }
    bool operator<(const Record<K, P>& r) const { return key < r.key; }
    friend std::ostream& operator<<(std::ostream& os,
                                    const Record<K, P>& r) {
        os << "<" << r.key << ", " << r.payload << ">";
          return os;
    }		
};
*/

// we need this since alingment of records in pages is necessary and this is
// done through unions, which imply the use of types with parameter-less
// constructor, so we can't use std::pair
/*
template <typename P1, typename P2>
struct CombinedRecord {
    P1 first;
    P2 second;
};

template <typename K, typename P>
Record<K, P> makeRecord(const K& k, const P& p) {
    Record<K, P> rec;
    rec.key = k;
    rec.payload = p;
    return rec;
}

template <typename P1, typename P2>
CombinedRecord<P1, P2> makeCombinedRecord(const P1& p1, const P2& p2) {
    CombinedRecord<P1, P2> cr;
    cr.first = p1;
    cr.second = p2;
    return cr;
}
*/

template <typename R>
class Page {
public:
    //enum { size = get_pagesize() };
    enum { size = deceve::storage::PAGE_SIZE_PERSISTENT };
  //enum { header_size = sizeof(size_t) + sizeof(void*) };
    enum { header_size = sizeof(size_t) };
    typedef R record_type;

    Page(): numrecs(0) {}
    ~Page() {}
    
private:
    static const size_t datasize = size - header_size;
    
public:
    static const size_t allocation = datasize/sizeof(record_type);
    
private:
    size_t numrecs;
    union {
        record_type records[allocation];
        char padding[datasize - (datasize % sizeof(void*))];
    } data;

public:
    /*const*/ size_t capacity() const { return allocation; }
    /*const*/ size_t length() const { return numrecs; }    
    const record_type& get(off_t i) const { return data.records[i]; }
    record_type& get(off_t i) { return data.records[i]; }
    void set(off_t i, const record_type& r) { data.records[i] = r; }
    off_t add(const record_type& r) {
      //std::cout << "allocation is " << allocation << "num is " << numrecs << "\n";
        require(numrecs < allocation, "out of space");
        data.records[numrecs++] = r;
        return numrecs;
    }
    void clear() { numrecs = 0; }
    
    //void setNumberOfRecords(size_t n) { numrecs = n; }
    void setAll(const record_type* recs, size_t nr) {
        require(nr <= allocation, "data does not fit");
        ::memmove(&data.records[0], recs, nr*sizeof(record_type));
        numrecs = nr;
    }
    /*
    void addAll(const record_type* recs, size_t nr) {
        require(nr < allocationdata.records
    }
    */
};

} //:~ namespace bama
} //:~ namespace deceve

#endif //:~ __PAGE_HH__
