#ifndef __IO_HH__
#define __IO_HH__

#include "../storage/book.hh"

#ifdef MEMORY
#include "memfile.hh"
#elif VECTOR
#include "vecfile.hh"
#elif RAMDISK
#include "readwriters.hh"
#else
#include "readwriters.hh"
#endif

#endif
