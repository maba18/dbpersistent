/*
 * StatsManager.hh
 *
 *  Created on: 3 Mar 2016
 *      Author: mike
 */

#ifndef STORAGE_STATSMANAGER_HH_
#define STORAGE_STATSMANAGER_HH_

#include <list>
#include <map>
#include <unordered_map>
#include <mutex>

#include "../utils/types.hh"
#include "../utils/defs.hh"
#include "../utils/require.hh"
#include "../utils/helpers.hh"

namespace deceve {

namespace storage {

// This class keeps statistics about previous executions of specific operators
// It is useful for giving an estimate for the cost of the operation
class StatsManager {
  // SECTION FOR WRITE STATISTICS //
 private:
  std::mutex statsMutex;
  long uniqueIdentifier{0};
  std::map<long, long> monitorKeys;
  std::unordered_map<std::string, long> monitorConverter;
  std::map<deceve::storage::table_key, std::list<long> > keyIdsMap;

 public:
  void insertTkInStatsMap(const deceve::storage::table_key& tk) {
    std::lock_guard<std::mutex> guard(statsMutex);
    auto it = keyIdsMap.find(tk);
    if (it == keyIdsMap.end()) {
      std::list<long> v = {};
      keyIdsMap.insert(std::make_pair(tk, v));
    }
  }

  void insertKeyIdInMap(const deceve::storage::table_key& tk, long id) {
    //    std::lock_guard<std::mutex> guard(statsMutex);
    auto it = keyIdsMap.find(tk);
    if (it == keyIdsMap.end()) {
      std::list<long> v = {id};
      keyIdsMap.insert(std::make_pair(tk, v));
    } else {
      it->second.push_back(id);
      if (it->second.size() > 10) {
        it->second.pop_front();
      }
    }
  }

  // check if the operation was calculated before
  bool didTheOperationAppear(const deceve::storage::table_key& tk) {
    std::lock_guard<std::mutex> guard(statsMutex);
    return keyIdsMap.find(tk) != keyIdsMap.end();
  }

  long getAverageWritesForTk(const deceve::storage::table_key& tk) {
    std::lock_guard<std::mutex> guard(statsMutex);

    long averageWrites = 0;
    long counter = 0;
    auto it = keyIdsMap.find(tk);

    // check if key exists in statistics
    if (it == keyIdsMap.end()) {
      return 0;
    }
    // check if list with values is empty
    if (it->second.size() == 0) {
      return 0;
    }

    // iterate over list
    for (auto vit : it->second) {
      counter++;
      auto kit = monitorKeys.find(vit);
      long numWrites = 0;
      if (kit != monitorKeys.end()) {
        numWrites = kit->second;
      }
      averageWrites += numWrites;
    }
    averageWrites = averageWrites / counter;



    return averageWrites;
  }

  // stats for joins

  void addToNameMonitorConverterJoin(std::string filename, std::string prefix,
                                     deceve::storage::table_key join_tk,
                                     size_t number_of_partitions) {
    std::lock_guard<std::mutex> guard(statsMutex);
    insertKeyIdInMap(join_tk, uniqueIdentifier);
    // COUT << "addToNameConverter " << "\n";
    for (unsigned int i = 0; i < number_of_partitions; ++i) {
      std::stringstream s;
      s << filename << prefix << "." << i;
      monitorConverter.insert(std::make_pair(s.str(), uniqueIdentifier));
    }
    uniqueIdentifier++;
  }

  void deleteFromNameMonitorConverterJoin(std::string filename,
                                          std::string prefix,
                                          size_t number_of_partitions) {
    std::lock_guard<std::mutex> guard(statsMutex);

    // COUT << "addToNameConverter " << "\n";
    for (unsigned int i = 0; i < number_of_partitions; ++i) {
      std::stringstream s;
      s << filename << prefix << "." << i;
      monitorConverter.erase(s.str());
    }
  }

  // stats for sort

  long getIdentifierForSort(deceve::storage::table_key join_tk) {
    std::lock_guard<std::mutex> guard(statsMutex);
    insertKeyIdInMap(join_tk, uniqueIdentifier);
    return uniqueIdentifier++;
  }

  void addToNameMonitorConverterSort(std::string filename, long identifier) {
    std::lock_guard<std::mutex> guard(statsMutex);
    monitorConverter.insert(std::make_pair(filename, identifier));
  }

  void deleteFromNameMonitorConverterSort(std::string filename) {
    std::lock_guard<std::mutex> guard(statsMutex);
    monitorConverter.erase(filename);
  }

  // other operations
  void insertInMonitorMap(long id) {
    std::lock_guard<std::mutex> guard(statsMutex);

    auto it = monitorKeys.find(id);
    if (it == monitorKeys.end()) {
      monitorKeys.insert(std::make_pair(id, 0));
    }
  }

  void deleteFromMonitorMap(long id) {
    std::lock_guard<std::mutex> guard(statsMutex);

    auto it = monitorKeys.find(id);

    if (it != monitorKeys.end()) {
      monitorKeys.erase(id);
    }
  }

  void increaseMonitorMap(const std::string& fname) {
    std::lock_guard<std::mutex> guard(statsMutex);

    auto itConverter = monitorConverter.find(fname);
    if (itConverter != monitorConverter.end()) {
      auto writesForDS = monitorKeys.find(itConverter->second);
      if (writesForDS != monitorKeys.end()) {
        int counter = writesForDS->second;
        counter++;
        writesForDS->second = counter;
      } else {
        monitorKeys.insert(std::make_pair(itConverter->second, 1));
      }
    }
  }

  void printMonitorMap() {
//    std::lock_guard<std::mutex> guard(statsMutex);


    DEBUG(monitorKeys);
    DEBUG(monitorConverter);

    std::cout << "printMonitorMap" << std::endl;
    for (auto it : keyIdsMap) {
      statsMutex.lock();
      std::cout << it.first << " : [";
      for (auto vit : it.second) {
        std::cout << " (id: " << vit << " ";
        auto kit = monitorKeys.find(vit);
        if (kit != monitorKeys.end()) {
          std::cout << " value: " << kit->second << ") ";
        }
      }
      statsMutex.unlock();
      std::cout << " ] (avg: " << getAverageWritesForTk(it.first) << ")"
                << std::endl;
    }
  }
};

} /* namespace deceve */
}
#endif /* STORAGE_STATSMANAGER_HH_ */
