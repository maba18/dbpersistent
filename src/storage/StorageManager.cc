/*******************************************************************************
 * Copyright (c) 2015 Michail Basios and Stratis Viglas.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michail Basios - initial API and implementation
 *     Stratis Viglas - initial API and implementation
 *******************************************************************************/

#include <iostream>
#include <fstream>
#include <fcntl.h>
#include "../storage/StorageManager.hh"
#include "../utils/types.hh"
#include "../utils/helpers.hh"
#include "../utils/util.hh"

#include "../utils/global.hh"
#include "../queryplans/definitions.hh"

// determine which platform
#ifdef _WIN32
// not entirely sure about win
#include <direct.h>
#define MAXPATHLEN MAX_PATH
#define getcwd _getcwd
#else
#include <unistd.h>
#include <sys/param.h>
#endif

using namespace std;
namespace db = deceve::bama;

namespace deceve {
namespace storage {

StorageManager::StorageManager()
    : open_files(),
      primaryFiles(),
      auxiliaryFiles(),
      intermediateFiles(),
      readCostReal(0),
      writeCostReal(0),
      persistentClock(0),
      atomicUniqueFileId(0),
      atomicUniqueQueryId(0),
      atomicUniqueNodeId(0) {
  loadBasicTablesInCatalog();
  // char path[MAXPATHLEN];
  // location = ::getcwd(path, MAXPATHLEN);
  // if(loadFilesFlag){
  // loadBasicTablesInCatalog();
  // loadFilesFlag=false;
  // }
  totalFileSizes = 0;
  totalNumberOfReferences = 0;
  budgetSizePermanent = 0;
  storageReadDelay = 0;
  storageWriteDelay = 0;
}

// StorageManager::StorageManager(const std::string& s) :
// location(s), open_files(), primary_files(), auxiliary_files(),
// intermediate_files(), read_cost_real(
// 0), write_cost_real(0), persistent_clock(0), atomicUniqueQueryId(
// 0), atomicUniqueNodeId(0) {
// }

StorageManager::~StorageManager() {
  // COUT << "Storage Manager destructor is called" << "\n";
  for (auto it = open_files.begin(); it != open_files.end(); ++it) {
    delete it->second;
    // open_files.erase(it);
  }
  open_files.erase(open_files.begin(), open_files.end());
}

// LOCKED
File* StorageManager::openFile(const string& filename, FileMode m) {
  // std::lock_guard < std::mutex > lock(openFilesMutex);
  File* f;
  auto it = open_files.find(filename);
  // {
  // if file is opened than just return the pointer
  // to the file otherwise insert it.
  if (it != open_files.end()) return it->second;
  open_files.insert(pair<string, File*>(filename, f = new File(filename)));
  // }

  if (m == PRIMARY) {
    assert((auxiliaryFiles.find(filename) == auxiliaryFiles.end()) &&
           ": mode change (from auxiliary to primary)");
    assert((intermediateFiles.find(filename) == intermediateFiles.end()) &&
           ": mode change (from intermediate to primary)");
    // primaryFilesMutex.lock();
    primaryFiles.insert(filename);
    // primaryFilesMutex.unlock();
  } else if (m == INTERMEDIATE) {
    assert((primaryFiles.find(filename) == primaryFiles.end()) &&
           ": mode change (from primary to intermediate)");
    assert((auxiliaryFiles.find(filename) == auxiliaryFiles.end()) &&
           ": mode change (from auxiliary to intermediate)");
    // intermediateFilesMutex.lock();
    intermediateFiles.insert(filename);
    // intermediateFilesMutex.unlock();
  } else if (m == AUXILIARY) {
    assert((primaryFiles.find(filename) == primaryFiles.end()) &&
           ": mode change (from primary to auxiliary)");
    assert((intermediateFiles.find(filename) == intermediateFiles.end()) &&
           ": mode change (from intermediate to auxiliary)");
    // auxiliaryFilesMutex.lock();
    auxiliaryFiles.insert(filename);
    // auxiliaryFilesMutex.unlock();
  }

  return f;
}
// LOCKED
void StorageManager::closeFile(const string& filename) {
  // std::lock_guard < std::mutex > lock(openFilesMutex);
  // std::cout<<"Close file: "<<filename<<std::endl;
  std::this_thread::sleep_for(std::chrono::nanoseconds(2));

  auto it = open_files.find(filename);
  // std::cout<<"Trigger close file from open_files: "<<filename<<"\n";
  // for(auto it : open_files){
  //   std::cout<<"Open file: "<<it.first<<std::endl;
  // }
  assert(it != open_files.end() && "File is not open");
  delete it->second;
  open_files.erase(it);
}
// LOCKED
void StorageManager::removeFile(const string& filename) {
  if (isOpen(filename)) {
    // First make sure to close the file and remove it from open_files
    // std::cout<<"Trigger removeFile file from removeFile function:
    // "<<filename<<"\n";
    closeFile(filename);

    // Next remove files from the corresponding set
    // (primary_files, auxilary_files, intermediate_files)

    std::unordered_set<std::string>* fileModeSet = getFileModeSet(filename);
    auto it1 = fileModeSet->find(filename);
    assert(it1 != fileModeSet->end() &&
           "file doesn't belong in a set and this is why it cannot be removed");

    // FileMode fmode = getFileMode(filename);

    fileModeSet->erase(it1);

    // remove it from the current map that keep statistics about file sizes
    auto it = fileStatistics.find(filename);
    assert(it != fileStatistics.end() &&
           "file doesn't belong in a set and this is why it cannot be removed");
    fileStatistics.erase(it);

    // Remove from dirty pages counters

    auto dirtyIterator = dirtyCounters.find(filename);
    if (dirtyIterator != dirtyCounters.end()) {
      dirtyCounters.erase(dirtyIterator);
    }

    // Finally remove it from disk
    // require(::remove(filename.c_str()) == 0, "could not delete file.");
    int resultFlag = ::remove(filename.c_str());
    assert(resultFlag == 0 && "could not delete file.");
    (void)resultFlag;
  }
}
// LOCKED
// Unused
void StorageManager::renameFile(const std::string& filename,
                                const std::string& n) {
  // First make sure to rename it in the open_files
  auto it = open_files.find(filename);
  assert(it != open_files.end() && "File is not open");

  File* file = it->second;
  {
    // std::lock_guard < std::mutex > lock(openFilesMutex);
    open_files.erase(it);
    open_files.insert(pair<std::string, File*>(n, file));
  }
  // Change the file name at the corresponding set
  // (primary_files, auxilary_files, intermediate_files)
  std::unordered_set<std::string>* fileModeSet = getFileModeSet(filename);
  auto it1 = fileModeSet->find(filename);
  assert(it1 != fileModeSet->end() &&
         "file doesn't belong in any set and this is why it cannot be removed");

  if (getFileMode(filename) == PRIMARY) {
    // primaryFilesMutex.lock();
    fileModeSet->erase(it1);
    // primaryFilesMutex.unlock();
  } else if (getFileMode(filename) == INTERMEDIATE) {
    // intermediateFilesMutex.lock();
    fileModeSet->erase(it1);
    // intermediateFilesMutex.unlock();
  } else {
    // auxiliaryFilesMutex.lock();
    fileModeSet->erase(it1);
    // auxiliaryFilesMutex.unlock();
  }

  if (getFileMode(n) == PRIMARY) {
    // primaryFilesMutex.lock();
    fileModeSet->insert(n);
    // primaryFilesMutex.unlock();
  } else if (getFileMode(n) == INTERMEDIATE) {
    // intermediateFilesMutex.lock();
    fileModeSet->insert(n);
    // intermediateFilesMutex.unlock();
  } else {
    // auxiliaryFilesMutex.lock();
    fileModeSet->insert(n);
    // auxiliaryFilesMutex.unlock();
  }

  // fileModeSet->erase(it1);
  // fileModeSet->insert(n);

  // fileStatisticsMutex.lock();
  // std::lock_guard < std::mutex > slock(fileStatisticsMutex);
  // Rename it from the current map that keep statistics about file sizes
  auto it2 = fileStatistics.find(filename);
  assert(it2 != fileStatistics.end() &&
         "File is not inserted in the file statistics");

  offset_t size = it2->second.size;
  FileMode mode = it2->second.mode;
  Version version = it2->second.version;
  fileStatistics.erase(it2);

  auto it3 = fileStatistics.find(n);

  // Reassure that the new name does not exist in the file_statistics already
  if (it3 == fileStatistics.end()) {
    FileInformation fi;
    fi.size = size;
    fi.mode = mode;
    fi.version = version;
    fileStatistics.insert(pair<std::string, FileInformation>(n, fi));
  }
  // fileStatisticsMutex.unlock();

  // require(::rename(filename.c_str(), n.c_str()) == 0,
  // "could not rename files.");
  int resultRenameFlag = ::rename(filename.c_str(), n.c_str());
  assert(resultRenameFlag == 0 && "could not rename files.");
  (void)resultRenameFlag;
  // open_files.insert(pair<string, File *>(filename, f = new File(filename)));
}

bool StorageManager::isOpen(const string& filename) const {
  return open_files.find(filename) != open_files.end();
}

inline bool StorageManager::isPrimary(const std::string& filename) const {
  return (primaryFiles.find(filename) != primaryFiles.end());
}

inline bool StorageManager::isAuxiliary(const std::string& filename) const {
  return (auxiliaryFiles.find(filename) != auxiliaryFiles.end());
}

inline bool StorageManager::isIntermediate(const std::string& filename) const {
  return (intermediateFiles.find(filename) != intermediateFiles.end());
}

void StorageManager::readPage(const MemPage& pg, FileMode fmode) {
  readPage(pg.getFilename(), pg.getOffset(), pg.getData(), fmode);
}

void StorageManager::readPage(const string& fname, const offset_t offt,
                              char* const buffer, FileMode fmode) {
  uint64_t startTimer = rdtsc();
  auto start = chrono::steady_clock::now();

  // page has not been written, it was just created
  offset_t file_size_tmp = getFileSizeFromFileSystem(fname, fmode);
  if (file_size_tmp <= offt) {
    return;
  }

  auto it = open_files.find(fname);
  assert(it != open_files.end() && ": File should have been open, but isn't.");
  File* f = it->second;
  int fd = f->getFD();

  int flagLseek = lseek64(fd, offt, SEEK_SET);
  assert(flagLseek >= 0 && "Error while seeking");
  (void)flagLseek;

  ssize_t flagRead = read(fd, buffer, PAGE_SIZE_PERSISTENT);
  assert(flagRead && "Error while reading");
  (void)flagRead;

  // -----------------------------------------------------
  db::delay(storageReadDelay * (db::get_pagesize() / db::cacheline_size));
  // db::increment_reads(db::get_pagesize() / db::cacheline_size);
  physicalReads += db::get_pagesize() / db::cacheline_size;

  if (fmode == PRIMARY) {
    physicalPrimaryReads += db::get_pagesize() / db::cacheline_size;
  } else if (fmode == INTERMEDIATE) {
    physicalIntermediateReads += db::get_pagesize() / db::cacheline_size;
  } else if (fmode == AUXILIARY) {
    physicalAuxiliaryReads += db::get_pagesize() / db::cacheline_size;
  }

  uint64_t endTimer = rdtsc();
  auto end = chrono::steady_clock::now();

  totalReadChronoTime += end - start;
  totalReadChronoCounter++;
  totalRunTimeForStorageReadingPages += (endTimer - startTimer);

  //  if (pageStorageReadTimerFlag < 10000) {
  //    pageStorageReadTimer += (endTimer - startTimer);
  //    pageStorageReadTimerFlag++;
  //  }
}

void StorageManager::writePage(MemPage& pg, FileMode fmode) {
  uint64_t startTimer = rdtsc();
  auto start = chrono::steady_clock::now();

  writePage(pg.getFilename(), pg.getOffset(), pg.getData(), fmode);
  pg.setDirty(false);

  uint64_t endTimer = rdtsc();
  auto end = chrono::steady_clock::now();
  totalRunTimeForStorageWritingPages += (endTimer - startTimer);
  totalWriteChronoCounter++;
  totalWriteChronoTime += end - start;
  //  if (pageStorageWriteTimerFlag < 10000) {
  //    pageStorageWriteTimer += (endTimer - startTimer);
  //    pageStorageWriteTimerFlag++;
  //  }
}
// LOCKED
void StorageManager::writePage(const string& fname, const offset_t offt,
                               char* const buffer, FileMode fmode) {
  auto it = open_files.find(fname);
  //  std::cout<<"writePage: "<<fname<<" "<<offt<<std::endl;
  if (it == open_files.end()) {
    errorCounter++;
    //  std::cout<<"HEREEEE File was previously deleted so dont write
    //  page"<<fname<<" "<<offt<<std::endl;
    return;
  }
  //  assert(it != open_files.end() && ": File should have been open, but
  //  isn't.");
  // openFilesMutex.unlock();

  File* f = it->second;
  int fd = f->getFD();

  int flagLseek = lseek64(fd, offt, SEEK_SET);
  assert(flagLseek >= 0 && "Error while seeking");
  (void)flagLseek;

  ssize_t flagWrite = write(fd, buffer, PAGE_SIZE_PERSISTENT);

  assert(flagWrite && "Error while writing");
  (void)flagWrite;
  //   fsync(fd);

  statsManager.increaseMonitorMap(fname);

  // Update dirty counter
  auto dirtyIterator = dirtyCounters.find(fname);

  if (dirtyIterator == dirtyCounters.end()) {
    dirtyCounters.insert(std::make_pair(fname, 1));
  } else {
    dirtyIterator->second++;
  }

  db::delay(storageWriteDelay * (db::get_pagesize() / db::cacheline_size));

  physicalWrites += db::get_pagesize() / db::cacheline_size;
  if (fmode == PRIMARY) {
    physicalPrimaryWrites += db::get_pagesize() / db::cacheline_size;
  } else if (fmode == INTERMEDIATE) {
    physicalIntermediateWrites += db::get_pagesize() / db::cacheline_size;
  } else if (fmode == AUXILIARY) {
    physicalAuxiliaryWrites += db::get_pagesize() / db::cacheline_size;
  }
}
// LOCKED
void StorageManager::closeAllFiles() {
  // std::lock_guard < std::mutex > lock(openFilesMutex);
  for (auto it = open_files.begin(); it != open_files.end(); it++)
    delete it->second;
  open_files.clear();
}

// LOCKED for update
// this function opens the file and gets it size
offset_t StorageManager::getFileSizeFromFileSystem(const string& filename,
                                                   FileMode mode) {
  File* tmp = openFile(filename, mode);
  FileInformation fi;
  fi.size = tmp->getSize();
  fi.mode = mode;
  // std::lock_guard < std::mutex > lock(fileStatisticsMutex);
  fileStatistics.insert(pair<std::string, FileInformation>(filename, fi));
  return fi.size;
}

offset_t StorageManager::getFileSizeFromCatalog(const std::string& filename,
                                                FileMode mode) {
  // std::unique_lock < std::mutex > lock(getFileSizeMutex);
  if (isOpen(filename)) {
    auto it = fileStatistics.find(filename);

    // if(it->second.mode != mode){
    // it->second.mode = mode;
    // }

    // for(std::map<std::string, FileInformation>::iterator
    //    it= fileStatistics.begin();
    // it != fileStatistics.end(); ++it){
    // COUT<<"Filename: "<<it->first<<" File Information:
    // "<<it->second.size<<"\n";
    // }
    assert(it != fileStatistics.end() && "File is not added in statistics");
    return it->second.size;
  } else {
    return getFileSizeFromFileSystem(filename, mode);
  }
}

offset_t StorageManager::getFileSizeInPagesFromCatalog(
    const std::string& filename, FileMode mode) {
  return getFileSizeFromCatalog(filename, mode) /
         deceve::storage::PAGE_SIZE_PERSISTENT;
}
/**
 * Returns number of pages that a primary file
 */
size_t StorageManager::getPrimaryFileSizeInPagesForProjectedRelation(
    table_key tk) {
  auto calculatedSizeIter = primaryCalculatedSizes.find(tk);
  if (calculatedSizeIter != primaryCalculatedSizes.end()) {
    return calculatedSizeIter->second;
  }

  auto enumToFilenameIter = tableNameMapper.find(tk.table_name);

  //  assert(enumToFilenameIter != tableNameMapper.end() && "StorageManager.hh,
  //  Filename not in tableNameMapper");

  if (enumToFilenameIter == tableNameMapper.end()) {
    // std::cout << "File: " << tk << " not in the system anymore" << std::endl;
    return 1;
  }

  size_t fileSize = getFileSizeFromCatalog(
                        "dbfiles/" + enumToFilenameIter->second + ".table") /
                    deceve::storage::PAGE_SIZE_PERSISTENT;

  if (tk.projected_field == ALL) {
    //  std::cout << "getPrimaryFileSizeInPages: ALL fields are projected so
    //  just "
    //             "return the filesize"
    //           << tk << std::endl;
    primaryCalculatedSizes.insert(std::make_pair(tk, fileSize));
    return fileSize;
  }
  // size_t sizeOfTupleOfRelationInBytes =
  //     calculateSizeOfTupleOfInitialRelation(tk.table_name);
  // size_t sizeOfProjectedFieldInBytes =
  //     calculateSizeOfProjectedField(tk.projected_field);

  size_t numberOfRelationTuplesInPage =
      calculateNumberOfTuplesInPage(tk.table_name);
  size_t numberOfProjectedTuplesInPage =
      calculateNumberOfTuplesInPage(tk.projected_field);
  size_t totalNumberOfTuplesForTk = fileSize * numberOfRelationTuplesInPage;
  //  std::cout << "---- RELATION (" << totalNumberOfTuplesForTk << ")------"
  //            << std::endl;
  //  std::cout << "sizeOfRelationTupleInBytes: " <<
  //  sizeOfTupleOfRelationInBytes
  //            << std::endl;
  //  std::cout << "numberOfRelationTuplesInPage: " <<
  //  numberOfRelationTuplesInPage
  //            << std::endl;
  //  std::cout << "fileSize: " << fileSize << std::endl;
  //

  size_t totalNumberOfPagesForProjectedRelation =
      totalNumberOfTuplesForTk / numberOfProjectedTuplesInPage;
  //
  //  std::cout << "---- PROJECTED_RELATION (" << totalNumberOfTuplesForTk
  //            << ")------" << std::endl;
  //  std::cout << "sizeOfProjectedTupleInBytes: " <<
  //  sizeOfProjectedFieldInBytes
  //            << std::endl;
  //  std::cout << "numberOfProjectedTuplesInPage: "
  //            << numberOfProjectedTuplesInPage << std::endl;
  //  std::cout << "totalNumberOfPagesForProjectedRelation "
  //            << totalNumberOfPagesForProjectedRelation << std::endl;
  //  std::cout << "--------------------------------------" << std::endl;
  //
  //

  primaryCalculatedSizes.insert(
      std::make_pair(tk, totalNumberOfPagesForProjectedRelation));

  return totalNumberOfPagesForProjectedRelation;
  //    return getFileSizeFromCatalog("dbfiles/" + it->second + ".table") /
  //    deceve::bama::PAGE_SIZE_PERSISTENT;
}

size_t StorageManager::getPrimaryFileSizeInPagesForRelation(table_key tk) {
  table_key cloneTk = tk;
  cloneTk.projected_field = ALL;
  return getPrimaryFileSizeInPagesForProjectedRelation(cloneTk);
}

size_t StorageManager::getPrimaryFileSizeInTuplesForRelation(table_key tk) {
  table_key cloneTk = tk;
  cloneTk.projected_field = ALL;

  auto enumToFilenameIter = tableNameMapper.find(cloneTk.table_name);

  if (enumToFilenameIter == tableNameMapper.end()) {
    //    std::cout << "File: " << cloneTk << " not in the system anymore"
    //              << std::endl;
    return 1;
  }

  size_t fileSize = getFileSizeFromCatalog(
                        "dbfiles/" + enumToFilenameIter->second + ".table") /
                    deceve::storage::PAGE_SIZE_PERSISTENT;

  //  size_t sizeOfTupleOfRelationInBytes =
  //      calculateSizeOfTupleOfInitialRelation(cloneTk.table_name);

  size_t numberOfRelationTuplesInPage =
      calculateNumberOfTuplesInPage(cloneTk.table_name);

  size_t totalNumberOfTuplesForTk = fileSize * numberOfRelationTuplesInPage;

  return totalNumberOfTuplesForTk;
}

int StorageManager::getNumberOfDirtyPagesFromSM(const std::string& fn,
                                                FileMode mode) {
  int fileSizeInPages =
      static_cast<int>(getFileSizeInPagesFromCatalog(fn, mode));

  auto dirtyIter = dirtyCounters.find(fn);
  if (dirtyIter == dirtyCounters.end()) {
    //      std::cout<<"getNumberOfDirtyPagesFromSM fname (zero dirty pages):
    //      "<<fn<<" dirtypages: "<<fileSizeInPages<<std::endl;
    return fileSizeInPages;
  } else {
    //      std::cout<<"getNumberOfDirtyPagesFromSM fname: "<<fn<<"
    //      dirtypages: "<<fileSizeInPages- dirtyIter->second<<std::endl;
    return fileSizeInPages - dirtyIter->second;
  }
}

double StorageManager::getNumberOfTuplesForLineitem() {
  deceve::storage::table_key lineitemKey;
  lineitemKey.table_name = deceve::storage::LINEITEM;
  double lineitemTuples =
      static_cast<double>(getPrimaryFileSizeInTuplesForRelation(lineitemKey));
  //  std::cout << "lineitem key left tuples: " << lineitemTuples << std::endl;

  return lineitemTuples;
}

FileMode StorageManager::getFileModeFromCatalog(const std::string& filename) {
  if (isOpen(filename)) {
    auto it = fileStatistics.find(filename);
    assert(it != fileStatistics.end() && "File is not added in statistics");
    //  return it->second.mode;
    return it->second.mode;

  } else {
    return PRIMARY;
  }
}

// LOCKED
void StorageManager::updateFileSize(const std::string& filename,
                                    offset_t size) {
  // std::lock_guard < std::mutex > lock(fileStatisticsMutex);

  auto it = fileStatistics.find(filename);
  assert(it != fileStatistics.end() && "File is not added in statistics");
  it->second.size = size;
}

// LOCKED
void StorageManager::updateFileMode(const std::string& filename,
                                    FileMode mode) {
  // fileStatisticsMutex.lock();
  auto it = fileStatistics.find(filename);
  assert(it != fileStatistics.end() && "File is not added in statistics");

  it->second.mode = mode;
  // fileStatisticsMutex.unlock();

  // Change the file name at the corresponding
  // set (primary_files, auxilary_files, intermediate_files)
  std::unordered_set<std::string>* fileModeSet = getFileModeSet(filename);
  std::unordered_set<std::string>* newFileModeSet = getFileModeSet(mode);
  std::unordered_set<std::string>::iterator it1 = fileModeSet->find(filename);
  assert(it1 != fileModeSet->end() &&
         "file doesn't belong in any set and this is why it cannot be removed");

  if (getFileMode(filename) == PRIMARY) {
    // primaryFilesMutex.lock();
    fileModeSet->erase(it1);
    // primaryFilesMutex.unlock();
  } else if (getFileMode(filename) == INTERMEDIATE) {
    // intermediateFilesMutex.lock();
    fileModeSet->erase(it1);
    // intermediateFilesMutex.unlock();
  } else if (getFileMode(filename) == AUXILIARY) {
    // auxiliaryFilesMutex.lock();
    fileModeSet->erase(it1);
    // auxiliaryFilesMutex.unlock();
  }

  if (mode == PRIMARY) {
    // primaryFilesMutex.lock();
    newFileModeSet->insert(filename);
    // primaryFilesMutex.unlock();
  } else if (mode == INTERMEDIATE) {
    // intermediateFilesMutex.lock();
    newFileModeSet->insert(filename);
    // intermediateFilesMutex.unlock();
  } else if (mode == AUXILIARY) {
    // auxiliaryFilesMutex.lock();
    newFileModeSet->insert(filename);
    // auxiliaryFilesMutex.unlock();
  }
}

// DATA STRUCTURE FUNCTIONS
// table_catalog
void StorageManager::insertIntoTableCatalog(const std::string& tname) {
  tableCatalog.insert(tname);
}

bool StorageManager::isInTableCatalog(const std::string& tname) {
  return tableCatalog.find(tname) != tableCatalog.end();
}

// DATA STRUCTURE FUNCTIONS
// persisted_file_catalog
bool StorageManager::isInPersistedFileCatalog(const table_key& tk) {
  return persistedFileCatalog.find(tk) != persistedFileCatalog.end();
}

void StorageManager::insertInPersistedFileCatalog(
    const std::pair<table_key, table_value>& record) {
  // std::lock_guard < std::mutex > lock(persistedCurrentFileCatalogMutex);

  table_value modifiedTV = record.second;
  // Get the classifier from the temporal catalog with files being persisted
  // auto it = getIteratorRecordFromTmpFileSet();
  auto it = persistedTmpFileSet.find(record.first);

  assert((it != persistedTmpFileSet.end()) && ": Cannot Find File Classifier");

  modifiedTV.fileClassifier = it->second;

  persistedFileCatalog.insert({record.first, modifiedTV});

  // for (size_t i = 0; i < record.second.num_files; ++i) {
  // std::stringstream filename;
  // filename << record.second.output_name << record.second.full_output_path
  // << "_persistent." << i;
  // COUT << "Matched file: " << filename.str() << "\n";
  // nameConverter.insert(std::make_pair(filename.str(), record.first));
  // }
}

void StorageManager::deleteFromPersistedFileCatalog(
    const global_table_map::iterator& it) {
  // std::lock_guard < std::mutex > lock(persistedCurrentFileCatalogMutex);
  persistedFileCatalog.erase(it);
}

global_table_map::iterator StorageManager::getIterOfPersistedFileCatalog(
    const table_key& tk) {
  return persistedFileCatalog.find(tk);
}

// DATA STRUCTURE FUNCTIONS
// persisted_future_file_references
bool StorageManager::isInPersistedFutureFileReferences(const table_key& tk) {
  //  std::cout << "isInPersistedFutureFileReferences: " << tk << std::endl;
  // printPersistentFutureFileReferences();
  return persistedFutureFileReferences.find(tk) !=
         persistedFutureFileReferences.end();
}
void StorageManager::insertInPersistedFutureFileReferences(
    const std::pair<table_key, std::pair<int, int> >& record) {
  // std::lock_guard < std::mutex > lock(persistedFutureFileReferencesMutex);
  std::vector<std::pair<int, int> > list;
  list.push_back(record.second);
  persistedFutureFileReferences.insert(std::make_pair(record.first, list));
}

void StorageManager::deleteFromPersistedFutureFileReferences(
    const global_table_references::iterator& it) {
  // std::lock_guard < std::mutex > lock(persistedFutureFileReferencesMutex);
  persistedFutureFileReferences.erase(it);
}
global_table_references::iterator
StorageManager::getIterOfPersistedFutureFileReferences(const table_key& tk) {
  return persistedFutureFileReferences.find(tk);
}
void StorageManager::clearPersistedFutureFileReferences() {
  // std::lock_guard < std::mutex > lock(persistedFutureFileReferencesMutex);
  while (!persistedFutureFileReferences.empty()) {
    persistedFutureFileReferences.begin()->second.clear();
    persistedFutureFileReferences.erase(persistedFutureFileReferences.begin());
  }
}

void StorageManager::clearFutureOrderedQueries() {
  futureOrderedQueries.clear();
}

int StorageManager::getTotalNumberOfReferences() {
  int total = 0;

  for (auto it = persistedFutureFileReferences.begin();
       it != persistedFutureFileReferences.end(); ++it) {
    total += it->second.size();
  }
  return total;
}

int StorageManager::getTotalNumberOfFilesPastReferences() {
  int total = 0;

  for (auto it = persistedPastFileStatistics.begin();
       it != persistedPastFileStatistics.end(); ++it) {
    total += it->second.references;
  }

  return total;
}

size_t StorageManager::calculateNumberOfPartitions(
    const std::string& leftfile, const std::string& rightfile,
    int numberOfAvailablePrimaryPages) {
  size_t fileSizeInPagesLeft = getFileSizeInPagesFromCatalog(leftfile);
  size_t fileSizeInPagesRight = getFileSizeInPagesFromCatalog(rightfile);

  //  std::cout << "calculateNumberOfPartitions: fileSizeInPagesLeft: "
  //            << fileSizeInPagesLeft << std::endl;
  //  std::cout << "calculateNumberOfPartitions: fileSizeInPagesRight: "
  //            << fileSizeInPagesRight << std::endl;

  size_t fileSizeInPages = (fileSizeInPagesLeft > fileSizeInPagesRight)
                               ? fileSizeInPagesLeft
                               : fileSizeInPagesRight;
  //    size_t number_of_partitions =
  //        (size_t)fileSizeInPages /
  //        (numberOfAvailablePrimaryPages / numberOfThreads);

  size_t number_of_partitions =
      (size_t)ceil(static_cast<double>(fileSizeInPages) /
                   (ceil(static_cast<double>(numberOfAvailablePrimaryPages) /
                         static_cast<double>(numberOfThreads))));

  if (number_of_partitions == 0 || number_of_partitions > 1000) {
    number_of_partitions = 1;
  }

  number_of_partitions = number_of_partitions + amountOfPartitions;

  return number_of_partitions;
}

size_t StorageManager::calculateNumberOfPartitions(
    size_t fileSizeInPagesLeft, size_t fileSizeInPagesRight,
    size_t numberOfAvailablePrimaryPages) {
  //  std::cout << "calculateNumberOfPartitions: fileSizeInPagesLeft (given): "
  //            << fileSizeInPagesLeft << std::endl;
  //  std::cout << "calculateNumberOfPartitions: fileSizeInPagesRight (given): "
  //            << fileSizeInPagesRight << std::endl;

  fileSizeInPagesRight += 50;
  fileSizeInPagesLeft += 50;

  size_t fileSizeInPages = (fileSizeInPagesLeft > fileSizeInPagesRight)
                               ? fileSizeInPagesLeft
                               : fileSizeInPagesRight;

  size_t number_of_partitions =
      (size_t)ceil(static_cast<double>(fileSizeInPages) /
                   (ceil(static_cast<double>(numberOfAvailablePrimaryPages) /
                         static_cast<double>(numberOfThreads))));

  if (number_of_partitions == 0 || number_of_partitions > 1000) {
    number_of_partitions = 1;
  }

  number_of_partitions = number_of_partitions + amountOfPartitions;

  return number_of_partitions;
}

size_t StorageManager::calculateNumberOfPartitions(
    const std::string& leftfile, int numberOfAvailablePrimaryPages) {
  size_t fileSizeInPages = getFileSizeInPagesFromCatalog(leftfile);

  size_t number_of_partitions =
      (size_t)ceil(static_cast<double>(fileSizeInPages) /
                   (ceil(static_cast<double>(numberOfAvailablePrimaryPages) /
                         static_cast<double>(numberOfThreads))));

  //    size_t number_of_partitions =
  //        (size_t)fileSizeInPages /
  //        (numberOfAvailablePrimaryPages / numberOfThreads);

  if (number_of_partitions == 0 || number_of_partitions > 1000) {
    number_of_partitions = 1;
  }

  number_of_partitions = number_of_partitions + amountOfPartitions;

  return number_of_partitions;
}

double StorageManager::getInternalOperationCost(const table_key& tk) {
  auto it = internalScores.find(tk);
  if (it != internalScores.end()) {
    return it->second;
  }
  return -1;
}

void StorageManager::setInternalOperationCost(const table_key& tk,
                                              double score) {
  internalScores.insert(std::make_pair(tk, score));
}

// DATA STRUCTURE FUNCTIONS
// persisted_file_statistics
// LOCKED
void StorageManager::insertToPersistentStatistics(
    const std::pair<table_key, table_statistics>& tuple) {
  // std::lock_guard < std::mutex > lock(persistedPastFileStatisticsMutex);
  if (!isContainedInPersistentStatistics(tuple.first)) {
    persistedPastFileStatistics.insert(tuple);
  }
}

global_table_statistics::const_iterator
StorageManager::getIterOfPersistentStatistics(const table_key& tk) {
  return persistedPastFileStatistics.find(tk);
}

// LOCKED
void StorageManager::updatePersistentStatistics(table_key tk,
                                                table_statistics ts) {
  // std::lock_guard < std::mutex > lock(persistedPastFileStatisticsMutex);
  auto it = persistedPastFileStatistics.find(tk);

  if (it != persistedPastFileStatistics.end()) {
    it->second.hits += ts.hits;
    it->second.references += ts.references;
    it->second.timestamp = ts.timestamp;
  }
}

// LOCKED
void StorageManager::incrementPersistentStatisticsReferences(
    table_key tk, bool persistentFlag) {
  // std::lock_guard < std::mutex > lock(persistedPastFileStatisticsMutex);
  auto it = persistedPastFileStatistics.find(tk);
  if (it != persistedPastFileStatistics.end()) {
    //    COUT << "Number of references: " << it->second.references << "\n";
    it->second.references = it->second.references + 1;
    if (persistentFlag)
      it->second.timesPersisted = it->second.timesPersisted + 1;
    it->second.timestamp = getNextPersistentTime();
    //    COUT << "New Value: " << it->second << "\n";
  }
}
// LOCKED
void StorageManager::incrementPersistentStatisticsHits(table_key tk) {
  // std::lock_guard < std::mutex > lock(persistedPastFileStatisticsMutex);
  auto it = persistedPastFileStatistics.find(tk);
  if (it != persistedPastFileStatistics.end()) {
    it->second.hits++;
    it->second.timestamp = getNextPersistentTime();
    //    COUT << "Number of hits: " << it->second << "\n";
  }
}

bool StorageManager::isContainedInPersistentStatistics(table_key tk) {
  return (persistedPastFileStatistics.find(tk) !=
          persistedPastFileStatistics.end());
}

// DATA STRUCTURE FUNCTIONS
// persisted_tmp_file_set
bool StorageManager::isInPersistedTmpFileSet(const table_key& tk) {
  return persistedTmpFileSet.find(tk) != persistedTmpFileSet.end();
}

void StorageManager::insertInPersistedTmpFileSet(
    const table_key& tk, const FILE_CLASSIFIER& fileClassifier) {
  // std::lock_guard < std::mutex > lock(persistedTmpFileSetMutex);
  persistedTmpFileSet.insert({tk, fileClassifier});
}

void StorageManager::deleteFromPersistedTmpFileSet(const table_key& tk) {
  // std::lock_guard < std::mutex > lock(persistedTmpFileSetMutex);
  persistedTmpFileSet.erase(tk);
}

table_key StorageManager::getNameFromConverter(const std::string& filename) {
  auto it = nameConverter.find(filename);
  if (it == nameConverter.end()) {
    std::cout << "FILENAME: " << filename << std::endl;
  }
  assert(it != nameConverter.end() && "Filename not in nameConverter");
  return it->second;
}

std::string StorageManager::generateUniqueFileName(const std::string& name) {
  // Need to check that the generated file doesn't exist in the saved files
  // std::lock_guard < std::mutex > lock(uniqueFileNameMutex);
  // std::stringstream ss;
  // ss << name << "_" << getNextUniqueFileId();
  // COUT << "FILENAME_GENERATED: " << ss.str() << "\n";
  // return ss.str();
  return name + "_" + std::to_string(getNextUniqueFileId());
}

File* StorageManager::getFile(const std::string& filename) {
  auto it = open_files.find(filename);
  return (it == open_files.end()) ? NULL : it->second;
}

int StorageManager::calculateSpaceOfNewPersistedFiles() {
  int spaceOfNewPersistedFiles = 0;
  for (auto it = persistedTmpFileSet.begin(); it != persistedTmpFileSet.end();
       ++it) {
    // if (it->second == fileClassifier)
    spaceOfNewPersistedFiles += static_cast<int>(
        getPrimaryFileSizeInPagesForProjectedRelation(it->first));
  }
  return spaceOfNewPersistedFiles;
}

int StorageManager::calculateSpaceOfPersistedFiles(
    FILE_CLASSIFIER fileClassifier) {
  int spaceOfPersistedFiles = 0;
  for (auto it = persistedFileCatalog.begin(); it != persistedFileCatalog.end();
       ++it) {
    //      if (it->second.fileClassifier == fileClassifier && it->second.pins
    //      > 0) {
    if (it->second.fileClassifier == fileClassifier && it->second.pins > 0) {
      spaceOfPersistedFiles += static_cast<int>(
          getPrimaryFileSizeInPagesForProjectedRelation(it->first));
    }
  }
  return spaceOfPersistedFiles;
}

int StorageManager::calculateTotalSpaceOfPersistedFiles() {
  int spaceOfPersistedFiles = 0;
  for (auto it = persistedFileCatalog.begin(); it != persistedFileCatalog.end();
       ++it) {
    if (it->second.pins > 0) {
      spaceOfPersistedFiles += static_cast<int>(
          getPrimaryFileSizeInPagesForProjectedRelation(it->first));
    }
  }
  return spaceOfPersistedFiles;
}

int StorageManager::calculateSpaceOfFuturePersistedFiles(
    FILE_CLASSIFIER fileClassifier) {
  int spaceOfPersistedFiles = 0;
  for (auto it = persistedFileCatalog.begin(); it != persistedFileCatalog.end();
       ++it) {
    //      if (it->second.fileClassifier == fileClassifier && it->second.pins
    //      > 0) {
    if (it->second.fileClassifier == fileClassifier) {
      spaceOfPersistedFiles += static_cast<int>(
          getPrimaryFileSizeInPagesForProjectedRelation(it->first));
    }
  }
  return spaceOfPersistedFiles;
}

FileMode StorageManager::getFileMode(const std::string& filename) const {
  if (isPrimary(filename)) {
    return PRIMARY;
  } else if (isIntermediate(filename)) {
    return INTERMEDIATE;
  } else if (isAuxiliary(filename)) {
    return AUXILIARY;
  }
  return PRIMARY;
}

// Get the set that contain files from a specific mode
std::unordered_set<std::string>* StorageManager::getFileModeSet(
    const std::string& filename) {
  if (isPrimary(filename)) {
  } else if (isIntermediate(filename)) {
    return &intermediateFiles;
  } else if (isAuxiliary(filename)) {
    return &auxiliaryFiles;
  }
  return &primaryFiles;
}

// Get the set that contain files from a specific mode
std::unordered_set<std::string>* StorageManager::getFileModeSet(
    const FileMode& fmode) {
  if (fmode == PRIMARY) {
    return &primaryFiles;
  } else if (fmode == INTERMEDIATE) {
    return &intermediateFiles;
  } else if (fmode == AUXILIARY) {
    return &auxiliaryFiles;
  }
  return &primaryFiles;
}

void StorageManager::loadBasicTablesInCatalog() {
  std::cout << "Load basic tables \n";
  tableCatalog.insert("dbfiles/customer.table");
  tableCatalog.insert("dbfiles/nation.table");
  tableCatalog.insert("dbfiles/partsupp.table");
  tableCatalog.insert("dbfiles/region.table");
  tableCatalog.insert("dbfiles/lineitem.table");
  tableCatalog.insert("dbfiles/orders.table");
  tableCatalog.insert("dbfiles/part.table");
  tableCatalog.insert("dbfiles/supplier.table");

  tableNameMapper.insert(std::make_pair(CUSTOMER, "customer"));
  tableNameMapper.insert(std::make_pair(NATION, "nation"));
  tableNameMapper.insert(std::make_pair(PARTSUPP, "partsupp"));
  tableNameMapper.insert(std::make_pair(REGION, "region"));
  tableNameMapper.insert(std::make_pair(LINEITEM, "lineitem"));
  tableNameMapper.insert(std::make_pair(ORDERS, "orders"));
  tableNameMapper.insert(std::make_pair(PART, "part"));
  tableNameMapper.insert(std::make_pair(SUPPLIER, "supplier"));

  tableTupleSizeMapper.insert(
      std::make_pair(CUSTOMER, sizeof(CUSTOMER_STRUCT_FORMAT)));
  tableTupleSizeMapper.insert(
      std::make_pair(NATION, sizeof(NATION_STRUCT_FORMAT)));
  tableTupleSizeMapper.insert(
      std::make_pair(PARTSUPP, sizeof(PARTSUPP_STRUCT_FORMAT)));
  tableTupleSizeMapper.insert(
      std::make_pair(REGION, sizeof(REGION_STRUCT_FORMAT)));
  tableTupleSizeMapper.insert(
      std::make_pair(LINEITEM, sizeof(LINEITEM_STRUCT_FORMAT)));
  tableTupleSizeMapper.insert(
      std::make_pair(ORDERS, sizeof(ORDER_STRUCT_FORMAT)));
  tableTupleSizeMapper.insert(std::make_pair(PART, sizeof(PART_STRUCT_FORMAT)));
  tableTupleSizeMapper.insert(
      std::make_pair(SUPPLIER, sizeof(SUPPLIER_STRUCT_FORMAT)));

  /*
   * enum PROJECTED_FIELD {
   ALL,
   CUSTOMER_Q7,
   LINEITEM_Q5,
   LINEITEM_Q8,
   LINEITEM_Q9,
   LINEITEM_Q14,
   LINEITEM_Q17,
   LINEITEM_Q18,
   ORDER_Q7_SORTED,
   ORDER_Q12,
   ORDER_Q13_SORTED,
   PARTSUPP_Q2,
   PARTSUPP_Q11,
   PARTSUPP_Q16
   };
   */

  // for queries 8,9,14,17
  projectedTableTupleSizeMapper.insert(std::make_pair(
      LINEITEM_UNIFIED, sizeof(deceve::queries::UNIFIED_PR_LINEITEM)));

  // for queries 5 and 18
  projectedTableTupleSizeMapper.insert(
      std::make_pair(LINEITEM_UNIFIED_ORDER_KEY,
                     sizeof(deceve::queries::UNIFIED_PR_LINEITEM_ORDER_KEY)));

  // for queries 2 and 16
  projectedTableTupleSizeMapper.insert(std::make_pair(
      PARTSUPP_UNIFIED_PARTKEY, sizeof(deceve::queries::Q2_PR_PARTSUPP)));

  // for queries 12 and 18
  projectedTableTupleSizeMapper.insert(std::make_pair(
      ORDER_UNIFIED_ORDER_KEY, sizeof(deceve::queries::Q2_PR_PARTSUPP)));

  projectedTableTupleSizeMapper.insert(
      std::make_pair(CUSTOMER_Q7, sizeof(deceve::queries::Q7_PR_CUSTOMER)));
  projectedTableTupleSizeMapper.insert(
      std::make_pair(LINEITEM_Q5, sizeof(deceve::queries::Q5_PR_LINEITEM)));
  projectedTableTupleSizeMapper.insert(
      std::make_pair(LINEITEM_Q8, sizeof(deceve::queries::Q8_PR_LINEITEM)));

  projectedTableTupleSizeMapper.insert(
      std::make_pair(LINEITEM_Q9, sizeof(deceve::queries::Q9_PR_LINEITEM)));
  projectedTableTupleSizeMapper.insert(
      std::make_pair(LINEITEM_Q14, sizeof(deceve::queries::Q14_PR_LINEITEM)));
  projectedTableTupleSizeMapper.insert(
      std::make_pair(LINEITEM_Q17, sizeof(deceve::queries::Q17_PR_LINEITEM)));
  projectedTableTupleSizeMapper.insert(
      std::make_pair(LINEITEM_Q18, sizeof(deceve::queries::Q18_PR_LINEITEM)));
  projectedTableTupleSizeMapper.insert(std::make_pair(
      ORDER_Q7_SORTED, sizeof(deceve::queries::Q7_PR_ORDER_SORT)));
  projectedTableTupleSizeMapper.insert(
      std::make_pair(ORDER_Q12, sizeof(deceve::queries::Q12_PR_ORDER)));
  projectedTableTupleSizeMapper.insert(std::make_pair(
      ORDER_Q13_SORTED, sizeof(deceve::queries::Q13_PR_ORDER_SORT)));
  projectedTableTupleSizeMapper.insert(
      std::make_pair(ORDER_Q18, sizeof(deceve::queries::Q18_PR_ORDER)));
  projectedTableTupleSizeMapper.insert(
      std::make_pair(PART_Q17_OTHER, sizeof(deceve::queries::Q17_PR_PART)));
  projectedTableTupleSizeMapper.insert(
      std::make_pair(PARTSUPP_Q2, sizeof(deceve::queries::Q2_PR_PARTSUPP)));
  projectedTableTupleSizeMapper.insert(
      std::make_pair(PARTSUPP_Q11, sizeof(deceve::queries::Q11_PR_PARSTSUPP)));
  projectedTableTupleSizeMapper.insert(
      std::make_pair(PARTSUPP_Q16, sizeof(deceve::queries::Q16_PR_PARTSUPP)));
}

void StorageManager::printTableCatalog() {
  std::cout << "---------PRIMARY TABLE_CATALOG START---------"
            << "\n";
  for (std::set<std::string>::const_iterator it = tableCatalog.begin();
       it != tableCatalog.end(); ++it) {
    std::cout << *it << "\n";
  }
  std::cout << "---------PRIMARY TABLE_CATALOG END---------"
            << "\n";
}

void StorageManager::printPersistentFileCatalog() {
  COUT << "---------  Persistent CurrentFileCatalog state START------ "
       << "\n";

  DEBUG(persistedFileCatalog);

  //  for (auto it = persistedFileCatalog.begin(); it !=
  //  persistedFileCatalog.end();
  //       ++it) {
  //    std::cout << it->first << " " << it->second << " ";
  //    int candidateSize = static_cast<int>(
  //        getPrimaryFileSizeInPagesForProjectedRelation(it->first));
  //    std::cout << " sizeTest: " << candidateSize << " ";
  //    auto internalScore = internalScores.find(it->first);
  //    if (internalScore != internalScores.end()) {
  //      std::cout << "internal score: " << internalScore->second << std::endl;
  //    } else {
  //      std::cout << "internal score not calculated yet " << std::endl;
  //    }
  //  }
  COUT << "--------- Persistent CurrentFileCatalog state END ------"
       << "\n";
}

void StorageManager::printPersistentFutureFileReferences() {
  std::cout << "---- Persistent Future File References state START------  "
            << "\n";
  for (auto it = persistedFutureFileReferences.begin();
       it != persistedFutureFileReferences.end(); ++it) {
    std::cout << it->first;
    std::cout << " ---> [";

    for (auto vectorIt = it->second.begin(); vectorIt != it->second.end();
         ++vectorIt) {
      std::cout << " (" << vectorIt->first << ": " << vectorIt->second << ") ,";
    }

    DEBUG(it->second);

    std::cout << "]"
              << "\n";
  }
  std::cout << "------- Persistent Future File References state END--------- "
            << "\n";
}

void StorageManager::printFutureOrderQueries() {
  std::cout << "-------------- FutureOrderedQueries START------------ "
            << "\n";

  for (auto it = futureOrderedQueries.begin(); it != futureOrderedQueries.end();
       ++it) {
    std::cout << *it << "\n";
  }

  std::cout << "------------ FutureOrderedQueries END-----------------  "
            << "\n";
}

void StorageManager::printFileStatistics() {
  std::cout << "Print file statistics:"
            << "\n";
  for (auto it = fileStatistics.begin(); it != fileStatistics.end(); ++it) {
    std::cout << "key: \"" << it->first << "\" "
              << "value: " << it->second.size << "\n";
  }
}

void StorageManager::printPesistentFileStatistics() {
  COUT << "----- Persisted PastFile Statistics state START ----- "
       << "\n";

  for (auto it = persistedPastFileStatistics.begin();
       it != persistedPastFileStatistics.end(); ++it) {
    std::cout << "key: \"" << it->first << "\" "
              << "value: " << it->second << "\n";
  }

  std::cout << "----- Persisted PastFile Statistics state END --------- "
            << "\n";
}

void StorageManager::printOpenFiles() {
  std::cout << "Print Open files:"
            << "\n";
  for (auto it = open_files.begin(); it != open_files.end(); ++it) {
    std::cout << "key: \"" << it->first << "\" "
              << "value: " << it->second << "\n";
  }
}

void StorageManager::printFiles(FileMode mode) {
  std::unordered_set<std::string>* fileset;
  std::string header;

  if (mode == PRIMARY) {
    fileset = &primaryFiles;
    header = "PRIMARY Files ";
  } else if (mode == AUXILIARY) {
    fileset = &auxiliaryFiles;
    header = "AUXILIARY Files ";
  } else if (mode == INTERMEDIATE) {
    fileset = &intermediateFiles;
    header = "INTERMEDIATE Files ";
  } else if (mode == FREE) {
    fileset = &intermediateFiles;
    header = "FREE Files ";
  } else {
    fileset = &primaryFiles;
    header = "PRIMARY Files ";
  }

  std::cout << "------------- " << header << " --------------"
            << "\n";
  for (std::unordered_set<std::string>::const_iterator it = fileset->begin();
       it != fileset->end(); ++it) {
    std::cout << "Filename: " << *it << " Size (Number of pages): "
              << getFileSizeFromCatalog(*it) /
                     deceve::storage::PAGE_SIZE_PERSISTENT
              << "\n";
  }
}

void StorageManager::printAllFiles() {
  printFiles(PRIMARY);
  printFiles(AUXILIARY);
  printFiles(INTERMEDIATE);
}

void StorageManager::printCurrentStateOfPool() {
  printPersistentFutureFileReferences();
  printPersistentFileCatalog();
  printPesistentFileStatistics();
}

void StorageManager::printFutureStatisticsCounter() {
  std::cout << "printFutureStatisticsCounter" << std::endl;
  for (const auto& it : futurePersistedStatisticsCounter) {
    std::cout << it.first << " " << it.second << std::endl;
  }
}

void StorageManager::printInternalScores() {
  std::vector<std::pair<table_key, double> > inScores;
  std::cout << "printInternalScores" << std::endl;
  for (const auto& it : internalScores) {
    inScores.push_back(std::make_pair(it.first, it.second));
  }
  std::sort(inScores.begin(), inScores.end(),
            [](std::pair<table_key, double> a, std::pair<table_key, double> b) {
              return a.second > b.second;
            });
  for (const auto& it : inScores) {
    std::cout << "{" << it.second << " " << it.first << "} ";
  }
  std::cout << std::endl;
  inScores.erase(inScores.begin(), inScores.end());
}

void StorageManager::printSharingStatistics(const std::string& filename) {
  std::fstream file;
  file.open(filename, std::ios::out | std::ios::app);

  file << "---------------------- Persisted PastFile Statistics state START "
          "---------------------- "
       << "\n";

  auto totalNumberOfReferences = 0;
  auto totalNumberOfHits = 0;
  auto totalNumberOfTimesPersisted = 0;
  auto totalCostSaved = 0;
  std::map<table_key, table_statistics>::const_iterator it;
  for (it = persistedPastFileStatistics.begin();
       it != persistedPastFileStatistics.end(); ++it) {
    table_key cloneKey = it->first;
    cloneKey.projected_field = ALL;

    size_t initialSize =
        getPrimaryFileSizeInPagesForProjectedRelation(cloneKey);
    size_t currentSize =
        getPrimaryFileSizeInPagesForProjectedRelation(it->first);
    double percentageSize =
        static_cast<double>(currentSize) / static_cast<double>(initialSize);

    file << "key: \"" << it->first << "\" "
         << "value: " << it->second << " size: " << currentSize;
    file << " vs initial_size: " << initialSize << " , " << setprecision(2)
         << percentageSize << "\n";
    if (it->first.version != HASHJOIN && it->first.version != SORT) {
      continue;
    } else {
      totalNumberOfReferences += it->second.references;
      totalNumberOfHits += it->second.hits;
      totalNumberOfTimesPersisted += it->second.timesPersisted;
      totalCostSaved += it->second.hits * it->second.cost;
    }
  }

  file << "---------------------- Persisted PastFile Statistics state END "
          "---------------------- "
       << "\n";

  file << "---------------------- Persisted Statistics START "
          "---------------------- "
       << "\n";

  file << "totalNumberOfReferences: " << totalNumberOfReferences << std::endl;
  file << "totalNumberOfTimesPersisted: " << totalNumberOfTimesPersisted
       << std::endl;
  file << "totalNumberOfHits: " << totalNumberOfHits << std::endl;
  file << "totalCostSaved: " << totalCostSaved << std::endl;

  file << "---------------------- Persisted Statistics END "
          "---------------------- "
       << "\n";

  file.close();
}

void StorageManager::exportRuntimesToFile(const std::string& filename,
                                          size_t numCycles) {
  std::ofstream file;
  file.open(filename, std::ios::out | std::ios::app);

  file << "\n";
  for (auto t : runTimes) {
    file << t << "   :   " << t / numCycles << "\n";
  }
  file.close();

  runTimes.erase(runTimes.begin(), runTimes.end());
}

// void StorageManager::loadBasicTablesInCatalog() {
// insertIntoTableCatalog(
// deceve::queries::constructFullPath("customer.table"));
// insertIntoTableCatalog(deceve::queries::constructFullPath("nation.table"));
// insertIntoTableCatalog(
// deceve::queries::constructFullPath("partsupp.table"));
// insertIntoTableCatalog(deceve::queries::constructFullPath("region.table"));
// insertIntoTableCatalog(
// deceve::queries::constructFullPath("lineitem.table"));
// insertIntoTableCatalog(deceve::queries::constructFullPath("orders.table"));
// insertIntoTableCatalog(deceve::queries::constructFullPath("part.table"));
// insertIntoTableCatalog(
// deceve::queries::constructFullPath("supplier.table"));
// }

}  // namespace storage
}  // namespace deceve
