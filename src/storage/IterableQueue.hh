/*
 * IterableQueue.hh
 *
 *  Created on: 30 Apr 2015
 *      Author: michail
 */

#ifndef PBM2015_S1250553_SRC_STORAGE_ITERABLEQUEUE_HH_
#define PBM2015_S1250553_SRC_STORAGE_ITERABLEQUEUE_HH_

#include <iostream>
#include "stdlib.h"
#include "stdio.h"
#include <iomanip>
#include <cstdio>
#include <sys/types.h>
#include <fstream>
#include <cstring>
#include <vector>
#include <time.h>
#include <queue>

// Wrapper over queue for const iterator
template <typename T, typename Container = std::deque<T> >
class iterable_queue : public std::queue<T, Container> {
 public:
  typedef typename Container::iterator iterator;
  typedef typename Container::const_iterator const_iterator;
  typedef typename Container::reverse_iterator reverse_iterator;
  typedef typename Container::const_reverse_iterator const_reverse_iterator;

  iterator begin() { return this->c.begin(); }
  iterator end() { return this->c.end(); }

  const_iterator begin() const { return this->c.begin(); }
  const_iterator end() const { return this->c.end(); }

  reverse_iterator rbegin() { return this->c.rbegin(); }
  reverse_iterator rend() { return this->c.rend(); }

  const_reverse_iterator rbegin() const { return this->c.rbegin(); }
  const_reverse_iterator rend() const { return this->c.rend(); }
};

#endif /* PBM2015_S1250553_SRC_STORAGE_ITERABLEQUEUE_HH_ */
