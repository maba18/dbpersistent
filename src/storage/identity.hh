#ifndef __IDENTITY_HH__
#define __IDENTITY_HH__

namespace deceve {
namespace bama {

template<typename T>
class Identity {
 public:
  typedef T key_type;
  const key_type& operator()(const T& t) const {
    return t;
  }
};

template<typename F, typename S>
class Pair {
 public:
  F first;
  S second;
};

template<typename F, typename S>
class PairCombiner {
 public:
  typedef Pair<F, S> record_type;
  Pair<F, S> operator()(const F& f, const S& s) const {
    Pair<F, S> pair;
    pair.first = f;
    pair.second = s;
    return pair;
  }
};

template<typename T>
struct DefaultComparator {
  bool operator()(const T& a) {
    (void) a;
    return true;
  }
};

template<typename T>
class Converter {
 public:
  typedef T key_type;
  const key_type& operator()(const T& t) const {
    return t;
  }
};

}
}

#endif
