#ifndef HEAPFILE_HH
#define HEAPFILE_HH

#include <iterator>
#include <iostream>

#include "../utils/types.hh"
#include "../utils/helpers.hh"
#include "../storage/Pages.hh"
#include "../storage/BufferManager.hh"

namespace deceve { namespace storage {

// the preamble of any heap file
struct HeapPreamble {
    offset_t firstPageOffset;
    offset_t lastPageOffset;
};
	
// the page used for storing the preamble
class HeapPreamblePage: public PageBase {
public:
    HeapPreamble preambleInfo;
private:
    char padding[PAGE_SIZE - sizeof(HeapPreamble)];
		
public:
    HeapPreamblePage(): preambleInfo() {}
    HeapPreamblePage(const HeapPreamble& hp): preambleInfo(hp) {}
    ~HeapPreamblePage() {}
};
	
// the header of every page in the heap file
struct HeapPageHeader {
    offset_t id;
    offset_t previous;
    offset_t next;
};
	
// standard mapping of a heap page
template <typename R>
class HeapPage: public PackedPage<HeapPageHeader, R> {
public:
    HeapPage(): PackedPage<HeapPageHeader, R>() {}
    ~HeapPage() {}
};

// heap file for records with key of type K and payload of type P
template <typename K, typename P>
class Heap {
		
    typedef Record<K, P> R;
    typedef HeapPage<R> HPage;
		
    // enumeration for testing whether a key is present in
     // a file or not
    enum Presence {
	FOUND,
	NOT_FOUND
    };
    // structure of a lookup result
    struct LocateResult {
	Presence present;
	HPage* page;
	unsigned int index;
	unsigned long emptyPageID;
			
	LocateResult(Presence p, HPage* hp, 
		     unsigned int i, unsigned long ep)
	    : present(p), page(hp), 
	      index(i), emptyPageID(ep) {}
    };

    BufferManager * bm;
    std::string filename;
    HeapPreamble preamble;   // file's preamble cached
		
public:
    typedef R record_type;
    typedef K key_type;
    typedef P payload_type;

    Heap(BufferManager * bmptr, const std::string& s): bm(bmptr), filename(s) {
	if (bm->getFileSize(filename) == 0) {
	    // the file does not exist yet,
	    // so simply bootstrap it
	    preamble.firstPageOffset = 1;
	    preamble.lastPageOffset = preamble.firstPageOffset;
	    savePreamble();
	    HPage hp;
	    hp.getHeader().id = preamble.firstPageOffset;
	    hp.getHeader().previous = 0;
	    hp.getHeader().next = 0;
	    writePage(preamble.firstPageOffset, hp);
	}
	else loadPreamble();
	pinPage(0);
    }
	~Heap() { savePreamble(); releasePage(0);}
		
    // inner iterator class
    class iterator;
    friend class iterator;
    class iterator: public std::iterator<std::forward_iterator_tag, R> {
    private:
	Heap& heap;
	HPage* page;
	unsigned long pageID;
	unsigned int idx;
			
    public:
	friend class Heap;
	iterator(Heap& h, 
		 HPage* p, 
		 unsigned int i = 0): heap(h), 
				      page(p), 
				      pageID(p->getHeader().id),
				      idx(i) { }
	
	iterator(Heap& h): heap(h), page(0), pageID(0), idx(0) {
	    if (page)
		heap.pinPage(page->getHeader().id);
	}
	
	~iterator() {
	    if (page)
		heap.releasePage(page->getHeader().id);
	}

	const iterator& operator=(const iterator& b) {
	    heap = b.heap;
	    page = b.page;
	    pageID = b.pageID;
	    idx = b.idx;
	    return *this;
	}
	
	R& current() { return page->getRecord(idx); }

	R& operator*() { return current(); }

	R* operator->() { return &current(); }

	iterator& operator++() {

	    if (idx < page->numRecords()-1) {
		idx++;
		return *this;
	    }
				
	    if (page->getHeader().id == heap.preamble.lastPageOffset) {
		heap.releasePage(page->getHeader().id);
		page = 0;
		pageID = 0;
		idx = 0;
		return *this;
	    }
				
	    page = heap.readPage<HPage>(page->getHeader().next);
	    heap.releasePage(pageID);
	    pageID = page->getHeader().id;
	    heap.pinPage(pageID);
	    idx = 0;

	    return *this;
	}
	iterator& operator++(int) { return operator++(); }
	bool operator==(const iterator& it) const {
	    return pageID == it.pageID && idx == it.idx;
	}
	bool operator!=(const iterator& it) const {
	    return pageID != it.pageID || idx != it.idx;
	}
    };
		
    // begin iterator through the file
    iterator begin() {
	HPage* hp = readPage<HPage>(preamble.firstPageOffset);
	return iterator(*this, hp, 0);
    }
	
    // end sentinel iterator
    iterator end() { return iterator(*this); }
		
    // return an iterator to the record with the given key
    iterator find(const K& k) {
	LocateResult lr = locate(k);
	if (lr.present == NOT_FOUND) return end();
	return iterator(*this, lr.page, lr.index);
    }
		
    bool insert(const R& r) {
	// go through locate() and not find()
	// just so we won't have to expand the
	// file if we don't have to
	LocateResult lr = locate(r.key);
	if (lr.present == FOUND) {
	    lr.page->getRecord(lr.index).payload = r.payload;
	    writePage(lr.page->getHeader().id, *lr.page);
	    releasePage(lr.page->getHeader().id);
	    //    std::cout << "Inserting from (1)" << "\n";
	    return false;
	}
			
	// we found a non-empty page when traversing
	// the file
	if (lr.emptyPageID != 0) {
	    HPage* hp = readPage<HPage>(lr.emptyPageID);
	    pinPage(hp->getHeader().id);
	    require(hp->addRecord(r), "Could not add record in non-empty page.");
	    writePage(hp->getHeader().id, *hp);
	    releasePage(hp->getHeader().id);
	    // std::cout << "Inserting from (2)" << "\n";
	    return true;
	}
			
	// there are no empty pages, just try to add
	// the record to the last page of the file
	HPage *hp = readPage<HPage>(preamble.lastPageOffset);
	if (hp->hasRoom()) {
	    pinPage(hp->getHeader().id);
	    require(hp->addRecord(r), "Could not add record in non-empty page.");
	    writePage(hp->getHeader().id, *hp);
	    releasePage(hp->getHeader().id);
	    //std::cout << "Inserting from (3)" << "\n";
	    return true;
	}
			
	// page is out of room, allocate another one
	// we need to allocate another page and expand the file
	pinPage(hp->getHeader().id);
	HPage newpage;
	newpage.getHeader().id = ++preamble.lastPageOffset;
	newpage.getHeader().previous = hp->getHeader().id;
	newpage.getHeader().next = 0;
	require(newpage.addRecord(r), "Could not add record in new page.");
	hp->getHeader().next = newpage.getHeader().id;
	writePage(hp->getHeader().id, *hp);
	releasePage(hp->getHeader().id);
	savePreamble();
	writePage(newpage.getHeader().id, newpage);
	//std::cout << "Inserting from (4)" << "\n";
	return true;
    }
		
    // remove a record by key -- return false if the record
    // cannot be found
    bool remove(const K& k) {
	LocateResult fr = locate(k);
	if (fr.present == NOT_FOUND) return false;
	fr.page->removeRecord(fr.index);
	writePage(fr.page->getHeader().id, *fr.page);
	releasePage(fr.page->getHeader().id);
	return true;
    }
		
    // remove a record given iterator position -- return false
    // if the iterator points past the end of the file
    bool remove(iterator& it) {
	if (it == end()) return false;
	it.page->removeRecord(it.idx);
	writePage(it.page->getHeader().id, *it.page);
	releasePage(it.page->getHeader().id);
	// check if the last record of the file has been reached
	if (it.page->getHeader().id == preamble.lastPageOffset
	    && it.idx == it.page->numRecords()) {
	    it.page = 0;
	    it.pageID = 0;
	    it.idx = 0;
	    }		
	return true;
    }
		
    // update a record given an iterator pointing to it
    // return false if the iterator points past the end
    // of the file
    bool update(iterator it, const P& p) {
	if (it == end()) return false;
	it.page->getRecord(it.idx).payload = p;
	writePage(it.page->getHeader().id, *it.page);
	return true;
    }
		
    bool update(const K& k, const P& p) {
	iterator it = find(k);
	if (it == end()) return false;
	return update(it, p);
    }
		
protected:
    LocateResult locate(const K& k) {
	offset_t offset = preamble.firstPageOffset;
	HPage* hp = NULL;
	LocateResult lr(NOT_FOUND, hp, 0, 0);
			
	while (offset) {
//	    std::cout << "Locate looking into page: " << offset << "\n";
	    hp = readPage<HPage>(offset);
	    pinPage(hp->getHeader().id);
	    
	    for (unsigned int i = 0; i < hp->numRecords(); i++) {
		if (hp->getRecord(i).key == k) {
		    lr.present = FOUND;
		    lr.page = hp;
		    lr.index = i;
		    return lr;
		}
	    }
				
	    if (hp->hasRoom() && lr.emptyPageID == 0) 
		lr.emptyPageID = hp->getHeader().id;
				
	    offset_t next = hp->getHeader().next;
	    releasePage(offset);
	    offset = next;
	}
			
	return lr;
    }

    // save the file's preamble
    void savePreamble() {
	writePage(0, HeapPreamblePage(preamble));
    }
    
    // load the preamble from the file
    void loadPreamble() {
	preamble = (readPage<HeapPreamblePage>(0))->preambleInfo;
    }

    // read a page from the file using the buffer manager
    template <typename PG>
    PG* const readPage (offset_t offt) {
	return (PG*) bm->readPage(filename, offt * PAGE_SIZE);
    }
    
    // write a page to the file using the buffer manager
    void writePage (offset_t offt, const PageBase& p) {
	bm->writePage(filename, offt * PAGE_SIZE, (char *) &p);
    }

    void pinPage(offset_t offt) {
	bm->pinPage(filename, offt * PAGE_SIZE);
    }
    
    void releasePage(offset_t offt) {
	bm->releasePage(filename, offt * PAGE_SIZE);
    }
		
};	// class Heap //:~

}}

#endif	// HEAPFILE_HH //:~
