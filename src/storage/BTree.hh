#ifndef BTREE_HH
#define BTREE_HH

#include "../utils/types.hh"
#include "../utils/helpers.hh"
#include "../storage/Pages.hh"
#include "../storage/BufferManager.hh"
#include <iterator>

namespace deceve {
namespace storage {

struct BTreePreamble {
  offset_t rootID;
  offset_t firstLeafID;
  unsigned int depth;
  offset_t totalPages;
};

class BTreePreamblePage : public PageBase {
 public:
  BTreePreamble preambleInfo;

 private:
  char padding[PAGE_SIZE_PERSISTENT - sizeof(BTreePreamble)];

 public:
  BTreePreamblePage() : preambleInfo() {}
  BTreePreamblePage(const BTreePreamble& bp) : preambleInfo(bp) {}
  ~BTreePreamblePage() {}
};

struct BTreeIndexHeader {
  offset_t id;
  offset_t lowPointer;
};

struct BTreeLeafHeader {
  offset_t id;
  offset_t previous;
  offset_t next;
};

template <typename K, typename P>
class BTree {
  typedef Record<K, P> R;
  typedef Record<K, offset_t> BTIndexRecord;
  typedef SetOrderedPage<BTreeIndexHeader, BTIndexRecord> BTIndexPage;
  typedef SetOrderedPage<BTreeLeafHeader, R> BTLeafPage;
  typedef typename BTLeafPage::iterator leaf_iterator;
  typedef typename BTIndexPage::iterator node_iterator;

  enum Presence { FOUND, NOT_FOUND };

  enum Overflow { OVERFLOWN, NO_OVERFLOWN };

  struct OverflowResult {
    Presence present;
    Overflow overflow;
    BTIndexRecord promoted;
  };

  struct LocateResult {
    Presence present;
    BTLeafPage* page;
    unsigned int index;

    LocateResult(Presence p, BTLeafPage* hp, unsigned int i)
        : present(p), page(hp), index(i) {}
  };

  BufferManager* bm;
  std::string filename;
  BTreePreamble preamble;

 public:
  typedef R record_type;
  typedef K key_type;
  typedef P payload_type;

  BTree(BufferManager* bmptr, const std::string& s) : bm(bmptr), filename(s) {
    if (bm->getFileSize(filename, deceve::storage::INTERMEDIATE) == 0) {
      // the file does not exist yet,
      // so bootstrap it
      preamble.rootID = 1;
      preamble.firstLeafID = 1;
      preamble.depth = 1;
      preamble.totalPages = 2;
      savePreamble();
      BTLeafPage bp;
      bp.getHeader().id = preamble.rootID;
      bp.getHeader().previous = 0;
      bp.getHeader().next = 0;
//      std::cout << "writePage: " << preamble.rootID  << std::endl;
      writePage(preamble.rootID, bp);
    } else {
      loadPreamble();
    }

    pinPage(0);
  }
  ~BTree() {
    savePreamble();
    releasePage(0);
    std::cout << "B-tree " << filename << " has " << preamble.totalPages
              << " pages in total"
              << "\n";
  }

  // inner iterator
  class iterator;
  friend class iterator;
  class iterator : public std::iterator<std::forward_iterator_tag, R> {
   private:
    BTree& btree;
    BTLeafPage* leaf;
    offset_t leafID;
    unsigned int idx;

   public:
    friend class BTree;
    iterator(BTree& bt, BTLeafPage* l, unsigned int i = 0)
        : btree(bt), leaf(l), leafID(l->getHeader().id), idx(i) {
      // sanity check
      if (leaf) {
        btree.pinPage(leafID);
      }
    }
    iterator(BTree& bt) : btree(bt), leaf(0), leafID(0), idx(0) {}
    ~iterator() {
      // release the page if it has not been released yet
      if (leaf) btree.releasePage(leaf->getHeader().id);
    }
    iterator& operator=(const iterator& it) {
      btree = it.btree;
      leaf = it.leaf;
      leafID = it.leafID;
      idx = it.idx;
    }
    R& current() { return leaf->getRecord(idx); }
    R& operator*() { return current(); }
    R* operator->() { return &current(); }
    iterator& operator++() {
      if (idx < leaf->numRecords() - 1) {
        idx++;
        return *this;
      }

      if (leaf->getHeader().next == 0) {
        btree.releasePage(leaf->getHeader().id);
        leaf = 0;
        leafID = 0;
        idx = 0;
        return *this;
      }
      //	    std::cout << "entering page: " << leaf->getHeader().next <<
      //"\n";
      leaf = btree.readPage<BTLeafPage>(leaf->getHeader().next);
      btree.releasePage(leafID);
      leafID = leaf->getHeader().id;
      btree.pinPage(leafID);
      idx = 0;

      return *this;
    }
    iterator& operator++(int) { return operator++(); }
    bool operator==(const iterator& it) const {
      return leafID == it.leafID && idx == it.idx;
    }
    bool operator!=(const iterator& it) const {
      return leafID != it.leafID || idx != it.idx;
    }

    offset_t getOffset() const { return leafID; }
  };

  iterator begin() {
    BTLeafPage* bp = readPage<BTLeafPage>(preamble.firstLeafID);
    // iterator constructor will pin page if it is not pinned
    return iterator(*this, bp, 0);
  }

  // end sentinel iterator
  iterator end() { return iterator(*this); }

  iterator find(const K& k) {
    LocateResult lr = locate(k);
    if (lr.present == NOT_FOUND) {
      releasePage(lr.page->getHeader().id);
      return end();
    }
    // locate() results are pinned
    return iterator(*this, lr.page, lr.index);
  }

  iterator lower(const K& k) {
    LocateResult lr = locate(k);
    if (lr.page->numRecords() == 0) {
      // an empty page was reached
      offset_t id = lr.page->getHeader().next;
      while (id != 0) {
        lr.page = readPage<BTLeafPage>(id);
        if (lr.page->numRecords() == 0) {
          lr.index = 0;
          return iterator(*this, lr.page, lr.index);
        }
        id = lr.page->getHeader().next;
      }
      releasePage(lr.page->getHeader().id);
      return end();
    }
    return iterator(*this, lr.page, lr.index);
  }

  iterator upper(const K& k) {
    iterator it = lower(k);
    while (it != end() && it->key <= k) it++;
    return it;
  }

  bool insert(const R& r) {
    // call the recursive insertion algorithm recording
    // its result
    OverflowResult ores = insert(r, 1, preamble.rootID);

    // the root overflowed, so we need to create
    // a new root
    if (ores.overflow == OVERFLOWN) {
      // the root overflowed, create a new root
      BTIndexPage newroot;
      newroot.getHeader().id = preamble.totalPages++;
      newroot.getHeader().lowPointer = preamble.rootID;
      require(newroot.addRecord(ores.promoted),
              "Could not add record to new root.");
      preamble.rootID = newroot.getHeader().id;
      writePage(newroot.getHeader().id, newroot);
      preamble.depth++;
      savePreamble();
    }

    // everything went well, simply return
    // whether it was an update or not
    return (ores.present == NOT_FOUND);
  }

  bool remove(const K& k) {
    LocateResult lr = locate(k);
    if (lr.present == NOT_FOUND) {
      return false;
    }

    // pages returned by locate() are always pinned
    bool val = lr.page->removeRecord(lr.index);
    writePage(lr.page->getHeader().id, *lr.page);
    releasePage(lr.page->getHeader().id);
    return val;
  }

  // update a record given an iterator pointing to it
  // return false if the iterator points past the end
  // of the file
  bool update(iterator it, const P& p) {
    if (it == end()) return false;

    // pages in iterators are always pinned
    it.leaf->getRecord(it.idx).payload = p;
    writePage(it.leaf->getHeader().id, *it.leaf);
    return true;
  }

  bool update(iterator it, const R& r) {
    if (it == end()) return false;

    it.leaf.setRecord(it.idx, r);
    writePage(it.leaf->getHeader().id, *it.leaf);
    return true;
  }

  bool update(const K& k, const P& p) {
    iterator it = find(k);
    if (it == end()) return false;

    return update(it, p);
  }

 protected:
  OverflowResult insert(const R& r, unsigned int level, offset_t id) {
    // we have reached the leaves, call the leaf
    // insertion method and bail out
    if (level == preamble.depth) return leafInsert(r, id);

    // we are not at the leaves, keep descending
    BTIndexPage* node = readPage<BTIndexPage>(id);
    pinPage(node->getHeader().id);
    OverflowResult childRes = insert(r, level + 1, scanNode(node, r.key));
    // the child overflowed
    if (childRes.overflow == OVERFLOWN) {
      OverflowResult selfRes = indexInsert(childRes.promoted, node);
      childRes.overflow = selfRes.overflow;
      if (childRes.overflow == OVERFLOWN) {
        childRes.promoted = selfRes.promoted;
      }
    }
    releasePage(node->getHeader().id);
    return childRes;
  }

  OverflowResult leafInsert(const R& r, offset_t id) {
    OverflowResult ores;

    BTLeafPage* leaf = readPage<BTLeafPage>(id);
    pinPage(leaf->getHeader().id);
    leaf_iterator it = leaf->find(r);

    if (it != leaf->end()) {
      // the key was there, so replace it
      // std::cout << "will update" << "\n";
      // BUG: Updates not working !!!!!!!!!!!!!!!!
      // removing the following pointer causes SEG FAULT
      // the right Page::setRecord() cannot be found

      // leaf->update(it, r);

      writePage(id, *leaf);
      // unpin the page and bail out
      releasePage(leaf->getHeader().id);
      ores.present = FOUND;
      ores.overflow = NO_OVERFLOWN;
      return ores;
    }

    // the key was not found, so try to insert it
    if (leaf->addRecord(r)) {
      // there was room in the page, so
      // make the insertion, write the page
      // and report success
      writePage(id, *leaf);
      releasePage(leaf->getHeader().id);
      ores.present = NOT_FOUND;
      ores.overflow = NO_OVERFLOWN;
      return ores;
    }

    // there was no room in the page, so it's time
    // for a split
    // NB: leafSplit() will write the pages as well.
    BTIndexRecord bti = leafSplit(leaf, r);
    // unpin the page before returning
    releasePage(leaf->getHeader().id);
    ores.present = NOT_FOUND;
    ores.overflow = OVERFLOWN;
    ores.promoted = bti;
    return ores;
  }

  OverflowResult indexInsert(const BTIndexRecord& r, BTIndexPage* node) {
    OverflowResult ores;
    ores.overflow = NO_OVERFLOWN;
    // page is already pinned -- nobody will give us grief
    if (!node->addRecord(r)) {
      // record could not be inserted, so we
      // must split and propagate
      // NB: indexSplit() will write pages
      BTIndexRecord bti = indexSplit(node, r);
      ores.overflow = OVERFLOWN;
      ores.promoted = bti;
      return ores;
    }

    // all's well, write and bail out
    writePage(node->getHeader().id, *node);
    return ores;
  }

  // const BTLeaf* since we shall not change the value of the
  // pointer -- only the page contents
  BTIndexRecord leafSplit(BTLeafPage* page, const R& r) {
    // assumption: the page being split is already pinned so
    // we should not anticipate anyone to give us any grief
    BTLeafPage newpage;
    newpage.getHeader().id = preamble.totalPages++;
    newpage.getHeader().previous = page->getHeader().id;
    newpage.getHeader().next = page->getHeader().next;
    page->getHeader().next = newpage.getHeader().id;

    // split the records of the page
    // nothing fancy, just copy and split
    unsigned int cap = page->capacity() + 1;
    // R records[cap];
    R* records = new R[cap];
    for (unsigned int i = 0; i < page->numRecords(); i++) {
      records[i] = page->getRecord(i);
    }

    unsigned int pos = 0;
    while (pos < cap - 1 && records[pos].key < r.key) pos++;

    // make room and add the record
    if (pos != cap - 1) {
      memmove((char*)&records[pos + 1], (char*)&records[pos],
              (cap - pos - 1) * sizeof(R));
    }
    records[pos] = r;

    // clear the original page
    page->clear();
    // copy contents to the two pages
    unsigned int mid = cap / 2;
    for (unsigned int i = 0; i < mid; i++) page->addRecord(records[i]);
    for (unsigned int i = mid; i < cap; i++) newpage.addRecord(records[i]);

    // write the two pages and the preamble and return
    /*	std::cout << "leaf old last "
     << page->getRecord(page->numRecords()-1).key
     << " leaf new first "
     << newpage.getRecord(0).key
     << "\n";
     */
    //	std::cout << "leaf splitting when inserting: " << r.key.kval << "\n";
    //	std::cout << "splitting of leaf " << page->getHeader().id
    //	     << " creates leaf " << newpage.getHeader().id << "\n";
    delete[] records;
    writePage(page->getHeader().id, *page);
    // only write the new page, the old page is already
    // pinned and backed by the buffer pool
    writePage(newpage.getHeader().id, newpage);
    savePreamble();
    // create the entry to propagate
    BTIndexRecord bti;
    bti.key = records[mid].key;
    bti.payload = newpage.getHeader().id;
    return bti;
  }

  BTIndexRecord indexSplit(BTIndexPage* page, const BTIndexRecord& btr) {
    // std::cout << "will split index " << "\n";
    BTIndexPage newpage;
    newpage.getHeader().id = preamble.totalPages++;

    // copy and split
    unsigned int cap = page->capacity() + 1;
    // BTIndexRecord records[cap];
    BTIndexRecord* records = new BTIndexRecord[cap];
    for (unsigned int i = 0; i < page->numRecords(); i++)
      records[i] = page->getRecord(i);

    // find position
    unsigned int pos = 0;
    while (pos < cap - 1 && records[pos].key < btr.key) pos++;
    // std::cout << "pos is " << pos << " out of "
    //	  << cap << "\n";
    // make room and add the record
    if (pos != cap - 1) {
      memmove((char*)&records[pos + 1], (char*)&records[pos],
              (cap - pos - 1) * sizeof(BTIndexRecord));
    }
    records[pos] = btr;
    // std::cout << "inserted new" << "\n";
    // offset of the promoted key
    unsigned int mid = cap / 2;
    // promote mid key and pointer to new page
    // low key of new page is promoted key's old pointer

    //	newpage.getHeader().lowPointer = page->getRecord(mid).payload;
    newpage.getHeader().lowPointer = records[mid].payload;
    BTIndexRecord bti;
    //	bti.key = page->getRecord(mid).key;
    bti.key = records[mid].key;
    bti.payload = newpage.getHeader().id;

    // clear the original page
    page->clear();
    // copy contents to the two pages
    for (unsigned int i = 0; i < mid; i++) page->addRecord(records[i]);
    for (unsigned int i = mid + 1; i < cap; i++) newpage.addRecord(records[i]);

    delete[] records;
    // std::cout << "node old last "
    //	  << page.getRecord(page.numRecords()-1)
    //	  << " node new first "
    //	  << newpage.getRecord(0)
    //	  << "\n";

    // write the pages and return
    writePage(page->getHeader().id, *page);

    // no need to unpin page since it was not pinned
    // tqo begin with
    writePage(newpage.getHeader().id, newpage);
    savePreamble();
    return bti;
  }

  LocateResult locate(const K& k) { return locate(k, 1, preamble.rootID); }

  LocateResult locate(const K& k, unsigned int level, offset_t id) {
    if (level == preamble.depth) {
      // must read a leaf page
      BTLeafPage* page = readPage<BTLeafPage>(id);
      pinPage(page->getHeader().id);
      unsigned int pos = 0;
      // if (page->numRecords() == 0) return LocateResult(NOT_FOUND,
      //						 page, -1);
      while (pos < page->numRecords() && page->getRecord(pos).key < k) pos++;

      if (page->numRecords() != 0 && pos == page->numRecords()) pos--;

      Presence p = NOT_FOUND;
      if (page->numRecords() != 0) {
        p = (page->getRecord(pos).key == k ? FOUND : NOT_FOUND);
      }
      // the leaf page is always pinned before being returned
      // to the called (we do not know what they will do with it
      // so let us be a bit conservative)
      return LocateResult(p, page, pos);
    }

    // need to descend the index; read the index page
    // and scan it (scan is wrapped in a pin/release pair)
    BTIndexPage* page = readPage<BTIndexPage>(id);
    pinPage(page->getHeader().id);
    offset_t scan_res = scanNode(page, k);
    LocateResult lr = locate(k, level + 1, scan_res);
    releasePage(page->getHeader().id);
    return lr;
  }

  offset_t scanNode(BTIndexPage* node, const K& k) {
    // assumption: the node is always pinned when it is being
    // scanned (either for lookup or insertion)
    if (node->getRecord(0).key > k) {
      return node->getHeader().lowPointer;
    }

    unsigned int pos = 0;
    while (pos < node->numRecords() && node->getRecord(pos).key <= k) {
      pos++;
    }
    pos--;
    return node->getRecord(pos).payload;
  }

  // load the preamble from the file
  void loadPreamble() {
    preamble = (readPage<BTreePreamblePage>(0))->preambleInfo;
  }

  // save the file's preamble
  void savePreamble() { writePage(0, BTreePreamblePage(preamble)); }

  // read a page from the file using the buffer manager
  template <typename PG>
  PG* const readPage(offset_t offt) {
    return (PG*)bm->readPage(filename, offt * PAGE_SIZE_PERSISTENT,  deceve::storage::PRIMARY);
  }

  // write a page to the file using the buffer manager
  void writePage(offset_t offt, const PageBase& p) {
    bm->writePage(filename, offt * PAGE_SIZE_PERSISTENT, (char*)&p, deceve::storage::PRIMARY);
  }

  void pinPage(offset_t offt) {
   // std::cout << "pin page for " << (offt * PAGE_SIZE_PERSISTENT) << "\n";
    bm->pinPage(filename, offt * PAGE_SIZE_PERSISTENT,  deceve::storage::PRIMARY);
  }

  void releasePage(offset_t offt) {
    bm->releasePage(filename, offt * PAGE_SIZE_PERSISTENT, deceve::storage::PRIMARY);
  }
};
// class BTree //:~
}
}

#endif  // BTREE_HH //:~
