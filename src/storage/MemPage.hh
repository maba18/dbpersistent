#ifndef MEMPAGE_HH
#define MEMPAGE_HH

#include <iostream>
#include "../utils/types.hh"
#include <atomic>

namespace deceve {
namespace storage {

class MemPage {

public:
	MemPage(const std::string& fname, const offset_t& offt, char * data);
	MemPage(const FilePos& fpos, char * data, const Timestamp& ts);
	~MemPage();
//    inline void addPin() { pinned = true; }
//    inline void removePin() { pinned = false; }

//	inline void addPin() {
	void addPin() {
		//	std::cout << "addPin value before: " << pinned << "\n";
		++pinned;
		//	std::cout << "addPin value after: " << pinned << "\n";
	}
	void removePin() {
		//	std::cout << "removePin value before: " << pinned << "\n";
		--pinned;
		//	std::cout << "removePin value afters: " << pinned << "\n";
	}

	//Getters
	Timestamp& getTimestamp() {
		return timestamp;
	}
	const Timestamp& getTimestamp() const {
		return timestamp;
	}
	FilePos& getFilepos() {
		return filepos;
	}
	const std::string& getFilename() const {
		return filepos.filename;
	}
	void setFilename(const std::string& fn) {
		filepos.filename = fn;
	}
	offset_t getOffset() const {
		return filepos.offset;
	}
	const FilePos& getFilepos() const {
		return filepos;
	}
	char * getData() const {
		return data;
	}
	bool isDirty() const {
		return dirty;
	}
	bool isPinned() const {
		return (pinned.load() > 0) ? true : false;
	}
	void printPin() {
		std::cout << "IS Pinned: " << isPinned() << "\n";
		std::cout << "Pinned?: " << pinned.load() << "\n";
	}

	//Setters
	void setDirty(bool b) {
		dirty = b;
	}
	void invertDirtyness() {
		dirty = ~dirty;
	}
	void setTimestamp(const Timestamp& ts) {
		timestamp = ts;
	}
	void setPin(bool pinValue) {
		pinned.store(pinValue);
	}

private:
	FilePos filepos;
	Timestamp timestamp;
	std::atomic<int> pinned;
	bool dirty;
	char * data;

public:
	int times_read;
	int times_written;

};

std::ostream& operator <<(std::ostream& os, const MemPage& pg);

}
}

#endif
