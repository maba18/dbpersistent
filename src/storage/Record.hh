#ifndef RECORD_HH
#define RECORD_HH

namespace deceve { namespace storage {

struct MyKey {
    unsigned long kval;
    unsigned long dummy;
    
    bool operator==(const MyKey& b) const { return kval == b.kval; }
    bool operator<(const MyKey& b) const { return kval < b.kval; }
    bool operator>(const MyKey& b) const { return kval > b.kval; }
    bool operator<=(const MyKey& b) const { return kval <= b.kval; }
    bool operator>=(const MyKey& b) const { return kval >= b.kval; }

    friend std::ostream& operator<<(std::ostream& os, const MyKey& k) {
	os << k.kval;
	return os;
    }		
};

struct MyPayload {
    long long pvals[10];
    
    friend std::ostream& operator<<(std::ostream& os, const MyPayload& p) {
	for (int i = 0; i < 10; i++)
	    os << p.pvals[i] << " ";
	return os;
    }
 };

MyKey makeKey(int k) {
    MyKey key;
    key.kval = k;
    return key;
}

MyPayload makePayload(long p) {
    MyPayload payload;
    for (int i = 0; i < 10; i++)
	payload.pvals[i] = ++p;
    return payload;
}

}}

#endif /* RECORD_HH */
