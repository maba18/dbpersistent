#include <iostream>
#include "../storage/MemPage.hh"
#include "../utils/types.hh"

namespace deceve {
namespace storage {

MemPage::MemPage(const std::string& fname, const offset_t& offt, char* d_ptr)
    : filepos(fname, offt),
      timestamp(),
      pinned(0),
      dirty(false),
      data(d_ptr),
      times_read(0),
      times_written(0)

{}

MemPage::MemPage(const FilePos& fpos, char* d_ptr, const Timestamp& ts)
    : filepos(fpos),
      timestamp(ts),
      pinned(0),
      dirty(false),
      data(d_ptr),
      times_read(0),
      times_written(0) {}

MemPage::~MemPage() {
  //   std::cout << "MemPage destructor" << "\n";
}

std::ostream& operator<<(std::ostream& os, const MemPage& pg) {
  return os << "File: " << pg.getFilepos().filename
            << ", Offset: " << pg.getFilepos().offset << ", Number: "
            << pg.getFilepos().offset / deceve::storage::PAGE_SIZE_PERSISTENT
            << ", Dirty: " << pg.isDirty() << ", Pinned: " << pg.isPinned()
            << ", Time: " << pg.getTimestamp() << ", Reads: " << pg.times_read
            << ", Writes: " << pg.times_written;
}
}
}
