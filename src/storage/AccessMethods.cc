#include <cstring>
#include "../storage/AccessMethods.hh"

namespace deceve {
namespace storage {

void Scanner::open() {
  offset_t size = bufferManager->getFileSize(filename);
  if (size == 0) {
    pagesNo = 1;
    char* ptr = new char[PAGE_SIZE_PERSISTENT];
    ::memset(ptr, 0, PAGE_SIZE_PERSISTENT);
    bufferManager->writePage(filename, 0, ptr);
    delete[] ptr;
  } else {
    pagesNo = size / PAGE_SIZE_PERSISTENT;
  }
  currentOffset = 0;
}

bool Scanner::hasNext() { return currentOffset < pagesNo; }

const char* Scanner::nextPage() {
  bufferManager->releasePage(filename, currentOffset * PAGE_SIZE_PERSISTENT);
  currentOffset++;
  const char* pg =
      bufferManager->readPage(filename, currentOffset * PAGE_SIZE_PERSISTENT);
  bufferManager->pinPage(filename, currentOffset * PAGE_SIZE_PERSISTENT);
  return pg;
}

void Scanner::close() {
  bufferManager->releasePage(filename, currentOffset * PAGE_SIZE_PERSISTENT);
}
}
}
