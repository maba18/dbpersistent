/*******************************************************************************
 * Copyright (c) 2015 Michail Basios and Stratis Viglas.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michail Basios - initial API and implementation
 *     Stratis Viglas - initial API and implementation
 *******************************************************************************/

#ifndef BUFFERPOOL_HH
#define BUFFERPOOL_HH

// C headers
#include <time.h>

// Boost headers
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/mem_fun.hpp>
#include <boost/any.hpp>
#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/ptr_container/ptr_map.hpp>

// C++ Headers
#include <ctime>
#include <sstream>
#include <iomanip>
#include <string>
#include <fstream>
#include <iostream>
#include <set>
#include <map>
#include <stack>
#include <cstring>
#include <cstddef>

// Multithreading libraries
#include <mutex>
#include <thread>
#include <atomic>
#include <condition_variable>
#include <algorithm>
#include <utility>

#include "../utils/types.hh"
#include "../utils/require.hh"
#include "../utils/helpers.hh"
#include "../storage/MemPage.hh"
#include "../storage/StorageManager.hh"
#include "../utils/defs.hh"
#include "../utils/util.hh"
#include "../utils/global.hh"

namespace bmi = boost::multi_index;
using namespace std;

namespace deceve {
namespace storage {

struct MemPageP {
  MemPage& ref;
  explicit MemPageP(MemPage* pagep) : ref(*pagep) {}
  const FilePos& getFilepos() const { return ref.getFilepos(); }
  const Timestamp& getTimestamp() const { return ref.getTimestamp(); }
  bool getDirtiness() const { return ref.isDirty(); }
};

struct newTimestamp {
  explicit newTimestamp(const Timestamp& nt) : new_timestamp(nt) {}
  void operator()(MemPageP& mp) { mp.ref.setTimestamp(new_timestamp); }

 private:
  Timestamp new_timestamp;
};

struct newFilename {
  newFilename(const std::string& nf) : new_filename(nf) {}
  void operator()(MemPageP& mp) { mp.ref.setFilename(new_filename); }

 private:
  std::string new_filename;
};

// maps pool offsets to MemPage objects
// typedef std::map<unsigned long, MemPage *> PoolMap;
struct Dirtiness {};

// indexes MemPages by FilePos and Timestamp
typedef bmi::multi_index_container<
    MemPageP,
    bmi::indexed_by<
        bmi::hashed_unique<
            bmi::tag<FilePos>,
            bmi::const_mem_fun<MemPageP, const FilePos&, &MemPageP::getFilepos>,
            FilePos_hash, FilePos_equal>,
        bmi::ordered_unique<bmi::tag<Timestamp>,
                            bmi::const_mem_fun<MemPageP, const Timestamp&,
                                               &MemPageP::getTimestamp> >,
        bmi::ordered_unique<
            bmi::tag<Dirtiness>,
            bmi::composite_key<
                MemPageP,
                bmi::const_mem_fun<MemPageP, bool, &MemPageP::getDirtiness>,
                bmi::const_mem_fun<MemPageP, const Timestamp&,
                                   &MemPageP::getTimestamp> > > > >
    MemPageIndex;

typedef MemPageIndex::index<FilePos>::type mpi_by_filepos;
typedef MemPageIndex::index<Timestamp>::type mpi_by_time;
typedef MemPageIndex::index<Dirtiness>::type mpi_by_dirtiness;

// iterator dereference helpers
inline MemPage& page(mpi_by_filepos::iterator it) { return it->ref; }
inline MemPage& page(mpi_by_time::iterator it) { return it->ref; }
inline MemPage& page(mpi_by_dirtiness::iterator it) { return it->ref; }

class BufferPool {
 public:
  deceve::profiling::Timer timerFetchFromPrimaryMode{4, "FetchFromPrimaryMode"};
  deceve::profiling::Timer timerPhysicalReadPageClean{4,
                                                      "physicalReadPageClean"};
  deceve::profiling::Timer timerFreePage{4, "FreePage"};
  deceve::profiling::Timer timerEvictPageTwoLists{3, "timerEvictPageTwoLists"};
  deceve::profiling::Timer timerEvictPageTwoListsClever{
      8, "timerEvictPageTwoListsClever"};

  int algorithm3Case{0};

 private:
  // number of buffer pool pages
  unsigned long pool_size;
  std::atomic<unsigned long> used_pages;
  std::atomic<unsigned long> primary_used_pages;
  std::atomic<unsigned long> auxiliary_used_pages;
  std::atomic<unsigned long> intermediate_used_pages;

  // assigns timestamps to pages
  std::atomic<Timestamp> clock;
  char *pool, *realpool;
  StorageManager* sm;

  // All indexes that are on top of the bufferpool
  MemPageIndex pg_index;
  MemPageIndex auxiliary_pg_index;
  MemPageIndex intermediate_pg_index;
  MemPageIndex free_page_index;
  MemPageIndex free_intermediate_page_index;

  // TODO clean2-new
  MemPageIndex internal_intermediate_index;

  std::map<FilePos, size_t> pin_counts;
  unsigned long primary_references;
  unsigned long primary_hits;
  unsigned long auxiliary_references;
  unsigned long auxiliary_hits;
  unsigned long intermediate_references;
  unsigned long intermediate_hits;

  // slider that decides the number of primary pages
  unsigned long sliderCount;
  unsigned long primaryWrites;
  unsigned long intermediateWrites;
  unsigned long auxiliaryWrites;

  //  the cost saved if pages are forced
  //  to stay in the intermediate cache
  double costSavedIntermediateCache{0};

  // cost to be paid in order to move slider right
  double costOfRegion{0.0};

  // previous cost to be paid in order to move slider right
  double previousCostOfRegion{0};
  double totalDirtyIntermediatePages{0};

  // number of intermediate writes in
  // order to start decreasing the cost
  int flagNumberForIntermediatePages{0};

  int counterOfPrints{0};
  double totalCostDecreased{0};
  double previousHitRatio{0};

  // number of pages for data structure storage
  unsigned long region_pool_size;
  char *regionpool, *realregionpool;

  // number of used pages MemPageIndex region_index;
  unsigned long region_used_pages;

  typedef std::map<std::string, unsigned long> location_map;
  location_map region_index;

  int print_counter;
  int numberOfAvailablePrimaryPages;
  int algorithmId;
  double segmentPercentage{0.1};

  unsigned long uniqueDirtyId{0};

  // Mutexes
  std::mutex pinCounterMutex;
  std::map<table_key, std::stack<MemPageP>*> fileStacks;
  boost::ptr_vector<MemPageIndex> v;
  //   boost::ptr_map<int, MemPageIndex> v1;

 public:
  std::vector<scoreTuple> permanentScores;
  std::map<int64_t, scoreTuple, std::greater<double> > permanentScoresMap;
  std::map<double, scoreTuple, std::less<double> > temporalScoresMap;
  std::vector<scoreTuple> temporalScores;

  std::vector<scoreTuple> orderedScores;
  boost::ptr_map<table_key, MemPageIndex> indexMapTkIndex;
  std::condition_variable condVariable;
  std::mutex condMutex;

  BufferPool(const unsigned long pool_size_b, StorageManager* const sm_ptr);
  ~BufferPool();

  // Read page from the storage manager
  MemPage& physicalReadPage(const FilePos& filepos, FileMode fmode);
  MemPage& physicalReadPageDefault(const FilePos& filepos, FileMode fmode);
  MemPage& physicalReadPageClean(const FilePos& filepos, FileMode fmode);
  MemPage& physicalReadPageClean2(const FilePos& filepos, FileMode fmode);

  // the requested page must have previously been assigned to the file,
  // i.e. the page already belongs to the file
  MemPage& fetchPage(const std::string& fname, offset_t offt);
  MemPage& fetchPage(const std::string& fname, offset_t offt, FileMode fmode);
  MemPage& fetchPage(const FilePos& filepos);
  MemPage& fetchPage(const FilePos& filepos, FileMode fmode);

  MemPage& fetchPageDefault(const FilePos& filepos, FileMode fmode);
  MemPage& fetchPageClean(const FilePos& filepos, FileMode fmode);
  // TODO clean2-new
  MemPage& fetchPageClean2(const FilePos& filepos, FileMode fmode);
  MemPage& fetchFromPrimaryMode(const FilePos& filepos, FileMode fmode,
                                MemPageIndex* primaryIndex,
                                MemPageIndex* intermediateIndex);

  char* requestPage(const FileMode& mode = PRIMARY);
  char* evictPage();

  // Data Structure Store Functions
  void persistResultToDRAM(const std::string&);
  void persistResultToNVRAM(const std::string&);
  void writePageToDataStore(const FilePos& filepos, FileMode new_mode);
  void writePageToDataStoreWithCopy(const FilePos& filepos, FileMode new_mode);
  void deleteFileFromDataStore(const std::string&);
  void saveFileToPersistentMemoryFromDataStore(const std::string&);

  char* requestRegion(unsigned long number_of_pages);
  char* evictRegion();

  inline Timestamp getNextTime() {
    clock++;
    return clock.load();
  }
  void updateTimestamp(mpi_by_filepos::const_iterator it);
  void updateTimestamp(mpi_by_time::const_iterator it);
  void updateTimestamp(MemPageIndex* idx, mpi_by_filepos::const_iterator it);
  void updateTimestamp(MemPageIndex* idx, mpi_by_time::const_iterator it);
  void pinPage(const FilePos& pageid);
  void pinPage(const FilePos& pageid, FileMode fmode);
  void releasePage(const FilePos& pageid);
  void releasePage(const FilePos& pageid, FileMode fmode);
  bool isInIndex(const FilePos& pageid, FileMode fmode);
  void releaseFilePages(const std::string& filename);
  void invalidateFilePages(const std::string& filename);
  void invalidateFilePages(const std::string& filename, FileMode fmode);
  void invalidateFilePages(const table_key& tk);
  void changeFileNameOnBufferPoolPages(const std::string& filename,
                                       const std::string& n);
  void printPages(const FileMode& mode);
  void printPages(MemPageIndex* index);
  void printPagesByTime(const FileMode& mode);
  void printPagesByDirtiness(const FileMode& mode);
  void testMPIndex();
  void printHitRatio();
  void printHitRatio(std::ofstream* f);
  void printMode(FileMode mode);
  void close();
  char* getPool() const { return pool; }

  void setAlgorithm(int algId) { algorithmId = algId; }
  int getAlgorithm() { return algorithmId; }

  friend std::ostream& operator<<(std::ostream& os, const BufferPool& pg);

  MemPageIndex* findIndex(const FileMode& mode);

  // Data Structure Store
  void changeIndex(const FilePos& fp, const FileMode& newMode);
  void changeIndex(MemPageIndex* fromIndex, const FilePos& fp,
                   const FileMode& newMode);
  void changeIndex(MemPageIndex* fromIndex, const FilePos& fp,
                   MemPageIndex* toIndex);

  inline size_t getPinCount() { return pin_counts.size(); }
  size_t getPrimaryPinCount();
  inline size_t getPoolSize() { return pool_size; }

  inline unsigned long numberOfPrimaryPages() const {
    //    if (used_pages.load() < pool_size)
    return pool_size;
    //    return pg_index.size() + free_page_index.size();
  }
  inline unsigned long numberOfAuxiliaryPages() const {
    return auxiliary_pg_index.size();
  }
  inline unsigned long numberOfIntermediatePages() const {
    return pool_size - sliderCount;
  }
  inline unsigned long numberOfFreePages() const {
    return free_page_index.size();
  }

  size_t getSizeOfFile(table_key tk);

  unsigned long getSliderCount() { return sliderCount; }
  void setSliderCount(unsigned long x) { sliderCount = x; }

  double getPrimaryHitRatio() { return hitRatio(PRIMARY); }

  double getIntermediateHitRatio() { return hitRatio(INTERMEDIATE); }

  double getAuxiliaryHitRatio() { return hitRatio(AUXILIARY); }

  double getTotalHitRatio() {
    double totalReferences =
        primary_references + intermediate_references + auxiliary_references;
    double totalHits = primary_hits + intermediate_hits + auxiliary_hits;
    if (totalReferences > 0) {
      return totalHits / totalReferences;
    } else {
      return 0.0;
    }
  }

  double getPrimaryReferences() { return primary_references; }
  double getIntermediateReferences() { return intermediate_references; }

  void addToIntermediateReferences(unsigned long value) {
    intermediate_references += value;
  }

  // Cost of Intermediate dirty pages
  // Cost is calculated in nano_seconds as cost = n * write_delay or cost = n *
  // read_delay
  void increaseCostIntermediateCache(double cost) {
    //    std::cout << "costSavedIntermediateCache: " << cost << std::endl;
    costSavedIntermediateCache += cost;
  }

  void decreaseCostIntermediateCache(double cost) {
    if (costSavedIntermediateCache > 0) {
      costSavedIntermediateCache -= cost;
      totalCostDecreased += cost;
    }
    if (costSavedIntermediateCache < 0) {
      costSavedIntermediateCache = 0;
    }
  }

  void decreaseIntermediateCosts(bool isDirty, FileMode mode);
  // functions of current cost region
  void initialiseCostOfRegion() {
    costOfRegion = segmentPercentage * costSavedIntermediateCache;
    previousCostOfRegion = costOfRegion;
    //    std::cout
    //        << "initialiseCostOfRegion :@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :
    //        "
    //        << costOfRegion << std::endl;
    //    std::cout << "percentage: " << segmentPercentage << std::endl;
  }

  void decreaseCostOfRegion(double cost) {
    if (flagNumberForIntermediatePages > 0) return;
    //    if ((counterOfPrints) % 10000 == 0) {
    //      std::cout << "decreaseCostOfRegion previous cost: " << costOfRegion
    //                << std::endl;
    //    }

    costOfRegion -= cost;
    if (costOfRegion <= (-1) * previousCostOfRegion) {
      //      if (costOfRegion != 0.0) {
      //        std::cout << "RESTORE COST REGION" << std::endl;
      //        std::cout << "Old cost =" << costOfRegion << std::endl;
      //      }
      costOfRegion = previousCostOfRegion;
      //      if (costOfRegion != 0.0) {
      //        std::cout << "New cost =" << costOfRegion << std::endl;
      //      }
    }

    //    if ((counterOfPrints) % 10000 == 0) {
    //      std::cout << "decreaseCostOfRegion: " << costOfRegion << std::endl;
    //    }
  }
  // functions about previous cost region
  void setPreviousCostRegion(double amount) { previousCostOfRegion = amount; }
  double getPreviousCostRegion() { return previousCostOfRegion; }

  // functions to calculate the number of dirty pages
  double getTotalNumberOfDirtyIntermediatePages() const {
    return totalDirtyIntermediatePages;
  }
  void addToTotalNumberOfDirtyIntermediatePages(double numOfPages) {
    this->totalDirtyIntermediatePages += numOfPages;
  }
  void decreaseTotalNumberOfDirtyIntermediatePages() {
    this->totalDirtyIntermediatePages--;
    if (totalDirtyIntermediatePages < 0) totalDirtyIntermediatePages = 0;
  }
  void addFlagNumberForIntermediatePages(int amount) {
    flagNumberForIntermediatePages += amount;
  }

  // function that gives a percentage that
  // represents how pool size affects performance of cache
  inline double functionWithRatio() {
    return totalDirtyIntermediatePages / static_cast<double>(pool_size);
  }

  void setSegmentPercentage(double segment) { segmentPercentage = segment; }

 private:
  void incrementReferences(const FileMode&);
  void incrementHits(FileMode);
  double hitRatio(FileMode);

  void incrementPins(const FilePos&);
  void decrementPins(const FilePos&);
  MemPageIndex* findIndex(const FilePos&);
  MemPageIndex* findIndex(const std::string&);

  char* evictPageByType();
  char* evictPageWithLRU(FileMode mode);

  // Simple versions
  char* evictPageSillyLRU(FileMode mode);
  char* evictPageSillyCRU(FileMode mode);
  char* evictPageSillySRU(FileMode mode);

  // Optimised Versions
  char* evictPageByCost(FileMode mode);
  char* evictPageByCostLRU(FileMode mode);
  char* evictPageCleanFirstLRU2Lists(FileMode mode);
  char* evictPageCleanFirstLRU2ListsClever(FileMode mode);
  char* evictPageCleanFirstLRU2ListsCleverCase1(FileMode mode);
  char* evictPageCleanFirstLRU2ListsCleverCase2(FileMode mode);
  char* evictPageCleanFirstLRU2ListsCleverCase3(FileMode mode);
  char* evictPageByFileCostClean(FileMode mode);
  char* cflru(FileMode mode);

  std::pair<char*, bool> getPrimaryPage();
  std::pair<char*, bool> getIntermediateBasicPage();
  std::pair<char*, bool> getIntermediatePage(
      FILE_CLASSIFIER fileClassifier = NOTCLASSIFIED);
  std::pair<char*, bool> getAuxiliaryPage();
  std::pair<char*, bool> getPageFromIndex(MemPageIndex* index);
  std::pair<char*, bool> getCleanPageOrDirtyFromIndex(MemPageIndex* index);
  std::pair<char*, bool> getCleanPrimaryPage();
  std::pair<char*, bool> getCleanPrimaryPageWithFlag(bool isDirty);
  std::pair<char*, bool> getCleanIntermediateBasicPage();
  std::pair<char*, bool> getCleanIntermediatePage(
      FILE_CLASSIFIER fileClassifier = NOTCLASSIFIED);
  std::pair<char*, bool> getCleanIntermediatePageWithFlag(
      FILE_CLASSIFIER fileClassifier, bool isDirty);
  std::pair<char*, bool> getAFreePage(FileMode mode);

  MemPageIndex* prioritiseIndexes(FileMode mode, MemPageIndex* index1,
                                  MemPageIndex*& index2);
  FileMode getIndexMode(FileMode mode);

  int getFlagAlgorithm(FileMode mode);
  void updateSliderValue();
  // --------Optimised Versions ---------------
  char* evictPageAndAdaptToSlider(FileMode mode);
  char* evictPageByType(FileMode mode);
  char* evictPageByRankingClean(FileMode mode);
  char* evictPageByRanking(FileMode mode);
  char* evictPageByRankingRandom(FileMode mode);
  char* evictPageByRankingRandomClean(FileMode mode);
  char* evictPageCleanFirst();
  char* evictPageByTypeCleanFirst();
  char* evictLRUPage();

  template <typename Iterator>
  Iterator findCandidate(Iterator start, Iterator end) {
    Iterator it = start;
    while (it != end && page(it).isPinned()) it++;
    return it;
  }

  template <typename Iterator>
  Iterator findCandidateReverse(Iterator start, Iterator end) {
    Iterator it = end;
    it--;
    if (!page(it).isPinned()) {
      return it;
    }
    while (it != start && page(it).isPinned()) {
      it--;
    }
    return it;
  }

  std::pair<bool, mpi_by_dirtiness::const_iterator> findCandidate(
      MemPageIndex* i, bool v);

  mpi_by_time::const_iterator findCandidateTwoLists(MemPageIndex* i);

  void movePage(MemPageIndex* indexFirst, MemPageIndex* indexSecond);

 public:
  /**
   * Calculates statistics about dirty and clean pages for every category and
   * exports
   * them in different .txt files. Those files are used to generate a pie for
   * different
   *  timestamps of the workload
   */
  void printDirtyPagesForAllDataStructures();

  // Exports information about the bufferpool into different json files
  void printAllDirtyPagesDSTreeMap();

  // Get the total number of dirty pages in the bufferpool
  size_t dirtyPagesAll();

  // Get the number of dirty pages for a specific index
  size_t dirtyPagesForIndex(MemPageIndex* index);
  // Get the number of dirty pages for a specific index
  // and for a specific filename
  size_t dirtyPagesForIndex(MemPageIndex* index, const std::string& fileName);

  // Get the total number of dirty intermediate pages
  size_t dirtyPagesIntermediate();

  // Get the number of dirty pages for file category
  size_t dirtyPagesForCategory(FILE_CLASSIFIER fileClassifier);

  // Provide alternative way of finding total number of
  // dirty pages for lru and clean replacement policies
  size_t dirtyPagesForCategoryLruOrClean(FILE_CLASSIFIER fileClassifier);

  // Dirty pages for ranking algorithms (different indexes)
  size_t dirtyPagesForCategoryRanking(FILE_CLASSIFIER fileClassifier);

  // Get the number of dirty pages given the data structure key
  size_t dirtyPagesForDS(const table_key& tk);

  // Get number of dirty pages for a list of filenames
  size_t dirtyPagesForMultipleFiles(
      const FileMode& mode, const std::unordered_set<std::string>& filenames);

  // Get the number of dirty pages given the filename and its path
  size_t dirtyPagesForFile(const FileMode& mode, const std::string& fn);

 private:
  // Function that prints the current state of the bufferpool into a json file
  // that can be visually represented by a treemap-chart
  std::string printParentHeader();
  std::string printParent(const std::string& name, const std::string& kid1,
                          const std::string& kid2);
  std::string printParentLast(const std::string& name, const std::string& kid1,
                              const std::string& kid2);
  std::string printKid(const std::string& name, int numOfDirty, int numOfClean);
  std::string printLastKid(const std::string& name, int numOfDirty,
                           int numOfClean);
  std::string printUniqueKidLast(const std::string& filename, int count,
                                 const std::string& dirtiness);
  std::string printUniqueKid(const std::string& filename, int count,
                             const std::string& dirtiness);
  std::string printFooter();
  std::string splitFilename(const std::string& str);

  //  Return a pair with the name of the file and the number
  //  of clean or dirty pages for each file in the system
  std::pair<std::string, std::string> printPagesPerDataStructure(
      MemPageIndex* index, const std::string& mode);

  // Calculate clean and dirty pages for each file in the system
  std::pair<std::string, std::string> printPagesPerFileClassifier(
      FILE_CLASSIFIER fileClassifier);
};

}  //  namespace storage
}  //  namespace deceve

#endif
