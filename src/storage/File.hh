#ifndef FILE_HH
#define FILE_HH

#include <string>
#include <vector>
#include <iostream>

typedef int cost_t;
typedef unsigned long long offset_t;

namespace deceve { namespace storage {

class File {
private:
    std::string filename;    
    int filedesc;
    offset_t size;
  //  offset_t currentSize;
    
public:
    File(); 
    File(const std::string& fn);
    ~File();

    inline const std::string& getName() const { return filename; }
    inline int getFD() const { return filedesc; }
    //inline offset_t getSize() const { return size; }
    inline offset_t getSize() const { return getPhysicalSize(); }
 //   inline offset_t getCurrentSize() const {

  //     	std::cout<<"%%%%%%%%%%%%%%%%%%%%%%%%"<<"\n";

    //	return currentSize; }
    inline void updateSize(offset_t s) { size = s; }
    offset_t getPhysicalSize() const;
 //   void setCurrentSize(offset_t n) {currentSize = n;}
    friend std::ostream& operator << (std::ostream& os, const File& s);
};

}}

#endif
