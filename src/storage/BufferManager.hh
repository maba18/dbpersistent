#ifndef BUFFERMANAGER_HH
#define BUFFERMANAGER_HH

#include <iostream>
#include <vector>
#include <algorithm>

#include "../utils/types.hh"
#include "../utils/helpers.hh"
#include "../storage/BufferPool.hh"
#include "../storage/MemPage.hh"
#include "../storage/StatsManager.hh"
#include "../utils/global.hh"

#include <thread>
#include <mutex>
#include <atomic>

namespace deceve {
namespace storage {

class BufferManager {
 private:
  StorageManager sm;
  BufferPool bp;
  std::vector<std::pair<table_key, double> > scores;

 public:
  uint64_t bufferReadDelay;
  uint64_t bufferWriteDelay;
  deceve::profiling::Timer bmrWritePageTimer{3, "Bmr_WritePage"};
  deceve::profiling::Timer bmrReadPageTimer{1, "Bmr_ReadPage"};
  deceve::profiling::Timer bmrPinPageTimer{1, "Bmr_PinPage"};
  deceve::profiling::Timer bmrReleasePageTimer{1, "Bmr_ReleasePage"};

  int costCalculatorCase = {0};

 public:
  BufferManager(unsigned int pool_size_b)
      : sm(), bp(pool_size_b, &sm) {
    bufferReadDelay = 0;
    bufferWriteDelay = 0;
  }
  BufferManager(const std::string& s, unsigned int pool_size_b)
      : sm(s), bp(pool_size_b, &sm) {
    bufferReadDelay = 0;
    bufferWriteDelay = 0;
  }
  ~BufferManager() {}

  BufferPool& getBufferPool() { return bp; }
  StorageManager& getSM() { return sm; }
  StatsManager& getStatsManager() { return sm.getStatsManager(); }

  // basic operations on pages

  // return a pointer to the page starting at offset of the file
  const char* readPage(const std::string&, offset_t);
  const char* readPage(const std::string&, offset_t, FileMode fmode);

  // copy PAGE_SIZE bytes starting at data to the specified file
  // starting at the specified offset
  void writePage(const std::string&, offset_t, const char*);
  void writePage(const std::string&, offset_t, const char*, FileMode fmode);

  // add pin to page
  void pinPage(const std::string&, offset_t);
  void pinPage(const std::string&, offset_t, FileMode fmode);

  // release page
  void releasePage(const std::string&, offset_t);
  void releasePage(const std::string&, offset_t, FileMode fmode);

  // basic file operations
  void releaseFile(const std::string&);
  void removeFile(const std::string&);
  void closeFile(const std::string&);
  void renameFile(const std::string& filename, const std::string& n);

  // operations about filesizes
  // return the size of the file in bytes. If the file is not open,
  // open it
  offset_t getFileSize(const std::string&, FileMode mode = PRIMARY);
  offset_t getFileSizeFromCatalog(const std::string&, FileMode mode = PRIMARY);
  offset_t getFileSizeFromCatalogWithoutMode(const std::string&);
  FileMode getFileMode(const std::string& filename);

  // basis operations on data structures
  bool requestPersistence(const table_key& tk);
  void removePersistentFile(table_key tk);
  void removeCompletelyPersistentFile(table_key tk);
  void removeCompletelyAllPersistentFiles();

  // scoring operations on data structures
  double calculateOperatorCost(const table_key&);
  double calculateOperatorCostInternal(const table_key&);
  double calculateOperatorCostInternalSort(const table_key&);
  double calculateOperatorCostInternalHashJoin(const table_key&);
  double calculateTotalScore(const table_key&, bool isCandidate,
                             FILE_CLASSIFIER fileClassifier);
  double calculateFutureScore(const table_key&, bool isCandidate,
                              bool isWeightImportant);
  double calculateFutureScorePessimistic(const table_key&,
                                         bool isCandidate = false,
                                         bool isWeightImportant = true);

  double calculatePastScore(const table_key&, bool isCandidate);
  double calculateFullCostOfOperation(const table_key& tk);

  // utility function to calculate the number of writes
  // if the memory and file size are given
  double estimateDirtyWrites(double sizeOfProjectedFile,
                             double availableMemory);

  inline double getScoreForMemory(double fileSize, double memory);

  bool isInIndex(const std::string&, offset_t, FileMode fmode);

  // Estimations about the number of writes for
  // different replacement polocies
  int getEstimateForLruWrites(int M, int F);
  int getEstimateForLruReads(int M, int F);
  int getEstimateForRankedBasedWrites(int M, int F);
  int getEstimateForRankedBasedReads(int M, int F);
  int getEstimateForClruWrites(int M, int F);
  int getEstimateForClruReads(int M, int F);

  // utility functions
  bool isDatastructure(const deceve::storage::table_key& tk);
  bool isMaterialised(const deceve::storage::table_key& tk);
  bool isBeingGenerated(const deceve::storage::table_key& tk);

  std::string getAlgorithmToken(int alg) {
    std::stringstream s;
    switch (alg) {
      case 1:
        return "LRU";
      case 2:
        return "CLRU-2";
      case 3:
        if (getBufferPool().algorithm3Case == 0) {
          return "CLRU-RANKED";
        } else if (getBufferPool().algorithm3Case == 1) {
          return "DRAM";
        } else {
          return "NVRAM";
        }
      case 4:
        return "CLRU";
      case 6:
        return "RAN";
      case 7:
        return "RAN-CLEAN";
      case 8:
        return "RAN-CLEAN-AD";
      case 10:
        return "LRU-AD";
      case 15:
        return "AD";
      default:
        s << "algo:" << alg << "";
        return s.str();
    }
  }
};
}
}

#endif
