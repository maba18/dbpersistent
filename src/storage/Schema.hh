#ifndef SCHEMA_HH
#define SCHEMA_HH

#include <vector>
#include <set>
#include <boost/operators.hpp>

namespace deceve { namespace storage {

enum length_t {
  FIXED,
  VARIABLE
};

enum type_t {
  SHORT,
  UNSIGNED_SHORT,
  INTEGER,
  UNSIGNED_INTEGER,
  LONG,
  UNSIGNED_LONG,
  FLOAT,
  DOUBLE,
  CHAR,
  STRING
};

std::ostream& operator<<(std::ostream& os, const type_t& t) {
  switch (t) {
    case SHORT: os << "short"; break;
    case UNSIGNED_SHORT: os << "unsigned short"; break;
    case INTEGER: os << "int"; break;
    case UNSIGNED_INTEGER: os << "unsigned int"; break;
    case LONG: os << "long"; break;
    case UNSIGNED_LONG: os << "unsigned long"; break;
    case FLOAT: os << "float"; break;
    case DOUBLE: os << "double"; break;
    case CHAR: os << "char"; break;
    case STRING: os << "string"; break;
  }
  return os;
}

size_t size(const type_t& t) {
  switch (t) {
    case SHORT: return sizeof(short);
    case UNSIGNED_SHORT: return sizeof(unsigned short);
    case INTEGER: return sizeof(int);
    case UNSIGNED_INTEGER: return sizeof(unsigned int);
    case LONG: return sizeof(long);
    case UNSIGNED_LONG: return sizeof(unsigned long);
    case FLOAT: return sizeof(float);
    case DOUBLE: return sizeof(double);
    case CHAR: return sizeof(char);
    case STRING: return sizeof(unsigned long);
  }
  return 0;
}

struct Field : public boost::totally_ordered<Field> {
  std::string name;
  type_t type;
  length_t length;
  
  Field(const std::string& n,
        const type_t& t)
    : name(n), type(t), length(type == STRING ? VARIABLE : FIXED)
  {}
  
  bool operator==(const Field& f) const
  { return name == f.name && type == f.type; }
  bool operator<(const Field& f) const
  { return (name == f.name) ? type < f.type : name < f.name; }
};

class Schema {
  friend std::ostream& operator<<(std::ostream& os, const Schema& s);
public:
  Schema(): m_fields() {}
  ~Schema() {}

  void add(const Field& f) { 
    m_fields.push_back(f); 
  }
  
  void merge(const Schema& to_merge) {
    std::copy(to_merge.m_fields.begin(), to_merge.m_fields.end(),
              std::back_inserter(m_fields));
  }

  size_t length() const { return m_fields.size(); }

  size_t size() const {
    size_t len = 0;
    for (std::vector<Field>::const_iterator it = m_fields.begin();
         it != m_fields.end(); it++) {
        len += deceve::storage::size(it->type);
    }
    return len;
  }
  
private:    
  std::vector<Field> m_fields;
};

std::ostream& operator<<(std::ostream& os, const Schema& s) {
    std::vector<Field>::const_iterator it = s.m_fields.begin();    
    os << "{" << it->name << ": " << it->type;
    it++;
    for (; it != s.m_fields.end(); it++)
        os << ", " << it->name << ": " << it->type;
    os << " (" << s.size() << " bytes)}";
    return os;
}

}}

#endif
