#include <iostream>
#include <fstream>
#include <string>
#include <fcntl.h>
#include <sys/stat.h>  // required for file size

#include "../utils/types.hh"
#include "../utils/helpers.hh"
#include "../storage/File.hh"

using namespace std;

namespace deceve {
namespace storage {

File::File() : filename(""), filedesc(-1), size(0) {}

File::File(const string& fn) : filename(fn), filedesc(-1), size(0) {
#ifdef _linux
  require((filedesc = open(fn.c_str(), O_RDWR | O_CREAT | O_DIRECT, 0644)) > 0,
          "Could not open file1 " + fn);
#else
  require((filedesc = open(fn.c_str(), O_RDWR | O_CREAT, 0644)) > 0,
          "Could not open file2 " + fn);
#endif
#if __APPLE__ & __MACH__
  require((fcntl(filedesc, F_NOCACHE, 1) >= 0), "Could not bypass cache " + fn);
#endif
}

offset_t File::getPhysicalSize() const {
  struct stat result;
  if (fstat(filedesc, &result) == 0)
    return result.st_size;
  else
    std::cout << "Error while trying to get file size"
              << "\n";
  return 0;
}

File::~File() { close(filedesc); }

ostream& operator<<(ostream& os, const File& f) {
  return os << f.filename << " " << f.filedesc << "\n";
}
}
}
