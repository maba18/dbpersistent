/*******************************************************************************
 * Copyright (c) 2015 Michail Basios and Stratis Viglas.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michail Basios - initial API and implementation
 *     Stratis Viglas - initial API and implementation
 *******************************************************************************/
#include "../storage/BufferManager.hh"

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>

#include <cstdio>
#include <iomanip>
#include <iostream>
#include <string>
#include <chrono>

#include "../storage/BufferPool.hh"
#include "../storage/StorageManager.hh"
#include "../utils/types.hh"
#include "../storage/page.hh"
#include "../utils/global.hh"
#include "../utils/defs.hh"

// using namespace std;

namespace deceve {
namespace storage {

const char* BufferManager::readPage(const std::string& filename,
                                    offset_t offset) {
#ifdef MULTI_THREADED
  std::lock_guard<std::mutex> lock(getSM().bufferManagerOperationMutex);
#endif
  assert(offset % PAGE_SIZE_PERSISTENT == 0 &&
         " Page requested is not PAGE_SIZE_PERSISTENT - aligned. (1)");
  FileMode fMode = sm.getFileMode(filename);
  MemPage& pg = bp.fetchPage(FilePos(filename, offset), fMode);
  return pg.getData();
}

const char* BufferManager::readPage(const std::string& filename,
                                    offset_t offset, FileMode fmode) {
#ifdef MULTI_THREADED
  std::lock_guard<std::mutex> lock(getSM().bufferManagerOperationMutex);
#endif
  assert(offset % PAGE_SIZE_PERSISTENT == 0 &&
         " Page requested is not PAGE_SIZE_PERSISTENT - aligned. (1)");
#ifdef PROFILING
  bmrReadPageTimer.start();
#endif

  MemPage& pg = bp.fetchPage(FilePos(filename, offset), fmode);

#ifdef PROFILING
  bmrReadPageTimer.end(0);
#endif
  return pg.getData();
}

void BufferManager::writePage(const std::string& filename, offset_t offset,
                              const char* data) {
#ifdef MULTI_THREADED
  std::lock_guard<std::mutex> lock(getSM().bufferManagerOperationMutex);
#endif

  assert(offset % PAGE_SIZE_PERSISTENT == 0 &&
         "Page requested is not PAGE_SIZE_PERSISTENT - aligned. (2)");
  if (offset + PAGE_SIZE_PERSISTENT > sm.getFileSizeFromCatalog(filename)) {
    sm.updateFileSize(filename, offset + PAGE_SIZE_PERSISTENT);
  }
  FileMode fMode = sm.getFileMode(filename);
  MemPage& pg = bp.fetchPage(FilePos(filename, offset), fMode);
  if (data != pg.getData()) {
    ::memcpy(pg.getData(), data, PAGE_SIZE_PERSISTENT);
  }
  pg.setDirty(true);
}

void BufferManager::writePage(const std::string& filename, offset_t offset,
                              const char* data, FileMode fmode) {
#ifdef MULTI_THREADED
  std::lock_guard<std::mutex> lock(getSM().bufferManagerOperationMutex);
#endif

// ################### SECTION 1 #####################

#ifdef PROFILING
  bmrWritePageTimer.start();
#endif

  assert(offset % PAGE_SIZE_PERSISTENT == 0 &&
         "Page requested is not PAGE_SIZE_PERSISTENT - aligned. (2)");

  if (offset + PAGE_SIZE_PERSISTENT >
      sm.getFileSizeFromCatalog(filename, fmode)) {
    sm.updateFileSize(filename, offset + PAGE_SIZE_PERSISTENT);
  }

#ifdef PROFILING
  bmrWritePageTimer.end(0);
#endif

// ################### SECTION 2 #####################
#ifdef PROFILING
  bmrWritePageTimer.start();
#endif

  MemPage& pg = bp.fetchPage(FilePos(filename, offset), fmode);

#ifdef PROFILING
  bmrWritePageTimer.end(1);
#endif

// ################### SECTION 3 #####################
#ifdef PROFILING
  bmrWritePageTimer.start();
#endif

  if (data != pg.getData()) {
    ::memcpy(pg.getData(), data, PAGE_SIZE_PERSISTENT);
  }
  pg.setDirty(true);

#ifdef PROFILING
  bmrWritePageTimer.end(2);
#endif
}

void BufferManager::pinPage(const std::string& filename, offset_t offset) {
#ifdef MULTI_THREADED
  std::lock_guard<std::mutex> lock(getSM().bufferManagerOperationMutex);
#endif
  assert(offset % PAGE_SIZE_PERSISTENT == 0 &&
         "Page requested is not PAGE_SIZE_PERSISTENT - aligned. (3)");
  bp.pinPage(FilePos(filename, offset));
}

void BufferManager::pinPage(const std::string& filename, offset_t offset,
                            FileMode fmode) {
#ifdef MULTI_THREADED
  std::lock_guard<std::mutex> lock(getSM().bufferManagerOperationMutex);
#endif
  assert(offset % PAGE_SIZE_PERSISTENT == 0 &&
         "Page requested is not PAGE_SIZE_PERSISTENT - aligned. (3)");

#ifdef PROFILING
  bmrPinPageTimer.start();
#endif

  bp.pinPage(FilePos(filename, offset), fmode);

#ifdef PROFILING
  bmrPinPageTimer.end(0);
#endif
}

void BufferManager::releasePage(const std::string& filename, offset_t offset) {
#ifdef MULTI_THREADED
  std::lock_guard<std::mutex> lock(getSM().bufferManagerOperationMutex);
#endif
  assert(offset % PAGE_SIZE_PERSISTENT == 0 &&
         "Page requested is not PAGE_SIZE_PERSISTENT - aligned. (4)");
  bp.releasePage(FilePos(filename, offset));
}

void BufferManager::releasePage(const std::string& filename, offset_t offset,
                                FileMode fmode) {
#ifdef MULTI_THREADED
  std::lock_guard<std::mutex> lock(getSM().bufferManagerOperationMutex);
#endif
  assert(offset % PAGE_SIZE_PERSISTENT == 0 &&
         "Page requested is not PAGE_SIZE_PERSISTENT - aligned. (4)");

#ifdef PROFILING
  bmrReleasePageTimer.start();
#endif

  bp.releasePage(FilePos(filename, offset), fmode);

#ifdef PROFILING
  bmrReleasePageTimer.end(0);
#endif
}

// void BufferManager::releaseFile(const std::string& filename) {
// bp.releaseFilePages(filename);
//}

void BufferManager::removeFile(const std::string& filename) {
#ifdef MULTI_THREADED
  std::lock_guard<std::mutex> lock(getSM().bufferManagerOperationMutex);
#endif
  bp.invalidateFilePages(filename);
  return sm.removeFile(filename);
}

void BufferManager::closeFile(const std::string& filename) {
#ifdef MULTI_THREADED
  std::lock_guard<std::mutex> lock(getSM().bufferManagerOperationMutex);
#endif
  sm.closeFile(filename);
}

void BufferManager::renameFile(const std::string& filename,
                               const std::string& n) {
#ifdef MULTI_THREADED
  std::lock_guard<std::mutex> lock(getSM().bufferManagerOperationMutex);
#endif
  // change the file name for the buffer pool pages
  bp.changeFileNameOnBufferPoolPages(filename, n);
  // make all the remaining necessary file changes
  return sm.renameFile(filename, n);
}

offset_t BufferManager::getFileSize(const std::string& filename,
                                    FileMode mode) {
#ifdef MULTI_THREADED
  std::lock_guard<std::mutex> lock(getSM().bufferManagerOperationMutex);
#endif
  return sm.getFileSizeFromFileSystem(filename, mode);
}

offset_t BufferManager::getFileSizeFromCatalog(const std::string& filename,
                                               FileMode mode) {
#ifdef MULTI_THREADED
  std::lock_guard<std::mutex> lock(getSM().bufferManagerOperationMutex);
#endif

  return sm.getFileSizeFromCatalog(filename, mode);
}

offset_t BufferManager::getFileSizeFromCatalogWithoutMode(
    const std::string& filename) {
#ifdef MULTI_THREADED
  std::lock_guard<std::mutex> lock(getSM().bufferManagerOperationMutex);
#endif

  return sm.getFileSizeFromCatalog(filename, getFileMode(filename));
}

FileMode BufferManager::getFileMode(const std::string& filename) {
#ifdef MULTI_THREADED
  std::lock_guard<std::mutex> lock(getSM().bufferManagerOperationMutex);
#endif
  return sm.getFileModeFromCatalog(filename);
}

void BufferManager::removeCompletelyPersistentFile(table_key tk) {
#ifdef MULTI_THREADED
  std::lock_guard<std::mutex> lock(getSM().bufferManagerOperationMutex);
#endif
  // global_table_map::iterator it = sm.persisted_file_catalog.find(tk);
  global_table_map::iterator it = sm.getIterOfPersistedFileCatalog(tk);

  // Remove pages from the second index
  bp.invalidateFilePages(tk);

  if (sm.isInPersistedFileCatalog(tk)) {
    // remove sorted version of the file
    if (tk.version == SORT) {
      std::stringstream filename;
      filename << it->second.output_name << it->second.full_output_path
               << "_persistent";

      bp.invalidateFilePages(filename.str());
      sm.removeFile(filename.str());
      sm.deleteFromPersistedFileCatalog(it);

    } else if (tk.version == HASHJOIN) {
      // COUT << "remove hash version of the file: " << tk << "\n";

      std::stringstream baseName;
      baseName << it->second.output_name << it->second.full_output_path
               << "_persistent";
      sm.removeFile(baseName.str());

      for (size_t i = 0; i < it->second.num_files; i++) {
        std::stringstream filename;
        filename << it->second.output_name << it->second.full_output_path
                 << "_persistent." << i;
        bp.invalidateFilePages(filename.str());
        sm.removeFile(filename.str());
      }
      sm.deleteFromPersistedFileCatalog(it);
    }
  }
}

void BufferManager::removeCompletelyAllPersistentFiles() {
  //  std::cout << "Remove all persisted remaining files" << std::endl;
  for (auto it = sm.getIteratorPersistedCatalogBegin();
       it != sm.getIteratorPersistedCatalogEnd(); ++it) {
    removeCompletelyPersistentFile(it->first);
  }
}

double BufferManager::calculateOperatorCost(const table_key& tk) {
  return calculateOperatorCostInternal(tk);
}

double BufferManager::calculateOperatorCostInternal(const table_key& tk) {
  //  std::cout << "-------------- Calculate InternalCost for: " << tk
  //            << "------------------ \n";

  //  double calculatedScore = sm.getInternalOperationCost(tk);
  //  // Check if score is already calculated
  //  if (calculatedScore != -1) {
  //    std::cout << "calculateOperatorCostInternal: (already calculated) score:
  //    "
  //              << calculatedScore << std::endl;
  //    return calculatedScore;
  //  }

  if (tk.version == SORT) {
    return calculateOperatorCostInternalSort(tk);
  } else if (tk.version == HASHJOIN) {
    return calculateOperatorCostInternalHashJoin(tk);
  }

  return 0.0;
}

double BufferManager::calculateOperatorCostInternalSort(const table_key& tk) {
  double readDelayFactor = static_cast<double>(getSM().getStorageReadDelay());
  double writeDelayFactor = static_cast<double>(getSM().getStorageWriteDelay());

  //  auto tableNameIter = sm.tableNameMapper.find(tk.table_name);
  //  assert(tableNameIter != sm.tableNameMapper.end() &&
  //         "BufferManager tableNameIter not found");

  size_t fileSize = sm.getPrimaryFileSizeInPagesForRelation(tk);
  size_t fileSizeProjected =
      sm.getPrimaryFileSizeInPagesForProjectedRelation(tk);

  double finalScore = 0;

  // double operation_cost = 0;
  // double readCost = 0, writeCost = 0;
  // check if operation appeared before
  if (getStatsManager().didTheOperationAppear(tk)) {
    long writesBasedOnStatistics = getStatsManager().getAverageWritesForTk(tk);

    double newWriteCost = writeDelayFactor * writesBasedOnStatistics;

    double newReadCost =
        writesBasedOnStatistics * readDelayFactor + readDelayFactor * fileSize;

    finalScore = newReadCost + newWriteCost;
  } else {
    //    double M = 0.0;
    //
    //    double scoreFactor = 0.0;
    //    double memoryForThisOperator =
    //        bp.numberOfPrimaryPages() / (sm.getNumberOfThreads() + 1);
    //
    //    if (fileSizeProjected < memoryForThisOperator) {
    //      scoreFactor = 0;
    //    } else {
    //      scoreFactor = fileSizeProjected - memoryForThisOperator;
    //    }

    double M = bp.numberOfPrimaryPages();

    double logOfT = 0;
    if (M > 1) {
      logOfT = ceil(log(ceil(fileSizeProjected / M)) / log(M - 1));
    } else {
      logOfT = 0;
    }

    double numberOfPasses = 1 + logOfT;

    // Multiple it with a factor of 1.5 as sorting is more expensive then
    // hash
    //    double numberOfWritesInPersistent =
    //        getEstimateForLruWrites(memoryForThisOperator, filesizeProjected);
    //    double numberOfReadsInPersistent =
    //        getEstimateForLruReads(memoryForThisOperator, filesizeProjected);
    //
    //    readCost = readDelayFactor * filesize;
    //    //    writeCost = 10 * writeDelayFactor * filesizeProjected *
    //    //    numberOfPasses;
    //    writeCost =
    //        writeDelayFactor * (1 + numberOfWritesInPersistent) *
    //        numberOfPasses;

    //    std::cout << "numberOfPasses: " << numberOfPasses << std::endl;
    finalScore = readDelayFactor * fileSize +
                 numberOfPasses * writeDelayFactor * fileSizeProjected +
                 numberOfPasses * readDelayFactor * fileSizeProjected;
  }

  //  std::cout << "calculateOperatorCostInternal (SORT): TOTAL COST OF SORT "
  //               "OPERATION: "
  //            << std::fixed << finalScore << std::endl;

  return finalScore;
}

double BufferManager::calculateOperatorCostInternalHashJoin(
    const table_key& tk) {
  double readDelayFactor = static_cast<double>(getSM().getStorageReadDelay());
  double writeDelayFactor = static_cast<double>(getSM().getStorageWriteDelay());

  //  auto tableNameIter = sm.tableNameMapper.find(tk.table_name);
  //  assert(tableNameIter != sm.tableNameMapper.end() &&
  //         "BufferManager tableNameIter not found");

  double filesize =
      static_cast<double>(sm.getPrimaryFileSizeInPagesForRelation(tk));
  double filesizeProjected =
      static_cast<double>(sm.getPrimaryFileSizeInPagesForProjectedRelation(tk));

  double finalScore = 0.0;

  //  std::cout << "calculateOperatorCostInternalHashJoin" << std::endl;
  // check if operation appeared before
  if (getStatsManager().didTheOperationAppear(tk)) {
    long writesBasedOnStatistics = getStatsManager().getAverageWritesForTk(tk);

    double newWriteCost = writeDelayFactor * writesBasedOnStatistics;

    double newReadCost =
        writesBasedOnStatistics * readDelayFactor + readDelayFactor * filesize;

    //    double costWithoutDs =
    //        readDelayFactor * filesize + newWriteCost + newReadCost;
    //
    //    double costWithDs =
    //        readDelayFactor * filesizeProjected +
    //        (filesizeProjected - writesBasedOnStatistics) * writeDelayFactor;

    finalScore = newReadCost + newWriteCost;
    //
    //    std::cout << "writesBasedOnStatistics: " << writesBasedOnStatistics
    //              << std::endl;
    //    std::cout << "newWriteCost: " << newWriteCost << std::endl;
    //    std::cout << "newReadCost: " << newReadCost << std::endl;
    //    std::cout << "costWithoutDs: " << costWithoutDs << std::endl;
    //    std::cout << "costWithDs: " << costWithDs << std::endl;
  } else {
    // CHANGEME remember to change this. Experimental only
    // pessimistic approach, assume that the generated intermediate file will be
    // written

    // example: 100 memory pages, 150 filesize pages
    // 50 pages are going to be written for sure
    double availableMemory = static_cast<double>(bp.getPoolSize());

    double amountOfPagesToBeWritten = 0;
    if (availableMemory < filesizeProjected) {
      amountOfPagesToBeWritten = filesizeProjected - availableMemory;
    }

    finalScore = readDelayFactor * filesize +
                 writeDelayFactor * amountOfPagesToBeWritten +
                 readDelayFactor * amountOfPagesToBeWritten;
  }

  //  std::cout << "finalScore: " << finalScore << std::endl;
  //  std::cout << "filesize: " << filesize << std::endl;
  //  std::cout << "filesizeProjected: " << filesizeProjected << std::endl;

  //  sm.setInternalOperationCost(tk, finalScore);

  return finalScore;
}
// @HOT calculateTotalScore
double BufferManager::calculateTotalScore(const table_key& tk, bool isCandidate,
                                          FILE_CLASSIFIER fileClassifier) {
  if (fileClassifier == PERMANENT) {
    return calculatePastScore(tk, isCandidate);
  } else if (fileClassifier == TEMPORAL) {
    return calculateFutureScore(tk, isCandidate, true);
  }
  return 0.0;
}

// This is the Cost function
double BufferManager::calculateFutureScore(const table_key& tk,
                                           bool isCandidate,
                                           bool isWeightImportant) {
  return calculateFutureScorePessimistic(tk, isCandidate, isWeightImportant);

  int n = sm.getTotalNumberOfReferences();  // returns n in the score function
  if (n == 0) {
    // std::cout << "No queries in the future that have some interesting
    // primary
    // versions of tables. SCORE=0  \n";
    return 0.0;  // No queries in the future that have some interesting
                 // primary
                 // versions of tables
  }

  // std::cout << "-------- Calculate future score BEGIN -----------" << tk <<
  // "
  // candidate:" << isCandidate << " \n";
  auto futureReferencesIter = sm.getIterOfPersistedFutureFileReferences(tk);

  // Check if there are no references for this data structure in the future
  if (futureReferencesIter == sm.getIteratorFutureFileReferencesEnd()) {
    //    std::cout << "calculateFutureScore: No future references for the
    //    current "
    //                 "workload. SCORE=0 \n";
    if (isCandidate) {
      //   std::cout << " ISCANDIDATE: No future references for the
      //   current "
      //               "workload. SCORE=0 \n";
      return 0.0;
    } else {
      double operatorCost = calculateOperatorCost(tk);
      //      std::cout
      //          << "calculateFutureScore: NEW CANDIDATE operatorCostInteranl
      //          for : "
      //          << tk << " " << operatorCost << std::endl;
      double numberOfDirtyPages = 0.0;
      if (sm.isInPersistedTmpFileSet(tk)) {
        //      std::cout << "DATA STRUCTURE NOT READY YET: " << tk << "
        //      dirty
        //      pages: 0"
        //               << std::endl;
        numberOfDirtyPages = 0.0;
      } else {
        numberOfDirtyPages = bp.dirtyPagesForDS(tk);
        //        std::cout
        //            << "calculateFutureScore: Candidate Number of dirty pages
        //            for tk: "
        //            << tk << " is: " << numberOfDirtyPages << std::endl;
      }
      //    std::cout << "Just return operator cost"
      //             << operatorCost - numberOfDirtyPages << std::endl;
      double totalFutureScore =
          operatorCost -
          numberOfDirtyPages *
              static_cast<double>(getSM().getStorageWriteDelay());

      //      std::cout << "Total future score: " << tk
      //                << " isCandidate: " << isCandidate
      //                << " isWeightImportant: " << isWeightImportant
      //                << " score: " << totalFutureScore << std::endl;

      return totalFutureScore;
    }
  }

  size_t numberOfReferencesForOperator = 0;

  if (futureReferencesIter->second.size() > 0)
    numberOfReferencesForOperator = futureReferencesIter->second.size();
  //  std::cout << "calculateFutureScore: numberOfReferencesForOperator bigger "
  //               "than zero:: "
  //            << tk << " " << numberOfReferencesForOperator << std::endl;

  if (numberOfReferencesForOperator > (size_t)sm.getQueueDepth() + 10) {
    numberOfReferencesForOperator = 0;
    // std::cout << "queueSize zero: " << std::endl;
  }

  // HOT: Check well the cost functions
  // Cache cost and not calculate each time.
  //  std::pair<double, double> costs = calculateOperatorCostInternal(tk);
  //
  //  double readCost = costs.first;
  //  double writeCost = costs.second;
  double operatorCost = calculateOperatorCost(tk);
  double costToBeGainedFromFutureReferences =
      static_cast<double>(numberOfReferencesForOperator) * operatorCost;
  double costThatMayBePaid = 0.0;

  //  std::cout << "calculateFutureScore: calculateOperatorCostInternal:: for
  //  tk: "
  //            << tk << " " << operatorCost << std::endl;
  //  std::cout << "calculateFutureScore: numberOfReferencesForOperator::"
  //            << numberOfReferencesForOperator << std::endl;
  //  std::cout << "calculateFutureScore: costToBeGainedFromFutureReferences: "
  //            << costToBeGainedFromFutureReferences << std::endl;

  if (isCandidate) {
    // auto tableNameIter = sm.tableNameMapper.find(tk.table_name);
    // assert(tableNameIter != sm.tableNameMapper.end() &&
    //        "BufferManager tableNameIter not found");

    //    sm.getFileSizeInPagesFromCatalog(
    //        deceve::queries::getFullPath(tableNameIter->second));
    size_t fileSizeInPages =
        sm.getPrimaryFileSizeInPagesForProjectedRelation(tk);

    double memoryForCurrentOperator =
        bp.numberOfPrimaryPages() / (sm.getNumberOfThreads());

    if (fileSizeInPages < memoryForCurrentOperator) {
      memoryForCurrentOperator = static_cast<double>(fileSizeInPages);
    }

    costThatMayBePaid = memoryForCurrentOperator *
                        static_cast<double>(getSM().getStorageWriteDelay());

    //    score =
    //        costToBeGainedFromFutureReferences + costToBePaid -
    //        memoryForCurrentOperator *
    //    (static_cast<double>(getStorageManager().getStorageWriteDelay()));

    // PRINT SECTION
    //    std::cout << "---------- IS-Candidate--------------- " << tk <<
    //    std::endl;
    //    std::cout << "calculateFutureScore: fileSizeInPages: " <<
    //    fileSizeInPages
    //              << std::endl;
    //    std::cout << "calculateFutureScore: queueReferences: "
    //              << numberOfReferencesForOperator << std::endl;
    //    std::cout << "calculateFutureScore: memoryForEachOperator: "
    //              << memoryForCurrentOperator << std::endl;
    //    std::cout << "calculateFutureScore: operatorCost: " << operatorCost
    //              << std::endl;
  } else {
    auto fit = sm.getIterOfPersistedFileCatalog(tk);
    //        std::cout << "---------- Not-Candidate---------------" << tk
    //        <<
    //        std::endl;
    int numberOfDirtyPages = 0;
    if (fit != sm.getIteratorPersistedCatalogEnd()) {
      if (sm.isInPersistedTmpFileSet(tk)) {
        //  std::cout << "DATA STRUCTURE NOT READY YET: " << tk << "
        //  dirty
        //  pages: 0"
        //           << std::endl;
        numberOfDirtyPages = 0;
      } else {
        numberOfDirtyPages = bp.dirtyPagesForDS(tk);
        //           std::cout << "Number of dirty pages for tk: " << tk
        //                    << " is: " << numberOfDirtyPages <<
        //                    std::endl;
      }
    }
    costToBeGainedFromFutureReferences =
        costToBeGainedFromFutureReferences + operatorCost;

    costThatMayBePaid = (numberOfDirtyPages) *
                        static_cast<double>(getSM().getStorageWriteDelay());

    //    score = costToBeGainedFromFutureReferences + operatorCost -
    //            (numberOfDirtyPages) *
    //    static_cast<double>(getStorageManager().getStorageWriteDelay());

    // PRINT SECTION
    //    std::cout << "---------- NOT-Candidate--------------- " << tk <<
    //    std::endl;
    //    std::cout << "calculateFutureScore: queueReferences: "
    //              << numberOfReferencesForOperator << std::endl;
    //    std::cout << "calculateFutureScore: numberOfDirtyPages: "
    //              << numberOfDirtyPages << std::endl;
    //    std::cout << "calculateFutureScore: operatorCost: " << operatorCost
    //              << std::endl;
    //    std::cout << "calculateFutureScore:
    //    costToBeGainedFromFutureReferences: "
    //              << costToBeGainedFromFutureReferences << std::endl;
  }

  // sm.printPersistentFutureFileReferences();
  double weightFactor = 0;
  for (auto vectorIt = futureReferencesIter->second.begin();
       vectorIt != futureReferencesIter->second.end(); ++vectorIt) {
    double weight =
        1.0 - (static_cast<double>(vectorIt->first) / static_cast<double>(n));
    //   std::cout << "static_cast<double>(vectorIt->first): "
    //           << static_cast<double>(vectorIt->first)
    //         << " vs n= " << static_cast<double>(n) << std::endl;
    // std::cout << "weight: " << weight << std::endl;
    weightFactor += weight;
  }

  // std::cout << "Final weightFactor: " << weightFactor << "\n";

  //  for (auto vectorIt = rit->second.begin(); vectorIt != rit->second.end();
  //       ++vectorIt) {
  //    double weight = (double)vectorIt->first / (double)n;
  //
  //    score +=
  //        (1.0 - weight) * operatorCost - numberOfDirtyPages * (1 - (1 -
  //        weight));
  //    //
  //    COUT << "positive weight: " << (1.0 - (double)vectorIt->first /
  //    (double)n)
  //         << "\n";
  //    COUT << "negative weight: "
  //         << 1.0 - (1.0 - (double)vectorIt->first / (double)n) << "\n";
  //  }
  //
  //  calculatePastScore(tk);

  // return weightFactor * score

  // std::cout << "weightFactor * score " << weightFactor * score <<
  // std::endl;
  // std::cout << "-------- Calculate future score END ----------- \n";

  double finalScore = costToBeGainedFromFutureReferences - costThatMayBePaid;

  //  std::cout << "costToBeGainedFromFutureReferences: " <<
  //  costToBeGainedFromFutureReferences << std::endl;
  //  std::cout << "costThatMayBePaid: " << costThatMayBePaid << std::endl;
  //  std::cout << "finalScore: " << finalScore << std::endl;

  weightFactor =
      weightFactor +
      1;  // If weightFactor zero I don't want to elliminate the score

  //  std::cout<<"weightFactor: "<<weightFactor<<std::endl;
  if (isWeightImportant) {
    //      std::cout << "Final Score with(weight Factor) is: "
    //              << weightFactor * finalScore << std::endl;
    double totalFutureScore = weightFactor * finalScore;

    //    std::cout << "Total future score: " << tk << " isCandidate: " <<
    //    isCandidate
    //              << " isWeightImportant: " << isWeightImportant
    //              << " score: " << totalFutureScore << std::endl;

    return totalFutureScore;
  }
  //   std::cout << "Final Score without weight is: " << weightFactor *
  //   finalScore
  //           << std::endl;

  //  std::cout << "Total future score: " << tk << " isCandidate: " <<
  //  isCandidate
  //            << " isWeightImportant: " << isWeightImportant
  //            << " score: " << finalScore << std::endl;
  return finalScore;
}

/*
 *  Function that will be used is the following
 *
 *     score for materialised relation:
 *     S_materialised = (n-1)*r(R) - dirtyPages(R’) -(n-1)*r(R') > 0
 *     score for Candidate:
 *     S_candidate = (n-1)*r(R) + - w(R') - (n-1)*r(R') > 0
 *     Need to calculate:
 *     n           : number of future occurrences
 *     R           : size of relation
 *     R'          : size of projected relation
 *     dirtyPages  : number of dirty pages of materialised data structure
 *
 */
double BufferManager::calculateFutureScorePessimistic(const table_key& tk,
                                                      bool isCandidate,
                                                      bool isWeightImportant) {
  int totalReferences =
      sm.getTotalNumberOfReferences();  // returns n in the score function
  if (totalReferences == 0) {
    return 0.0;  // No queries in the future that have some interesting
                 // primary
                 // versions of tables
  }

  auto futureReferencesIter = sm.getIterOfPersistedFutureFileReferences(tk);

  // Check if there are no references for this data structure in the future
  if (futureReferencesIter == sm.getIteratorFutureFileReferencesEnd()) {
    //    std::cout << "calculateFutureScore: No future references for the
    //    current "
    //                 "workload. SCORE=0 \n";
    if (isCandidate) {
      return 0.0;
    } else {
      double numberOfDirtyPages = 0.0;
      if (sm.isInPersistedTmpFileSet(tk)) {
        numberOfDirtyPages = 0.0;
      } else {
        numberOfDirtyPages = bp.dirtyPagesForDS(tk);
        // std::cout
        //     << "calculateFutureScore: Candidate Number of dirty pages for tk:
        //     "
        //     << tk << " is: " << numberOfDirtyPages << std::endl;
      }

      //      double fileSizeInPages =
      //          static_cast<double>(sm.getPrimaryFileSizeInPagesForRelation(tk));
      double costOfOperation = calculateOperatorCost(tk);
      double totalFutureScore =
          costOfOperation -
          numberOfDirtyPages *
              static_cast<double>(getSM().getStorageWriteDelay());
      return totalFutureScore;
    }
  }

  size_t numberOfReferencesForOperator = 0;

  if (futureReferencesIter->second.size() > 0) {
    numberOfReferencesForOperator = futureReferencesIter->second.size();

    //    std::cout << "calculateFutureScore: numberOfReferencesForOperator
    //    bigger "
    //                 "than zero:: "
    //              << tk << " " << numberOfReferencesForOperator << std::endl;
  }
  // a stupid check for some exception that was thrown
  if (numberOfReferencesForOperator > (size_t)sm.getQueueDepth() + 10) {
    numberOfReferencesForOperator = 0;
  }

  double fileSize =
      static_cast<double>(sm.getPrimaryFileSizeInPagesForRelation(tk));
  double fileSizeProjected =
      static_cast<double>(sm.getPrimaryFileSizeInPagesForProjectedRelation(tk));

  double costOfWritingProjectedRelation = 0.0;

  double costOfOperation = calculateOperatorCost(tk);

  double writesBasedOnStatistics = 0;
  if (getStatsManager().didTheOperationAppear(tk)) {
    writesBasedOnStatistics = getStatsManager().getAverageWritesForTk(tk);
  } else {
    double availableMemory = static_cast<double>(bp.getPoolSize());
    if (availableMemory < fileSizeProjected) {
      writesBasedOnStatistics = fileSizeProjected - availableMemory;
    }
  }

  std::pair<double, double> resultCosts;

  if (isCandidate) {
    // auto tableNameIter = sm.tableNameMapper.find(tk.table_name);
    // assert(tableNameIter != sm.tableNameMapper.end() &&
    //        "BufferManager tableNameIter not found");

    costOfWritingProjectedRelation =
        (fileSizeProjected - writesBasedOnStatistics) *
        static_cast<double>(getSM().getStorageWriteDelay());

  } else {
    auto fit = sm.getIterOfPersistedFileCatalog(tk);

    int numberOfDirtyPages = 0;
    if (fit != sm.getIteratorPersistedCatalogEnd()) {
      if (sm.isInPersistedTmpFileSet(tk)) {
        numberOfDirtyPages = 0;
      } else {
        numberOfDirtyPages = bp.dirtyPagesForDS(tk);
      }
    }
    costOfWritingProjectedRelation =
        numberOfDirtyPages *
        static_cast<double>(getSM().getStorageWriteDelay());

    // std::cout << "numberOfDirtyPages: " << numberOfDirtyPages << std::endl;
  }

  // std::cout << "costOfOperation: " << costOfOperation << std::endl;
  // std::cout << "costOfWritingProjectedRelation: "
  //           << costOfWritingProjectedRelation << std::endl;

  // std::cout << "writesBasedOnStatistics: " << writesBasedOnStatistics
  //           << std::endl;
  // std::cout << "fileSizeProjected: " << fileSizeProjected << std::endl;
  // std::cout << "fileSizeProjected - writesBasedOnStatistics: "
  //           << fileSizeProjected - writesBasedOnStatistics << std::endl;

  size_t n = numberOfReferencesForOperator + 1;
  double r = static_cast<double>(getSM().getStorageReadDelay());
//  double w = static_cast<double>(getSM().getStorageWriteDelay());
//  double R = fileSize;
  double Rp = fileSizeProjected;
  double dirtyCost = costOfWritingProjectedRelation;

  // Sc = (n-1)*r(R)  - dirtyCost - (n-1)*r(R') > 0
  //  double finalScore = (n - 1) * r * R - dirtyCost - (n - 1) * r * Rp;

  double finalScore = (n - 1) * costOfOperation - dirtyCost - (n - 1) * r * Rp;
  // PRINT SECTION
  // if (isCandidate) {
  //   std::cout << "---------- calculateFutureScore:
  //   IS-Candidate--------------- "
  //             << tk << std::endl;
  // } else {
  //   std::cout
  //       << "---------- calculateFutureScore: IS-Materialised--------------- "
  //       << tk << std::endl;
  // }

  // std::cout << "calculateFutureScore: fileSizeInPages: " << fileSize
  //           << std::endl;
  // std::cout << "calculateFutureScore: fileSizeInPagesForProjectedRelation: "
  //           << fileSizeProjected << std::endl;
  // std::cout << "costOfOperation is: " << costOfOperation << std::endl;
  // std::cout << "costOfOperation is: " << dirtyCost << std::endl;
  // std::cout << "finalScore is: " << finalScore << std::endl;

  return finalScore;
}

double BufferManager::calculatePastScore(const table_key& tk,
                                         bool isCandidate) {
  // std::cout<<"Calculate past score \n";

  int totalNumberOfPastReferences = 0;

  auto pastReferences = sm.getIterOfPersistentStatistics(tk);

  if (pastReferences != sm.getIteratorPersistentStatisticsEnd()) {
    // std::cout<<"No past record. SCORE= 0 \n";
    totalNumberOfPastReferences += pastReferences->second.references;
  }

  if (!isCandidate) {
    totalNumberOfPastReferences += 1;
  }

  if (totalNumberOfPastReferences == 0) {
    // std::cout
    //     << "calculatePastScore: score = 0, there aren't any past references.
    //     "
    //     << tk << std::endl;
    return 0.0;
  }

  //  double writeCost = costs.second;
  double operatorCost = calculateOperatorCost(tk);

  double totalScore =
      static_cast<double>(totalNumberOfPastReferences * operatorCost);

  // std::cout << "Past operatorCost: " << operatorCost << "\n";
  //  std::cout << "calculatePastScore: TOTAL PAST SCORE: " << totalScore <<
  //  "\n";

  return totalScore;
}

double BufferManager::calculateFullCostOfOperation(const table_key& tk) {
  if (!isDatastructure(tk)) {
    return 0.0;
  }

  size_t projectedSize = sm.getPrimaryFileSizeInPagesForProjectedRelation(tk);
  size_t realSize = sm.getPrimaryFileSizeInPagesForRelation(tk);
  size_t sizeOfPool = bp.getPoolSize();
  size_t numberOfDirtyPages = bp.dirtyPagesAll();
  size_t amountOfMemoryAvailable = sizeOfPool - numberOfDirtyPages;
  //  std::cout << "local_tk: " << tk << std::endl;
  //  std::cout << "projected: " << projectedSize << std::endl;
  //  std::cout << "real: " << realSize << std::endl;
  //  std::cout << "numberOfDirtyPages (All): " << numberOfDirtyPages <<
  //  std::endl;
  //  std::cout << "numberOfDirtyPages (TEMPORAL): "
  //            << bp.dirtyPagesForCategory(TEMPORAL) << std::endl;
  //  std::cout << "numberOfDirtyPages: (PERMANENT) "
  //            << bp.dirtyPagesForCategory(PERMANENT) << std::endl;
  //  std::cout << "sizeOfPool: " << sizeOfPool << std::endl;
  //  std::cout << "amountOfMemoryAvailable: " << amountOfMemoryAvailable
  //            << std::endl;

  size_t costIfMaterialise = 0;
  if (projectedSize <= amountOfMemoryAvailable) {
    //    std::cout << "pages to be written: 0" << std::endl;
    costIfMaterialise = projectedSize;
  } else {
    costIfMaterialise = amountOfMemoryAvailable;
  }

  double amountWritten = estimateDirtyWrites(sizeOfPool, projectedSize);

  //  std::cout << "amountWritten: " << amountWritten << std::endl;
  //  std::cout << "amountToWrite: " << costIfMaterialise << std::endl;

//  double costOperation = sm.getStorageReadDelay() * realSize +
//                         sm.getStorageWriteDelay() * amountWritten;
//  double costOfFutureOperation = sm.getStorageReadDelay() * projectedSize +
//                                 sm.getStorageWriteDelay() * costIfMaterialise;
  //
  //  std::cout << "costOperation vs costOfFutureOperation: " << costOperation
  //            << " , " << costOfFutureOperation << std::endl;

  return amountWritten;
}
// HOT 03/02/2016 estimateDirtyWrites
double BufferManager::estimateDirtyWrites(double sizeOfProjectedFile,
                                          double candidateDirtyPages) {
  double memorySize = bp.getPoolSize();
  double numberOfDirtyPages = bp.dirtyPagesIntermediate() + candidateDirtyPages;

  // calculate the free space left from the dirty intermediate pages
  size_t amountOfMemoryAvailable =
      (memorySize > numberOfDirtyPages) ? memorySize - numberOfDirtyPages : 10;

  //  std::cout << "estimateDirtyWrites memorySize: " << memorySize <<
  //  std::endl;
  //  std::cout << "estimateDirtyWrites candidateDirtyPages: "
  //            << candidateDirtyPages << std::endl;
  //  std::cout << "estimateDirtyWrites numberOfDirtyPages: " <<
  //  numberOfDirtyPages
  //            << std::endl;
  //  std::cout << "estimateDirtyWrites amountOfMemoryAvailable: "
  //            << amountOfMemoryAvailable << std::endl;
  //  std::cout << "estimateDirtyWrites: totalSize of all files: "
  //            << sizeOfProjectedFile << std::endl;

  // Simple example:
  // if projectedFileSize = 100, availableMemory=50
  // only 100-50=50 pages are going to be written to
  // persistent memory. Remember that those pages
  // are not necessary the pages of this data structure.
  double result = 0.0;
  if (sizeOfProjectedFile <= amountOfMemoryAvailable) {
    result = 0;  // if file fits in memory there are no writes appearing
  } else {
    result = sizeOfProjectedFile - amountOfMemoryAvailable;
  }

  //  std::cout << "estimateDirtyWrites result: " << result << std::endl;
  return result;
}

double BufferManager::getScoreForMemory(double fileSize, double memory) {
  double memoryForThisOperator = memory / (sm.getNumberOfThreads() + 1);
  if (fileSize < memoryForThisOperator) {
    return 0;
  } else {
    return fileSize - memoryForThisOperator;
  }
}

bool BufferManager::isInIndex(const std::string& filename, offset_t offset,
                              FileMode fmode) {
#ifdef MULTI_THREADED
  std::lock_guard<std::mutex> lock(getSM().bufferManagerOperationMutex);
#endif
  return bp.isInIndex(FilePos(filename, offset), fmode);
}

int BufferManager::getEstimateForLruWrites(int M, int F) {
  if (M <= 0.1 * F) {
    return F;
  } else if (M <= 0.2 * F) {
    return 0.98 * F;
  } else if (M <= 0.3 * F) {
    return 0.95 * F;
  } else if (M <= 0.5 * F) {
    return 0.90 * F;
  } else if (M <= 0.65 * F) {
    return 0.86 * F;
  } else if (M <= 0.75 * F) {
    return 0.80 * F;
  } else if (M <= 0.85 * F) {
    return 0.75 * F;
  } else if (M <= F) {
    return 0.72 * F;
  } else if (M <= 1.15 * F) {
    return 0.65 * F;
  } else if (M <= 1.25 * F) {
    return 0.55 * F;
  } else if (M <= 1.40 * F) {
    return 0.50 * F;
  } else if (M <= 1.5 * F) {
    return 0.40 * F;
  } else if (M <= 1.65 * F) {
    return 0.35 * F;
  } else if (M <= 1.75 * F) {
    return 0.25 * F;
  } else if (M <= 1.85 * F) {
    return 0.15 * F;
  } else if (M <= 2 * F) {
    return 0.05 * F;
  } else {
    return 0;
  }
}

int BufferManager::getEstimateForLruReads(int M, int F) {
  if (M <= 0.1 * F) {
    return 1.95 * F;
  } else if (M <= 0.2 * F) {
    return 1.95 * F;
  } else if (M <= 0.3 * F) {
    return 1.95 * F;
  } else if (M <= 0.5 * F) {
    return 1.85 * F;
  } else if (M <= 0.75 * F) {
    return 1.85 * F;
  } else if (M <= 0.85 * F) {
    return 1.75 * F;
  } else if (M <= F) {
    return 1.78 * F;
  } else if (M <= 1.25 * F) {
    return 1.6 * F;
  } else if (M <= 1.5 * F) {
    return 1.25 * F;
  } else if (M <= 1.85 * F) {
    return 1.2 * F;
  } else if (M <= 2 * F) {
    return 1.09 * F;
  } else {
    return F;
  }
}

int BufferManager::getEstimateForRankedBasedWrites(int M, int F) {
  if (M <= 0.1 * F) {
    return F;
  } else if (M <= 0.2 * F) {
    return 0.98 * F;
  } else if (M <= 0.3 * F) {
    return 0.95 * F;
  } else if (M <= 0.5 * F) {
    return 0.8 * F;
  } else if (M <= 0.75 * F) {
    return 0.45 * F;
  } else if (M <= 0.85 * F) {
    return 0.3 * F;
  } else if (M <= F) {
    return 0.1 * F;
  } else if (M <= 1.15 * F) {
    return 0.05 * F;
  } else {
    return 0;
  }
}

int BufferManager::getEstimateForRankedBasedReads(int M, int F) {
  if (M <= 0.1 * F) {
    return 1.95 * F;
  } else if (M <= 0.2 * F) {
    return 1.95 * F;
  } else if (M <= 0.3 * F) {
    return 1.90 * F;
  } else if (M <= 0.5 * F) {
    return 1.85 * F;
  } else if (M <= 0.75 * F) {
    return 1.5 * F;
  } else if (M <= 0.85 * F) {
    return 1.35 * F;
  } else if (M <= F) {
    return 1.1 * F;
  } else if (M <= 1.15 * F) {
    return 1.05 * F;
  } else {
    return F;
  }
}

int BufferManager::getEstimateForClruWrites(int M, int F) {
  if (M <= 0.1 * F) {
    return 0.95 * F;
  } else if (M <= 0.2 * F) {
    return 0.9 * F;
  } else if (M <= 0.3 * F) {
    return 0.8 * F;
  } else if (M <= 0.5 * F) {
    return 0.6 * F;
  } else if (M <= 0.75 * F) {
    return 0.3 * F;
  } else if (M <= 0.85 * F) {
    return 0.2 * F;
  } else if (M <= F) {
    return 0.05 * F;
  } else if (M <= 1.15 * F) {
    return 0.03 * F;
  } else {
    return 0;
  }
}

int BufferManager::getEstimateForClruReads(int M, int F) {
  if (M <= 0.1 * F) {
    return 1.95 * F;
  } else if (M <= 0.2 * F) {
    return 1.9 * F;
  } else if (M <= 0.3 * F) {
    return 1.8 * F;
  } else if (M <= 0.5 * F) {
    return 1.6 * F;
  } else if (M <= 0.75 * F) {
    return 1.4 * F;
  } else if (M <= 0.85 * F) {
    return 1.2 * F;
  } else if (M <= F) {
    return 1.1 * F;
  } else if (M <= 1.15 * F) {
    return 1.05 * F;
  } else {
    return F;
  }
}

// void BufferManager::reorderQueries(iterable_queue<deceve::queries::QOperator
// *> *queryQueue) {
//  std::cout << "TRY TO reorderQueries" << std::endl;
//
//  //  std::vector<std::vector<ScoreKeyTuple> *>
//  orderedTuples;
//  std::vector<std::pair<double, QOperator *> > orderedTuples;
//
//  //******* EXTRACT KEYS FROM QUERIES ********
//  int localCounter = 0;
//  //  for (iterable_queue<QOperator *>::iterator query = queryQueue->begin();
//  //       query != queryQueue->end(); ++query) {
//  for (auto query : (*queryQueue)) {
//    std::vector<ScoreKeyTuple> *localVector = new
//    std::vector<ScoreKeyTuple>();
//
//    std::cout << "Extract keys from query (tpchID): " << query->getTpchId() <<
//    std::endl;
//    std::cout << "Local Counter:" << localCounter++ << std::endl;
//
//    query->extractTable_keys(localVector);
//
//    double scoreOfQuery = 0.0;
//    for (const auto &localIterator : (*localVector)) {
//      scoreOfQuery += localIterator.first;
//    }
//    std::cout << std::endl;
//    std::cout << "QUERY id: " << query->getQueryId() << std::endl;
//    std::cout << "QUERY TPCH-ID: " << query->getTpchId() << std::endl;
//    std::cout << "QUERY TOTAL SCORE IS: " << scoreOfQuery << std::endl;
//    orderedTuples.push_back(std::make_pair(scoreOfQuery, query));
//  }
//
//  std::cout << "UNSORTED" << std::endl;
//  for (const auto &queryVector : orderedTuples) {
//    std::cout << "SCORE: " << queryVector.first << " " << "qId: " <<
//    queryVector.second->getQueryId() << " tpchId: "
//              << queryVector.second->getTpchId() << std::endl;
//  }
//
//  std::sort(orderedTuples.begin(), orderedTuples.end(), [](const
//  std::pair<double, QOperator *> &a,
//      const std::pair<double, QOperator *> &b) {
//    return a.first > b.first;
//  });
//
//  std::cout << "SORTED" << std::endl;
//  for (const auto &queryVector : orderedTuples) {
//    std::cout << "SCORE: " << queryVector.first << " " << "qId: " <<
//    queryVector.second->getQueryId() << " tpchId: "
//              << queryVector.second->getTpchId() << std::endl;
//  }
//
//  std::cout << "---QUEUE STATE----" << std::endl;
//  for (auto query : (*queryQueue)) {
//    std::cout << "query: " << query->getTpchId() << std::endl;
//  }
//
//  int numberOfQueries = 0;
//  std::cout << "SORTED" << std::endl;
//  for (const auto &queryVector : orderedTuples) {
//    (*queryQueue).push(queryVector.second);
//    (*queryQueue).pop();
//    numberOfQueries++;
//  }
//
//
//  std::cout << "----FINAL QUEUE STATE----" << std::endl;
//  for (auto query : (*queryQueue)) {
//    std::cout << "query: " << query->getTpchId() << std::endl;
//  }
//  std::cout << "----------" << std::endl;
//
//  //              int minSize = a.size() < b.size() ? a.size() : b.size();
//  //              int i;
//  //              for (i = 0; i < minSize; i++) {
//  //                // Check firstly the scores
//  //                if (std::get<0>(a[i]) != std::get<0>(b[i])) {
//  //                  return std::get<0>(a[i]) > std::get<0>(b[i]);
//  //                }
//  //                if (a[i] != b[i]) {
//  //                  return a[i] < b[i];
//  //                }
//  //              }
//  //              if (a.size() == i) {
//  //                return false;
//  //              }
//  //              if (b.size() == i) {
//  //                return true;
//  //              }
//  //              return a[0] < b[0];
//  //            });
//  //
//  //  std::cout << "SORTED" << std::endl;
//  //  for (const auto &queryVector : orderedTuples) {
//  //    std::cout << "QUERY: " << std::endl;
//  //    for (const auto &queryOperator : queryVector) {
//  //      std::cout << "score: " << std::get<0>(queryOperator)
//  //                << " key: " << std::get<1>(queryOperator) << " ";
//  //    }
//  //  }
//
//  orderedTuples.clear();
//}

bool BufferManager::isDatastructure(const sto::table_key& tk) {
  if (tk.version == sto::SORT || tk.version == sto::HASHJOIN) {
    return true;
  }
  return false;
}

bool BufferManager::isMaterialised(const deceve::storage::table_key& tk) {
  return sm.isInPersistedFileCatalog(tk);
}

bool BufferManager::isBeingGenerated(const deceve::storage::table_key& tk) {
  return sm.isInPersistedTmpFileSet(tk);
}

}  // namespace storage
}  // namespace deceve
