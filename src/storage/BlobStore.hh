#ifndef BLOBSTORE_HH
#define BLOBSTORE_HH

namespace deceve { namespace storage {

class BlobStore {
private:
  enum { CHUNK_SIZE = 1024*1024 };
  enum { PAGE_SIZE = 4096 };

public:
  BlobStore(const std::string&);
  ~BlobStore();

  bool read(off_t, unsigned char *, size_t &) const;
  bool allocateAndRead(off_t, unsigned char **, size_t &) const;
  size_t occupied() const { return m_occupied; }
  void append(unsigned char *, size_t);
  void write(off_t, unsigned char*, size_t);
  bool invalidate(off_t);
protected:
  void map();
  void unmap();
  void expand();

private:
  unsigned char* m_map;
  std::string m_filename;  
  int m_fileDescriptor;
  size_t m_allocated;
  size_t m_occupied;
};

}}

#endif
