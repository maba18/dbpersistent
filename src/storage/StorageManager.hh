#ifndef STORAGEMANAGER_HH
#define STORAGEMANAGER_HH

// C Headers
#include <math.h>

// C++ Headers
#include <iostream>
#include <map>
#include <unordered_map>
#include <string>
#include <vector>
#include <utility>
#include <set>
#include <iomanip>

// Multithreading Headers
#include <thread>
#include <mutex>
#include <atomic>
#include <unordered_set>
#include <unordered_map>
#include <list>

// Custom Headers
#include "File.hh"
#include "MemPage.hh"
#include "../utils/types.hh"
#include "../utils/defs.hh"
#include "../utils/require.hh"
#include "../utils/helpers.hh"
#include "../storage/StatsManager.hh"

#include <chrono>

// Multithreading libraries
// #include <boost/thread/mutex.hpp>
// #include <boost/thread/thread.hpp>

namespace deceve {
namespace storage {

//#define LRUTEST
//#define CLEANTEST
//#define CLEANTEST2

#define LOCKS

#ifdef LRUTEST
enum FileMode {
  PRIMARY = 0,
  AUXILIARY = 0,
  INTERMEDIATE = 0,
  FREE,
  FREE_INTERMEDIATE
};
// for replacement algorithm 3, case1 and case2
#elif AUXILIARY_MERGED
enum FileMode {
  PRIMARY = 0,
  AUXILIARY = 0,
  INTERMEDIATE,
  FREE,
  FREE_INTERMEDIATE
};
#else
// for algorithm 2 and 3 default
enum FileMode { PRIMARY, AUXILIARY, INTERMEDIATE, FREE, FREE_INTERMEDIATE };
#endif

enum Version { NORMAL, SORTED, HASHED };
enum SorterMode { BOTH_FILES, LEFT_FILE_ONLY, RIGHT_FILE_ONLY, NO_FILE };
enum HistoryMode { PAST_FUTURE, PAST_ONLY, FUTURE_ONLY };
enum FilePath { PRIMARY_PATH, INTERMEDIATE_PATH };
enum ProjectionFlag { LEFT_PROJECT, RIGHT_PROJECT, BOTH_PROJECT };

typedef struct FileInformation {
  FileMode mode;
  offset_t size;
  Version version;

  FileInformation(FileMode m = PRIMARY, offset_t s = 0, Version ver = NORMAL)
      : mode(m), size(s), version(ver) {}

  bool operator<(const FileInformation& e) const {
    bool off_comp = size < e.size;
    return (off_comp ? true : false);
  }

  bool operator==(const FileInformation& e) const {
    bool off_comp = size == e.size;
    return (off_comp ? true : false);
  }
} FileInformation;

struct FileInformation_equal {
  bool operator()(const FileInformation& x, const FileInformation& y) const {
    return (x.size == y.size);
  }
};

struct FileInformation_hash {
  std::size_t operator()(const FileInformation& f) const {
    std::size_t seed = 0;
    offset_t page_offset = f.size / PAGE_SIZE_PERSISTENT;
    long short_offset = (long)page_offset;
    boost::hash_combine(seed, f.mode);
    boost::hash_combine(seed, short_offset);
    return seed;
  }
};

class StorageManager {
 public:
  StorageManager();
  explicit StorageManager(const std::string& s);
  ~StorageManager();

  std::chrono::duration<double> totalReadChronoTime{0};
  double totalReadChronoCounter{0};
  std::chrono::duration<double> totalWriteChronoTime{0};
  double totalWriteChronoCounter{0};

 private:
  typedef std::map<FilePos, size_t> location_map_type;

  std::set<std::string> tableCatalog;
  bool loadFilesFlag{true};

  // Main files
  std::unordered_map<std::string, File*> open_files;  // LOCKED DONE
  std::unordered_set<std::string> primaryFiles;
  std::unordered_set<std::string> auxiliaryFiles;
  std::unordered_set<std::string> intermediateFiles;
  std::unordered_map<std::string, FileInformation> fileStatistics;

  // Persisted Files
  global_table_statistics persistedPastFileStatistics;  // LOCKED DONE
  global_table_map persistedFileCatalog;
  global_table_references persistedFutureFileReferences;
  global_table_classifier persistedTmpFileSet;

  // Additional variables
  uint64_t readCostReal;
  uint64_t writeCostReal;
  Timestamp persistentClock;
  //  assigns timestamps to persistent files
  //  std::atomic<unsigned long> atomicUniqueFileId;
  //  std::atomic<unsigned long> atomicUniqueQueryId;
  //  std::atomic<unsigned long> atomicUniqueNodeId;

  unsigned long atomicUniqueFileId;
  unsigned long atomicUniqueQueryId;
  unsigned long atomicUniqueNodeId;

  uint64_t storageReadDelay;
  uint64_t storageWriteDelay;
  unsigned long physicalReads{0};
  unsigned long physicalWrites{0};
  unsigned long physicalPrimaryWrites{0};
  unsigned long physicalIntermediateWrites{0};
  unsigned long physicalAuxiliaryWrites{0};
  unsigned long physicalPrimaryReads{0};
  unsigned long physicalIntermediateReads{0};
  unsigned long physicalAuxiliaryReads{0};
  size_t budgetSizePermanent;
  HistoryMode historyFlag{PAST_FUTURE};

  // performance Timers
  uint64_t totalRunTimeForStorageReadingPages{0};
  uint64_t totalRunTimeForStorageWritingPages{0};
  uint64_t totalRunTimeForBufferManagerReadingPages{0};
  uint64_t totalRunTimeForBufferManagerWritingPages{0};
  std::vector<uint64_t> runTimes;
  std::mutex runtimeLocker;
  uint64_t fetchPageTimerInDRam{0};
  uint64_t fetchPageTimerInNvram{0};
  size_t amountOfPartitions{5};
  size_t numberOfThreads{1};
  int reorderFlag{0};
  int orderFlag{0};
  int queueDepth{5};
  std::map<table_key, double> internalScores;

  StatsManager statsManager;

 public:
  std::mutex bufferManagerOperationMutex;
  std::mutex queryOperatorsMutex;
  std::mutex readwritersMainMutex;
  std::mutex pinFileMutex;
  std::recursive_mutex taskQueueMutex;
  std::mutex readerOpenFilesMutex;
  std::mutex dirtyLocker;

  std::unordered_map<std::string, table_key> nameConverter;
  std::map<table_key, size_t> idConverter;
  std::unordered_map<TABLENAME, std::string, std::hash<int> > tableNameMapper;
  std::unordered_map<TABLENAME, size_t, std::hash<int> > tableTupleSizeMapper;
  std::unordered_map<PROJECTED_FIELD, size_t, std::hash<int> >
      projectedTableTupleSizeMapper;
  std::vector<table_key> futureOrderedQueries;

  double totalFileSizes;
  double totalNumberOfReferences;
  int errorCounter{0};

  std::map<table_key, double> futurePersistedStatisticsCounter;
  std::unordered_map<std::string, int> dirtyCounters;
  std::map<deceve::storage::table_key, int> writesPredictions;

  void printWritePredictions() {
    std::cout << "printWritePredictions (start) " << std::endl;
    for (auto it : writesPredictions) {
      std::cout << it.first << " " << it.second << std::endl;
    }
    std::cout << "printWritePredictions (end) " << std::endl;
  }

  void printNameConverters() {
    std::cout << "printNameConverters (start) " << std::endl;
    for (auto it : nameConverter) {
      std::cout << it.first << " " << it.second << std::endl;
    }
    std::cout << "printNameConverters (end) " << std::endl;
  }

  int filesPersistedCounterFuture{0};
  int filesPersistedCostFuture{0};
  int filesPersistedCounterPast{0};
  int filesPersistedCostPast{0};

  std::atomic<unsigned int> reorderCounter{0};

  std::map<table_key, size_t> primaryCalculatedSizes;
  // all file operations
  // use openFile to create a new file
  File* openFile(const std::string& filename, FileMode m = PRIMARY);
  void closeFile(const std::string& filename);
  void removeFile(const std::string& filename);
  void renameFile(const std::string& filename, const std::string& n);
  bool isOpen(const std::string& filename) const;
  inline bool isPrimary(const std::string& filename) const;
  inline bool isAuxiliary(const std::string& filename) const;
  inline bool isIntermediate(const std::string& filename) const;
  FileMode getFileMode(const std::string& filename) const;

  std::unordered_set<std::string>* getFileModeSet(const std::string& filename);
  std::unordered_set<std::string>* getFileModeSet(const FileMode& filemode);

  // all read-write operations
  void readPage(const MemPage& pg, FileMode fmode = PRIMARY);
  void readPage(const std::string& fname, const offset_t offt,
                char* const buffer, FileMode fmode = PRIMARY);
  void writePage(MemPage& pg, FileMode fmode = PRIMARY);
  void writePage(const std::string& fname, const offset_t offt,
                 char* const buffer, FileMode fmode = PRIMARY);
  void closeAllFiles();

  offset_t getFileSizeFromFileSystem(const std::string& filename,
                                     FileMode mode = PRIMARY);
  offset_t getFileSizeFromCatalog(const std::string&, FileMode mode = PRIMARY);

  FileMode getFileModeFromCatalog(const std::string&);
  void updateFileSize(const std::string&, offset_t size);
  void updateFileMode(const std::string&, FileMode mode);

  // table_catalog_operations
  void insertIntoTableCatalog(const std::string& tname);
  bool isInTableCatalog(const std::string& tname);

  offset_t getFileSizeInPagesFromCatalog(const std::string& filename,
                                         FileMode mode = PRIMARY);
  size_t getPrimaryFileSizeInPagesForProjectedRelation(table_key tk);
  size_t getPrimaryFileSizeInPagesForRelation(table_key tk);
  size_t getPrimaryFileSizeInTuplesForRelation(table_key tk);
  int getNumberOfDirtyPagesFromSM(const std::string& fn, FileMode mode);
  double getNumberOfTuplesForLineitem();

  // persistedCurrentFileCatalog operations used for keeping the persisted files
  // and information about them
  bool isInPersistedFileCatalog(const table_key& tk);
  void insertInPersistedFileCatalog(
      const std::pair<table_key, table_value>& record);
  void deleteFromPersistedFileCatalog(const table_key& tk);
  void deleteFromPersistedFileCatalog(const global_table_map::iterator& it);

  // persistedFutureFileReferences operations, keeps statistics about the new
  // queries coming
  bool isInPersistedFutureFileReferences(const table_key& tk);
  void insertInPersistedFutureFileReferences(
      const std::pair<table_key, std::pair<int, int> >& record);
  void deleteFromPersistedFutureFileReferences(const table_key& tk);
  void deleteFromPersistedFutureFileReferences(
      const global_table_references::iterator& it);
  global_table_references::iterator getIterOfPersistedFutureFileReferences(
      const table_key& tk);
  void clearPersistedFutureFileReferences();
  void clearFutureOrderedQueries();

  int getTotalNumberOfReferences();
  int getTotalNumberOfFilesPastReferences();

  size_t calculateNumberOfPartitions(const std::string& leftfile,
                                     const std::string& rightfile,
                                     int numberOfAvailablePrimaryPages);
  size_t calculateNumberOfPartitions(size_t fileSizeInPagesLeft,
                                     size_t fileSizeInPagesRight,
                                     size_t numberOfAvailablePrimaryPages);
  size_t calculateNumberOfPartitions(const std::string& leftfile,
                                     int numberOfAvailablePrimaryPages);
  double getInternalOperationCost(const table_key& tk);
  void setInternalOperationCost(const table_key& tk, double score);

  // persistedStatisticsOperations, keeps statistics about the queries past
  void insertToPersistentStatistics(
      const std::pair<table_key, table_statistics>& tuple);
  void updatePersistentStatistics(table_key tk, table_statistics ts);
  void incrementPersistentStatisticsReferences(table_key tk,
                                               bool persistentFlag = false);
  void incrementPersistentStatisticsHits(table_key tk);
  bool isContainedInPersistentStatistics(table_key tk);

  global_table_statistics::const_iterator getIterOfPersistentStatistics(
      const table_key& tk);

  global_table_statistics::const_iterator getIteratorPersistentStatisticsBegin()
      const {
    return persistedPastFileStatistics.begin();
  }
  global_table_statistics::const_iterator getIteratorPersistentStatisticsEnd()
      const {
    return persistedPastFileStatistics.end();
  }

  // PersistedTmpFileset_operations used for locking the current processed
  // data-structures
  bool isInPersistedTmpFileSet(const table_key& tk);
  void insertInPersistedTmpFileSet(
      const table_key& tk,
      const FILE_CLASSIFIER& fileClassifier = NOTCLASSIFIED);
  void deleteFromPersistedTmpFileSet(const table_key& tk);

  global_table_classifier::const_iterator getIteratorRecordFromTmpFileSet(
      const table_key& tk);

  global_table_classifier::const_iterator getIteratorRecordFromTmpFileSetBegin()
      const {
    return persistedTmpFileSet.begin();
  }
  global_table_classifier::const_iterator getIteratorRecordFromTmpFileSetEnd()
      const {
    return persistedTmpFileSet.end();
  }

  table_key getNameFromConverter(const std::string& filename);

  void insertInNameConverter(const std::string& filename, const table_key& tk) {
    nameConverter.insert(std::make_pair(filename, tk));
    // nameConverter.insert( std::make_tuple(filename, tk));
  }

  void insertInIdConverter(const table_key& tk, const unsigned int id) {
    idConverter.insert(std::make_pair(tk, id));
  }

  unsigned int getIdConverter(const table_key tk) {
    if (idConverter.find(tk) != idConverter.end())
      return idConverter.find(tk)->second;
    return 200000;
  }

  // Generate filenames functions
  std::string generateUniqueFileName(const std::string& name = "");

  inline unsigned long getNextUniqueFileId() {
    atomicUniqueFileId++;
    // return atomicUniqueFileId.load();
    return atomicUniqueFileId;
  }
  inline unsigned long generateUniqueQueryId() {
    atomicUniqueQueryId++;
    // return atomicUniqueQueryId.load();
    return atomicUniqueQueryId;
  }
  inline unsigned long generateUniqueNodeTreeId() {
    atomicUniqueNodeId++;
    // return atomicUniqueNodeId.load();
    return atomicUniqueNodeId;
  }

  File* getFile(const std::string& filename);
  int calculateSpaceOfNewPersistedFiles();
  int calculateSpaceOfPersistedFiles(FILE_CLASSIFIER fileClassifier);
  int calculateTotalSpaceOfPersistedFiles();
  int calculateSpaceOfFuturePersistedFiles(FILE_CLASSIFIER fileClassifier);

  void loadBasicTablesInCatalog();

  //####### All printing functions
  void printTableCatalog();
  void printAttrCatalog();
  void printPersistentFileCatalog();
  void printPersistentFutureFileCounters();
  void printPersistentFutureFileReferences();
  void printFutureOrderQueries();
  void printFileStatistics();
  void printPesistentFileStatistics();
  void printOpenFiles();
  void printFiles(FileMode mode);
  void printAllFiles();
  void printCurrentStateOfPool();
  void printFutureStatisticsCounter();
  void printInternalScores();
  void printSharingStatistics(const std::string& filename);
  void exportRuntimesToFile(const std::string& filename, size_t numCycles);

  // #################### All getters and setters ####################

  uint64_t getRead_cost_real() { return readCostReal; }
  uint64_t getWrite_cost_real() { return writeCostReal; }

  inline Timestamp getNextPersistentTime() { return persistentClock++; }

  void setStorageReadDelay(uint64_t readDelay) { storageReadDelay = readDelay; }

  void setStorageWriteDelay(uint64_t writeDelay) {
    storageWriteDelay = writeDelay;
  }

  uint64_t getStorageReadDelay() { return storageReadDelay; }

  uint64_t getStorageWriteDelay() { return storageWriteDelay; }

  void setBudgetPermanentSize(size_t budget) { budgetSizePermanent = budget; }

  size_t getBudgetPermanentSize() { return budgetSizePermanent; }


  void setPhysicalReadNumber(unsigned long n) { physicalReads = n; }
  void setPhysicalWriteNumber(unsigned long n) { physicalWrites = n; }

  unsigned long getPhysicalReadsNumber() { return physicalReads; }
  unsigned long getPhysicalWritesNumber() { return physicalWrites; }

  unsigned long getPrimaryPhysicalWritesNumber() {
    return physicalPrimaryWrites;
  }

  unsigned long getIntermediatePhysicalWritesNumber() {
    return physicalIntermediateWrites;
  }

  unsigned long getAuxiliaryPhysicalWritesNumber() {
    return physicalAuxiliaryWrites;
  }

  unsigned long getPrimaryPhysicalReadsNumber() { return physicalPrimaryReads; }
  unsigned long getIntermediatePhysicalReadsNumber() {
    return physicalIntermediateReads;
  }
  unsigned long getAuxiliaryPhysicalPhysicalReadsNumber() {
    return physicalAuxiliaryReads;
  }

  uint64_t getTotalRunTimeForStorageReadingPages() {
    return totalRunTimeForStorageReadingPages;
  }

  uint64_t getTotalRunTimeForStorageWritingPages() {
    return totalRunTimeForStorageWritingPages;
  }

  uint64_t getTotalRunTimeForBufferReadingPages() {
    return totalRunTimeForBufferManagerReadingPages;
  }

  uint64_t getTotalRunTimeForBufferWritingPages() {
    return totalRunTimeForBufferManagerWritingPages;
  }

  void setAmountOfPartitions(size_t partitions) {
    amountOfPartitions = partitions;
  }
  size_t getAmountOfPartitions() { return amountOfPartitions; }

  void setReorderFlag(int r) { reorderFlag = r; }

  int getReorderFlag() { return reorderFlag; }

  void setOrderFlag(int r) { orderFlag = r; }

  int getOrderFlag() { return orderFlag; }

  void setQueueDepth(int d) { queueDepth = d; }

  int getQueueDepth() { return queueDepth; }

  void setNumberOfThreads(size_t threads) { numberOfThreads = threads; }
  size_t getNumberOfThreads() { return numberOfThreads; }

  void addRunTime(uint64_t timeValue) {
    std::lock_guard<std::mutex> flocker(runtimeLocker);
    runTimes.push_back(timeValue);
  }

  void incrementFetchPageTimerInDram(uint64_t timeValue) {
    fetchPageTimerInDRam += timeValue;
  }
  uint64_t getFetchPageTimerInDram() { return fetchPageTimerInDRam; }

  void incrementFetchPageTimerInNvram(uint64_t timeValue) {
    fetchPageTimerInNvram += timeValue;
  }

  uint64_t getFetchPageTimerInNVram() { return fetchPageTimerInNvram; }

  int getErrorCounter() { return errorCounter; }

  global_table_references::const_iterator getIteratorFutureFileReferencesBegin()
      const {
    return persistedFutureFileReferences.begin();
  }
  global_table_references::const_iterator getIteratorFutureFileReferencesEnd()
      const {
    return persistedFutureFileReferences.end();
  }

  global_table_map::iterator getIterOfPersistedFileCatalog(const table_key& tk);

  global_table_map::const_iterator getIteratorPersistedCatalogBegin() const {
    return persistedFileCatalog.begin();
  }
  global_table_map::const_iterator getIteratorPersistedCatalogEnd() const {
    return persistedFileCatalog.end();
  }

  size_t getPersistedFileCatalogSize() { return persistedFileCatalog.size(); }

  HistoryMode getHistoryFlag() const { return historyFlag; }

  void setHistoryFlag(HistoryMode history) { this->historyFlag = history; }

  FIELD getRandomPartKeyField(int posotita) {
    // srand (time(NULL));
    /* generate secret number between 1 and 10: */
    //    int q = rand() % posotita;
    int q = posotita;
    //    std::cout << "RANDOM: " << q << std::endl;

    switch (q) {
      case 0:
        return L_PARTKEY1;
      case 1:
        return L_PARTKEY2;
      case 2:
        return L_PARTKEY3;
      case 3:
        return L_PARTKEY4;
      case 4:
        return L_PARTKEY5;
      case 5:
        return L_PARTKEY6;
      case 6:
        return L_PARTKEY7;
      default:
        return L_PARTKEY;
    }
  }

  FIELD getRandomOrderKeyField(int posotita) {
    // srand (time(NULL));
    /* generate secret number between 1 and 10: */
    //    int q = rand() % posotita;
    int q = posotita;
    //    std::cout << "RANDOM: " << q << std::endl;

    switch (q) {
      case 0:
        return L_ORDERKEY1;
      case 1:
        return L_ORDERKEY2;
      case 2:
        return L_ORDERKEY3;
      case 3:
        return L_ORDERKEY4;
      default:
        return L_ORDERKEY;
    }
  }

  size_t calculateNumberOfTuplesInPage(TABLENAME tableName) {
    auto tupleSizeIterator = tableTupleSizeMapper.find(tableName);
    size_t tupleSize = tupleSizeIterator->second;
    //    std::cout << "PROSOXI: calculateNumberOfTuples size of TUPLE: " <<
    //    tupleSize << std::endl;
    size_t tuplesPerPage = deceve::storage::PAGE_SIZE_PERSISTENT / tupleSize;
    return tuplesPerPage;
  }

  size_t calculateNumberOfTuplesInPage(PROJECTED_FIELD projectedField) {
    auto tupleSize = calculateSizeOfProjectedField(projectedField);
    size_t tuplesPerPage = deceve::storage::PAGE_SIZE_PERSISTENT / tupleSize;
    return tuplesPerPage;
  }

  size_t calculateSizeOfProjectedField(PROJECTED_FIELD projectedField) {
    auto tupleSizeIterator = projectedTableTupleSizeMapper.find(projectedField);
    if (tupleSizeIterator == projectedTableTupleSizeMapper.end()) {
      std::cout << "Corresponding size was not found for projectedField: "
                << projectedField << std::endl;
      return 1;
    }
    size_t tupleSize = tupleSizeIterator->second;
    return tupleSize;
  }

  size_t calculateSizeOfTupleOfInitialRelation(TABLENAME tableName) {
    auto tupleSizeIterator = tableTupleSizeMapper.find(tableName);
    if (tupleSizeIterator == tableTupleSizeMapper.end()) {
      std::cout << "Corresponding size was not found for tableName: "
                << tableName << std::endl;
      return 1;
    }
    size_t tupleSize = tupleSizeIterator->second;
    return tupleSize;
  }

  table_key mergeKeys(const table_key& tk1, const table_key& tk2) {
    table_key resultTk;

    if (tk1.table_name != tk2.table_name || tk1.version != tk2.version ||
        tk1.field != tk2.field) {
      std::cout << "ERROR: keys cannot be merged" << std::endl;
      std::cout << "tk1: " << tk1 << " <-> tk2: " << tk2 << std::endl;
      return resultTk;
    }

    resultTk.table_name = tk1.table_name;
    resultTk.field = tk1.field;
    resultTk.version = tk1.version;
    resultTk.projected_field = tk1.projected_field;
    // find set union of vectors of attributes
    std::vector<FIELD>::iterator it;
    std::vector<FIELD> newFields(tk1.attributes.size() + tk2.attributes.size());

    std::vector<FIELD> attributes1 = tk1.attributes;
    std::vector<FIELD> attributes2 = tk2.attributes;

    std::sort(attributes1.begin(), attributes1.end());
    std::sort(attributes2.begin(), attributes2.end());

    it = std::set_union(attributes1.begin(), attributes1.end(),
                        attributes2.begin(), attributes2.end(),
                        newFields.begin());
    newFields.resize(it - newFields.begin());

    resultTk.attributes = newFields;

    return resultTk;
    //
    //    FILE_VERSION version{OTHER};
    //    TABLENAME table_name{OTHER_TABLENAME};
    //    FIELD field{OTHER_FIELD};
    //    PROJECTED_FIELD projected_field{ALL};
    //    std::vector<FIELD> attributes{ALL_ATTRIBUTES};
  }

  void addToNameConverterJoin(std::string filename, std::string prefix,
                              deceve::storage::table_key join_tk,
                              size_t number_of_partitions) {
    //    std::lock_guard<std::mutex> guard(statsMutex);

    // COUT << "addToNameConverter " << "\n";
    for (unsigned int i = 0; i < number_of_partitions; ++i) {
      std::stringstream s;
      s << filename << prefix << "." << i;
      // COUT << "Add filename: " << s.str() << " for tk: " << join_tk
      // << "\n";
      insertInNameConverter(s.str(), join_tk);
    }
  }

  StatsManager& getStatsManager() { return statsManager; }



  //  // SECTION FOR WRITE STATISTICS //
  //  std::mutex statsMutex;
  //  long uniqueIdentifier{0};
  //  std::map<long, long> monitorKeys;
  //  std::unordered_map<std::string, long> monitorConverter;
  //  std::map<deceve::storage::table_key, std::list<long> > keyIdsMap;
  //
  //  void insertTkInStatsMap(const deceve::storage::table_key& tk) {
  //    std::lock_guard<std::mutex> guard(statsMutex);
  //    auto it = keyIdsMap.find(tk);
  //    if (it == keyIdsMap.end()) {
  //      std::list<long> v = {};
  //      keyIdsMap.insert(std::make_pair(tk, v));
  //    }
  //  }
  //
  //  void insertKeyIdInMap(const deceve::storage::table_key& tk, long id) {
  //    //    std::lock_guard<std::mutex> guard(statsMutex);
  //    auto it = keyIdsMap.find(tk);
  //    if (it == keyIdsMap.end()) {
  //      std::list<long> v = {id};
  //      keyIdsMap.insert(std::make_pair(tk, v));
  //    } else {
  //      it->second.push_back(id);
  //      if (it->second.size() > 10) {
  //        it->second.pop_front();
  //      }
  //    }
  //  }
  //
  //  long getAverageWritesForTk(const deceve::storage::table_key& tk) {
  //
  //    std::lock_guard<std::mutex> guard(statsMutex);
  //
  //    long averageWrites = 0;
  //    long counter = 0;
  //    auto it = keyIdsMap.find(tk);
  //
  //    // check if key exists in statistics
  //    if (it == keyIdsMap.end()) {
  //      return 0;
  //    }
  //    // check if list with values is empty
  //    if (it->second.size() == 0) {
  //      return 0;
  //    }
  //
  //    // iterate over list
  //    for (auto vit : it->second) {
  //      counter++;
  //      auto kit = monitorKeys.find(vit);
  //      long numWrites = 0;
  //      if (kit != monitorKeys.end()) {
  //        numWrites = kit->second;
  //      }
  //      averageWrites += numWrites;
  //    }
  //    averageWrites = averageWrites / counter;
  //
  //    std::cout << "averageWrites: " << averageWrites << std::endl;
  //
  //    return averageWrites;
  //  }
  //
  //  void addToNameMonitorConverterJoin(std::string filename, std::string
  //  prefix,
  //                                     deceve::storage::table_key join_tk,
  //                                     size_t number_of_partitions) {
  //    std::lock_guard<std::mutex> guard(statsMutex);
  //    insertKeyIdInMap(join_tk, uniqueIdentifier);
  //    // COUT << "addToNameConverter " << "\n";
  //    for (unsigned int i = 0; i < number_of_partitions; ++i) {
  //      std::stringstream s;
  //      s << filename << prefix << "." << i;
  //      monitorConverter.insert(std::make_pair(s.str(), uniqueIdentifier));
  //    }
  //    uniqueIdentifier++;
  //  }
  //
  //  void deleteFromNameMonitorConverterJoin(std::string filename,
  //                                          std::string prefix,
  //                                          size_t number_of_partitions) {
  //    std::lock_guard<std::mutex> guard(statsMutex);
  //
  //    // COUT << "addToNameConverter " << "\n";
  //    for (unsigned int i = 0; i < number_of_partitions; ++i) {
  //      std::stringstream s;
  //      s << filename << prefix << "." << i;
  //      monitorConverter.erase(s.str());
  //    }
  //  }
  //
  //  void insertInMonitorMap(long id) {
  //    std::lock_guard<std::mutex> guard(statsMutex);
  //    std::cout << "insertInMonitorMap: " << id << std::endl;
  //    auto it = monitorKeys.find(id);
  //    if (it == monitorKeys.end()) {
  //      monitorKeys.insert(std::make_pair(id, 0));
  //    }
  //  }
  //
  //  void deleteFromMonitorMap(long id) {
  //    std::lock_guard<std::mutex> guard(statsMutex);
  //    std::cout << "deleteFromMonitorMap: " << id << std::endl;
  //    auto it = monitorKeys.find(id);
  //
  //    if (it != monitorKeys.end()) {
  //      monitorKeys.erase(id);
  //    }
  //  }
  //
  //  void increaseMonitorMap(const std::string& fname) {
  //    std::lock_guard<std::mutex> guard(statsMutex);
  //
  //    auto itConverter = monitorConverter.find(fname);
  //    if (itConverter != monitorConverter.end()) {
  //      auto writesForDS = monitorKeys.find(itConverter->second);
  //      if (writesForDS != monitorKeys.end()) {
  //        int counter = writesForDS->second;
  //        counter++;
  //        writesForDS->second = counter;
  //      } else {
  //        monitorKeys.insert(std::make_pair(itConverter->second, 1));
  //      }
  //    }
  //  }
  //
  //  void printMonitorMap() {
  //    std::lock_guard<std::mutex> guard(statsMutex);
  //
  //    DEBUG(monitorKeys);
  //    DEBUG(monitorConverter);
  //
  //    std::cout << "printMonitorMap" << std::endl;
  //    for (auto it : keyIdsMap) {
  //      std::cout << it.first << " : [";
  //      for (auto vit : it.second) {
  //        std::cout << " (id: " << vit << " ";
  //        auto kit = monitorKeys.find(vit);
  //        if (kit != monitorKeys.end()) {
  //          std::cout << " value: " << kit->second << ") ";
  //        }
  //      }
  //      std::cout << " ]" << std::endl;
  //    }
  //  }
};

}  //  namespace storage
}  //  namespace deceve

#endif  // STORAGEMANAGER_HH
