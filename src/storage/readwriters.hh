#ifndef __FILE_HH__
#define __FILE_HH__

#include "../utils/defs.hh"
#include "../utils/require.hh"
#include "../storage/page.hh"
#include "../utils/util.hh"
#include <sys/types.h>
#include <iostream>
#include <string>
#include <fcntl.h>  // O_RDWR, O_CREAT
#include <cmath>    // ceil
#include <cstdlib>  // posix_memalign
#include <cstring>  // memset
#include <cstdio>
#include "../storage/BufferManager.hh"
#include "../storage/StorageManager.hh"
#include "../storage/BufferPool.hh"
#include "../storage/File.hh"

#include <mutex>
#include <atomic>
#include <thread>

namespace deceve {
namespace bama {

struct example {
  int a;
  int b;
  char c[8];

  friend std::ostream& operator<<(std::ostream& os, const example& ex) {
    os << ex.a << " " << ex.b << " " << ex.c << "\n";
    return os;
  }
};

template <typename R>
class Reader {
 public:
  typedef R record_type;
  typedef Page<R> page_type;

 private:
  deceve::storage::BufferManager* bufferManager;
  std::string filename;
  page_type* page;
  size_t num_pages;
  size_t current_page;
  size_t record_index;
  size_t marked_page;
  size_t marked_record;
  deceve::storage::FileMode mode;

 public:
  Reader(deceve::storage::BufferManager* bm,
         deceve::storage::FileMode m = deceve::storage::PRIMARY)
      : bufferManager(bm),
        filename(""),
        page(0),
        num_pages(0),
        current_page(0),
        record_index(0),
        marked_page(0),
        marked_record(0),
        mode(m){};

  Reader(deceve::storage::BufferManager* bm, const std::string& fn,
         deceve::storage::FileMode m = deceve::storage::PRIMARY)
      : bufferManager(bm),
        filename(fn),
        page(0),
        num_pages(0),
        current_page(0),
        record_index(0),
        marked_page(0),
        marked_record(0),
        mode(m) {
    open(filename);
  }

  ~Reader() {
    //  if (isOpen()) close();
  }

  void open(const std::string& fn) {
#ifdef MULTI_THREADED
    std::lock_guard<std::mutex> lock(
        bufferManager->getSM().readwritersMainMutex);
#endif

    filename = fn;
    // If the file doesn't exist than open it and update file metadata on the
    // storageManager
    offset_t size = bufferManager->getFileSizeFromCatalog(filename, mode);

    mode = bufferManager->getSM().getFileMode(filename);

    // std::cout << "READER: OPEN FILE: " << fn << " with MODE: " << mode <<
    // std::endl;

    //  COUT<<"Get size: "<<size<<"\n";
    if (size == 0) {
      //   COUT << "The file doesn't exist, so create a new one" << "\n";
      //      char *ptr = new char[deceve::bama::get_pagesize()];
      unsigned char* ptr = aligned_new(deceve::bama::get_pagesize());
      // ::memset(ptr, 0, deceve::bama::get_pagesize());
      bufferManager->writePage(filename, 0, (char*)ptr, mode);
      pinPage(0);
      aligned_delete(ptr);
      //      delete[] ptr;
      num_pages = 1;
      current_page = 0;
      record_index = 0;
      page = readPage(0);

    } else {
      num_pages = size / deceve::bama::get_pagesize();
      current_page = 0;
      record_index = 0;
      page = 0;
      page = readPage(current_page);
      pinPage(current_page);
    }

    //  COUT<<"----------Open END----------"<<"\n";
  }

  void open(const std::string& fn, deceve::storage::FileMode m) {
#ifdef MULTI_THREADED
    std::lock_guard<std::mutex> lock(
        bufferManager->getSM().readwritersMainMutex);
#endif

    mode = m;
    filename = fn;
    // If the file doesn't exist than open it and update file metadata on the
    // storageManager
    offset_t size = bufferManager->getFileSizeFromCatalog(filename, mode);
    //    COUT << "Open Reader Size of file " << filename << " is: " << size
    //        << "\n";
    if (size == 0) {
      //  COUT << "The file doesn't exist, so create a new one"
      //      << "\n";
      //      char *ptr = new char[deceve::bama::get_pagesize()];
      unsigned char* ptr = aligned_new(deceve::bama::get_pagesize());
      //      ::memset(ptr, 0, deceve::bama::get_pagesize());
      bufferManager->writePage(filename, 0, (char*)ptr, mode);
      pinPage(0);
      aligned_delete(ptr);
      //      delete[] ptr;
      num_pages = 1;
      current_page = 0;
      record_index = 0;
      page = readPage(0);

    } else {
      num_pages = size / deceve::bama::get_pagesize();
      current_page = 0;
      record_index = 0;
      page = 0;
      page = readPage(current_page);
      pinPage(current_page);
    }

    //    COUT << "Number of pages is: " << num_pages << "\n";
  }

  bool hasNext() {
#ifdef MULTI_THREADED
    std::lock_guard<std::mutex> lock(
        bufferManager->getSM().readwritersMainMutex);
#endif

    if (record_index < page->length()) {
      return true;
    }
    if (current_page + 1 < num_pages) {
      releasePage(current_page);
      page = 0;
      current_page++;
      record_index = 0;
      page = readPage(current_page);
      pinPage(current_page);
      return true;
    }
    return false;
  }

  void mark() {
#ifdef MULTI_THREADED
    std::lock_guard<std::mutex> lock(
        bufferManager->getSM().readwritersMainMutex);
#endif
    marked_page = current_page;
    marked_record = record_index;
  }

  void rollback() {
#ifdef MULTI_THREADED
    std::lock_guard<std::mutex> lock(
        bufferManager->getSM().readwritersMainMutex);
#endif
    releasePage(current_page);
    current_page = marked_page;
    page = readPage(current_page);
    pinPage(current_page);
    record_index = marked_record;
  }

  bool skip(size_t offset) {
#ifdef MULTI_THREADED
    std::lock_guard<std::mutex> lock(
        bufferManager->getSM().readwritersMainMutex);
#endif
    releasePage(0);
    if (current_page == num_pages - 1 && record_index == page->length())
      return false;
    if (record_index + offset < page->length()) {
      record_index += offset;
      return true;
    }

    offset -= (page->length() - record_index);
    size_t npgs = (size_t)::ceil((double)offset / page->capacity());
    record_index = offset - (npgs - 1) * page->capacity();
    if (current_page + npgs < num_pages) {
      current_page += npgs;
      page = readPage(current_page);
      pinPage(current_page);
      return true;
    } else {
      current_page = num_pages - 1;
      page = readPage(current_page);
      pinPage(current_page);
      record_index = page->length() - 1;
      return false;
    }
  }

  record_type nextRecord() { return page->get(record_index++); }

  void close() { releasePage(current_page); }

  bool isOpen() const { return bufferManager->getSM().isOpen(filename); }

  page_type* readPage(offset_t offt) {
    //  std::lock_guard<std::mutex>
    //  lock(bufferManager->getStorageManager().readerPinReleaseMutex);
    return (page_type*)bufferManager->readPage(
        filename, offt * deceve::bama::get_pagesize(), mode);
  }

  void writePage(offset_t offt, const page_type& p) {
    //  std::lock_guard<std::mutex>
    //  lock(bufferManager->getStorageManager().readerPinReleaseMutex);
    bufferManager->writePage(filename, offt * deceve::bama::get_pagesize(),
                             (char*)&p, mode);
  }

  void pinPage(offset_t offt) {
    //    COUT<<"Reader pin page: "<<filename<<" offset: "<<offt<<"\n";
    bufferManager->pinPage(filename, offt * deceve::bama::get_pagesize(), mode);
  }

  void releasePage(offset_t offt) {
    bufferManager->releasePage(filename, offt * deceve::bama::get_pagesize(),
                               mode);
  }

  // Check if the value returned is correct
  size_t numPages() { return num_pages; }

  size_t getCurrentPageNumber() { return current_page; }
};
//:~ class Reader

template <typename R>
class Writer {
 public:
  typedef R record_type;
  typedef Page<R> page_type;

 private:
  deceve::storage::BufferManager* bufferManager;
  std::string filename;
  page_type* page;
  page_type tmpPage;
  size_t num_pages;
  deceve::storage::FileMode mode;

 public:
  Writer(deceve::storage::BufferManager* bm,
         deceve::storage::FileMode m = deceve::storage::PRIMARY)
      : bufferManager(bm), filename(""), page(0), num_pages(0), mode(m){};

  Writer(deceve::storage::BufferManager* bm, const std::string& fn,
         deceve::storage::FileMode m = deceve::storage::PRIMARY)
      : bufferManager(bm), filename(fn), page(0), num_pages(0), mode(m) {
    open(filename);
  }

  ~Writer() {}

  void open(const std::string& fn) {
#ifdef MULTI_THREADED
    std::lock_guard<std::mutex> lock(
        bufferManager->getSM().readwritersMainMutex);
#endif

    num_pages = 0;
    page = 0;
    filename = fn;

    offset_t size = bufferManager->getFileSizeFromCatalog(filename, mode);
    mode = bufferManager->getSM().getFileMode(filename);

    // std::cout << "WRITER: OPEN FILE: " << fn << " with MODE: " << mode <<
    // std::endl;

    if (size == 0) {
      // There is no page so create a new one
      //      page_type *ptr = new page_type[deceve::bama::get_pagesize()];
      unsigned char* ptr = aligned_new(deceve::bama::get_pagesize());
      //      ::memset(ptr, 0, deceve::bama::get_pagesize());

      bufferManager->writePage(filename, 0, (char*)ptr, mode);
      //      writePage(0, *ptr);
      //      delete[] ptr;
      aligned_delete(ptr);
      num_pages = 1;
      page = readPage(0);
      pinPage(0);

    } else {
      // Get the number of pages of the file and read the last page
      num_pages = size / deceve::bama::get_pagesize();
      page = readPage(num_pages - 1);
      pinPage(num_pages - 1);
    }
  }

  void open(const std::string& fn, deceve::storage::FileMode m) {
#ifdef MULTI_THREADED
    std::lock_guard<std::mutex> lock(
        bufferManager->getSM().readwritersMainMutex);
#endif

    mode = m;
    num_pages = 0;
    page = 0;
    filename = fn;
    offset_t size = bufferManager->getFileSizeFromCatalog(filename, mode);
    if (size == 0) {
      // There is no page so create a new one
      //      page_type *ptr = new page_type[deceve::bama::get_pagesize()];
      unsigned char* ptr = aligned_new(deceve::bama::get_pagesize());
      //      ::memset(ptr, 0, deceve::bama::get_pagesize());

      //      writePage(0, *ptr);
      bufferManager->writePage(filename, 0, (char*)ptr, mode);
      aligned_delete(ptr);
      //      delete[] ptr;
      num_pages = 1;
      page = readPage(0);
      pinPage(0);

    } else {
      // Get the number of pages of the file and read the last page
      num_pages = size / deceve::bama::get_pagesize();
      page = readPage(num_pages - 1);
      pinPage(num_pages - 1);
      //      pinPage(num_pages - 1);
    }
  }

  void write(const record_type& r) {
#ifdef MULTI_THREADED
    std::lock_guard<std::mutex> lock(
        bufferManager->getSM().readwritersMainMutex);
#endif

    if (page->length() == page->capacity()) {
      writePage(num_pages - 1, *page);

      releasePage(num_pages - 1);

      num_pages++;

      //      page_type *ptr = new page_type[deceve::bama::get_pagesize()];
      //      ::memset(ptr, 0, deceve::bama::get_pagesize());

      unsigned char* ptr = aligned_new(deceve::bama::get_pagesize());

      bufferManager->writePage(filename,
                               (num_pages - 1) * deceve::bama::get_pagesize(),
                               (char*)ptr, mode);
      // offt * deceve::bama::get_pagesize()
      //      writePage(num_pages - 1, *ptr);

      aligned_delete(ptr);
      //      delete[] ptr;

      page = readPage(num_pages - 1);
      pinPage(num_pages - 1);
    }
    page->add(r);

    // COUT<<"Record: "<<r<<"\n";
  }

  void close() { flush(); }
  void flush() {
    if (page->length() > 0) {
      writePage((num_pages - 1), *page);
    }
    releasePage(num_pages - 1);
  }
  bool isOpen() const { return bufferManager->getSM().isOpen(filename); }

  // read a page from the file using the buffer manager
  page_type* readPage(offset_t offt) {
    //    //  std::lock_guard<std::mutex>
    //    lock(bufferManager->getStorageManager().readerPinReleaseMutex);
    return (page_type*)bufferManager->readPage(
        filename, offt * deceve::bama::get_pagesize(), mode);
  }

  // write a page to the file using the buffer manager
  void writePage(offset_t offt, const page_type& p) {
    //  std::lock_guard<std::mutex>
    //  lock(bufferManager->getStorageManager().readerPinReleaseMutex);
    bufferManager->writePage(filename, offt * deceve::bama::get_pagesize(),
                             (char*)&p, mode);
  }

  void pinPage(offset_t offt) {
    bufferManager->pinPage(filename, offt * deceve::bama::get_pagesize(), mode);
  }

  void releasePage(offset_t offt) {
    bufferManager->releasePage(filename, offt * deceve::bama::get_pagesize(),
                               mode);
  }

  std::string getFileName() { return filename; }

  size_t getNumPages() { return num_pages; }
};
//:~ class Writer

template <typename R, size_t S = 512>
class BulkProcessor {
 public:
  typedef R record_type;
  typedef Page<R> page_type;

 private:
  deceve::storage::BufferManager* bufferManager;
  std::string filename;
  size_t num_pages;
  deceve::storage::FileMode mode;

 public:
  BulkProcessor(deceve::storage::BufferManager* bm,
                deceve::storage::FileMode m = deceve::storage::PRIMARY)
      : bufferManager(bm), filename(""), num_pages(0), mode(m){};

  BulkProcessor(deceve::storage::BufferManager* bm, const std::string& fn,
                deceve::storage::FileMode m = deceve::storage::PRIMARY)
      : bufferManager(bm), filename(fn), num_pages(0), mode(m) {
    open(filename);
  }

  ~BulkProcessor() {}

  void open(const std::string& fn) {
    std::lock_guard<std::mutex> lock(
        bufferManager->getSM().readerOpenFilesMutex);

    filename = fn;

    offset_t size = bufferManager->getFileSizeFromCatalog(filename, mode);

    mode = bufferManager->getSM().getFileMode(filename);

    if (size == 0) {
      //      char *ptr = new char[deceve::bama::get_pagesize()];
      unsigned char* ptr = aligned_new(deceve::bama::get_pagesize());
      //      ::memset(ptr, 0, deceve::bama::get_pagesize());
      bufferManager->writePage(filename, 0, (char*)ptr, mode);
      //      delete[] ptr;
      aligned_delete(ptr);
      num_pages = 1;

    } else {
      num_pages = size / deceve::bama::get_pagesize();
    }
  }

  void bmBulkRead(size_t o, page_type* pages, size_t& ps) {
    size_t np = num_pages;
    if ((size_t)o + ps > (size_t)np) ps = np - o;

    for (size_t i = o; i < o + ps; i++) {
      ::memcpy((char*)pages + (i - o) * deceve::bama::get_pagesize(),
               (page_type*)readPage(i), deceve::bama::get_pagesize());
      bufferManager->pinPage(filename, i * deceve::bama::get_pagesize(), mode);
    }
  }

  void mikeBulkRelease(size_t o, page_type* pages, size_t& ps) {
    (void)pages;
    size_t np = num_pages;
    if ((size_t)o + ps > (size_t)np) ps = np - o;

    for (size_t i = o; i < o + ps; i++) {
      bufferManager->releasePage(filename, i * deceve::bama::get_pagesize(),
                                 mode);
    }
  }

  void bmReleasePages(size_t o, size_t& ps) {
    size_t np = num_pages;

    if ((size_t)o + ps > (size_t)np) ps = np - o;

    for (size_t i = o; i < o + ps; i++) {
      bufferManager->releasePage(filename, i * deceve::bama::get_pagesize(),
                                 mode);
    }
  }

  page_type* bmBulkRead(size_t o, size_t& ps) {
    size_t np = num_pages;

    if ((size_t)o + ps > (size_t)np) ps = np - o;

    //    char *pages = new char[deceve::bama::get_pagesize() * ps];
    //    ::memset(pages, 0, deceve::bama::get_pagesize() * ps);

    unsigned char* pages = aligned_new(deceve::bama::get_pagesize() * ps);
    //     ::memset(pages, 0, deceve::bama::get_pagesize() * ps);

    for (size_t i = o; i < o + ps; i++) {
      ::memcpy(pages + (i - o) * deceve::bama::get_pagesize(),
               (page_type*)readPage(i), deceve::bama::get_pagesize());
      bufferManager->pinPage(filename, i * deceve::bama::get_pagesize(), mode);
    }

    return (page_type*)pages;
  }

  void bmBulkWrite(size_t o, page_type* pgs, size_t ps) {
    //    char * tempPage = new char[deceve::bama::get_pagesize()];
    unsigned char* tempPage = aligned_new(deceve::bama::get_pagesize());
    //    ::memset(tempPage, 0, deceve::bama::get_pagesize());

    for (size_t i = o; i < o + ps; i++) {
      ::memcpy(tempPage, (char*)pgs + (i - o) * deceve::bama::get_pagesize(),
               deceve::bama::get_pagesize());

      bufferManager->writePage(filename, (i)*deceve::bama::get_pagesize(),
                               (char*)tempPage, mode);
    }
    //    delete[] tempPage;
    aligned_delete(tempPage);
  }

  record_type* bulkRead(size_t o, size_t& ps, size_t& len) {
#ifdef MULTI_THREADED
    std::lock_guard<std::mutex> lock(
        bufferManager->getSM().readwritersMainMutex);
#endif
    page_type* pages = bmBulkRead(o, ps);
    record_type* recs = (record_type*)pages;
    len = compact(recs, ps);
    return recs;
  }

  void releasePages(size_t o, size_t& ps) {
#ifdef MULTI_THREADED
    std::lock_guard<std::mutex> lock(
        bufferManager->getSM().readwritersMainMutex);
#endif

    bmReleasePages(o, ps);
  }

  void bulkRead(size_t o, record_type* recs, size_t& ps, size_t& len) {
    bmBulkRead(o, (page_type*)recs, ps);
    len = compact(recs, ps);
  }

  void bulkReleaseMike(size_t o, record_type* recs, size_t& ps, size_t& len) {
    (void)len;
    mikeBulkRelease(o, (page_type*)recs, ps);
  }

  void bulkWrite(size_t o, record_type* recs, size_t len, bool bpa = true) {
    record_type* nrecs;
    if (!bpa)
      nrecs = allocate(len);
    else
      nrecs = recs;
    size_t np = expand(nrecs, len);
    bmBulkWrite(o, (page_type*)nrecs, np);
    if (!bpa) aligned_delete((unsigned char*)nrecs);
  }

  void close() {
    //    bufferManager->closeFile(filename);
  }
  bool isOpen() const { return bufferManager->getSM().isOpen(filename); }

 private:
  size_t compact(record_type* recs, size_t np) const {
    char* p = (char*)recs;
    size_t len = 0;
    size_t nr = 0;
    for (size_t i = 0; i < np; i++) {
      nr = ((page_type*)p)->length();
      ::memmove((char*)(&recs[len]), p + page_type::header_size,
                sizeof(record_type) * nr);
      len += nr;
      p += deceve::bama::get_pagesize();
    }
    return len;
  }

  size_t expand(record_type* recs, size_t len) const {
    size_t np = numPages(len);
    size_t capacity = (deceve::bama::get_pagesize() - page_type::header_size) /
                      sizeof(record_type);
    char* p = (char*)recs;
    size_t i = np - 1;
    p += (deceve::bama::get_pagesize() * i);
    size_t nrecs = (len % capacity == 0 ? capacity : len % capacity);
    len -= nrecs;
    size_t prev_recs = nrecs;
    ::memmove(&p[page_type::header_size], (char*)(&recs[len]),
              sizeof(record_type) * nrecs);
    i--;
    p -= deceve::bama::get_pagesize();
    nrecs = capacity;
    while (i + 1 >= 1) {
      len -= nrecs;
      ::memmove(&p[page_type::header_size], (char*)(&recs[len]),
                sizeof(record_type) * nrecs);
      *((size_t*)(p + deceve::bama::get_pagesize())) = prev_recs;
      prev_recs = nrecs;
      p -= get_pagesize();
      i--;
    }
    *((size_t*)(p + deceve::bama::get_pagesize())) = prev_recs;
    return np;
  }

  record_type* allocate(size_t len) const {
    return (record_type*)aligned_new(numPages(len));
  }

  size_t numPages(size_t l) const {
    return ((double)l / ((double)(deceve::bama::get_pagesize() -
                                  page_type::header_size) /
                         sizeof(record_type)) +
            0.5);
  }

  // read a page from the file using the buffer manager
  page_type* readPage(offset_t offt) {
    //  std::lock_guard<std::mutex>
    //  lock(bufferManager->getStorageManager().readerPinReleaseMutex);
    return (page_type*)bufferManager->readPage(
        filename, offt * deceve::bama::get_pagesize(), mode);
  }

  // write a page to the file using the buffer manager
  void writePage(offset_t offt, const page_type& p) {
    //  std::lock_guard<std::mutex>
    //  lock(bufferManager->getStorageManager().readerPinReleaseMutex);
    bufferManager->writePage(filename, offt * deceve::bama::get_pagesize(),
                             (char*)&p);
  }

  void pinPage(offset_t offt) {
    bufferManager->pinPage(filename, offt * deceve::bama::get_pagesize(), mode);
  }

  void releasePage(offset_t offt) {
    bufferManager->releasePage(filename, offt * deceve::bama::get_pagesize(),
                               mode);
  }
};

}  //:~ namespace bama
}  //:~ namespace deceve

#endif
