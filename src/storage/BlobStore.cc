#include <iostream>
#include <string>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <unistd.h>
#include "../storage/BlobStore.hh"

namespace deceve { namespace storage {

BlobStore::BlobStore(const std::string& fn)
  : m_map(0), m_filename(fn), m_fileDescriptor(-1), 
    m_allocated(0), m_occupied(0) {
  m_fileDescriptor = ::open(m_filename.c_str(), O_RDWR | O_CREAT, 0644);
  if (m_fileDescriptor != -1) {
    struct stat sbuf;
    fstat(m_fileDescriptor, &sbuf);
    if (sbuf.st_size == 0) {
      char pad[PAGE_SIZE];
      ::memcpy((void *) pad, (void *) &m_occupied, sizeof(m_occupied));
      ::write(m_fileDescriptor, pad, PAGE_SIZE);
      expand();
      m_allocated = CHUNK_SIZE;
      m_occupied = 0;
    }
    else {
      m_allocated = sbuf.st_size - PAGE_SIZE;
      ::lseek(m_fileDescriptor, 0, SEEK_SET);
      ::read(m_fileDescriptor, (void *) &m_occupied, sizeof(m_occupied));
    }
  }
  map();
}

BlobStore::~BlobStore() {
  ::lseek(m_fileDescriptor, 0, SEEK_SET);
  char pad[PAGE_SIZE];
  ::memcpy((void *) pad, (void *) &m_occupied, sizeof(m_occupied));
  ::write(m_fileDescriptor, pad, PAGE_SIZE);
  unmap();
  ::close(m_fileDescriptor);
}

bool BlobStore::read(off_t where, unsigned char *data, size_t &len) const {
  bool presence = (m_map[where++] != 0);
  if (presence) {
    len = *((size_t *) &m_map[where]);
    where += sizeof(len);
    ::memcpy((void *) data, (void *) &m_map[where], len);
  }
  return presence;
}

bool BlobStore::allocateAndRead(off_t where, unsigned char **data, 
				size_t &len) const {
  *data = 0; 
  len = 0;
  bool presence = (m_map[where++] != 0);
  if (presence) {
    len = *((size_t *) &m_map[where]);
    where += sizeof(len);
    *data = new unsigned char [len];
    if (*data) { ::memcpy((void *) *data, (void *) &m_map[where], len); }
  }
  return presence;
}

void BlobStore::append(unsigned char *data, size_t len) {
  while (m_occupied + 1 + len >= m_allocated) { unmap(); expand(); map(); }
  ::memcpy((void *) &m_map[m_occupied], "1", 1);
  ::memcpy((void *) &m_map[m_occupied + 1], (void *) &len, sizeof(len));
  ::memcpy((void *) &m_map[m_occupied + 1 + sizeof(len)], (void *) data, len);
  m_occupied += 1 + sizeof(len) + len;
}

void BlobStore::write(off_t where, unsigned char* data, size_t len) {
  while (where + 1 + len >= m_allocated) { unmap(); expand(); map(); }
  ::memcpy((void *) &m_map[where], "1", 1);
  ::memcpy((void *) &m_map[where + 1], (void *) &len, sizeof(len));
  ::memcpy((void *) &m_map[where + 1 + sizeof(len)], (void *) data, len);
  if (where + 1 + len > m_occupied) { m_occupied = where + 1 + len; }
}

bool BlobStore::invalidate(off_t where) {
  if (where >= (off_t) m_occupied) return false;
  ::memset((void *) &m_map[where], 0, 1);
  return true;
}
    
void BlobStore::map() {
  m_map = (unsigned char*) ::mmap((::caddr_t) 0, 
				  m_allocated, PROT_READ | PROT_WRITE,
				  MAP_SHARED, m_fileDescriptor, PAGE_SIZE);
}

void BlobStore::unmap() {
  ::munmap((void *) m_map, m_allocated);
}

void BlobStore::expand() {
  ::lseek(m_fileDescriptor, CHUNK_SIZE - 1, SEEK_END);
  ::write(m_fileDescriptor, "1", 1);
  m_allocated += CHUNK_SIZE;
}

}}
