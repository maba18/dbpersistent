#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# check if tpch files are already generated
FILE=./files/lineitem.tbl

if [ ! -f "$FILE" ]
then
    echo "File $FILE does not exists"
    echo "DOWNLOAD AND GENERATE INITIAL TPCH-WORKLOAD FILES"
    cd tpchWorkloadFiles/
    ./createBenchmarkFiles.sh
else
	make -j 4 runs/startDatabase
fi
