#ifndef __SELECTIONSORT_HH__
#define __SELECTIONSORT_HH__

#include "../storage/page.hh"
#include "../storage/io.hh"
#include "../storage/identity.hh"
#include <sstream>
#include <sys/stat.h>
#include <algorithm>
#include <cstdio>
#include <vector>
#include "../storage/BufferManager.hh"
#include "../utils/types.hh"
#include "../utils/helpers.hh"

namespace deceve { namespace bama {

template <typename Record,
          typename Extract = Identity<Record>,
          typename Compare = std::less<typename Extract::key_type> >
class ReadOnlySelectionSort {
public:
    typedef Record record_type;
    typedef typename Extract::key_type key_type;
    
private:
    typedef Reader<record_type> reader_type;
    typedef Writer<record_type> writer_type;
    typedef BulkProcessor<record_type> bulk_processor_type;
    
public:    
    ReadOnlySelectionSort(deceve::storage::BufferManager *bm, const std::string& fn, 
                          const std::string& of,
                          unsigned int nb = 1024,
                          size_t i = 0)
        : bufferManager(bm), filename(fn), outfile(of), number_of_buffers(nb),
          number_of_ignored_records(i),
          extractor(Extract()), comparator(Compare()),
          value_comparator(extractor, comparator),
          heap_comparator(extractor, comparator),
          sort_comparator(extractor, comparator) {
    }
    
    ~ReadOnlySelectionSort() {}
    
    void sort() {
        writer_type writer(bufferManager);
        std::vector<std::pair<record_type, size_t> > heap;
        std::pair<key_type, size_t>* max_values = 0;
        size_t limit = 0;
        size_t length = 0;
        size_t processed = 0;
        
        writer.open(outfile);
        /*
        if (number_of_ignored_records != 0) {
            require(writer.skip(number_of_ignored_records), "could not skip");
        }
        */
        bool more_passes = first_pass(writer, max_values, heap,
                                      &limit, &length, &processed);
        while (more_passes) {
            more_passes = make_pass(writer, max_values, heap,
                                    limit, length, &processed);
        }
        if (max_values) delete max_values;
        writer.close();
        //std::cout << "processed " << processed << "\n";
    }
    
private:
    
    class value_cmp {
    private:
        const Extract& cmp_extract;
        const Compare& cmp_predicate;
    public:
        value_cmp(const Extract& e, const Compare& p)
            : cmp_extract(e), cmp_predicate(p) {}
        bool operator()(const record_type& x, const record_type& y) const {
            return cmp_predicate(cmp_extract(x), cmp_extract(y));
        }
    };
    
    class heap_cmp {
    private:
        const Extract& cmp_extract;
        const Compare& cmp_predicate;
    public:
        heap_cmp(const Extract& e, const Compare& p)
            : cmp_extract(e), cmp_predicate(p) {}
        bool operator()(const std::pair<record_type, size_t>& x,
                        const std::pair<record_type, size_t>& y) const {
            bool less = cmp_predicate(cmp_extract(x.first),
                                       cmp_extract(y.first));
            bool equal = !less && !cmp_predicate(cmp_extract(y.first),
                                                 cmp_extract(x.first));
            return equal ? x.second < y.second : less;
        }
    };

    class sort_cmp {
    private:
        const Extract& cmp_extract;
        const Compare& cmp_predicate;
    public:
        sort_cmp(const Extract& e, const Compare& p)
            : cmp_extract(e), cmp_predicate(p) {}
        bool operator()(const std::pair<record_type, size_t>& x,
                        const std::pair<record_type, size_t>& y) const {
            bool less = cmp_predicate(cmp_extract(x.first),
                                       cmp_extract(y.first));
            bool equal = !less && !cmp_predicate(cmp_extract(y.first),
                                                 cmp_extract(x.first));
            return equal ? x.second < y.second : less;
        }
    };


    bool first_pass(writer_type& writer,
                    std::pair<key_type, size_t>*& max_values,
                    std::vector<std::pair<record_type, size_t> >& heap,
                    size_t* limit,
                    size_t* position,
                    size_t* processed) {
        *limit = ((number_of_buffers-2) * get_pagesize()) / sizeof(record_type);
        reader_type reader(bufferManager);
        record_type record;
        
        heap.clear();
        reader.open(filename);
        if (number_of_ignored_records != 0) {
            //std::cout << "skipping " << number_of_ignored_records << "\n";
            require(reader.skip(number_of_ignored_records), "could not skip");
        }
        *position = number_of_ignored_records;
        while (reader.hasNext()) {
            record = reader.nextRecord();
            if (heap.size() < *limit) {
                heap.push_back(std::make_pair(record, *position));
                std::push_heap(heap.begin(), heap.end(), heap_comparator);
            }
            else {
                std::pair<record_type, size_t> top = heap[0];
                if (value_comparator(record, top.first)) {
                    std::pop_heap(heap.begin(), heap.end(),
                                  heap_comparator);
                    heap[heap.size()-1] = std::make_pair(record, *position);
                    std::push_heap(heap.begin(), heap.end(),
                                   heap_comparator);
                }
            }
            (*position)++;
        }
        // we have extracted the minimal set, now write it out
        //std::cout << "made pass, length = " << (*position) << "\n";
        dump_heap(heap, writer, max_values);
        (*processed) = number_of_ignored_records + heap.size();
        // close the input
        reader.close();
        return (*processed) < (*position);
    }

    bool make_pass(writer_type& writer,
                   std::pair<key_type, size_t>*& max_values,
                   std::vector<std::pair<record_type, size_t> >& heap,
                   size_t limit,
                   size_t length,
                   size_t* processed) {
        reader_type reader(bufferManager);
        record_type record;
        size_t position;
        
        heap.clear();
        reader.open(filename);
        if (number_of_ignored_records != 0) {
            //std::cout << "skipping " << number_of_ignored_records << "\n";
            require(reader.skip(number_of_ignored_records), "could not skip");
        }
        position = number_of_ignored_records;
        while (reader.hasNext()) {
            record = reader.nextRecord();
            bool less = value_comparator(max_values->first, record);
            bool equal = !less && !value_comparator(record, max_values->first);
            if (less || (equal && max_values->second < position)) {
                if (heap.size() < limit) {
                    heap.push_back(std::make_pair(record, position));
                    std::push_heap(heap.begin(), heap.end(), heap_comparator);
                }
                else {
                    std::pair<record_type, size_t> top = heap[0];
                    if (value_comparator(record, top.first)) {
                        std::pop_heap(heap.begin(), heap.end(),
                                      heap_comparator);
                        heap[heap.size()-1] = std::make_pair(record, position);
                        std::push_heap(heap.begin(), heap.end(),
                                       heap_comparator);
                    }
                }
            }
            position++;
        }
        // we have extracted the minimal set, now write it out
        //std::cout << "made pass, length = " << (*position) << "\n";
        dump_heap(heap, writer, max_values);
        (*processed) += heap.size();
        // close the input
        reader.close();
        return (*processed) < length;
    }

    void dump_heap(std::vector<std::pair<record_type, size_t> >& heap,
                   writer_type& writer,
                   std::pair<key_type, size_t>*& max_values) {
        std::sort(heap.begin(), heap.end(), sort_comparator);
        for (typename std::vector<std::pair<record_type,
                 size_t> >::const_iterator it = heap.begin();
             it != heap.end(); it++) {
            writer.write(it->first);
        }
        if (! max_values) {
            max_values = new std::pair<key_type,
                                       size_t>(heap[heap.size()-1].first,
                                               heap[heap.size()-1].second);
        }
        else {
            max_values->first = heap[heap.size()-1].first;
            max_values->second = heap[heap.size()-1].second;
        }
    }

private:
    deceve::storage::BufferManager *bufferManager;
    std::string filename;
    std::string outfile;
    const size_t number_of_buffers;
    const size_t number_of_ignored_records;
    Extract extractor;
    Compare comparator;
    value_cmp value_comparator;
    heap_cmp heap_comparator;
    sort_cmp sort_comparator;
};

/**
 *
 */

template <typename Record,
          typename Extract = Identity<Record>,
          typename Compare = std::less<typename Extract::key_type> >
class ReadWriteSelectionSort {
public:
    typedef Record record_type;
    typedef typename Extract::key_type key_type;
    
private:
    typedef Reader<record_type> reader_type;
    typedef Writer<record_type> writer_type;
    typedef BulkProcessor<record_type> bulk_processor_type;
    
public:    
    ReadWriteSelectionSort(deceve::storage::BufferManager *bm, const std::string& fn, 
                           const std::string& of,
                           const Extract& ex,
                           size_t nb = 1024,
                           float r = 2.0)
        : bufferManager(bm), filename(fn), temp_filename(fn), outfile(of), 
          ratio(r), number_of_buffers(nb),
          extractor(ex), comparator(Compare()),
          value_comparator(extractor, comparator),
          heap_comparator(extractor, comparator),
          sort_comparator(extractor, comparator),
        first_swap(true) {
        temp_filename.append(".1");
    }
    
    ~ReadWriteSelectionSort() {}
    
    void sort() {


     //   std::cout<<"temp_filename: "<<temp_filename<<"\n";
        writer_type writer(bufferManager);
        std::vector<std::pair<record_type, size_t> > heap;
        std::pair<record_type, size_t>* max_values = 0;
        size_t limit = 0;
        size_t length = 0;
        size_t processed = 0;
        size_t passes = 1;
        
        writer.open(outfile);        
        bool more_passes = true;        
        while (more_passes) {
            //std::cout << "passes is " << passes << "\n";
            if (passes == 1) {
                more_passes = first_pass(writer, max_values, heap,
                                         &limit, &length, &processed, &passes);
            }
            else {
                more_passes = make_pass(writer, max_values, heap,
                                        limit, length, &processed, &passes);
            }
        }
        if (max_values) delete max_values;
        if (! first_swap) {
            //std::cout << "file is " << filename << "\n";
          //  fs::remove(filename);
            
            bufferManager->removeFile(filename);
            //require(::remove(filename.c_str()) == 0,
            //        "could not delete original file.");
            //std::cout << "temp file is " << temp_filename << "\n";
            //require(::remove(temp_filename.c_str()) == 0,
            //        "could not delete temporary file.");
        }
    }
    
private:
    
    class value_cmp {
    private:
        const Extract& cmp_extract;
        const Compare& cmp_predicate;
    public:
        value_cmp(const Extract& e, const Compare& p)
            : cmp_extract(e), cmp_predicate(p) {}
        bool operator()(const record_type& x, const record_type& y) const {
            return cmp_predicate(cmp_extract(x), cmp_extract(y));
        }
    };
    
    class heap_cmp {
    private:
        const Extract& cmp_extract;
        const Compare& cmp_predicate;
    public:
        heap_cmp(const Extract& e, const Compare& p)
            : cmp_extract(e), cmp_predicate(p) {}
        bool operator()(const std::pair<record_type, size_t>& x,
                        const std::pair<record_type, size_t>& y) const {
            bool less = cmp_predicate(cmp_extract(x.first),
                                       cmp_extract(y.first));
            bool equal = !less && !cmp_predicate(cmp_extract(y.first),
                                                 cmp_extract(x.first));
            return equal ? x.second < y.second : less;
        }
    };

    class sort_cmp {
    private:
        const Extract& cmp_extract;
        const Compare& cmp_predicate;
    public:
        sort_cmp(const Extract& e, const Compare& p)
            : cmp_extract(e), cmp_predicate(p) {}
        bool operator()(const std::pair<record_type, size_t>& x,
                        const std::pair<record_type, size_t>& y) const {
            bool less = cmp_predicate(cmp_extract(x.first),
                                       cmp_extract(y.first));
            bool equal = !less && !cmp_predicate(cmp_extract(y.first),
                                                 cmp_extract(x.first));
            return equal ? x.second < y.second : less;
        }
    };


    bool first_pass(writer_type& writer,
                    std::pair<record_type, size_t>*& max_values,
                    std::vector<std::pair<record_type, size_t> >& heap,
                    size_t* limit,
                    size_t* position,
                    size_t* processed,
                    size_t *passes) {
        reader_type reader(bufferManager);
        record_type record;
        bool materialize;
        writer_type temp_writer(bufferManager);

        *limit = ((number_of_buffers-2) * get_pagesize()) / sizeof(record_type);
        //std::cout << "limit is " << *limit << "\n";
        *passes = 1;
        reader.open(filename);
        materialize = (*passes >= (reader.numPages() / number_of_buffers)
                       * (ratio / (ratio+1)));
        //std::cout << "materialize is " << materialize << "\n";
        if (materialize) { temp_writer.open(temp_filename); }
        heap.clear();
        *processed = 0;
        *position = 0;
        while (reader.hasNext()) {
            record = reader.nextRecord();
            //std::cout << "read " << record << "\n";
            if (heap.size() < *limit) {
                heap.push_back(std::make_pair(record, *position));
                std::push_heap(heap.begin(), heap.end(), heap_comparator);
            }
            else {
                std::pair<record_type, size_t> top = heap[0];                
                if (value_comparator(record, top.first)) {
                    if (materialize) { temp_writer.write(top.first); }
                    std::pop_heap(heap.begin(), heap.end(),
                                  heap_comparator);
                    heap[heap.size()-1] = std::make_pair(record, *position);
                    std::push_heap(heap.begin(), heap.end(),
                                   heap_comparator);
                }
                else if (materialize) { temp_writer.write(record); }
            }
            (*position)++;
        }
        //std::cout << "scanned" << "\n";
        // we have extracted the minimal set, now write it out
        if (! heap.empty()) { dump_heap(heap, writer, max_values); }
        //std::cout << "dumped" << "\n";
        (*processed) = heap.size();
        // close the input
        reader.close();
        if (materialize) { swap_files(temp_writer, max_values, passes); }
        (*passes)++;
        return (*processed) < (*position);
    }

    bool make_pass(writer_type& writer,
                   std::pair<record_type, size_t>*& max_values,
                   std::vector<std::pair<record_type, size_t> >& heap,
                   size_t limit,
                   size_t length,
                   size_t* processed,
                   size_t *passes) {
        reader_type reader(bufferManager);
        record_type record;
        size_t position;
        bool materialize;
        writer_type temp_writer(bufferManager);
        
        heap.clear();
        reader.open(filename);
        position = 0;
        materialize = (*passes >= (reader.numPages() / number_of_buffers)
                       * (ratio / (ratio+1)));
        if (materialize) { temp_writer.open(temp_filename); }
        while (reader.hasNext()) {
            record = reader.nextRecord();
            bool less = value_comparator(max_values->first, record);
            bool equal = !less && !value_comparator(record, max_values->first);
            if (less || (equal && max_values->second < position)) {
                if (heap.size() < limit) {
                    heap.push_back(std::make_pair(record, position));
                    std::push_heap(heap.begin(), heap.end(), heap_comparator);
                }
                else {
                    std::pair<record_type, size_t> top = heap[0];
                    if (value_comparator(record, top.first)) {
                        if (materialize) { temp_writer.write(top.first); }
                        std::pop_heap(heap.begin(), heap.end(),
                                      heap_comparator);
                        heap[heap.size()-1] = std::make_pair(record, position);
                        std::push_heap(heap.begin(), heap.end(),
                                       heap_comparator);
                    }
                    else if (materialize) { temp_writer.write(record); }
                }
            }
            position++;
        }
        // we have extracted the minimal set, now write it out
        //std::cout << "made pass, length = " << position << "\n";
        if (! heap.empty()) { dump_heap(heap, writer, max_values); }
        (*processed) += heap.size();
        // close the input
        reader.close();
        if (materialize) { swap_files(temp_writer, max_values, passes); }
        (*passes)++;
        return (*processed) < length;
    }

    void dump_heap(std::vector<std::pair<record_type, size_t> >& heap,
                   writer_type& writer,
                   std::pair<record_type, size_t>*& max_values) {
        std::sort(heap.begin(), heap.end(), sort_comparator);
        //std::cout << "sorted " << "\n";
        for (typename std::vector<std::pair<record_type,
                 size_t> >::const_iterator it = heap.begin();
             it != heap.end(); it++) {
            //std::cout << "wrote " << it->first << "\n";
            writer.write(it->first);
        }
        //std::cout << "wrote " << "\n";
        if (! max_values) {
            //std::cout << "alloc " << heap[heap.size()-1].first
            //          << " - " << heap[heap.size()-1].second << "\n";
            max_values = new std::pair<record_type,
                                       size_t>(heap[heap.size()-1].first,
                                               heap[heap.size()-1].second);
            //std::cout << "alloced" << "\n";
        }
        else {
            max_values->first = heap[heap.size()-1].first;
            max_values->second = heap[heap.size()-1].second;
        }
    }

    void swap_files(writer_type &writer,
                    std::pair<record_type, size_t>*& max_values,
                    size_t* passes) {
        writer.close();
        if (first_swap) {
            filename = std::string(temp_filename);
            temp_filename.append(".2");
            first_swap = false;
        }
        else {
            bufferManager->removeFile(filename);

   //         std::cout<<"Before renaming"<<"\n";

   //         	bufferManager->getBufferPool().printPages();

	//			bufferManager->getStorageManager().printPrimaryFiles();

	//			bufferManager->getStorageManager().printOpenFiles();

	//			bufferManager->getStorageManager().printFileStatistics();


            bufferManager->renameFile(temp_filename, filename);


     //   	std::cout<<"After renaming"<<"\n";

  //      			bufferManager->getBufferPool().printPages();

    //    			bufferManager->getStorageManager().printPrimaryFiles();

     //   			bufferManager->getStorageManager().printOpenFiles();

   //     			bufferManager->getStorageManager().printFileStatistics();



           // fs::remove(filename);
            //fs::rename(temp_filename, filename);
            /*
            require(::remove(filename.c_str()) == 0,
                    "could not delete original file.");
            require(::rename(temp_filename.c_str(), filename.c_str()) == 0,
                    "could not rename files.");
            */
        }
        *passes = 0;
        if (max_values) { delete max_values; max_values = 0; }
    }

private:
    deceve::storage::BufferManager *bufferManager;
    std::string filename;
    std::string temp_filename;
    std::string outfile;
    const float ratio;
    const size_t number_of_buffers;
    Extract extractor;
    Compare comparator;
    value_cmp value_comparator;
    heap_cmp heap_comparator;
    sort_cmp sort_comparator;
    bool first_swap;
};

}}

#endif
