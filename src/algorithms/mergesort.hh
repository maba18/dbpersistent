#ifndef __MERGESORT_HH__
#define __MERGESORT_HH__

#include "../storage/page.hh"
#include "../storage/io.hh"
#include <sstream>
#include <sys/stat.h>
#include <algorithm>
#include <cstdio>
#include <vector>

namespace deceve {

template<typename K, typename P>
class MergeSort {
public:
	typedef Record<K, P> record_type;

private:
	typedef Reader<K, P> reader_type;
	typedef Writer<K, P> writer_type;
	typedef BulkProcessor<K, P> bulk_processor_type;

public:

	MergeSort(const std::string& fn, size_t nb) :
			MergeSort(fn, fn, fn, nb) {
	}
	;

	MergeSort(const std::string& fn, const std::string& temp,
			const std::string& out, size_t nb) :
			filename(fn), temp_prefix(temp), outfile(out), number_of_runs(0), processed_so_far(
					0), number_of_buffers(nb) {
	}
	~MergeSort() {
	}

	void sort() {
		create_runs_bulk();
		//dump_runs();
		/* Commented by Mike
		 merge_runs();
		 remove_runs();
		 * /
		 */
		//dump();
	}

private:

	void create_runs_bulk() {
		bulk_processor_type bulk_reader(filename);
		bulk_processor_type bulk_writer;
		number_of_runs = 0;

		size_t np = bulk_reader.numPages();
		size_t pages_so_far = 0;
		size_t buffers = number_of_buffers;
		size_t num_recs;
		record_type* recs = (record_type*) aligned_new(number_of_buffers * S);
		while (pages_so_far < np) {
			bulk_reader.bulkRead(pages_so_far, recs, buffers, num_recs);
			std::sort(recs, recs + num_recs);
			bulk_writer.open(generate_run());
			bulk_writer.bulkWrite(0, recs, num_recs);
			bulk_writer.close();
			pages_so_far += buffers;
		}
		aligned_delete((unsigned char*) recs);
		bulk_reader.close();
	}

	void create_runs() {
		reader_type reader(filename);
		writer_type writer;
		number_of_runs = 0;

		unsigned int count = 0;
		// we need -2 from the number of buffer pool pages since we have to
		// account for the two pages of the reader and writer
		const unsigned int limit = ((number_of_buffers - 2) * S)
				/ sizeof(record_type);
		record_type records[limit];
		unsigned int idx = 0;
		while (reader.hasNext()) {
			// load up the buffer
			records[idx++] = reader.nextRecord();
			if (idx == limit) {
				// sort the contents of the buffer
				std::sort(records, records + limit);
				// open the writer and dump the contents
				writer.open(generate_run());
				//writer_type writer(generate_run());
				for (unsigned int i = 0; i < limit; i++) {
					writer.write(records[i]);
					count++;
				}
				// then close the writer
				writer.close();
				idx = 0;
			}
		}

		// last run is always smaller, so pick up the spares and write them
		// in a separate run
		std::sort(records, records + idx);
		writer.open(generate_run());
		for (unsigned int i = 0; i < idx; i++) {
			writer.write(records[i]);
			count++;
		}
		writer.close();

		// reset the bookkeeping so we know nothing has been processed
		processed_so_far = 0;
		delete[] records;
	}

	void merge_runs() {
		//require(::remove(filename.c_str()) == 0,
		//        "could not delete input file.");
		writer_type output;
		do {
			std::pair<unsigned int, unsigned int> pair = pick_runs();
			processed_so_far = pair.second;
			output.open(
					processed_so_far != number_of_runs ?
							generate_run() : outfile);
			merge_runs(pair.first, pair.second, output);
			output.close();
		} while (processed_so_far < number_of_runs);
	}

	class heap_comparator {
	public:
		bool operator()(const std::pair<record_type, unsigned int>& x,
				const std::pair<record_type, unsigned int>& y) {
			return x.first > y.first;
		}
	};

	void merge_runs(unsigned int f, unsigned int l, writer_type& output) {
		std::vector<std::pair<record_type, unsigned int> > heap;
		std::vector<reader_type*> readers;
		std::vector<unsigned int> exhausted;

		for (unsigned int i = f; i < l; i++) {
			std::stringstream s;
			s << temp_prefix << "." << i << std::ends;
			readers.push_back(new reader_type(s.str()));
		}

		for (unsigned int i = 0; i < l - f; i++)
			heap.push_back(std::make_pair(readers[i]->nextRecord(), i));

		bool done = false;
		while (!done) {
			std::make_heap(heap.begin(), heap.end(), heap_comparator());
			output.write(heap[0].first);
			if (readers[heap[0].second]->hasNext())
				heap[0] = std::make_pair(readers[heap[0].second]->nextRecord(),
						heap[0].second);
			else {
				exhausted.push_back(heap[0].second);
				heap.erase(heap.begin());
			}
			done = exhausted.size() == readers.size();
		}

		for (typename std::vector<reader_type*>::iterator it = readers.begin();
				it != readers.end(); it++)
			delete *it;
		readers.clear();

		output.flush();
	}

	std::pair<unsigned int, unsigned int> pick_runs() const {
		return std::make_pair(processed_so_far,
				(processed_so_far + number_of_buffers - 1 > number_of_runs ?
						number_of_runs :
						processed_so_far + number_of_buffers - 1));
	}

	void remove_runs() const {
		for (unsigned int i = 0; i < number_of_runs; i++) {
			std::stringstream s;
			s << temp_prefix << "." << i << std::ends;
			fs::remove(s.str());
			//require(::remove(s.str().c_str()) == 0,
			//        "could not delete run file.");
		}
	}

	void dump_runs() {
		reader_type reader;
		for (unsigned int i = 0; i < number_of_runs; i++) {
			std::stringstream s;
			s << temp_prefix << "." << i << std::ends;
			reader.open(s.str());
			unsigned j = 0;
			while (reader.hasNext())
				COUT<< "run " << i << ", " << j++ << ": "
				<< reader.nextRecord() << "\n";
			reader.close();
		}
	}

	void dump() {
		reader_type reader(outfile);
		while (reader.hasNext())
			COUT<<"out " << reader.nextRecord() << "\n";
		}

	private:
		std::string filename;
		std::string temp_prefix;
		std::string outfile;
		unsigned int number_of_runs;
		unsigned int processed_so_far;
		const size_t number_of_buffers;

		std::string generate_run() {
			std::stringstream s;
			s << temp_prefix << "." << number_of_runs << std::ends;
			number_of_runs++;
			return s.str();
		}
	};

}
;

#endif
