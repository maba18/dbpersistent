#ifndef __SEGMENTJOIN_HH__
#define __SEGMENTJOIN_HH__

#include "../storage/page.hh"
#include "../storage/io.hh"
#include "../storage/identity.hh"
#include <sstream>
#include <sys/stat.h>
#include <algorithm>
#include <cstdio>
#include <vector>
#include <map>

namespace deceve { namespace bama {

template <typename Left, typename Right,
          typename LeftExtract = Identity<Left>,
          typename RightExtract = Identity<Right>,
          typename Combine = PairCombiner<Left, Right>,
          typename Less = std::less<typename LeftExtract::key_type> >
class SegmentJoin {
public:
    typedef Left left_record_type;
    typedef Right right_record_type;
    typedef typename Combine::record_type output_record_type;
    typedef typename LeftExtract::key_type key_type;

private:
    typedef Reader<left_record_type> left_reader_type;
    typedef Reader<right_record_type> right_reader_type;
    typedef Writer<left_record_type> left_writer_type;
    typedef Writer<right_record_type> right_writer_type;
    typedef Writer<output_record_type> writer_type;
    typedef BulkProcessor<left_record_type> bulk_processor_type;

public:
    SegmentJoin(deceve::storage::BufferManager *bm, const std::string& l,
                const std::string& r,
                const std::string& o,
                const LeftExtract& le = LeftExtract(),
                const RightExtract& re = RightExtract(),
                const Combine& c = Combine(),
                size_t np = 20,
                float p = 0.5)
        : bufferManager(bm), leftfile(l), rightfile(r), outfile(o),
          left_extractor(le), right_extractor(re), combiner(c),
          number_of_partitions(np), percentage(p)
    {}

    ~SegmentJoin() {}

    void join() {
        size_t limit = percentage * number_of_partitions;
        partition<Left, LeftExtract>(leftfile, left_extractor, limit);
        partition<Right, RightExtract>(rightfile, right_extractor, limit);
        grace(limit);
        lazy_grace(limit);
        delete_partition_files(limit);
    }

private:
    deceve::storage::BufferManager *bufferManager;
    std::string leftfile;
    std::string rightfile;
    std::string outfile;
    LeftExtract left_extractor;
    RightExtract right_extractor;
    Combine combiner;
    Less less;
    size_t number_of_partitions;
    float percentage;

    template <typename R, typename E>
    void partition(const std::string& filename, const E& extractor,
                   size_t limit) {
        Reader<R> reader(bufferManager, filename);
        size_t partition;
        
        std::vector<Writer<R>*> writers;
        for (size_t i = 0; i < limit; i++) {
            writers.push_back(new Writer<R>(bufferManager,
                                  generate_partition_name(filename, i)));
        }
        
        R rec;
        while (reader.hasNext()) {
            rec = reader.nextRecord();
            partition = hash_of(extractor(rec)) % number_of_partitions;
            if (partition < limit) {
                writers[partition]->write(rec);
            }
        }
        
        for (typename std::vector<Writer<R>*>::iterator
                 it = writers.begin(); it != writers.end(); it++) {
            (*it)->close();
            delete *it;
        }
        
        reader.close();
    }

    void grace(size_t limit) {
        left_reader_type left(bufferManager);
        right_reader_type right(bufferManager);
        writer_type output(bufferManager, outfile);
        std::multimap<key_type, left_record_type, Less> map;
        
        left_record_type left_record;
        std::pair<typename std::multimap<key_type, left_record_type,
                                         Less>::iterator,
                  typename std::multimap<key_type, left_record_type,
                                         Less>::iterator> range;
        
        for (size_t p = 0; p < limit; p++) {
            left.open(generate_partition_name(leftfile, p));
            while (left.hasNext()) {
                left_record = left.nextRecord();
                //std::cout << "left " << left_record << "\n";
                map.insert(std::pair<key_type, left_record_type>(
                               left_extractor(left_record), left_record));
            }
            left.close();
            
            right.open(generate_partition_name(rightfile, p));
            right_record_type right_record;
            while (right.hasNext()) {
                right_record = right.nextRecord();
                range = map.equal_range(right_extractor(right_record));
                for (typename std::multimap<key_type,
                         left_record_type>::iterator it = range.first;
                     it != range.second; it++) {
                    output.write(combiner(it->second, right_record));
                }
            }
            right.close();
            map.clear();
        }
        output.close();
    }

    void lazy_grace(size_t limit) {
        left_reader_type left(bufferManager);
        right_reader_type right(bufferManager);
        writer_type output(bufferManager, outfile);
        std::multimap<key_type, left_record_type, Less> map;
        
        left_record_type left_record;
        std::pair<typename std::multimap<key_type, left_record_type,
                                         Less>::iterator,
                  typename std::multimap<key_type, left_record_type,
                                         Less>::iterator> range;
        size_t partition;
        
        for (size_t p = limit; p < number_of_partitions; p++) {
            left.open(leftfile);
            while (left.hasNext()) {
                left_record = left.nextRecord();
                partition = hash_of(left_extractor(left_record))
                    % number_of_partitions;
                if (partition == p) {
                    //std::cout << "left " << left_record << "\n";
                    map.insert(std::pair<key_type, left_record_type>(
                                   left_extractor(left_record), left_record));
                }
            }
            left.close();
            
            right.open(rightfile);
            right_record_type right_record;
            while (right.hasNext()) {
                right_record = right.nextRecord();
                partition = hash_of(right_extractor(right_record))
                    % number_of_partitions;
                if (partition == p) {
                    range = map.equal_range(right_extractor(right_record));
                    for (typename std::multimap<key_type,
                             left_record_type>::iterator it = range.first;
                         it != range.second; it++) {
                        output.write(combiner(it->second, right_record));
                    }
                }
            }
            right.close();
            map.clear();
        }
        output.close();
    }

    std::string generate_partition_name(const std::string& p, size_t i) {
        std::stringstream s;
        s << p << "." << i;// << std::ends;
        return s.str();
    }

    void delete_partition_files(size_t limit) {
        //std::cout << "called for " << number_of_partitions << "\n";
        for (size_t i = 0; i < limit; i++) {
            //std::cout << "removing partition " << i << "\n";

        	bufferManager->removeFile(generate_partition_name(leftfile, i));
        	bufferManager->removeFile(generate_partition_name(rightfile, i));
//            fs::remove(generate_partition_name(leftfile, i));
//            fs::remove(generate_partition_name(rightfile, i));
            /*
            require(::remove(generate_partition_name(leftfile, i).c_str()) == 0,
                    "could not delete partition file.");
            require(::remove(generate_partition_name(rightfile, i).c_str()) == 0,
                    "could not delete partition file.");
            */
        }
    }

    size_t hash_of(const key_type& value) {
        char* k = (char*) &value;
        size_t hash = 5381;
        for (size_t i = 0; i < sizeof(key_type); i++)
            hash = ((hash << 5) + hash) + k[i];
        return hash;
    }
};

}}


#endif
