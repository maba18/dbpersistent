#ifndef __HYBRIDJOIN_HH__
#define __HYBRIDJOIN_HH__

#include "../storage/page.hh"
#include "../storage/io.hh"
#include "../storage/identity.hh"
#include <sstream>
#include <sys/stat.h>
#include <algorithm>
#include <cstdio>
#include <vector>
#include <map>

namespace deceve { namespace bama {

template <typename Left, typename Right,
          typename LeftExtract = Identity<Left>,
          typename RightExtract = Identity<Right>,
          typename Combine = PairCombiner<Left, Right>,
          typename Less = std::less<typename LeftExtract::key_type> >
class HybridJoin {
public:
    typedef Left left_record_type;
    typedef Right right_record_type;
    typedef typename Combine::record_type output_record_type;
    typedef typename LeftExtract::key_type key_type;

private:
    typedef Reader<left_record_type> left_reader_type;
    typedef Reader<right_record_type> right_reader_type;
    typedef Writer<output_record_type> writer_type;
    typedef BulkProcessor<left_record_type> bulk_processor_type;

public:
    HybridJoin(deceve::storage::BufferManager *bm, const std::string& l,
               const std::string& r,
               const std::string& o,
               const LeftExtract& le = LeftExtract(),
               const RightExtract& re = RightExtract(),
               const Combine&c = Combine(),
               size_t np = 20,
               float lp = 0.5,
               float rp = 0.5)
        : bufferManager(bm), leftfile(l), rightfile(r), outfile(o),
          left_extractor(le),
          right_extractor(re),
          combiner(c),
          less(Less()), 
          number_of_partitions(np),
          left_percentage(lp),
          right_percentage(rp),
          left_pages(0),
          left_records(0),
          right_pages(0),
          right_records(0) {
        left_reader_type lreader(bufferManager, leftfile);
        left_pages = left_percentage * lreader.numPages();
        left_records = left_pages * Page<left_record_type>::allocation;
        lreader.close();
        right_reader_type rreader(bufferManager, rightfile);
        right_pages = right_percentage * rreader.numPages();
        right_records = right_pages * Page<right_record_type>::allocation;
        rreader.close();
    }

    ~HybridJoin() {}

    void join() {
        partition<Left, LeftExtract>(leftfile, left_records,
                                     left_extractor);
        size_t right_records_processed =
            partition<Right, RightExtract>(rightfile, right_records,
                                           right_extractor);
        grace(right_records_processed);
        nestedloops(right_records_processed);
        delete_partition_files();
    }

private:
    deceve::storage::BufferManager *bufferManager;
    std::string leftfile;
    std::string rightfile;
    std::string outfile;
    LeftExtract left_extractor;
    RightExtract right_extractor;
    Combine combiner;
    Less less;
    size_t number_of_partitions;
    float left_percentage;
    float right_percentage;
    size_t left_pages;
    size_t left_records;
    size_t right_pages;
    size_t right_records;
    
    template <typename R, typename E>
    size_t partition(const std::string& filename, size_t max_recs,
                     const E& extractor) {
        Reader<R> reader(bufferManager, filename);
        
        std::vector<Writer<R>*> writers;
        for (size_t i = 0; i < number_of_partitions; i++) {
            writers.push_back(new Writer<R>(bufferManager,
                                  generate_partition_name(filename, i)));
        }
        
        R rec;
        size_t count = 0;
        while (reader.hasNext() && count < max_recs) {
            rec = reader.nextRecord();
            writers[hash_of(extractor(rec)) % number_of_partitions]->write(rec);
            count++;
        }
        
        for (typename std::vector<Writer<R>*>::iterator
                 it = writers.begin(); it != writers.end(); it++) {
            (*it)->close();
            delete *it;
        }
        
        reader.close();
        return count;
    }
    
    void grace(size_t right_records_processed) {
        left_reader_type left(bufferManager);
        right_reader_type right(bufferManager);
        writer_type output(bufferManager, outfile);
        std::multimap<key_type, left_record_type, Less> map;
        
        left_record_type left_record;
        std::pair<typename std::multimap<key_type, left_record_type,
                                         Less>::iterator,
                  typename std::multimap<key_type, left_record_type,
                                         Less>::iterator> range;
        
        for (size_t p = 0; p < number_of_partitions; p++) {
            left.open(generate_partition_name(leftfile, p));
            while (left.hasNext()) {
                left_record = left.nextRecord();
                //std::cout << "left " << left_record << "\n";
                map.insert(std::pair<key_type, left_record_type>(
                               left_extractor(left_record), left_record));
            }
            left.close();
            
            right.open(generate_partition_name(rightfile, p));
            right_record_type right_record;
            while (right.hasNext()) {
                right_record = right.nextRecord();
                range = map.equal_range(right_extractor(right_record));
                for (typename std::multimap<key_type,
                         left_record_type>::iterator it = range.first;
                     it != range.second; it++) {
                    output.write(combiner(it->second, right_record));
                }
            }
            right.close();

            right.open(rightfile);
            right.skip(right_records_processed);
            while (right.hasNext()) {
                right_record = right.nextRecord();
                if (p == (hash_of(right_extractor(right_record))
                          % number_of_partitions)) {
                    range = map.equal_range(right_extractor(right_record));
                    for (typename std::multimap<key_type,
                             left_record_type>::iterator it = range.first;
                         it != range.second; it++) {
                        output.write(combiner(it->second, right_record));
                    }
                }
            }
            right.close();            
            map.clear();
        }
        output.close();
    }

    void nestedloops(size_t right_records_processed) {
        //left_reader_type left_reader;
        right_reader_type right_reader(bufferManager);
        bulk_processor_type bulk_reader(bufferManager);
        right_record_type right_record;
        size_t left_records_read;        
        std::vector<std::multimap<key_type, left_record_type, Less> >
            map(number_of_partitions);
        std::pair<typename std::multimap<key_type, left_record_type,
                                         Less>::iterator,
                  typename std::multimap<key_type, left_record_type,
                                         Less>::iterator> range;
        writer_type output(bufferManager, outfile);
    
        
        bulk_reader.open(leftfile);
        //bulk_reader.skip(left_records_processed);
        //bulk_reader.skip(left_pages);
        size_t chunk = left_pages;
        size_t increment = left_pages;
        left_record_type* records =
            (left_record_type*) aligned_new(increment
                                            * get_pagesize());
        //std::cout << "allocated" << "\n";
        bulk_reader.bulkRead(chunk,
                             records, increment,
                             left_records_read);
        while (left_records_read != 0) {
            //std::cout << "processing chunk " << chunk << "\n";
            // build the hash table
            size_t p;
            for (size_t i = 0; i < left_records_read; i++) {
                //std::cout << "read " << records[i] << "\n";
                p = hash_of(left_extractor(records[i])) % number_of_partitions;
                //std::cout << "partition is " << p << "\n";
                map[p].insert(std::pair<key_type, left_record_type>(
                                  left_extractor(records[i]), records[i]));
            }
            //std::cout << "built map" << "\n";
            // then perform the partition join
            for (size_t i = 0; i < number_of_partitions; i++) {
                right_reader.open(generate_partition_name(rightfile, i));
                while (right_reader.hasNext()) {
                    right_record = right_reader.nextRecord();
                    p = hash_of(right_extractor(right_record))
                        % number_of_partitions;
                    range = map[p].equal_range(right_extractor(right_record));
                    for (typename std::multimap<key_type,
                             left_record_type>::iterator it = range.first;
                         it != range.second; it++) {
                        output.write(combiner(it->second, right_record));
                    }
                }
                right_reader.close();
            }
            //std::cout << "performed partition join" << "\n";
            // then process the rest of the right input by scanning
            // and looking up into the in-memory table
            right_reader.open(rightfile);
            right_reader.skip(right_records_processed);
            while (right_reader.hasNext()) {
                right_record = right_reader.nextRecord();
                p = hash_of(right_extractor(right_record))
                        % number_of_partitions;
                range = map[p].equal_range(right_extractor(right_record));
                for (typename std::multimap<key_type,
                         left_record_type>::iterator it = range.first;
                     it != range.second; it++) {
                    output.write(combiner(it->second, right_record));
                }
            }
            //std::cout << "performed right scan" << "\n";
            for (size_t i = 0; i < number_of_partitions; i++) {
                map[i].clear();
            }
            chunk += increment;
            bulk_reader.bulkRead(chunk,
                                 records, increment,
                                 left_records_read);
            right_reader.close();
        }
        aligned_delete((unsigned char*) records);
        bulk_reader.close();
        //right_reader.close();
        output.close();
    }
        

    std::string generate_partition_name(const std::string& p, size_t i) {
        std::stringstream s;
        s << p << "." << i;
        return s.str();
    }

    void delete_partition_files() {
        //std::cout << "called for " << number_of_partitions << "\n";
        for (size_t i = 0; i < number_of_partitions; i++) {
            //std::cout << "removing partition " << i << "\n";
        	bufferManager->removeFile(generate_partition_name(leftfile, i));
        	bufferManager->removeFile(generate_partition_name(rightfile, i));
//            fs::remove(generate_partition_name(leftfile, i));
//            fs::remove(generate_partition_name(rightfile, i));
            /*
            require(::remove(generate_partition_name(leftfile, i).c_str()) == 0,
                    "could not delete partition file.");
            require(::remove(generate_partition_name(rightfile, i).c_str()) == 0,
                    "could not delete partition file.");
            */
        }
    }

    size_t hash_of(const key_type& value) {
        char* k = (char*) &value;
        size_t hash = 5381;
        for (size_t i = 0; i < sizeof(key_type); i++)
            hash = ((hash << 5) + hash) + k[i];
        return hash;
    }
};

}}

#endif
