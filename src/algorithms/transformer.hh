/*
 * transformer.hh
 *
 *  Created on: 3 Apr 2015
 *      Author: michail
 */

#ifndef TRANSFORMER_HH_
#define TRANSFORMER_HH_


#include "../storage/page.hh"
#include "../storage/io.hh"
#include "../storage/identity.hh"
#include <sstream>
#include <sys/stat.h>
#include <algorithm>
#include <cstdio>
#include <vector>
#include "../utils/util.hh"
//#include "../storage/readwriters.hh"

namespace deceve {
namespace bama {

template<typename Record, typename RecordOutput, typename Transform >
class Transformer {
public:
	typedef Record record_input_type;
	typedef RecordOutput record_output_type;

private:
	typedef Reader<record_input_type> reader_type;
	typedef Writer<record_output_type> writer_type;

private:


public:
	Transformer(deceve::storage::BufferManager *bm, const std::string& fn,
			const std::string& op, const Transform& e) :
			bufferManager(bm), inputName(fn), outputName(op), transformer(e) {
	};

	~Transformer() {
	}

	void transform() {

		reader_type inputReader(bufferManager, inputName);

//		COUT << "Transformation happening on: " << inputName << "\n";

		writer_type outputWriter(bufferManager, outputName);


		record_input_type rec;
		while (inputReader.hasNext()) {
			rec = inputReader.nextRecord();
//			writers[hash_of(transformer(rec)) % number_of_partitions]->write(rec);
			outputWriter.write(transformer(rec));
		}

		inputReader.close();
		outputWriter.close();
	}

private:
	deceve::storage::BufferManager *bufferManager;
	std::string inputName;
	std::string outputName;
	const Transform transformer;
};

}
}



#endif /* TRANSFORMER_HH_ */
