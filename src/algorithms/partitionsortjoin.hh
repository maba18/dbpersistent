/*
 * partitionsortjoin.hh
 *
 *  Created on: 15 Mar 2015
 *      Author: michail
 */

#ifndef PARTITIONSORTJOIN_HH_
#define PARTITIONSORTJOIN_HH_

#include "../storage/page.hh"
#include "../storage/io.hh"
#include "../storage/identity.hh"
#include <sstream>
#include <sys/stat.h>
#include <algorithm>
#include <cstdio>
#include <map>

namespace deceve {
namespace bama {

template <typename Left, typename Right, typename LeftExtract = Identity<Left>,
          typename RightExtract = Identity<Right>,
          typename Combine = PairCombiner<Left, Right>,
          typename Less = std::less<typename LeftExtract::key_type> >
class PartitionSortJoin {
 public:
  typedef Left left_record_type;
  typedef Right right_record_type;
  typedef typename Combine::record_type output_record_type;
  typedef typename LeftExtract::key_type key_type;

 private:
  typedef Reader<left_record_type> left_reader_type;
  typedef Reader<right_record_type> right_reader_type;
  typedef Writer<output_record_type> writer_type;

 public:
  PartitionSortJoin(
      deceve::storage::BufferManager* bm, const std::string& l,
      const std::string& r, const std::string& o,
      const LeftExtract& le = LeftExtract(),
      const RightExtract& re = RightExtract(), const Combine& c = Combine(),
      size_t np = 20, bool persistFlag = false, bool useFlag = false,
      int result = -1,
      const deceve::storage::table_key& l_key = deceve::storage::table_key(),
      const deceve::storage::table_key& r_key = deceve::storage::table_key())
      : bufferManager(bm),
        leftfile(l),
        rightfile(r),
        outfile(o),
        left_extractor(le),
        right_extractor(re),
        combiner(c),
        less(Less()),
        number_of_partitions(np),
        persistOutput_flag(persistFlag),
        usePersistedOutput_flag(useFlag),
        result_id(result),
        left_tk(l_key),
        right_tk(r_key){};

  ~PartitionSortJoin() {}

  void join() {
    //    left_reader_type leftTmp(bufferManager, leftfile);
    //    right_reader_type rightTmp(bufferManager, rightfile);
    //
    //    std::cout << "------------------- LEFT -------------------------" <<
    //    std::endl;
    //    while (leftTmp.hasNext()) {
    //      std::cout << "left: " << leftTmp.nextRecord() << std::endl;
    //    }
    //    leftTmp.close();
    //    std::cout << "-------------------- RIGHT ------------------------" <<
    //    std::endl;
    //    while (rightTmp.hasNext()) {
    //      std::cout << "right: " << rightTmp.nextRecord() << std::endl;
    //    }
    //    rightTmp.close();

    left_reader_type left(bufferManager, leftfile);
    right_reader_type right(bufferManager, rightfile);
    writer_type output(bufferManager, outfile);
    left_record_type left_record;
    right_record_type right_record;
    right_record_type temp_record;

    right_record = right.nextRecord();

    while (left.hasNext() && right.hasNext()) {
      left_record = left.nextRecord();

      while (left_extractor(left_record) < right_extractor(right_record) &&
             left.hasNext()) {
        left_record = left.nextRecord();
      }

      while (left_extractor(left_record) > right_extractor(right_record) &&
             right.hasNext()) {
        right_record = right.nextRecord();
      }

      // a group begins here
      bool more = true;
      //			while (left_record.key == right_record.key &&
      // more) {
      while (left_extractor(left_record) == right_extractor(right_record) &&
             more) {
        // mark the beginning of the group
        right.mark();
        temp_record = right_record;
        more = true;
        // we know there is at least one match
        do {
          // create the output record and write it
          if (isStreamFlag) {
            combiner(left_record, right_record);
          } else {
            output.write(combiner(left_record, right_record));
            numberOfOutputTuples++;
          }

          more = right.hasNext();
          if (more) {
            right_record = right.nextRecord();
            more = left_extractor(left_record) == right_extractor(right_record);
          }
        } while (more);
        // roll back to the beginning of the group
        right.rollback();
        right_record = temp_record;
      }
    }

    output.close();
    left.close();
    right.close();

    // std::cout << "SORTING left_tk: " << left_tk << std::endl;
    //  std::cout << "SORTING right_tk: " << right_tk << std::endl;
    // std::cout << "result_id: " << result_id << std::endl;

    if (result_id != 0 && result_id != 1) deleteInputFile(leftfile, left_tk);
    if (result_id != 0 && result_id != 2) deleteInputFile(rightfile, right_tk);

    //    if (!bufferManager->getStorageManager().isInTableCatalog(leftfile) &&
    //        bufferManager->getFileMode(leftfile) ==
    //        deceve::storage::AUXILIARY) {
    //      bufferManager->removeFile(leftfile);
    //    }
    //    if (!bufferManager->getStorageManager().isInTableCatalog(rightfile) &&
    //        bufferManager->getFileMode(rightfile) ==
    //        deceve::storage::AUXILIARY) {
    //      bufferManager->removeFile(rightfile);
    //    }
  }

  void deleteInputFile(const std::string& fname,
                       deceve::storage::table_key tk) {
    //    bool check1 =
    //    bufferManager->getStorageManager().isInTableCatalog(fname);
    //    bool check2 =
    //        bufferManager->getStorageManager().isInPersistedFileCatalog(tk);
    //
    //    std::cout << "check1: " << std::boolalpha << check1 << std::endl;
    //    std::cout << "check1: " << std::boolalpha << check2 << std::endl;

    if (!bufferManager->getSM().isInTableCatalog(fname) &&
        !bufferManager->getSM().isInPersistedFileCatalog(tk)) {
      //      std::cout << "DELETE FROM PARTITION-JOIN: " << fname << " " << tk
      //      << "\n";
      bufferManager->removeFile(fname);
    }
  }

 private:
  std::string generate_partition_name(const std::string& p, size_t i) {
    std::stringstream s;
    s << p << "." << i;
    return s.str();
  }

 public:
  void setStreamFlag(bool streamFlag) { isStreamFlag = streamFlag; }
  double getNumberOfOutputTuples() { return numberOfOutputTuples; }

 private:
  deceve::storage::BufferManager* bufferManager;
  std::string leftfile;
  std::string rightfile;
  std::string outfile;
  LeftExtract left_extractor;
  RightExtract right_extractor;
  Combine combiner;
  Less less;
  size_t number_of_partitions;
  bool persistOutput_flag;
  bool usePersistedOutput_flag;
  int result_id;
  deceve::storage::table_key left_tk;
  deceve::storage::table_key right_tk;

  bool isStreamFlag{false};
  double numberOfOutputTuples{0};
};

}  //:~ namespace bama
}  //:~ namespace deceve

#endif /* PARTITIONSORTJOIN_HH_ */
