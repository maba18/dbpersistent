/*
 * partitionjoinMapReduce.hh
 *
 *  Created on: 31 Mar 2015
 *      Author: michail
 */

#ifndef PARTITIONJOINMAPREDUCE_HH_
#define PARTITIONJOINMAPREDUCE_HH_

#include "../storage/page.hh"
#include "../storage/io.hh"
#include "../storage/identity.hh"
#include <sstream>
#include <sys/stat.h>
#include <algorithm>
#include <cstdio>
#include <map>

namespace deceve {
namespace bama {

template<typename Left, typename Right, typename LeftExtract = Identity<Left>,
		typename RightExtract = Identity<Right>,
		typename Combine = PairCombiner<Left, Right>, typename Less = std::less<
				typename LeftExtract::key_type> >
class PartitionJoinMapReduce {
public:
	typedef Left left_record_type;
	typedef Right right_record_type;
	typedef typename Combine::record_type output_record_type;
	typedef typename LeftExtract::key_type key_type;

private:
	typedef Reader<left_record_type> left_reader_type;
	typedef Reader<right_record_type> right_reader_type;
	typedef Writer<output_record_type> writer_type;

public:
	PartitionJoinMapReduce(deceve::storage::BufferManager *bm, const std::string& l,
			const std::string& r, const std::string& o, const LeftExtract& le =
					LeftExtract(), const RightExtract& re = RightExtract(),
			const Combine& c = Combine(), size_t np = 20, bool persistFlag =
					false, bool useFlag = false, int result = -1) :
			bufferManager(bm), leftfile(l), rightfile(r), outfile(o), left_extractor(
					le), right_extractor(re), combiner(c), less(Less()), number_of_partitions(
					np), persistOutput_flag(persistFlag), usePersistedOutput_flag(
					useFlag), result_id(result) {
	};

	~PartitionJoinMapReduce() {
	}

	void join() {

		left_reader_type left(bufferManager);
		right_reader_type right(bufferManager);
		writer_type output(bufferManager, outfile);

		COUT << "Merge Join phase happening: " << outfile << "\n";
		std::multimap<key_type, left_record_type, Less> map;

		left_record_type left_record;
		std::pair<
				typename std::multimap<key_type, left_record_type, Less>::iterator,
				typename std::multimap<key_type, left_record_type, Less>::iterator> range;
		for (size_t p = 0; p < number_of_partitions; p++) {
			COUT << "File left :" << generate_partition_name(leftfile, p)
					<< "\n";
			left.open(generate_partition_name(leftfile, p));
			while (left.hasNext()) {
				left_record = left.nextRecord();
				map.insert(
						std::pair<key_type, left_record_type>(
								left_extractor(left_record), left_record));
			}
			left.close();

			COUT << "File right :" << generate_partition_name(rightfile, p)
					<< "\n";
			right.open(generate_partition_name(rightfile, p));
			right_record_type right_record;
			while (right.hasNext()) {
				right_record = right.nextRecord();
				range = map.equal_range(right_extractor(right_record));
				for (typename std::multimap<key_type, left_record_type>::iterator it =
						range.first; it != range.second; it++) {
					output.write(combiner(it->second, right_record));
				}
			}
			right.close();
			map.clear();

			if (bufferManager->getFileMode(generate_partition_name(leftfile, p))
					== deceve::storage::PRIMARY) {
				bufferManager->removeFile(generate_partition_name(leftfile, p));
			}
			if (bufferManager->getFileMode(
					generate_partition_name(rightfile, p))
					== deceve::storage::PRIMARY) {
				bufferManager->removeFile(
						generate_partition_name(rightfile, p));
			}

//            fs::remove(generate_partition_name(leftfile, p));
//            fs::remove(generate_partition_name(rightfile, p));
			/*
			 require(::remove(generate_partition_name(leftfile,
			 p).c_str()) == 0,
			 "could not delete partition file.");
			 require(::remove(generate_partition_name(rightfile,
			 p).c_str()) == 0,
			 "could not delete partition file.");
			 */
		}

		output.close();
	}

private:
	deceve::storage::BufferManager *bufferManager;
	std::string leftfile;
	std::string rightfile;
	std::string outfile;
	LeftExtract left_extractor;
	RightExtract right_extractor;
	Combine combiner;
	Less less;
	size_t number_of_partitions;
	bool persistOutput_flag;
	bool usePersistedOutput_flag;
	int result_id;

	std::string generate_partition_name(const std::string& p, size_t i) {
		std::stringstream s;
		s << p << "." << i;
		return s.str();
	}
};

} //:~ namespace bama
} //:~ namespace deceve

#endif
