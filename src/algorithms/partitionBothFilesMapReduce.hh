/*
 * partitionBothFilesMapReduce.hh
 *
 *  Created on: 1 Apr 2015
 *      Author: michail
 */

#ifndef PARTITIONBOTHFILESMAPREDUCE_HH_
#define PARTITIONBOTHFILESMAPREDUCE_HH_

#include "../storage/page.hh"
#include "../storage/io.hh"
#include "../storage/identity.hh"
 #include "../utils/util.hh"
#include <sstream>
#include <sys/stat.h>
#include <algorithm>
#include <cstdio>
#include <map>
#include "../utils/util.hh"

namespace deceve {
namespace bama {

template<typename Left, typename Right, typename LeftExtract = Identity<Left>,
		typename RightExtract = Identity<Right>,
		typename Combine = PairCombiner<Left, Right>, typename Less = std::less<
				typename LeftExtract::key_type> >
class PartitionJoin {
public:
	typedef Left left_record_type;
	typedef Right right_record_type;
	typedef typename Combine::record_type output_record_type;
	typedef typename LeftExtract::key_type key_type;

private:
	typedef Reader<left_record_type> left_reader_type;
	typedef Reader<right_record_type> right_reader_type;
	typedef Writer<output_record_type> writer_type;

public:
	PartitionJoin(deceve::storage::BufferManager *bm, const std::string& l,
			const std::string& r, const std::string& o, const LeftExtract& le =
					LeftExtract(), const RightExtract& re = RightExtract(),
			const Combine& c = Combine(), size_t np = 20, bool persistFlag =
					false, bool useFlag = false, int result = -1) :
			bufferManager(bm), leftfile(l), rightfile(r), outfile(o), left_extractor(
					le), right_extractor(re), combiner(c), less(Less()), number_of_partitions(
					np), persistOutput_flag(persistFlag), usePersistedOutput_flag(
					useFlag), result_id(result) {
	};

	~PartitionJoin() {
	}

	void join() {

		left_reader_type left(bufferManager, leftfile);
		right_reader_type right(bufferManager, rightfile);

		COUT << "PARTITION happening on both files: " << leftfile << " "
				<< rightfile << "\n";

		std::vector<writer_type*> writers;
		for (unsigned int i = 0; i < number_of_partitions; ++i) {
			std::stringstream s;
			s << outfile << "." << i;
			writers.push_back(
					new writer_type(bufferManager, s.str(),
							deceve::storage::PRIMARY));
		}

		left_record_type left_rec;
		right_record_type right_rec;

		while (left.hasNext()) {
			left_rec = left.nextRecord();
			writers[hash_of(left_extractor(left_rec)) % number_of_partitions]->write(
					combiner(left_rec, right_rec));
		}

		for (typename std::vector<writer_type*>::iterator it = writers.begin();
				it != writers.end(); it++) {
			(*it)->flush();
			delete *it;
		}

		left.close();

	}

private:
	deceve::storage::BufferManager *bufferManager;
	std::string leftfile;
	std::string rightfile;
	std::string outfile;
	LeftExtract left_extractor;
	RightExtract right_extractor;
	Combine combiner;
	Less less;
	size_t number_of_partitions;
	bool persistOutput_flag;
	bool usePersistedOutput_flag;
	int result_id;

	size_t hash_of(const key_type& value) {
		char* k = (char*) &value;
		size_t hash = 5381;
		for (size_t i = 0; i < sizeof(key_type); i++)
			hash = ((hash << 5) + hash) + k[i];
		return hash;
	}

	std::string generate_partition_name(const std::string& p, size_t i) {
		std::stringstream s;
		s << p << "." << i;
		return s.str();
	}
};

} //:~ namespace bama
} //:~ namespace deceve

#endif
