#ifndef __LAZYJOIN_HH__
#define __LAZYJOIN_HH__

#include "../storage/page.hh"
#include "../storage/io.hh"
#include "../storage/identity.hh"
#include <sstream>
#include <sys/stat.h>
#include <algorithm>
#include <cstdio>
#include <vector>
#include <map>

namespace deceve { namespace bama {

template <typename Left, typename Right,
          typename LeftExtract = Identity<Left>,
          typename RightExtract = Identity<Right>,
          typename Combine = PairCombiner<Left, Right>,
          typename Less = std::less<typename LeftExtract::key_type> >
class LazyJoin {
public:
    typedef Left left_record_type;
    typedef Right right_record_type;
    typedef typename Combine::record_type output_record_type;
    typedef typename LeftExtract::key_type key_type;

private:
    typedef Reader<left_record_type> left_reader_type;
    typedef Reader<right_record_type> right_reader_type;
    typedef Writer<left_record_type> left_writer_type;
    typedef Writer<right_record_type> right_writer_type;
    typedef Writer<output_record_type> writer_type;
    typedef BulkProcessor<left_record_type> bulk_processor_type;

public:
    LazyJoin(deceve::storage::BufferManager *bm, const std::string& l,
             const std::string& r,
             const std::string& o,
             const LeftExtract& le = LeftExtract(),
             const RightExtract& re = RightExtract(),
             const Combine& c = Combine(),
             size_t np = 20,
             float rt = 2.0)
        : bufferManager(bm), leftfile(l), rightfile(r), outfile(o),
          left_extractor(le), right_extractor(re), combiner(c),
          number_of_partitions(np), ratio(rt),
          left_sub(true), right_sub(true),
          number_of_materializations(0)
    {}

    ~LazyJoin() {}

    void join() {
        writer_type writer(bufferManager, outfile);
        size_t pass = 1;
        
        for (size_t p = 0; p < number_of_partitions; p++) {
            make_pass(writer, p, pass);
        }

        if (number_of_materializations > 1) {

        	bufferManager->removeFile(generate_materialized_input(leftfile,
                                                   number_of_materializations));
        //    fs::remove(generate_materialized_input(leftfile,
        //                                           number_of_materializations));

        	bufferManager->removeFile(generate_materialized_input(rightfile,
                    number_of_materializations));
//            fs::remove(generate_materialized_input(rightfile,
//                                                   number_of_materializations));
        }
        
        writer.close();
    }

private:
    deceve::storage::BufferManager *bufferManager;
    std::string leftfile;
    std::string rightfile;
    std::string outfile;
    LeftExtract left_extractor;
    RightExtract right_extractor;
    Combine combiner;
    Less less;
    size_t number_of_partitions;
    float ratio;
    bool left_sub;
    bool right_sub;
    size_t number_of_materializations;

    std::string generate_materialized_input(const std::string& f,
                                            size_t p) const {
        if (p == 0) return f;
        std::stringstream s;
        s << f << "." << p;
        return s.str();
    }

    std::string generate_materialized_output(const std::string &f,
                                             size_t p) const {
        std::stringstream s;
        s << f << "." << (p+1);
        return s.str();
    }
    
    void make_pass(writer_type& output, size_t partition, size_t& pass) {
        //left_reader_type left_reader(leftfile);
        //right_reader_type right_reader(rightfile);
        left_reader_type left_reader(bufferManager,
            generate_materialized_input(leftfile, number_of_materializations));
        right_reader_type right_reader(bufferManager,
            generate_materialized_input(rightfile, number_of_materializations));
        left_record_type left_record;
        right_record_type right_record;
        left_writer_type left_writer(bufferManager);
        right_writer_type right_writer(bufferManager);
        size_t prt;
        std::multimap<key_type, left_record_type, Less> map;
        std::pair<typename std::multimap<key_type, left_record_type,
                                         Less>::iterator,
                  typename std::multimap<key_type, left_record_type,
                                         Less>::iterator> range;
        /*
        bool materialize =
            pass > (number_of_partitions-partition)/(ratio + 1);
        */
        bool materialize = pass > (number_of_partitions-partition-pass-1)*ratio;
        //std::cout << "partition: " << partition << ", materialize: " << materialize << "\n";
        
        if (materialize) {
            //left_writer.open(generate_temp_filename(leftfile));
            //right_writer.open(generate_temp_filename(rightfile));
            left_writer.open(
                generate_materialized_output(leftfile,
                                             number_of_materializations));
            right_writer.open(
                generate_materialized_output(rightfile,
                                             number_of_materializations));
        }
        while (left_reader.hasNext()) {
            left_record = left_reader.nextRecord();
            prt = hash_of(left_extractor(left_record)) % number_of_partitions;
            if (prt == partition) {
                map.insert(std::pair<key_type, left_record_type>(
                               left_extractor(left_record), left_record));
            }
            else if (materialize && prt > partition) {
                left_writer.write(left_record);
            }
        }
        while (right_reader.hasNext()) {
            right_record = right_reader.nextRecord();
            //std::cout << "read " << right_record << "\n";
            prt = hash_of(right_extractor(right_record)) % number_of_partitions;
            if (prt == partition) {
                range = map.equal_range(right_extractor(right_record));
                for (typename std::multimap<key_type,
                         left_record_type>::iterator it = range.first;
                     it != range.second; it++) {
                    output.write(combiner(it->second, right_record));
                }
            }
            else if (materialize && prt > partition) {
                right_writer.write(right_record);
            }
        }
        
        left_reader.close();
        right_reader.close();
        if (materialize) {
            left_writer.close();
            right_writer.close();
            if (number_of_materializations > 0) {

            	bufferManager->removeFile(generate_materialized_input(leftfile,
                                                number_of_materializations));

            	bufferManager->removeFile( generate_materialized_input(rightfile,
                                                number_of_materializations));
//                fs::remove(
//                    generate_materialized_input(leftfile,
//                                                number_of_materializations));
//                fs::remove(
//                    generate_materialized_input(rightfile,
//                                                number_of_materializations));
            }
            //substitute_file(left_sub, leftfile,
            //                generate_temp_filename(leftfile));
            //substitute_file(right_sub, rightfile,
            //                generate_temp_filename(rightfile));
            pass = 0;
            number_of_materializations++;
        }
        pass++;
    }

    std::string generate_temp_filename(const std::string& p) const {
        std::stringstream s;
        s << p << ".tmp";
        return s.str();
    }

    void substitute_file(bool &first_sub,
                         std::string& orig,
                         const std::string& sub) const {
        if (first_sub) {
            orig.clear();
            orig.append(sub);
            first_sub = false;
        }
        else {
        	bufferManager->removeFile(orig);
        	bufferManager->renameFile(sub, orig);
//            fs::remove(orig);
//            fs::rename(sub, orig);
        }
        //require(::remove(orig.c_str()) == 0,
        //        "could not delete original file.");
        //require(::rename(sub.c_str(), orig.c_str()) == 0,
        //        "could not rename files.");
    }

    size_t hash_of(const key_type& value) {
        char* k = (char*) &value;
        size_t hash = 5381;
        for (size_t i = 0; i < sizeof(key_type); i++)
            hash = ((hash << 5) + hash) + k[i];
        return hash;
    }
};

}}


#endif
