#ifndef __MERGEJOIN_HH__
#define __MERGEJOIN_HH__

#include "../storage/page.hh"
#include "../storage/io.hh"
#include "../storage/readwriters.hh"
#include "../storage/BufferManager.hh"
#include <sstream>
#include <sys/stat.h>
#include <algorithm>
#include <cstdio>
#include <vector>
 #include "../utils/util.hh"

namespace deceve {
namespace bama {

template<typename K, typename L, typename R>
class MergeJoin {
public:
	typedef Record<K, L> left_record_type;
	typedef Record<K, R> right_record_type;
	typedef Record<K, CombinedRecord<L, R> > output_record_type;

private:
	typedef Reader<K, L> left_reader_type;
	typedef Reader<K, R> right_reader_type;
	typedef Writer<K, CombinedRecord<L, R> > writer_type;

public:
	MergeJoin(deceve::storage::BufferManager *bm, const std::string& l,
			const std::string& r, const std::string& o) :
			bufferManager(bm), leftfile(l), rightfile(r), outfile(o) {
	};

	~MergeJoin() {
	}

	void join() {
		left_reader_type left(bufferManager, leftfile);
		right_reader_type right(bufferManager, rightfile);
		writer_type output(bufferManager, outfile);
		left_record_type left_record;
		right_record_type right_record;

		while (left.hasNext() && right.hasNext()) {
			left_record = left.nextRecord();
			right_record = right.nextRecord();
			while (left_record.key < right_record.key && left.hasNext()) {
				left_record = left.nextRecord();
			}
			while (left_record.key > right_record.key && right.hasNext()) {
				right_record = right.nextRecord();
			}

			//a group begins here
			bool more = true;
			while (left_record.key == right_record.key && more) {
				// mark the beginning of the group
				right.mark();
				more = true;
				// we know there is at least one match
				do {
					// create the output record and write it
					output.write(
							makeRecord(left_record.key,
									makeCombinedRecord(left_record.payload,
											right_record.payload)));
					more = right.hasNext();
					if (more) {
						right_record = right.nextRecord();
						more = left_record.key == right_record.key;
					}
				} while (more);
				// roll back to the beginning of the group
				right.rollback();
			}
		}
	}

private:
	deceve::storage::BufferManager *bufferManager;
	std::string leftfile;
	std::string rightfile;
	std::string outfile;
};

}
}

#endif
