#ifndef __PARTITIONCONVERT_HH__
#define __PARTITIONCONVERT_HH__

#include "../storage/page.hh"
#include "../storage/io.hh"
#include "../storage/identity.hh"
#include <sstream>
#include <sys/stat.h>
#include <algorithm>
#include <cstdio>
#include <vector>
#include "../utils/util.hh"
#include <stdint.h>

namespace deceve {
namespace bama {

template <typename Record, typename RecordProjected,
          typename Extract = Identity<Record>,
          typename Convert = Converter<Record>,
          typename PredicateFilter = DefaultComparator<Record> >
class PartitionConvert {
 public:
  typedef Record record_type;
  typedef RecordProjected record_type_projected;
  typedef typename Extract::key_type key_type;

 private:
  typedef Reader<record_type> reader_type;
  typedef Writer<record_type> writer_type;
  typedef Writer<record_type_projected> writer_type_projected;

 private:
  /*
   class key_extractor {
   private:
   const Extractor& extractor;
   public:
   const K& operator()(const record_type& r) const {
   return extractor(r);
   }
   };
   */

 public:
  PartitionConvert(deceve::storage::BufferManager* bm, const std::string& fn,
                   const std::string& op, const Extract& e = Extract(),
                   const Convert& c = Convert(), size_t np = 20,
                   deceve::storage::FileMode m = deceve::storage::PRIMARY,
                   PredicateFilter filter = PredicateFilter())
      : bufferManager(bm),
        filename(fn),
        outfile_prefix(op),
        extractor(e),
        converter(c),
        number_of_partitions(np),
        mode(m),
        predicateFilter(filter){};

  ~PartitionConvert() {}

  void partition() {
    reader_type reader(bufferManager, filename);

    if (number_of_partitions == 0) number_of_partitions++;

    std::vector<writer_type_projected*> writers;
    for (unsigned int i = 0; i < number_of_partitions; i++) {
      std::stringstream s;
      // std::string s;
      // s.reserve(50);
      // s.append(outfile_prefix);
      // s.append(".");
      // s.append(std::to_string(i));
      s << outfile_prefix << "." << i;
      //			std::cout<<"PARTITION NAME outfile_prefix:
      //"<<outfile_prefix<<std::endl;
      writers.push_back(
          new writer_type_projected(bufferManager, s.str(), mode));
    }

    record_type rec;
    record_type_projected rec_projected;
    while (reader.hasNext()) {
      numberOfTuples++;
      rec = reader.nextRecord();
      rec_projected = converter(rec);
      // FILTER RECORDS
      if (predicateFilter(rec)) {
        writers[hash_of_fast(extractor(rec)) % number_of_partitions]->write(
            rec_projected);
        predicateCounter++;
      }
    }

    for (typename std::vector<writer_type_projected*>::iterator it =
             writers.begin();
         it != writers.end(); it++) {
      (*it)->close();
      delete *it;
    }

    reader.close();
  }

 public:
  double getPredicatePercentage() {
    return predicateCounter / getNumberOfTuples();
  }
  double getNumberOfTuples() {
    if (numberOfTuples == 0) {
      double tuples =
          bufferManager->getSM().getFileSizeInPagesFromCatalog(
              filename) *
          (deceve::storage::PAGE_SIZE_PERSISTENT / sizeof(record_type));
      return tuples+1;
    } else {
      return numberOfTuples+1;
    }
  }

 private:
  deceve::storage::BufferManager* bufferManager;
  std::string filename;
  std::string outfile_prefix;
  const Extract extractor;
  const Convert converter;
  // Hash hasher;
  size_t number_of_partitions;
  deceve::storage::FileMode mode;
  PredicateFilter predicateFilter;
  double numberOfTuples{0};
  double predicateCounter{0};

  size_t hash_of(const key_type& value) {
    char* k = (char*)&value;
    size_t hash = 5381;
    for (size_t i = 0; i < sizeof(key_type); i++)
      hash = ((hash << 5) + hash) + k[i];
    return hash;
  }

  // The previous hash function sometimes does not distribute data to files
  // correctly
  // Use this version better (tested that distribution is better)
  uint32_t hash_of_fast(const key_type& value) {
    const char* data = (char*)&value;
    int len = 8;
    uint32_t hash = len, tmp;
    int rem;

    if (len <= 0 || data == NULL) return 0;

    rem = len & 3;
    len >>= 2;

    /* Main loop */
    for (; len > 0; len--) {
      hash += get16bits(data);
      tmp = (get16bits(data + 2) << 11) ^ hash;
      hash = (hash << 16) ^ tmp;
      data += 2 * sizeof(uint16_t);
      hash += hash >> 11;
    }

    /* Handle end cases */
    switch (rem) {
      case 3:
        hash += get16bits(data);
        hash ^= hash << 16;
        hash ^= ((signed char)data[sizeof(uint16_t)]) << 18;
        hash += hash >> 11;
        break;
      case 2:
        hash += get16bits(data);
        hash ^= hash << 11;
        hash += hash >> 17;
        break;
      case 1:
        hash += (signed char)*data;
        hash ^= hash << 10;
        hash += hash >> 1;
    }

    /* Force "avalanching" of final 127 bits */
    hash ^= hash << 3;
    hash += hash >> 5;
    hash ^= hash << 4;
    hash += hash >> 17;
    hash ^= hash << 25;
    hash += hash >> 6;

    return hash;
  }
};
}
}

#endif
