#ifndef __REPLACEMENTSORT_HH__
#define __REPLACEMENTSORT_HH__

#include "../storage/page.hh"
#include "../storage/io.hh"
#include "../storage/identity.hh"
#include <sstream>
#include <sys/stat.h>
#include <algorithm>
#include <cstdio>
#include <vector>
#include <cstdio>
#include <sys/types.h>
#include "../utils/types.hh"
#include "../utils/helpers.hh"

#include <thread>
#include <mutex>
#include <chrono>

namespace deceve {
namespace bama {

template <typename Record, typename Extract = Identity<Record>,
          typename Compare = std::less<typename Extract::key_type> >
class ReplacementSort {
 public:
  typedef Record record_type;
  typedef typename Extract::key_type key_type;

 private:
  typedef Reader<record_type> reader_type;
  typedef Writer<record_type> writer_type;
  typedef BulkProcessor<record_type> bulk_processor_type;

 public:
  ReplacementSort(deceve::storage::BufferManager* bm, const std::string& fn,
                  const std::string& of, const Extract& extract = Extract(),
                  unsigned int nb = 1024,
                  deceve::storage::FileMode m = deceve::storage::INTERMEDIATE)
      : bufferManager(bm),
        filename(fn),
        outfile(of),
        number_of_runs(0),
        processed_so_far(0),
        number_of_buffers(nb),
        max_records(0),
        run_comparator(extract, Compare()),
        heap_comparator(extract, Compare()),
        mode(m),
        persistOutput_flag(false),
        usePersistedOutput_flag(false),
        main_tk(),
        main_tk_flag(false) {
    runNamePrefix = bufferManager->getSM().generateUniqueFileName(filename);
  };

  ~ReplacementSort() {}

  void sort() {
    // add key in the stats for calculating the pages to be written
    // 1) used in generateRun()
    // 2) delete run();
    if(main_tk.version == deceve::storage::SORT){
      uniqueIdentifier =
          bufferManager->getStatsManager().getIdentifierForSort(main_tk);
    }


    // std::lock_guard<std::mutex> sortLocker(
    //     bufferManager->getStorageManager().replacementSortingMutex);

    if (mode == deceve::storage::INTERMEDIATE && main_tk_flag) {
      /* code */
      addToNameConverter(outfile, main_tk);
    }

    if (!usePersistedOutput_flag) {
      create_runs_bulk();
    }
    // else {
    // 	COUT << "Use generated runs" << "\n";
    // 	COUT << "filename: " << filename << "\n";
    // 	COUT << "NUmber of runs: " << number_of_runs;
    // 	COUT << "NUmber of buffers: " << number_of_buffers;
    // }

    merge_runs();

    // COUT << "SORT (End) OPERATION ON FILE: " << filename << "\n";
  }

  unsigned int getNumberOfRuns() { return number_of_runs; }

  unsigned int getNumberOfBuffers() { return number_of_buffers; }

  void setNumberOfRuns(unsigned int nr) { number_of_runs = nr; }

  void setNumberOfBuffers(unsigned int nr) { number_of_buffers = nr; }

  void usePersistedResult(const std::string& fname, unsigned int n_runs) {
    usePersistedOutput_flag = true;
    filename = fname;
    number_of_runs = n_runs;
    persistResult();
  }

  void persistResult() { persistOutput_flag = true; }

  void persistResult(const deceve::storage::table_key& tk) {
    persistOutput_flag = true;
    main_tk = tk;
    main_tk_flag = true;
  }

  void setKey(const deceve::storage::table_key& key) { main_tk = key; }

 private:
  class run_cmp {
   private:
    const Extract& cmp_extract;
    const Compare& cmp_predicate;

   public:
    run_cmp(const Extract& e, const Compare& p)
        : cmp_extract(e), cmp_predicate(p){};
    bool operator()(const record_type& x, const record_type& y) const {
      //			return !cmp_predicate(cmp_extract(x),
      // cmp_extract(y));
      return cmp_extract(x) > cmp_extract(y);
    }
  };

  class heap_cmp {
   private:
    const Extract& cmp_extract;
    const Compare& cmp_predicate;

   public:
    heap_cmp(const Extract& e, const Compare& p)
        : cmp_extract(e), cmp_predicate(p){};
    bool operator()(const std::pair<record_type, unsigned int>& x,
                    const std::pair<record_type, unsigned int>& y) const {
      return cmp_extract(x.first) > cmp_extract(y.first);
      //			return !cmp_predicate(cmp_extract(x.first),
      // cmp_extract(y.first));
    }
  };

  void create_runs_bulk() {
    reader_type reader(bufferManager);
    writer_type writer(bufferManager, deceve::storage::AUXILIARY);
    number_of_runs = 0;

    // load up the heap
    bulk_processor_type bulk_reader(bufferManager, filename);
    size_t pages = 10;
    ;

    fileSizeInPages =
        bufferManager->getSM().getFileSizeInPagesFromCatalog(filename);

    // COUT << "The number of fileSizeInPages: " << filename << " :"
    // << fileSizeInPages << "\n";
    // COUT << "The pages: " << filename << " :" << pages << "\n";

    size_t numberOfPrimaryPages =
        bufferManager->getBufferPool().numberOfPrimaryPages();

    if (fileSizeInPages == 0) {
      pages = 1;
    } else {
      if (fileSizeInPages < (numberOfPrimaryPages - 10)) {
        pages = (fileSizeInPages > 9) ? fileSizeInPages - 9 : fileSizeInPages;
      } else {
        pages = (numberOfPrimaryPages - 9);
      }
    }

    // std::cout << "bufferManager->getBufferPool().numberOfPrimaryPages(): "
    //           << bufferManager->getBufferPool().numberOfPrimaryPages() <<
    //           "\n";
    // std::cout << "bufferManager->getBufferPool().getPrimaryPinCount(): "
    //           << bufferManager->getBufferPool().getPrimaryPinCount() << "\n";

    // std::cout << "Number of pages for sort: " << pages << std::endl;

    size_t number_of_partitions_calculated;

    number_of_partitions_calculated =
        bufferManager->getSM().calculateNumberOfPartitions(
            filename, numberOfPrimaryPages);

    if (number_of_partitions_calculated < 1) {
      number_of_partitions_calculated = 1;
    }

    // MANUALLY MIKE BUG
    // Add this manually for now cause otherwise join takes ages
    number_of_partitions_calculated += 19;

    pages = pages / number_of_partitions_calculated;

    if (pages <= 0) pages = 1;

    size_t current_heap_length;
    record_type* records = bulk_reader.bulkRead(0, pages, current_heap_length);
    size_t limit = current_heap_length+10;

    // bulk_reader.close();

    reader.open(filename);
    reader.skip(current_heap_length + 1);
    // start up a run
    writer.open(generate_run(), deceve::storage::AUXILIARY);
    // start counting records, just in case we can get away with
    // one pass

    //		size_t c = 1;

    while (reader.hasNext()) {
      numOfTuples++;

      std::make_heap(records, records + current_heap_length, run_comparator);

      while (current_heap_length > 0 && reader.hasNext()) {
        record_type top = records[0];
        std::pop_heap(records, records + current_heap_length, run_comparator);
        writer.write(top);
        // std::cout << "FIRST: " << top << std::endl;

        record_type rec = reader.nextRecord();
        records[current_heap_length - 1] = rec;
        if (!run_comparator(top, rec)) {
          std::push_heap(records, records + current_heap_length,
                         run_comparator);
        } else {
          current_heap_length--;
        }
      }

      // only close the run if the run has ended -- if there are
      // no more records, we'll bail in the cleanup
      if (current_heap_length == 0) {
        //				COUT << "close counter: " << c << "\n";
        //				COUT << "left heap counter: " <<
        // tCounter
        //<<
        //"\n";
        //				COUT << "right heap counter: " <<
        // rCounter
        //<<
        //"\n";
        //				COUT << "limit: " << limit << "\n";
        writer.close();

        //		COUT<<"Open file if length=0"<<"\n";
        writer.open(generate_run(), deceve::storage::AUXILIARY);
        current_heap_length = limit;
      }
    }
    // std::cout << "Separate One (number of runs): " << number_of_runs <<
    // std::endl;
    if (number_of_runs > 1) {
      // at least one run has been closed
      // sort the elements remaining in the heap (left side)

      // COUT << "NUMNER OF RUNS IS (START): " << number_of_runs
      // << "\n";

      std::sort(records, records + current_heap_length, run_comparator);

      //			for (unsigned int i = 0; i <
      // current_heap_length; i++) {
      //				writer.write(records[i]);
      //				tempC++;
      //			}

      // COUT << "current_heap_length: " << current_heap_length
      // << "\n";
      // COUT << "limit: " << limit << "\n";

      if (current_heap_length > 1) {
        for (int i = current_heap_length - 1; i >= 0; i--) {
          writer.write(records[i]);
        }
      }
      writer.close();

      // COUT << "First sort finished" << "\n";

      if (limit > 1) {
        // sort the last items of the heap (right side)
        writer.open(generate_run(), deceve::storage::AUXILIARY);

        // COUT << "Now start sorting" << "\n";

        std::sort(records + current_heap_length, records + limit,
                  run_comparator);

        // COUT << "Second sort finished" << "\n";

        //			for (unsigned int i = current_heap_length; i <
        // limit; i++) {
        //						writer.write(records[i]);
        //						tempCr++;
        //					}

        for (int i = limit - 1; i >= (int)current_heap_length; i--) {
          writer.write(records[i]);
        }
        writer.close();
      }

      // COUT << "NUMBER OF RUNS IS (END): " << number_of_runs
      // << "\n";

    } else {
      // no run has been closed, everything fits in memory -- close
      // the run file, sort the in-memory data, open the outfile
      // and write them out

      // COUT << "A single run is enough" << "\n";

      //      writer.close();
      //      std::stringstream s;
      //      unsigned int endPrefix = 0;
      //      s << runNamePrefix << "_run." << endPrefix;

      //			bufferManager->renameFile(s.str(), outfile);

      //      int numberOfRecords = 0;
      //      reader_type singleReader(bufferManager, s.str());
      //
      //      writer.open(outfile, mode);
      //
      //      int counterr = 0;
      //      while (singleReader.hasNext()) {
      //        numberOfRecords++;
      //        record_type item = singleReader.nextRecord();
      //        std::cout << "Case1: " << item << std::endl;
      //        writer.write(item);
      //        counterr++;
      //      }
      //      std::cout << "Separate: " << std::endl;
      //      //	COUT << "numberOfRecords: " << numberOfRecords << "\n";
      //      writer.close();
      //      singleReader.close();

      //   bufferManager->removeFile(s.str(), deceve::storage::AUXILIARY);

      std::sort(records, records + current_heap_length, run_comparator);

      //			for (unsigned int i = 0; i <
      // current_heap_length; i++) {
      //				COUT << "item: " << records[i] << "\n";
      //				writer.write(records[i]);
      //				tempCounter++;
      //			}

      if (current_heap_length > 1) {
        //        writer.open(s.str(), mode);
        for (int i = (int)current_heap_length - 1; i >= 0; i--) {
          // std::cout << "Case2: " << records[i] << std::endl;
          writer.write(records[i]);
        }

        //			for (unsigned int i = current_heap_length - 1; i
        //>=
        // 0; i--) {
        //				COUT << "item: " << cccc++ << " i: " <<
        // i
        //<<
        //"
        //"
        //						<< records[i] << "\n";
        //				writer.write(records[i]);
        //				tempCounter++;
        //			}
        writer.close();
      }

      if (limit > 1) {
        // sort the last items of the heap (right side)
        //        writer.open(outfile, mode);
        writer.open(generate_run(), deceve::storage::AUXILIARY);
        std::sort(records + current_heap_length, records + limit,
                  run_comparator);
        //			for (unsigned int i = current_heap_length; i <
        // limit; i++) {
        //				COUT << "item: " << cccc++ << " i: " <<
        // i
        //<<
        //"
        //"
        //						<< records[i] << "\n";
        //				writer.write(records[i]);
        //			}

        for (int i = limit - 1; i >= (int)current_heap_length; i--) {
          // std::cout << "Case3: " << records[i] << std::endl;
          writer.write(records[i]);
        }
        writer.close();

        ////std::cout << "MERGE TWO FILES" << std::endl;
        merge_two_runs(outfile);

        number_of_runs = 1;

      } else {
        std::stringstream s;
        unsigned int endPrefix = 0;
        s << runNamePrefix << "_run." << endPrefix;

        bufferManager->renameFile(s.str(), outfile);

        int numberOfRecords = 0;
        reader_type singleReader(bufferManager, s.str());

        writer.open(outfile, mode);

        int counterr = 0;
        while (singleReader.hasNext()) {
          numberOfRecords++;
          record_type item = singleReader.nextRecord();
          //   std::cout << "Case1: " << item << std::endl;
          writer.write(item);
          counterr++;
        }
        //   std::cout << "Separate: " << std::endl;
        //  COUT << "numberOfRecords: " << numberOfRecords << "\n";
        writer.close();
        singleReader.close();
        bufferManager->removeFile(s.str());
      }
    }

    reader.close();
    //	bufferManager->getBufferPool().printPages(deceve::storage::PRIMARY);

    processed_so_far = 0;
    //		aligned_delete((unsigned char*) records);

    delete[] records;

    bulk_reader.releasePages(0, pages);
  }

  void merge_runs() {
    // COUT << "Number of buffers (start of merge runs): "
    // << number_of_buffers << "\n";
    // if there's only one run we're done
    if (number_of_runs == 1) return;

    number_of_buffers = number_of_runs;
    writer_type output(bufferManager);
    std::string runFilename;
    do {
      std::pair<unsigned int, unsigned int> pair = pick_runs();
      // std::cout << "pair: " << pair.first << " <> " << pair.second << "\n";
      processed_so_far = pair.second;
      // COUT << "processed_so_far " << processed_so_far << "\n";
      // COUT << "number_of_runs " << number_of_runs << "\n";

      if (processed_so_far != number_of_runs) {
        runFilename = generate_run();
        // COUT << "Temp name: " << runFilename << "\n";
        output.open(runFilename, deceve::storage::AUXILIARY);
      } else {
        // COUT << "Final name: " << outfile << "\n";
        output.open(outfile, mode);
      }
      //			output.open(
      //					processed_so_far !=
      // number_of_runs ?
      //	generate_run() : outfile);
      merge_runs(pair.first, pair.second, output);
      output.close();

    } while (processed_so_far < number_of_runs);

    //		bufferManager->getBufferPool().printPages(
    //				deceve::storage::INTERMEDIATE);
  }

  void merge_runs(unsigned int f, unsigned int l, writer_type& output) {
    std::vector<std::pair<record_type, unsigned int> > heap;
    std::vector<reader_type*> readers;
    std::vector<unsigned int> exhausted;

    // COUT << "FILES TO BE MERGED" << "\n";
    for (unsigned int i = f; i < l; i++) {
      std::stringstream s;
      s << runNamePrefix << "_run." << i;
      // COUT << "reader: " << s.str() << "\n";
      readers.push_back(new reader_type(bufferManager, s.str()));
      // COUT
      // << "FileSize:
      // "<<bufferManager->getStorageManager().getFileSizeInPagesFromCatalog(
      // 		s.str()) << "\n";
    }

    for (unsigned int i = 0; i < l - f; i++)
      heap.push_back(std::make_pair(readers[i]->nextRecord(), i));

    std::make_heap(heap.begin(), heap.end(), heap_comparator);
    bool done = false;

    // COUT << "Heap size: " << heap.size() << "\n";

    while (!done) {
      std::pair<record_type, unsigned int> top = heap.front();
      output.write(top.first);
      std::pop_heap(heap.begin(), heap.end(), heap_comparator);
      heap.pop_back();
      if (readers[top.second]->hasNext()) {
        heap.push_back(
            std::make_pair(readers[top.second]->nextRecord(), top.second));
        std::push_heap(heap.begin(), heap.end(), heap_comparator);
      } else {
        exhausted.push_back(top.second);
      }
      done = exhausted.size() == readers.size();
    }

    for (typename std::vector<reader_type*>::iterator it = readers.begin();
         it != readers.end(); it++) {
      (*it)->close();
      delete *it;
    }

    //		if (!persistOutput_flag)

    remove_runs(f, l);

    ///	COUT<<"Number of reads: "<<deceve::bama::reads<<"\n";
    output.flush();

    //	bufferManager->getStorageManager().removeFile("out.dat");
  }

  void merge_two_runs(const std::string& fname) {
    writer_type output(bufferManager, deceve::storage::AUXILIARY);
    output.open(fname, deceve::storage::AUXILIARY);

    std::vector<std::pair<record_type, unsigned int> > heap;
    std::vector<reader_type*> readers;
    std::vector<unsigned int> exhausted;

    // COUT << "FILES TO BE MERGED" << "\n";
    for (unsigned int i = 0; i < 2; i++) {
      std::stringstream s;
      s << runNamePrefix << "_run." << i;
      // COUT << "reader: " << s.str() << "\n";
      // std::cout << "File TO BE MERGED: " << s.str() << std::endl;
      readers.push_back(new reader_type(bufferManager, s.str()));
      // COUT
      // << "FileSize:
      // "<<bufferManager->getStorageManager().getFileSizeInPagesFromCatalog(
      //    s.str()) << "\n";
    }

    record_type rec1;
    record_type rec2;

    if (readers[0]->hasNext()) rec1 = readers[0]->nextRecord();

    if (readers[1]->hasNext()) rec2 = readers[1]->nextRecord();

    while (readers[0]->hasNext() && readers[1]->hasNext()) {
      // std::cout << "COMPARE REC1 VS REC2: " << rec1 << " \n " << rec2 <<
      // std::endl;
      // std::cout << "_______________________" << std::endl;
      if (!run_comparator(rec1, rec2)) {
        output.write(rec1);
        // std::cout << "INSERT REC1: " << rec1 << std::endl;
        rec1 = readers[0]->nextRecord();
      } else {
        // std::cout << "INSERT REC2: " << rec2 << std::endl;
        output.write(rec2);
        rec2 = readers[1]->nextRecord();
      }
    }

    while (readers[0]->hasNext()) {
      record_type record1;
      record1 = readers[0]->nextRecord();
      // std::cout << "CONTINUE REC1: " << rec1 << std::endl;
      output.write(record1);
    }

    while (readers[1]->hasNext()) {
      record_type record2;
      record2 = readers[1]->nextRecord();
      // std::cout << "CONTINUE REC2: " << rec2 << std::endl;
      output.write(record2);
    }

    //    for (unsigned int i = 0; i < 2; i++)
    //      heap.push_back(std::make_pair(readers[i]->nextRecord(), i));
    //
    //    std::make_heap(heap.begin(), heap.end(), heap_comparator);
    //    bool done = false;
    //
    //    // COUT << "Heap size: " << heap.size() << "\n";
    //
    //    while (!done) {
    //      std::pair<record_type, unsigned int> top = heap.front();
    //      std::cout << "Heap record: " << top.first << std::endl;
    //      output.write(top.first);
    //      std::pop_heap(heap.begin(), heap.end(), heap_comparator);
    //      heap.pop_back();
    //      if (readers[top.second]->hasNext()) {
    //        heap.push_back(std::make_pair(readers[top.second]->nextRecord(),
    //        top.second));
    //        std::push_heap(heap.begin(), heap.end(), heap_comparator);
    //      } else {
    //        exhausted.push_back(top.second);
    //      }
    //      done = exhausted.size() == readers.size();
    //    }

    for (typename std::vector<reader_type*>::iterator it = readers.begin();
         it != readers.end(); it++) {
      (*it)->close();
      delete *it;
    }

    //    if (!persistOutput_flag)

    remove_runs(0, 2);

    /// COUT<<"Number of reads: "<<deceve::bama::reads<<"\n";
    output.flush();
    output.close();

    //  bufferManager->getStorageManager().removeFile("out.dat");
  }

  std::pair<unsigned int, unsigned int> pick_runs() const {
    // COUT<<"processed_so_far: "<<processed_so_far<<"\n";
    // COUT<<"number_of_buffers: "<<number_of_buffers<<"\n";
    // COUT<<"number_of_runs: "<<number_of_runs<<"\n";
    return std::make_pair(
        processed_so_far,
        (processed_so_far + number_of_buffers - 1 > number_of_runs
             ? number_of_runs
             : processed_so_far + number_of_buffers - 1));
  }

  void remove_runs(size_t f, size_t t) const {
    for (size_t i = f; i < t; i++) {
      std::stringstream s;
      s << runNamePrefix << "_run." << i;
      // COUT << "remove file: " << s.str();
      bufferManager->removeFile(s.str());
      if (main_tk.version == deceve::storage::SORT) {
        bufferManager->getStatsManager().deleteFromNameMonitorConverterSort(
            s.str());
      }
    }
  }

  void dump_runs() {
    reader_type reader(bufferManager);
    for (unsigned int i = 0; i < number_of_runs; i++) {
      std::stringstream s;
      s << runNamePrefix << "_run." << i;
      reader.open(s.str());
      unsigned j = 0;
      while (reader.hasNext())
        COUT << "run " << i << ", " << j++ << ": " << reader.nextRecord()
             << "\n";
      reader.close();
    }
  }

  void dump() {
    // reader_type reader(filename);
    reader_type reader(bufferManager, outfile);
    while (reader.hasNext()) COUT << "out " << reader.nextRecord() << "\n";
  }

 public:
  double getNumberOfTuples() {
    size_t fileSize =
        bufferManager->getSM().getFileSizeInPagesFromCatalog(filename);
    return fileSize *
           (deceve::storage::PAGE_SIZE_PERSISTENT / sizeof(record_type));
  }

 private:
  std::string generate_run() {
    std::stringstream s;
    s << runNamePrefix << "_run." << number_of_runs;
    // COUT << "Generate run: " << s.str() << "\n";
    number_of_runs++;

    // track number of pages of this runs that are written
    if (main_tk.version == deceve::storage::SORT) {
      bufferManager->getStatsManager().addToNameMonitorConverterSort(
          s.str(), uniqueIdentifier);
    }
    return s.str();
  }

  void addToNameConverter(std::string fname,
                          deceve::storage::table_key sort_tk) {
//    COUT << "addToNameConverter sorted filename "
//         << "\n";

    bufferManager->getSM().insertInNameConverter(fname, sort_tk);
  }

 private:
  deceve::storage::BufferManager* bufferManager;
  std::string tablename;
  std::string filename;
  std::string outfile;
  unsigned int number_of_runs;
  unsigned int processed_so_far;
  size_t number_of_buffers;
  size_t max_records;
  run_cmp run_comparator;
  heap_cmp heap_comparator;
  deceve::storage::FileMode mode;
  // This variable determines if intermediate output files should be stored
  bool persistOutput_flag;
  // This variable determines if stored results should be used
  bool usePersistedOutput_flag;

  deceve::storage::table_key main_tk;
  bool main_tk_flag;

  size_t fileSizeInPages{0};
  std::string runNamePrefix;
  double numOfTuples{0};

  long uniqueIdentifier;
};
}
}

#endif
