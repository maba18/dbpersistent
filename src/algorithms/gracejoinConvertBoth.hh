#ifndef __GRACEJOINCONVERTBOTH_HH__
#define __GRACEJOINCONVERTBOTH_HH__

// C++ headers
#include <string>
#include <functional>

#include "../algorithms/partitionjoin.hh"
#include "../algorithms/partitionConvert.hh"
#include "../algorithms/partition.hh"
#include "../storage/identity.hh"
#include "../utils/util.hh"

namespace deceve {
namespace bama {

template <typename Left, typename ProjectedFileLeft, typename Right,
          typename ProjectedFileRight, typename LeftExtract = Identity<Left>,
          typename RightExtract = Identity<Right>,
          typename ProjectedConvertLeft = Converter<Left>,
          typename ProjectedConvertRight = Converter<Right>,
          typename ProjectedExtractLeft = Identity<ProjectedFileLeft>,
          typename ProjectedExtractRight = Identity<ProjectedFileRight>,
          typename Combine = PairCombiner<Left, Right>,
          typename Less = std::less<typename LeftExtract::key_type>,
          typename PredicateLeftFilter = DefaultComparator<Left>,
          typename PredicateRightFilter = DefaultComparator<Right> >
class GraceJoinConvertBoth {
 public:
  typedef Left left_record_type;
  typedef Right right_record_type;
  typedef ProjectedFileLeft projected_record_type_left;
  typedef ProjectedFileRight projected_record_type_right;

  typedef typename Combine::record_type output_record_type;
  typedef typename LeftExtract::key_type key_type_left;
  typedef typename RightExtract::key_type key_type_right;

 private:
  typedef Reader<left_record_type> left_reader_type;
  typedef Reader<right_record_type> right_reader_type;
  typedef Writer<output_record_type> writer_type;

 public:
  GraceJoinConvertBoth(
      deceve::storage::BufferManager* bm, const std::string& l,
      const std::string& r, const std::string& o,
      const LeftExtract& le = LeftExtract(),
      const RightExtract& re = RightExtract(),
      const ProjectedConvertLeft& lc = ProjectedConvertLeft(),
      const ProjectedConvertRight& rc = ProjectedConvertRight(),
      const ProjectedExtractLeft& l_extract_convert = ProjectedExtractLeft(),
      const ProjectedExtractRight& r_extract_convert = ProjectedExtractRight(),
      const Combine& c = Combine(), size_t np = 20,
      deceve::storage::FileMode fmode = deceve::storage::PRIMARY,
      deceve::storage::ProjectionFlag pr_flag = deceve::storage::BOTH_PROJECT)
      : bufferManager(bm),
        leftfile(l),
        rightfile(r),
        outfile(o),
        left_extractor(le),
        right_extractor(re),
        pr_converter_left(lc),
        pr_converter_right(rc),
        pr_extractor_left(l_extract_convert),
        pr_extractor_right(r_extract_convert),
        combiner(c),
        less(Less()),
        number_of_partitions(np),
        persistOutput_flag(false),
        usePersistedOutput_flag(false),
        result_id(-1),
        mode(fmode),
        left_join_tv(),
        right_join_tv(),
        left_join_tk(),
        right_join_tk(),
        projectFlag(pr_flag) {
    runNamePrefix = bufferManager->getSM().generateUniqueFileName("_join");
  }

  ~GraceJoinConvertBoth() {}

  void join() {
    if (left_join_tk.version == deceve::storage::HASHJOIN && result_id != 0 &&
        result_id != 1) {
      bufferManager->getStatsManager().insertTkInStatsMap(left_join_tk);
    }
    if (right_join_tk.version == deceve::storage::HASHJOIN && result_id != 0 &&
        result_id != 2) {
      bufferManager->getStatsManager().insertTkInStatsMap(right_join_tk);
    }

    // COUT << "Result id: " << result_id << "\n";
    // COUT << "Persist Flag: " << persistOutput_flag << "\n";
    // COUT << "Use persisted results Flag: " << usePersistedOutput_flag
    // << "\n";

    // --------Calculate the number of partitions -------------
    //    size_t fileSizeInPagesLeft =
    //    bufferManager->getStorageManager().getFileSizeInPagesFromCatalog(leftfile);
    //    size_t fileSizeInPagesRight =
    //    bufferManager->getStorageManager().getFileSizeInPagesFromCatalog(rightfile);
    //    size_t fileSizeInPages = (fileSizeInPagesLeft >
    //    fileSizeInPagesRight)
    //    ? fileSizeInPagesLeft : fileSizeInPagesRight;
    //    number_of_partitions = (size_t) fileSizeInPages /
    //    (bufferManager->getBufferPool().numberOfPrimaryPages());
    //
    //    if (number_of_partitions == 0) {
    //      number_of_partitions = 1;
    //    }
//    size_t fileSizeInPagesLeft =
//        bufferManager->getSM().getFileSizeInPagesFromCatalog(leftfile);
//    size_t fileSizeInPagesRight =
//        bufferManager->getSM().getFileSizeInPagesFromCatalog(rightfile);
//
//    size_t leftTupleSize =
//        deceve::storage::PAGE_SIZE_PERSISTENT / sizeof(left_record_type);
//    size_t rightTupleSize =
//        deceve::storage::PAGE_SIZE_PERSISTENT / sizeof(right_record_type);

//    size_t fileSizeInTuplesLeft = fileSizeInPagesLeft * leftTupleSize;
//    size_t fileSizeInTuplesRight = fileSizeInPagesRight * rightTupleSize;
//
//    size_t leftTupleSizeProjected = deceve::storage::PAGE_SIZE_PERSISTENT /
//                                    sizeof(projected_record_type_left);
//    size_t rightTupleSizeProjected = deceve::storage::PAGE_SIZE_PERSISTENT /
//                                     sizeof(projected_record_type_right);

//    size_t fileSizeInPagesLeftProjected =
//        fileSizeInTuplesLeft / leftTupleSizeProjected;
//    size_t fileSizeInPagesRightProjected =
//        fileSizeInTuplesRight / rightTupleSizeProjected;

//    std::cout << "gracejoin: leftfile: " << leftfile << std::endl;
//    std::cout << "gracejoin: rightfile: " << rightfile << std::endl;
//    std::cout << "gracejoin: fileSizeInPagesLeft: " << fileSizeInPagesLeft
//              << std::endl;
//    std::cout << "gracejoin: fileSizeInPagesRight: " << fileSizeInPagesRight
//              << std::endl;
//    std::cout << "gracejoin: fileSizeInPagesLeftProjected: "
//              << fileSizeInPagesLeftProjected << std::endl;
//    std::cout << "gracejoin: fileSizeInPagesRightProjected: "
//              << fileSizeInPagesRightProjected << std::endl;

    size_t parts = bufferManager->getSM().calculateNumberOfPartitions(
        leftfile, rightfile,
        bufferManager->getBufferPool().numberOfPrimaryPages());
    number_of_partitions = parts;

    // ------------ Change filenames -------------------
    std::string leftInputPrefix = "";
    std::string leftOutputPrefix = runNamePrefix;
    std::string rightInputPrefix = "";
    std::string rightOutputPrefix = runNamePrefix;

    deceve::storage::FileMode leftMode = deceve::storage::AUXILIARY;
    deceve::storage::FileMode rightMode = deceve::storage::AUXILIARY;

    if (usePersistedOutput_flag) {
      if (result_id == 0) {
        leftInputPrefix = left_join_tv.full_output_path + "_persistent";
        leftMode = deceve::storage::INTERMEDIATE;
        rightInputPrefix = right_join_tv.full_output_path + "_persistent";
        rightMode = deceve::storage::INTERMEDIATE;
        number_of_partitions = left_join_tv.num_files;
        //    std::cout << "NUMBER OF PARTITIONS FROM FILECATALOG
        //    (gracejoin-both): "
        //            << number_of_partitions << std::endl;
      } else if (result_id == 1) {
        leftInputPrefix = left_join_tv.full_output_path + "_persistent";
        leftMode = deceve::storage::INTERMEDIATE;
        number_of_partitions = left_join_tv.num_files;
        //    std::cout << "NUMBER OF PARTITIONS FROM FILECATALOG
        //    (gracejoin-left): "
        //            << number_of_partitions << std::endl;
      } else if (result_id == 2) {
        rightInputPrefix = right_join_tv.full_output_path + "_persistent";
        rightMode = deceve::storage::INTERMEDIATE;
        number_of_partitions = right_join_tv.num_files;
        // std::cout
        //   << "NUMBER OF PARTITIONS FROM FILECATALOG (gracejoin -right ): "
        // << number_of_partitions << std::endl;
      }
    }

    if (persistOutput_flag) {
      if (result_id == 0) {
        leftOutputPrefix = runNamePrefix + "_persistent";
        leftMode = deceve::storage::INTERMEDIATE;
        rightOutputPrefix = runNamePrefix + "_persistent";
        rightMode = deceve::storage::INTERMEDIATE;
      }
      if (result_id == 1) {
        leftOutputPrefix = runNamePrefix + "_persistent";
        leftMode = deceve::storage::INTERMEDIATE;
      } else if (result_id == 2) {
        rightOutputPrefix = runNamePrefix + "_persistent";
        rightMode = deceve::storage::INTERMEDIATE;
      }
    }

    // -------- Partition Files ---------------------
    joinWithBothProjectedFile(leftInputPrefix, leftOutputPrefix, leftMode,
                              rightInputPrefix, rightOutputPrefix, rightMode);
    // << bufferManager->getFileSizeFromCatalog(outfile) << "\n";

    if (left_join_tk.version == deceve::storage::HASHJOIN && result_id != 0 &&
        result_id != 1) {
      bufferManager->getStatsManager().deleteFromNameMonitorConverterJoin(
          leftfile, leftOutputPrefix, number_of_partitions);
    }

    if (right_join_tk.version == deceve::storage::HASHJOIN && result_id != 0 &&
        result_id != 2) {
      bufferManager->getStatsManager().deleteFromNameMonitorConverterJoin(
          rightfile, rightOutputPrefix, number_of_partitions);
    }
  }

  void setStreamFlag(bool streamFlag) { isStreamFlag = streamFlag; }

 private:
  void joinWithBothProjectedFile(const std::string& leftInputPrefix,
                                 std::string leftOutputPrefix,
                                 deceve::storage::FileMode leftMode,
                                 const std::string& rightInputPrefix,
                                 std::string rightOutputPrefix,
                                 deceve::storage::FileMode rightMode) {
    if (left_join_tk.version == deceve::storage::HASHJOIN && result_id != 0 &&
        result_id != 1) {
      bufferManager->getStatsManager().addToNameMonitorConverterJoin(
          leftfile, leftOutputPrefix, left_join_tk, number_of_partitions);
    }

    if (right_join_tk.version == deceve::storage::HASHJOIN && result_id != 0 &&
        result_id != 2) {
      bufferManager->getStatsManager().addToNameMonitorConverterJoin(
          rightfile, rightOutputPrefix, right_join_tk, number_of_partitions);
    }

    // -------- Partition Files ---------------------
    PartitionConvert<left_record_type, projected_record_type_left, LeftExtract,
                     ProjectedConvertLeft, PredicateLeftFilter>
        lpartitioner(bufferManager, leftfile + leftInputPrefix,
                     leftfile + leftOutputPrefix, left_extractor,
                     pr_converter_left, number_of_partitions, leftMode);

    PartitionConvert<right_record_type, projected_record_type_right,
                     RightExtract, ProjectedConvertRight, PredicateRightFilter>
        rpartitioner(bufferManager, rightfile + rightInputPrefix,
                     rightfile + rightOutputPrefix, right_extractor,
                     pr_converter_right, number_of_partitions, rightMode);

    if (usePersistedOutput_flag) {
      if (result_id == 1) {
        leftOutputPrefix = left_join_tv.full_output_path + "_persistent";
        if (persistOutput_flag) {
          addToNameConverter(rightfile, rightOutputPrefix, right_join_tk);
        }

        rpartitioner.partition();
        deleteInputFile(rightfile + rightInputPrefix, right_join_tk);
      } else if (result_id == 2) {
        rightOutputPrefix = right_join_tv.full_output_path + "_persistent";
        if (persistOutput_flag) {
          addToNameConverter(leftfile, leftOutputPrefix, left_join_tk);
        }
        lpartitioner.partition();
        deleteInputFile(leftfile + leftInputPrefix, left_join_tk);
      } else {
        leftOutputPrefix = left_join_tv.full_output_path + "_persistent";
        rightOutputPrefix = right_join_tv.full_output_path + "_persistent";
      }
    } else {
      if (persistOutput_flag) {
        if (result_id == 0) {
          addToNameConverter(leftfile, leftOutputPrefix, left_join_tk);
          addToNameConverter(rightfile, rightOutputPrefix, right_join_tk);
        } else if (result_id == 1) {
          addToNameConverter(leftfile, leftOutputPrefix, left_join_tk);
        } else if (result_id == 2) {
          addToNameConverter(rightfile, rightOutputPrefix, right_join_tk);
        }
      }
      //     bufferManager->getBufferPool().printDirtyPagesForAllDataStructures();
      lpartitioner.partition();
      //    bufferManager->getBufferPool().printDirtyPagesForAllDataStructures();
      rpartitioner.partition();

      deleteInputFile(leftfile + leftInputPrefix, left_join_tk);
      //   bufferManager->getBufferPool().printDirtyPagesForAllDataStructures();
      deleteInputFile(rightfile + rightInputPrefix, right_join_tk);
      //  bufferManager->getBufferPool().printDirtyPagesForAllDataStructures();
    }
    // ---------- Merge Files ----------------------
    PartitionJoin<projected_record_type_left, projected_record_type_right,
                  ProjectedExtractLeft, ProjectedExtractRight, Combine, Less>
        joiner(bufferManager, leftfile + leftOutputPrefix,
               rightfile + rightOutputPrefix, outfile, pr_extractor_left,
               pr_extractor_right, combiner, number_of_partitions,
               usePersistedOutput_flag, persistOutput_flag, result_id);
    joiner.setStreamFlag(isStreamFlag);

    joiner.join();

    leftNumOfTuples = lpartitioner.getNumberOfTuples();
    predicatePercentageLeft = lpartitioner.getPredicatePercentage();
    rightNumOfTuples = rpartitioner.getNumberOfTuples();
    predicatePercentageRight = rpartitioner.getPredicatePercentage();
    numberOfTuplesOutput = joiner.getNumberOfTuplesOutput();

    //    std::cout << " GRACE-JOIN ESTIMATES BEGIN" << std::endl;
    //    std::cout << "joiner->leftNumOfTuples(): " << leftNumOfTuples <<
    //    std::endl;
    //    std::cout << "joiner->getPredicatePercentageLeft(): "
    //              << predicatePercentageLeft << std::endl;
    //    std::cout << "joiner->rightNumOfTuples(): " << rightNumOfTuples
    //              << std::endl;
    //    std::cout << "joiner->getPredicatePercentageRight(): "
    //              << predicatePercentageRight << std::endl;
    //
    //    std::cout << "joiner->numberOfTuplesOutput(): " <<
    //    numberOfTuplesOutput
    //              << std::endl;
    //    std::cout << "joiner->getOutputPercentage(): " <<
    //    getOutputPercentage()
    //              << std::endl;
    //    std::cout << " GRACE-JOIN ESTIMATES END " << std::endl;
  }

 public:
  void usePersistedResult(deceve::storage::table_value tv, int r_id = 0) {
    usePersistedOutput_flag = true;
    result_id = r_id;

    if (r_id == 1) {
      left_join_tv = tv;
    } else {
      right_join_tv = tv;
    }
  }

  void usePersistedResult(deceve::storage::table_value ltv,
                          deceve::storage::table_value rtv, int r_id = 0) {
    usePersistedOutput_flag = true;
    result_id = r_id;
    left_join_tv = ltv;
    right_join_tv = rtv;
  }

  void persistResult(int r_id = 0) {
    persistOutput_flag = true;
    result_id = r_id;
  }

  void persistResult(deceve::storage::table_key tk, int r_id = 0) {
    persistOutput_flag = true;
    result_id = r_id;

    if (r_id == 1) {
      left_join_tk = tk;
    } else {
      right_join_tk = tk;
    }
  }

  void persistResult(deceve::storage::table_key ltk,
                     deceve::storage::table_key rtk, int r_id = 0) {
    persistOutput_flag = true;
    result_id = r_id;
    left_join_tk = ltk;
    right_join_tk = rtk;
  }

  void setJoinTableKeys(const deceve::storage::table_key& leftKey,
                        const deceve::storage::table_key& rightKey) {
    left_join_tk = leftKey;
    right_join_tk = rightKey;
  }

  size_t getNumberOfPartitions() { return number_of_partitions; }

  int getResultId() { return result_id; }

  std::string getRunNamePrefixOfPersistentFile() { return runNamePrefix; }

  void addToNameConverter(std::string filename, std::string prefix,
                          deceve::storage::table_key join_tk) {
    // COUT << "addToNameConverter " << "\n";
    for (unsigned int i = 0; i < number_of_partitions; ++i) {
      std::stringstream s;
      s << filename << prefix << "." << i;
      // COUT << "Add filename: " << s.str() << " for tk: " << join_tk
      // << "\n";
      bufferManager->getSM().insertInNameConverter(s.str(), join_tk);
    }
  }

  void deleteInputFile(const std::string& fname,
                       deceve::storage::table_key tk) {
    if (!bufferManager->getSM().isInTableCatalog(fname) &&
        !bufferManager->getSM().isInPersistedFileCatalog(tk)) {
//      std::cout << "DELETE FROM GRACE-JOIN: " << fname << " " << tk << "\n";
      bufferManager->removeFile(fname);
    }
  }

  double getPredicatePercentageLeft() { return predicatePercentageLeft; }
  double getPredicatePercentageRight() { return predicatePercentageRight; }
  double getNumberOfTuplesOutput() { return numberOfTuplesOutput; }
  double getNumberOfLeftTuplesOutput() { return leftNumOfTuples; }
  double getNumberOfRightTuplesOutput() { return rightNumOfTuples; }
  double getOutputPercentage() {
    double max = (leftNumOfTuples > rightNumOfTuples) ? leftNumOfTuples
                                                      : rightNumOfTuples;
    return numberOfTuplesOutput / (max + 1);
  }

 private:
  deceve::storage::BufferManager* bufferManager;
  std::string leftfile;
  std::string rightfile;
  std::string outfile;
  LeftExtract left_extractor;
  RightExtract right_extractor;
  ProjectedConvertLeft pr_converter_left;
  ProjectedConvertRight pr_converter_right;
  ProjectedExtractLeft pr_extractor_left;
  ProjectedExtractRight pr_extractor_right;
  Combine combiner;
  Less less;
  size_t number_of_partitions;
  // This variable determines if intermediate output files should be stored
  bool persistOutput_flag;
  // This variable determines if stored results should be used
  bool usePersistedOutput_flag;
  int result_id;
  deceve::storage::FileMode mode;
  std::string runNamePrefix;

  // deceve::storage::table_value join_tv;
  deceve::storage::table_value left_join_tv;
  deceve::storage::table_value right_join_tv;
  // deceve::storage::table_key join_tk;
  deceve::storage::table_key left_join_tk;
  deceve::storage::table_key right_join_tk;
  deceve::storage::ProjectionFlag projectFlag;
  bool isStreamFlag{false};

  double leftNumOfTuples{0};
  double rightNumOfTuples{0};
  double predicatePercentageLeft{0};
  double predicatePercentageRight{0};
  double numberOfTuplesOutput{0};
};
}  // namespace bama
}  // namespace deceve

#endif
