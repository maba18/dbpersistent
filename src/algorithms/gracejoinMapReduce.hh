/*
 * gracejoinMapReduce.hh
 *
 *  Created on: 31 Mar 2015
 *      Author: michail
 */

#ifndef GRACEJOINMAPREDUCE_HH_
#define GRACEJOINMAPREDUCE_HH_

#include "../algorithms/partitionjoinMapReduce.hh"
#include "../algorithms/partitionMapReduce.hh"
#include "../algorithms/partitionjoin.hh"
#include "../storage/identity.hh"
#include "../utils/util.hh"
//#include <iostream>
//#include <cstdlib>
//#include <ctime>
//#include <sys/time.h>

namespace deceve {
namespace bama {

template<typename Left, typename Right, typename ReducedRecord,
		typename LeftTransformer, typename RightTransformer,
		typename LeftExtract = Identity<Left>, typename RightExtract = Identity<
				Right>, typename OutputExtract = Identity<ReducedRecord>,
		typename Combine = PairCombiner<Left, Right>, typename Less = std::less<
				typename LeftExtract::key_type> >
class GraceJoinMapReduce {
public:
	typedef Left left_record_type;
	typedef Right right_record_type;
	typedef ReducedRecord reduced_record_type;
	typedef typename Combine::record_type combined_record_type;
	typedef typename LeftExtract::key_type key_type;

private:
	typedef Reader<left_record_type> left_reader_type;
	typedef Reader<right_record_type> right_reader_type;
	typedef Writer<reduced_record_type> left_writer_type;
	typedef Writer<reduced_record_type> right_writer_type;
	typedef Writer<combined_record_type> writer_type;

public:
	GraceJoinMapReduce(deceve::storage::BufferManager *bm, const std::string& l,
			const std::string& r, const std::string& o,
			const LeftTransformer& lt, const RightTransformer& rt,
			const LeftExtract& le = LeftExtract(), const RightExtract& re =
					RightExtract(), const OutputExtract& oe = OutputExtract(),
			const Combine& c = Combine(), size_t np = 20,
			deceve::storage::FileMode fmode = deceve::storage::PRIMARY) :
			bufferManager(bm), leftfile(l), rightfile(r), outfile(o), left_transformer(
					lt), right_transformer(rt), left_extractor(le), right_extractor(
					re), output_extractor(oe), combiner(c), less(Less()), number_of_partitions(
					np), persistOutput_flag(false), usePersistedOutput_flag(
			false), result_id(-1), mode(fmode) {

		runNamePrefix =
				bufferManager->getSM().generateUniqueFileName(
						"_join");

	}
	;

	~GraceJoinMapReduce() {
	}

	void join() {

		COUT << "Result id: " << result_id << "\n";
		COUT << "Persist Flag: " << persistOutput_flag << "\n";
		COUT << "Use persisted results Flag: " << usePersistedOutput_flag
				<< "\n";

		//--------------Calculate the number of partitions ----------------------

//		size_t fileSizeInPagesLeft =
//				bufferManager->getStorageManager().getFileSizeInPagesFromCatalog(
//						leftfile);
//
//		size_t fileSizeInPagesRight =
//				bufferManager->getStorageManager().getFileSizeInPagesFromCatalog(
//						rightfile);
//
//		size_t fileSizeInPages =
//				(fileSizeInPagesLeft > fileSizeInPagesRight) ?
//						fileSizeInPagesLeft : fileSizeInPagesRight;
		bufferManager->getSM().getFileSizeInPagesFromCatalog(
				leftfile);
//		number_of_partitions = (size_t) fileSizeInPages
//				/ (bufferManager->getBufferPool().numberOfPrimaryPages());

		number_of_partitions = 10;

		if (number_of_partitions == 0) {
			number_of_partitions = 1;
		}

		//---------------- Change filenames -----------------------

		std::string leftInputPrefix = "";
		std::string leftOutputPrefix = runNamePrefix;
		std::string rightInputPrefix = "";
		std::string rightOutputPrefix = runNamePrefix;

		deceve::storage::FileMode leftMode = deceve::storage::PRIMARY;
		deceve::storage::FileMode rightMode = deceve::storage::PRIMARY;

		if (usePersistedOutput_flag) {
			if (result_id == 0) {
				leftInputPrefix = left_join_tv.full_output_path + "_persistent";
				rightInputPrefix = right_join_tv.full_output_path
						+ "_persistent";
				number_of_partitions = left_join_tv.num_files;
			} else if (result_id == 1) {
				leftInputPrefix = left_join_tv.full_output_path + "_persistent";
				number_of_partitions = left_join_tv.num_files;
			} else if (result_id == 2) {
				rightInputPrefix = right_join_tv.full_output_path
						+ "_persistent";
				number_of_partitions = right_join_tv.num_files;
			}
		}

		if (persistOutput_flag) {
			if (result_id == 0) {
				leftOutputPrefix = runNamePrefix + "_persistent";
				leftMode = deceve::storage::INTERMEDIATE;
				rightOutputPrefix = runNamePrefix + "_persistent";
				rightMode = deceve::storage::INTERMEDIATE;
			}
			if (result_id == 1) {
				leftOutputPrefix = runNamePrefix + "_persistent";
				leftMode = deceve::storage::INTERMEDIATE;
			} else if (result_id == 2) {
				rightOutputPrefix = runNamePrefix + "_persistent";
				rightMode = deceve::storage::INTERMEDIATE;
			}
		}

		//-------------- Partition Files ---------------------------
		PartitionMapReduce<Left, ReducedRecord, LeftTransformer, LeftExtract,
				OutputExtract> lpartitioner(bufferManager,
				leftfile + leftInputPrefix, leftfile + leftOutputPrefix,
				left_transformer, left_extractor, output_extractor,
				number_of_partitions, leftMode);

		PartitionMapReduce<Right, ReducedRecord, RightTransformer, RightExtract,
				OutputExtract> rpartitioner(bufferManager,
				rightfile + rightInputPrefix, rightfile + rightOutputPrefix,
				right_transformer, right_extractor, output_extractor,
				number_of_partitions, rightMode);

		if (usePersistedOutput_flag) {
			if (result_id == 1) {
				leftOutputPrefix = left_join_tv.full_output_path
						+ "_persistent";
				if (persistOutput_flag)
					addToNameConverter(rightfile, rightOutputPrefix,
							right_join_tk);

				rpartitioner.partition();

			} else if (result_id == 2) {
				rightOutputPrefix = right_join_tv.full_output_path
						+ "_persistent";
				if (persistOutput_flag)
					addToNameConverter(leftfile, leftOutputPrefix,
							left_join_tk);
				lpartitioner.partition();

			} else {
				leftOutputPrefix = left_join_tv.full_output_path
						+ "_persistent";
				rightOutputPrefix = right_join_tv.full_output_path
						+ "_persistent";
			}
		} else {

			if (persistOutput_flag) {
				if (result_id == 0) {
					addToNameConverter(leftfile, leftOutputPrefix,
							left_join_tk);
					addToNameConverter(rightfile, rightOutputPrefix,
							right_join_tk);
				} else if (result_id == 1) {
					addToNameConverter(leftfile, leftOutputPrefix,
							left_join_tk);
				} else if (result_id == 2) {
					addToNameConverter(rightfile, rightOutputPrefix,
							right_join_tk);
				}
			}
			COUT << "Partition both files" << "\n";

			lpartitioner.partition();
			rpartitioner.partition();

		}

		//-------------- Merge Files ---------------------------

		for (size_t i = 0; i < number_of_partitions; ++i) {

			std::cout << "Merge file: " << leftfile + leftOutputPrefix << " "
					<< i << "\n";
			std::cout << "Merge file: " << rightfile + rightOutputPrefix << " "
					<< i << "\n";

			PartitionJoin<ReducedRecord, ReducedRecord, OutputExtract,
			OutputExtract, Combine, Less> joiner(bufferManager,
					leftfile + leftOutputPrefix + std::to_string(i), rightfile + rightOutputPrefix+ std::to_string(i),
					outfile+".mr."+ std::to_string(i), output_extractor, output_extractor, combiner,
					number_of_partitions, usePersistedOutput_flag,
					persistOutput_flag, result_id);
			joiner.join();
			COUT << "output FILESIZE: " << outfile << ": "
					<< bufferManager->getFileSizeFromCatalog(outfile) << "\n";
		}

//		PartitionJoin<left_record_type, right_record_type, LeftExtract,
//				RightExtract, Combine, Less> joiner(bufferManager,
//				leftfile + leftOutputPrefix, rightfile + rightOutputPrefix,
//				outfile, left_extractor, right_extractor, combiner,
//				number_of_partitions, usePersistedOutput_flag,
//				persistOutput_flag, result_id);
//		joiner.join();
//		COUT << "output FILESIZE: " << outfile << ": "
//				<< bufferManager->getFileSizeFromCatalog(outfile) << "\n";

	}

private:
	deceve::storage::BufferManager *bufferManager;
	std::string leftfile;
	std::string rightfile;
	std::string outfile;
	LeftTransformer left_transformer;
	RightTransformer right_transformer;
	LeftExtract left_extractor;
	RightExtract right_extractor;
	OutputExtract output_extractor;
	Combine combiner;
	Less less;
	size_t number_of_partitions;
//This variable determines if intermediate output files should be stored
	bool persistOutput_flag;
//This variable determines if stored results should be used
	bool usePersistedOutput_flag;
	int result_id;
	deceve::storage::FileMode mode;
	std::string runNamePrefix;

//	deceve::storage::table_value join_tv;
	deceve::storage::table_value left_join_tv;
	deceve::storage::table_value right_join_tv;
//	deceve::storage::table_key join_tk;
	deceve::storage::table_key left_join_tk;
	deceve::storage::table_key right_join_tk;

public:

	void usePersistedResult(deceve::storage::table_value tv, int r_id = 0) {

		usePersistedOutput_flag = true;
		result_id = r_id;

		if (r_id == 1) {
			left_join_tv = tv;
		} else {
			right_join_tv = tv;
		}
	}

	void usePersistedResult(deceve::storage::table_value ltv,
			deceve::storage::table_value rtv, int r_id = 0) {

		usePersistedOutput_flag = true;
		result_id = r_id;
		left_join_tv = ltv;
		right_join_tv = rtv;

	}

	void persistResult(int r_id = 0) {

		persistOutput_flag = true;
		result_id = r_id;
	}

	void persistResult(deceve::storage::table_key tk, int r_id = 0) {

		persistOutput_flag = true;
		result_id = r_id;

		if (r_id == 1) {
			left_join_tk = tk;
		} else {
			right_join_tk = tk;
		}
	}

	void persistResult(deceve::storage::table_key ltk,
			deceve::storage::table_key rtk, int r_id = 0) {

		persistOutput_flag = true;
		result_id = r_id;
		left_join_tk = ltk;
		right_join_tk = rtk;

	}

	size_t getNumberOfPartitions() {
		return number_of_partitions;
	}

	int getResultId() {
		return result_id;
	}

	std::string getRunNamePrefixOfPersistentFile() {

		return runNamePrefix;
	}

	void addToNameConverter(std::string filename, std::string prefix,
			deceve::storage::table_key join_tk) {

		COUT << "addToNameConverter " << "\n";

		for (unsigned int i = 0; i < number_of_partitions; ++i) {
			std::stringstream s;
			s << filename << prefix << "." << i;
			COUT << "Add filename: " << s.str() << " for tk: " << join_tk
					<< "\n";
			bufferManager->getSM().insertInNameConverter(s.str(),
					join_tk);
		}
	}

};

}
}

#endif
