#ifndef __HASHJOIN_HH__
#define __HASHJOIN_HH__

#include "../storage/page.hh"
#include "../storage/io.hh"
#include "../storage/identity.hh"
#include <sstream>
#include <sys/stat.h>
#include <algorithm>
#include <cstdio>
#include <vector>
#include <map>
#include "../utils/global.hh"
#include "../utils/util.hh"

#include <mutex>

namespace deceve {
namespace bama {

template<typename Left, typename Right, typename LeftExtract = Identity<Left>, typename RightExtract = Identity<Right>,
    typename Combine = PairCombiner<Left, Right>, typename Less = std::less<typename LeftExtract::key_type> >
class HashJoin {
 public:
  typedef Left left_record_type;
  typedef Right right_record_type;
  typedef typename Combine::record_type output_record_type;
  typedef typename LeftExtract::key_type key_type;

 private:
  typedef Reader<left_record_type> left_reader_type;
  typedef Reader<right_record_type> right_reader_type;
  typedef Writer<left_record_type> left_writer_type;
  typedef Writer<right_record_type> right_writer_type;
  typedef Writer<output_record_type> writer_type;

 public:
  HashJoin(deceve::storage::BufferManager* bm, const std::string& l, const std::string& r, const std::string& o,
           const LeftExtract& le = LeftExtract(), const RightExtract& re = RightExtract(), const Combine& c = Combine(),
           size_t np = 20, deceve::storage::FileMode fmode = deceve::storage::PRIMARY)
      : bufferManager(bm),
        leftfile(l),
        rightfile(r),
        outfile(o),
        left_extractor(le),
        right_extractor(re),
        combiner(c),
        number_of_partitions(np + 1),
        left_sub(true),
        right_sub(true),
        persistOutput_flag(false),
        usePersistedOutput_flag(false),
        result_id(-1),
        mode(fmode),
        join_tv(),
        left_join_tv(),
        right_join_tv(),
        left_join_tk(),
        right_join_tk() {
    runNamePrefix = bufferManager->getSM().generateUniqueFileName("_join");

  }
  ;

  ~HashJoin() {
  }

  void join() {
    if (bufferManager->getFileSizeFromCatalog(leftfile) / deceve::storage::PAGE_SIZE_PERSISTENT == 1) {
      number_of_partitions = 1;
    }

    //    size_t fileSizeInPages =
    //    bufferManager->getStorageManager().getFileSizeInPagesFromCatalog(leftfile);
    //    number_of_partitions = (size_t) fileSizeInPages /
    //    (bufferManager->getBufferPool().numberOfPrimaryPages());
    //
    //    number_of_partitions = 5;

    // Calculate number of partitions
    //    size_t fileSizeInPagesLeft =
    //    bufferManager->getStorageManager().getFileSizeInPagesFromCatalog(leftfile);
    //    size_t fileSizeInPagesRight =
    //    bufferManager->getStorageManager().getFileSizeInPagesFromCatalog(rightfile);
    //
    //    size_t fileSizeInPages = (fileSizeInPagesLeft > fileSizeInPagesRight)
    //    ? fileSizeInPagesLeft : fileSizeInPagesRight;
    //    bufferManager->getStorageManager().getFileSizeInPagesFromCatalog(leftfile);
    //    number_of_partitions = (size_t) fileSizeInPages /
    //    (bufferManager->getBufferPool().numberOfPrimaryPages());
    //
    //    if (number_of_partitions == 0) {
    //      number_of_partitions = 1;
    //    }
    //
    //    number_of_partitions = number_of_partitions + 5;

    size_t parts = bufferManager->getSM().calculateNumberOfPartitions(
        leftfile, rightfile, bufferManager->getBufferPool().numberOfPrimaryPages());

    //    std::cout << "HASHJOIN AMOUNT OF PARTITIONS FROM STORAGE MANAGER: "
    //              << parts;
    number_of_partitions = parts;
//    std::cout << "HASHJOIN Final Number of Partitions OF PARTITIONS FROM "
//              "STORAGE MANAGER: "
//              << parts;

    writer_type writer(bufferManager, outfile);
    //    std::cout << "HASHJOIN: Number of partitions is: " <<
    //    number_of_partitions
    //              << "\n";

    if (!persistOutput_flag && !usePersistedOutput_flag) {
      //      std::cout << "HASHJOIN: Make simple pass"
      //                << "\n";
      for (size_t p = 0; p < number_of_partitions; p++) {
        make_pass(writer, p);
      }
    }
    if (persistOutput_flag) {
      //      std::cout << "HASHJOIN: Persist output results"
      //                << "\n";
      //      std::cout << "RESULT_ID: " << result_id << "\n";
      if (result_id == 0 || result_id == 1 || result_id == 2) {
        for (size_t p = 0; p < number_of_partitions; p++) {
          make_pass_and_persist_results(writer, p);
        }
      }
    }
    if (usePersistedOutput_flag) {
      //      std::cout << "HASHJOIN: Use Generated results"
      //                << "\n";
      //      std::cout << "Id for that is: " << result_id << "\n";

      if (result_id == 0) {
        number_of_partitions = left_join_tv.num_files;
        //        std::cout << "NUMBER OF PARTITIONS FROM FILECATALOG: "
        //                  << number_of_partitions << std::endl;
        //        std::cout << "************* make_pass_for_both files"
        //                  << "\n";
        //        std::cout << "left_join_tv: " << left_join_tv << "\n";
        //        std::cout << "right_join_tv: " << right_join_tv << "\n";
        for (size_t p = 0; p < number_of_partitions; p++) {
          use_generated_persistent_files_for_both_tables(writer, p);
        }
      } else if (result_id == 1) {
        number_of_partitions = left_join_tv.num_files;
        //        std::cout << "NUMBER OF PARTITIONS FROM FILECATALOG: "
        //                  << number_of_partitions << std::endl;
        //        std::cout << "************* make_pass_for_one_file_left"
        //                  << "\n";
        //        std::cout << "left_join_tv: " << left_join_tv << "\n";

        for (size_t p = 0; p < number_of_partitions; p++) {
          use_generated_persistent_file_for_left_side(writer, p);
        }
      }

      else if (result_id == 2) {
        number_of_partitions = right_join_tv.num_files;
        //        std::cout << "NUMBER OF PARTITIONS FROM FILECATALOG: "
        //                  << number_of_partitions << std::endl;
        //        std::cout << "************* make_pass_for_one_file_left"
        //                  << "\n";
        //        std::cout << "right_join_tv: " << right_join_tv << "\n";

        for (size_t p = 0; p < number_of_partitions; p++) {
          use_generated_persistent_file_for_right_side(writer, p);
        }
      }

      //      std::cout << "join_tv: " << join_tv << "\n";
    }

    //  if (!usePersistedOutput_flag) {
    //		std::lock_guard < std::mutex
    //		> lockfile(
    //			bufferManager->getStorageManager().removeGeneratedFileMutex);

    //    std::cout << "Number of partitions: " << number_of_partitions << "\n";
    //    std::cout << "runNamePrefix: " << runNamePrefix << " "
    //              << number_of_partitions << "\n";

    if (number_of_partitions >= 1) {
      //      std::cout << "result_id" << result_id << "\n";

      if (result_id == -1 || result_id == 0) {
        //        std::cout << "JOIN: Remove both files: " <<
        //        number_of_partitions
        //                  << "\n";
        //        std::cout << generate_partition_input(leftfile,
        //        number_of_partitions)
        //                  << "\n";
        //        std::cout << generate_partition_input(rightfile,
        //        number_of_partitions)
        //                  << "\n";

        if (!usePersistedOutput_flag) {
          bufferManager->removeFile(generate_partition_input(leftfile, number_of_partitions));
          bufferManager->removeFile(generate_partition_input(rightfile, number_of_partitions));
        }
      }

      else if (result_id == 1) {
        //        std::cout << "JOIN: Remove right files: " <<
        //        number_of_partitions
        //                  << "\n";
        //        std::cout << generate_partition_input(rightfile,
        //        number_of_partitions)
        //                  << "\n";
        bufferManager->removeFile(generate_partition_input(rightfile, number_of_partitions));
        if (!usePersistedOutput_flag) {
          bufferManager->removeFile(generate_partition_input(leftfile, number_of_partitions));
        }
      } else if (result_id == 2) {
        //        std::cout << "JOIN: Remove k files: " << number_of_partitions
        //        << "\n";
        //        std::cout << generate_partition_input(leftfile,
        //        number_of_partitions)
        //                  << "\n";
        bufferManager->removeFile(generate_partition_input(leftfile, number_of_partitions));
        if (!usePersistedOutput_flag) {
          bufferManager->removeFile(generate_partition_input(rightfile, number_of_partitions));
        }
      }
    }
    // }

    writer.close();
  }

 private:
  deceve::storage::BufferManager* bufferManager;
  std::string leftfile;
  std::string rightfile;
  std::string outfile;
  LeftExtract left_extractor;
  RightExtract right_extractor;
  Combine combiner;
  Less less;
  size_t number_of_partitions;bool left_sub;bool right_sub;
  // This variable determines if intermediate output files should be stored
  bool persistOutput_flag;
  // This variable determines if stored results should be used
  bool usePersistedOutput_flag;
  int result_id;
  deceve::storage::FileMode mode;
  std::string runNamePrefix;

  deceve::storage::table_value join_tv;
  deceve::storage::table_value left_join_tv;
  deceve::storage::table_value right_join_tv;

  deceve::storage::table_key left_join_tk;
  deceve::storage::table_key right_join_tk;

  std::string generate_partition_input(const std::string& f, size_t p) const {
    if (p == 0)
      return f;
    std::stringstream s;
    s << f << runNamePrefix << "." << p;
    return s.str();
  }

  std::string generate_partition_output(const std::string& f, size_t p) const {
    std::stringstream s;
    s << f << runNamePrefix << "." << (p + 1);
    return s.str();
  }

  std::string generate_partition_persistent(const std::string& f, size_t p) const {
    std::stringstream s;
    s << f << runNamePrefix << "_persistent." << p;
    return s.str();
  }

  std::string generate_partition_persistent(const std::string& f, const std::string& fprefix, size_t p) const {
    std::stringstream s;
    s << f << fprefix << "_persistent." << p;
    return s.str();
  }

  std::string use_partition_persistent(const std::string& fprefix, size_t p) const {
    std::stringstream s;
    s << fprefix << p;
    return s.str();
  }

  std::string generate_name_for_existing_persistent_files(const std::string& f, size_t p) const {
    std::stringstream s;
    s << runNamePrefix << f << runNamePrefix << "." << p;
    return s.str();
  }

  void make_pass(writer_type& output, size_t partition) {
    // left_reader_type left_reader(leftfile);
    // right_reader_type right_reader(rightfile);
    left_reader_type left_reader(bufferManager, generate_partition_input(leftfile, partition));
    right_reader_type right_reader(bufferManager, generate_partition_input(rightfile, partition));
    left_record_type left_record;
    right_record_type right_record;
    left_writer_type left_writer(bufferManager);
    right_writer_type right_writer(bufferManager);
    size_t prt;
    std::multimap<key_type, left_record_type, Less> map;
    std::pair<typename std::multimap<key_type, left_record_type, Less>::iterator,
        typename std::multimap<key_type, left_record_type, Less>::iterator> range;
    // left_writer.open(generate_temp_filename(leftfile));
    // right_writer.open(generate_temp_filename(rightfile));
    left_writer.open(generate_partition_output(leftfile, partition));
    right_writer.open(generate_partition_output(rightfile, partition));

    // size_t count = 0;
    while (left_reader.hasNext()) {
      left_record = left_reader.nextRecord();
      //			COUT<<"left record: "<<left_record<<"\n";
      prt = hash_of_fast(left_extractor(left_record)) % number_of_partitions;
      if (prt == partition) {
        map.insert(std::pair<key_type, left_record_type>(left_extractor(left_record), left_record));
      } else {
        left_writer.write(left_record);
      }
      // count++;
    }
    // count = 0;
    while (right_reader.hasNext()) {
      right_record = right_reader.nextRecord();
      //			COUT << "right record " << right_record << "\n";
      prt = hash_of_fast(right_extractor(right_record)) % number_of_partitions;
      if (prt == partition) {
        range = map.equal_range(right_extractor(right_record));
        for (typename std::multimap<key_type, left_record_type>::iterator it = range.first; it != range.second; it++) {
          output.write(combiner(it->second, right_record));
        }

      } else {
        right_writer.write(right_record);
      }
      // count++;
    }
    // COUT << "right count: " << count << "\n";
    left_reader.close();
    right_reader.close();
    left_writer.close();
    right_writer.close();

    // substitute_file(left_sub, leftfile,
    //                generate_temp_filename(leftfile));
    // substitute_file(right_sub, rightfile,
    //                generate_temp_filename(rightfile));
    //	std::lock_guard < std::mutex
    //	> lockfile(
    //		bufferManager->getStorageManager().removeGeneratedFileMutex);
    if (partition != 0) {
      bufferManager->removeFile(generate_partition_input(leftfile, partition));
      bufferManager->removeFile(generate_partition_input(rightfile, partition));
    }
  }

  void make_pass_and_persist_results(writer_type& output, size_t partition) {
    left_reader_type left_reader(bufferManager, generate_partition_input(leftfile, partition));
    right_reader_type right_reader(bufferManager, generate_partition_input(rightfile, partition));
    left_record_type left_record;
    right_record_type right_record;
    left_writer_type left_writer(bufferManager);
    right_writer_type right_writer(bufferManager);

    // Persist results
    left_writer_type left_writer_persistent(bufferManager);
    right_writer_type right_writer_persistent(bufferManager);

    size_t prt;
    std::multimap<key_type, left_record_type, Less> map;
    std::pair<typename std::multimap<key_type, left_record_type, Less>::iterator,
        typename std::multimap<key_type, left_record_type, Less>::iterator> range;

    //    if (result_id != 0 && result_id != 1) {
    left_writer.open(generate_partition_output(leftfile, partition));
    //    }
    //    if (result_id != 0 && result_id != 1) {
    right_writer.open(generate_partition_output(rightfile, partition));
    //    }
    // generate names for persistent partitions
    if (result_id == 0) {
      //      bufferManager->getStorageManager().insertInNameConverter(s.str(),
      //                                                                    join_tk);
      std::string leftPartitionName = generate_partition_persistent(leftfile, partition);
      std::string rightPartitionName = generate_partition_persistent(rightfile, partition);
      bufferManager->getSM().insertInNameConverter(leftPartitionName, left_join_tk);
      bufferManager->getSM().insertInNameConverter(rightPartitionName, right_join_tk);
      //      std::cout << "BOTH PERSISTENT FILES: (left): " <<
      //      leftPartitionName
      //                << std::endl;
      //      std::cout << "BOTH PERSISTENT FILES: (right) " <<
      //      rightPartitionName
      //                << std::endl;
      // partitionName
      left_writer_persistent.open(leftPartitionName, mode);
      right_writer_persistent.open(rightPartitionName, mode);
    } else if (result_id == 1) {
      std::string leftPartitionName = generate_partition_persistent(leftfile, partition);
      bufferManager->getSM().insertInNameConverter(leftPartitionName, left_join_tk);
      //      std::cout << "BOTH PERSISTENT FILES (left): " << leftPartitionName
      //                << std::endl;
      left_writer_persistent.open(generate_partition_persistent(leftfile, partition), mode);
    } else if (result_id == 2) {
      std::string rightPartitionName = generate_partition_persistent(rightfile, partition);
      bufferManager->getSM().insertInNameConverter(rightPartitionName, right_join_tk);
      //      std::cout << "BOTH PERSISTENT FILES (right): " <<
      //      rightPartitionName
      //                << std::endl;
      right_writer_persistent.open(generate_partition_persistent(rightfile, partition), mode);
    }

    // size_t count = 0;
    while (left_reader.hasNext()) {
      left_record = left_reader.nextRecord();
      //			COUT<<"left record: "<<left_record<<"\n";
      prt = hash_of_fast(left_extractor(left_record)) % number_of_partitions;
      if (prt == partition) {
        map.insert(std::pair<key_type, left_record_type>(left_extractor(left_record), left_record));

        // Persist each partition
        if (result_id == 0 || result_id == 1)
          left_writer_persistent.write(left_record);
      } else {
        left_writer.write(left_record);
      }
      // count++;
    }
    // count = 0;
    while (right_reader.hasNext()) {
      right_record = right_reader.nextRecord();
      //			COUT << "right record " << right_record << "\n";
      prt = hash_of_fast(right_extractor(right_record)) % number_of_partitions;
      if (prt == partition) {
        range = map.equal_range(right_extractor(right_record));
        for (typename std::multimap<key_type, left_record_type>::iterator it = range.first; it != range.second; it++) {
          output.write(combiner(it->second, right_record));
        }

        // Persist each partition
        if (result_id == 0 || result_id == 2)
          right_writer_persistent.write(right_record);

      } else {
        right_writer.write(right_record);
      }
      // count++;
    }
    left_reader.close();
    right_reader.close();
    left_writer.close();
    right_writer.close();

    // close and persist final files
    if (result_id == 0 || result_id == 1)
      left_writer_persistent.close();
    if (result_id == 0 || result_id == 2)
      right_writer_persistent.close();

    if (partition != 0) {
      //      std::cout << "Remove files for partition: " << partition << "\n";
      bufferManager->removeFile(generate_partition_input(leftfile, partition));
      bufferManager->removeFile(generate_partition_input(rightfile, partition));
    }
  }

  void use_generated_persistent_files_for_both_tables(writer_type& output, size_t partition) {
    // left_reader_type left_reader(leftfile);
    // right_reader_type right_reader(rightfile);
    left_reader_type left_reader(bufferManager,
                                 generate_partition_persistent(leftfile, left_join_tv.full_output_path, partition));

    right_reader_type right_reader(bufferManager,
                                   generate_partition_persistent(rightfile, right_join_tv.full_output_path, partition));
    left_record_type left_record;
    right_record_type right_record;

    std::multimap<key_type, left_record_type, Less> map;
    std::pair<typename std::multimap<key_type, left_record_type, Less>::iterator,
        typename std::multimap<key_type, left_record_type, Less>::iterator> range;

    // size_t count = 0;
    while (left_reader.hasNext()) {
      left_record = left_reader.nextRecord();
      map.insert(std::pair<key_type, left_record_type>(left_extractor(left_record), left_record));

      // count++;
    }
    // count = 0;
    while (right_reader.hasNext()) {
      right_record = right_reader.nextRecord();
      //			COUT << "right record " << right_record << "\n";
      range = map.equal_range(right_extractor(right_record));
      for (typename std::multimap<key_type, left_record_type>::iterator it = range.first; it != range.second; it++) {
        output.write(combiner(it->second, right_record));
      }
    }
    // COUT << "right count: " << count << "\n";
    left_reader.close();
    right_reader.close();
  }

  void use_generated_persistent_file_for_left_side(writer_type& output, size_t partition) {
    left_reader_type left_reader_persistent(
        bufferManager, generate_partition_persistent(leftfile, left_join_tv.full_output_path, partition));

    right_reader_type right_reader(bufferManager, generate_partition_input(rightfile, partition));
    left_record_type left_record;
    right_record_type right_record;
    right_writer_type right_writer(bufferManager);

    size_t prt;
    std::multimap<key_type, left_record_type, Less> map;
    std::pair<typename std::multimap<key_type, left_record_type, Less>::iterator,
        typename std::multimap<key_type, left_record_type, Less>::iterator> range;

    right_writer.open(generate_partition_output(rightfile, partition));

    // size_t count = 0;
    while (left_reader_persistent.hasNext()) {
      left_record = left_reader_persistent.nextRecord();
      map.insert(std::pair<key_type, left_record_type>(left_extractor(left_record), left_record));
    }
    while (right_reader.hasNext()) {
      right_record = right_reader.nextRecord();
      prt = hash_of_fast(right_extractor(right_record)) % number_of_partitions;
      if (prt == partition) {
        range = map.equal_range(right_extractor(right_record));
        for (typename std::multimap<key_type, left_record_type>::iterator it = range.first; it != range.second; it++) {
          output.write(combiner(it->second, right_record));
        }
      } else {
        right_writer.write(right_record);
      }
      // count++;
    }
    // COUT << "right count: " << count << "\n";
    left_reader_persistent.close();
    right_reader.close();
    right_writer.close();

    if (partition != 0) {
      bufferManager->removeFile(generate_partition_input(rightfile, partition));
    }
  }

  void use_generated_persistent_file_for_right_side(writer_type& output, size_t partition) {
    right_reader_type right_reader_persistent(
        bufferManager, generate_partition_persistent(rightfile, right_join_tv.full_output_path, partition));

    left_reader_type left_reader(bufferManager, generate_partition_input(leftfile, partition));
    left_record_type left_record;
    right_record_type right_record;
    //		right_writer_type right_writer(bufferManager);
    left_writer_type left_writer(bufferManager);

    size_t prt;
    std::multimap<key_type, left_record_type, Less> map;
    std::pair<typename std::multimap<key_type, left_record_type, Less>::iterator,
        typename std::multimap<key_type, left_record_type, Less>::iterator> range;

    left_writer.open(generate_partition_output(leftfile, partition));

    // size_t count = 0;
    while (left_reader.hasNext()) {
      left_record = left_reader.nextRecord();
      //			COUT<<"left record: "<<left_record<<"\n";
      prt = hash_of_fast(left_extractor(left_record)) % number_of_partitions;
      if (prt == partition) {
        map.insert(std::pair<key_type, left_record_type>(left_extractor(left_record), left_record));

      } else {
        left_writer.write(left_record);
      }
      // count++;
    }

    while (right_reader_persistent.hasNext()) {
      right_record = right_reader_persistent.nextRecord();

      range = map.equal_range(right_extractor(right_record));
      for (typename std::multimap<key_type, left_record_type>::iterator it = range.first; it != range.second; it++) {
        output.write(combiner(it->second, right_record));
      }
    }

    // COUT << "right count: " << count << "\n";
    left_reader.close();
    right_reader_persistent.close();
    left_writer.close();

    if (partition != 0) {
      bufferManager->removeFile(generate_partition_input(leftfile, partition));
    }
  }

  std::string generate_temp_filename(const std::string& p) const {
    std::stringstream s;
    s << p << ".tmp";
    return s.str();
  }

  void substitute_file(bool& first_sub, std::string& orig, const std::string& sub) const {
    COUT << "subbing " << orig << " with " << sub << "(" << first_sub << ")" << "\n";
    if (first_sub) {
      orig.clear();
      orig.append(sub);
      first_sub = false;
    } else {
      // fs::remove(orig);
      bufferManager->renameFile(sub, orig);
      // fs::rename(sub, orig);
    }
    /*
     require(::remove(orig.c_str()) == 0,
     "could not delete original file.");
     require(::rename(sub.c_str(), orig.c_str()) == 0,
     "could not rename files.");
     */
  }

  size_t hash_of(const key_type& value) {
    char* k = (char*) &value;
    size_t hash = 5381;
    for (size_t i = 0; i < sizeof(key_type); i++)
      hash = ((hash << 5) + hash) + k[i];
    return hash;
  }

  //The previous hash function sometimes does not distribute data to files correctly
  // Use this version better (tested that distribution is better)
  uint32_t hash_of_fast(const key_type& value) {

    const char * data = (char*) &value;
    int len = 8;
    uint32_t hash = len, tmp;
    int rem;

    if (len <= 0 || data == NULL)
      return 0;

    rem = len & 3;
    len >>= 2;

    /* Main loop */
    for (; len > 0; len--) {
      hash += get16bits(data);
      tmp = (get16bits (data+2) << 11) ^ hash;
      hash = (hash << 16) ^ tmp;
      data += 2 * sizeof(uint16_t);
      hash += hash >> 11;
    }

    /* Handle end cases */
    switch (rem) {
      case 3:
        hash += get16bits(data);
        hash ^= hash << 16;
        hash ^= ((signed char) data[sizeof(uint16_t)]) << 18;
        hash += hash >> 11;
        break;
      case 2:
        hash += get16bits(data);
        hash ^= hash << 11;
        hash += hash >> 17;
        break;
      case 1:
        hash += (signed char) *data;
        hash ^= hash << 10;
        hash += hash >> 1;
    }

    /* Force "avalanching" of final 127 bits */
    hash ^= hash << 3;
    hash += hash >> 5;
    hash ^= hash << 4;
    hash += hash >> 17;
    hash ^= hash << 25;
    hash += hash >> 6;

    return hash;
  }

 public:
  //  void usePersistedResult(deceve::storage::table_value tv, int r_id = 0) {
  //    usePersistedOutput_flag = true;
  //    result_id = r_id;
  //
  //    if (r_id == 1) {
  //      left_join_tv = tv;
  //    } else {
  //      right_join_tv = tv;
  //    }
  //  }
  //
  //  void usePersistedResult(deceve::storage::table_value ltv,
  //                          deceve::storage::table_value rtv, int r_id = 0)
  //                          {
  //    usePersistedOutput_flag = true;
  //    result_id = r_id;
  //    left_join_tv = ltv;
  //    right_join_tv = rtv;
  //  }
  //
  //  void persistResult(int r_id = 0) {
  //    persistOutput_flag = true;
  //    result_id = r_id;
  //  }

  void usePersistedResult(deceve::storage::table_value tv, int r_id = 0) {
    usePersistedOutput_flag = true;
    result_id = r_id;

    if (r_id == 1) {
      left_join_tv = tv;
    } else {
      right_join_tv = tv;
    }
  }

  void usePersistedResult(deceve::storage::table_value ltv, deceve::storage::table_value rtv, int r_id = 0) {
    usePersistedOutput_flag = true;
    result_id = r_id;
    left_join_tv = ltv;
    right_join_tv = rtv;
  }

  void persistResult(int r_id = 0) {
    persistOutput_flag = true;
    result_id = r_id;
  }

  void persistResult(deceve::storage::table_key tk, int r_id = 0) {
    persistOutput_flag = true;
    result_id = r_id;

    if (r_id == 1) {
      left_join_tk = tk;
    } else {
      right_join_tk = tk;
    }
  }

  void persistResult(deceve::storage::table_key ltk, deceve::storage::table_key rtk, int r_id = 0) {
    persistOutput_flag = true;
    result_id = r_id;
    left_join_tk = ltk;
    right_join_tk = rtk;
  }

  size_t getNumberOfPartitions() {
    return number_of_partitions;
  }

  int getResultId() {
    return result_id;
  }

  std::string getRunNamePrefixOfPersistentFile() {
    return runNamePrefix;
  }
};
}
}

#endif
