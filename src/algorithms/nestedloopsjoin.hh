#ifndef __NESTEDLOOPSJOIN_HH__
#define __NESTEDLOOPSJOIN_HH__

#include <sstream>
#include <sys/stat.h>
#include <algorithm>
#include <cstdio>
#include <vector>
#include <map>

#include "../utils/defs.hh"
#include "../storage/page.hh"
#include "../storage/io.hh"
#include "../storage/identity.hh"
#include "../storage/readwriters.hh"

namespace deceve {
namespace bama {

template<typename Left, typename Right, typename LeftExtract = Identity<Left>, typename RightExtract = Identity<Right>,
    typename Combine = PairCombiner<Left, Right>, typename Less = std::less<typename LeftExtract::key_type> >
class NestedLoopsJoin {
 public:
  typedef Left left_record_type;
  typedef Right right_record_type;
  typedef typename Combine::record_type output_record_type;
  typedef typename LeftExtract::key_type key_type;

 private:
  typedef Reader<left_record_type> left_reader_type;
  typedef Reader<right_record_type> right_reader_type;
  typedef Writer<output_record_type> writer_type;
  typedef BulkProcessor<left_record_type> bulk_processor_type;

 public:
  NestedLoopsJoin(deceve::storage::BufferManager *bm, const std::string& l, const std::string& r, const std::string& o,
                  const LeftExtract& le = LeftExtract(), const RightExtract& re = RightExtract(), const Combine& c =
                      Combine(),
                  size_t nb = 1024)
      : bufferManager(bm),
        leftfile(l),
        rightfile(r),
        outfile(o),
        left_extractor(le),
        right_extractor(re),
        combiner(c),
        less(Less()),
        number_of_buffers(nb) {
    runNamePrefix = bufferManager->getSM().generateUniqueFileName("_join");
  }

  ~NestedLoopsJoin() {
  }

  void join() {
    //left_reader_type left_reader;
    bulk_processor_type bulk_reader(bufferManager);
    right_reader_type right_reader(bufferManager);
    right_record_type right_record;
    std::multimap<key_type, left_record_type, Less> map;
    std::pair<typename std::multimap<key_type, left_record_type, Less>::iterator,
        typename std::multimap<key_type, left_record_type, Less>::iterator> range;
    writer_type output(bufferManager, outfile);

    size_t fileSizeInPagesLeft = bufferManager->getSM().getFileSizeInPagesFromCatalog(leftfile);
    size_t numberOfAvailablePrimaryPages = bufferManager->getBufferPool().numberOfPrimaryPages();

    if (fileSizeInPagesLeft < 10) {
      fileSizeInPagesLeft = 10;
    } else {
      number_of_buffers =
          (fileSizeInPagesLeft <= numberOfAvailablePrimaryPages / bufferManager->getSM().getNumberOfThreads()) ?
              fileSizeInPagesLeft : numberOfAvailablePrimaryPages / bufferManager->getSM().getNumberOfThreads();
    }

    if (number_of_buffers <= 0) {
      number_of_buffers = 1;
    }

    // std::cout << "number_of_buffers NESTED LOOPS JOIN: " << number_of_buffers << std::endl;
    // std::cout << "numberOfAvailablePrimaryPages = " << bufferManager->getBufferPool().numberOfPrimaryPages()
    //          << std::endl;
    // std::cout << "getPrimaryPinCount: " << bufferManager->getBufferPool().getPrimaryPinCount() << std::endl;

    //bulk_reader.skip(left_records_processed);
    bulk_reader.open(leftfile);
    left_record_type* records = (left_record_type*) aligned_new(number_of_buffers * deceve::bama::get_pagesize());
    //std::cout << "allocated" << "\n";

    require(number_of_buffers > 0, "number of buffers should be bigger than 0");

    size_t chunk = number_of_buffers;
    size_t offset = 0;
    size_t left_records_read = 0;

    //	size_t current_heap_length;

    //	left_record_type* records = bulk_reader.bulkRead(0, chunk,
    //			left_records_read);

    //size_t limit = current_heap_length;
    //	bulk_reader.close();

    //std::cout<<"Number of buffers"<<number_of_buffers<<std::endl;

    bulk_reader.bulkRead(offset, records, chunk, left_records_read);

  //  std::cout << "numberOfAvailablePrimaryPages = " << bufferManager->getBufferPool().numberOfPrimaryPages()
  //            << std::endl;
   // std::cout << "getPrimaryPinCount: " << bufferManager->getBufferPool().getPrimaryPinCount() << std::endl;

    //std::cout<<"records: "<<left_records_read<<"\n";

    while (left_records_read != 0) {
      //std::cout << "processing chunk " << chunk << "\n";
      // build the hash table
      for (size_t i = 0; i < left_records_read; i++) {
        //std::cout << "read " << records[i] << "\n";
        //std::cout << "partition is " << p << "\n";
        map.insert(std::pair<key_type, left_record_type>(left_extractor(records[i]), records[i]));
      }
      //std::cout << "built map" << "\n";
      // then perform the partition join

      right_reader.open(rightfile);
      while (right_reader.hasNext()) {
        right_record = right_reader.nextRecord();
        range = map.equal_range(right_extractor(right_record));
        for (typename std::multimap<key_type, left_record_type>::iterator it = range.first; it != range.second; it++) {
          //std::cout<<combiner(it->second, right_record)<<"\n";
          output.write(combiner(it->second, right_record));
        }
      }
      right_reader.close();

      map.clear();
      bulk_reader.bulkReleaseMike(offset, records, chunk, left_records_read);
      offset += chunk;

//            records = bulk_reader.bulkRead(offset, chunk,
//            				left_records_read);
      bulk_reader.bulkRead(offset, records, chunk, left_records_read);
    }
    aligned_delete((unsigned char*) records);
    bulk_reader.bulkReleaseMike(offset, records, chunk, left_records_read);
    bulk_reader.close();
    //right_reader.close();
  //  std::cout<<"Close output"<<std::endl;
    output.close();

 //   std::cout << "getPrimaryPinCount END: " << bufferManager->getBufferPool().getPrimaryPinCount() << std::endl;

 //   std::cout<<"MIKE---------"<<std::endl;
  }

 private:
  deceve::storage::BufferManager *bufferManager;
  std::string leftfile;
  std::string rightfile;
  std::string outfile;
  LeftExtract left_extractor;
  RightExtract right_extractor;
  Combine combiner;
  Less less;
  size_t number_of_buffers;
  deceve::storage::FileMode mode;
  std::string runNamePrefix;
};

}
}

#endif
