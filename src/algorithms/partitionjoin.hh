#ifndef __PARTITIONJOIN_HH__
#define __PARTITIONJOIN_HH__

#include "../storage/page.hh"
#include "../storage/io.hh"
#include "../storage/identity.hh"
#include <sstream>
#include <sys/stat.h>
#include <algorithm>
#include <cstdio>
#include <map>
#include "../utils/util.hh"

namespace deceve {
namespace bama {

template <typename Left, typename Right, typename LeftExtract = Identity<Left>,
          typename RightExtract = Identity<Right>,
          typename Combine = PairCombiner<Left, Right>,
          typename Less = std::less<typename LeftExtract::key_type> >
class PartitionJoin {
 public:
  typedef Left left_record_type;
  typedef Right right_record_type;
  typedef typename Combine::record_type output_record_type;
  typedef typename LeftExtract::key_type key_type;

 private:
  typedef Reader<left_record_type> left_reader_type;
  typedef Reader<right_record_type> right_reader_type;
  typedef Writer<output_record_type> writer_type;

 public:
  PartitionJoin(deceve::storage::BufferManager* bm, const std::string& l,
                const std::string& r, const std::string& o,
                const LeftExtract& le = LeftExtract(),
                const RightExtract& re = RightExtract(),
                const Combine& c = Combine(), size_t np = 20,
                bool persistFlag = false, bool useFlag = false, int result = -1)
      : bufferManager(bm),
        leftfile(l),
        rightfile(r),
        outfile(o),
        left_extractor(le),
        right_extractor(re),
        combiner(c),
        less(Less()),
        number_of_partitions(np),
        persistOutput_flag(persistFlag),
        usePersistedOutput_flag(useFlag),
        result_id(result){};

  ~PartitionJoin() {}

  void join() {
    left_reader_type left(bufferManager);
    right_reader_type right(bufferManager);

    writer_type output(bufferManager, outfile);

    //    std::cout<<"Merge Join phase happening: "<<outfile<<"\n";
    std::multimap<key_type, left_record_type, Less> map;

    left_record_type left_record;
    std::pair<
        typename std::multimap<key_type, left_record_type, Less>::iterator,
        typename std::multimap<key_type, left_record_type, Less>::iterator>
        range;
    for (size_t p = 0; p < number_of_partitions; p++) {
      //      bufferManager->getBufferPool().printDirtyPagesForAllDataStructures();
      // COUT<<"File left :"<<generate_partition_name(leftfile, p)<<"\n";
      left.open(generate_partition_name(leftfile, p));
      while (left.hasNext()) {
        left_record = left.nextRecord();
        map.insert(std::pair<key_type, left_record_type>(
            left_extractor(left_record), left_record));
      }
      left.close();

      //      std::cout<<"File right :"<<generate_partition_name(rightfile,
      //      p)<<"\n";
      right.open(generate_partition_name(rightfile, p));
      right_record_type right_record;
      while (right.hasNext()) {
        right_record = right.nextRecord();
        range = map.equal_range(right_extractor(right_record));
        for (typename std::multimap<key_type, left_record_type>::iterator it =
                 range.first;
             it != range.second; it++) {
          if (isStreamFlag) {
            // just stream it to the network for the user
            combiner(it->second, right_record);
          } else {
            // write it to be used from the next operator
            output.write(combiner(it->second, right_record));
            numberOfTuplesOutput++;
          }
        }
      }
      right.close();
      map.clear();

      //      bufferManager->getBufferPool().printDirtyPagesForAllDataStructures();
      if (bufferManager->getFileMode(generate_partition_name(leftfile, p)) ==
          deceve::storage::AUXILIARY) {
        if (result_id != 1 && result_id != 0) {
          bufferManager->removeFile(generate_partition_name(leftfile, p));
          //          bufferManager->getBufferPool().printDirtyPagesForAllDataStructures();
        }
      }
      if (bufferManager->getFileMode(generate_partition_name(rightfile, p)) ==
          deceve::storage::AUXILIARY) {
        if (result_id != 2 && result_id != 0) {
          bufferManager->removeFile(generate_partition_name(rightfile, p));
          //          bufferManager->getBufferPool().printDirtyPagesForAllDataStructures();
        }
      }

      //            fs::remove(generate_partition_name(leftfile, p));
      //            fs::remove(generate_partition_name(rightfile, p));
      /*
       require(::remove(generate_partition_name(leftfile,
       p).c_str()) == 0,
       "could not delete partition file.");
       require(::remove(generate_partition_name(rightfile,
       p).c_str()) == 0,
       "could not delete partition file.");
       */
    }

    output.close();
  }

  void setStreamFlag(bool streamFlag) { isStreamFlag = streamFlag; }

  double getNumberOfTuplesOutput() { return numberOfTuplesOutput; }

 private:
  deceve::storage::BufferManager* bufferManager;
  std::string leftfile;
  std::string rightfile;
  std::string outfile;
  LeftExtract left_extractor;
  RightExtract right_extractor;
  Combine combiner;
  Less less;
  size_t number_of_partitions;
  bool persistOutput_flag;
  bool usePersistedOutput_flag;
  int result_id;

  // used in case the result is returned to the user
  // and it is not necessary to write it in memory
  bool isStreamFlag;
  double numberOfTuplesOutput{0};

  std::string generate_partition_name(const std::string& p, size_t i) {
    std::stringstream s;
    s << p << "." << i;
    return s.str();
  }
};

}  //:~ namespace bama
}  //:~ namespace deceve

#endif
