/*
 * partitionMapReduce.hh
 *
 *  Created on: Mar 31, 2015
 *      Author: maba18
 */

#ifndef PARTITIONMAPREDUCE_HH_
#define PARTITIONMAPREDUCE_HH_


#include "../storage/page.hh"
#include "../storage/io.hh"
#include "../storage/identity.hh"
#include "replacementsort.hh"
#include <sstream>
#include <sys/stat.h>
#include <algorithm>
#include <cstdio>
#include <vector>
#include "../utils/util.hh"

namespace deceve {
namespace bama {

template<typename Record, typename RecordOutput, typename Transform, typename Extract = Identity<Record>, 
	typename ExtractOutput = Identity<RecordOutput> >
class PartitionMapReduce {
public:
	typedef Record record_type;
	typedef RecordOutput record_output_type;
	typedef typename Extract::key_type key_type;

private:
	typedef Reader<record_type> reader_type;
	typedef Writer<record_output_type> writer_output_type;

private:
	/*
	 class key_extractor {
	 private:
	 const Extractor& extractor;
	 public:
	 const K& operator()(const record_type& r) const {
	 return extractor(r);
	 }
	 };
	 */

public:
	PartitionMapReduce(deceve::storage::BufferManager *bm, const std::string& fn,
			const std::string& op, const Transform& t,  const Extract& e, const ExtractOutput& eo,
			 size_t np = 20,
			deceve::storage::FileMode m = deceve::storage::PRIMARY) :
			bufferManager(bm), filename(fn), outfile_prefix(op), transformer(t), 
			extractor(e), output_extractor(eo),
			 number_of_partitions(np), mode(m) {
	};

	~PartitionMapReduce() {
	}

	void partition() {


		reader_type reader(bufferManager, filename);

		COUT << "MAP-REDUCE PARTITION phase happening on: " << filename << "\n";

		COUT<<"Number of partitions is: "<<number_of_partitions<<"\n";

		std::vector<writer_output_type*> writers;
		for (unsigned int i = 0; i < number_of_partitions; i++) {
			std::stringstream s;
			s << outfile_prefix << ".partition." << i;
			COUT<<"Generate new writer: "<<s.str()<<"\n";
			writers.push_back(new writer_output_type(bufferManager, s.str(), deceve::storage::PRIMARY));
		}

		record_type rec;
		while (reader.hasNext()) {
			rec = reader.nextRecord();
			writers[hash_of(extractor(rec)) % number_of_partitions]->write(transformer(rec));
		}

		for (typename std::vector<writer_output_type*>::iterator it = writers.begin();
				it != writers.end(); it++) {
			(*it)->flush();
			delete *it;
		}


		for (unsigned int i = 0; i < number_of_partitions; i++) {
			std::stringstream s;
			std::stringstream s1;
			s << outfile_prefix << "." << i;
			s1 << outfile_prefix << ".partition." << i;
			std::cout<<"Sort File: "<<s.str()<<"\n";

			deceve::bama::ReplacementSort<RecordOutput, ExtractOutput> *sorter  = new deceve::bama::ReplacementSort<RecordOutput,
					ExtractOutput>(bufferManager, s1.str(), s.str(),
					output_extractor, 1000, mode);
			sorter->sort();

			delete sorter;

			Reader<RecordOutput> printReader(bufferManager, s.str());

			while(printReader.hasNext()) {
			    
			    COUT<<printReader.nextRecord()<<"\n";
			}

			printReader.close();

		}


		reader.close();
	}

private:
	deceve::storage::BufferManager *bufferManager;
	std::string filename;
	std::string outfile_prefix;
	const Transform transformer;
	const Extract extractor;
	const ExtractOutput output_extractor;

	//Hash hasher;
	size_t number_of_partitions;
	deceve::storage::FileMode mode;

	size_t hash_of(const key_type& value) {
		char* k = (char*) &value;
		size_t hash = 5381;
		for (size_t i = 0; i < sizeof(key_type); i++)
			hash = ((hash << 5) + hash) + k[i];
		return hash;
	}

};

}
}


#endif /* PARTITIONMAPREDUCE_HH_ */
