#ifndef __OPT_HYBRIDSORT_HH__
#define __OPT_HYBRIDSORT_HH__

#include "../storage/page.hh"
#include "../storage/io.hh"
#include "../storage/identity.hh"
#include "../algorithms/replacementsort.hh"
#include "../algorithms/selectionsort.hh"
#include "../storage/readwriters.hh"
#include <sstream>
#include <sys/stat.h>
#include <algorithm>
#include <cstdio>
#include <vector>
#include <cmath>

namespace deceve { namespace bama {

template <typename Record,
          typename Extract = Identity<Record>,
          typename Compare = std::less<typename Extract::key_type> >
class HybridSort {
public:
    typedef Record record_type;
    typedef typename Extract::key_type key_type;
    
private:
    typedef Reader<record_type> reader_type;
    typedef Writer<record_type> writer_type;
    typedef BulkProcessor<record_type> bulk_processor_type;
    
public:
    
    HybridSort(deceve::storage::BufferManager *bm, const std::string& fn,
               const std::string& of,
               const Extract& ex = Extract(),
               unsigned int nb = 1024,
               float p = 0.5)
        : bufferManager(bm),filename(fn), outfile(of),
          number_of_buffers(nb),
          percentage(p >= 0.0 && p <= 1.0 ? p : 0.5),
          max_records(0), records_written(0),
          extractor(ex), comparator(Compare()),
          rep_run_comparator(extractor, comparator),
          rep_heap_comparator(extractor, comparator),
          sel_value_comparator(extractor, comparator),
          sel_heap_comparator(extractor, comparator),
          sel_sort_comparator(extractor, comparator) {
        reader_type reader(bufferManager, filename);
        size_t num_pages = reader.numPages();
        reader.close();
        max_records = ((size_t) (percentage * num_pages))
            * Page<record_type>::allocation;
    }
    
    ~HybridSort() {}

    void sort() {
        if (! create_runs_bulk()) merge_runs();
        //dump_runs();
        //remove_runs();
    }

private:

    class rep_run_cmp {
    private:
        const Extract& cmp_extract;
        const Compare& cmp_predicate;
    public:
        rep_run_cmp(const Extract& e, const Compare& p)
            : cmp_extract(e), cmp_predicate(p) {}
        bool operator()(const record_type& x, const record_type& y) const {
            return !cmp_predicate(cmp_extract(x), cmp_extract(y));
        }
    };

    class rep_heap_cmp {
    private:
        const Extract& cmp_extract;
        const Compare& cmp_predicate;
    public:
        rep_heap_cmp(const Extract& e, const Compare& p)
            : cmp_extract(e), cmp_predicate(p) {}
        bool operator()(const std::pair<record_type, unsigned int>& x,
                        const std::pair<record_type, unsigned int>& y) const {
            return !cmp_predicate(cmp_extract(x.first), cmp_extract(y.first));
        }
    };

    class sel_value_cmp {
    private:
        const Extract& cmp_extract;
        const Compare& cmp_predicate;
    public:
        sel_value_cmp(const Extract& e, const Compare& p)
            : cmp_extract(e), cmp_predicate(p) {}
        bool operator()(const record_type& x, const record_type& y) const {
            return cmp_predicate(cmp_extract(x), cmp_extract(y));
        }
    };

    class sel_heap_cmp {
    private:
        const Extract& cmp_extract;
        const Compare& cmp_predicate;
    public:
        sel_heap_cmp(const Extract& e, const Compare& p)
            : cmp_extract(e), cmp_predicate(p) {}
        bool operator()(const std::pair<record_type, size_t>& x,
                        const std::pair<record_type, size_t>& y) const {
            bool less = cmp_predicate(cmp_extract(x.first),
                                       cmp_extract(y.first));
            bool equal = !less && !cmp_predicate(cmp_extract(y.first),
                                                 cmp_extract(x.first));
            return equal ? x.second < y.second : less;
        }
    };

    class sel_sort_cmp {
    private:
        const Extract& cmp_extract;
        const Compare& cmp_predicate;
    public:
        sel_sort_cmp(const Extract& e, const Compare& p)
            : cmp_extract(e), cmp_predicate(p) {}
        bool operator()(const std::pair<record_type, size_t>& x,
                        const std::pair<record_type, size_t>& y) const {
            bool less = cmp_predicate(cmp_extract(x.first),
                                       cmp_extract(y.first));
            bool equal = !less && !cmp_predicate(cmp_extract(y.first),
                                                 cmp_extract(x.first));
            return equal ? x.second < y.second : less;
        }
    };

    bool create_runs_bulk() {

    //	std::cout<<"Create run bulks Start"<<"\n";
        reader_type reader(bufferManager);
        writer_type writer(bufferManager);
        bool done = false;
        
        number_of_runs = 0;
        // load up the heap
        bulk_processor_type bulk_reader(bufferManager, filename);
        require(number_of_buffers>3, "Number of buffers should be bigger than 2");
        size_t pages = number_of_buffers-2;
        size_t current_heap_length;
        record_type* records = 
            bulk_reader.bulkRead(0, pages, current_heap_length);
        size_t limit = current_heap_length;
        bulk_reader.close();
        if (reader.isOpen()) { reader.close(); }
        reader.open(filename);
        reader.skip(current_heap_length);
        
        // start up a run
        writer.open(generate_run());
        size_t count = current_heap_length;
        bool more = reader.hasNext() && count < max_records;
        records_written = 0;
        while (more) {
            std::make_heap(records, records + current_heap_length,
                           rep_run_comparator);
                    
            while (current_heap_length > 0 && more) {
                record_type top = records[0];
                std::pop_heap(records,
                              records + current_heap_length,
                              rep_run_comparator);
                writer.write(top);
                record_type rec = reader.nextRecord();
                count++;
                records[current_heap_length-1] = rec;
                if (! rep_run_comparator(top, rec)) {
                    std::push_heap(records, records + current_heap_length,
                                   rep_run_comparator);
                }
                else {
                    current_heap_length--;
                }
                more = reader.hasNext() && count < max_records;
            }
            // only close the run if the run has ended -- if there are
            // no more records, we'll bail in the cleanup
            if (current_heap_length == 0) {
                writer.close();
                writer.open(generate_run());
                current_heap_length = limit;
            }
            more = reader.hasNext() && count < max_records;
        }
        
        if (number_of_runs > 1) {
        //    std::cout << "num runs " << number_of_runs << "\n";
            // at least one run has been closed
            std::sort(records, records + current_heap_length,
                      rep_run_comparator);
            for (unsigned int i = 0; i < current_heap_length; i++)
                writer.write(records[i]);
            writer.close();
            writer.open(generate_run());
            std::sort(records + current_heap_length, records + limit,
                      rep_run_comparator);
            for (unsigned int i = current_heap_length; i < limit; i++)
                writer.write(records[i]);
            writer.close();
        }
        else {
            // no run has been closed, everything fits in memory -- close
            // the run file, sort the in-memory data, open the outfile
            // and write them out
            /*
            writer.close();
            std::stringstream s;
            s << filename << ".0" << std::ends;
            require(::rename(s.str().c_str(), outfile.c_str()) == 0,
                    "could not rename files.");
            writer.open(outfile);
            */
            std::sort(records, records + current_heap_length,
                      rep_run_comparator);
            for (unsigned int i = 0; i < current_heap_length; i++) {
                writer.write(records[i]);
            }
            writer.close();
            //done = true;
        }

        create_selection_sort_run(count);
        
        //reader.close();
        processed_so_far = 0;
        aligned_delete((unsigned char*) records);
      //  std::cout<<"Create run bulks Finish"<<"\n";
        return done;
    }

    void create_selection_sort_run(size_t& ignored) {
    //    std::cout << "creating ss run" << "\n";
        writer_type writer(bufferManager);
        std::vector<std::pair<record_type, size_t> > heap;
        std::pair<record_type, size_t>* max_values = 0;
        size_t limit = 0;
        size_t length = 0;
        size_t processed = 0;
        
        writer.open(generate_run());
        /*
        if (number_of_ignored_records != 0) {
            require(writer.skip(number_of_ignored_records), "could not skip");
        }
        */
        bool more_passes = first_pass(ignored, writer, max_values, heap,
                                      &limit, &length, &processed);
        while (more_passes) {
            more_passes = make_pass(ignored, writer, max_values, heap,
                                    limit, length, &processed);
        }
        if (max_values) { delete max_values; max_values = 0; }
        writer.close();
    }

    bool first_pass(size_t& ignored,
                    writer_type& writer,
                    std::pair<record_type, size_t>*& max_values,
                    std::vector<std::pair<record_type, size_t> >& heap,
                    size_t* limit,
                    size_t* position,
                    size_t* processed) {
        *limit = ((number_of_buffers-2) * get_pagesize()) / sizeof(record_type);
        reader_type reader(bufferManager);
        record_type record;
        
        heap.clear();
        reader.open(filename);
        if (ignored != 0) {
            //std::cout << "skipping " << number_of_ignored_records << "\n";
            require(reader.skip(ignored), "could not skip");
        }
        *position = ignored;
        while (reader.hasNext()) {
            record = reader.nextRecord();
            if (heap.size() < *limit) {
                heap.push_back(std::make_pair(record, *position));
                std::push_heap(heap.begin(), heap.end(),
                               sel_heap_comparator);
            }
            else {
                std::pair<record_type, size_t> top = heap[0];
                if (sel_value_comparator(record, top.first)) {
                    std::pop_heap(heap.begin(), heap.end(),
                                  sel_heap_comparator);
                    heap[heap.size()-1] = std::make_pair(record, *position);
                    std::push_heap(heap.begin(), heap.end(),
                                   sel_heap_comparator);
                }
            }
            (*position)++;
        }
        // we have extracted the minimal set, now write it out
        //std::cout << "made pass, length = " << (*position) << "\n";
        if (! heap.empty()) { dump_heap(heap, writer, max_values, ignored); }
        (*processed) = ignored + heap.size();
        //(*processed) = ignored + heap_count;
        // close the input
        reader.close();
        return (*processed) < (*position);
    }

    bool make_pass(size_t& ignored,
                   writer_type& writer,
                   std::pair<record_type, size_t>*& max_values,
                   std::vector<std::pair<record_type, size_t> >& heap,
                   size_t limit,
                   size_t length,
                   size_t* processed) {
        reader_type reader(bufferManager);
        record_type record;
        size_t position;
        
        heap.clear();
        reader.open(filename);
        if (ignored != 0) {
            //std::cout << "skipping " << number_of_ignored_records << "\n";
            require(reader.skip(ignored), "could not skip");
        }
        position = ignored;
        while (reader.hasNext()) {
            record = reader.nextRecord();
            bool less = sel_value_comparator(max_values->first, record);
            bool equal = !less && !sel_value_comparator(record, max_values->first);
            if (less || (equal && max_values->second < position)) {
                if (heap.size() < limit) {
                    heap.push_back(std::make_pair(record, position));
                    std::push_heap(heap.begin(), heap.end(),
                                   sel_heap_comparator);
                }
                else {
                    std::pair<record_type, size_t> top = heap[0];
                    if (sel_value_comparator(record, top.first)) {
                        std::pop_heap(heap.begin(), heap.end(),
                                      sel_heap_comparator);
                        heap[heap.size()-1] = std::make_pair(record, position);
                        std::push_heap(heap.begin(), heap.end(),
                                       sel_heap_comparator);
                    }
                }
            }
            position++;
        }
        // we have extracted the minimal set, now write it out
        //std::cout << "made pass, length = " << (*position) << "\n";
        if (! heap.empty()) { dump_heap(heap, writer, max_values, ignored); }
        (*processed) += heap.size();
        // close the input
        reader.close();
        return (*processed) < length;
    }

    void dump_heap(std::vector<std::pair<record_type, size_t> >& heap,
                   writer_type& writer,
                   std::pair<record_type, size_t>*& max_values,
                   size_t& ignored) {
        std::sort(heap.begin(), heap.end(), sel_sort_comparator);
        for (typename std::vector<std::pair<record_type,
                 size_t> >::const_iterator it = heap.begin();
             it != heap.end(); it++) {            
            writer.write(it->first);
            // mini optimization -- advance ignored records if possible
            if (it->second == ignored) ignored++;
        }
        if (! max_values) {
            max_values = new std::pair<record_type,
                                       size_t>(heap[heap.size()-1].first,
                                               heap[heap.size()-1].second);
        }
        else {
            max_values->first = heap[heap.size()-1].first;
            max_values->second = heap[heap.size()-1].second;
        }
    }

    void merge_runs() {

    	//std::cout<<"Start merging files"<<"\n";

        // if there's only one run we're done
        if (number_of_runs == 1) return;

        writer_type output(bufferManager);
        records_written = 0;
        do {
            std::pair<unsigned int, unsigned int> pair = pick_runs();
            processed_so_far = pair.second;
            output.open(processed_so_far != number_of_runs
                        ? generate_run() : outfile);
            merge_runs(pair.first, pair.second, output,
                       (processed_so_far == number_of_runs));
            output.close();
        } while (processed_so_far < number_of_runs);

     //   std::cout<<"Finish merging files"<<"\n";
    }    
    

    void merge_runs(unsigned int f, unsigned int l,
                    writer_type& output,
                    bool final_merge) {
        std::vector<std::pair<record_type, unsigned int> > heap;
        std::vector<reader_type*> readers;
        std::vector<unsigned int> exhausted;

        for (unsigned int i = f; i < l; i++) {
            std::stringstream s;
            s << filename << "." << i;
            readers.push_back(new reader_type(bufferManager, s.str()));
        }

        for (unsigned int i = 0; i < l-f; i++)
            heap.push_back(std::make_pair(readers[i]->nextRecord(), i));

        std::make_heap(heap.begin(), heap.end(), rep_heap_comparator);
        bool done = false;
        while (! done) {
            std::pair<record_type, unsigned int> top = heap.front();
            output.write(top.first);
            if (final_merge) { records_written++; }
            //count++;
            std::pop_heap(heap.begin(), heap.end(), rep_heap_comparator);
            heap.pop_back();
            if (readers[top.second]->hasNext()) {
                heap.push_back(std::make_pair(
                        readers[top.second]->nextRecord(), top.second));
                std::push_heap(heap.begin(), heap.end(),
                               rep_heap_comparator);
            }
            else {
                exhausted.push_back(top.second);
            }
            done = exhausted.size() == readers.size();
        }
        
        for (typename std::vector<reader_type*>::iterator it = readers.begin();
             it != readers.end(); it++) {
            (*it)->close();
            delete *it;
        }

        remove_runs(f, l);
        output.flush();
    }

    std::pair<unsigned int, unsigned int> pick_runs() const {
        return std::make_pair(processed_so_far,
                              (processed_so_far + number_of_buffers-1
                               > number_of_runs
                               ? number_of_runs
                               : processed_so_far + number_of_buffers-1));
    }
    
    void remove_runs() const {
        if (number_of_runs == 1) return;
        
        for (unsigned int i = 0; i < number_of_runs; i++) {
            std::stringstream s;
            s << filename << "." << i;

            bufferManager->removeFile(s.str());
      //      fs::remove(s.str());
            //require(::remove(s.str().c_str()) == 0,
            //        "could not delete run file.");
        }
    }

    void remove_runs(size_t f, size_t l) const {
        for (unsigned int i = f; i < l; i++) {
            std::stringstream s;
            s << filename << "." << i;
            bufferManager->removeFile(s.str());
//            fs::remove(s.str());
            //require(::remove(s.str().c_str()) == 0,
            //        "could not delete run file.");
        }
    }

    /*
    void dump_runs() {
        reader_type reader;
        for (unsigned int i = 0; i < number_of_runs; i++) {
            std::stringstream s;
            s << filename << "." << i << std::ends;
            reader.open(s.str());
            unsigned j = 0;
            while (reader.hasNext())
                std::cout << "run " << i << ", " << j++ << ": "
                          << reader.nextRecord() << "\n";
            reader.close();
        }
    }

    void dump() {
        //reader_type reader(filename);
        reader_type reader(outfile);
        while (reader.hasNext())
            std::cout << "out " << reader.nextRecord() << "\n";
    }
    */

private:
    deceve::storage::BufferManager *bufferManager;
    std::string filename;
    std::string outfile;
    const size_t number_of_buffers;
    float percentage;
    unsigned int number_of_runs;
    unsigned int processed_so_far;
    size_t max_records;
    size_t records_written;
    Extract extractor;
    Compare comparator;
    rep_run_cmp rep_run_comparator;
    rep_heap_cmp rep_heap_comparator;
    sel_value_cmp sel_value_comparator;
    sel_heap_cmp sel_heap_comparator;
    sel_sort_cmp sel_sort_comparator;

    std::string generate_run() {
        std::stringstream s;
        s << filename << "." << number_of_runs;
        number_of_runs++;
        return s.str();
    }
};


}}

#endif

