#ifndef __SEGMENTSORT_HH__
#define __SEGMENTSORT_HH__

#include "../storage/page.hh"
#include "../storage/io.hh"
#include "../storage/identity.hh"
#include <sstream>
#include <sys/stat.h>
#include <algorithm>
#include <cstdio>
#include <vector>

namespace deceve { namespace bama {

template <typename Record,
          typename Extract = Identity<Record>,
          typename Compare = std::less<typename Extract::key_type> >
class SegmentSort {
public:
    typedef Record record_type;
    typedef typename Extract::key_type key_type;
    
private:
    typedef Reader<record_type> reader_type;
    typedef Writer<record_type> writer_type;
    typedef BulkProcessor<record_type> bulk_processor_type;

public:
    SegmentSort(deceve::storage::BufferManager *bm, const std::string& fn,
                const std::string& of,
                const Extract& ex,
                size_t nb = 1024,
                float p = 1.0)
        : bufferManager(bm), filename(fn), outfile(of),
          //replacement_sort_outfile(fn),
          //selection_sort_outfile(fn),
          number_of_buffers(nb),
          //max_records(0), records_written(0),
          extractor(ex), comparator(Compare()),
          value_comparator(extractor, comparator),
          rep_value_comparator(extractor, comparator),
          minheap_comparator(extractor, comparator),
          heap_comparator(extractor, comparator),
          //sel_value_comparator(extractor, comparator),
          //sel_heap_comparator(extractor, comparator),
          //sel_sort_comparator(extractor, comparator),        
    //ratio(r),
          percentage(p) {        
    }

    ~SegmentSort() {}

    void sort() {
    //	std::cout<<"Create Run Bulk Start"<<"\n";
        create_runs_bulk();
    //	std::cout<<"Create Run Bulk Finish"<<"\n";
        //dump_runs();
   //  	std::cout<<"Merge runs Start"<<"\n";
        merge_runs();
  //   	std::cout<<"Merge runs Finish"<<"\n";
        //remove_runs();
    }

private:

    class value_cmp {
    private:
        const Extract& cmp_extract;
        const Compare& cmp_predicate;
    public:
        value_cmp(const Extract& e, const Compare& p)
            : cmp_extract(e), cmp_predicate(p) {}
        bool operator()(const record_type& x, const record_type& y) const {
            return cmp_predicate(cmp_extract(x), cmp_extract(y));
        }
    };
    
    class minheap_cmp {
    private:
        const Extract& cmp_extract;
        const Compare& cmp_predicate;
    public:
        minheap_cmp(const Extract& e, const Compare& p)
            : cmp_extract(e), cmp_predicate(p) {}
        bool operator()(const record_type& x,
                        const record_type& y) const {
            bool less = cmp_predicate(cmp_extract(x),
                                      cmp_extract(y));
            bool equal = !less && !cmp_predicate(cmp_extract(y),
                                                 cmp_extract(x));
            return equal ? false : less;
        }
    };

    class rep_value_cmp {
    private:
        const Extract& cmp_extract;
        const Compare& cmp_predicate;
    public:
        rep_value_cmp(const Extract& e, const Compare& p)
            : cmp_extract(e), cmp_predicate(p) {}
        bool operator()(const record_type& x, const record_type& y) const {
            return !cmp_predicate(cmp_extract(x), cmp_extract(y));
        }
    };

    class heap_cmp {
    private:
        const Extract& cmp_extract;
        const Compare& cmp_predicate;
    public:
        heap_cmp(const Extract& e, const Compare& p)
            : cmp_extract(e), cmp_predicate(p) {}
        bool operator()(const std::pair<record_type, unsigned int>& x,
                        const std::pair<record_type, unsigned int>& y) const {
            return !cmp_predicate(cmp_extract(x.first), cmp_extract(y.first));
        }
    };
    
    void create_runs_bulk() {
        reader_type reader(bufferManager);
        writer_type output(bufferManager);
        writer_type writer(bufferManager);
        std::vector<record_type> run;
        size_t minheap_length;

        size_t pages = number_of_buffers * percentage;//number_of_buffers / 2;
        record_type* records = allocate_bulk_heap(pages, minheap_length);
        number_of_runs = 0;
        // load up the heap
        reader.open(filename);
        reader.skip(minheap_length);
        size_t max_run_length = number_of_buffers * (1-percentage) //(number_of_buffers / 2)
            * Page<record_type>::allocation;
        size_t run_length = 0;
        writer.open(generate_run());
        std::make_heap(records, records + minheap_length, minheap_comparator);
        while (reader.hasNext()) {
            record_type record = reader.nextRecord();
            record_type top = records[0];
            if (value_comparator(record, top)) {
                std::pop_heap(records, records + minheap_length,
                              minheap_comparator);
                records[minheap_length-1] = record;
                std::push_heap(records, records + minheap_length,
                               minheap_comparator);
                //run.push_back(top);
                select_and_replace(run, run_length, max_run_length,
                                   top, writer);
            }
            else {
                //run.push_back(record);
                select_and_replace(run, run_length, max_run_length,
                                   record, writer);
            }
            /*
            if (run.size() == max_run_size) {
                //dump_heap(heap);
                std::sort(run.begin(), run.end(), value_comparator);
                writer.open(generate_run());
                for (typename std::vector<record_type>::const_iterator it = run.begin();
                     it != run.end(); it++) {
                    writer.write(*it);
                }
                writer.close();
                run.clear();
            }
            */
        }
        /*
        if (! run.empty()) {
            std::sort(run.begin(), run.end(), value_comparator);
            writer.open(generate_run());
            for (typename std::vector<record_type>::const_iterator it = run.begin();
                 it != run.end(); it++) {
                writer.write(*it);
            }
            writer.close();
        }
        */

        if (run_length != 0) {
            //std::cout << "num runs " << number_of_runs << "\n";
            // at least one run has been closed
            std::sort(run.begin(), run.begin() + run_length, value_comparator);
            for (unsigned int i = 0; i < run_length; i++)
                writer.write(run[i]);   
            writer.close();
            if (number_of_runs > 1 && run_length < max_run_length) {
                int count = 0;
                writer.open(generate_run());
                std::sort(run.begin() + run_length,
                          run.begin() + max_run_length, value_comparator);
                for (unsigned int i = run_length; i < max_run_length; i++, count++)
                    writer.write(run[i]);
                //std::cout << "last run " << count << "\n";
                writer.close();
            }
        }
        
   //     std::cout<<"Trigger close from segment sort"<<"\n";
        reader.close();
        processed_so_far = 0;
        //std::cout << "dumping run zero" << "\n";
        output.open(outfile);
        std::sort(records, records + minheap_length, value_comparator);
        for (size_t i = 0; i < minheap_length; i++) {
            output.write(records[i]);
            //std::cout << "wrote " << records[i] << "\n";
        }
        aligned_delete((unsigned char*) records);
        output.close();
   //     std::cout << "number of runs " << number_of_runs << "\n";
    }

    void select_and_replace(std::vector<record_type>& run,
                            size_t& run_length,
                            const size_t limit,
                            const record_type& record,
                            writer_type& writer) {
        if (run.size() < limit) {
            run.push_back(record);
            std::push_heap(run.begin(), run.end(), rep_value_comparator);
            run_length++;
            return;
        }

        record_type top = run[0];
        std::pop_heap(run.begin(), run.begin() + run_length,
                      rep_value_comparator);
        //std::cout << "dumping " << top << "\n";
        writer.write(top);
        run[run_length-1] = record;
        if (value_comparator(top, record)) {
            //run[run_length-1] = record;
            std::push_heap(run.begin(), run.begin() + run_length,
                           rep_value_comparator);
        }
        else {
            run_length--;
        }
        if (run_length == 0) {
            writer.close();
            writer.open(generate_run());
            std::make_heap(run.begin(), run.begin() + limit,
                           rep_value_comparator);
            run_length = limit;
        }
    }
    
    record_type* allocate_bulk_heap(size_t &pages, size_t& length) {
        bulk_processor_type bulk_reader(bufferManager,filename);
        record_type* records = 
            bulk_reader.bulkRead(0, pages, length);
    //    std::cout<<"Bulk reader close"<<"\n";
        bulk_reader.close();
        return records;
    }

    void merge_runs() {
        // if there's only one run we're done
        if (number_of_runs == 1) return;

        writer_type output(bufferManager);
        do {
            std::pair<unsigned int, unsigned int> pair = pick_runs();
            processed_so_far = pair.second;
            output.open(processed_so_far != number_of_runs
                        ? generate_run() : outfile);
            merge_runs(pair.first, pair.second, output);
            output.close();
        } while (processed_so_far < number_of_runs);
    }    
    

    void merge_runs(unsigned int f, unsigned int l,
                    writer_type& output) {
        std::vector<std::pair<record_type, unsigned int> > heap;
        std::vector<reader_type*> readers;
        std::vector<unsigned int> exhausted;

        for (unsigned int i = f; i < l; i++) {
            std::stringstream s;
            s << filename << "." << i ;
            readers.push_back(new reader_type(bufferManager, s.str()));
        }

        for (unsigned int i = 0; i < l-f; i++)
            heap.push_back(std::make_pair(readers[i]->nextRecord(), i));

        std::make_heap(heap.begin(), heap.end(), heap_comparator);
        bool done = false;
        while (! done) {
            std::pair<record_type, unsigned int> top = heap.front();
            output.write(top.first);
            //if (final_merge) { records_written++; }
            //count++;
            std::pop_heap(heap.begin(), heap.end(), heap_comparator);
            heap.pop_back();
            if (readers[top.second]->hasNext()) {
                heap.push_back(std::make_pair(
                        readers[top.second]->nextRecord(), top.second));
                std::push_heap(heap.begin(), heap.end(),
                               heap_comparator);
            }
            else {
                exhausted.push_back(top.second);
            }
            done = exhausted.size() == readers.size();
        }
        
        for (typename std::vector<reader_type*>::iterator it = readers.begin();
             it != readers.end(); it++) {
            (*it)->close();
            delete *it;
        }

   //     std::cout<<"Remove runs: filename"<<f<<" "<<"\n";

        remove_runs(f, l);
        output.flush();
    }

    std::pair<unsigned int, unsigned int> pick_runs() const {
        return std::make_pair(processed_so_far,
                              (processed_so_far + number_of_buffers-1
                               > number_of_runs
                               ? number_of_runs
                               : processed_so_far + number_of_buffers-1));
    }

    void remove_runs() const {
        if (number_of_runs == 1) return;
        //std::cout << "number of runs " << number_of_runs << "\n";
        for (unsigned int i = 0; i < number_of_runs; i++) {
            std::stringstream s;
            s << filename << "." << i;
            bufferManager->removeFile(s.str());
          //  fs::remove(s.str());
            //require(::remove(s.str().c_str()) == 0,
            //        "could not delete run file.");
        }
    }


    void remove_runs(size_t f, size_t t) const {
        for (size_t i = f; i < t; i++) {
            std::stringstream s;
            s << filename << "." << i ;
            bufferManager->removeFile(s.str());
    
        //  fs::remove(s.str());
            //require(::remove(s.str().c_str()) == 0,
            //        "could not delete run file.");
        }
    }


    
    void dump_runs() {
        reader_type reader(bufferManager);
        for (size_t i = 0; i < number_of_runs; i++) {
            std::stringstream s;
            s << filename << "." << i ;
            reader.open(s.str());
            size_t j = 0; 
            while (reader.hasNext())
                std::cout << "run " << i << ", " << j++ << ": "
                          << reader.nextRecord() << "\n";
            reader.close();
        }
    }
    
private:
    deceve::storage::BufferManager *bufferManager;
    std::string filename;
    std::string outfile;
    size_t number_of_runs;
    size_t processed_so_far;
    const size_t number_of_buffers;
    //size_t records_written;
    Extract extractor;
    Compare comparator;
    value_cmp value_comparator;
    rep_value_cmp rep_value_comparator;
    minheap_cmp minheap_comparator;
    heap_cmp heap_comparator;
    //sel_value_cmp sel_value_comparator;
    //sel_heap_cmp sel_heap_comparator;
    //sel_sort_cmp sel_sort_comparator;
    float percentage;
    
    std::string generate_run() {
        std::stringstream s;
        s << filename << "." << number_of_runs;
        number_of_runs++;
        return s.str();
    }
};

}}

#endif
