/*
 * mergesortjoin.hh
 *
 *  Created on: 15 Mar 2015
 *      Author: michail
 */

#ifndef MERGESORTJOINCONVERTRIGHT_HH_
#define MERGESORTJOINCONVERTRIGHT_HH_

#include "../algorithms/replacementsortConvert.hh"
#include "../algorithms/replacementsort.hh"
#include "../algorithms/partitionsortjoin.hh"
#include "../storage/identity.hh"
#include "../utils/util.hh"
//#include <iostream>
//#include <cstdlib>
//#include <ctime>
//#include <sys/time.h>

namespace deceve {
namespace bama {

template <typename Left, typename Right, typename ProjectedRight,
          typename LeftExtract = Identity<Left>,
          typename RightExtract = Identity<Right>,
          typename ProjectedRightExtract = Identity<ProjectedRight>,
          typename RightConvert = Converter<Right>,
          typename Combine = PairCombiner<Left, ProjectedRight>,
          typename Less = std::less<typename LeftExtract::key_type> >
class MergeSortJoinConvertRight {
 public:
  typedef Left left_record_type;
  typedef Right right_record_type;
  typedef ProjectedRight projected_right_record_type;
  typedef typename Combine::record_type output_record_type;
  typedef typename LeftExtract::key_type key_type;

 private:
  typedef Reader<left_record_type> left_reader_type;
  typedef Reader<right_record_type> right_reader_type;
  typedef Reader<projected_right_record_type> projected_right_reader_type;
  typedef Writer<output_record_type> writer_type;

 public:
  MergeSortJoinConvertRight(
      deceve::storage::BufferManager* bm, const std::string& l,
      const std::string& r, const std::string& o,
      const LeftExtract& le = LeftExtract(),
      const RightExtract& re = RightExtract(),
      const ProjectedRightExtract& pre = ProjectedRightExtract(),
      const RightConvert& rco = RightConvert(), const Combine& c = Combine(),
      size_t np = 20,
      deceve::storage::FileMode fmode = deceve::storage::PRIMARY,
      deceve::storage::SorterMode sIndex = deceve::storage::NO_FILE)
      : bufferManager(bm),
        leftfile(l),
        rightfile(r),
        outfile(o),
        left_extractor(le),
        right_extractor(re),
        projected_right_extractor(pre),
        right_converter(rco),
        combiner(c),
        less(Less()),
        number_of_partitions(np),
        persistOutput_flag(false),
        usePersistedOutput_flag(false),
        result_id(-1),
        mode(fmode),
        sortIndex(sIndex),
        left_join_tv(),
        right_join_tv(),
        left_join_tk(),
        right_join_tk() {
    runNamePrefix = bufferManager->getSM().generateUniqueFileName("_join");
  };

  //	deceve::storage::table_value left_join_tv;
  //	deceve::storage::table_value right_join_tv;
  //	deceve::storage::table_key left_join_tk;
  //	deceve::storage::table_key right_join_tk;

  ~MergeSortJoinConvertRight() {}

  void join() {
//    std::cout << "usePersistedOutput_flag: " << usePersistedOutput_flag
//              << std::endl;
//    std::cout << "persistOutput_flag: " << persistOutput_flag << std::endl;
//    std::cout << "result_id: " << result_id << std::endl;
//
//    std::cout << left_join_tk << std::endl;
//    std::cout << right_join_tk << std::endl;

//    if (left_join_tk.version == deceve::storage::SORT && result_id != 0 &&
//        result_id != 1) {
//      std::cout << "monitor left" << std::endl;
//      bufferManager->getStatsManager().insertTkInStatsMap(left_join_tk);
//    }
//    if (right_join_tk.version == deceve::storage::SORT && result_id != 0 &&
//        result_id != 2) {
//      std::cout << "monitor right" << std::endl;
//      bufferManager->getStatsManager().insertTkInStatsMap(right_join_tk);
//    }
    // COUT<<"Result id: " << result_id << "\n";
    // COUT << "Persist Flag: " << persistOutput_flag << "\n";
    // COUT << "Use persisted results Flag: " << usePersistedOutput_flag
    //<< "\n";
    // COUT << "sortIndex: " << sortIndex << "\n";

    //--------------Calculate the number of partitions ----------------------
    //    size_t fileSizeInPages = bufferManager->getStorageManager()
    //        .getFileSizeInPagesFromCatalog(leftfile);
    //    number_of_partitions = (size_t) fileSizeInPages
    //        / (bufferManager->getBufferPool().numberOfPrimaryPages());
    //
    //    if (number_of_partitions == 0) {
    //      number_of_partitions = 1;
    //    }

    size_t parts = bufferManager->getSM().calculateNumberOfPartitions(
        leftfile, rightfile,
        bufferManager->getBufferPool().numberOfPrimaryPages());

    //    std::cout << "HASHJOIN AMOUNT OF PARTITIONS FROM STORAGE MANAGER: " <<
    //    parts<<std::endl;
    number_of_partitions = parts;
    //    std::cout << "HASHJOIN Final Number of Partitions OF PARTITIONS FROM
    //    STORAGE MANAGER: " << parts;

    // COUT << "Number of partitions: " << number_of_partitions
    //<< "\n";

    //---------------- Change filenames -----------------------

    std::string leftInputPrefix = "";
    std::string leftOutputPrefix = runNamePrefix;
    std::string rightInputPrefix = "";
    std::string rightOutputPrefix = runNamePrefix;

    deceve::storage::FileMode leftMode = deceve::storage::PRIMARY;
    deceve::storage::FileMode rightMode = deceve::storage::PRIMARY;

    if (usePersistedOutput_flag) {
      if (result_id == 0) {
        leftInputPrefix = left_join_tv.full_output_path + "_persistent";
        rightInputPrefix = right_join_tv.full_output_path + "_persistent";
        number_of_partitions = left_join_tv.num_files;
      } else if (result_id == 1) {
        leftInputPrefix = left_join_tv.full_output_path + "_persistent";
        number_of_partitions = left_join_tv.num_files;
      } else if (result_id == 2) {
        rightInputPrefix = right_join_tv.full_output_path + "_persistent";
        number_of_partitions = right_join_tv.num_files;
      }
    }

    if (persistOutput_flag) {
      if (result_id == 0) {
        leftOutputPrefix = runNamePrefix + "_persistent";
        leftMode = deceve::storage::INTERMEDIATE;
        rightOutputPrefix = runNamePrefix + "_persistent";
        rightMode = deceve::storage::INTERMEDIATE;
      }
      if (result_id == 1) {
        leftOutputPrefix = runNamePrefix + "_persistent";
        leftMode = deceve::storage::INTERMEDIATE;
      } else if (result_id == 2) {
        rightOutputPrefix = runNamePrefix + "_persistent";
        rightMode = deceve::storage::INTERMEDIATE;
      }
    }

    ReplacementSort<left_record_type, LeftExtract> lsorter(
        bufferManager, leftfile + leftInputPrefix, leftfile + leftOutputPrefix,
        left_extractor, number_of_partitions, leftMode);

    ReplacementSortConvert<right_record_type, projected_right_record_type,
                           RightExtract, ProjectedRightExtract, RightConvert>
        rsorter(bufferManager, rightfile + rightInputPrefix,
                rightfile + rightOutputPrefix, right_extractor,
                projected_right_extractor, right_converter,
                number_of_partitions, rightMode);

    lsorter.setKey(left_join_tk);
    rsorter.setKey(right_join_tk);

//    if (left_join_tk.version == deceve::storage::SORT && result_id != 0 &&
//        result_id != 1) {
//      bufferManager->getStatsManager().addToNameMonitorConverterSort(
//          leftfile, leftOutputPrefix, left_join_tk);
//    }
//
//    if (right_join_tk.version == deceve::storage::SORT && result_id != 0 &&
//        result_id != 2) {
//      bufferManager->getStatsManager().addToNameMonitorConverterSort(
//          rightfile, rightOutputPrefix, right_join_tk);
//    }

    if (usePersistedOutput_flag) {
      if (result_id == 1) {
        // COUT << "Use Persisted Left file and sort only right file"
        //<< "\n";
        leftOutputPrefix = left_join_tv.full_output_path + "_persistent";
        if (sortIndex == deceve::storage::RIGHT_FILE_ONLY ||
            sortIndex == deceve::storage::BOTH_FILES) {
          if (persistOutput_flag)
            addToNameConverter(rightfile, rightOutputPrefix, right_join_tk);
          rsorter.sort();
        } else {
          rightOutputPrefix = "";
        }
      } else if (result_id == 2) {
        // COUT << "Use Persisted Right file and sort only left file"
        //<< "\n";
        rightOutputPrefix = right_join_tv.full_output_path + "_persistent";
        if (sortIndex == deceve::storage::LEFT_FILE_ONLY ||
            sortIndex == deceve::storage::BOTH_FILES) {
          if (persistOutput_flag)
            addToNameConverter(leftfile, leftOutputPrefix, left_join_tk);
          lsorter.sort();
        } else {
          leftOutputPrefix = "";
        }

      } else {
        leftOutputPrefix = left_join_tv.full_output_path + "_persistent";
        rightOutputPrefix = right_join_tv.full_output_path + "_persistent";
      }
    } else {
      //	COUT << "Sort both files" << "\n";
      //	COUT << "sortIndex:" << sortIndex << "\n";
      //			lsorter.sort();
      //			rsorter.sort();

      if (sortIndex == deceve::storage::LEFT_FILE_ONLY ||
          sortIndex == deceve::storage::BOTH_FILES) {
        // COUT << "lsorter.sort()" << "\n";
        addToNameConverter(leftfile, leftOutputPrefix, left_join_tk);
        lsorter.sort();
      } else {
        leftOutputPrefix = "";
      }
      if (sortIndex == deceve::storage::RIGHT_FILE_ONLY ||
          sortIndex == deceve::storage::BOTH_FILES) {
        //	COUT << "rsorter.sort()" << "\n";
        addToNameConverter(rightfile, rightOutputPrefix, right_join_tk);
        rsorter.sort();
      } else {
        rightOutputPrefix = "";
      }
    }

    //-------------- Merge Files ---------------------------

    // COUT << "Merge file: " << leftfile + leftOutputPrefix << "\n";
    // COUT << "Merge file: " << rightfile + rightOutputPrefix
    //<< "\n";

    PartitionSortJoin<left_record_type, projected_right_record_type,
                      LeftExtract, ProjectedRightExtract, Combine, Less>
        joiner(bufferManager, leftfile + leftOutputPrefix,
               rightfile + rightOutputPrefix, outfile, left_extractor,
               projected_right_extractor, combiner, number_of_partitions,
               usePersistedOutput_flag, persistOutput_flag, result_id,
               left_join_tk, right_join_tk);

    joiner.join();

    leftNumOfTuples = lsorter.getNumberOfTuples();
    rightNumOfTuples = rsorter.getNumberOfTuples();
    numberOfOutputTuples = joiner.getNumberOfOutputTuples();
    //		COUT << "output FILESIZE: " << outfile << ": "
    //				<<
    // bufferManager->getFileSizeFromCatalog(outfile)
    //<<
    //"\n";

//    std::cout << "mergesortjoin: leftNumOfTuples: " << leftNumOfTuples
//              << std::endl;
//    std::cout << "mergesortjoin: rightNumOfTuples: " << rightNumOfTuples
//              << std::endl;
//    std::cout << "mergesortjoin: numberOfOutputTuples: " << numberOfOutputTuples
//              << std::endl;
//
//    std::cout << "mergesortjoin: getOutputPercentage: " << getOutputPercentage()
//              << std::endl;

//    if (left_join_tk.version == deceve::storage::SORT && result_id != 0 &&
//        result_id != 1) {
//      bufferManager->getStatsManager().deleteFromNameMonitorConverterJoin(
//          leftfile, leftOutputPrefix, number_of_partitions);
//    }
//
//    if (right_join_tk.version == deceve::storage::SORT && result_id != 0 &&
//        result_id != 2) {
//      bufferManager->getStatsManager().deleteFromNameMonitorConverterJoin(
//          rightfile, rightOutputPrefix, number_of_partitions);
//    }
  }

 public:
  void usePersistedResult(deceve::storage::table_value tv, int r_id = 0) {
    usePersistedOutput_flag = true;
    result_id = r_id;

    if (r_id == 1) {
      left_join_tv = tv;
    } else {
      right_join_tv = tv;
    }
  }

  void usePersistedResult(deceve::storage::table_value ltv,
                          deceve::storage::table_value rtv, int r_id = 0) {
    usePersistedOutput_flag = true;
    result_id = r_id;
    left_join_tv = ltv;
    right_join_tv = rtv;
  }

  void persistResult(int r_id = 0) {
    persistOutput_flag = true;
    result_id = r_id;
  }

  void persistResult(deceve::storage::table_key tk, int r_id = 0) {
    persistOutput_flag = true;
    result_id = r_id;

    if (r_id == 1) {
      left_join_tk = tk;
    } else {
      right_join_tk = tk;
    }
  }

  void persistResult(deceve::storage::table_key ltk,
                     deceve::storage::table_key rtk, int r_id = 0) {
    persistOutput_flag = true;
    result_id = r_id;
    left_join_tk = ltk;
    right_join_tk = rtk;
  }

  void setJoinTableKeys(const deceve::storage::table_key& leftKey,
                        const deceve::storage::table_key& rightKey) {
    left_join_tk = leftKey;
    right_join_tk = rightKey;
  }

  size_t getNumberOfPartitions() { return 1; }

  int getResultId() { return result_id; }

  std::string getRunNamePrefixOfPersistentFile() { return runNamePrefix; }

  void addToNameConverter(std::string filename, std::string prefix,
                          deceve::storage::table_key join_tk) {
    // COUT << "addToNameConverter " << "\n";

    std::stringstream s;
    s << filename << prefix;
    // COUT << "Add filename: " << s.str() << " for tk: " << join_tk
    //<< "\n";
    bufferManager->getSM().insertInNameConverter(s.str(), join_tk);
  }

 public:
  void setStreamFlag(bool streamFlag) { isStreamFlag = streamFlag; }
  double getPredicatePercentageLeft() { return predicatePercentageLeft; }
  double getPredicatePercentageRight() { return predicatePercentageRight; }
  double getNumberOfTuplesOutput() { return numberOfOutputTuples; }
  double getNumberOfLeftTuplesOutput() { return leftNumOfTuples; }
  double getNumberOfRightTuplesOutput() { return rightNumOfTuples; }
  double getOutputPercentage() {
    double max = (leftNumOfTuples > rightNumOfTuples) ? leftNumOfTuples
                                                      : rightNumOfTuples;
    return numberOfOutputTuples / (max + 1);
  }

 private:
  deceve::storage::BufferManager* bufferManager;
  std::string leftfile;
  std::string rightfile;
  std::string outfile;
  LeftExtract left_extractor;
  RightExtract right_extractor;
  ProjectedRightExtract projected_right_extractor;
  RightConvert right_converter;
  Combine combiner;
  Less less;
  size_t number_of_partitions;
  // This variable determines if intermediate output files should be stored
  bool persistOutput_flag;
  // This variable determines if stored results should be used
  bool usePersistedOutput_flag;
  int result_id;
  deceve::storage::FileMode mode;
  deceve::storage::SorterMode sortIndex;
  std::string runNamePrefix;

  //	deceve::storage::table_value join_tv;
  deceve::storage::table_value left_join_tv;
  deceve::storage::table_value right_join_tv;
  deceve::storage::table_key left_join_tk;
  deceve::storage::table_key right_join_tk;

  bool isStreamFlag{false};

  double leftNumOfTuples{0};
  double rightNumOfTuples{0};
  double predicatePercentageLeft{0};
  double predicatePercentageRight{0};
  double numberOfOutputTuples{0};
};
}
}

#endif /* MERGESORTJOIN_HH_ */
