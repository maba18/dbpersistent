/*
 * testKnapsack.cc
 *
 *  Created on: 27 May 2015
 *      Author: mike
 */

#define CATCH_CONFIG_MAIN
#include "../../queryplans/Knapsack.hh"
#include <iostream>
#include "../catch.hpp"

using namespace std;



TEST_CASE( "Knapsack", "[knapsack]" ) {

  int budget = 301;

  deceve::queries::Knapsack kn;

  std::vector<int> weights;
  std::vector<int> values;

  weights.push_back(100);
  values.push_back(0.5 * 100 );

  weights.push_back(100);
  values.push_back(0.3 * 100);

  weights.push_back(100);
  values.push_back(0.2 * 100);

  weights.push_back(100);
  values.push_back(4 * 100);


  std::set<int> results1;

  // / = kn.greedyKnapsack(weights.size(), budget, weights, values);

  // for (auto &it : results1) {
  //   std::cout << "Result: " << it << " ";
  // }

  // std::cout << "\n";

 SECTION( "knapsack1" ) {
    REQUIRE( results1.find(0) != results1.end() );
    REQUIRE( results1.find(1) != results1.end() );
    REQUIRE( results1.find(3) != results1.end() );
  }

}

