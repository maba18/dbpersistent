#include <iostream>
#include <fstream>
#include "../utils/types.hh"
#include "../storage/File.hh"
#include "../storage/MemPage.hh"
#include "../storage/BufferPool.hh"
#include "../storage/BufferManager.hh"
#include "../storage/Record.hh"
#include "../storage/Pages.hh"
#include "../storage/HeapFile.hh"
#include "../storage/BTree.hh"
#include "../storage/Schema.hh"
#include "../storage/AccessMethods.hh"

using namespace std;

void print4096(const char *s) {
  std::cout << "\t-------------- Page Start --------------" << "\n";
  for (int i = 0; i < 4096; i++)
    std::cout << s[i];
  std::cout << "\n";
  std::cout << "\t-------------- Page End --------------" << "\n";
}

void testSchema() {
  deceve::storage::Schema s1;
  deceve::storage::Schema s2;

  s1.add(deceve::storage::Field("lala", deceve::storage::INTEGER));
  s1.add(deceve::storage::Field("bingo", deceve::storage::INTEGER));
  s1.add(deceve::storage::Field("bingo1", deceve::storage::DOUBLE));
  s1.add(deceve::storage::Field("bingo2", deceve::storage::LONG));
  s2.add(deceve::storage::Field("koko", deceve::storage::LONG));
  s1.merge(s2);
  std::cout << s1 << "\n";
  /*
   std::set<size_t> pl;
   pl.insert(1);
   pl.insert(3);
   s1.project(pl);
   std::cout << s1 << "\n";
   */
}

int main() {

  typedef deceve::storage::BTree<deceve::storage::MyKey,
      deceve::storage::MyPayload> MyBTree;
  typedef MyBTree::record_type MyRecord;

  // buffer pool size in pages
  unsigned int bp_size = 100;
  srand(1);

  deceve::storage::BufferManager bm(bp_size * deceve::storage::PAGE_SIZE);
  MyBTree testTree(&bm, "testTree.bin");

  unsigned long val = 0;

  for (int times = 0; times < 10000; times++) {
    std::cout << "inserting record... " << times << "\n";
    val = rand() % numeric_limits<long>::max();
    testTree.insert(
        deceve::storage::makeRecord<deceve::storage::MyKey,
            deceve::storage::MyPayload>(deceve::storage::makeKey(val),
                                        deceve::storage::makePayload(val)));
    std::cout << "inserted" << "\n";
  }

  int total = 0;
  for (MyBTree::iterator it2 = testTree.begin(); it2 != testTree.end();) {
    std::cout << " Key: " << it2->key
              /*<< "\tPayload: " << it2->payload */<< "\n";
    it2++;
    total++;
  }

  std::cout << "Total records in b-tree: " << total << "\n";

  testSchema();

  deceve::storage::Scanner scanner(&bm, "testTree.bin");
  const char *ptr = 0;
  unsigned int count = 0;
  scanner.open();
  while (scanner.hasNext()) {
    ptr = scanner.nextPage();
    std::cout << "read page " << count++ << "\n";
  }
  scanner.close();
  return 0;
}

