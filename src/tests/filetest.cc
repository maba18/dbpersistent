#include <iostream>
#include "../storage/io.hh"
// #include "../file.hh"
// x#include "../mergesort.hh"

int main() {
  /*
  typedef deceve::bama::File<int, int, 8192> MyFile;
  typedef MyFile::page_type MyPage;
  MyFile file("test.bin");

  MyPage page;
  page.add(deceve::bama::makeRecord(10, 10));
  page.add(deceve::bama::makeRecord(10, 10));

  file.write(0, page);
  MyPage *p2 = file.read(0);
  std::cout << "page size is " << p2->size
            << ", slot capacity: " << p2->capacity()
            << " (" << p2->length() << " slots occupied)"
            << "\n";
  delete p2;
  */
  deceve::bama::fs::init();
  deceve::bama::Writer<int> wf("lala.bin");

  for (int i = 0; i < 600; i++) {
    wf.write(i);
    // std::cout << "wrote " << i << "\n";
  }

  wf.flush();
  wf.close();
  std::cout << "created file"
            << "\n";
  /*
  deceve::bama::Reader<int, int, 4096> rf("lala.bin");
  while (rf.hasNext()) {
      std::cout << rf.nextRecord() << "\n";
      rf.skip(10);
  }
  rf.close();
  */

  size_t len;
  size_t pages = 3;
  deceve::bama::BulkProcessor<int> bpf("lala.bin");
  deceve::bama::BulkProcessor<int>::record_type* r =
      bpf.bulkRead(1, pages, len);
  std::cout << "read " << len << " records, " << pages << " pages"
            << "\n";

  // for (int i = 0; i < len; i++) std::cout << i << ": " << r[i] << "\n";

  for (size_t i = 0; i < len; i++) r[i] = len;
  // deceve::bama::aligned_delete((unsigned char*) r);
  bpf.bulkWrite(1, r, len);
  std::cout << "wrote " << len << " records"
            << "\n";
  r = bpf.bulkRead(1, pages, len);
  std::cout << "read " << len << " records, " << pages << " pages"
            << "\n";
  // for (int i = 0; i < len; i++) std::cout << i << ": " << r[i] << "\n";

  bpf.close();
  // deceve::bama::aligned_delete((unsigned char*) r);

  deceve::bama::Reader<int> rf("lala.bin");
  while (rf.hasNext()) {
    std::cout << rf.nextRecord() << "\n";
  }
  rf.close();
  deceve::bama::fs::shutdown();
}
