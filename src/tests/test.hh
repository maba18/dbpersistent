#include <iostream>
#include <string>
#include <tuple>
#include <time.h>       /* time */
#include <stdlib.h>     /* srand, rand */


std::tuple<int,int> getRandomShipModes(){

  srand (time(NULL));

    std::vector<std::string> shipmodes = { "REG AIR", "AIR", "RAIL", "SHIP",
        "TRUCK", "MAIL", "FOB" };

    int randomShipMode1 = 0;
    int randomShipMode2 = 0;

   randomShipMode1 = (rand() % 100)%7;



    do {
      randomShipMode2 = (rand() %100)% 7;
    } while (randomShipMode2 == randomShipMode1);

    std::cout<<randomShipMode1<<""<<randomShipMode2<<std::endl;

    return std::make_tuple(randomShipMode1, randomShipMode2);

}
