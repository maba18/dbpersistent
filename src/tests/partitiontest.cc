#include "../algorithms/partition.hh"
//#include "../mergesort.hh"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <sys/time.h>

class extractor {
public:
    int operator()(const int& x) const { return x; }
};


class hash {
public:
    size_t operator()(const int& value) {
        char* k = (char*) &value;
        size_t hash = 5381;
        for (size_t i = 0; i < sizeof(int); i++)
            hash = ((hash << 5) + hash) + k[i];
        return hash;
    }
};

int main() {
    typedef deceve::bama::Writer<int> MyWriter;
    typedef deceve::bama::Reader<int> MyReader;
    typedef MyWriter::record_type MyRecord;

    MyWriter writer("test.bin");
    
    //for (int i = 50000000; i > 0; i--)
    //writer.write(deceve::makeRecord(i, i));
    for (int i = 50000; i > 0; i--) {
        int key = ::rand() % 300000;
        //if (key < 5000) key = key % 5000 + 5;
        writer.write(key);
    }
    writer.flush();
    
    //std::cout << "file created" << "\n";

    //deceve::MergeSort<int, int, 4096> sorter("test.bin",
    deceve::bama::Partition<int>//, hash, 4096>
        partitioner("test.bin", "test.bin", 10);
    partitioner.partition();

    MyReader reader("test.bin");
    int i = 0;
    while (reader.hasNext()) {
        std::cout << (i++) << ": " << reader.nextRecord() << "\n";
    }
    reader.close();
}
