#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do
                           // this in one cpp file

#include <tuple>
#include "catch.hpp"

#include "test.hh"



// Include General Functions
#include "../utils/types.hh"
#include "../utils/global.hh"
#include "../utils/defs.hh"
#include "../utils/util.hh"
#include "../storage/io.hh"


// Include Storage Classes
#include "../storage/BufferManager.hh"
#include "../storage/IterableQueue.hh"
#include "../storage/StorageManager.hh"

#include "../queryplans/queries/MyQuery5.hh"

deceve::queries::QOperator *getQueryWithId(deceve::storage::BufferManager *bm,
                                           deceve::queries::QueryManager *qm,
                                           int number) {
  // std::cout << "Query Id: " << random_query_index << "\n";
  switch (number) {
    //    case 1:
    //      detailedStatisticsFile << "Query1 | ";
    //      return new deceve::queries::MyQuery1(bm, qm);
    //    case 2:
    //      detailedStatisticsFile << "Query2 | ";
    //      return new deceve::queries::MyQuery2(bm, qm);
    //    case 3:
    //      detailedStatisticsFile << "Query3 | ";
    //      return new deceve::queries::MyQuery3(bm, qm);
    //    case 4:
    //      detailedStatisticsFile << "Query4 | ";
    //      return new deceve::queries::MyQuery4(bm, qm);
    case 5:
      return new deceve::queries::MyQuery5(bm, qm);
    //    case 6:
    //      detailedStatisticsFile << "Query6 | ";
    //      return new deceve::queries::MyQuery6(bm, qm);
    //    case 7:
    //      detailedStatisticsFile << "Query7 | ";
    //      return new deceve::queries::MyQuery7(bm, qm);
    //    case 8:
    //      detailedStatisticsFile << "Query8 | ";
    //      return new deceve::queries::MyQuery8(bm, qm);
    //    case 9:
    //      detailedStatisticsFile << "Query9 | ";
    //      return new deceve::queries::MyQuery9(bm, qm);
    //    case 10:
    //      detailedStatisticsFile << "Query10 | ";
    //      return new deceve::queries::MyQuery10(bm, qm);
    //    case 11:
    //      detailedStatisticsFile << "Query11 | ";
    //      return new deceve::queries::MyQuery11(bm, qm);
    //    case 12:
    //      detailedStatisticsFile << "Query12 | ";
    //      return new deceve::queries::MyQuery12(bm, qm);
    //    case 13:
    //      detailedStatisticsFile << "Query13 | ";
    //      return new deceve::queries::MyQuery13(bm, qm);
    //    case 14:
    //      detailedStatisticsFile << "Query14 | ";
    //      return new deceve::queries::MyQuery14(bm, qm);
    //    case 15:
    //      detailedStatisticsFile << "Query15 | ";
    //      return new deceve::queries::MyQuery15(bm, qm);
    //    case 16:
    //      detailedStatisticsFile << "Query16 | ";
    //      return new deceve::queries::MyQuery16(bm, qm);
    //    case 17:
    //      detailedStatisticsFile << "Query17 | ";
    //      return new deceve::queries::MyQuery17(bm, qm);
    //    case 18:
    //      detailedStatisticsFile << "Query18 | ";
    //      return new deceve::queries::MyQuery18(bm, qm);
    //    case 19:
    //      detailedStatisticsFile << "Query19 | ";
    //      return new deceve::queries::MyQuery19(bm, qm);
    //    case 20:
    //      detailedStatisticsFile << "JOIN PRIMARY TABLES | ";
    //      return new deceve::queries::MyQueryJoin(bm, qm);
    //    case 21:
    //      detailedStatisticsFile << "Just A selection on Lineitem PRIMARY| ";
    //      return new deceve::queries::MyQuery(bm, qm);
    default:
      return new deceve::queries::MyQuery5(bm, qm);
  }
}

TEST_CASE("Check shipmodes function", "[example]") {
  int ship1 = 0;
  int ship2 = 0;

  std::tie(ship1, ship2) = getRandomShipModes();
  REQUIRE(ship1 != ship2);
}

// ################# QOperator Functions testing ####################

TEST_CASE("QOperator::doesQueryContainDS(tk)", "[doesQueryContainDS]") {
  deceve::queries::QueryManager qm;
  deceve::storage::BufferManager bm(1000 * 1024, 1);


  deceve::queries::QOperator *randQuery = getQueryWithId(&bm, &qm, 5);
  deceve::storage::table_key right_tk;

  right_tk.version = deceve::storage::HASHJOIN;
  right_tk.table_name = deceve::storage::LINEITEM;
  right_tk.field = deceve::storage::L_ORDERKEY;
  right_tk.projected_field = deceve::storage::LINEITEM_Q5;

  bool isContained = randQuery->doesQueryContainDS(right_tk);

  REQUIRE(isContained == true);

  SECTION("changing key") {
    right_tk.table_name = deceve::storage::OTHER_TABLENAME;
    isContained = randQuery->doesQueryContainDS(right_tk);
    INFO("The value of tk is " << right_tk);  // prints with message
    CAPTURE(right_tk);  // prints the values right_tk in case of a fail
    REQUIRE_FALSE(isContained);
  }
}
