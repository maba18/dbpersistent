#include "../storage/heap.hh"
#include <iostream>

int main() {
    deceve::Heap<int, 100> heap;
    for (int i = 0; i < 30; i++) heap.add(i);
    heap.toTree(std::cout);

    deceve::Heap<int, 100> heap2;
    deceve::Heap<int, 100>::element_type array[30];
    for (int i = 0; i < 30; i++) array[i] = i;
    heap2.dump(&array[0], 30);
    heap2.build();
    heap2.toTree(std::cout);
}
