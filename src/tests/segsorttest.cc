#include "../algorithms/segmentsort.hh"
//#include "../mergesort.hh"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <sys/time.h>

class sort_comparator {
public:
    bool operator()(const int& x, const int& y) const {
        return x < y;
    }
};

int main(int ac, char **av) {
    if (ac != 5) {
        std::cerr << "not enough parameters" << "\n";
        exit(1);
    }
    
    typedef deceve::bama::Writer<int> MyWriter;
    typedef deceve::bama::Reader<int> MyReader;
    typedef MyWriter::record_type MyRecord;
    
    MyWriter writer("test.bin");
    
    //for (int i = 50000000; i > 0; i--)
    //writer.write(deceve::makeRecord(i, i));
    for (int i = 0; i < ::atoi(av[1]); i++) {
        int key = ::rand() % 300000;
        //int key = (::atoi(av[1]) - i);// % 15;
        //int key = i;
        //if (key < 5000) key = key % 5000 + 5;
        writer.write(key);
        //std::cout << i << " -- inserted " << key << "\n";
    }
    writer.flush();
    
    deceve::bama::reset_reads();
    deceve::bama::reset_writes();
    
    //std::cout << "file created" << "\n";
    
    //deceve::MergeSort<int, int, 4096> sorter("test.bin",
    
    deceve::bama::SegmentSort<int, deceve::bama::Identity<int>, 
                              std::less<int> >    
        sorter("test.bin", "out.bin", ::atoi(av[2]), ::atof(av[4]));
    sorter.sort();
    

    /*
    MyReader reader("out.bin");    
    //MyReader reader("test.bin");    
    int i = 0;
    while (reader.hasNext()) {
        std::cout << (i++) << ": " << reader.nextRecord() << "\n";
    }
    reader.close();
    */
    
    std::cout << "reads: " << deceve::bama::reads 
              << ", writes: " << deceve::bama::writes 
              << ", cost: " << (deceve::bama::reads
                                + ::atof(av[3])*deceve::bama::writes)
              << "\n";
}
