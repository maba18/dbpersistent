#include <iostream>
#include "../storage/BlobStore.hh"

int main() {
  deceve::storage::BlobStore store("blob.bin");
  unsigned char *data;
  size_t len;

  for (int i = 0; i < 1000; i++) {
    len = 100*i + 10;
    data = new unsigned char[len];
    ::memset(data, i, len);
    store.append(data, len);
    std::cout << i << ": " << len << " bytes written" << "\n";
    delete [] data;
  }

  off_t off = 0;
  for (int i = 0; i < 1000; i++) {        
    store.allocateAndRead(off, &data, len);
    std::cout << i << ": " << len << " bytes read" << "\n";
    off += len + sizeof(off) + 1;
    delete [] data;
  }
}
