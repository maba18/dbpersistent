#include "../algorithms/mergejoin.hh"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <sys/time.h>

int main() {
    typedef deceve::Writer<int, int> MyWriter;
    typedef MyWriter::record_type MyRecord;
    
    MyWriter leftwriter("left.bin");
    MyWriter rightwriter("right.bin");
    
    for (int i = 0; i < 7000; i++) {
        leftwriter.write(deceve::makeRecord(i, i));
        for (int j = 0; j < 10; j++) {
            rightwriter.write(deceve::makeRecord(i, i));
        }
    }
    leftwriter.flush();
    rightwriter.flush();
    
    /*
    deceve::Reader<int, int, 4096> reader("left.bin");
    while (reader.hasNext()) std::cout << reader.nextRecord() << "\n";
    */
    
    deceve::MergeJoin<int, int, int> joiner("left.bin", "right.bin",
                                            "out.bin");
    joiner.join();
}
