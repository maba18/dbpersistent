#include "../algorithms/hybridjoin.hh"
#include "../storage/identity.hh"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <sys/time.h>

struct lala {
    int first;
    int second;
};

int main(int ac, char **av) {
    if (ac != 7) {
        std::cerr << "not enough parameters" << "\n";
        exit(1);
    }
    
    typedef deceve::bama::Writer<int> MyWriter;
    typedef deceve::bama::Reader<struct lala> MyReader;
    typedef MyWriter::record_type MyRecord;
    
    MyWriter leftwriter("left.bin");
    MyWriter rightwriter("right.bin");
    
    for (int i = 0; i < ::atoi(av[1]); i++) {
        leftwriter.write(i);
        for (int j = 0; j < ::atoi(av[2]); j++) {
            rightwriter.write(i);
        }
    }
    //leftwriter.flush();
    //rightwriter.flush();
    leftwriter.close();
    rightwriter.close();

    deceve::bama::reset_reads();
    deceve::bama::reset_writes();
    
    deceve::bama::HybridJoin<int, int,
                             deceve::bama::PairCombiner<int, int>,
                             deceve::bama::Identity<int>,
                             deceve::bama::Identity<int>,
                             std::less<int> >
        joiner("left.bin", "right.bin", "out.bin", ::atoi(av[4]),
               ::atof(av[5]), ::atof(av[6]));
    joiner.join();

    /*
    MyReader reader("out.bin");
    int i = 0;
    //deceve::bama::Pair<int, int> pair;
    struct lala pair;
    while (reader.hasNext()) {
        pair = reader.nextRecord();
        std::cout << (i++) << ": " << pair.first << " - "
                  << pair.second << "\n";
    }
    reader.close();
    */

    std::cout << "reads: " << deceve::bama::reads 
              << ", writes: " << deceve::bama::writes 
              << ", cost: " << (deceve::bama::reads
                                + ::atof(av[3])*deceve::bama::writes)
              << "\n";
}
