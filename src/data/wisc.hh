#ifndef __DECEVE_BAMA_WISC_HH__
#define __DECEVE_BAMA_WISC_HH__

#include <sys/types.h>
#include <sys/stat.h>

#include <string>
#include <cstring>
#include <iostream>
#include "../storage/io.hh"


namespace deceve { namespace bama {

struct wisc_t {
    unsigned int unique1;
    unsigned int serial;
    unsigned int two;
    unsigned int four;
    unsigned int ten;
    unsigned int twenty;
    unsigned int one_percent;
    unsigned int ten_percent;
    unsigned int twenty_percent;
    unsigned int fifty_percent;
    unsigned int skewed;
    char stringu1[8];
    char stringu2[8];
    char stringu3[8];
    friend std::ostream& operator<<(std::ostream& o, const wisc_t& r);
};

class wisc_generator {
public:
    typedef Writer<wisc_t> writer_type;
    
    wisc_generator()
        : m_prime(0), 
          m_generator(0), 
          m_length(0), 
          m_skew(0), 
          m_skewed_keys(0) 
    {}
    ~wisc_generator() {}
    
    void set_length(size_t len);
    inline void set_skew(double s) { m_skew = s; }
    inline void set_number_of_skewed_keys(size_t n) { m_skewed_keys = n; }
    void create_file(const std::string& fn);
    void create_file(deceve::storage::BufferManager *bm ,const std::string& fn) ;

protected:
    size_t randomize(size_t s);
    void populate_relation(writer_type &w);
    void cycle(char* s, size_t i);

private:    
    size_t m_prime;
    size_t m_generator;
    size_t m_length;
    float m_skew;
    size_t m_skewed_keys;
};

}
}

#endif
