#ifndef __WISC_EXTRACTOR_HH__
#define __WISC_EXTRACTOR_HH__

#include "../data/wisc.hh"

class column_extractor {
public:
    typedef unsigned int key_type;

    column_extractor(size_t col): m_column(col) {}
    
    inline const key_type& extract(const deceve::bama::wisc_t& w) const {
        switch (m_column) {
        case 0: return w.unique1;
        case 1: return w.serial;
        case 2: return w.two;
        case 3: return w.four;
        case 4: return w.ten;
        case 5: return w.twenty;
        case 6: return w.one_percent;
        case 7: return w.ten_percent;
        case 8: return w.twenty_percent;
        case 9: return w.fifty_percent;
        case 10: return w.skewed;
        default: return w.unique1;
        }
    }
    /*
    key_type extract(const deceve::bama::wisc_t& w) {
        switch (m_column) {
        case 0: return w.unique1;
        case 1: return w.serial;
        case 2: return w.two;
        case 3: return w.four;
        case 4: return w.ten;
        case 5: return w.twenty;
        case 6: return w.one_percent;
        case 7: return w.ten_percent;
        case 8: return w.twenty_percent;
        case 9: return w.fifty_percent;
        case 10: return w.skewed;
        default: return w.unique1;
        }
    }
    */
    inline const key_type& operator()(const deceve::bama::wisc_t& w) const {
        return extract(w);
    }
    
private:
    size_t m_column;
};

template <typename Record>
class column_extractor_template {
public:
    typedef unsigned int key_type;

    column_extractor_template(size_t col): m_column(col) {}

    inline const key_type& extract(const Record& w) const {
        switch (m_column) {
        case 0: return w.unique1;
        case 1: return w.serial;
        case 2: return w.two;
        case 3: return w.four;
        case 4: return w.ten;
        case 5: return w.twenty;
        case 6: return w.one_percent;
        case 7: return w.ten_percent;
        case 8: return w.twenty_percent;
        case 9: return w.fifty_percent;
        case 10: return w.skewed;
        default: return w.unique1;
        }
    }
    /*
    key_type extract(const deceve::bama::wisc_t& w) {
        switch (m_column) {
        case 0: return w.unique1;
        case 1: return w.serial;
        case 2: return w.two;
        case 3: return w.four;
        case 4: return w.ten;
        case 5: return w.twenty;
        case 6: return w.one_percent;
        case 7: return w.ten_percent;
        case 8: return w.twenty_percent;
        case 9: return w.fifty_percent;
        case 10: return w.skewed;
        default: return w.unique1;
        }
    }
    */
    inline const key_type& operator()(const Record& w) const {
        return extract(w);
    }

private:
    size_t m_column;
};

class bound_selector {
public:
    typedef deceve::bama::wisc_t input_type;
    typedef deceve::bama::wisc_t output_type;
    bound_selector(unsigned int x): m_bound(x) {}

    bool operator()(const input_type& i) const { return i.unique1 < m_bound; }
private:
    unsigned int m_bound;
};

class column_projector {
public:
    typedef deceve::bama::wisc_t input_type;

    column_projector(size_t f)
        : m_length((f > 0 && f < 11 ? f : 1) * sizeof(unsigned int))
    {}

    void operator()(const input_type& in, char* out) const {
        ::memmove(out, (char*) &in, m_length);
    }

private:
    size_t m_length;
};

class simple_combinator {
public:
    typedef deceve::bama::wisc_t record_type;

    record_type operator()(const deceve::bama::wisc_t& l,
                           const deceve::bama::wisc_t &r) const {
        (void) r; 
        return l;
    }
};

#endif
