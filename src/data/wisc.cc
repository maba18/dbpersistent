#include "wisc.hh"

namespace deceve {
namespace bama {

std::ostream& operator<<(std::ostream& o, const wisc_t& r) {
	o << "[" << r.unique1 << ", " << r.serial << ", " << r.two << ", " << r.four
			<< ", " << r.ten << ", " << r.twenty << ", " << r.one_percent
			<< ", " << r.ten_percent << ", " << r.twenty_percent << ", "
			<< r.fifty_percent << ", " << r.skewed << ", " << r.stringu1 << ", "
			<< r.stringu2 << ", " << r.stringu3 << "]";
	return o;
}

void wisc_generator::set_length(size_t l) {
	m_length = l;
	if (m_length <= 1000) {
		m_generator = 279;
		m_prime = 1009;
	} else if (m_length <= 10000) {
		m_generator = 2969;
		m_prime = 10007;
	} else if (m_length <= 100000) {
		m_generator = 21395;
		m_prime = 100003;
	} else if (m_length <= 1000000) {
		m_generator = 2107;
		m_prime = 1000003;
	} else if (m_length <= 10000000) {
		m_generator = 211;
		m_prime = 10000019;
	} else if (m_length <= 100000000) {
		m_generator = 21;
		m_prime = 100000007;
	} else {
		m_length = 100000000;
		m_generator = 21;
		m_prime = 100000007;
	}
}

//void wisc_generator::create_file(const std::string& fn) {
//    writer_type writer(fn);
//    populate_relation(writer);
//    writer.close();
//}

std::string generate_run(const std::string & filename, int number_of_runs) {
	std::stringstream s;
	s << filename << "." << number_of_runs;
//	std::cout << filename << "." << number_of_runs << std::ends;
	//number_of_runs++;
	return s.str();
}

void wisc_generator::create_file(deceve::storage::BufferManager *bm,
		const std::string& fn) {
	writer_type writer(bm, fn, deceve::storage::INTERMEDIATE);

	populate_relation(writer);
	writer.close();

//	int counter = 0;
//	writer.open(generate_run(fn, counter));
//	populate_relation(writer);
//
//	for (int i = 0; i < 10; ++i) {
//
//		if (counter%2 ==1) {
//			writer.close();
//
//			writer.open(generate_run(fn, counter));
//			populate_relation(writer);
//
//
//		}
//		counter++;
//	}
//
//	writer.close();

}

size_t wisc_generator::randomize(size_t seed) {
	do {
		seed = (m_generator * seed) % m_prime;
	} while (seed > m_length);
	return seed;
}

void wisc_generator::populate_relation(writer_type& writer) {
	size_t seed = m_generator;
	wisc_t record;
	for (size_t i = 0; i < m_length; i++) {
		seed = randomize(seed);
		record.unique1 = seed - 1;
		record.serial = i;
		record.two = record.unique1 % 2;
		record.four = record.unique1 % 4;
		record.ten = record.unique1 % 10;
		record.twenty = record.unique1 % 20;
		record.one_percent = record.unique1 / 100;
		record.ten_percent = record.unique1 / 10;
		record.twenty_percent = record.unique1 / 5;
		record.fifty_percent = record.unique1 / 2;
		if (record.unique1 < m_skew * m_length) {
			record.skewed = record.unique1 % m_skewed_keys;
		} else {
			record.skewed = record.unique1;
		}
		cycle(record.stringu1, record.unique1);
		cycle(record.stringu2, record.serial);
		cycle(record.stringu3, record.twenty);

		writer.write(record);
	//	std::cout<<"Record: "<<record<<"\n";
	}
}

void wisc_generator::cycle(char* s, size_t i) {
	switch (i % 4) {
	case 0:
		::strcpy(s, "AAAAAAA");
		break;
	case 1:
		::strcpy(s, "HHHHHHH");
		break;
	case 2:
		::strcpy(s, "VVVVVVV");
		break;
	default:
		::strcpy(s, "OOOOOOO");
	}
}

}
}
