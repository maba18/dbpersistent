#!/bin/bash


newName=index"$1".php

echo $newName


if [ "$(uname)" == "Darwin" ]
then

cp patternMac.txt $newName

elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]
then

cp pattern.txt $newName

elif [ -n "$COMSPEC" -a -x "$COMSPEC" ]
then 
  echo $0: this script does not support Windows \:\(
fi

pattern="s/INDEX/$1/g"

sed -i -- $pattern $newName