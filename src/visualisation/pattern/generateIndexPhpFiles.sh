#!/bin/bash


count=$(ls -l *json* | wc -l)


echo $count

limit=$((count - 1))


 for i in `seq 0 $limit`;
        do
                ./makeACopy.sh $i
        done


if [ "$(uname)" == "Darwin" ]
then

rm /Applications/MAMP/htdocs/test/*php
rm /Applications/MAMP/htdocs/test/*json

cp *.php  /Applications/MAMP/htdocs/test/
cp *.json /Applications/MAMP/htdocs/test/

elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]
then

rm /var/www/html/test/*php
rm /var/www/html/test/*json

cp *.php /var/www/html/test/
cp *.json /var/www/html/test/


elif [ -n "$COMSPEC" -a -x "$COMSPEC" ]
then 
  echo $0: this script does not support Windows \:\(
fi