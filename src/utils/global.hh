#ifndef __GLOBAL_HH__
#define __GLOBAL_HH__

#include <cstring>
#include <cstdlib>
#include <sys/types.h>
#include <unistd.h>
#include <stdint.h>
#include <iostream>
#include <chrono>
#include <vector>

//#define PROFILING

namespace deceve {

namespace profiling {

using namespace std;

// static Timer bmrTimer{bmrWriteCosts};
// static std::vector<std::chrono::duration<double>> bmrWriteCosts{};

class Timer {
 public:
  Timer(int secs, const std::string& headerString) {
    sections = secs;
    for (int i = 0; i < sections; i++) {
      times.push_back(std::chrono::duration<double>(0));
      operations.push_back(0);
    }
    headerName = headerString;
  }
  void start() { timer1 = std::chrono::steady_clock::now(); }
  void end(int i) {
    timer2 = std::chrono::steady_clock::now();
    std::chrono::duration<double> result = timer2 - timer1;
    times[i] += result;
    operations[i]++;
    numberOfOperations++;
  }
  void printTimes() {
    if (sections == 0 || numberOfOperations == 0 ||
        numberOfOperations / sections == 0) {
      std::cout << "ERROR: divide with 0" << std::endl;
      return;
    }

    std::cout << "---- TIME PROFILING ---- " << std::endl;
    std::cout << "---- " << headerName << " ---- " << std::endl;
    std::cout << "Total Number of calls: " << numberOfOperations / sections
              << std::endl;

    // Total time in ms
    double totalTime = std::chrono::duration<double, milli>(times[0]).count();
    for (size_t i = 1; i < times.size(); i++) {
      totalTime += std::chrono::duration<double, milli>(times[i]).count();
    }
    std::cout << "Total time: " << totalTime << std::endl;

    // Avg time in ms
    double avgTime = totalTime / (numberOfOperations / sections);
    std::cout << "Avgr. time: " << avgTime << std::endl;

    for (size_t i = 0; i < times.size(); i++) {
      std::cout << " ----------- SECTION " << i << "----------------"
                << std::endl;
      std::cout << "number calls: " << operations[i] << std::endl;
      std::cout << "tot_time(ms): "
                << std::chrono::duration<double, milli>(times[i]).count()
                << "\n";
    }

    for (size_t i = 0; i < times.size(); i++) {
      std::cout << "s: " << i << " avg_time(ms): "
                << std::chrono::duration<double, milli>(times[i]).count() /
                       (numberOfOperations / sections) << "\n";
    }
  }

 private:
  chrono::time_point<std::chrono::steady_clock> timer1;
  chrono::time_point<std::chrono::steady_clock> timer2;
  std::vector<std::chrono::duration<double>> times;
  std::vector<int> operations;
  int sections{0};
  int numberOfOperations{0};
  std::string headerName{"header"};
};
}

namespace queries {

__attribute__((unused)) static unsigned long POOLSIZE = 80000;

const static std::string DBFILES_FOLDER = "dbfiles";
const static std::string DBFILES_TMP_FOLDER = "tmp";
const static std::string LINE_ITEM_PATH = "lineitem.table";
const static std::string CUSTOMER_PATH = "customer.table";
const static std::string ORDERS_PATH = "orders.table";
const static std::string PART_PATH = "part.table";
const static std::string PARTSUPP_PATH = "partsupp.table";
const static std::string SUPPLIER_PATH = "supplier.table";
const static std::string NATION_PATH = "nation.table";
const static std::string REGION_PATH = "region.table";

// static unsigned long tmpFileCounter = 1;

enum OPERATION { SELECT, JOIN, GROUP, SORT, VIEW, FILTER };
enum PRINT_MODE { SIMPLE_PATH, TMP_PATH, FULL_PATH };

inline std::string constructFullPath(const std::string& filename) {
  return DBFILES_FOLDER + "/" + filename;
}
inline std::string constructOutputTmpFullPath(const std::string& filename) {
  return DBFILES_FOLDER + "/" + DBFILES_TMP_FOLDER + "/" + filename;
}

inline std::string getFullPath(const std::string& filename) {
  return DBFILES_FOLDER + "/" + filename + ".table";
}

inline std::string constructOutputTmpFileName(const std::string& filename,
                                              OPERATION operation) {
  switch (operation) {
    case SELECT:
      return DBFILES_FOLDER + "/" + DBFILES_TMP_FOLDER + "/" + filename +
             "_select";
    case GROUP:
      return DBFILES_FOLDER + "/" + DBFILES_TMP_FOLDER + "/" + filename +
             "_group";
    case SORT:
      return DBFILES_FOLDER + "/" + DBFILES_TMP_FOLDER + "/" + filename +
             "_sort";
    case VIEW:
      return DBFILES_FOLDER + "/" + DBFILES_TMP_FOLDER + "/" + filename +
             "_view";
    case FILTER:
      return DBFILES_FOLDER + "/" + DBFILES_TMP_FOLDER + "/" + filename +
             "_filter";
    default:
      return DBFILES_FOLDER + "/" + DBFILES_TMP_FOLDER + "/" + filename;
  }
}

inline std::string constructInputTmpFullPath(const std::string& filename) {
  return DBFILES_FOLDER + "/" + DBFILES_TMP_FOLDER + "/" + filename;
}

inline std::string constructJoinTmpFileName(const std::string& l,
                                            const std::string& r) {
  return DBFILES_FOLDER + "/" + DBFILES_TMP_FOLDER + "/" + l.c_str()[0] + " " +
         r.c_str()[0];
}
}
}

#endif
