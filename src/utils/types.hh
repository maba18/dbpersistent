#ifndef TYPES_HH
#define TYPES_HH

#include <map>
#include <set>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include "../storage/File.hh"
#include <fcntl.h>

#include <atomic>
#include <iostream>
#include <sstream>  // stringstream
#include <string>
#include <tuple>

// HOT Tags meaning
// changeme -> to complicated or to big
// checkme  -> check if the functionality is correct
// explainme -> need to add comments to understand what the function does
// manually -> parts of code with explicit values
// hot      -> what I am really working now.
// useme    -> see if this function is used somewhere

namespace sto = deceve::storage;
namespace deceve {
namespace storage {

enum { PAGE_SIZE_PERSISTENT = 4096 };

static const size_t WINDOW_SIZE = 10;

typedef unsigned long long Timestamp;

// unsigned long on 64 bit machines,
// unsigned int on 32 bit machines
#ifdef __LP64__
typedef unsigned long ptr_type;
#else
typedef unsigned int ptr_type;
#endif

#ifdef __APPLE__
#ifdef __MACH__
#define lseek64 lseek
#define open64 open
#endif
#endif

//#define UNIFIED

typedef struct FilePos {
  std::string filename;
  offset_t offset;

  FilePos(const std::string fname, const offset_t offt)
      : filename(fname), offset(offt){};

  bool operator<(const FilePos &e) const {
    bool fn_comp = filename < e.filename;
    bool off_comp = offset < e.offset;
    return (fn_comp ? true : (filename == e.filename ? off_comp : false));
  }

  bool operator==(const FilePos &e) {
    bool fn_comp = filename == e.filename;
    bool off_comp = offset == e.offset;
    return (fn_comp ? true : (filename == e.filename ? off_comp : false));
  }

} FilePos;

struct FilePos_equal {
  bool operator()(const FilePos &x, const FilePos &y) const {
    return (x.filename == y.filename && x.offset == y.offset);
  };
};

struct FilePos_hash {
  std::size_t operator()(const FilePos &f) const {
    std::size_t seed = 0;
    offset_t page_offset = f.offset / PAGE_SIZE_PERSISTENT;
    long short_offset = (long)page_offset;
    boost::hash_combine(seed, f.filename);
    boost::hash_combine(seed, short_offset);
    return seed;
  };
};

// Comparison operators
enum CompOp {
  NO_OP,  // no comparison
  EQ_OP,
  NE_OP,
  LT_OP,  //<
  GT_OP,
  LE_OP,  //<=
  GE_OP   //>= // binary atomic operators
};

struct AttrValue {
  std::string attrName;
  std::string value;
  CompOp operation;

  AttrValue(const std::string &type, const std::string &val, CompOp op = EQ_OP)
      : attrName(type), value(val), operation(op){};

  bool operator==(const AttrValue &e) const {
    if (attrName != e.attrName) return false;
    if (value != e.value) return false;
    if (operation != e.operation) return false;

    return true;
  }

  friend std::ostream &operator<<(std::ostream &s, const AttrValue &v) {
    s << "[" << v.attrName << " " << v.operation << " " << v.value << "] ";
    return s;
  }
};

struct Details {
  std::string version;
  std::vector<AttrValue> fields;
  int order;

  Details(const std::string &v, const std::vector<AttrValue> &fs, int o = 0)
      : version(v), fields(fs), order(o){};

  bool operator<(const Details &e) const {
    bool off_comp = order < e.order;
    return (off_comp ? true : false);
  }

  bool operator==(const Details &e) const {
    if (version != e.version) return false;

    if (fields.size() != e.fields.size()) {
      return false;
    } else {
      for (size_t i = 0; i < fields.size(); i++) {
        if (!(fields[i] == e.fields[i])) {
          return false;
        }
      }
    }
    return true;
  }

  friend std::ostream &operator<<(std::ostream &s, const Details &v) {
    s << "Version: [" << v.version << "] Fields: ";

    for (std::vector<AttrValue>::const_iterator it = v.fields.begin();
         it != v.fields.end(); ++it) {
      s << *it << ", ";
    }
    s << ""
      << "\n";
    return s;
  }
};

struct FileStatus {
  std::string filename;
  Details file_details;

  FileStatus(const std::string &fn, const Details &det)
      : filename(fn), file_details(det){};

  bool operator==(const FileStatus &e) const {
    if (filename != e.filename) return false;
    if (!(file_details == e.file_details)) return false;
    return true;
  }

  friend std::ostream &operator<<(std::ostream &s, const FileStatus &v) {
    std::cout << "Filename: [" << v.filename << "] " << v.file_details;
    return s;
  }
};

// Attributes

// Attribute types
enum AttrType { INT, FLOAT, STRING };

typedef struct AttributeInfo {
  std::string attr_name;
  std::string attr_type;
  size_t attr_length;
  size_t index;
  size_t offset;

  friend std::ostream &operator<<(std::ostream &os, const AttributeInfo &v) {
    os << "name: " << v.attr_name << " type: " << v.attr_type
       << " length: " << v.attr_length << " "
       << "index: " << v.index << " offset:: " << v.offset;
    os << "\n";
    return os;
  };
} AttributeInfo;

// Used in selection and joins where we need both the name of the relation and
// the name of the attribute
struct RelAttr {
  std::string rel_name;
  std::string attr_name;
};

// Aggregation functions for group by
enum AggFun {
  NO_F,
  MIN_F,
  MAX_F,
  COUNT_F,
  SUM_F,
  AVG_F  // numeric args only
};

// Pin Strategy Hint
enum ClientHint {
  NO_HINT  // default value
};

struct AggRelAttr {
  AggFun func;
  char *relName;   // Relation name (may be NULL)
  char *attrName;  // Attribute name

  // Print function
  friend std::ostream &operator<<(std::ostream &s, const AggRelAttr &ra);
};

struct Value {
  AttrType val_type;
  void *data; /* value                       */
  /* print function              */
  friend std::ostream &operator<<(std::ostream &s, const Value &v);
};

struct Condition {
  RelAttr lhsAttr; /* left-hand side attribute            */
  CompOp op;       /* comparison operator                 */
  int bRhsIsAttr;  /* TRUE if the rhs is an attribute,    */
  /* in which case rhsAttr below is valid;*/
  /* otherwise, rhsValue below is valid.  */
  RelAttr rhsAttr; /* right-hand side attribute            */
  Value rhsValue;  /* right-hand side value                */
  /* print function                               */
  friend std::ostream &operator<<(std::ostream &s, const Condition &c);
};

// Used by Printer class
struct DataAttrInfo {
  std::string relName;   // Relation name
  std::string attrName;  // Attribute name
  int offset;            // Offset of attribute
  AttrType attrType;     // Type of attribute
  int attrLength;        // Length of attribute
  int indexNo;           // Attribute index number
};

struct AttrAndValue {
  std::string attrName;
  std::string value;
};

enum FILE_VERSION { OTHER, SORT, HASHJOIN, SELECT, GROUP };

enum FILE_CLASSIFIER { NOTCLASSIFIED, PERMANENT, TEMPORAL };

// struct TABLENAME_Hash {
//  template <typename T>
//  std::size_t operator()(T t) const {
//    return static_cast<std::size_t>(t);
//  }
//};

enum TABLENAME {
  OTHER_TABLENAME,
  CUSTOMER,
  NATION,
  PARTSUPP,
  REGION,
  LINEITEM,
  ORDERS,
  PART,
  SUPPLIER,
  REVENUE,
  LEFT_RESULT,
  RIGHT_RESULT
};

enum FIELD {
  OTHER_FIELD,
  C_CUSTKEY,
  C_NATIONKEY,
  C_MKTSEGMENT,
  L_DISCOUNT,
  L_EXTENDEDPRICE,
  L_FLAG,
  L_ORDERKEY,
  L_ORDERDATE,
  L_PARTKEY,
  L_QUANTITY,
  L_SHIPDATE,
  L_SUPPKEY,
  L_MIKEKEY,
  N_NATIONKEY,
  N_REGIONKEY,
  O_CUSTKEY,
  O_ORDERDATE,
  O_ORDERKEY,
  P_PARTKEY,
  PS_PARTKEY,
  PS_SUPPKEY,
  R_NAME,
  R_REGIONKEY,
  S_NATIONKEY,
  S_SUPPKEY,
  SUPPLIER_NO,
  LEFT_KEY,
  RIGHT_KEY,
  L_PARTKEY1,
  L_PARTKEY2,
  L_PARTKEY3,
  L_PARTKEY4,
  L_PARTKEY5,
  L_PARTKEY6,
  L_PARTKEY7,
  L_ORDERKEY1,
  L_ORDERKEY2,
  L_ORDERKEY3,
  L_ORDERKEY4,
  ALL_ATTRIBUTES
};

enum PROJECTED_FIELD {
  ALL,
  LINEITEM_UNIFIED,
  LINEITEM_UNIFIED_ORDER_KEY,
  CUSTOMER_Q7,
  LINEITEM_Q5,
  LINEITEM_Q8,
  LINEITEM_Q9,
  LINEITEM_Q14,
  LINEITEM_Q17,
  LINEITEM_Q18,
  ORDER_Q7_SORTED,
  ORDER_Q12,
  ORDER_Q18,
  ORDER_UNIFIED_ORDER_KEY,
  ORDER_Q13_SORTED,
  PART_Q17_OTHER,
  PARTSUPP_Q2,
  PARTSUPP_UNIFIED_PARTKEY,
  PARTSUPP_Q11,
  PARTSUPP_Q16
};

struct table_key {
  FILE_VERSION version{OTHER};
  TABLENAME table_name{OTHER_TABLENAME};
  FIELD field{OTHER_FIELD};
  PROJECTED_FIELD projected_field{ALL};
  std::vector<FIELD> attributes{ALL_ATTRIBUTES};

  bool operator==(const table_key &other) {
    if (version != other.version) return false;
    if (table_name != other.table_name) return false;

    return (field == other.field) ? projected_field == other.projected_field
                                  : false;
  };

  friend bool operator<(const table_key &o1, const table_key &o2) {
    //		return std::make_tuple(o1.table_name, o1.field, o1.version) <
    // std::make_tuple(o2.table_name, o2.field, o2.version);

    if (o1.version != o2.version) {
      return o1.version < o2.version;
    } else if (o1.table_name != o2.table_name) {
      return o1.table_name < o2.table_name;
    } else if (o1.field != o2.field) {
      return o1.field < o2.field;
    } else if (o1.projected_field != o2.projected_field) {
      return o1.projected_field < o2.projected_field;
    } else {
      return false;
    }
  }

  friend bool operator==(const table_key &o1, const table_key &o2) {
    //		return std::make_tuple(o1.table_name, o1.field, o1.version) <
    // std::make_tuple(o2.table_name, o2.field, o2.version);

    //   std::cout << "Equality check between: " << o1 << " " << o2 <<
    //   std::endl;

    //    if (o1.version != o2.version) {
    //      return o1.version < o2.version;
    //    } else if (o1.table_name != o2.table_name) {
    //      return o1.table_name < o2.table_name;
    //    } else if (o1.field != o2.field) {
    //      return o1.field < o2.field;
    //    } else if (o1.projected_field != o2.projected_field) {
    //      return o1.projected_field < o2.projected_field;
    //    }

    bool check1 = (o1.version == o2.version);
    bool check2 = (o1.table_name == o2.table_name);
    bool check3 = (o1.field == o2.field);
    bool check4 = (o1.projected_field == o2.projected_field);

    if (check1 && check2 && check3 && check4) {
      return true;
    }
    return false;
  }

  friend std::ostream &operator<<(std::ostream &os, const table_key &c) {
    static std::map<TABLENAME, std::string> tableNamesMapping;
    if (tableNamesMapping.size() == 0) {
#define INSERT_ELEMENT(p) tableNamesMapping[p] = #p

      INSERT_ELEMENT(PARTSUPP);
      INSERT_ELEMENT(REGION);
      INSERT_ELEMENT(LINEITEM);
      INSERT_ELEMENT(ORDERS);
      INSERT_ELEMENT(PART);
      INSERT_ELEMENT(SUPPLIER);
      INSERT_ELEMENT(REVENUE);
      INSERT_ELEMENT(LEFT_RESULT);
      INSERT_ELEMENT(RIGHT_RESULT);
      INSERT_ELEMENT(OTHER_TABLENAME);
      INSERT_ELEMENT(CUSTOMER);
      INSERT_ELEMENT(NATION);
#undef INSERT_ELEMENT
    }

    static std::map<FIELD, std::string> fieldsMapping;
    if (fieldsMapping.size() == 0) {
#define INSERT_ELEMENT1(p) fieldsMapping[p] = #p

      INSERT_ELEMENT1(OTHER_FIELD);
      INSERT_ELEMENT1(C_CUSTKEY);
      INSERT_ELEMENT1(C_NATIONKEY);
      INSERT_ELEMENT1(C_MKTSEGMENT);
      INSERT_ELEMENT1(L_EXTENDEDPRICE);
      INSERT_ELEMENT1(L_DISCOUNT);
      INSERT_ELEMENT1(L_FLAG);
      INSERT_ELEMENT1(L_ORDERKEY);
      INSERT_ELEMENT1(L_ORDERDATE);
      INSERT_ELEMENT1(L_PARTKEY);
      INSERT_ELEMENT1(L_QUANTITY);
      INSERT_ELEMENT1(L_SHIPDATE);
      INSERT_ELEMENT1(L_SUPPKEY);
      INSERT_ELEMENT1(L_MIKEKEY);
      INSERT_ELEMENT1(N_NATIONKEY);
      INSERT_ELEMENT1(N_REGIONKEY);
      INSERT_ELEMENT1(O_CUSTKEY);
      INSERT_ELEMENT1(O_ORDERDATE);
      INSERT_ELEMENT1(O_ORDERKEY);
      INSERT_ELEMENT1(P_PARTKEY);
      INSERT_ELEMENT1(PS_PARTKEY);
      INSERT_ELEMENT1(PS_SUPPKEY);
      INSERT_ELEMENT1(R_NAME);
      INSERT_ELEMENT1(R_REGIONKEY);
      INSERT_ELEMENT1(S_NATIONKEY);
      INSERT_ELEMENT1(S_SUPPKEY);
      INSERT_ELEMENT1(SUPPLIER_NO);
      INSERT_ELEMENT1(LEFT_KEY);
      INSERT_ELEMENT1(RIGHT_KEY);
      INSERT_ELEMENT1(L_PARTKEY1);
      INSERT_ELEMENT1(L_PARTKEY2);
      INSERT_ELEMENT1(L_PARTKEY3);
      INSERT_ELEMENT1(L_PARTKEY4);
      INSERT_ELEMENT1(L_PARTKEY5);
      INSERT_ELEMENT1(L_PARTKEY6);
      INSERT_ELEMENT1(L_PARTKEY7);
      INSERT_ELEMENT1(L_ORDERKEY1);
      INSERT_ELEMENT1(L_ORDERKEY2);
      INSERT_ELEMENT1(L_ORDERKEY3);
      INSERT_ELEMENT1(L_ORDERKEY4);
      INSERT_ELEMENT1(ALL_ATTRIBUTES);
#undef INSERT_ELEMENT1
    }

    static std::map<FILE_VERSION, std::string> versionMapping;
    if (versionMapping.size() == 0) {
#define INSERT_ELEMENT1(p) versionMapping[p] = #p
      INSERT_ELEMENT1(OTHER);
      INSERT_ELEMENT1(SORT);
      INSERT_ELEMENT1(HASHJOIN);
      INSERT_ELEMENT1(SELECT);
      INSERT_ELEMENT1(GROUP);
#undef INSERT_ELEMENT1
    }

    static std::map<PROJECTED_FIELD, std::string> projectedFieldMapping;
    if (projectedFieldMapping.size() == 0) {
#define INSERT_ELEMENT1(p) projectedFieldMapping[p] = #p
      INSERT_ELEMENT1(ALL);
      INSERT_ELEMENT1(LINEITEM_UNIFIED);
      INSERT_ELEMENT1(LINEITEM_UNIFIED_ORDER_KEY);
      INSERT_ELEMENT1(CUSTOMER_Q7);
      INSERT_ELEMENT1(LINEITEM_Q5);
      INSERT_ELEMENT1(LINEITEM_Q8);
      INSERT_ELEMENT1(LINEITEM_Q9);
      INSERT_ELEMENT1(LINEITEM_Q14);
      INSERT_ELEMENT1(LINEITEM_Q17);
      INSERT_ELEMENT1(LINEITEM_Q18);
      INSERT_ELEMENT1(ORDER_Q7_SORTED);
      INSERT_ELEMENT1(ORDER_Q12);
      INSERT_ELEMENT1(ORDER_Q18);
      INSERT_ELEMENT1(ORDER_UNIFIED_ORDER_KEY);
      INSERT_ELEMENT1(ORDER_Q13_SORTED);
      INSERT_ELEMENT1(PART_Q17_OTHER);
      INSERT_ELEMENT1(PARTSUPP_Q2);
      INSERT_ELEMENT1(PARTSUPP_UNIFIED_PARTKEY);
      INSERT_ELEMENT1(PARTSUPP_Q11);
      INSERT_ELEMENT1(PARTSUPP_Q16);
#undef INSERT_ELEMENT1
    }

    os << "table: [" << tableNamesMapping[c.table_name] << "] field: ["
       << fieldsMapping[c.field] << "] version: [" << versionMapping[c.version]
       << "]"
       << " projected_field: [" << projectedFieldMapping[c.projected_field]
       << "]";
    os << " attributes: {";
    for (auto it : c.attributes) {
      os << fieldsMapping[it] << " ";
    }
    os << "}";
    return os;
  }

  std::string const printFriendly() const {
    static std::map<TABLENAME, std::string> tableNamesMapping;
    if (tableNamesMapping.size() == 0) {
#define INSERT_ELEMENT(p) tableNamesMapping[p] = #p

      INSERT_ELEMENT(PARTSUPP);
      INSERT_ELEMENT(REGION);
      INSERT_ELEMENT(LINEITEM);
      INSERT_ELEMENT(ORDERS);
      INSERT_ELEMENT(PART);
      INSERT_ELEMENT(SUPPLIER);
      INSERT_ELEMENT(REVENUE);
      INSERT_ELEMENT(LEFT_RESULT);
      INSERT_ELEMENT(RIGHT_RESULT);
      INSERT_ELEMENT(OTHER_TABLENAME);
      INSERT_ELEMENT(CUSTOMER);
      INSERT_ELEMENT(NATION);
#undef INSERT_ELEMENT
    }

    static std::map<FIELD, std::string> fieldsMapping;
    if (fieldsMapping.size() == 0) {
#define INSERT_ELEMENT1(p) fieldsMapping[p] = #p

      INSERT_ELEMENT1(OTHER_FIELD);
      INSERT_ELEMENT1(C_CUSTKEY);
      INSERT_ELEMENT1(C_NATIONKEY);
      INSERT_ELEMENT1(C_MKTSEGMENT);
      INSERT_ELEMENT1(L_EXTENDEDPRICE);
      INSERT_ELEMENT1(L_DISCOUNT);
      INSERT_ELEMENT1(L_FLAG);
      INSERT_ELEMENT1(L_ORDERKEY);
      INSERT_ELEMENT1(L_ORDERDATE);
      INSERT_ELEMENT1(L_PARTKEY);
      INSERT_ELEMENT1(L_QUANTITY);
      INSERT_ELEMENT1(L_SHIPDATE);
      INSERT_ELEMENT1(L_SUPPKEY);
      INSERT_ELEMENT1(L_MIKEKEY);
      INSERT_ELEMENT1(N_NATIONKEY);
      INSERT_ELEMENT1(N_REGIONKEY);
      INSERT_ELEMENT1(O_CUSTKEY);
      INSERT_ELEMENT1(O_ORDERDATE);
      INSERT_ELEMENT1(O_ORDERKEY);
      INSERT_ELEMENT1(P_PARTKEY);
      INSERT_ELEMENT1(PS_PARTKEY);
      INSERT_ELEMENT1(PS_SUPPKEY);
      INSERT_ELEMENT1(R_NAME);
      INSERT_ELEMENT1(R_REGIONKEY);
      INSERT_ELEMENT1(S_NATIONKEY);
      INSERT_ELEMENT1(S_SUPPKEY);
      INSERT_ELEMENT1(SUPPLIER_NO);
      INSERT_ELEMENT1(LEFT_KEY);
      INSERT_ELEMENT1(RIGHT_KEY);
      INSERT_ELEMENT1(L_PARTKEY1);
      INSERT_ELEMENT1(L_PARTKEY2);
      INSERT_ELEMENT1(L_PARTKEY3);
      INSERT_ELEMENT1(L_PARTKEY4);
      INSERT_ELEMENT1(L_PARTKEY5);
      INSERT_ELEMENT1(L_PARTKEY6);
      INSERT_ELEMENT1(L_PARTKEY7);
      INSERT_ELEMENT1(L_ORDERKEY1);
      INSERT_ELEMENT1(L_ORDERKEY2);
      INSERT_ELEMENT1(L_ORDERKEY3);
      INSERT_ELEMENT1(L_ORDERKEY4);
      INSERT_ELEMENT1(ALL_ATTRIBUTES);
#undef INSERT_ELEMENT1
    }

    static std::map<FILE_VERSION, std::string> versionMapping;
    if (versionMapping.size() == 0) {
#define INSERT_ELEMENT1(p) versionMapping[p] = #p
      INSERT_ELEMENT1(OTHER);
      INSERT_ELEMENT1(SORT);
      INSERT_ELEMENT1(HASHJOIN);
      INSERT_ELEMENT1(SELECT);
      INSERT_ELEMENT1(GROUP);
#undef INSERT_ELEMENT1
    }

    static std::map<PROJECTED_FIELD, std::string> projectedFieldMapping;
    if (projectedFieldMapping.size() == 0) {
#define INSERT_ELEMENT1(p) projectedFieldMapping[p] = #p
      INSERT_ELEMENT1(ALL);
      INSERT_ELEMENT1(LINEITEM_UNIFIED);
      INSERT_ELEMENT1(LINEITEM_UNIFIED_ORDER_KEY);
      INSERT_ELEMENT1(CUSTOMER_Q7);
      INSERT_ELEMENT1(LINEITEM_Q5);
      INSERT_ELEMENT1(LINEITEM_Q8);
      INSERT_ELEMENT1(LINEITEM_Q9);
      INSERT_ELEMENT1(LINEITEM_Q14);
      INSERT_ELEMENT1(LINEITEM_Q17);
      INSERT_ELEMENT1(LINEITEM_Q18);
      INSERT_ELEMENT1(ORDER_Q7_SORTED);
      INSERT_ELEMENT1(ORDER_Q12);
      INSERT_ELEMENT1(ORDER_Q18);
      INSERT_ELEMENT1(ORDER_UNIFIED_ORDER_KEY);
      INSERT_ELEMENT1(ORDER_Q13_SORTED);
      INSERT_ELEMENT1(PART_Q17_OTHER);
      INSERT_ELEMENT1(PARTSUPP_Q2);
      INSERT_ELEMENT1(PARTSUPP_UNIFIED_PARTKEY);
      INSERT_ELEMENT1(PARTSUPP_Q11);
      INSERT_ELEMENT1(PARTSUPP_Q16);
#undef INSERT_ELEMENT1
    }
    std::stringstream ss;
    ss << tableNamesMapping[table_name] << fieldsMapping[field]
       << versionMapping[version] << projectedFieldMapping[projected_field];
    return ss.str();
  }
};

//
// struct table_key_equal {
//  bool operator()(const table_key &x, const table_key &y) const {
//    return (x.version == y.version && x.table_name == y.table_name &&
//            x.field == y.field);
//  };
//};

struct table_key_hash {
  std::size_t operator()(const table_key &f) const {
    std::size_t seed = 0;
    offset_t page_offset = f.version / PAGE_SIZE_PERSISTENT;
    long short_offset = (long)page_offset;
    boost::hash_combine(seed, f.table_name);
    boost::hash_combine(seed, f.field);
    boost::hash_combine(seed, f.projected_field);
    boost::hash_combine(seed, short_offset);
    return seed;
  };
};

struct table_value {
  std::string output_name;
  unsigned int num_files;
  std::string full_output_path;
  int pins;
  FILE_CLASSIFIER fileClassifier;

  bool operator==(const table_value &other) {
    return (output_name == other.output_name) ? (num_files == other.num_files)
                                              : false;
  };

  friend std::ostream &operator<<(std::ostream &os, const table_value &c) {
    os << "output_name: [" << c.output_name << "] num_of_files: ["
       << c.num_files << "] output_full_path: [" << c.full_output_path
       << "] pins: " << c.pins << "] classifier: " << c.fileClassifier << "]";
    return os;
  }
};

struct scoreTuple {
  table_key tk;
  double score;
  double fileSize;
  double numberOfReferences;

  bool operator==(const scoreTuple &other) { return (tk == other.tk); };

  bool operator<(const scoreTuple &other) const {
    return (score < other.score);
  }

  bool operator>(const scoreTuple &other) const {
    return (score > other.score);
  }
};

struct table_classifier {
  table_classifier() {
    decisionFlag = false;
    fileClassifier = deceve::storage::NOTCLASSIFIED;
  }

  table_classifier(bool dec, FILE_CLASSIFIER fc) {
    decisionFlag = dec;
    fileClassifier = fc;
  }
  bool decisionFlag;
  deceve::storage::FILE_CLASSIFIER fileClassifier;

  bool operator==(const table_classifier &other) {
    return (decisionFlag == other.decisionFlag)
               ? (fileClassifier == other.fileClassifier)
               : false;
  };

  friend std::ostream &operator<<(std::ostream &os, const table_classifier &c) {
    os << "table_classifier->  decisionFlag: [" << c.decisionFlag
       << "] fileClassifier: [" << c.fileClassifier << "]";
    return os;
  }
};

struct table_statistics {
  size_t references;
  size_t hits;
  size_t timesPersisted;
  uint64_t cost;
  Timestamp timestamp;

  friend std::ostream &operator<<(std::ostream &os, const table_statistics &c) {
    os << "references: [" << c.references << "]"
       << " timesPersisted: [" << c.timesPersisted << "] hits: [" << c.hits;

    if (c.timesPersisted > 0) {
      os << "] cost: [" << c.cost << "]";
    }else{
      os << "] cost: [" << -1 << "]";
    }
    os << " timestamp: [" << c.timestamp << "]";
    return os;
  };
};

// maps file names to File objects
typedef std::map<std::string, File *> FileMap;
typedef std::map<table_key, table_value> global_table_map;
typedef std::map<table_key, int> global_table_counters;
typedef std::map<table_key, std::vector<std::pair<int, int> > >
    global_table_references;
typedef std::map<table_key, table_statistics> global_table_statistics;
typedef std::map<table_key, FILE_CLASSIFIER> global_table_classifier;

typedef std::pair<double, deceve::storage::table_key> ScoreKeyTuple;

struct page_info_vector {
  size_t used_pages;
  size_t allocated_pages;
  unsigned char *data;

  page_info_vector() : used_pages(0), allocated_pages(0), data(0) {}
};

struct PART_STRUCT_FORMAT {
  double P_RETAILPRICE;
  int P_PARTKEY;
  int P_SIZE;
  char P_NAME[56];
  char P_MFGR[26];
  char P_BRAND[11];
  char P_TYPE[26];
  char P_CONTAINER[11];
  char P_COMMENT[24];
};

struct SUPPLIER_STRUCT_FORMAT {
  double S_ACCTBAL;
  int S_NATIONKEY;
  int S_SUPPKEY;
  char S_NAME[26];
  char S_ADDRESS[41];
  char S_PHONE[16];
  char S_COMMENT[102];
};

struct PARTSUPP_STRUCT_FORMAT {
  double PS_SUPPLYCOST;
  int PS_PARTKEY;
  int PS_SUPPKEY;
  int PS_AVAILQTY;
  char PS_COMMENT[199];
};

struct CUSTOMER_STRUCT_FORMAT {
  double C_ACCTBAL;
  int C_CUSTKEY;
  int C_NATIONKEY;
  char C_NAME[26];
  char C_ADDRESS[41];
  char C_PHONE[16];
  char C_MKTSEGMENT[11];
  char C_COMMENT[118];
};

struct ORDER_STRUCT_FORMAT {
  char O_COMMENT[80];
  char O_ORDERPRIORITY[16];
  char O_CLERK[16];
  char O_ORDERDATE[11];
  double O_TOTALPRICE;
  int O_SHIPPRIORITY;
  int O_ORDERKEY;
  int O_CUSTKEY;
  char O_ORDERSTATUS;
};

struct LINEITEM_STRUCT_FORMAT {
  char L_COMMENT[45];
  char L_SHIPINSTRUCT[26];
  char L_SHIPDATE[11];
  char L_COMMITDATE[11];
  char L_RECEIPTDATE[11];
  char L_SHIPMODE[11];
  double L_QUANTITY;
  double L_EXTENDEDPRICE;
  double L_DISCOUNT;
  double L_TAX;
  int L_ORDERKEY;
  int L_PARTKEY;
  int L_SUPPKEY;
  int L_LINENUMBER;
  char L_RETURNFLAG;
  char L_LINESTATUS;
};

struct NATION_STRUCT_FORMAT {
  char N_COMMENT[153];
  char N_NAME[26];
  int N_NATIONKEY;
  int N_REGIONKEY;
};

struct REGION_STRUCT_FORMAT {
  char R_COMMENT[153];
  char R_NAME[26];
  int R_REGIONKEY;
};

class NullStream {
 public:
  NullStream() {}
  template <typename T>
  NullStream &operator<<(T const &) {
    return *this;
  }
};

// flags for enabling asserts, if defined asserts are disabled
#define NDEBUG

#ifdef NDEBUG
#define DEBUG(var)
#else
#define DEBUG(var) \
  { std::cout << #var << ": " << (var) << std::endl; }
#endif

// PRINT PAIR
template <typename T1, typename T2>
std::ostream &operator<<(std::ostream &out, const std::pair<T1, T2> &M) {
  out << " ( ";
  out << M.first << " , " << M.second << ")";
  return out;
}

// PRINT MAP
template <typename T1, typename T2>
std::ostream &operator<<(std::ostream &out, const std::map<T1, T2> &M) {
  out << " map#{ ";
  for (auto item : M) out << item.first << " -> " << item.second << " \n";
  out << "}";
  return out;
}

// PRINT Unordered_MAP
template <typename T1, typename T2>
std::ostream &operator<<(std::ostream &out, const std::unordered_map<T1, T2> &M) {
  out << " unordered_map#{ ";
  for (auto item : M) out << item.first << " -> " << item.second << " \n";
  out << "}";
  return out;
}

// PRINT SET
template <typename T1>
std::ostream &operator<<(std::ostream &out, const std::set<T1> &M) {
  out << " set#{ ";
  for (auto item : M) out << item << " \n";
  out << "}";
  return out;
}

// PRINT VECTOR
template <typename T1>
std::ostream &operator<<(std::ostream &out, const std::vector<T1> &M) {
  out << " list#{ ";
  for (auto item : M) out << item << " ";
  out << " \n }";
  return out;
}

// #ifndef NDEBUG
// #define BOOST_MULTI_INDEX_ENABLE_INVARIANT_CHECKING
// #define BOOST_MULTI_INDEX_ENABLE_SAFE_MODE
// #endif

//#define DEBUG
#ifdef DEBUG
#define COUT std::cout
#else
#define COUT NullStream()
#endif

//#define LOGGING
#ifdef LOGGING
#define LOG std::cout
#else
#define LOG NullStream()
#endif
}
}

#endif
