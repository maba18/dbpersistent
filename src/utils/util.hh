#ifndef __UTIL_HH__
#define __UTIL_HH__

#ifdef __linux
#include <stdint.h>
#endif

#include <sys/types.h>

// #include "pstdint.h" /* Replace with <stdint.h> if appropriate */
#undef get16bits
#if (defined(__GNUC__) && defined(__i386__)) || defined(__WATCOMC__) || \
    defined(_MSC_VER) || defined(__BORLANDC__) || defined(__TURBOC__)
#define get16bits(d) (*((const uint16_t *)(d)))
#endif

#if !defined(get16bits)
#define get16bits(d)                                \
  ((((uint32_t)(((const uint8_t *)(d))[1])) << 8) + \
   (uint32_t)(((const uint8_t *)(d))[0]))
#endif

namespace deceve {
namespace bama {

static __inline__ uint64_t rdtsc() {
  uint32_t lo, hi;
  /* We cannot use "=A", since this would use %rax on x86_64 */
  __asm__ __volatile__("rdtsc" : "=a"(lo), "=d"(hi));
  return (uint64_t)hi << 32 | lo;
}

// static inline void  delay(size_t cycles) {
//  uint64_t start = rdtsc();
//  while (rdtsc() - start < cycles) {
//  }
//}

#if defined(__clang__)
/* Clang/LLVM. ---------------------------------------------- */
static inline void __attribute__((optnone)) delay(size_t cycles) {
  uint64_t start = rdtsc();
  while (rdtsc() - start < cycles) {
  }
}

#elif defined(__GNUC__) || defined(__GNUG__)
/* GNU GCC/G++. --------------------------------------------- */
#pragma GCC push_options
#pragma GCC optimize("O0")
static inline void delay(size_t cycles) {
  uint64_t start = rdtsc();
  while (rdtsc() - start < cycles) {
  }
}
#pragma GCC pop_options

#endif

/*
 static double ticks_to_seconds(double t, double tps) {
 return t / tps;
 }
 */

static inline double ticks_per_second(size_t duration = 10) {
  uint64_t ticks = 0;
  uint64_t start = 0, end = 0;
  for (size_t i = 0; i < duration; i++) {
    start = rdtsc();
    sleep(1);
    end = rdtsc();
    ticks += (end - start);
  }
  return ((double)ticks) / duration;
}
}
}

#endif
