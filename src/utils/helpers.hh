#ifndef HELPERS_HH
#define HELPERS_HH

#include <sys/stat.h>
#include <iostream>
#include <string>
#include "types.hh"
#include <cassert>

namespace deceve { namespace storage {

inline void require(bool r, const std::string& s) {
    if (! r) {
	std::cerr << s << "\n";
	exit(42);
    }
}

inline size_t file_size(int fd) {
    struct stat st;
    fstat(fd, &st);
    return st.st_size;
}

static __inline__ uint64_t rdtsc() {
 uint32_t lo, hi;
 /* We cannot use "=A", since this would use %rax on x86_64 */
 __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
 return (uint64_t)hi << 32 | lo;
}

inline uint64_t getTimeDiff(uint64_t start, uint64_t end) {
    return end - start;
}

}}

#endif	// HELPERS_HH //:~
