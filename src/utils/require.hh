#ifndef __REQUIRE_HH__
#define __REQUIRE_HH__

#include <iostream>
#include <string>
#include <cstdlib>
#include <cassert>

namespace deceve {


  //inline void require(bool r, const std::string& s) {
  inline void require(bool r, const char* s) {
      if (! r) {
          std::cerr << s << "\n";
          ::exit(42);
      }
  }

  inline void warn(bool r, const std::string& s) {
      if (! r) std::cerr << "Warning: " << s << "\n";
  }

};

#endif	// __REQUIRE_HH__ //:~
