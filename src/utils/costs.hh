#ifndef __COSTS_HH__
#define __COSTS_HH__

namespace deceve {

struct cost {
    unsigned int read;
    unsigned int write;
    
    cost(): read(1), write(1) {}
};

};

#endif
