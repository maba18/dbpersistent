#ifndef STATISTICS_HH
#define STATISTICS_HH

#include <iostream>
#include <sstream>
#include <string>

namespace deceve {
namespace statistics {
void printRuntimeDetailsConsole(size_t dump,
                                deceve::storage::BufferManager& bm) {
  // ----------
  if (dump == 1) {
    std::cout << "PRIMARY PAGES: "
              << "\n";
    bm.getBufferPool().printPagesByTime(deceve::storage::PRIMARY);
    std::cout << "INTERMEDIATE PAGES: "
              << "\n";
    bm.getBufferPool().printPagesByTime(deceve::storage::INTERMEDIATE);
    std::cout << "FREE PAGES: "
              << "\n";
    bm.getBufferPool().printPagesByTime(deceve::storage::FREE);
    std::cout << "FREE_INTERMEDIATE PAGES: "
              << "\n";
    bm.getBufferPool().printPagesByTime(deceve::storage::FREE_INTERMEDIATE);
  }
  bm.getSM().printFiles(deceve::storage::PRIMARY);
  bm.getSM().printFiles(deceve::storage::INTERMEDIATE);
  bm.getSM().printFiles(deceve::storage::AUXILIARY);
  bm.getBufferPool().printHitRatio();
  bm.getSM().printPersistentFileCatalog();
  bm.getSM().printPesistentFileStatistics();
}


}
}

#endif
