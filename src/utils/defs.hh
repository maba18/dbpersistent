#ifndef __DEFS_HH__
#define __DEFS_HH__

#include <cstring>
#include <cstdlib>
#include <sys/types.h>
#include <unistd.h>
#include <stdint.h>
#include <iostream>
#include <mutex>

namespace deceve { namespace bama {


#ifndef PAGESIZE
static const size_t PAGE_SIZE_PERSISTENT = 4096;
#else
static const size_t PAGE_SIZE_PERSISTENT= PAGESIZE;
#endif

static inline size_t get_pagesize() { return PAGE_SIZE_PERSISTENT; }

static size_t reads = 0;
static size_t writes = 0;
static uint64_t write_delay = 0;
static uint64_t read_delay = 0;
static size_t cacheline_size = 128;


static inline size_t increment_reads(size_t i = 1) { reads += i;  return i; }
static inline size_t increment_writes(size_t i = 1) { writes += i;  return i; }
static inline void reset_reads() { reads = 0; }
static inline void reset_writes() { writes = 0; }
static inline void set_cacheline_size(size_t b) { cacheline_size = b; }
static inline void set_read_delay(uint64_t c) { read_delay = c; }
static inline void set_write_delay(uint64_t c) { write_delay = c; }

static inline unsigned char* aligned_new(size_t size) {
    unsigned char* var;
#ifdef MEMALIGN    
    if (::posix_memalign((void**) &var, 4096, size)) return 0;
#else
    if (!(var = (unsigned char*) malloc(size))) return 0;
#endif
    ::memset(var, 0, size);
    //std::cout << "allocated " << (void*) var << "\n";
    return var;
}

static inline void aligned_delete(unsigned char* p) {
    //std::cout << "deleting " << (void*) p << "\n";
    free(p);
	//delete [] p;
}

}}

#endif
