/*
 * definitions.cc
 *
 *  Created on: 29 Dec 2014
 *      Author: michail
 */

#include <stdio.h>

#include <iostream>
#include <string>

#include "../utils/global.hh"
#include "../queryplans/definitions.hh"

namespace deceve {
namespace queries {

void q1_combineStructs(q1_struct& a, const q1_struct& b) {
  a.sum_qty += b.sum_qty;
  a.sum_base_price += b.sum_base_price;
  a.sum_disc_price += b.sum_disc_price;
  a.sum_charge += b.sum_charge;
  a.count_order += 1;
  a.avg_qty = a.sum_qty / a.count_order;
  a.avg_price = a.sum_base_price / a.count_order;
  a.tmp_sum_disc += b.tmp_sum_disc;
  a.avg_disc = a.tmp_sum_disc / a.count_order;
}

std::ostream& operator<<(std::ostream& os, const q1_struct& output) {
  os << output.l_returnflag << " " << output.l_linestatus << " " << output.sum_qty << " " << output.sum_base_price
     << " " << output.sum_disc_price << " " << output.sum_charge << " " << output.avg_qty << " " << output.avg_price
     << " " << output.avg_disc << " " << output.count_order << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const q1_key& output) {
  os << output.l_returnflag << " " << output.l_linestatus << " " << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const query14_struct& output) {
  os << "l_partkey: " << output.l_partkey << " " << output.type << " " << output.extended_price << " "
     << output.discount << " " << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const q15_struct& output) {
  os << "[" << output.supplier_no << "," << output.total_revenue << "]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const q15_struct_final_output& output) {
  os << "[" << output.s_suppkey << "," << output.s_name << "," << output.s_address << "," << output.s_phone << ","
     << output.total_revenue << "]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const query3_struct_customer_order& output) {
  os << "[" << output.o_orderkey << "," << output.o_orderdate << "," << output.o_shippriority << "]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const q3_final_struct& output) {
  os << "[" << output.o_orderkey << "," << output.l_extendedprice << "," << output.l_discount << ","
     << output.o_orderdate << "," << output.o_shippriority << "]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const q3_struct_select& output) {
  os << "[" << output.o_orderkey << "," << output.revenue << "," << output.o_orderdate << "," << output.o_shippriority
     << "]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const q3_struct_group& output) {
  os << "[" << output.l_orderkey << "," << output.o_orderdate << "," << output.o_shippriority << "]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q5_JOIN1_STRUCT_REGION_NATION& output) {
  os << "[" << output.n_nationkey << "," << output.n_name << "]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q5_JOIN2_STRUCT_RESULT1_CUSTOMER& output) {
  os << "[" << output.c_customerkey << "," << output.n_nationkey << "," << output.n_name << "]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q5_JOIN3_STRUCT_RESULT2_ORDER& output) {
  os << "[" << output.o_orderkey << "," << output.n_nationkey << "," << output.n_name << "]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q5_JOIN4_STRUCT_RESULT3_ORDER& output) {
  os << "[" << output.l_suppkey << "," << output.n_nationkey << "," << output.n_name << "]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q5_JOIN5_STRUCT_RESULT4_SUPPLIER& output) {
  os << "[" << output.l_suppkey << "," << output.n_nationkey << "," << output.n_name << "]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q8_JOIN1_STRUCT_PART_LINEITEM& output) {
  os << "[part_key: " << output.l_partkey << "," << output.p_type << "]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q8_JOIN2_STRUCT_RESULT_ORDERS& output) {
  os << "[cust_key: " << output.o_custkey << "," << output.l_orderkey << "," << output.o_orderdate << "]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q8_JOIN3_STRUCT_RESULT_CUSTOMER& output) {
  os << "[nation_key: " << output.c_nationkey << ", cust_key: " << output.o_custkey << "," << output.l_discount << "]"
     << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q8_JOIN4_STRUCT_RESULT_NATION& output) {
  os << "[region_key: " << output.n_regionkey << "," << output.n_nationame << " " << output.l_extendedprice << " "
     << output.l_discount << " " << output.l_suppkey << " " << output.c_nationkey << "  " << output.o_orderdate << "]"
     << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q8_JOIN5_STRUCT_RESULT_REGION& output) {
  os << "[region_key: " << output.n_regionkey << "," << output.n_nationame << " " << output.l_extendedprice << " "
     << output.l_discount << " " << output.l_suppkey << " " << output.c_nationkey << "  " << output.o_orderdate << "]"
     << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q8_JOIN6_STRUCT_RESULT_SUPPLIER& output) {
  os << "[region_key: " << output.n_regionkey << "," << output.n_nationame << " " << output.l_extendedprice << " "
     << output.l_discount << " " << output.l_suppkey << " " << output.c_nationkey << "  " << output.o_orderdate << "]"
     << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q8_JOIN7_STRUCT_RESULT_NATION1& output) {
  os << "[region_key: " << output.n_regionkey << "," << output.n_nationame << " " << output.l_extendedprice << " "
     << output.l_discount << " " << output.l_suppkey << " " << output.c_nationkey << "  " << output.o_orderdate << "]"
     << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q9_JOIN1_STRUCT_PART_LINEITEM& output) {
  os << "Q9Join1: [part_key: " << output.l_partkey << "]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q9_PR_LINEITEM& output) {
  os << "Q9_PR_LINEITEM: [part_key: " << output.L_PARTKEY << "]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q9_JOIN2_STRUCT_RESULT_SUPPLIER& output) {
  os << "Q9Join2: [supplier_key: " << output.l_suppkey << "]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q9_JOIN3_STRUCT_RESULT_NATION& output) {
  os << "Q9Join3: [nation_name: " << output.n_name << ", o_order_key: " << output.l_orderkey << ", o_quantity: "
     << output.l_quantity << " ]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q9_JOIN4_STRUCT_RESULT_ORDER& output) {
  os << "Q9Join4: [nation_name: " << output.n_name << ", o_order_key: " << output.l_orderkey << ", o_quantity: "
     << output.l_quantity << " ]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q9_JOIN5_STRUCT_RESULT_PARTSUPP& output) {
  os << "Q9Join5: [nation_name: " << output.n_name << ", partkey: " << output.l_partkey << ", o_quantity: "
     << output.l_quantity << " ]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q10_JOIN1_STRUCT_ORDER_LINEITEM& output) {
  os << "Q10Join1: [o_custkey: " << output.o_custkey << ", l_extendedprice: " << output.l_extendedprice << " ]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q10_JOIN2_STRUCT_RESULT_CUSTOMER& output) {
  os << "Q10Join2: [o_custkey: " << output.o_custkey << ", c_name: " << output.c_name << ", c_name: "
     << output.c_address << " ]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q10_JOIN3_STRUCT_RESULT_NATION& output) {
  os << "Q10Join3: [o_nationkey: " << output.c_nationkey << ", c_name: " << output.c_name << ", o_custkey: "
     << output.o_custkey << " ]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q11_JOIN1_STRUCT_SUPPLIER_NATION& output) {
  os << "Q11Join1: [s_suppkey: " << output.s_suppkey << " ]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q11_JOIN2_STRUCT_PARTSUPP_RESULT& output) {
  os << "Q11Join2: [o_nationkey: " << output.ps_partkey << " ]" << "[ps_availqty: " << output.ps_availqty << " ]"
     << "[ps_supplycost: " << output.ps_supplycost << " ]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q11_RESULT3_STRUCT_RESULT& output) {
  os << "Q11Result: [ps_partkey: " << output.ps_partkey << " ]" << "[ps_availqty: " << output.value << " ] \n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q12_JOIN1_STRUCT_LINEITEM_ORDER& output) {
  os << "Q12Join1: [l_shipmode: " << output.l_shipmode << ", o_orderpriority: " << output.o_orderpriority << " ]"
     << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const QUERY13_STRUCT_CUSTOMER_ORDER& output) {
  os << "Q13Join1: [l_shipmode: " << output.c_custkey << " ]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q16_JOIN1_STRUCT_PART_PARTSUPP& output) {
  os << "Q16Join1: [p_partkey: " << output.p_partkey << " " << output.p_size << " ]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const TRANSFORMED_LINEITEM_STRUCT& output) {
  os << "transformed_lineitem_struct: [l_partkey: " << output.l_partkey << ", tag: " << output.tag << ", l_quantity: "
     << output.l_quantity << ", l_extended_price: " << output.l_extendedprice << " ]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q2_JOIN1_STRUCT_SUPPLIER_PARTSUPP& output) {
  os << "Q2_JOIN1_STRUCT_PART_PARTSUPP: [ps_suppkey: " << output.s_suppkey << ", ps_supplycost: "
     << output.ps_supplycost << " " << output.s_nationkey << " ]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q2_JOIN2_STRUCT_SUPPLIER_RESULT& output) {
  os << "Q2_JOIN2_STRUCT_SUPPLIER_RESULT: [s_nationkey: " << output.s_nationkey << ", ps_supplycost: "
     << output.ps_supplycost << " region" << output.n_regionkey << " ]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q2_JOIN3_STRUCT_NATION_RESULT& output) {
  os << "Q2_JOIN3_STRUCT_REGION_RESULT: [s_nationkey: " << output.s_nationkey << ", ps_supplycost: "
     << output.ps_supplycost << " region" << output.n_regionkey << " ]" << "\n";
  return os;
}

// Query7

std::ostream& operator<<(std::ostream& os, const Q7_JOIN1_STRUCT_CUSTOMER_NATION& output) {
  os << "Q7_JOIN1_STRUCT_CUSTOMER_NATION: [c_custkey: " << output.c_custkey << ", n_name: " << output.n_name << " ]"
     << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q7_JOIN2_STRUCT_ORDER_RESULT& output) {
  os << "Q7_JOIN2_STRUCT_ORDER_RESULT: [o_orderkey: " << output.o_orderkey << ", n_name: " << output.n_name << " ]"
     << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q7_JOIN3_STRUCT_LINEITEM_RESULT& output) {
  os << "Q7_JOIN3_STRUCT_LINEITEM_RESULT: [l_suppkey: " << output.l_suppkey << " , " << output.l_shipdate
     << ", n_name: " << output.n_name << " ]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q7_JOIN4_STRUCT_SUPPLIER_RESULT& output) {
  os << "Q7_JOIN4_STRUCT_SUPPLIER_RESULT: [" << output.l_shipdate << "output.n_nationkey: [" << output.n_nationkey
     << ", n_name: " << output.n_name << " ]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q7_JOIN5_STRUCT_NATION1_RESULT& output) {
  os << "Q7_JOIN5_STRUCT_NATION1_RESULT: [" << output.l_shipdate << ", n_name: " << output.n_name << " ]" << "\n";
  return os;
}

// Query3

std::ostream& operator<<(std::ostream& os, const Q3_JOIN1_STRUCT_ORDER_CUSTOMER& output) {
  os << "Q3_JOIN1_STRUCT_ORDER_CUSTOMER: o_orderdate: [" << output.o_orderdate << " o_orderkey: [" << output.o_orderkey
     << ", o_shippriority: " << output.o_shippriority << " ]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q3_JOIN2_STRUCT_LINEITEM_RESULT& output) {
  os << "Q3_JOIN2_STRUCT_LINEITEM_RESULT: revenue: " << output.l_extendedprice * (1 - output.l_discount)
     << "o_orderdate: [" << output.o_orderdate << " l_orderkey: [" << output.l_orderkey << ", o_shippriority: "
     << output.o_shippriority << " ]" << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q17_PR_LINEITEM& output) {
  os << "Q17_PROJECT_LINEITEM:  l_extendedprice" << output.L_EXTENDEDPRICE << " l_partkey: " << output.L_PARTKEY
     << "output.l_quantity: " << output.L_QUANTITY << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q18_JOIN1_STRUCT_INT_ORDER& output) {
  os << "Q18_JOIN1_STRUCT_INT_ORDER: " << output.O_CUSTKEY << " " << output.O_TOTALPRICE << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q18_JOIN2_STRUCT_CUSTOMER_RESULT& output) {
  os << "Q18_JOIN2_STRUCT_CUSTOMER_RESULT: " << output.O_CUSTKEY << " " << output.O_TOTALPRICE << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q18_JOIN3_STRUCT_RESULT_LINEITEM& output) {
  os << "Q18_JOIN3_STRUCT_CUSTOMER_RESULT: " << output.O_CUSTKEY << " " << output.O_TOTALPRICE << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q4_JOIN1_STRUCT_ORDER_LINEITEM& output) {
  os << "Q4_JOIN1_STRUCT_ORDER_LINEITEM: revenue: " << output.o_orderpriority << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q4_GROUP_STRUCT_ORDER_LINEITEM& output) {
  os << "Q4_GROUP_ORDER_LINEITEM: revenue: " << output.o_orderpriority << " count: " << output.order_count << "\n";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Q2_PR_PARTSUPP& output) {
  os << "Q2_PR_PARTSUPP: " << output.PS_SUPPKEY << " " << output.PS_SUPPLYCOST << "\n";
  return os;
}

/*
 * s_suppkey,
 s_name,
 s_address,
 s_phone,
 total_revenue
 */

// Date functions
long gday(const date& d) { /* convert date to day number */
  long y, m;

  m = (d.month + 9) % 12; /* mar=0, feb=11 */
  y = d.year - m / 10; /* if Jan/Feb, year-- */
  return y * 365 + y / 4 - y / 100 + y / 400 + (m * 306 + 5) / 10 + (d.day - 1);
}

date dtf(long d) {  //  convert day number to y,m,d format
  date pd;
  long y, ddd, mi;

  y = (10000 * d + 14780) / 3652425;
  ddd = d - (y * 365 + y / 4 - y / 100 + y / 400);
  if (ddd < 0) {
    y--;
    ddd = d - (y * 365 + y / 4 - y / 100 + y / 400);
  }
  mi = (52 + 100 * ddd) / 3060;
  pd.year = y + (mi + 2) / 12;
  pd.month = (mi + 2) % 12 + 1;
  pd.day = ddd - (mi * 306 + 5) / 10 + 1;
  return pd;
}

double fRand(double fMin, double fMax) {
  double f = static_cast<double>(rand() / RAND_MAX);
  return fMin + f * (fMax - fMin);
}

std::ostream& operator<<(std::ostream& o, const date& r) {
  o << "[";
  o << "" << r.year << ",";
  o << " " << r.month << ",";
  o << " " << r.day << "]" << "\n";
  return o;
}

class IDGenerator {
 private:
  static unsigned int s_nNextID;
  static unsigned int s_nNextID2;
  static unsigned int s_nNextID3;

 public:
  static unsigned int GetNextID() {
    return s_nNextID++;
  }
  static unsigned int GetNextNodeId() {
    return s_nNextID2++;
  }
  static unsigned int GetNextQueryId() {
    return s_nNextID3++;
  }
};

// We'll start generating IDs at 1
unsigned int IDGenerator::s_nNextID = 1;
unsigned int IDGenerator::s_nNextID2 = 1;
unsigned int IDGenerator::s_nNextID3 = 1;

std::string generateFileName(const std::string& name) {
  // Need to check that the generated file doesn't exist in the saved files
  std::stringstream ss;
  ss << name << "_" << IDGenerator::GetNextID();
  COUT << "FILENAME_GENERATED: " << ss.str() << "\n";
  return ss.str();
}

unsigned int generateNodeTreeId() {
  return IDGenerator::GetNextNodeId();
}

unsigned int generateQueryId() {
  return IDGenerator::GetNextQueryId();
}
}  // namespace queries
}  // namespace deceve
