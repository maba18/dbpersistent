/*
 * PoolNode.hh
 *
 *  Created on: Jan 20, 2015
 *      Author: maba18
 */

#ifndef POOLNODE_HH_
#define POOLNODE_HH_

#include "NodeTree.hh"
#include "QOperator.hh"

namespace deceve { namespace queries {

class Pool_Node {

public:
	QOperator *query;
	std::vector<NodeTree *> tnodes;
};
}
}

#endif /* POOLNODE_HH_ */
