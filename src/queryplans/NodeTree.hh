/*
 * NodeTree.hh
 *
 *  Created on: Jan 20, 2015
 *      Author: maba18
 */

#ifndef NODETREE_HH_
#define NODETREE_HH_

// Include General Functions
#include <iostream>
#include <set>
#include "../utils/types.hh"
#include "definitions.hh"

// Include Storage Classes
#include "../storage/BufferManager.hh"
#include "../storage/readwriters.hh"
#include "../utils/global.hh"

// Multithreading libraries
#include <mutex>
#include <thread>
#include <atomic>
#include <condition_variable>

namespace deceve {
namespace queries {

class NodeTree {
 public:
  NodeTree(sto::BufferManager* bmr, const std::string& iname = "nothing",
           NodeTree* lNode = NULL, NodeTree* rNode = NULL)
      : bm(bmr),
        node_input_name(iname),
        leftNode(lNode),
        rightNode(rNode),
        node_output_name(),
        node_left_name(),
        node_right_name(),
        tk(),
        tv(),
        left_tk(),
        right_tk(),
        left_tv(),
        right_tv(),
        id(bmr->getSM().generateUniqueNodeTreeId()),
        input_file_mode(sto::PRIMARY),
        left_file_mode(sto::PRIMARY),
        right_file_mode(sto::PRIMARY),
        input_file_path(sto::PRIMARY_PATH),
        left_file_path(sto::PRIMARY_PATH),
        right_file_path(sto::PRIMARY_PATH),
        check_PersistResult_flag(false),
        check_usePersistedResult_flag(),
        information_about_persisted_files(),
        information_about_left_persisted_file(),
        information_about_right_persisted_file(),
        result_id(),
        number_of_input_files(1),
        final_full_output_filename(){};

  NodeTree(sto::BufferManager* bmr, const std::string& lname = "nothing",
           const std::string& rname = "nothing", NodeTree* lNode = NULL,
           NodeTree* rNode = NULL)
      : bm(bmr),
        node_input_name("nothing"),
        leftNode(lNode),
        rightNode(rNode),
        node_output_name(),
        node_left_name(lname),
        node_right_name(rname),
        tk(),
        tv(),
        left_tk(),
        right_tk(),
        left_tv(),
        right_tv(),
        id(bmr->getSM().generateUniqueNodeTreeId()),
        input_file_mode(sto::PRIMARY),
        left_file_mode(sto::PRIMARY),
        right_file_mode(sto::PRIMARY),
        input_file_path(sto::PRIMARY_PATH),
        left_file_path(sto::PRIMARY_PATH),
        right_file_path(sto::PRIMARY_PATH),
        check_PersistResult_flag(false),
        check_usePersistedResult_flag(false),
        information_about_persisted_files(),
        information_about_left_persisted_file(),
        information_about_right_persisted_file(),
        result_id(0),
        number_of_input_files(1),
        final_full_output_filename() {}

  void printId() {
    COUT << "NodeId: [" << id << "] [" << tk << "] "
         << "\n";
  }

  virtual void execute();

  virtual void run(sto::BufferManager* bm, const std::string& input_name,
                   const std::string& simple_output_name) {
    (void)bm;
    (void)input_name;
    (void)simple_output_name;
  };

  virtual void run(sto::BufferManager* bm, const std::string& left_file,
                   const std::string& right_file,
                   const std::string& simple_output_name) {
    (void)bm;
    (void)left_file;
    (void)right_file;
    (void)simple_output_name;
  };

  void usePersistedResult(const sto::table_value tv, int r_id = 0);
  void useLeftPersistedResult(const sto::table_value tv, int r_id = 0);
  void useRightPersistedResult(const sto::table_value tv, int r_id = 0);
  void usePersistedResult(const sto::table_value left_tv,
                          const sto::table_value right_tv, int r_id = 0);

  // result_id is used for the join operation. It can be 0,1,2. 0 declares that
  // both files should be persisted by default.
  // 1 declare the left file only and 2 the right file only
  void persistResult(int r_id = 0);
  void persistLeftResult(const sto::table_key tk, int r_id = 0);
  void persistRightResult(const sto::table_key tk, int r_id = 0);
  void persistResult(const sto::table_key left_tk,
                     const sto::table_key right_tk, int r_id = 0);

  virtual ~NodeTree(){};

  NodeTree* getLeftNode() { return leftNode; }
  NodeTree* getRightNode() { return rightNode; }
  void setLeftNode(NodeTree* left) { leftNode = left; }
  void setRightNode(NodeTree* right) { rightNode = right; }

  template <typename T>
  void scan(const std::string& full_input_name) {
    //    COUT << "SCANNING OUTPUT \n";
    size_t counter = 0;
    deceve::bama::Reader<T> reader(bm, full_input_name,
                                   bm->getFileMode(full_input_name));
    while (reader.hasNext()) {
      std::cout << reader.nextRecord();
      counter++;
    }
    //    std::cout << "Query Finished: Final NUMBER OF ITEMS: " << counter <<
    //    "\n";
    reader.close();
  }

  template <typename T>
  void checkResult(const std::string& full_input_name) {
    size_t counter = 0;
    deceve::bama::Reader<T> reader(bm, full_input_name,
                                   bm->getFileMode(full_input_name));
    while (reader.hasNext()) {
      reader.nextRecord();
      counter++;
    }
    // COUT << "NUMBER OF ITEMS: " << counter << "\n";
    reader.close();
  }

  void buildLeftFileName(const std::string& left_file, std::string& left);
  void buildLeftFilePath(const std::string& left_file, std::string& left);
  void buildRightFileName(const std::string& right_file, std::string& right);
  void buildRightFilePath(const std::string& right_file, std::string& right);

  void addToStatistics(sto::BufferManager* bm, int persisted_result_id,
                       sto::table_key leftKey, sto::table_key rightKey);

  void deletePreviousFilesJoin(sto::BufferManager* bm, const std::string& left,
                               const std::string& right);

  void addFilesToCatalogs(sto::BufferManager* bm, int tmp_result_id,
                          const std::string& left, const std::string& right,
                          unsigned int numOfPartitionFiles,
                          const std::string& runNamePrefixOfPersistentFile);

  // Calculate costs based on estimations that the
  // database should in theory provide
  double getEstimatedCost(double candidateDirtyPages,
                          const sto::table_key& candidateTk);
  double getEstimatedCostHashJoin(double availableMemory,
                                  const sto::table_key& candidateTk);

  // given a key calculate the number of tuples
  double numberOfTuplesForFile(const sto::table_key& tmpTk,
                               double tmpManualNumberOfTuples);

  // find number of reads happening for each relation of the hashjoin
  double calculateReads(const sto::table_key& tmpTk, double fileSizeInTuples,
                        double writeFactor, size_t tupleSize,
                        const sto::table_key& candidateTk,
                        double dirtyPagesOfCandidate, size_t fileSizeInPages);

  double calculateReadsDefault(const sto::table_key& tmpTk,
                               double fileSizeInTuples, double writeFactor,
                               size_t tupleSize,
                               const sto::table_key& candidateTk,
                               size_t fileSizeInPages);

  double calculateReadsForCandidate(const sto::table_key& tmpTk,
                                    double fileSizeInTuples, double writeFactor,
                                    size_t tupleSize,
                                    const sto::table_key& candidateTk,
                                    size_t fileSizeInPages);

  double calculateReadsForMaterialisedDS(const sto::table_key& tmpTk,
                                         double fileSizeInTuples,
                                         double writeFactor, size_t tupleSize,
                                         const sto::table_key& candidateTk,
                                         size_t fileSizeInPages);

  // find final size of projected file by checking if the file
  // is already materialised
  double projectedFinalSize(const sto::table_key& tmpTk,
                            double fileSizeInTuples, double writeFactor,
                            size_t tupleSize, const sto::table_key& candidateTk,
                            double dirtyPagesOfCandidate);

  double projectedFinalSizeDefault(const sto::table_key& tmpTk,
                                   double fileSizeInTuples, double writeFactor,
                                   size_t tupleSize,
                                   const sto::table_key& candidateTk);

  double projectedFinalSizeForCandidate(const sto::table_key& tmpTk,
                                        double fileSizeInTuples,
                                        double writeFactor, size_t tupleSize,
                                        const sto::table_key& candidateTk);

  double projectedFinalSizeForMaterialisedDS(const sto::table_key& tmpTk,
                                             double fileSizeInTuples,
                                             double writeFactor,
                                             size_t tupleSize,
                                             const sto::table_key& candidateTk);

  // given a table_key and its size in tuples
  // it return the size of the projected tuple
  double getSizeOfProjectedFile(const sto::table_key& tmpTk,
                                double fileSizeInTuples, size_t tupleSize);

  void leftFileDetails(double writeFactor, size_t tupleSize,
                       size_t projectedTupleSize, double manualNumOfTuples = 0);
  void rightFileDetails(double writeFactor, size_t tupleSize,
                        size_t projectedTupleSize,
                        double manualNumOfTuples = 0);
  void outputFileDetails(double writeFactor, double tupleSize);

 public:
  sto::BufferManager* bm;
  std::string node_input_name;
  NodeTree* leftNode;
  NodeTree* rightNode;
  std::string node_output_name;
  std::string node_left_name;
  std::string node_right_name;

  // These variable are used to keep information about the table_name, fields
  // and operation that is performed on the node
  sto::table_key tk;
  sto::table_value tv;

  sto::table_key left_tk;
  sto::table_key right_tk;
  sto::table_value left_tv;
  sto::table_value right_tv;

  unsigned long id;

  sto::FileMode input_file_mode;
  sto::FileMode left_file_mode;
  sto::FileMode right_file_mode;
  sto::FilePath input_file_path;
  sto::FilePath left_file_path;
  sto::FilePath right_file_path;

  bool check_PersistResult_flag;
  bool check_usePersistedResult_flag;
  sto::table_value information_about_persisted_files;
  sto::table_value information_about_left_persisted_file;
  sto::table_value information_about_right_persisted_file;

  sto::table_key key_about_left_persisted_file;
  sto::table_key key_about_right_persisted_file;

  int result_id;
  int number_of_input_files;

  std::string final_full_output_filename;

  // estimate factors for the output of the
  // join operation
  double outputWriteFactor{0.0};
  double outputTupleSize{0.0};
  double outputTotalSizeInPages{0.0};

  //  input tuple in case of sort
  double inputWriteFactor{1};
  size_t inputTupleSize{1};
  size_t inputTupleSizeProjected{1};

  // left input
  double leftWriteFactor{1};
  size_t leftTupleSize{1};
  size_t leftTupleSizeProjected{1};

  // right input
  double rightWriteFactor{1};
  size_t rightTupleSize{1};
  size_t rightTupleSizeProjected{1};

  double manualNumberOfLeftTuples{0};
  double manualNumberOfRightTuples{0};

  double predicateLeftTotalCounter{0};
  double predicateLeftCounter{0};

  double predicateRightTotalCounter{0};
  double predicateRightCounter{0};

  // utility flags for future references
  std::set<sto::table_key> futureAppearances;
  bool isInFutureAppearances(const sto::table_key& tk);
};
}
}

#endif /* NODETREE_HH_ */
