/*
 * QueueManager.cc
 *
 *  Created on: 12 Feb 2016
 *      Author: mike
 */

#include "QueueManager.hh"
#include "QOperator.hh"
#include "../storage/BufferManager.hh"

namespace deceve {
namespace queries {

QueueManager::QueueManager() {}
QueueManager::~QueueManager() {}

void QueueManager::addQuery(QOperator *query) { queueWithTasks.push(query); }

QOperator *QueueManager::getFirstQuery() {
  QOperator *query = queueWithTasks.front();
  queueWithTasks.pop();
  return query;
}

void QueueManager::removeFirstQuery() { queueWithTasks.pop(); }

size_t QueueManager::size() { return queueWithTasks.size(); }

iterable_queue<QOperator *> *QueueManager::getQueue() {
  return &queueWithTasks;
}

// void QueueManager::analyseQueries(unsigned int &dataStructureId) {
//  std::cout << "QueueManager::analyseQueries()" << std::endl;
//  std::lock_guard<std::mutex> lockAnalyser(bm->getSM().taskQueueMutex);
//  for (iterable_queue<QOperator *>::iterator it = queueWithTasks.begin();
//       it != queueWithTasks.end(); ++it) {
//    //    QOperator *query = (*it);
//    (*it)->exportQueryDetails(dataStructureId);
//  }
//  bm->getSM().printPersistentFutureFileReferences();
//  //  bm->getStorageManager().printFutureOrderQueries();
//}

}  // namespace queries
}  // namespace deceve
