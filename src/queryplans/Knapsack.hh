/*
 * Knapsack.hh
 *
 *  Created on: Mar 4, 2015
 *      Author: maba18
 */

#ifndef KNAPSACK_HH_
#define KNAPSACK_HH_

#include <stdio.h>
#include <vector>
#include <iostream>
#include <algorithm>
#include <set>

//#define max(a,b) (a > b ? a : b)

namespace deceve {
namespace queries {

class Knapsack {
 public:
  Knapsack() {
    // matrix[100][100]  = {0};

    //    std::cout << "Initialize knapsack" << "\n";
    //
    //    matrix = new int*[x];
    //    for (int i = 0; i < x; ++i)
    //      matrix[i] = new int[y];
    //    picks = new int*[x];
    //    for (int i = 0; i < x; ++i)
    //      picks[i] = new int[y];
    //
    //    for (int i = 0; i < x; i++) {
    //      for (int j = 0; j < y; j++) {
    //        matrix[i][j] = 0;
    //      }
    //    }
    //
    //    for (int i = 0; i < x; i++) {
    //      for (int j = 0; j < y; j++) {
    //        picks[i][j] = 0;
    //      }
    //    }

  }
  ;

  ~Knapsack() {
    //    std::cout << "Delete knapsack" << "\n";
    //
    //    for (int i = 0; i < x; ++i) {
    //      delete[] matrix[i];
    //    }
    //    delete[] matrix;
    //
    //    for (int i = 0; i < x; ++i) {
    //      delete[] picks[i];
    //    }
    //    delete[] picks;
  }

 private:
  //  int** matrix;
  //  int** picks;
  //  int x;
  //  int y;

  std::set<int> result_ids;

 public:
  int maximum(int a, int b) {
    return a > b ? a : b;
  }

  //  int knapsack(int nItems, int size, std::vector<int> weights,
  //               std::vector<int> values) {
  //
  //    int i, j;
  //    for (i = 1; i <= nItems; i++) {
  //      for (j = 0; j <= size; j++) {
  //        if (weights[i - 1] <= j) {
  //          matrix[i][j] = maximum(
  //              matrix[i - 1][j],
  //              values[i - 1] + matrix[i - 1][j - weights[i - 1]]);
  //          if (values[i - 1] + matrix[i - 1][j - weights[i - 1]]
  //              > matrix[i - 1][j])
  //            picks[i][j] = 1;
  //          else
  //            picks[i][j] = -1;
  //        } else {
  //          picks[i][j] = -1;
  //          matrix[i][j] = matrix[i - 1][j];
  //        }
  //      }
  //    }
  //
  //    return matrix[nItems][size];
  //
  //  }

  std::set<int> greedyKnapsack(int nItems, int budget, std::vector<double> weights, std::vector<double> values) {
    std::vector<std::pair<double, int> > profits;

    std::set<int> resultIds;

    for (int i = 0; i < nItems; i++) {
      //      profits.push_back( { (double) values[i] / (double) weights[i], i
      //      });
      profits.push_back( {values[i], i });
    }

    std::sort(profits.begin(), profits.end(), std::greater<std::pair<double, int> >());

    for (auto &it : profits) {
      //      std::cout << it.first << " " << it.second << std::endl;
      if (weights[it.second] < budget && values[it.second] > 0) {
        budget = budget - weights[it.second];
        //        std::cout << "In: " << it.first << " " << it.second << " ,new
        //        budget: "
        //                  << budget << std::endl;
        resultIds.insert(it.second);
      }
      //      else {
      //        std::cout << "Out: " << it.first << " " << it.second <<
      //        std::endl;
      //      }
    }

    return resultIds;
  }

  //  void printPicks(int item, int size, std::vector<int> weights) {
  //
  //    std::cout << "Print knapsack results:" << "\n";
  //
  //    while (item > 0) {
  //      if (picks[item][size] == 1) {
  ////				printf("%d \n", item - 1);
  //        std::cout << item - 1 << " - ";
  //        result_ids.insert(item - 1);
  //        item--;
  //        size -= weights[item];
  //      } else {
  //        item--;
  //      }
  //    }
  //
  //    printf("\n");
  //
  //    std::cout << "End knapsack function" << "\n";
  //    return;
  //  }

  std::set<int> getResultIds() {
    return result_ids;
  }
};
}
}

#endif /* KNAPSACK_HH_ */
