/*
 * MyQuery14.hh
 *
 *  Created on: Mar 12, 2015
 *      Author: maba18
 */

#ifndef MYQUERY14_HH_
#define MYQUERY14_HH_

#include <iostream>
#include <vector>

// Include General Functions
#include "../../utils/types.hh"
#include "../../utils/global.hh"
#include "../definitions.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"

// Include base class
#include "../QueryManager.hh"

// Include query nodes
#include "../querynodes/Query14Nodes.hh"
#include "../QOperator.hh"

namespace deceve {
namespace queries {

class MyQuery14 : public QOperator {
  /*
   * select
   c_custkey,
   date,
   from
   lineitem
   where
   l_orderdate >= date '[DATE]'
   order by
   revenue desc;
   */
 public:
  MyQuery14(deceve::storage::BufferManager *bmr, QueryManager *qmr)
      : QOperator(bmr), qm(qmr) {
    tpchId = 14;

    //		node1 = new Query14Node1(bm, LINE_ITEM_PATH);
    // left file auxiliary - right file normal without filter
    node2 = new Query14Node2(bm, LINE_ITEM_PATH, PART_PATH); //streamflag true
    node3 = new Query14Node3(bm, "nothing"); //not necessary
    //		node4 = new Query14Node4(bm, "nothing");
    rootNode = node2;
    node3->leftNode = node2;
    //		node2->leftNode= node1;

    //		rootNode = node4;
    //		node4->leftNode = node3;
    //		node3->leftNode = node2;
    //		node2->leftNode = node1;
  };

  virtual ~MyQuery14() {
    //		delete node1;
    delete node2;
    delete node3;
    //		delete node4;
  };

  void run();
  // Change nodes of other queries so as to use the generated persisted files
  void notifyOtherNodes();

 private:
  QueryManager *qm;
  //	Query14Node1 *node1;
  Query14Node2 *node2;
  Query14Node3 *node3;
  //	Query14Node4 *node4;
};
}
} /* namespace deceve */

#endif /* MYQUERY14_HH_ */
