/*
 * MyQuery5.hh
 *
 *  Created on: Mar 13, 2015
 *      Author: maba18
 */

#ifndef MYQUERY5_HH_
#define MYQUERY5_HH_

#include <iostream>
#include <vector>

// Include General Functions
#include "../../utils/types.hh"
#include "../../utils/global.hh"
#include "../definitions.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"

// Include base class
#include "../QueryManager.hh"

// Include query nodes
#include "../querynodes/Query5Nodes.hh"
#include "../QOperator.hh"

namespace deceve {
namespace queries {

class MyQuery5 : public QOperator {
  /*
   * select
   n_name,
   sum(l_extendedprice * (1 - l_discount)) as revenue
   from
   customer,
   orders,
   lineitem,
   supplier,
   nation,
   region
   where
   c_custkey = o_custkey
   and l_orderkey = o_orderkey
   and l_suppkey = s_suppkey
   and c_nationkey = s_nationkey //3 Join result of 2<>customer
   and s_nationkey = n_nationkey
   and n_regionkey = r_regionkey //2 Join nation<>region
   and r_name = '[REGION]' //1 Filter this
   and o_orderdate >= date '[DATE]'
   and o_orderdate < date '[DATE]' + interval '1' year
   group by
   n_name
   order by
   revenue desc;
   */

 public:
  MyQuery5(deceve::storage::BufferManager *bmr, QueryManager *qmr)
      : QOperator(bmr), qm(qmr), region_file(REGION_PATH) {
    tpchId = 5;
    node1 = new Query5Node1(bm, region_file);
    node2 = new Query5Node2(bm, NATION_PATH, "nothing");
    node3 = new Query5Node3(bm, "nothing", CUSTOMER_PATH);  // RIGHT AUXILIARY
    //	node4 = new Query5Node4(bm, ORDERS_PATH);
    node5 = new Query5Node5(bm, "nothing", ORDERS_PATH);  // NOT AUXILIARY
    node6 = new Query5Node6(bm, "nothing",
                            LINE_ITEM_PATH);  // RIGHT AUXILIARY, PROJECTED
    node7 = new Query5Node7(bm, "nothing", SUPPLIER_PATH);
    node8 = new Query5Node8(bm, "nothing");
    node9 = new Query5Node9(bm, "nothing");

    rootNode = node7;
    node9->leftNode = node8;
    node8->leftNode = node7;
    node7->leftNode = node6;
    node6->leftNode = node5;
    node5->leftNode = node3;
    // node5->rightNode = node4;
    node3->leftNode = node2;
    node2->rightNode = node1;
    //		node3->leftNode = node2;
    //		node2->leftNode = node1;
  };

  virtual ~MyQuery5() {
    delete node1;
    delete node2;
    delete node3;
    //	delete node4;
    delete node5;
    delete node6;
    delete node7;
    delete node8;
    delete node9;
  };

  void run();
  // Change nodes of other queries so as to use the generated persisted files
  void notifyOtherNodes();

 private:
  QueryManager *qm;
  std::string region_file;
  Query5Node1 *node1;
  Query5Node2 *node2;
  Query5Node3 *node3;
  //	Query5Node4 *node4;
  Query5Node5 *node5;
  Query5Node6 *node6;
  Query5Node7 *node7;
  Query5Node8 *node8;
  Query5Node9 *node9;
};
}
} /* namespace deceve */

#endif /* MYQUERY5_HH_ */
