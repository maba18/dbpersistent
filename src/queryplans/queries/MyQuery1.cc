/*
 * MyQuery1.cc
 *
 *  Created on: 11 Mar 2015
 *      Author: michail
 */

#include "MyQuery1.hh"

namespace deceve {
namespace queries {

void MyQuery1::run() {
  std::cout << "MyQuery1 with id: " << query_id << "\n";
  tpchId = 1;
  executeQueryPlan();
}
}
} /* namespace deceve */
