/*
 * MyQuery8.cc
 *
 *  Created on: 17 Mar 2015
 *      Author: michail
 */

#include "MyQuery8.hh"

namespace deceve {
namespace queries {

void MyQuery8::run() {
  std::cout << "MyQuery8 with id: " << query_id << "\n";
  tpchId = 8;
  executeQueryPlan();
}
}
} /* namespace deceve */
