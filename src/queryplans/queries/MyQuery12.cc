/*
 * MyQuery12.cc
 *
 *  Created on: 2 Aug 2015
 *      Author: mike
 */

#include "MyQuery12.hh"

namespace deceve {
namespace queries {

void MyQuery12::run() {
  std::cout << "MyQuery12 with id: " << query_id << "\n";
  tpchId = 12;
  executeQueryPlan();
}
}
} /* namespace deceve */
