/*
 * MyQuery11.cc
 *
 *  Created on: Apr 20, 2015
 *      Author: maba18
 */

#include "MyQuery11.hh"

namespace deceve {
namespace queries {

void MyQuery11::run() {
  std::cout << "MyQuery11 with id: " << query_id << "\n";
  tpchId = 11;
  executeQueryPlan();
}
}
} /* namespace deceve */
