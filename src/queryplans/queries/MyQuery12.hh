/*
 * MyQuery12.hh
 *
 *  Created on: 2 Aug 2015
 *      Author: mike
 */

#ifndef QUERYPLANS_QUERIES_MYQUERY12_HH_
#define QUERYPLANS_QUERIES_MYQUERY12_HH_

#include <iostream>
#include <vector>

// Include General Functions
#include "../../utils/types.hh"
#include "../../utils/global.hh"
#include "../definitions.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"

// Include base class
#include "../QueryManager.hh"

// Include query nodes
#include "../querynodes/Query12Nodes.hh"
#include "../QOperator.hh"

namespace deceve {
namespace queries {

class MyQuery12 : public QOperator {
  /*
   * select
   c_custkey,
   date,
   from
   lineitem
   where
   l_orderdate >= date '[DATE]'
   order by
   revenue desc;
   */
 public:
  MyQuery12(deceve::storage::BufferManager *bmr, QueryManager *qmr)
      : QOperator(bmr), qm(qmr) {
    tpchId = 12;

    node1 = new Query12Node1(bm, LINE_ITEM_PATH, ORDERS_PATH);
    node2 = new Query12Node2(bm, "nothing");
    node3 = new Query12Node3(bm, "nothing");
    rootNode = node3;
    node3->leftNode = node2;
    node2->leftNode = node1;
  };

  virtual ~MyQuery12() {
    delete node1;
    delete node2;
    delete node3;
  };

  void run();
  void notifyOtherNodes();

 private:
  QueryManager *qm;
  Query12Node1 *node1;
  Query12Node2 *node2;
  Query12Node3 *node3;
};
}
} /* namespace deceve */

#endif /* QUERYPLANS_QUERIES_MYQUERY12_HH_ */
