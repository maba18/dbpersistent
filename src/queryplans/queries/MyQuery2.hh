/*
 * MyQuery2.hh
 *
 *  Created on: Apr 29, 2015
 *      Author: maba18
 */

#ifndef QUERYPLANS_QUERIES_MYQUERY2_HH_
#define QUERYPLANS_QUERIES_MYQUERY2_HH_

#include <iostream>
#include <vector>

// Include General Functions
#include "../../utils/types.hh"
#include "../../utils/global.hh"
#include "../definitions.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"

// Include base class
#include "../QueryManager.hh"

// Include query nodes
#include "../querynodes/Query2Nodes.hh"
#include "../QOperator.hh"

namespace deceve {
namespace queries {

class MyQuery2 : public QOperator {
  /*
   * select
   n_name,
   sum(l_extendedprice * (1 - l_discount)) as revenue
   from
   customer,
   orders,
   lineitem,
   supplier,
   nation,
   region
   where
   c_custkey = o_custkey
   and l_orderkey = o_orderkey
   and l_suppkey = s_suppkey
   and c_nationkey = s_nationkey //3 Join result of 2<>customer
   and s_nationkey = n_nationkey
   and n_regionkey = r_regionkey //2 Join nation<>region
   and r_name = '[REGION]' //1 Filter this
   and o_orderdate >= date '[DATE]'
   and o_orderdate < date '[DATE]' + interval '1' year
   group by
   n_name
   order by
   revenue desc;
   */

 public:
  MyQuery2(deceve::storage::BufferManager *bmr, QueryManager *qmr)
      : QOperator(bmr), qm(qmr) {
    tpchId = 2;
    node1 =
        new Query2Node1(bm, SUPPLIER_PATH,
                        PARTSUPP_PATH);  // Right AUXILIARY (GRACE) - CONVERT
    node2 = new Query2Node2(bm, NATION_PATH, "nothing");
    node3Select = new Query2Node3RegionSelection(bm, REGION_PATH);
    node3 = new Query2Node3(bm, "nothing", "nothing");
    node4 = new Query2Node4(bm, PARTSUPP_PATH,
                            PART_PATH);  // left AUXILIARY (GRACE) - CONVERT
    node5 = new Query2Node5(bm, SUPPLIER_PATH, "nothing");
    node6 = new Query2Node6(bm, "nothing", "nothing");

    /***
     *            node6
     *          /      \
     *      node5      node3
     *      /  \        / \
     *       node4   node2
     *                 \
     *                 node1
     */

    rootNode = node6;

    node6->leftNode = node5;
    node6->rightNode = node3;

    node5->rightNode = node4;

    node3->leftNode = node3Select;
    node3->rightNode = node2;

    node2->rightNode = node1;
  };

  virtual ~MyQuery2() {
    delete node1;
    delete node2;
    delete node3Select;
    delete node3;
    delete node4;
    delete node5;
  };

  void run();
  // Change nodes of other queries so as to use the generated persisted files
  void notifyOtherNodes();

 private:
  QueryManager *qm;
  Query2Node1 *node1;
  Query2Node2 *node2;
  Query2Node3RegionSelection *node3Select;
  Query2Node3 *node3;
  Query2Node4 *node4;
  Query2Node5 *node5;
  Query2Node6 *node6;
};
}
} /* namespace deceve */

#endif /* QUERYPLANS_QUERIES_MYQUERY2_HH_ */
