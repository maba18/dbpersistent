/*
 * MyQuery9.cc
 *
 *  Created on: Mar 17, 2015
 *      Author: maba18
 */

#include "MyQuery10.hh"

namespace deceve {
namespace queries {

void MyQuery10::run() {
  std::cout << "MyQuery10 with id: " << query_id << "\n";
  tpchId = 10;
  executeQueryPlan();
}
}
} /* namespace deceve */
