/*******************************************************************************
 * Copyright (c) 2015 Michail Basios and Stratis Viglas.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michail Basios - initial API and implementation
 *     Stratis Viglas - initial API and implementation
 *******************************************************************************/
/*
 * MyQuery15.cc
 *
 *  Created on: 13 Mar 2015
 *      Author: michail
 */

#include "MyQuery15.hh"

namespace deceve {
namespace queries {

void MyQuery15::run() {
  std::cout << "MyQuery15 with id: " << query_id << "\n";
  tpchId = 15;
  executeQueryPlan();
}
}
} /* namespace deceve */
