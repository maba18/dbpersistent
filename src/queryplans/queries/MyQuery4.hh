/*
 * MyQuery4.hh
 *
 *  Created on: 4 Jun 2015
 *      Author: mike
 */

#ifndef MYQUERY4_HH_
#define MYQUERY4_HH_

#include <iostream>
#include <vector>

// Include General Functions
#include "../../utils/types.hh"
#include "../../utils/global.hh"
#include "../definitions.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"

// Include base class
#include "../QueryManager.hh"

// Include query nodes
#include "../querynodes/Query4Nodes.hh"
#include "../QOperator.hh"

namespace deceve {
namespace queries {

class MyQuery4 : public QOperator {
  /*
   *
   *
   * select
   o_orderpriority,
   count(*) as order_count
   from
   orders
   where
   o_orderdate >= date '[DATE]'
   and o_orderdate < date '[DATE]' + interval '3' month
   and exists (
   select
   *
   from
   lineitem
   where
   l_orderkey = o_orderkey
   and l_commitdate < l_receiptdate
   )
   group by
   o_orderpriority
   order by
   o_orderpriority;
   */
 public:
  MyQuery4(deceve::storage::BufferManager *bmr, QueryManager *qmr)
      : QOperator(bmr), qm(qmr) {
    tpchId = 4;
    node1 = new Query4Node1(bm, ORDERS_PATH, LINE_ITEM_PATH);
    node2 = new Query4Node2(bm, "nothing");
    node3 = new Query4Node3(bm, "nothing");

    rootNode = node3;
    node3->leftNode = node2;
    node2->leftNode = node1;
  };

  virtual ~MyQuery4() {
    delete node1;
    delete node2;
    delete node3;
  };

  void run();
  // Change nodes of other queries so as to use the generated persisted files
  void notifyOtherNodes();

 private:
  QueryManager *qm;
  Query4Node1 *node1;
  Query4Node2 *node2;
  Query4Node3 *node3;
};
}
} /* namespace deceve*/

#endif /* MYQUERY4_HH_ */
