/*
 * MyQuery3.hh
 *
 *  Created on: 2 Jun 2015
 *      Author: mike
 */

#ifndef QUERYPLANS_QUERIES_MYQUERY3_HH_
#define QUERYPLANS_QUERIES_MYQUERY3_HH_

#include <iostream>
#include <vector>

// Include General Functions
#include "../../utils/types.hh"
#include "../../utils/global.hh"
#include "../definitions.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"

// Include base class
#include "../QueryManager.hh"

// Include query nodes
#include "../querynodes/Query3Nodes.hh"
#include "../QOperator.hh"

namespace deceve {
namespace queries {

class MyQuery3 : public QOperator {
  /*
   *
   * select
   l_orderkey,
   sum(l_extendedprice*(1-l_discount)) as revenue,
   o_orderdate,
   o_shippriority
   from
   customer,
   orders,
   lineitem
   where
   c_mktsegment = '[SEGMENT]'
   and c_custkey = o_custkey
   and l_orderkey = o_orderkey
   and o_orderdate < date '[DATE]'
   and l_shipdate > date '[DATE]'
   group by
   l_orderkey,
   o_orderdate,
   o_shippriority
   order by
   revenue desc,
   o_orderdate;
   */
 public:
  MyQuery3(deceve::storage::BufferManager *bmr, QueryManager *qmr)
      : QOperator(bmr), qm(qmr) {
    tpchId = 3;

    // node1 = new Query3Node1(bm, CUSTOMER_PATH);
    // node2 = new Query3Node2(bm, ORDERS_PATH);
    node3 = new Query3Node3(bm, ORDERS_PATH, CUSTOMER_PATH);  // NOT-AUXILIARY
    // node4 = new Query3Node4(bm, LINE_ITEM_PATH);
    node5 = new Query3Node5(bm, LINE_ITEM_PATH, "nothing");  // NOT-AUXILIARY
    node6 = new Query3Node6(bm, "nothing");
    node7 = new Query3Node7(bm, "nothing");

    rootNode = node7;
    node7->leftNode = node6;
    node6->leftNode = node5;
    //    node5->leftNode = node4;
    node5->rightNode = node3;
    // node3->rightNode = node1;
    // node3->leftNode = node2;
  };

  virtual ~MyQuery3() {
    //  delete node1;
    //  delete node2;
    delete node3;
    //  delete node4;
    delete node5;
    delete node6;
    delete node7;
  };

  void run();
  // Change nodes of other queries so as to use the generated persisted files
  void notifyOtherNodes();

 private:
  QueryManager *qm;
  //  Query3Node1 *node1;
  // Query3Node2 *node2;
  Query3Node3 *node3;
  // Query3Node4 *node4;
  Query3Node5 *node5;
  Query3Node6 *node6;
  Query3Node7 *node7;
};
}
} /* namespace deceve */

#endif /* QUERYPLANS_QUERIES_MYQUERY3_HH_ */
