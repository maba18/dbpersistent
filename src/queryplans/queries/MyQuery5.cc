/*
 * MyQuery5.cc
 *
 *  Created on: Mar 13, 2015
 *      Author: maba18
 */

#include "MyQuery5.hh"

namespace deceve {
namespace queries {

void MyQuery5::run() {
  std::cout << "MyQuery5 with id: " << query_id << "\n";
  tpchId = 5;
  executeQueryPlan();
}
}
} /* namespace deceve */
