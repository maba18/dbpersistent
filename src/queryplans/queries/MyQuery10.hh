/*
 * MyQuery10.hh
 *
 *  Created on: 20 Mar 2015
 *      Author: michail
 */

#ifndef MYQUERY10_HH_
#define MYQUERY10_HH_

#include <iostream>
#include <vector>

// Include General Functions
#include "../../utils/types.hh"
#include "../../utils/global.hh"
#include "../definitions.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"

// Include base class
#include "../QueryManager.hh"

// Include query nodes
#include "../querynodes/Query10Nodes.hh"
#include "../QOperator.hh"

namespace deceve {
namespace queries {

class MyQuery10 : public QOperator {
  /*
   * Return only first 20 rows
   select
   c_custkey,
   c_name,
   sum(l_extendedprice * (1 - l_discount)) as revenue,
   c_acctbal,
   n_name,
   c_address,
   c_phone,
   c_comment
   from
   customer,
   orders,
   lineitem,
   nation
   where
   c_custkey = o_custkey
   and l_orderkey = o_orderkey
   and o_orderdate >= date '[DATE]'
   and o_orderdate < date '[DATE]' + interval '3' month
   and l_returnflag = 'R'
   and c_nationkey = n_nationkey
   group by
   c_custkey,
   c_name,
   c_acctbal,
   c_phone,
   n_name,
   c_address,
   c_comment
   order by
   revenue desc;
   */

 public:
  MyQuery10(deceve::storage::BufferManager *bmr, QueryManager *qmr)
      : QOperator(bmr), qm(qmr) {
    tpchId = 10;
    // Select Orders
    //		node0 = new Query10Node0(bm, ORDERS_PATH);
    // Select Lineitem
    //		node1 = new Query10Node1(bm, LINE_ITEM_PATH);
    //		//GraceJoin
    node2 = new Query10Node2(bm, ORDERS_PATH, LINE_ITEM_PATH);
    // MergeSortJoin
    node3 = new Query10Node3(bm, "nothing", CUSTOMER_PATH);
    // MergeSortJoin
    node4 = new Query10Node4(bm, "nothing", NATION_PATH);
    //		//sort output
    //		node4 = new Query9Node4(bm, "nothing");
    //		//MergeSortJoin
    //		node5 = new Query9Node5(bm, "nothing", ORDERS_PATH);
    //		//MergeSortJoin
    //		node6 = new Query9Node6(bm, "nothing", PARTSUPP_PATH);

    rootNode = node2;
    node4->leftNode = node3;
    node3->leftNode = node2;
    //		node2->leftNode = node0;
    //		node2->rightNode = node1;
    //		node0->leftNode= node1;
    //		node6->leftNode = node5;
    //		node5->leftNode = node4;
    //		node4->leftNode = node3;
    //		node3->leftNode = node2 ;
    //		node2->leftNode = node1;
    //		node1->leftNode = node0;
  };

  virtual ~MyQuery10() {
    //		delete node0;
    //		delete node1;
    delete node2;
    delete node3;
    delete node4;
    //		delete node5;
    //		delete node6;
  };

  void run();
  // Change nodes of other queries so as to use the generated persisted files
  void notifyOtherNodes();

 private:
  QueryManager *qm;
  //	Query10Node0 *node0;
  //	Query10Node1 *node1;
  Query10Node2 *node2;
  Query10Node3 *node3;
  Query10Node4 *node4;
  //	Query9Node5 *node5;
  //	Query9Node6 *node6;
};
}
} /* namespace deceve */

#endif /* MYQUERY10_HH_ */
