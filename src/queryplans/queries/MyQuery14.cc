/*
 * MyQuery14.cc
 *
 *  Created on: Mar 12, 2015
 *      Author: maba18
 */

#include "MyQuery14.hh"

namespace deceve {
namespace queries {

void MyQuery14::run() {
  std::cout << "MyQuery14 with id: " << query_id << "\n";
  tpchId = 14;
  executeQueryPlan();
}
}
} /* namespace deceve */
