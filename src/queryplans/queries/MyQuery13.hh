/*
 * MyQuery13.hh
 *
 *  Created on: 8 Oct 2015
 *      Author: mike
 */

#ifndef MYQUERY13_HH_
#define MYQUERY13_HH_

#include <iostream>
#include <vector>

// Include General Functions
#include "../../utils/types.hh"
#include "../../utils/global.hh"
#include "../definitions.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"

// Include base class
#include "../QueryManager.hh"

// Include query nodes
#include "../querynodes/Query13Nodes.hh"
#include "../QOperator.hh"

namespace deceve {
namespace queries {

class MyQuery13 : public QOperator {
  /*
   -- TPC-H Query 13

   select
   c_count,
   count(*) as custdist
   from
   (
   select
   c_custkey,
   count(o_orderkey) c_count
   from
   customer left outer join orders on
   c_custkey = o_custkey
   and o_comment not like '%special%requests%'
   group by
   c_custkey
   ) as c_orders
   group by
   c_count
   order by
   custdist desc,
   c_count desc
   */
 public:
  MyQuery13(deceve::storage::BufferManager *bmr, QueryManager *qmr)
      : QOperator(bmr), qm(qmr) {
    tpchId = 13;
    node1 = new Query13Node1(bm, CUSTOMER_PATH, ORDERS_PATH);
    node2 = new Query13Node2(bm, "nothing");
    rootNode = node2;
    node2->leftNode = node1;
  };

  virtual ~MyQuery13() {
    delete node1;
    delete node2;
  };

  void run();
  // Change nodes of other queries so as to use the generated persisted files
  void notifyOtherNodes();

 private:
  QueryManager *qm;
  Query13Node1 *node1;
  Query13Node2 *node2;
};
}
} /* namespace deceve */

#endif /* MYQUERY13_HH_ */
