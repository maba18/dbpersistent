/*
 * MyQuery6.cc
 *
 *  Created on: Mar 12, 2015
 *      Author: maba18
 */

#include "MyQuery6.hh"

namespace deceve {
namespace queries {

void MyQuery6::run() {
  std::cout << "MyQuery6 with id: " << query_id << "\n";
  tpchId = 6;
  executeQueryPlan();
}
}
} /* namespace deceve */
