/*
 * MyQuery1.hh
 *
 *  Created on: 11 Mar 2015
 *      Author: michail
 */

#ifndef MYQUERY1_HH_
#define MYQUERY1_HH_

#include <iostream>
#include <vector>

// Include General Functions
#include "../../utils/types.hh"
#include "../../utils/global.hh"
#include "../definitions.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"

// Include base class
#include "../QueryManager.hh"

// Include query nodes
#include "../querynodes/Query1Nodes.hh"
#include "../QOperator.hh"

namespace deceve {
namespace queries {

class MyQuery1 : public QOperator {
  /*
   * select
   c_custkey,
   date,
   from
   lineitem
   where
   l_orderdate >= date '[DATE]'
   order by
   revenue desc;
   */
  /*
   * Query plan:
   *
   * result
   *    | 24
   *  sort()
   *    | 24
   *  group()
   *    | 3M records
   * scan(lineitem)
   *
   */
 public:
  MyQuery1(deceve::storage::BufferManager *bmr, QueryManager *qmr)
      : QOperator(bmr), qm(qmr), lineitem_file(LINE_ITEM_PATH) {
    tpchId = 1;

    node1 = new Query1Node1(bm, lineitem_file);  // full table scan
    //	node2 = new Query1Node2(bm, "nothing");
    node3 = new Query1Node3(bm, "nothing");
    node4 = new Query1Node4(bm, "nothing");
    // lineitem.table
    //    rootNode = node4;
    node4->leftNode = node3;
    node3->leftNode = node1;
    // node2->leftNode = node1;

    rootNode = node1;
  };

  virtual ~MyQuery1() {
    delete node1;
    //	delete node2;
    delete node3;
    delete node4;
  };

  void run();
  // Change nodes of other queries so as to use the generated persisted files
  void notifyOtherNodes();

 private:
  QueryManager *qm;
  std::string lineitem_file;
  Query1Node1 *node1;
  //	Query1Node2 *node2;
  Query1Node3 *node3;
  Query1Node4 *node4;
};
}
} /* namespace deceve */

#endif /* MYQUERY1_HH_ */
