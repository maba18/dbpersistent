/*
 * MyQuery13.cc
 *
 *  Created on: 8 Oct 2015
 *      Author: mike
 */

#include "MyQuery13.hh"

namespace deceve {
namespace queries {

void MyQuery13::run() {
  std::cout << "MyQuery13 with id: " << query_id << "\n";
  tpchId = 13;
  executeQueryPlan();
}
}
} /* namespace deceve */
