/*
 * MyQuery18.cc
 *
 *  Created on: 14 Oct 2015
 *      Author: mike
 */

#include "MyQuery19.hh"

namespace deceve {
namespace queries {

void MyQuery19::run() {
  std::cout << "MyQuery19 with id: " << query_id << "\n";
  tpchId = 19;
  executeQueryPlan();
}
}
} /* namespace deceve */
