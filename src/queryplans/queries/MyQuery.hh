/*
 * MyQuery.hh
 *
 *  Created on: Jan 20, 2015
 *      Author: maba18
 */

#ifndef MYQUERY_HH_
#define MYQUERY_HH_

#include <iostream>
#include <vector>

// Include General Functions
#include "../../utils/types.hh"
#include "../../utils/global.hh"
#include "../definitions.hh"


// Include base class
#include "../QueryManager.hh"

// Include query nodes
#include "../querynodes/MyQueryNode1.hh"
#include "../querynodes/MyQueryNode2.hh"
#include "../QOperator.hh"

namespace deceve {

namespace storage{
  class BufferManager;
}

namespace queries {

class MyQuery : public QOperator {
  /*
   * select
   c_custkey,
   date,
   from
   lineitem
   where
   l_orderdate >= date '[DATE]'
   order by
   revenue desc;
   */

 public:
  MyQuery(deceve::storage::BufferManager *bmr, QueryManager *qmr)
      : QOperator(bmr), qm(qmr), lineitem_file(LINE_ITEM_PATH) {
    //    node1 = new MyQueryNode2(bm, LINE_ITEM_PATH);
    //		node2 = new MyQueryNode3(bm, PART_PATH);

    nodeWrite = new Query0NodeWrite(bm, "test.table");

    rootNode = nodeWrite;
    //		node1->leftNode = node2;
  };

  virtual ~MyQuery() {
    delete nodeWrite;
    //		delete node2;
  };

  void run();

  // Change nodes of other queries so as to use the generated persisted files
  void notifyOtherNodes();

  //	void printQueryPlan(NodeTree *t, int indent=0);

 private:
  QueryManager *qm;
  std::string lineitem_file;
  Query0NodeWrite *nodeWrite;
  //  MyQueryNode2 *node1;

  //	MyQueryNode3 *node2;
};
}
}

#endif /* MYQUERY_HH_ */
