/*
 * MyQuery17.hh
 *
 *  Created on: Mar 31, 2015
 *      Author: maba18
 */

#ifndef MYQUERY17_HH_
#define MYQUERY17_HH_

#include <iostream>
#include <vector>

// Include General Functions
#include "../../utils/types.hh"
#include "../../utils/global.hh"
#include "../definitions.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"

// Include base class
#include "../QueryManager.hh"

// Include query nodes
#include "../querynodes/Query17Nodes.hh"
#include "../QOperator.hh"

namespace deceve {
namespace queries {

class MyQuery17 : public QOperator {
  /*
   *
   * MAP-REDUCE QUERY
   */
 public:
  MyQuery17(deceve::storage::BufferManager *bmr, QueryManager *qmr)
      : QOperator(bmr), qm(qmr) {
    tpchId = 17;
    node1 = new Query17Node1(bm, PART_PATH, LINE_ITEM_PATH);

    //		node1 = new Query17Node1MapReduce(bm, LINE_ITEM_PATH);
    //		node2 = new Query17Node2MapReduce(bm, LINE_ITEM_PATH,
    // PART_PATH);
    //		node3 = new Query17Node3MapReduce(bm, LINE_ITEM_PATH);
    //		rootNode = node2;
    //		node2->leftNode=node1;
    rootNode = node1;
//    node1->rightNode=node00;


  };

  virtual ~MyQuery17() {
    delete node1;
    //		delete node2;
    //		delete node3;
  };

  void run();
  // Change nodes of other queries so as to use the generated persisted files
  void notifyOtherNodes();

 private:
  QueryManager *qm;
  Query17Node1 *node1;
  //	Query17Node1MapReduce *node1;
  //	Query17Node2MapReduce *node2;
  //	Query17Node3MapReduce *node3;
};
}
} /* namespace deceve */
#endif /* MYQUERY17_HH_ */
