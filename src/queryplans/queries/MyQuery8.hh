/*
 * MyQuery8.hh
 *
 *  Created on: 17 Mar 2015
 *      Author: michail
 */

#ifndef MYQUERY8_HH_
#define MYQUERY8_HH_

#include <iostream>
#include <vector>

// Include General Functions
#include "../../utils/types.hh"
#include "../../utils/global.hh"
#include "../definitions.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"

// Include base class
#include "../QueryManager.hh"

// Include query nodes
#include "../querynodes/Query8Nodes.hh"
#include "../QOperator.hh"

namespace deceve {
namespace queries {

class MyQuery8 : public QOperator {
  /*
   *
   * select
   o_year,
   sum(case
   when nation = '[NATION]'
   then volume
   else 0
   end) / sum(volume) as mkt_share
   from (
   select
   extract(year from o_orderdate) as o_year,
   l_extendedprice * (1-l_discount) as volume,
   n2.n_name as nation
   from
   part,
   supplier,
   lineitem,
   orders,
   customer,
   nation n1,
   nation n2,
   region
   where
   p_partkey = l_partkey
   and s_suppkey = l_suppkey
   and l_orderkey = o_orderkey
   and o_custkey = c_custkey
   and c_nationkey = n1.n_nationkey
   and n1.n_regionkey = r_regionkey
   and r_name = '[REGION]'
   and s_nationkey = n2.n_nationkey
   and o_orderdate between date '1995-01-01' and date '1996-12-31'
   and p_type = '[TYPE]'
   ) as all_nations
   group by
   o_year
   order by
   o_year;
   */

 public:
  MyQuery8(deceve::storage::BufferManager *bmr, QueryManager *qmr)
      : QOperator(bmr), qm(qmr) {
    tpchId = 8;
    //		node0 = new Query8Node0(bm, PART_PATH);
    node1 = new Query8Node1(bm, PART_PATH,
                            LINE_ITEM_PATH);  // Left Part not auxiliary
    //		node2 = new Query8Node2(bm, ORDERS_PATH);
    node3 = new Query8Node3(bm, "nothing",
                            ORDERS_PATH);  // Right Part not auxiliary
    node4 = new Query8Node4(bm, "nothing", CUSTOMER_PATH);
    node5 = new Query8Node5(bm, "nothing", NATION_PATH);
    node6 = new Query8Node6(bm, REGION_PATH);
    node7 = new Query8Node7(bm, "nothing", "nothing");
    node8 = new Query8Node8(bm, "nothing", SUPPLIER_PATH);
    node9 = new Query8Node9(bm, "nothing", NATION_PATH);
    node10 = new Query8Node10(bm, "nothing");
    node11 = new Query8Node11(bm, "nothing");
    rootNode = node8;
    // rootNode = node4;
    node11->leftNode = node10;
    node10->leftNode = node9;
    node9->leftNode = node8;
    node8->leftNode = node7;
    node7->leftNode = node5;
    node7->rightNode = node6;
    node5->leftNode = node4;
    node4->leftNode = node3;
    node3->leftNode = node1;
    //		node3->rightNode = node2;
    //		node1->leftNode = node0;
  };

  virtual ~MyQuery8() {
    //		delete node0;
    delete node1;
    //		delete node2;
    delete node3;
    delete node4;
    delete node5;
    delete node6;
    delete node7;
    delete node8;
    delete node9;
    delete node10;
    delete node11;
  };

  void run();
  // Change nodes of other queries so as to use the generated persisted files
  void notifyOtherNodes();

 private:
  QueryManager *qm;
  //	Query8Node0 *node0;
  Query8Node1 *node1;
  //	Query8Node2 *node2;
  Query8Node3 *node3;
  Query8Node4 *node4;
  Query8Node5 *node5;
  Query8Node6 *node6;
  Query8Node7 *node7;
  Query8Node8 *node8;
  Query8Node9 *node9;
  Query8Node10 *node10;
  Query8Node11 *node11;
};
}
} /* namespace deceve */

#endif /* MYQUERY8_HH_ */
