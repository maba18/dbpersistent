/*
 * MyQuery.cc
 *
 *  Created on: Jan 20, 2015
 *      Author: maba18
 */

#include "MyQueryJoin.hh"

namespace deceve {
namespace queries {

void MyQueryJoin::run() {
  std::cout << "Start MyQueryJoin Execution "
            << "\n";
  tpchId = 20;
  executeQueryPlan();
}

void MyQueryJoin::notifyOtherNodes() {}
}
}
