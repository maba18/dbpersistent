/*
 * MyQuery3.cc
 *
 *  Created on: 2 Jun 2015
 *      Author: mike
 */

#include "MyQuery3.hh"

namespace deceve {
namespace queries {

void MyQuery3::run() {
  std::cout << "MyQuery3 with id: " << query_id << "\n";
  tpchId = 3;
  executeQueryPlan();
}
}
} /* namespace deceve */
