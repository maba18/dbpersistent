/*
 * MyQuery18.cc
 *
 *  Created on: 14 Oct 2015
 *      Author: mike
 */

#include "MyQuery18.hh"

namespace deceve {
namespace queries {

void MyQuery18::run() {
  std::cout << "MyQuery18 with id: " << query_id << "\n";
  tpchId = 18;
  executeQueryPlan();
}
}
} /* namespace deceve */
