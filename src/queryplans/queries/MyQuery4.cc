/*
 * MyQuery4.cc
 *
 *  Created on: 4 Jun 2015
 *      Author: mike
 */

#include "MyQuery4.hh"

namespace deceve {
namespace queries {

void MyQuery4::run() {
  std::cout << "MyQuery4 with id: " << query_id << "\n";
  tpchId = 4;
  executeQueryPlan();
}
}
}
