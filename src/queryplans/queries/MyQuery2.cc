/*
 * MyQuery2.cc
 *
 *  Created on: Apr 29, 2015
 *      Author: maba18
 */

#include "MyQuery2.hh"

namespace deceve {
namespace queries {

void MyQuery2::run() {
  std::cout << "MyQuery2 with id: " << query_id << "\n";
  tpchId = 2;
  executeQueryPlan();
}
}
} /* namespace deceve */
