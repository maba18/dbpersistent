/*
 * MyQuery15.hh
 *
 *  Created on: 13 Mar 2015
 *      Author: michail
 */

#ifndef MYQUERY15_HH_
#define MYQUERY15_HH_

#include <iostream>
#include <vector>

// Include General Functions
#include "../../utils/types.hh"
#include "../../utils/global.hh"
#include "../definitions.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"

// Include base class
#include "../QueryManager.hh"

// Include query nodes
#include "../querynodes/Query15Nodes.hh"
#include "../QOperator.hh"

namespace deceve {
namespace queries {

class MyQuery15 : public QOperator {
  /*
   * select
   c_custkey,
   date,
   from
   lineitem
   where
   l_orderdate >= date '[DATE]'
   order by
   revenue desc;
   */
 public:
  MyQuery15(deceve::storage::BufferManager *bmr, QueryManager *qmr)
      : QOperator(bmr), qm(qmr) {
    tpchId = 15;

    node1 = new Query15Node1(bm, LINE_ITEM_PATH);
    node2 = new Query15Node2(bm, "nothing");
    node3 = new Query15Node3(bm, SUPPLIER_PATH, "nothing");
    node4 = new Query15Node4(bm, "nothing");
    rootNode = node4;
    node4->leftNode = node3;
    node3->rightNode = node2;
    node2->leftNode = node1;
  };

  virtual ~MyQuery15() {
    delete node1;
    delete node2;
    delete node3;
    delete node4;
  };

  void run();
  // Change nodes of other queries so as to use the generated persisted files
  void notifyOtherNodes();

 private:
  QueryManager *qm;
  Query15Node1 *node1;
  Query15Node2 *node2;
  Query15Node3 *node3;
  Query15Node4 *node4;
};
}
} /* namespace deceve */

#endif /* MYQUERY15_HH_ */
