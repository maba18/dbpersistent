/*
 * MyQuery.cc
 *
 *  Created on: Jan 20, 2015
 *      Author: maba18
 */

#include "MyQuery.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"

namespace deceve {
namespace queries {

void MyQuery::run() {
  std::cout << "MyQuery with id: " << query_id << "\n";
  executeQueryPlan();
}

void MyQuery::notifyOtherNodes() {}
}
}
