/*
 * MyQuery7.hh
 *
 *  Created on: 2 Jun 2015
 *      Author: mike
 */

#ifndef MYQUERY7_HH_
#define MYQUERY7_HH_

#include <iostream>
#include <vector>

// Include General Functions
#include "../../utils/types.hh"
#include "../../utils/global.hh"
#include "../definitions.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"

// Include base class
#include "../QueryManager.hh"

// Include query nodes
#include "../querynodes/Query7Nodes.hh"
#include "../QOperator.hh"

namespace deceve {
namespace queries {

class MyQuery7 : public QOperator {
  /*
   * select
   n_name,
   sum(l_extendedprice * (1 - l_discount)) as revenue
   from
   customer,
   orders,
   lineitem,
   supplier,
   nation,
   region
   where
   c_custkey = o_custkey
   and l_orderkey = o_orderkey
   and l_suppkey = s_suppkey
   and c_nationkey = s_nationkey //3 Join result of 2<>customer
   and s_nationkey = n_nationkey
   and n_regionkey = r_regionkey //2 Join nation<>region
   and r_name = '[REGION]' //1 Filter this
   and o_orderdate >= date '[DATE]'
   and o_orderdate < date '[DATE]' + interval '1' year
   group by
   n_name
   order by
   revenue desc;
   */

 public:
  MyQuery7(deceve::storage::BufferManager *bmr, QueryManager *qmr)
      : QOperator(bmr), qm(qmr) {
    tpchId = 7;
    node1 = new Query7Node1(bm, NATION_PATH);
    node2 = new Query7Node2(bm, CUSTOMER_PATH, "nothing");
    node3 = new Query7Node3(bm, ORDERS_PATH, "nothing");
    //    node4 = new Query7Node4(bm, LINE_ITEM_PATH);
    node5 = new Query7Node5(bm, LINE_ITEM_PATH, "nothing");  // Not AUXILIARY
    node6 = new Query7Node6(bm, SUPPLIER_PATH, "nothing");
    node7 = new Query7Node7(bm, "nothing");
    node8 = new Query7Node8(bm, NATION_PATH, node1->generatedNationName);
    node9 = new Query7Node9(bm, "nothing", "nothing");
    node10 = new Query7Node10(bm, "nothing");

    rootNode = node9;
    node10->leftNode = node9;
    node9->leftNode = node8;
    node9->rightNode = node7;
    node7->leftNode = node6;
    node6->rightNode = node5;
    //    node5->leftNode = node4;
    node5->rightNode = node3;
    node3->rightNode = node2;
    node2->rightNode = node1;
  };

  virtual ~MyQuery7() {
    delete node1;
    delete node2;
    delete node3;
    //    delete node4;
    delete node5;
    delete node6;
    delete node7;
    delete node8;
    delete node9;
    delete node10;
  };

  void run();
  // Change nodes of other queries so as to use the generated persisted files
  void notifyOtherNodes();

 private:
  QueryManager *qm;
  Query7Node1 *node1;
  Query7Node2 *node2;
  Query7Node3 *node3;
  //  Query7Node4 *node4;
  Query7Node5 *node5;
  Query7Node6 *node6;
  Query7Node7 *node7;
  Query7Node8 *node8;
  Query7Node9 *node9;
  Query7Node10 *node10;
};
}
} /* namespace deceve */

#endif /* MYQUERY7_HH_ */
