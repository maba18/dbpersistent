/*
 * MyQuery7.cc
 *
 *  Created on: 2 Jun 2015
 *      Author: mike
 */

#include "MyQuery7.hh"

namespace deceve {
namespace queries {

void MyQuery7::run() {
  std::cout << "MyQuery7 with id: " << query_id << "\n";
  tpchId = 7;
  executeQueryPlan();
}
}
} /* namespace deceve */
