/*
 * MyQuery11.hh
 *
 *  Created on: Apr 20, 2015
 *      Author: maba18
 */

#ifndef QUERYPLANS_QUERIES_MYQUERY11_HH_
#define QUERYPLANS_QUERIES_MYQUERY11_HH_

#include <iostream>
#include <vector>

// Include General Functions
#include "../../utils/types.hh"
#include "../../utils/global.hh"
#include "../definitions.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"

// Include base class
#include "../QueryManager.hh"

// Include query nodes
#include "../querynodes/Query11Nodes.hh"
#include "../QOperator.hh"

namespace deceve {
namespace queries {

class MyQuery11 : public QOperator {
  /*
   * select
   ps_partkey,
   sum(ps_supplycost * ps_availqty) as value
   from
   partsupp,
   supplier,
   nation
   where
   ps_suppkey = s_suppkey
   and s_nationkey = n_nationkey
   and n_name = '[NATION]'
   group by
   ps_partkey having
   sum(ps_supplycost * ps_availqty) > (
   select
   sum(ps_supplycost * ps_availqty) * [FRACTION]
   from
   partsupp,
   supplier,
   nation
   where
   ps_suppkey = s_suppkey
   and s_nationkey = n_nationkey
   and n_name = '[NATION]'
   )
   order by
   value desc;
   */

 public:
  MyQuery11(deceve::storage::BufferManager *bmr, QueryManager *qmr)
      : QOperator(bmr), qm(qmr) {
    tpchId = 11;

    // Select Nation
    node0 = new Query11Node0(bm, NATION_PATH);
    // GraceJoin
    node1 = new Query11Node1(bm, SUPPLIER_PATH, "nothing");
    // GraceJoin
    node2 = new Query11Node2(bm, PARTSUPP_PATH, "nothing"); //AUXILIARY LEFT

    // Select Nation
    node4 = new Query11Node0(bm, NATION_PATH);
    // GraceJoin
    node5 = new Query11Node1(bm, SUPPLIER_PATH, "nothing");
    // GraceJoin
    node6 = new Query11Node2(bm, PARTSUPP_PATH, "nothing");
    // Sum
    node7 = new Query11Node3(bm, "nothing");

    // Sum
    node3 = new Query11Node3(bm, "nothing");

    // Sort Final Results
    node8 = new Query11Node4(bm, "nothing");

    rootNode = node8;

    node8->leftNode = node3;

    node3->leftNode = node2;
    node2->rightNode = node1;
    node1->rightNode = node0;

    //--------------
    node3->rightNode = node7;
    node7->leftNode = node6;
    node6->rightNode = node5;
    node5->rightNode = node4;
  };

  virtual ~MyQuery11() {
    delete node0;
    delete node1;
    delete node2;
    delete node3;
    delete node4;
    delete node5;
    delete node6;
    delete node7;
    delete node8;
  };

  void run();
  // Change nodes of other queries so as to use the generated persisted files
  void notifyOtherNodes();

 private:
  QueryManager *qm;
  Query11Node0 *node0;
  Query11Node1 *node1;
  Query11Node2 *node2;
  Query11Node3 *node3;
  Query11Node0 *node4;
  Query11Node1 *node5;
  Query11Node2 *node6;
  Query11Node3 *node7;
  Query11Node4 *node8;
};
}
} /* namespace deceve */

#endif /* QUERYPLANS_QUERIES_MYQUERY11_HH_ */
