/*
 * MyQuery18.hh
 *
 *  Created on: 14 Oct 2015
 *      Author: mike
 */

#ifndef QUERYPLANS_QUERIES_MYQUERY18_HH_
#define QUERYPLANS_QUERIES_MYQUERY18_HH_

#include <iostream>
#include <vector>

// Include General Functions
#include "../../utils/types.hh"
#include "../../utils/global.hh"
#include "../definitions.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"

// Include base class
#include "../QueryManager.hh"

// Include query nodes
#include "../querynodes/Query18Nodes.hh"
#include "../QOperator.hh"

namespace deceve {
namespace queries {

class MyQuery18 : public QOperator {
  /*
   *
   */
 public:
  MyQuery18(deceve::storage::BufferManager *bmr, QueryManager *qmr)
      : QOperator(bmr), qm(qmr) {
    tpchId = 18;
    node0 = new Query18Node0(bm, LINE_ITEM_PATH);
    node1 = new Query18Node1(bm, "nothing", ORDERS_PATH);
    node2 = new Query18Node2(bm, CUSTOMER_PATH, "nothing");
    node3 = new Query18Node3(bm, "nothing", LINE_ITEM_PATH);
    node4 = new Query18Node4(bm, "nothing");

    rootNode = node3;
    node4->leftNode = node3;
    node3->leftNode = node2;
    node2->rightNode = node1;
    node1->leftNode = node0;
  };

  virtual ~MyQuery18() {
    delete node0;
    delete node1;
    delete node2;
    delete node3;
    delete node4;
  };

  void run();
  // Change nodes of other queries so as to use the generated persisted files
  void notifyOtherNodes();

 private:
  QueryManager *qm;
  Query18Node0 *node0;
  Query18Node1 *node1;
  Query18Node2 *node2;
  Query18Node3 *node3;
  Query18Node4 *node4;
};
}
} /* namespace deceve */

#endif /* QUERYPLANS_QUERIES_MYQUERY18_HH_ */
