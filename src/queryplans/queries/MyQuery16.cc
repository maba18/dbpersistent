
#include "MyQuery16.hh"

namespace deceve {
namespace queries {

void MyQuery16::run() {
  std::cout << "MyQuery16 with id: " << query_id << "\n";
  tpchId = 16;
  executeQueryPlan();
}
}
} /* namespace deceve */
