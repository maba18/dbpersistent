/*
 * MyQuery9.cc
 *
 *  Created on: Mar 17, 2015
 *      Author: maba18
 */

#include "MyQuery9.hh"

namespace deceve {
namespace queries {

void MyQuery9::run() {
  std::cout << "MyQuery9 with id: " << query_id << "\n";
  executeQueryPlan();
}
}
} /* namespace deceve */
