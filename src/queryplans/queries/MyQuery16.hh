/*
 * MyQueryJoin.hh
 *
 *  Created on: Jan 23, 2015
 *      Author: maba18
 */

#ifndef MYQUERY16_HH_
#define MYQUERY16_HH_

#include <iostream>
#include <vector>

// Include General Functions
#include "../../utils/types.hh"
#include "../../utils/global.hh"
#include "../definitions.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"
#include "../querynodes/GraceJoinExample.hh"

// Include base class
#include "../QueryManager.hh"

// Include query nodes for join
#include "../querynodes/Query16Nodes.hh"
#include "../QOperator.hh"

namespace deceve {
namespace queries {

class MyQuery16 : public QOperator {
 public:
  MyQuery16(deceve::storage::BufferManager *bmr, QueryManager *qmr)
      : QOperator(bmr), qm(qmr) {
    tpchId = 16;
    node1 = new Query16Node1(bm, PART_PATH, PARTSUPP_PATH);
    rootNode = node1;
  };

  virtual ~MyQuery16() { delete node1; };

  void run();

  // Change nodes of other queries so as to use the generated persisted files
  void notifyOtherNodes();

 private:
  QueryManager *qm;
  Query16Node1 *node1;
};
}
}

#endif /* MYQUERY16_HH_ */
