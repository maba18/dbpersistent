/*
 * MyQuery18.hh
 *
 *  Created on: 14 Oct 2015
 *      Author: mike
 */

#ifndef QUERYPLANS_QUERIES_MYQUERY19_HH_
#define QUERYPLANS_QUERIES_MYQUERY19_HH_

#include <iostream>
#include <vector>

// Include General Functions
#include "../../utils/types.hh"
#include "../../utils/global.hh"
#include "../definitions.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"

// Include base class
#include "../QueryManager.hh"

// Include query nodes
#include "../querynodes/Query19Nodes.hh"
#include "../QOperator.hh"

namespace deceve {
namespace queries {

class MyQuery19 : public QOperator {
  /*
   *
   */
 public:
  MyQuery19(deceve::storage::BufferManager *bmr, QueryManager *qmr)
      : QOperator(bmr), qm(qmr) {
    tpchId = 19;
    node1 = new Query19Node1(bm, PART_PATH, LINE_ITEM_PATH);

    rootNode = node1;
  };

  virtual ~MyQuery19() { delete node1; };

  void run();
  // Change nodes of other queries so as to use the generated persisted files
  void notifyOtherNodes();

 private:
  QueryManager *qm;
  Query19Node1 *node1;
};
}
} /* namespace deceve */

#endif /* QUERYPLANS_QUERIES_MYQUERY19_HH_ */
