/*
 * MyQuery9.hh
 *
 *  Created on: Mar 17, 2015
 *      Author: maba18
 */

#ifndef MYQUERY9_HH_
#define MYQUERY9_HH_

#include <iostream>
#include <vector>

// Include General Functions
#include "../../utils/types.hh"
#include "../../utils/global.hh"
#include "../definitions.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"

// Include base class
#include "../QueryManager.hh"

// Include query nodes
#include "../querynodes/Query9Nodes.hh"
#include "../QOperator.hh"

namespace deceve {
namespace queries {

class MyQuery9 : public QOperator {
  /*
   *
   select
   nation,
   o_year,
   sum(amount) as sum_profit
   from (
   select
   n_name as nation,
   extract(year from o_orderdate) as o_year,
   l_extendedprice * (1 - l_discount) - ps_supplycost * l_quantity as amount
   from
   part,
   supplier,
   lineitem,
   partsupp,
   orders,
   nation
   where
   s_suppkey = l_suppkey
   and ps_suppkey = l_suppkey
   and ps_partkey = l_partkey
   and p_partkey = l_partkey
   and o_orderkey = l_orderkey
   and s_nationkey = n_nationkey
   and p_name like '%[COLOR]%'
   ) as profit
   group by
   nation,
   o_year
   order by
   nation,
   o_year desc;
   */

 public:
  MyQuery9(deceve::storage::BufferManager *bmr, QueryManager *qmr)
      : QOperator(bmr), qm(qmr) {
    tpchId = 9;

    // Select Part
    //		node0 = new Query9Node0(bm, PART_PATH);
    // GraceSortJoin
    node1 =
        new Query9Node1(bm, PART_PATH, LINE_ITEM_PATH);  // Left not auxiliary
    // GraceSortJoin
    node2 = new Query9Node2(bm, "nothing", SUPPLIER_PATH); // Auxiliary
    // GraceSortJoin
    node3 = new Query9Node3(bm, "nothing", NATION_PATH);
    // sort output
    node4 = new Query9Node4(bm, "nothing");
    // MergeSortJoin
    node5 = new Query9Node5(bm, "nothing", ORDERS_PATH);
    // MergeSortJoin
    node6 = new Query9Node6(bm, "nothing", PARTSUPP_PATH);
    // Grouping
    node7 = new Query9Node7(bm, "nothing");
    // Sorting
    node8 = new Query9Node8(bm, "nothing");

    rootNode = node7;

    node8->leftNode = node7;
    node7->leftNode = node6;
    node6->leftNode = node5;
    node5->leftNode = node4;
    node4->leftNode = node3;
    node3->leftNode = node2;
    node2->leftNode = node1;
    //		node1->leftNode = node0;
  };

  virtual ~MyQuery9() {
    //		delete node0;
    delete node1;
    delete node2;
    delete node3;
    delete node4;
    delete node5;
    delete node6;
    delete node7;
    delete node8;
  };

  void run();
  // Change nodes of other queries so as to use the generated persisted files
  void notifyOtherNodes();

 private:
  QueryManager *qm;
  //	Query9Node0 *node0;
  Query9Node1 *node1;
  Query9Node2 *node2;
  Query9Node3 *node3;
  Query9Node4 *node4;
  Query9Node5 *node5;
  Query9Node6 *node6;
  Query9Node7 *node7;
  Query9Node8 *node8;
};
}
} /* namespace deceve */

#endif /* MYQUERY9_HH_ */
