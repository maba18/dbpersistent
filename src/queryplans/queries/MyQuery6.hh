/*
 * MyQuery6.hh
 *
 *  Created on: Mar 12, 2015
 *      Author: maba18
 */

#ifndef MYQUERY6_HH_
#define MYQUERY6_HH_

#include <iostream>
#include <vector>

// Include General Functions
#include "../../utils/types.hh"
#include "../../utils/global.hh"
#include "../definitions.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"

// Include base class
#include "../QueryManager.hh"

// Include query nodes
#include "../querynodes/Query6Nodes.hh"
#include "../QOperator.hh"

namespace deceve {
namespace queries {

class MyQuery6 : public QOperator {
  /*
   * select
   c_custkey,
   date,
   from
   lineitem
   where
   l_orderdate >= date '[DATE]'
   order by
   revenue desc;
   */
 public:
  MyQuery6(deceve::storage::BufferManager *bmr, QueryManager *qmr)
      : QOperator(bmr), qm(qmr), lineitem_file(LINE_ITEM_PATH) {
    tpchId = 6;
    node1 = new Query6Node1(bm, lineitem_file);
    rootNode = node1;
  };

  virtual ~MyQuery6() { delete node1; };

  void run();
  // Change nodes of other queries so as to use the generated persisted files
  void notifyOtherNodes();

 private:
  QueryManager *qm;
  std::string lineitem_file;
  Query6Node1 *node1;
};
}
} /* namespace deceve */

#endif /* MYQUERY6_HH_ */
