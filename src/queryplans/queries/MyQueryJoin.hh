/*
 * MyQueryJoin.hh
 *
 *  Created on: Jan 23, 2015
 *      Author: maba18
 */

#ifndef MYQUERYJOIN_HH_
#define MYQUERYJOIN_HH_

#include <iostream>
#include <vector>

// Include General Functions
#include "../../utils/types.hh"
#include "../../utils/global.hh"
#include "../definitions.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"
#include "../querynodes/GraceJoinExample.hh"

// Include base class
#include "../QueryManager.hh"

// Include query nodes for join
#include "../querynodes/QueryTestJoin1.hh"
#include "../QOperator.hh"

namespace deceve {
namespace queries {

class MyQueryJoin : public QOperator {
 public:
  MyQueryJoin(deceve::storage::BufferManager *bmr)
      : QOperator(bmr),

        left_file(LINE_ITEM_PATH),
        right_file(PART_PATH) {
    //		node1 = new QueryTestJoin2(bm, left_file, right_file); //
    node1 = new GraceJoinExample(bm, ORDERS_PATH, LINE_ITEM_PATH);
    node2 = new MergeJoinExample(bm, PART_PATH, LINE_ITEM_PATH);
    node3 = new HashJoinExample(bm, PART_PATH, LINE_ITEM_PATH);
    node4 = new NestedLoopExample(bm, PART_PATH, LINE_ITEM_PATH);
    rootNode = node2;
    tpchId = 20;
  };

  virtual ~MyQueryJoin() {
    //		delete node1;
    delete node1;
    delete node2;
    delete node3;
    delete node4;
  };

  void run();

  //	void analyseCurrentQueryPool();

  //	void takeDecisions();

  // Change nodes of other queries so as to use the generated persisted files
  void notifyOtherNodes();

  //	void printQueryPlan(NodeTree *t, int indent=0);

 private:
  std::string left_file;
  std::string right_file;
  GraceJoinExample *node1;
  MergeJoinExample *node2;
  HashJoinExample *node3;
  NestedLoopExample *node4;
};
}
}

#endif /* MYQUERYJOIN_HH_ */
