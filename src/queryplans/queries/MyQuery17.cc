/*
 * MyQuery17.cc
 *
 *  Created on: Mar 31, 2015
 *      Author: maba18
 */

#include "MyQuery17.hh"

namespace deceve {
namespace queries {

void MyQuery17::run() {
  std::cout << "MyQuery17 with id: " << query_id << "\n";
  tpchId = 17;
  executeQueryPlan();
}
}
} /* namespace deceve */
