/*
 * QGenerator.hh
 *
 *  Created on: 26 Feb 2016
 *      Author: mike
 */

#ifndef QUERYPLANS_QGENERATOR_HH_
#define QUERYPLANS_QGENERATOR_HH_

#include <queue>
#include <vector>
#include <map>
#include <chrono>
#include <sstream>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <mutex>

#include "../queryplans/queries/MyQuery.hh"
#include "../queryplans/queries/MyQueryJoin.hh"
#include "../queryplans/queries/MyQuery1.hh"
#include "../queryplans/queries/MyQuery2.hh"
#include "../queryplans/queries/MyQuery3.hh"
#include "../queryplans/queries/MyQuery4.hh"
#include "../queryplans/queries/MyQuery5.hh"
#include "../queryplans/queries/MyQuery6.hh"
#include "../queryplans/queries/MyQuery7.hh"
#include "../queryplans/queries/MyQuery8.hh"
#include "../queryplans/queries/MyQuery9.hh"
#include "../queryplans/queries/MyQuery10.hh"
#include "../queryplans/queries/MyQuery11.hh"
#include "../queryplans/queries/MyQuery12.hh"
#include "../queryplans/queries/MyQuery13.hh"
#include "../queryplans/queries/MyQuery14.hh"
#include "../queryplans/queries/MyQuery15.hh"
#include "../queryplans/queries/MyQuery16.hh"
#include "../queryplans/queries/MyQuery17.hh"
#include "../queryplans/queries/MyQuery18.hh"
#include "../queryplans/queries/MyQuery19.hh"

namespace deceve {

namespace storage {
// forward declaration
class BufferManager;
}
namespace queries {
// forward declaration
class QueryManager;
class QOperator;

enum GENERATOR { RANDOMLY, FROM_FILE, SKEWED };

class QGenerator {
 public:
  QGenerator(deceve::storage::BufferManager *bmr,
             deceve::queries::QueryManager *qmr, size_t numQueries,
             GENERATOR genMode);
  ~QGenerator();
  void generateQueries();
  void generateRandomQueries();
  void generateRandomSkewedQueries();
  void generateQueriesFromFile();

  deceve::queries::QOperator *getNextQuery();
  deceve::queries::QOperator *getDummyQuery();
  deceve::queries::QOperator *getQueryWithId(int qId);
  bool areThereMoreQueriesLeft();

  size_t getTotalNumberOfQueries();


 private:
  deceve::storage::BufferManager *bm;
  deceve::queries::QueryManager *qm;
  size_t totalNumberOfQueries{0};
  GENERATOR mode;
  std::queue<size_t> randomQueryIds;
  std::queue<size_t> randomPauses;
  size_t availableTpchQueries{20};
//  std::mutex queryGeneratorMutex;
};
}  // namespace queries
}  // namespace deceve

#endif /* QUERYPLANS_QGENERATOR_HH_ */
