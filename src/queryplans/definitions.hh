/*
 * definitions.hh
 *
 *  Created on: 29 Dec 2014
 *      Author: michail
 */

#ifndef DEFINITIONS_HH_
#define DEFINITIONS_HH_

#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include "../records/records.hh"
#include "../utils/types.hh"
#include "stdio.h"
#include "stdlib.h"

using namespace std;
using namespace boost;

namespace deceve {
namespace queries {

// General additional structs
struct date {
  date(int y, int m, int d) : year(y), month(m), day(d) {}

  date(const std::string& strdate) {
    char_separator<char> sep("-");
    tokenizer<char_separator<char> > tokens(strdate, sep);
    std::vector<std::string> fields;
    BOOST_FOREACH (const string& t, tokens) { fields.push_back(t); }
    year = atoi(fields[0].c_str());
    month = atoi(fields[1].c_str());
    day = atoi(fields[2].c_str());
  }

  date plus(int numOfMonths) {
    int nYear, nMonth, nDay;
    nYear = year;
    nMonth = month;
    nDay = day;

    int sumMonths = nMonth + numOfMonths;

    if (sumMonths > 12) {
      nMonth = sumMonths % 12;
      nYear = nYear + 1;
    } else {
      nMonth = month + numOfMonths;
    }

    return date(nYear, nMonth, nDay);
  }

  std::string toString() {
    std::ostringstream ss;
    ss << year;
    ss << month;
    ss << day;

    return ss.str();
  }

  date(char* data) {
    char y[5];
    char m[3];
    char d[3];

    memcpy(y, data, 4);
    y[4] = '\0';
    memcpy(m, data + 5, 2);
    m[2] = '\0';
    memcpy(d, data + 8, 3);

    //    COUT << y << "\n";
    //    COUT << m << "\n";
    //    COUT << d << "\n";

    year = atoi(y);
    month = atoi(m);
    day = atoi(d);
  }

  date() : year(2000), month(01), day(01) {}

  bool operator==(const date& rhs) const {
    return year == rhs.year && month == rhs.month && day == rhs.day;
  }

  bool operator<(const date& rhs) const {
    if (year < rhs.year) {
      return true;
    }
    if (year > rhs.year) {
      return false;
    } else {
      if (month < rhs.month) {
        return true;
      }
      if (month > rhs.month) {
        return false;
      } else {
        if (day < rhs.day) {
          return true;
        }
        if (day > rhs.day) {
          return false;
        }
      }
    }
    return false;
  }

  bool operator>(const date& rhs) const { return !(*this < rhs); }

  bool operator<=(const date& rhs) const {
    if (*this == rhs || *this < rhs) {
      return true;
    }
    return false;
  }

  bool operator>=(const date& rhs) const {
    if (*this == rhs || *this > rhs) {
      return true;
    }
    return false;
  }

  bool operator==(const std::string& data) const {
    date rhs(data);
    return year == rhs.year && month == rhs.month && day == rhs.day;
  }

  bool operator<(const std::string& data) const {
    date rhs(data);

    if (year < rhs.year) {
      return true;
    }
    if (year > rhs.year) {
      return false;
    } else {
      if (month < rhs.month) {
        return true;
      }
      if (month > rhs.month) {
        return false;
      } else {
        if (day < rhs.day) {
          return true;
        }
        if (day > rhs.day) {
          return false;
        }
      }
    }
    return false;
  }

  bool operator>(const std::string& data) const { return !(*this < data); }

  bool operator<=(const std::string& data) const {
    if (*this == data || *this < data) {
      return true;
    }
    return false;
  }

  bool operator>=(const std::string& data) const {
    if (*this == data || *this > data) {
      return true;
    }
    return false;
  }

  bool operator==(char* data) const {
    date rhs(data);
    return year == rhs.year && month == rhs.month && day == rhs.day;
  }

  bool operator<(char* data) const {
    date rhs(data);

    if (year < rhs.year) {
      return true;
    }
    if (year > rhs.year) {
      return false;
    } else {
      if (month < rhs.month) {
        return true;
      }
      if (month > rhs.month) {
        return false;
      } else {
        if (day < rhs.day) {
          return true;
        }
        if (day > rhs.day) {
          return false;
        }
      }
    }
    return false;
  }

  bool operator>(char* data) const { return !(*this < data); }

  bool operator<=(char* data) const {
    if (*this == data || *this < data) {
      return true;
    }
    return false;
  }

  bool operator>=(char* data) const {
    if (*this == data || *this > data) {
      return true;
    }
    return false;
  }

  int year;
  int month;
  int day;

  friend std::ostream& operator<<(std::ostream& o, const date& r);
};

// Date functions
long gday(const date& d); /* convert date to day number */
date dtf(long d);         /* convert day number to y,m,d format */
double fRand(double fMin, double fMax);

// Query1 updated Struct

struct testRec {
  double value1;
  double value2;
  double value3;
  double value4;
  int id;
};

// Query1 structs
struct q1_struct {
  // GroupKey
  char l_returnflag;
  char l_linestatus;
  // Remaining Variables
  double sum_qty;
  double sum_base_price;
  double sum_disc_price;
  double sum_charge;
  double avg_qty;
  double avg_price;
  double tmp_sum_disc;
  double avg_disc;
  double count_order;

  friend std::ostream& operator<<(std::ostream& os, const q1_struct& output);
};

struct q1_key {
  char l_returnflag;
  char l_linestatus;

  q1_key(const char& rf, const char& ls) {
    l_returnflag = rf;
    l_linestatus = ls;
  }
  q1_key();

  bool operator<(const q1_key& rhs) const {
    if (l_returnflag != rhs.l_returnflag) {
      return l_returnflag < rhs.l_returnflag;
    }
    if (l_linestatus != rhs.l_linestatus) {
      return l_linestatus < rhs.l_linestatus;
    }
    return true;
  }

  bool operator>(const q1_key& rhs) const { return !(*this < rhs); }

  bool operator==(const q1_key& rhs) const {
    if (l_returnflag == rhs.l_returnflag && l_linestatus == rhs.l_linestatus) {
      return true;
    }

    return false;
  }

  friend std::ostream& operator<<(std::ostream& os, const q1_key& output);
};

class q1_column_extractor {
 public:
  typedef std::string key_type;

  q1_column_extractor(size_t col) : m_column(col) { (void)m_column; }

  inline const key_type extract(const q1_struct& w) const {
    std::stringstream ss;

    ss << w.l_returnflag;
    ss << w.l_linestatus;

    return ss.str();
  }
  inline const key_type operator()(const q1_struct& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

void q1_combineStructs(q1_struct& a, const q1_struct& b);

// Query14 structs
struct query14_struct {
  int l_partkey;
  char type[26];
  double extended_price;
  double discount;
  friend std::ostream& operator<<(std::ostream& os,
                                  const query14_struct& output);
};

class column_extractor_part {
 public:
  typedef unsigned int key_type;

  column_extractor_part(size_t col) : m_column(col) {
    (void)m_column;
    (void)m_column;
  }

  inline key_type extract(const PART& w) const { return w.P_PARTKEY; }
  inline key_type operator()(const PART& w) const { return extract(w); }

 private:
  size_t m_column;
};

class column_extractor_part_size {
 public:
  typedef double key_type;

  column_extractor_part_size(size_t col) : m_column(col) { (void)m_column; }

  inline key_type extract(const PART& w) const { return w.P_RETAILPRICE; }
  inline key_type operator()(const PART& w) const { return extract(w); }

 private:
  size_t m_column;
};

class column_extractor_lineitem {
 public:
  typedef unsigned int key_type;

  column_extractor_lineitem(size_t col) : m_column(col) { (void)m_column; }

  inline key_type extract(const LINEITEM& w) const {
    switch (m_column) {
      case 0:
        return w.L_PARTKEY;
      case 1:
        return w.L_ORDERKEY;
      case 2:
        return w.L_PARTKEY;
      case 3:
        return w.L_SUPPKEY;
      case 4:
        return w.L_EXTENDEDPRICE;
      case 5:
        return w.L_DISCOUNT;
      default:
        return w.L_PARTKEY;
    }
  }
  inline key_type operator()(const LINEITEM& w) const { return extract(w); }

 private:
  size_t m_column;
};

class q14_combinator_part_line {
 public:
  typedef query14_struct record_type;

  record_type operator()(const LINEITEM& l, const PART& r) const {
    record_type output;
    //    output.type = r.P_TYPE;

    strcpy(output.type, r.P_TYPE);
    output.l_partkey = l.L_PARTKEY;
    output.discount = l.L_DISCOUNT;
    output.extended_price = l.L_EXTENDEDPRICE;

    return output;
  }
};

class my_combinator_part_lineitem {
 public:
  typedef query14_struct record_type;

  record_type operator()(const PART& l, const LINEITEM& r) const {
    record_type output;
    //    output.type = r.P_TYPE;

    strcpy(output.type, l.P_TYPE);
    output.l_partkey = r.L_PARTKEY;
    output.discount = r.L_DISCOUNT;
    output.extended_price = r.L_EXTENDEDPRICE;

    return output;
  }
};

// *********** PROJECTED FILE QUERY19 START *************

// First projected table
struct Q19_PR_PART {
  int P_PARTKEY;

  friend std::ostream& operator<<(std::ostream& os, const Q19_PR_PART& output);
};

class q19_converter_PR_PART {
 public:
  typedef Q19_PR_PART key_type;

  q19_converter_PR_PART() {}
  inline key_type convert(const PART& w) const {
    key_type localVariable;
    localVariable.P_PARTKEY = w.P_PARTKEY;
    return localVariable;
  }
  inline key_type operator()(const PART& w) const { return convert(w); }
};

class q19_extractor_PR_PART {
 public:
  typedef int key_type;

  q19_extractor_PR_PART(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q19_PR_PART& w) const { return w.P_PARTKEY; }
  inline key_type operator()(const Q19_PR_PART& w) const { return extract(w); }

 private:
  size_t m_column;
};

struct Q19_PR_LINEITEM {
  double L_EXTENDEDPRICE;
  double L_DISCOUNT;
  int L_PARTKEY;

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q19_PR_LINEITEM& output);
};

class q19_converter_PR_LINEITEM {
 public:
  typedef Q19_PR_LINEITEM key_type;

  q19_converter_PR_LINEITEM() {}
  inline key_type convert(const LINEITEM& w) const {
    key_type localVariable;
    localVariable.L_PARTKEY = w.L_PARTKEY;
    localVariable.L_EXTENDEDPRICE = w.L_EXTENDEDPRICE;
    localVariable.L_DISCOUNT = w.L_DISCOUNT;

    return localVariable;
  }
  inline key_type operator()(const LINEITEM& w) const { return convert(w); }
};

class q19_extractor_PR_LINEITEM {
 public:
  typedef int key_type;

  q19_extractor_PR_LINEITEM(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q19_PR_LINEITEM& w) const {
    return w.L_PARTKEY;
  }
  inline key_type operator()(const Q19_PR_LINEITEM& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

struct query19_struct {
  double L_EXTENDEDPRICE;
  double L_DISCOUNT;
  friend std::ostream& operator<<(std::ostream& os,
                                  const query19_struct& output);
};

// COMBINATOR
class q19_combinator_pr_part_pr_lineitem {
 public:
  typedef query19_struct record_type;

  record_type operator()(const Q19_PR_PART& l, const Q19_PR_LINEITEM& r) const {
    record_type output;

    output.L_DISCOUNT = r.L_DISCOUNT;
    output.L_EXTENDEDPRICE = r.L_EXTENDEDPRICE;
    (void)l;

    return output;
  }
};

// *********** PROJECTED FILE QUERY14 END *************

// *********** PROJECTED FILE QUERY14 START *************
struct Q14_PR_LINEITEM {
#ifdef UNIFIED
  double L_EXTENDEDPRICE;
  double L_QUANTITY;
  double L_DISCOUNT;
  int L_PARTKEY;
  int L_SUPPKEY;
  int L_ORDERKEY;
#else
  double L_EXTENDEDPRICE;
  double L_QUANTITY;
  double L_DISCOUNT;
  int L_PARTKEY;
#endif
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q14_PR_LINEITEM& output);
};

class q14_converter_PR_LINEITEM {
 public:
  typedef Q14_PR_LINEITEM key_type;

  q14_converter_PR_LINEITEM() {}
  inline key_type convert(const LINEITEM& w) const {
    key_type localVariable;
#ifdef UNIFIED
    localVariable.L_EXTENDEDPRICE = w.L_EXTENDEDPRICE;
    localVariable.L_DISCOUNT = w.L_DISCOUNT;
    localVariable.L_PARTKEY = w.L_PARTKEY;
    localVariable.L_ORDERKEY = w.L_ORDERKEY;
    localVariable.L_SUPPKEY = w.L_SUPPKEY;
    localVariable.L_DISCOUNT = w.L_DISCOUNT;
#else
    localVariable.L_PARTKEY = w.L_PARTKEY;
    localVariable.L_EXTENDEDPRICE = w.L_EXTENDEDPRICE;
    localVariable.L_QUANTITY = w.L_QUANTITY;
    localVariable.L_DISCOUNT = w.L_DISCOUNT;
#endif
    return localVariable;
  }
  inline key_type operator()(const LINEITEM& w) const { return convert(w); }
};

class q14_extractor_PR_LINEITEM {
 public:
  typedef int key_type;

  q14_extractor_PR_LINEITEM(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q14_PR_LINEITEM& w) const {
    return w.L_PARTKEY;
  }
  inline key_type operator()(const Q14_PR_LINEITEM& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// Second projected table
struct Q14_PR_PART {
  char P_TYPE[26];
  int P_PARTKEY;

  friend std::ostream& operator<<(std::ostream& os, const Q14_PR_PART& output);
};

class q14_converter_PR_PART {
 public:
  typedef Q14_PR_PART key_type;

  q14_converter_PR_PART() {}
  inline key_type convert(const PART& w) const {
    key_type localVariable;
    localVariable.P_PARTKEY = w.P_PARTKEY;
    strcpy(localVariable.P_TYPE, w.P_TYPE);
    return localVariable;
  }
  inline key_type operator()(const PART& w) const { return convert(w); }
};

class q14_extractor_PR_PART {
 public:
  typedef int key_type;

  q14_extractor_PR_PART(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q14_PR_PART& w) const { return w.P_PARTKEY; }
  inline key_type operator()(const Q14_PR_PART& w) const { return extract(w); }

 private:
  size_t m_column;
};

// COMBINATOR
class q14_combinator_pr_lineitem_pr_part {
 public:
  typedef query14_struct record_type;

  record_type operator()(const Q14_PR_LINEITEM& l, const Q14_PR_PART& r) const {
    record_type output;

    strcpy(output.type, r.P_TYPE);
    output.l_partkey = l.L_PARTKEY;
    output.discount = l.L_DISCOUNT;
    output.extended_price = l.L_EXTENDEDPRICE;

    return output;
  }
};

// *********** PROJECTED FILE QUERY14 END *************

// Query15 structs
struct q15_struct {
  int supplier_no;
  double total_revenue;
  friend std::ostream& operator<<(std::ostream& os, const q15_struct& output);
};

struct q15_struct_final_output {
  int s_suppkey;
  char s_name[26];
  char s_address[41];
  char s_phone[16];
  double total_revenue;
  friend std::ostream& operator<<(std::ostream& os,
                                  const q15_struct_final_output& output);
};

class column_extractor_supplier {
 public:
  typedef unsigned int key_type;

  column_extractor_supplier(size_t col) : m_column(col) { (void)m_column; }

  inline key_type extract(const SUPPLIER& w) const {
    return w.S_SUPPKEY;

    if (m_column == 0) {
      return w.S_SUPPKEY;
    } else if (m_column == 1) {
      return w.S_NATIONKEY;
    } else {
      return w.S_SUPPKEY;
    }
  }
  inline key_type operator()(const SUPPLIER& w) const { return extract(w); }

 private:
  size_t m_column;
};

class q15_column_extractor_revenue {
 public:
  typedef unsigned int key_type;

  q15_column_extractor_revenue(size_t col) : m_column(col) { (void)m_column; }

  inline key_type extract(const q15_struct& w) const { return w.supplier_no; }
  inline key_type operator()(const q15_struct& w) const { return extract(w); }

 private:
  size_t m_column;
};

class q15_combinator_supplier_revenue {
 public:
  typedef q15_struct_final_output record_type;

  record_type operator()(const SUPPLIER& l, const q15_struct& r) const {
    record_type output;

    output.s_suppkey = l.S_SUPPKEY;
    strcpy(output.s_name, l.S_NAME);
    strcpy(output.s_address, l.S_ADDRESS);
    strcpy(output.s_phone, l.S_PHONE);
    output.total_revenue = r.total_revenue;

    return output;
  }
};

class q15_column_extractor_final_struct {
 public:
  typedef unsigned int key_type;

  q15_column_extractor_final_struct(size_t col) : m_column(col) {
    (void)m_column;
  }

  inline key_type extract(const q15_struct_final_output& w) const {
    return w.s_suppkey;
  }
  inline key_type operator()(const q15_struct_final_output& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// Query 3

// query3 structs
struct query3_struct_customer_order {
  int o_orderkey;
  char o_orderdate[11];
  char o_shippriority[16];
  friend std::ostream& operator<<(std::ostream& os,
                                  const query3_struct_customer_order& output);
};

struct q3_final_struct {
  int o_orderkey;
  double l_extendedprice;
  double l_discount;
  char o_orderdate[11];
  char o_shippriority[16];
  friend std::ostream& operator<<(std::ostream& os,
                                  const q3_final_struct& output);
};

struct q3_struct_select {
  int o_orderkey;
  double revenue;
  char o_orderdate[11];
  char o_shippriority[16];
  friend std::ostream& operator<<(std::ostream& os,
                                  const q3_struct_select& output);
};

struct q3_struct_group {
  int l_orderkey;
  char o_orderdate[11];
  char o_shippriority[16];

  bool operator==(const q3_struct_group& t) const {
    if (!(strcmp(o_shippriority, t.o_shippriority) == 0)) {
      return false;
    }
    return (l_orderkey == t.l_orderkey &&
            date(o_orderdate) == date(t.o_orderdate));
  }

  bool operator<(const q3_struct_group& t) const {
    if (l_orderkey != t.l_orderkey) {
      return l_orderkey < t.l_orderkey;
    }
    if (!(o_orderdate == t.o_orderdate)) {
      return o_orderdate < t.o_orderdate;
    } else {
      return std::string(o_shippriority) < std::string(t.o_shippriority);
    }
  }

  friend std::ostream& operator<<(std::ostream& os,
                                  const q3_struct_group& output);
};

class q3_column_extractor_lineitem {
 public:
  typedef int key_type;

  q3_column_extractor_lineitem(size_t col) : m_column(col) { (void)m_column; }

  inline key_type extract(const LINEITEM& w) const { return w.L_ORDERKEY; }
  inline key_type operator()(const LINEITEM& w) const { return extract(w); }

 private:
  size_t m_column;
};

// query3 extractors

class q3_column_extractor_select_struct {
 public:
  typedef double key_type;

  q3_column_extractor_select_struct(size_t col) : m_column(col) {
    (void)m_column;
  }

  inline key_type extract(const q3_struct_select& w) const { return w.revenue; }
  inline key_type operator()(const q3_struct_select& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

class column_extractor_customer {
 public:
  typedef int key_type;

  column_extractor_customer(size_t col) : m_column(col) { (void)m_column; }

  inline key_type extract(const CUSTOMER& w) const {
    if (m_column == 0) {
      return w.C_CUSTKEY;
    } else {
      return w.C_NATIONKEY;
    }
  }
  inline key_type operator()(const CUSTOMER& w) const { return extract(w); }

 private:
  size_t m_column;
};

class column_extractor_order {
 public:
  typedef int key_type;

  column_extractor_order(size_t col) : m_column(col) { (void)m_column; }

  inline key_type extract(const ORDER& w) const {
    switch (m_column) {
      case 0:
        return w.O_CUSTKEY;
      case 1:
        return w.O_ORDERKEY;
      default:
        return w.O_CUSTKEY;
    }
  }
  inline key_type operator()(const ORDER& w) const { return extract(w); }

 private:
  size_t m_column;
};

class q3_column_extractor_struct_customer_order {
 public:
  typedef int key_type;

  q3_column_extractor_struct_customer_order(size_t col) : m_column(col) {
    (void)m_column;
  }

  inline key_type extract(const query3_struct_customer_order& w) const {
    return w.o_orderkey;
  }
  inline key_type operator()(const query3_struct_customer_order& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

class q3_combinator_customer_order {
 public:
  typedef query3_struct_customer_order record_type;

  record_type operator()(const CUSTOMER& l, const ORDER& r) const {
    record_type output;

    (void)l;

    output.o_orderkey = r.O_ORDERKEY;
    ::memcpy(&output.o_orderdate[0], r.O_ORDERDATE, 11);
    ::memcpy(&output.o_shippriority[0], r.O_ORDERPRIORITY, 16);

    return output;
  }
};

class q3_combinator_struct_with_lineitem {
 public:
  typedef q3_final_struct record_type;

  record_type operator()(const query3_struct_customer_order& l,
                         const LINEITEM& r) const {
    record_type output;

    output.o_orderkey = l.o_orderkey;
    output.l_extendedprice = r.L_EXTENDEDPRICE;
    output.l_discount = r.L_DISCOUNT;
    ::memcpy(&output.o_orderdate[0], l.o_orderdate, 11);
    ::memcpy(&output.o_shippriority[0], l.o_shippriority, 16);

    return output;
  }
};

// query 5
// query 5 structs

// query 5 extractors

class column_extractor_nation {
 public:
  typedef int key_type;

  column_extractor_nation(size_t col) : m_column(col) { (void)m_column; }

  inline key_type extract(const NATION& w) const {
    switch (m_column) {
      case 0:
        return w.N_REGIONKEY;
      case 1:
        return w.N_NATIONKEY;
      default:
        return w.N_REGIONKEY;
    }
  }
  inline key_type operator()(const NATION& w) const { return extract(w); }

 private:
  size_t m_column;
};

class column_extractor_region {
 public:
  typedef int key_type;

  column_extractor_region(size_t col) : m_column(col) { (void)m_column; }

  inline key_type extract(const REGION& w) const { return w.R_REGIONKEY; }
  inline key_type operator()(const REGION& w) const { return extract(w); }

 private:
  size_t m_column;
};

struct Q5_JOIN1_STRUCT_REGION_NATION {
  int n_nationkey;
  char n_name[25];
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q5_JOIN1_STRUCT_REGION_NATION& output);
};

class q5_join1_combinator_nation_region {
 public:
  typedef Q5_JOIN1_STRUCT_REGION_NATION record_type;

  record_type operator()(const NATION& l, const REGION& r) const {
    record_type output;
    output.n_nationkey = l.N_NATIONKEY;
    strcpy(output.n_name, l.N_NAME);
    (void)r;

    return output;
  }
};

// Query 5 Join 2
class q5_join2_extractor_result_of_1 {
 public:
  typedef int key_type;

  q5_join2_extractor_result_of_1(size_t col) : m_column(col) { (void)m_column; }

  inline key_type extract(const Q5_JOIN1_STRUCT_REGION_NATION& w) const {
    return w.n_nationkey;
  }
  inline key_type operator()(const Q5_JOIN1_STRUCT_REGION_NATION& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

struct Q5_JOIN2_STRUCT_RESULT1_CUSTOMER {
  int c_customerkey;
  int n_nationkey;
  char n_name[25];
  friend std::ostream& operator<<(
      std::ostream& os, const Q5_JOIN2_STRUCT_RESULT1_CUSTOMER& output);
};

class q5_join2_combinator_result_of_1_customer {
 public:
  typedef Q5_JOIN2_STRUCT_RESULT1_CUSTOMER record_type;

  record_type operator()(const Q5_JOIN1_STRUCT_REGION_NATION& l,
                         const CUSTOMER& r) const {
    record_type output;
    output.c_customerkey = r.C_CUSTKEY;
    output.n_nationkey = l.n_nationkey;
    strcpy(output.n_name, l.n_name);
    (void)r;

    return output;
  }
};

// *********** PROJECTED FILE QUERY5 Join1 START *************
struct Q5_PR_CUSTOMER {
  int C_CUSTKEY;
  int C_NATIONKEY;
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q5_PR_CUSTOMER& output);
};

class q5_converter_PR_CUSTOMER {
 public:
  typedef Q5_PR_CUSTOMER key_type;

  q5_converter_PR_CUSTOMER() {}
  inline key_type convert(const CUSTOMER& w) const {
    key_type localVariable;
    localVariable.C_CUSTKEY = w.C_CUSTKEY;
    localVariable.C_NATIONKEY = w.C_NATIONKEY;
    return localVariable;
  }
  inline key_type operator()(const CUSTOMER& w) const { return convert(w); }
};

class q5_extractor_PR_CUSTOMER {
 public:
  typedef int key_type;

  q5_extractor_PR_CUSTOMER(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q5_PR_CUSTOMER& w) const {
    return w.C_NATIONKEY;
  }
  inline key_type operator()(const Q5_PR_CUSTOMER& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

class q5_combinator_other_pr_customer {
 public:
  typedef Q5_JOIN2_STRUCT_RESULT1_CUSTOMER record_type;

  record_type operator()(const Q5_JOIN1_STRUCT_REGION_NATION& l,
                         const Q5_PR_CUSTOMER& r) const {
    record_type output;

    output.c_customerkey = r.C_CUSTKEY;
    output.n_nationkey = l.n_nationkey;
    strcpy(output.n_name, l.n_name);
    (void)r;

    return output;
  }
};

// *********** PROJECTED FILE QUERY5 Join1 END *************

// Query 5 Join 3
class q5_join3_extractor_result_of_2 {
 public:
  typedef int key_type;

  q5_join3_extractor_result_of_2(size_t col) : m_column(col) { (void)m_column; }

  inline key_type extract(const Q5_JOIN2_STRUCT_RESULT1_CUSTOMER& w) const {
    return w.c_customerkey;
  }
  inline key_type operator()(const Q5_JOIN2_STRUCT_RESULT1_CUSTOMER& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

struct Q5_JOIN3_STRUCT_RESULT2_ORDER {
  int o_orderkey;
  int n_nationkey;
  char n_name[25];
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q5_JOIN3_STRUCT_RESULT2_ORDER& output);
};

class q5_join3_combinator_result_of_2_order {
 public:
  typedef Q5_JOIN3_STRUCT_RESULT2_ORDER record_type;

  record_type operator()(const Q5_JOIN2_STRUCT_RESULT1_CUSTOMER& l,
                         const ORDER& r) const {
    record_type output;
    output.o_orderkey = r.O_ORDERKEY;
    output.n_nationkey = l.n_nationkey;
    strcpy(output.n_name, l.n_name);
    (void)r;
    return output;
  }
};

// *********** PROJECTED FILE QUERY5 FIRST START *************
// Second table

struct Q5_PR_ORDER {
  int O_ORDERKEY;
  int O_CUSTKEY;
  friend std::ostream& operator<<(std::ostream& os, const Q5_PR_ORDER& output);
};

class q5_converter_PR_ORDER {
 public:
  typedef Q5_PR_ORDER key_type;

  q5_converter_PR_ORDER() {}
  inline key_type convert(const ORDER& w) const {
    key_type localVariable;
    localVariable.O_ORDERKEY = w.O_ORDERKEY;
    localVariable.O_CUSTKEY = w.O_CUSTKEY;
    return localVariable;
  }
  inline key_type operator()(const ORDER& w) const { return convert(w); }
};

class q5_extractor_PR_ORDER {
 public:
  typedef int key_type;

  q5_extractor_PR_ORDER(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q5_PR_ORDER& w) const { return w.O_ORDERKEY; }
  inline key_type operator()(const Q5_PR_ORDER& w) const { return extract(w); }

 private:
  size_t m_column;
};

class q5_combinator_other_pr_order {
 public:
  typedef Q5_JOIN3_STRUCT_RESULT2_ORDER record_type;

  record_type operator()(const Q5_JOIN2_STRUCT_RESULT1_CUSTOMER& l,
                         const Q5_PR_ORDER& r) const {
    record_type output;

    output.o_orderkey = r.O_ORDERKEY;
    output.n_nationkey = l.n_nationkey;
    strcpy(output.n_name, l.n_name);
    (void)r;
    return output;
  }
};

// *********** PROJECTED FILE QUERY5 FIRST END *************

// Query 5 Join 4
class q5_join4_extractor_result_of_3 {
 public:
  typedef int key_type;

  q5_join4_extractor_result_of_3(size_t col) : m_column(col) { (void)m_column; }

  inline key_type extract(const Q5_JOIN3_STRUCT_RESULT2_ORDER& w) const {
    return w.o_orderkey;
  }
  inline key_type operator()(const Q5_JOIN3_STRUCT_RESULT2_ORDER& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

struct Q5_JOIN4_STRUCT_RESULT3_ORDER {
  int l_suppkey;
  int n_nationkey;
  char n_name[25];
  double l_discount;
  double l_extendedprice;

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q5_JOIN4_STRUCT_RESULT3_ORDER& output);
};

class q5_join4_combinator_result_of_3_lineitem {
 public:
  typedef Q5_JOIN4_STRUCT_RESULT3_ORDER record_type;

  record_type operator()(const Q5_JOIN3_STRUCT_RESULT2_ORDER& l,
                         const LINEITEM& r) const {
    record_type output;
    output.n_nationkey = l.n_nationkey;
    strcpy(output.n_name, l.n_name);
    output.l_suppkey = r.L_SUPPKEY;
    output.l_extendedprice = r.L_EXTENDEDPRICE;
    output.l_discount = r.L_DISCOUNT;
    return output;
  }
};

// *********** PROJECTED FILE QUERY5 START *************

struct Q5_PR_LINEITEM {
#ifdef UNIFIED
  double L_EXTENDEDPRICE;
  double L_DISCOUNT;
  double L_QUANTITY;
  int L_SUPPKEY;
  int L_ORDERKEY;
#else
  double L_EXTENDEDPRICE;
  double L_DISCOUNT;
  int L_SUPPKEY;
  int L_ORDERKEY;
#endif

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q5_PR_LINEITEM& output);
};

class q5_converter_PR_LINEITEM {
 public:
  typedef Q5_PR_LINEITEM key_type;

  q5_converter_PR_LINEITEM() {}
  inline key_type convert(const LINEITEM& w) const {
    key_type localVariable;

#ifdef UNIFIED
    localVariable.L_EXTENDEDPRICE = w.L_EXTENDEDPRICE;
    localVariable.L_DISCOUNT = w.L_DISCOUNT;
    localVariable.L_QUANTITY = w.L_QUANTITY;
    localVariable.L_SUPPKEY = w.L_SUPPKEY;
    localVariable.L_ORDERKEY = w.L_ORDERKEY;
#else
    localVariable.L_ORDERKEY = w.L_ORDERKEY;
    localVariable.L_SUPPKEY = w.L_SUPPKEY;
    localVariable.L_EXTENDEDPRICE = w.L_EXTENDEDPRICE;
    localVariable.L_DISCOUNT = w.L_DISCOUNT;
#endif

    return localVariable;
  }
  inline key_type operator()(const LINEITEM& w) const { return convert(w); }
};

class q5_combinator_part_pr_lineitem {
 public:
  typedef Q5_JOIN4_STRUCT_RESULT3_ORDER record_type;

  record_type operator()(const Q5_JOIN3_STRUCT_RESULT2_ORDER& l,
                         const Q5_PR_LINEITEM& r) const {
    record_type output;

    output.n_nationkey = l.n_nationkey;
    strcpy(output.n_name, l.n_name);
    output.l_suppkey = r.L_SUPPKEY;
    output.l_extendedprice = r.L_EXTENDEDPRICE;
    output.l_discount = r.L_DISCOUNT;

    return output;
  }
};

class q5_extractor_PR_LINEITEM {
 public:
  typedef int key_type;

  q5_extractor_PR_LINEITEM(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q5_PR_LINEITEM& w) const {
    return w.L_ORDERKEY;
  }
  inline key_type operator()(const Q5_PR_LINEITEM& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};
// *********** PROJECTED FILE QUERY9 END *************

// Query 5 Join 5
class q5_join5_extractor_result_of_4 {
 public:
  typedef int key_type;

  q5_join5_extractor_result_of_4(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q5_JOIN4_STRUCT_RESULT3_ORDER& w) const {
    return w.n_nationkey;
  }
  inline key_type operator()(const Q5_JOIN4_STRUCT_RESULT3_ORDER& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

struct Q5_JOIN5_STRUCT_RESULT4_SUPPLIER {
  int l_suppkey;
  int n_nationkey;
  char n_name[25];
  double l_discount;
  double l_extendedprice;
  friend std::ostream& operator<<(
      std::ostream& os, const Q5_JOIN5_STRUCT_RESULT4_SUPPLIER& output);
};

class q5_join5_combinator_result_of_4_supplier {
 public:
  typedef Q5_JOIN5_STRUCT_RESULT4_SUPPLIER record_type;

  record_type operator()(const Q5_JOIN4_STRUCT_RESULT3_ORDER& l,
                         const SUPPLIER& r) const {
    record_type output;
    output.n_nationkey = l.n_nationkey;
    strcpy(output.n_name, l.n_name);
    output.l_suppkey = r.S_SUPPKEY;
    output.l_extendedprice = l.l_extendedprice;
    output.l_discount = l.l_discount;
    (void)r;

    return output;
  }
};

// Query 5 Join 5
class q5_join5_extractor_result_of_5 {
 public:
  typedef double key_type;

  q5_join5_extractor_result_of_5(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q5_JOIN5_STRUCT_RESULT4_SUPPLIER& w) const {
    return w.l_extendedprice * (1 - w.l_discount);
  }
  inline key_type operator()(const Q5_JOIN5_STRUCT_RESULT4_SUPPLIER& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// Query8 Join1 Node8Operator1
struct Q8_JOIN1_STRUCT_PART_LINEITEM {
  int l_partkey;
  int l_orderkey;
  int l_suppkey;
  double l_extendedprice;
  double l_discount;
  char p_type[26];

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q8_JOIN1_STRUCT_PART_LINEITEM& output);
};

class q8_combinator_part_line {
 public:
  typedef Q8_JOIN1_STRUCT_PART_LINEITEM record_type;

  record_type operator()(const PART& l, const LINEITEM& r) const {
    record_type output;

    output.l_partkey = r.L_PARTKEY;
    output.l_orderkey = r.L_ORDERKEY;
    output.l_suppkey = r.L_SUPPKEY;
    output.l_extendedprice = r.L_EXTENDEDPRICE;
    output.l_discount = r.L_DISCOUNT;

    strcpy(output.p_type, l.P_TYPE);

    return output;
  }
};

// *********** PROJECTED FILE QUERY8 START *************

struct UNIFIED_PR_LINEITEM {
  double L_EXTENDEDPRICE;
  double L_QUANTITY;
  double L_DISCOUNT;
  int L_PARTKEY;
  int L_SUPPKEY;
  int L_ORDERKEY;
  friend std::ostream& operator<<(std::ostream& os,
                                  const UNIFIED_PR_LINEITEM& output);
};

struct UNIFIED_PR_LINEITEM_ORDER_KEY {
  double L_EXTENDEDPRICE;
  double L_DISCOUNT;
  double L_QUANTITY;
  int L_SUPPKEY;
  int L_ORDERKEY;
  friend std::ostream& operator<<(std::ostream& os,
                                  const UNIFIED_PR_LINEITEM_ORDER_KEY& output);
};

struct UNIFIED_PR_ORDER_ORDER_KEY {
  char O_ORDERPRIORITY[16];
  double O_TOTALPRICE;
  int O_CUSTKEY;
  int O_ORDERKEY;
  int O_SHIPPRIORITY;

  friend std::ostream& operator<<(std::ostream& os,
                                  const UNIFIED_PR_ORDER_ORDER_KEY& output);
};

// First projected table
struct Q8_PR_LINEITEM {
#ifdef UNIFIED
  double L_EXTENDEDPRICE;
  double L_QUANTITY;
  double L_DISCOUNT;
  int L_PARTKEY;
  int L_SUPPKEY;
  int L_ORDERKEY;
#else
  double L_EXTENDEDPRICE;
  double L_DISCOUNT;
  int L_PARTKEY;
  int L_SUPPKEY;
  int L_ORDERKEY;
#endif

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q8_PR_LINEITEM& output);
};

class q8_converter_PR_LINEITEM {
 public:
  typedef Q8_PR_LINEITEM key_type;

  q8_converter_PR_LINEITEM() {}
  inline key_type convert(const LINEITEM& w) const {
    key_type localVariable;

#ifdef UNIFIED
    localVariable.L_EXTENDEDPRICE = w.L_EXTENDEDPRICE;
    localVariable.L_DISCOUNT = w.L_DISCOUNT;
    localVariable.L_PARTKEY = w.L_PARTKEY;
    localVariable.L_ORDERKEY = w.L_ORDERKEY;
    localVariable.L_SUPPKEY = w.L_SUPPKEY;
    localVariable.L_DISCOUNT = w.L_DISCOUNT;
#else
    localVariable.L_PARTKEY = w.L_PARTKEY;
    localVariable.L_ORDERKEY = w.L_ORDERKEY;
    localVariable.L_SUPPKEY = w.L_SUPPKEY;
    localVariable.L_EXTENDEDPRICE = w.L_EXTENDEDPRICE;
    localVariable.L_DISCOUNT = w.L_DISCOUNT;
#endif

    return localVariable;
  }
  inline key_type operator()(const LINEITEM& w) const { return convert(w); }
};

class q8_extractor_PR_LINEITEM {
 public:
  typedef int key_type;

  q8_extractor_PR_LINEITEM(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q8_PR_LINEITEM& w) const { return w.L_PARTKEY; }
  inline key_type operator()(const Q8_PR_LINEITEM& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// Second projected table

struct Q8_PR_PART {
  char P_TYPE[26];
  int P_PARTKEY;

  friend std::ostream& operator<<(std::ostream& os, const Q8_PR_PART& output);
};

class q8_converter_PR_PART {
 public:
  typedef Q8_PR_PART key_type;

  q8_converter_PR_PART() {}
  inline key_type convert(const PART& w) const {
    key_type localVariable;
    localVariable.P_PARTKEY = w.P_PARTKEY;
    strcpy(localVariable.P_TYPE, w.P_TYPE);

    return localVariable;
  }
  inline key_type operator()(const PART& w) const { return convert(w); }
};

class q8_extractor_PR_PART {
 public:
  typedef int key_type;

  q8_extractor_PR_PART(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q8_PR_PART& w) const { return w.P_PARTKEY; }
  inline key_type operator()(const Q8_PR_PART& w) const { return extract(w); }

 private:
  size_t m_column;
};

class q8_combinator_pr_part_pr_lineitem {
 public:
  typedef Q8_JOIN1_STRUCT_PART_LINEITEM record_type;

  record_type operator()(const Q8_PR_PART& l, const Q8_PR_LINEITEM& r) const {
    record_type output;

    output.l_partkey = r.L_PARTKEY;
    output.l_orderkey = r.L_ORDERKEY;
    output.l_suppkey = r.L_SUPPKEY;
    output.l_extendedprice = r.L_EXTENDEDPRICE;
    output.l_discount = r.L_DISCOUNT;

    strcpy(output.p_type, l.P_TYPE);

    return output;
  }
};

class q8_combinator_part_pr_lineitem {
 public:
  typedef Q8_JOIN1_STRUCT_PART_LINEITEM record_type;

  record_type operator()(const PART& l, const Q8_PR_LINEITEM& r) const {
    record_type output;

    output.l_partkey = r.L_PARTKEY;
    output.l_orderkey = r.L_ORDERKEY;
    output.l_suppkey = r.L_SUPPKEY;
    output.l_extendedprice = r.L_EXTENDEDPRICE;
    output.l_discount = r.L_DISCOUNT;

    strcpy(output.p_type, l.P_TYPE);

    return output;
  }
};

// *********** PROJECTED FILE QUERY8 END *************

// Query8 Join2 Node8Operator3

class q8_join2_extractor_result {
 public:
  typedef int key_type;

  q8_join2_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q8_JOIN1_STRUCT_PART_LINEITEM& w) const {
    return w.l_orderkey;
  }
  inline key_type operator()(const Q8_JOIN1_STRUCT_PART_LINEITEM& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

struct Q8_JOIN2_STRUCT_RESULT_ORDERS {
  int l_partkey;
  int l_orderkey;
  int l_suppkey;
  double l_extendedprice;
  double l_discount;
  int o_custkey;
  char o_orderdate[11];

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q8_JOIN2_STRUCT_RESULT_ORDERS& output);
};

class q8_combinator_result_orders {
 public:
  typedef Q8_JOIN2_STRUCT_RESULT_ORDERS record_type;

  record_type operator()(const Q8_JOIN1_STRUCT_PART_LINEITEM& l,
                         const ORDER& r) const {
    record_type output;

    output.l_partkey = l.l_partkey;
    output.l_orderkey = l.l_orderkey;
    output.l_suppkey = l.l_suppkey;
    output.l_extendedprice = l.l_extendedprice;
    output.l_discount = l.l_discount;
    output.o_custkey = r.O_CUSTKEY;
    strcpy(output.o_orderdate, r.O_ORDERDATE);

    return output;
  }
};

// *********** PROJECTED FILE QUERY8 START ORDER *************
struct Q8_PR_ORDER {
  int O_CUSTKEY;
  int O_ORDERKEY;
  char O_ORDERDATE[11];
  friend std::ostream& operator<<(std::ostream& os, const Q8_PR_ORDER& output);
};

class q8_converter_PR_ORDER {
 public:
  typedef Q8_PR_ORDER key_type;

  q8_converter_PR_ORDER() {}
  inline key_type convert(const ORDER& w) const {
    key_type localVariable;
    localVariable.O_CUSTKEY = w.O_CUSTKEY;
    localVariable.O_ORDERKEY = w.O_ORDERKEY;
    strcpy(localVariable.O_ORDERDATE, w.O_ORDERDATE);
    return localVariable;
  }
  inline key_type operator()(const ORDER& w) const { return convert(w); }
};

class q8_extractor_PR_ORDER {
 public:
  typedef int key_type;

  q8_extractor_PR_ORDER(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q8_PR_ORDER& w) const { return w.O_ORDERKEY; }
  inline key_type operator()(const Q8_PR_ORDER& w) const { return extract(w); }

 private:
  size_t m_column;
};

class q8_combinator_other_pr_order {
 public:
  typedef Q8_JOIN2_STRUCT_RESULT_ORDERS record_type;

  record_type operator()(const Q8_JOIN1_STRUCT_PART_LINEITEM& l,
                         const Q8_PR_ORDER& r) const {
    record_type output;

    output.l_partkey = l.l_partkey;
    output.l_orderkey = l.l_orderkey;
    output.l_suppkey = l.l_suppkey;
    output.l_extendedprice = l.l_extendedprice;
    output.l_discount = l.l_discount;
    output.o_custkey = r.O_CUSTKEY;
    strcpy(output.o_orderdate, r.O_ORDERDATE);

    return output;
  }
};

// *********** PROJECTED FILE QUERY8 END ORDER *************

// Query8 Join3 Node8Operator3

class q8_join3_extractor_result {
 public:
  typedef int key_type;

  q8_join3_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q8_JOIN2_STRUCT_RESULT_ORDERS& w) const {
    return w.o_custkey;
  }
  inline key_type operator()(const Q8_JOIN2_STRUCT_RESULT_ORDERS& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

struct Q8_JOIN3_STRUCT_RESULT_CUSTOMER {
  int l_partkey;
  int l_orderkey;
  int l_suppkey;
  double l_extendedprice;
  double l_discount;
  char o_orderdate[11];
  int c_nationkey;
  int o_custkey;

  friend std::ostream& operator<<(
      std::ostream& os, const Q8_JOIN3_STRUCT_RESULT_CUSTOMER& output);
};

class q8_combinator_result_customers {
 public:
  typedef Q8_JOIN3_STRUCT_RESULT_CUSTOMER record_type;

  record_type operator()(const Q8_JOIN2_STRUCT_RESULT_ORDERS& l,
                         const CUSTOMER& r) const {
    record_type output;
    output.l_partkey = l.l_partkey;
    output.l_orderkey = l.l_orderkey;
    output.l_suppkey = l.l_suppkey;
    output.l_extendedprice = l.l_extendedprice;
    output.l_discount = l.l_discount;
    strcpy(output.o_orderdate, l.o_orderdate);
    output.c_nationkey = r.C_NATIONKEY;
    output.o_custkey = l.o_custkey;

    return output;
  }
};

// *********** PROJECTED FILE QUERY8 CUSTOMER START *************
struct Q8_PR_CUSTOMER {
  int C_CUSTKEY;
  int C_NATIONKEY;
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q8_PR_CUSTOMER& output);
};

class q8_converter_PR_CUSTOMER {
 public:
  typedef Q8_PR_CUSTOMER key_type;

  q8_converter_PR_CUSTOMER() {}
  inline key_type convert(const CUSTOMER& w) const {
    key_type localVariable;
    localVariable.C_CUSTKEY = w.C_CUSTKEY;
    localVariable.C_NATIONKEY = w.C_NATIONKEY;
    return localVariable;
  }
  inline key_type operator()(const CUSTOMER& w) const { return convert(w); }
};

class q8_extractor_PR_CUSTOMER {
 public:
  typedef int key_type;

  q8_extractor_PR_CUSTOMER(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q8_PR_CUSTOMER& w) const { return w.C_CUSTKEY; }
  inline key_type operator()(const Q8_PR_CUSTOMER& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

class q8_combinator_other_pr_customer {
 public:
  typedef Q8_JOIN3_STRUCT_RESULT_CUSTOMER record_type;

  record_type operator()(const Q8_JOIN2_STRUCT_RESULT_ORDERS& l,
                         const Q8_PR_CUSTOMER& r) const {
    record_type output;

    output.l_partkey = l.l_partkey;
    output.l_orderkey = l.l_orderkey;
    output.l_suppkey = l.l_suppkey;
    output.l_extendedprice = l.l_extendedprice;
    output.l_discount = l.l_discount;
    strcpy(output.o_orderdate, l.o_orderdate);
    output.c_nationkey = r.C_NATIONKEY;
    output.o_custkey = l.o_custkey;

    return output;
  }
};

// *********** PROJECTED FILE QUERY8 CUSTOMER END *************

// Query8 Join4

class q8_join4_extractor_result {
 public:
  typedef int key_type;

  q8_join4_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q8_JOIN3_STRUCT_RESULT_CUSTOMER& w) const {
    return w.c_nationkey;
  }
  inline key_type operator()(const Q8_JOIN3_STRUCT_RESULT_CUSTOMER& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

struct Q8_JOIN4_STRUCT_RESULT_NATION {
  int l_suppkey;
  double l_extendedprice;
  double l_discount;
  char o_orderdate[11];
  int c_nationkey;
  int n_regionkey;
  char n_nationame[26];

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q8_JOIN4_STRUCT_RESULT_NATION& output);
};

class q8_combinator_result_nation {
 public:
  typedef Q8_JOIN4_STRUCT_RESULT_NATION record_type;

  record_type operator()(const Q8_JOIN3_STRUCT_RESULT_CUSTOMER& l,
                         const NATION& r) const {
    record_type output;

    output.l_suppkey = l.l_suppkey;
    output.l_extendedprice = l.l_extendedprice;
    output.l_discount = l.l_discount;
    output.c_nationkey = r.N_NATIONKEY;
    output.n_regionkey = r.N_REGIONKEY;
    strcpy(output.o_orderdate, l.o_orderdate);
    strcpy(output.n_nationame, r.N_NAME);

    return output;
  }
};

// Query8 Join5

class q8_join5_extractor_result {
 public:
  typedef int key_type;

  q8_join5_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q8_JOIN4_STRUCT_RESULT_NATION& w) const {
    return w.n_regionkey;
  }
  inline key_type operator()(const Q8_JOIN4_STRUCT_RESULT_NATION& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

struct Q8_JOIN5_STRUCT_RESULT_REGION {
  int l_suppkey;
  double l_extendedprice;
  double l_discount;
  char o_orderdate[11];
  int c_nationkey;
  int n_regionkey;
  char n_nationame[26];

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q8_JOIN5_STRUCT_RESULT_REGION& output);
};

class q8_combinator_result_region {
 public:
  typedef Q8_JOIN5_STRUCT_RESULT_REGION record_type;

  record_type operator()(const Q8_JOIN4_STRUCT_RESULT_NATION& l,
                         const REGION& r) const {
    record_type output;

    output.l_suppkey = l.l_suppkey;
    output.l_extendedprice = l.l_extendedprice;
    output.l_discount = l.l_discount;
    output.c_nationkey = l.c_nationkey;
    output.n_regionkey = r.R_REGIONKEY;
    strcpy(output.o_orderdate, l.o_orderdate);
    strcpy(output.n_nationame, l.n_nationame);

    return output;
  }
};

// Query8 Join6

class q8_join6_extractor_result {
 public:
  typedef unsigned int key_type;

  q8_join6_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q8_JOIN5_STRUCT_RESULT_REGION& w) const {
    return w.l_suppkey;
  }
  inline key_type operator()(const Q8_JOIN5_STRUCT_RESULT_REGION& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

struct Q8_JOIN6_STRUCT_RESULT_SUPPLIER {
  int l_suppkey;
  double l_extendedprice;
  double l_discount;
  char o_orderdate[11];
  int c_nationkey;
  int n_regionkey;
  char n_nationame[26];

  friend std::ostream& operator<<(
      std::ostream& os, const Q8_JOIN6_STRUCT_RESULT_SUPPLIER& output);
};

class q8_combinator_result_supplier {
 public:
  typedef Q8_JOIN6_STRUCT_RESULT_SUPPLIER record_type;

  record_type operator()(const Q8_JOIN5_STRUCT_RESULT_REGION& l,
                         const SUPPLIER& r) const {
    record_type output;

    output.l_suppkey = r.S_SUPPKEY;
    output.l_extendedprice = l.l_extendedprice;
    output.l_discount = l.l_discount;
    output.c_nationkey = l.c_nationkey;
    output.n_regionkey = l.n_regionkey;
    strcpy(output.o_orderdate, l.o_orderdate);
    strcpy(output.n_nationame, l.n_nationame);

    return output;
  }
};

// Query8 Join7

class q8_join7_extractor_result {
 public:
  typedef unsigned int key_type;

  q8_join7_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q8_JOIN6_STRUCT_RESULT_SUPPLIER& w) const {
    return w.c_nationkey;
  }
  inline key_type operator()(const Q8_JOIN6_STRUCT_RESULT_SUPPLIER& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

struct Q8_JOIN7_STRUCT_RESULT_NATION1 {
  int l_suppkey;
  double l_extendedprice;
  double l_discount;
  char o_orderdate[11];
  int c_nationkey;
  int n_regionkey;
  char n_nationame[26];

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q8_JOIN7_STRUCT_RESULT_NATION1& output);
};

class q8_combinator_result_nation1 {
 public:
  typedef Q8_JOIN7_STRUCT_RESULT_NATION1 record_type;

  record_type operator()(const Q8_JOIN6_STRUCT_RESULT_SUPPLIER& l,
                         const NATION& r) const {
    record_type output;

    output.l_suppkey = l.l_suppkey;
    output.l_extendedprice = l.l_extendedprice;
    output.l_discount = l.l_discount;
    output.c_nationkey = r.N_NATIONKEY;
    output.n_regionkey = l.n_regionkey;
    strcpy(output.o_orderdate, l.o_orderdate);
    strcpy(output.n_nationame, l.n_nationame);

    return output;
  }
};

// Query8Node11

class q8_node11_extractor_result {
 public:
  typedef unsigned int key_type;

  q8_node11_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q8_JOIN7_STRUCT_RESULT_NATION1& w) const {
    return w.l_suppkey;
  }
  inline key_type operator()(const Q8_JOIN7_STRUCT_RESULT_NATION1& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// Query9 Node1
struct Q9_JOIN1_STRUCT_PART_LINEITEM {
  int l_partkey;
  int l_orderkey;
  int l_suppkey;
  double l_quantity;
  double l_extendedprice;
  double l_discount;
  char p_type[26];

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q9_JOIN1_STRUCT_PART_LINEITEM& output);
};

class q9_combinator_part_line {
 public:
  typedef Q9_JOIN1_STRUCT_PART_LINEITEM record_type;

  record_type operator()(const PART& l, const LINEITEM& r) const {
    record_type output;

    output.l_partkey = l.P_PARTKEY;
    output.l_orderkey = r.L_ORDERKEY;
    output.l_suppkey = r.L_SUPPKEY;
    output.l_extendedprice = r.L_EXTENDEDPRICE;
    output.l_discount = r.L_DISCOUNT;
    output.l_quantity = r.L_QUANTITY;

    return output;
  }
};

// *********** PROJECTED FILE QUERY9 START *************
struct Q9_PR_LINEITEM {
#ifdef UNIFIED
  double L_EXTENDEDPRICE;
  double L_QUANTITY;
  double L_DISCOUNT;
  int L_PARTKEY;
  int L_SUPPKEY;
  int L_ORDERKEY;
#else
  double L_EXTENDEDPRICE;
  double L_QUANTITY;
  double L_DISCOUNT;
  int L_PARTKEY;
  int L_SUPPKEY;
  int L_ORDERKEY;
#endif
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q9_PR_LINEITEM& output);
};

class q9_converter_PR_LINEITEM {
 public:
  typedef Q9_PR_LINEITEM key_type;

  q9_converter_PR_LINEITEM() {}
  inline key_type convert(const LINEITEM& w) const {
    key_type localVariable;
    localVariable.L_PARTKEY = w.L_PARTKEY;
    localVariable.L_ORDERKEY = w.L_ORDERKEY;
    localVariable.L_SUPPKEY = w.L_SUPPKEY;
    localVariable.L_EXTENDEDPRICE = w.L_EXTENDEDPRICE;
    localVariable.L_QUANTITY = w.L_QUANTITY;
    localVariable.L_DISCOUNT = w.L_DISCOUNT;

    return localVariable;
  }
  inline key_type operator()(const LINEITEM& w) const { return convert(w); }
};

class q9_extractor_PR_LINEITEM {
 public:
  typedef int key_type;

  q9_extractor_PR_LINEITEM(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q9_PR_LINEITEM& w) const { return w.L_PARTKEY; }
  inline key_type operator()(const Q9_PR_LINEITEM& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// second relation

// Second projected table

struct Q9_PR_PART {
  int P_PARTKEY;

  friend std::ostream& operator<<(std::ostream& os, const Q9_PR_PART& output);
};

class q9_converter_PR_PART {
 public:
  typedef Q9_PR_PART key_type;

  q9_converter_PR_PART() {}
  inline key_type convert(const PART& w) const {
    key_type localVariable;
    localVariable.P_PARTKEY = w.P_PARTKEY;
    return localVariable;
  }
  inline key_type operator()(const PART& w) const { return convert(w); }
};

class q9_extractor_PR_PART {
 public:
  typedef int key_type;

  q9_extractor_PR_PART(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q9_PR_PART& w) const { return w.P_PARTKEY; }
  inline key_type operator()(const Q9_PR_PART& w) const { return extract(w); }

 private:
  size_t m_column;
};

class q9_combinator_pr_part_pr_lineitem {
 public:
  typedef Q9_JOIN1_STRUCT_PART_LINEITEM record_type;

  record_type operator()(const Q9_PR_PART& l, const Q9_PR_LINEITEM& r) const {
    record_type output;

    output.l_partkey = l.P_PARTKEY;
    output.l_orderkey = r.L_ORDERKEY;
    output.l_suppkey = r.L_SUPPKEY;
    output.l_extendedprice = r.L_EXTENDEDPRICE;
    output.l_discount = r.L_DISCOUNT;
    output.l_quantity = r.L_QUANTITY;

    return output;
  }
};

// *********** PROJECTED FILE QUERY9 END *************

// Query9 Node2

class q9_join2_extractor_result {
 public:
  typedef int key_type;

  q9_join2_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q9_JOIN1_STRUCT_PART_LINEITEM& w) const {
    return w.l_suppkey;
  }
  inline key_type operator()(const Q9_JOIN1_STRUCT_PART_LINEITEM& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

struct Q9_JOIN2_STRUCT_RESULT_SUPPLIER {
  int l_partkey;
  int l_orderkey;
  int l_suppkey;
  double l_quantity;
  double l_extendedprice;
  double l_discount;
  int s_nationkey;

  friend std::ostream& operator<<(
      std::ostream& os, const Q9_JOIN2_STRUCT_RESULT_SUPPLIER& output);
};

class q9_combinator_result_supplier {
 public:
  typedef Q9_JOIN2_STRUCT_RESULT_SUPPLIER record_type;

  record_type operator()(const Q9_JOIN1_STRUCT_PART_LINEITEM& l,
                         const SUPPLIER& r) const {
    record_type output;

    output.l_partkey = l.l_partkey;
    output.l_orderkey = l.l_orderkey;
    output.l_suppkey = r.S_SUPPKEY;
    output.l_extendedprice = l.l_extendedprice;
    output.l_discount = l.l_discount;
    output.l_quantity = l.l_quantity;
    output.s_nationkey = r.S_NATIONKEY;

    return output;
  }
};

// Query9 Node3
class q9_join3_extractor_result {
 public:
  typedef int key_type;

  q9_join3_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q9_JOIN2_STRUCT_RESULT_SUPPLIER& w) const {
    return w.s_nationkey;
  }
  inline key_type operator()(const Q9_JOIN2_STRUCT_RESULT_SUPPLIER& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

struct Q9_JOIN3_STRUCT_RESULT_NATION {
  int l_partkey;
  int l_orderkey;
  double l_quantity;
  double l_extendedprice;
  double l_discount;
  char n_name[26];

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q9_JOIN3_STRUCT_RESULT_NATION& output);
};

class q9_combinator_result_nation {
 public:
  typedef Q9_JOIN3_STRUCT_RESULT_NATION record_type;

  record_type operator()(const Q9_JOIN2_STRUCT_RESULT_SUPPLIER& l,
                         const NATION& r) const {
    record_type output;

    output.l_partkey = l.l_partkey;
    output.l_orderkey = l.l_orderkey;
    output.l_quantity = l.l_quantity;
    output.l_extendedprice = l.l_extendedprice;
    output.l_discount = l.l_discount;
    strcpy(output.n_name, r.N_NAME);

    return output;
  }
};

// Query9 Node4 (SORT)
class q9_sort4_extractor_result {
 public:
  typedef int key_type;

  q9_sort4_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q9_JOIN3_STRUCT_RESULT_NATION& w) const {
    return w.l_orderkey;
  }
  inline key_type operator()(const Q9_JOIN3_STRUCT_RESULT_NATION& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// Query9 Node5 (Join4)
struct Q9_JOIN4_STRUCT_RESULT_ORDER {
  int l_partkey;
  int l_orderkey;
  double l_quantity;
  double l_extendedprice;
  double l_discount;
  char n_name[26];

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q9_JOIN4_STRUCT_RESULT_ORDER& output);
};

class q9_combinator_result_order {
 public:
  typedef Q9_JOIN4_STRUCT_RESULT_ORDER record_type;

  record_type operator()(const Q9_JOIN3_STRUCT_RESULT_NATION& l,
                         const ORDER& r) const {
    record_type output;

    output.l_partkey = l.l_partkey;
    output.l_orderkey = l.l_orderkey;
    output.l_quantity = l.l_quantity;
    output.l_extendedprice = l.l_extendedprice;
    output.l_discount = l.l_discount;
    strcpy(output.n_name, l.n_name);
    (void)r;
    return output;
  }
};

class q9_join5_extractor_result {
 public:
  typedef int key_type;

  q9_join5_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q9_JOIN4_STRUCT_RESULT_ORDER& w) const {
    return w.l_partkey;
  }
  inline key_type operator()(const Q9_JOIN4_STRUCT_RESULT_ORDER& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

class column_extractor_partsupp {
 public:
  typedef int key_type;

  column_extractor_partsupp(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const PARTSUPP& w) const {
    switch (m_column) {
      case 0:
        return w.PS_PARTKEY;
      case 1:
        return w.PS_SUPPKEY;
      default:
        return w.PS_PARTKEY;
    }
  }
  inline key_type operator()(const PARTSUPP& w) const { return extract(w); }

 private:
  size_t m_column;
};

// Query9 Node6 (Join5)
struct Q9_JOIN5_STRUCT_RESULT_PARTSUPP {
  int l_partkey;
  int l_orderkey;
  double l_quantity;
  double l_extendedprice;
  double l_discount;
  char n_name[26];

  friend std::ostream& operator<<(
      std::ostream& os, const Q9_JOIN5_STRUCT_RESULT_PARTSUPP& output);
};

class q9_combinator_result_partsupp {
 public:
  typedef Q9_JOIN5_STRUCT_RESULT_PARTSUPP record_type;

  record_type operator()(const Q9_JOIN4_STRUCT_RESULT_ORDER& l,
                         const PARTSUPP& r) const {
    record_type output;

    output.l_partkey = r.PS_PARTKEY;
    output.l_orderkey = l.l_orderkey;
    output.l_quantity = l.l_quantity;
    output.l_extendedprice = l.l_extendedprice;
    output.l_discount = l.l_discount;
    strcpy(output.n_name, l.n_name);
    return output;
  }
};

// Query9

class q9_node8_extractor_result {
 public:
  typedef unsigned int key_type;

  q9_node8_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q9_JOIN5_STRUCT_RESULT_PARTSUPP& w) const {
    return w.l_orderkey;
  }
  inline key_type operator()(const Q9_JOIN5_STRUCT_RESULT_PARTSUPP& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// Query10 Node2 (Join1)
struct Q10_JOIN1_STRUCT_ORDER_LINEITEM {
  int o_custkey;
  double l_quantity;
  double l_extendedprice;

  friend std::ostream& operator<<(
      std::ostream& os, const Q10_JOIN1_STRUCT_ORDER_LINEITEM& output);
};

class q10_combinator_order_lineitem {
 public:
  typedef Q10_JOIN1_STRUCT_ORDER_LINEITEM record_type;

  record_type operator()(const ORDER& l, const LINEITEM& r) const {
    record_type output;

    output.o_custkey = l.O_CUSTKEY;
    output.l_quantity = r.L_QUANTITY;
    output.l_extendedprice = r.L_EXTENDEDPRICE;

    return output;
  }
};

struct Q2_PR_LINEITEM_SORT {
  //  double L_QUANTITY;
  //  double L_EXTENDEDPRICE;
  int L_ORDERKEY;
  //  int L_PARTKEY;
  //  int L_SUPPKEY;
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q2_PR_LINEITEM_SORT& output);
};

class q2_converter_PR_LINEITEM_SORT {
 public:
  typedef Q2_PR_LINEITEM_SORT key_type;

  q2_converter_PR_LINEITEM_SORT() {}
  inline key_type convert(const LINEITEM& w) const {
    key_type localVariable;
    //    localVariable.L_QUANTITY = w.L_QUANTITY;
    //    localVariable.L_EXTENDEDPRICE = w.L_EXTENDEDPRICE;
    //    localVariable.L_PARTKEY = w.L_PARTKEY;
    localVariable.L_ORDERKEY = w.L_ORDERKEY;
    //    localVariable.L_SUPPKEY = w.L_SUPPKEY;

    return localVariable;
  }
  inline key_type operator()(const LINEITEM& w) const { return convert(w); }
};

class q2_extractor_PR_LINEITEM_SORT {
 public:
  typedef int key_type;

  q2_extractor_PR_LINEITEM_SORT(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q2_PR_LINEITEM_SORT& w) const {
    return w.L_ORDERKEY;
  }
  inline key_type operator()(const Q2_PR_LINEITEM_SORT& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// *********** PROJECTED FILE QUERY10 START *************
struct Q10_PR_LINEITEM {
  double L_QUANTITY;
  double L_EXTENDEDPRICE;
  int L_ORDERKEY;
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q10_PR_LINEITEM& output);
};

class q10_converter_PR_LINEITEM {
 public:
  typedef Q10_PR_LINEITEM key_type;

  q10_converter_PR_LINEITEM() {}
  inline key_type convert(const LINEITEM& w) const {
    key_type localVariable;
    localVariable.L_EXTENDEDPRICE = w.L_EXTENDEDPRICE;
    localVariable.L_QUANTITY = w.L_QUANTITY;
    localVariable.L_ORDERKEY = w.L_ORDERKEY;

    return localVariable;
  }
  inline key_type operator()(const LINEITEM& w) const { return convert(w); }
};

class q10_extractor_PR_LINEITEM {
 public:
  typedef int key_type;

  q10_extractor_PR_LINEITEM(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q10_PR_LINEITEM& w) const {
    return w.L_ORDERKEY;
  }
  inline key_type operator()(const Q10_PR_LINEITEM& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

class q10_combinator_order_pr_lineitem {
 public:
  typedef Q10_JOIN1_STRUCT_ORDER_LINEITEM record_type;

  record_type operator()(const ORDER& l, const Q10_PR_LINEITEM& r) const {
    record_type output;

    output.o_custkey = l.O_CUSTKEY;
    output.l_quantity = r.L_QUANTITY;
    output.l_extendedprice = r.L_EXTENDEDPRICE;

    return output;
  }
};

// Second table
struct Q10_PR_ORDER {
  int O_CUSTKEY;
  int O_ORDERKEY;
  friend std::ostream& operator<<(std::ostream& os, const Q10_PR_ORDER& output);
};

class q10_converter_PR_ORDER {
 public:
  typedef Q10_PR_ORDER key_type;

  q10_converter_PR_ORDER() {}
  inline key_type convert(const ORDER& w) const {
    key_type localVariable;
    localVariable.O_CUSTKEY = w.O_CUSTKEY;
    localVariable.O_ORDERKEY = w.O_ORDERKEY;
    return localVariable;
  }
  inline key_type operator()(const ORDER& w) const { return convert(w); }
};

class q10_extractor_PR_ORDER {
 public:
  typedef int key_type;

  q10_extractor_PR_ORDER(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q10_PR_ORDER& w) const { return w.O_ORDERKEY; }
  inline key_type operator()(const Q10_PR_ORDER& w) const { return extract(w); }

 private:
  size_t m_column;
};

class q10_combinator_pr_order_pr_lineitem {
 public:
  typedef Q10_JOIN1_STRUCT_ORDER_LINEITEM record_type;

  record_type operator()(const Q10_PR_ORDER& l,
                         const Q10_PR_LINEITEM& r) const {
    record_type output;

    output.o_custkey = l.O_CUSTKEY;
    output.l_quantity = r.L_QUANTITY;
    output.l_extendedprice = r.L_EXTENDEDPRICE;

    return output;
  }
};

// *********** PROJECTED FILE QUERY10 END *************

// Query10 Node3 Join2
class q10_join2_extractor_result {
 public:
  typedef int key_type;

  q10_join2_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q10_JOIN1_STRUCT_ORDER_LINEITEM& w) const {
    return w.o_custkey;
  }
  inline key_type operator()(const Q10_JOIN1_STRUCT_ORDER_LINEITEM& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

struct Q10_JOIN2_STRUCT_RESULT_CUSTOMER {
  int o_custkey;
  double l_quantity;
  double l_extendedprice;
  char c_name[26];
  double c_acctbal;
  char c_address[41];
  char c_phone[16];
  char c_comment[118];
  int c_nationkey;

  friend std::ostream& operator<<(
      std::ostream& os, const Q10_JOIN2_STRUCT_RESULT_CUSTOMER& output);
};

class q10_combinator_result_customer {
 public:
  typedef Q10_JOIN2_STRUCT_RESULT_CUSTOMER record_type;

  record_type operator()(const Q10_JOIN1_STRUCT_ORDER_LINEITEM& l,
                         const CUSTOMER& r) const {
    record_type output;

    output.o_custkey = l.o_custkey;
    output.l_quantity = l.l_quantity;
    output.l_extendedprice = l.l_extendedprice;
    strcpy(output.c_name, r.C_NAME);
    output.c_acctbal = r.C_ACCTBAL;
    strcpy(output.c_address, r.C_ADDRESS);
    strcpy(output.c_phone, r.C_PHONE);
    strcpy(output.c_comment, r.C_COMMENT);
    output.c_nationkey = r.C_NATIONKEY;

    return output;
  }
};

// *********** PROJECTED SORTED FILE QUERY13 START *************
struct Q10_PR_CUSTOMER_SORT {
  //  double L_QUANTITY;
  //  double L_EXTENDEDPRICE;
  char C_COMMENT[118];
  char C_ADDRESS[41];
  char C_NAME[26];
  char C_PHONE[16];
  double C_ACCTBAL;
  int C_CUSTKEY;
  int C_NATIONKEY;
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q10_PR_CUSTOMER_SORT& output);
};

class q10_converter_PR_CUSTOMER_SORT {
 public:
  typedef Q10_PR_CUSTOMER_SORT key_type;

  q10_converter_PR_CUSTOMER_SORT() {}
  inline key_type convert(const CUSTOMER& w) const {
    key_type localVariable;
    localVariable.C_CUSTKEY = w.C_CUSTKEY;
    localVariable.C_NATIONKEY = w.C_NATIONKEY;
    localVariable.C_ACCTBAL = w.C_ACCTBAL;
    strcpy(localVariable.C_NAME, w.C_NAME);
    strcpy(localVariable.C_ADDRESS, w.C_ADDRESS);
    strcpy(localVariable.C_PHONE, w.C_PHONE);
    strcpy(localVariable.C_COMMENT, w.C_COMMENT);

    return localVariable;
  }
  inline key_type operator()(const CUSTOMER& w) const { return convert(w); }
};

class q10_extractor_PR_CUSTOMER_SORT {
 public:
  typedef int key_type;

  q10_extractor_PR_CUSTOMER_SORT(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q10_PR_CUSTOMER_SORT& w) const {
    return w.C_CUSTKEY;
  }
  inline key_type operator()(const Q10_PR_CUSTOMER_SORT& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

class q10_combinator_result_pr_sort_customer {
 public:
  typedef Q10_JOIN2_STRUCT_RESULT_CUSTOMER record_type;

  record_type operator()(const Q10_JOIN1_STRUCT_ORDER_LINEITEM& l,
                         const Q10_PR_CUSTOMER_SORT& r) const {
    record_type output;
    output.o_custkey = l.o_custkey;
    output.l_quantity = l.l_quantity;
    output.l_extendedprice = l.l_extendedprice;
    strcpy(output.c_name, r.C_NAME);
    output.c_acctbal = r.C_ACCTBAL;
    strcpy(output.c_address, r.C_ADDRESS);
    strcpy(output.c_phone, r.C_PHONE);
    strcpy(output.c_comment, r.C_COMMENT);
    output.c_nationkey = r.C_NATIONKEY;

    return output;
  }
};

// *********** PROJECTED SORTED FILE QUERY13 END *************

// Query10 Node4 Join3
class q10_join3_extractor_result {
 public:
  typedef int key_type;

  q10_join3_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q10_JOIN2_STRUCT_RESULT_CUSTOMER& w) const {
    return w.c_nationkey;
  }
  inline key_type operator()(const Q10_JOIN2_STRUCT_RESULT_CUSTOMER& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

struct Q10_JOIN3_STRUCT_RESULT_NATION {
  int o_custkey;
  double l_quantity;
  double l_extendedprice;
  char c_name[26];
  double c_acctbal;
  char c_address[41];
  char c_phone[16];
  char c_comment[118];
  int c_nationkey;

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q10_JOIN3_STRUCT_RESULT_NATION& output);
};

class q10_combinator_result_nation {
 public:
  typedef Q10_JOIN3_STRUCT_RESULT_NATION record_type;

  record_type operator()(const Q10_JOIN2_STRUCT_RESULT_CUSTOMER& l,
                         const NATION& r) const {
    record_type output;

    output.o_custkey = l.o_custkey;
    output.l_quantity = l.l_quantity;
    output.l_extendedprice = l.l_extendedprice;
    strcpy(output.c_name, l.c_name);
    output.c_acctbal = l.c_acctbal;
    strcpy(output.c_address, l.c_address);
    strcpy(output.c_phone, l.c_phone);
    strcpy(output.c_comment, l.c_comment);
    output.c_nationkey = r.N_NATIONKEY;

    //    c_custkey,
    //    c_name,
    //    sum(l_extendedprice * (1 - l_discount)) as revenue,
    //    c_acctbal,
    //    n_name,
    //    c_address,
    //    c_phone,
    //    c_comment

    return output;
  };
};

// Query11

struct Q11_JOIN1_STRUCT_SUPPLIER_NATION {
  int s_suppkey;

  friend std::ostream& operator<<(
      std::ostream& os, const Q11_JOIN1_STRUCT_SUPPLIER_NATION& output);
};

class q11_join1_combinator_supplier_nation {
 public:
  typedef Q11_JOIN1_STRUCT_SUPPLIER_NATION record_type;

  record_type operator()(const SUPPLIER& l, const NATION& r) const {
    record_type output;
    output.s_suppkey = l.S_SUPPKEY;

    (void)r;

    return output;
  }
};

class q11_join2_extractor_result {
 public:
  typedef int key_type;

  q11_join2_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q11_JOIN1_STRUCT_SUPPLIER_NATION& w) const {
    return w.s_suppkey;
  }
  inline key_type operator()(const Q11_JOIN1_STRUCT_SUPPLIER_NATION& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

struct Q11_JOIN2_STRUCT_PARTSUPP_RESULT {
  int ps_partkey;
  double ps_availqty;
  double ps_supplycost;

  friend std::ostream& operator<<(
      std::ostream& os, const Q11_JOIN2_STRUCT_PARTSUPP_RESULT& output);
};

class q11_join2_combinator_partsupp_result {
 public:
  typedef Q11_JOIN2_STRUCT_PARTSUPP_RESULT record_type;

  record_type operator()(const PARTSUPP& l,
                         const Q11_JOIN1_STRUCT_SUPPLIER_NATION& r) const {
    record_type output;
    output.ps_partkey = l.PS_PARTKEY;
    output.ps_availqty = l.PS_AVAILQTY;
    output.ps_supplycost = l.PS_SUPPLYCOST;

    (void)r;

    return output;
  }
};

// *********** PROJECTED FILE QUERY11 START *************
struct Q11_PR_PARSTSUPP {
  double PS_AVAILQTY;
  double PS_SUPPLYCOST;
  int PS_PARTKEY;
  int PS_SUPPKEY;

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q11_PR_PARSTSUPP& output);
};

class q11_converter_PR_PARSTSUPP {
 public:
  typedef Q11_PR_PARSTSUPP key_type;

  q11_converter_PR_PARSTSUPP() {}
  inline key_type convert(const PARTSUPP& w) const {
    key_type localVariable;
    localVariable.PS_PARTKEY = w.PS_PARTKEY;
    localVariable.PS_AVAILQTY = w.PS_AVAILQTY;
    localVariable.PS_SUPPLYCOST = w.PS_SUPPLYCOST;
    localVariable.PS_SUPPKEY = w.PS_SUPPKEY;

    return localVariable;
  }
  inline key_type operator()(const PARTSUPP& w) const { return convert(w); }
};

class q11_converter_JOIN1_STRUCT_SUPPLIER_NATION {
 public:
  typedef Q11_JOIN1_STRUCT_SUPPLIER_NATION key_type;

  q11_converter_JOIN1_STRUCT_SUPPLIER_NATION() {}
  inline key_type convert(const Q11_JOIN1_STRUCT_SUPPLIER_NATION& w) const {
    key_type localVariable;
    localVariable.s_suppkey = w.s_suppkey;
    return localVariable;
  }
  inline key_type operator()(const Q11_JOIN1_STRUCT_SUPPLIER_NATION& w) const {
    return convert(w);
  }
};

class q11_combinator_pr_supplier_other {
 public:
  typedef Q11_JOIN2_STRUCT_PARTSUPP_RESULT record_type;

  record_type operator()(const Q11_PR_PARSTSUPP& l,
                         const Q11_JOIN1_STRUCT_SUPPLIER_NATION& r) const {
    record_type output;

    output.ps_partkey = l.PS_PARTKEY;
    output.ps_availqty = l.PS_AVAILQTY;
    output.ps_supplycost = l.PS_SUPPLYCOST;

    (void)r;

    return output;
  }
};

class q11_extractor_PR_PARTSUPP {
 public:
  typedef int key_type;

  q11_extractor_PR_PARTSUPP(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q11_PR_PARSTSUPP& w) const {
    return w.PS_SUPPKEY;
  }
  inline key_type operator()(const Q11_PR_PARSTSUPP& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};
// *********** PROJECTED FILE QUERY11 END *************

struct Q11_RESULT3_STRUCT_RESULT {
  int ps_partkey;
  double value;

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q11_RESULT3_STRUCT_RESULT& output);
};

class q11_joinLast_extractor_result {
 public:
  typedef int key_type;

  q11_joinLast_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q11_RESULT3_STRUCT_RESULT& w) const {
    return w.ps_partkey;
  }
  inline key_type operator()(const Q11_RESULT3_STRUCT_RESULT& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// Query12

struct Q12_JOIN1_STRUCT_LINEITEM_ORDER {
  char l_shipmode[11];
  char o_orderpriority[16];

  friend std::ostream& operator<<(
      std::ostream& os, const Q12_JOIN1_STRUCT_LINEITEM_ORDER& output);
};

class q12_combinator_lineitem_order {
 public:
  typedef Q12_JOIN1_STRUCT_LINEITEM_ORDER record_type;

  record_type operator()(const LINEITEM& l, const ORDER& r) const {
    record_type output;
    strcpy(output.l_shipmode, l.L_SHIPMODE);
    strcpy(output.o_orderpriority, r.O_ORDERPRIORITY);

    return output;
  }
};

class q12_extractor_final_result {
 public:
  typedef std::string key_type;

  q12_extractor_final_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q12_JOIN1_STRUCT_LINEITEM_ORDER& w) const {
    return std::string(w.l_shipmode);
  }
  inline key_type operator()(const Q12_JOIN1_STRUCT_LINEITEM_ORDER& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// *********** PROJECTED FILE QUERY12 START *************
struct Q12_PR_LINEITEM {
  char L_SHIPMODE[11];
  int L_ORDERKEY;
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q12_PR_LINEITEM& output);
};

class q12_converter_PR_LINEITEM {
 public:
  typedef Q12_PR_LINEITEM key_type;

  q12_converter_PR_LINEITEM() {}
  inline key_type convert(const LINEITEM& w) const {
    key_type localVariable;
    strcpy(localVariable.L_SHIPMODE, w.L_SHIPMODE);
    localVariable.L_ORDERKEY = w.L_ORDERKEY;

    return localVariable;
  }
  inline key_type operator()(const LINEITEM& w) const { return convert(w); }
};

class q12_extractor_PR_LINEITEM {
 public:
  typedef int key_type;

  q12_extractor_PR_LINEITEM(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q12_PR_LINEITEM& w) const {
    return w.L_ORDERKEY;
  }
  inline key_type operator()(const Q12_PR_LINEITEM& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// Second table
struct Q12_PR_ORDER {
#ifdef UNIFIED
  char O_ORDERPRIORITY[16];
  double O_TOTALPRICE;
  int O_CUSTKEY;
  int O_ORDERKEY;
  int O_SHIPPRIORITY;
#else
  char O_ORDERPRIORITY[16];
  int O_ORDERKEY;
#endif
  friend std::ostream& operator<<(std::ostream& os, const Q12_PR_ORDER& output);
};

class q12_converter_PR_ORDER {
 public:
  typedef Q12_PR_ORDER key_type;

  q12_converter_PR_ORDER() {}
  inline key_type convert(const ORDER& w) const {
    key_type localVariable;
#ifdef UNIFIED
    localVariable.O_ORDERKEY = w.O_ORDERKEY;
    strcpy(localVariable.O_ORDERPRIORITY, w.O_ORDERPRIORITY);
    localVariable.O_SHIPPRIORITY = w.O_SHIPPRIORITY;
    localVariable.O_CUSTKEY = w.O_CUSTKEY;
    localVariable.O_TOTALPRICE = w.O_TOTALPRICE;
#else
    localVariable.O_ORDERKEY = w.O_ORDERKEY;
    strcpy(localVariable.O_ORDERPRIORITY, w.O_ORDERPRIORITY);
#endif
    return localVariable;
  }
  inline key_type operator()(const ORDER& w) const { return convert(w); }
};

class q12_combinator_pr_lineitem_pr_order {
 public:
  typedef Q12_JOIN1_STRUCT_LINEITEM_ORDER record_type;

  record_type operator()(const Q12_PR_LINEITEM& l,
                         const Q12_PR_ORDER& r) const {
    record_type output;
    strcpy(output.l_shipmode, l.L_SHIPMODE);
    strcpy(output.o_orderpriority, r.O_ORDERPRIORITY);

    return output;
  }
};

class q12_extractor_PR_ORDER {
 public:
  typedef int key_type;

  q12_extractor_PR_ORDER(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q12_PR_ORDER& w) const { return w.O_ORDERKEY; }
  inline key_type operator()(const Q12_PR_ORDER& w) const { return extract(w); }

 private:
  size_t m_column;
};

// *********** PROJECTED FILE QUERY12 END *************

// query 13

struct QUERY13_STRUCT_CUSTOMER_ORDER {
  int c_custkey;
  friend std::ostream& operator<<(std::ostream& os,
                                  const QUERY13_STRUCT_CUSTOMER_ORDER& output);
};

class q13_join1_combinator_customer_order {
 public:
  typedef QUERY13_STRUCT_CUSTOMER_ORDER record_type;

  record_type operator()(const CUSTOMER& l, const ORDER& r) const {
    record_type output;
    output.c_custkey = l.C_CUSTKEY;
    (void)r;

    return output;
  }
};

class q13_extractor_final_result {
 public:
  typedef int key_type;

  q13_extractor_final_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const QUERY13_STRUCT_CUSTOMER_ORDER& w) const {
    return w.c_custkey;
  }
  inline key_type operator()(const QUERY13_STRUCT_CUSTOMER_ORDER& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// *********** PROJECTED SORTED FILE QUERY13 START *************
struct Q13_PR_ORDER_SORT {
  //  double L_QUANTITY;
  //  double L_EXTENDEDPRICE;
  int O_CUSTKEY;
  //  int L_PARTKEY;
  //  int L_SUPPKEY;
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q13_PR_ORDER_SORT& output);
};

class q13_converter_PR_ORDER_SORT {
 public:
  typedef Q13_PR_ORDER_SORT key_type;

  q13_converter_PR_ORDER_SORT() {}
  inline key_type convert(const ORDER& w) const {
    key_type localVariable;
    localVariable.O_CUSTKEY = w.O_CUSTKEY;

    return localVariable;
  }
  inline key_type operator()(const ORDER& w) const { return convert(w); }
};

class q13_extractor_PR_ORDER_SORT {
 public:
  typedef int key_type;

  q13_extractor_PR_ORDER_SORT(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q13_PR_ORDER_SORT& w) const {
    return w.O_CUSTKEY;
  }
  inline key_type operator()(const Q13_PR_ORDER_SORT& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

class q13_combinator_customer_pr_sort_order {
 public:
  typedef QUERY13_STRUCT_CUSTOMER_ORDER record_type;

  record_type operator()(const CUSTOMER& l, const Q13_PR_ORDER_SORT& r) const {
    record_type output;
    output.c_custkey = l.C_CUSTKEY;
    (void)r;

    return output;
  }
};
// *********** PROJECTED SORTED FILE QUERY13 END *************

// Query 16
struct Q16_JOIN1_STRUCT_PART_PARTSUPP {
  char P_TYPE[26];
  char P_BRAND[11];
  int p_partkey;
  int p_size;
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q16_JOIN1_STRUCT_PART_PARTSUPP& output);
};

class q16_join1_combinator_part_partsupp {
 public:
  typedef Q16_JOIN1_STRUCT_PART_PARTSUPP record_type;

  record_type operator()(const PART& l, const PARTSUPP& r) const {
    record_type output;
    output.p_partkey = l.P_PARTKEY;
    output.p_size = l.P_SIZE;
    (void)r;

    return output;
  }
};

// *********** PROJECTED FILE QUERY16 START *************
// First projected table
struct Q16_PR_PART {
  char P_TYPE[26];
  char P_BRAND[11];
  int P_SIZE;
  int P_PARTKEY;

  friend std::ostream& operator<<(std::ostream& os, const Q16_PR_PART& output);
};

class q16_converter_PR_PART {
 public:
  typedef Q16_PR_PART key_type;

  q16_converter_PR_PART() {}
  inline key_type convert(const PART& w) const {
    key_type localVariable;
    localVariable.P_PARTKEY = w.P_PARTKEY;
    localVariable.P_SIZE = w.P_SIZE;
    strcpy(localVariable.P_TYPE, w.P_TYPE);
    strcpy(localVariable.P_BRAND, w.P_BRAND);

    return localVariable;
  }
  inline key_type operator()(const PART& w) const { return convert(w); }
};

class q16_extractor_PR_PART {
 public:
  typedef int key_type;

  q16_extractor_PR_PART(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q16_PR_PART& w) const { return w.P_PARTKEY; }
  inline key_type operator()(const Q16_PR_PART& w) const { return extract(w); }

 private:
  size_t m_column;
};

// Second projected table
struct Q16_PR_PARTSUPP {
#ifdef UNIFIED
  double PS_SUPPLYCOST;
  int PS_SUPPKEY;
  int PS_PARTKEY;
#else
  int PS_SUPPKEY;
  int PS_PARTKEY;
#endif
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q16_PR_PARTSUPP& output);
};

class q16_converter_PR_PARTSUPP {
 public:
  typedef Q16_PR_PARTSUPP key_type;

  q16_converter_PR_PARTSUPP() {}
  inline key_type convert(const PARTSUPP& w) const {
    key_type localVariable;
#ifdef UNIFIED
    localVariable.PS_SUPPLYCOST = w.PS_SUPPLYCOST;
    localVariable.PS_SUPPKEY = w.PS_SUPPKEY;
    localVariable.PS_PARTKEY = w.PS_PARTKEY;
#else
    localVariable.PS_SUPPKEY = w.PS_SUPPKEY;
    localVariable.PS_PARTKEY = w.PS_PARTKEY;
#endif

    return localVariable;
  }
  inline key_type operator()(const PARTSUPP& w) const { return convert(w); }
};

class q16_extractor_PR_PARTSUPP {
 public:
  typedef int key_type;

  q16_extractor_PR_PARTSUPP(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q16_PR_PARTSUPP& w) const {
    return w.PS_PARTKEY;
  }
  inline key_type operator()(const Q16_PR_PARTSUPP& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

class q16_join1_combinator_pr_part_pr_partsupp {
 public:
  typedef Q16_JOIN1_STRUCT_PART_PARTSUPP record_type;

  record_type operator()(const Q16_PR_PART& l, const Q16_PR_PARTSUPP& r) const {
    record_type output;
    output.p_partkey = l.P_PARTKEY;
    output.p_size = l.P_SIZE;
    strcpy(output.P_TYPE, l.P_TYPE);
    strcpy(output.P_BRAND, l.P_BRAND);
    (void)r;

    return output;
  }
};

// *********** PROJECTED FILE QUERY16 END *************

// Query2

//  ----- First Join ------ //

struct Q2_JOIN1_STRUCT_SUPPLIER_PARTSUPP {
  double s_acctbal;
  int s_suppkey;
  double ps_supplycost;
  int s_nationkey;
  char s_name[26];
  friend std::ostream& operator<<(
      std::ostream& os, const Q2_JOIN1_STRUCT_SUPPLIER_PARTSUPP& output);
};

class q2_join1_combinator_supplier_partsupp {
 public:
  typedef Q2_JOIN1_STRUCT_SUPPLIER_PARTSUPP record_type;

  record_type operator()(const SUPPLIER& l, const PARTSUPP& r) const {
    record_type output;
    output.s_acctbal = l.S_ACCTBAL;
    output.s_suppkey = r.PS_PARTKEY;
    output.ps_supplycost = r.PS_SUPPLYCOST;
    strcpy(output.s_name, l.S_NAME);
    output.s_nationkey = l.S_NATIONKEY;
    (void)l;

    return output;
  }
};

class q2_join1_extractor_result {
 public:
  typedef int key_type;
  q2_join1_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q2_JOIN1_STRUCT_SUPPLIER_PARTSUPP& w) const {
    return w.s_nationkey;
  }
  inline key_type operator()(const Q2_JOIN1_STRUCT_SUPPLIER_PARTSUPP& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

//----- Second Join ------ //
struct Q2_JOIN2_STRUCT_SUPPLIER_RESULT {
  double ps_supplycost;
  double s_acctbal;
  int s_suppkey;
  int s_nationkey;
  int n_regionkey;
  char s_name[26];
  friend std::ostream& operator<<(
      std::ostream& os, const Q2_JOIN2_STRUCT_SUPPLIER_RESULT& output);
};

class q2_join2_combinator_nation_result {
 public:
  typedef Q2_JOIN2_STRUCT_SUPPLIER_RESULT record_type;

  record_type operator()(const NATION& l,
                         const Q2_JOIN1_STRUCT_SUPPLIER_PARTSUPP& r) const {
    record_type output;
    output.s_nationkey = l.N_NATIONKEY;
    output.ps_supplycost = r.ps_supplycost;
    output.s_acctbal = r.s_acctbal;
    output.s_suppkey = r.s_suppkey;
    strcpy(output.s_name, r.s_name);
    output.s_nationkey = r.s_nationkey;
    output.n_regionkey = l.N_REGIONKEY;

    return output;
  }
};

class q2_join2_extractor_result {
 public:
  typedef int key_type;
  q2_join2_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q2_JOIN2_STRUCT_SUPPLIER_RESULT& w) const {
    return w.s_nationkey;
  }
  inline key_type operator()(const Q2_JOIN2_STRUCT_SUPPLIER_RESULT& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// --- Join3 -----
struct Q2_JOIN3_STRUCT_NATION_RESULT {
  double ps_supplycost;
  double s_acctbal;
  int s_suppkey;
  int s_nationkey;
  int n_regionkey;
  char s_name[26];
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q2_JOIN3_STRUCT_NATION_RESULT& output);
};

class q2_join3_combinator_region_result {
 public:
  typedef Q2_JOIN3_STRUCT_NATION_RESULT record_type;

  record_type operator()(const REGION& l,
                         const Q2_JOIN2_STRUCT_SUPPLIER_RESULT& r) const {
    record_type output;
    output.s_nationkey = r.s_nationkey;
    output.ps_supplycost = r.ps_supplycost;
    output.s_acctbal = r.s_acctbal;
    output.s_suppkey = r.s_suppkey;
    strcpy(output.s_name, r.s_name);
    output.s_nationkey = r.s_nationkey;
    output.n_regionkey = l.R_REGIONKEY;

    return output;
  }
};

class q2_join3_extractor_result {
 public:
  typedef int key_type;
  q2_join3_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q2_JOIN3_STRUCT_NATION_RESULT& w) const {
    switch (m_column) {
      case 0:
        return w.n_regionkey;
      case 1:
        return w.s_suppkey;
      default:
        return w.n_regionkey;
    }
  }
  inline key_type operator()(const Q2_JOIN3_STRUCT_NATION_RESULT& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// 4th Join
//  ----- Fourth Join ------ //

struct Q2_JOIN4_STRUCT_PARTSUPP_PART {
  char P_MFGR[26];
  int ps_suppkey;
  double ps_supplycost;
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q2_JOIN4_STRUCT_PARTSUPP_PART& output);
};

class q2_join4_combinator_partsupp_part {
 public:
  typedef Q2_JOIN4_STRUCT_PARTSUPP_PART record_type;

  record_type operator()(const PARTSUPP& l, const PART& r) const {
    record_type output;
    output.ps_supplycost = l.PS_SUPPLYCOST;
    output.ps_suppkey = l.PS_SUPPKEY;
    (void)r;

    return output;
  }
};

class q2_join4_extractor_result {
 public:
  typedef int key_type;
  q2_join4_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q2_JOIN4_STRUCT_PARTSUPP_PART& w) const {
    return w.ps_suppkey;
  }
  inline key_type operator()(const Q2_JOIN4_STRUCT_PARTSUPP_PART& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// 5th Join
//  ----- Fifth Join ------ //

struct Q2_JOIN5_STRUCT_SUPPLIER_RESULT {
  int ps_suppkey;
  double ps_supplycost;
  int s_nationkey;
  friend std::ostream& operator<<(
      std::ostream& os, const Q2_JOIN5_STRUCT_SUPPLIER_RESULT& output);
};

class q2_join5_combinator_supplier_result {
 public:
  typedef Q2_JOIN5_STRUCT_SUPPLIER_RESULT record_type;

  record_type operator()(const SUPPLIER& l,
                         const Q2_JOIN4_STRUCT_PARTSUPP_PART& r) const {
    record_type output;
    output.s_nationkey = l.S_NATIONKEY;
    output.ps_supplycost = r.ps_supplycost;
    output.ps_suppkey = l.S_SUPPKEY;
    (void)r;

    return output;
  }
};

class q2_join5_extractor_result {
 public:
  typedef int key_type;
  q2_join5_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q2_JOIN5_STRUCT_SUPPLIER_RESULT& w) const {
    switch (m_column) {
      case 0:
        return w.s_nationkey;
      case 1:
        return w.ps_suppkey;
      default:
        return w.s_nationkey;
    }
  }
  inline key_type operator()(const Q2_JOIN5_STRUCT_SUPPLIER_RESULT& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// 6th Join
//  ----- Sixth Join ------ //

struct Q2_JOIN6_STRUCT_RESULT_RESULT {
  int ps_suppkey;
  double ps_supplycost;
  int s_nationkey;
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q2_JOIN6_STRUCT_RESULT_RESULT& output);
};

class q2_join6_combinator_result_result {
 public:
  typedef Q2_JOIN5_STRUCT_SUPPLIER_RESULT record_type;

  record_type operator()(const Q2_JOIN5_STRUCT_SUPPLIER_RESULT& l,
                         const Q2_JOIN3_STRUCT_NATION_RESULT& r) const {
    record_type output;
    output.s_nationkey = r.s_nationkey;
    output.ps_supplycost = r.ps_supplycost;
    output.ps_suppkey = r.s_suppkey;
    (void)l;

    return output;
  }
};

class q2_join6_extractor_result {
 public:
  typedef int key_type;
  q2_join6_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q2_JOIN6_STRUCT_RESULT_RESULT& w) const {
    return w.s_nationkey;
  }
  inline key_type operator()(const Q2_JOIN6_STRUCT_RESULT_RESULT& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// Query7

struct Q7_JOIN1_STRUCT_CUSTOMER_NATION {
  int c_custkey;
  char n_name[25];
  int n_nationkey;
  friend std::ostream& operator<<(
      std::ostream& os, const Q7_JOIN1_STRUCT_CUSTOMER_NATION& output);
};

class q7_join1_combinator_customer_nation {
 public:
  typedef Q7_JOIN1_STRUCT_CUSTOMER_NATION record_type;

  record_type operator()(const CUSTOMER& l, const NATION& r) const {
    record_type output;
    output.c_custkey = l.C_CUSTKEY;
    output.n_nationkey = r.N_NATIONKEY;
    strcpy(output.n_name, r.N_NAME);

    return output;
  }
};

// *********** PROJECTED FILE QUERY7 Join3 START *************
struct Q7_PR_CUSTOMER {
  int C_CUSTKEY;
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q7_PR_CUSTOMER& output);
};

class q7_converter_PR_CUSTOMER {
 public:
  typedef Q7_PR_CUSTOMER key_type;

  q7_converter_PR_CUSTOMER() {}
  inline key_type convert(const CUSTOMER& w) const {
    key_type localVariable;
    localVariable.C_CUSTKEY = w.C_CUSTKEY;
    return localVariable;
  }
  inline key_type operator()(const CUSTOMER& w) const { return convert(w); }
};

class q7_extractor_PR_CUSTOMER {
 public:
  typedef int key_type;

  q7_extractor_PR_CUSTOMER(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q7_PR_CUSTOMER& w) const { return w.C_CUSTKEY; }
  inline key_type operator()(const Q7_PR_CUSTOMER& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

class q7_combinator_pr_customer_nation {
 public:
  typedef Q7_JOIN1_STRUCT_CUSTOMER_NATION record_type;

  record_type operator()(const Q7_PR_CUSTOMER& l, const NATION& r) const {
    record_type output;

    output.c_custkey = l.C_CUSTKEY;
    output.n_nationkey = r.N_NATIONKEY;
    strcpy(output.n_name, r.N_NAME);
    return output;
  }
};

class q7_join1_extractor_result {
 public:
  typedef int key_type;

  q7_join1_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q7_JOIN1_STRUCT_CUSTOMER_NATION& w) const {
    return w.c_custkey;
  }
  inline key_type operator()(const Q7_JOIN1_STRUCT_CUSTOMER_NATION& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

struct Q7_JOIN2_STRUCT_ORDER_RESULT {
  int o_orderkey;
  char n_name[25];
  int n_nationkey;
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q7_JOIN2_STRUCT_ORDER_RESULT& output);
};

class q7_join2_combinator_orders_result {
 public:
  typedef Q7_JOIN2_STRUCT_ORDER_RESULT record_type;

  record_type operator()(const ORDER& l,
                         const Q7_JOIN1_STRUCT_CUSTOMER_NATION& r) const {
    record_type output;
    output.o_orderkey = l.O_ORDERKEY;
    strcpy(output.n_name, r.n_name);
    output.n_nationkey = r.n_nationkey;

    return output;
  }
};

class q7_join2_extractor_result {
 public:
  typedef int key_type;

  q7_join2_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q7_JOIN2_STRUCT_ORDER_RESULT& w) const {
    return w.o_orderkey;
  }
  inline key_type operator()(const Q7_JOIN2_STRUCT_ORDER_RESULT& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// *********** PROJECTED SORTED FILE QUERY7 START *************
struct Q7_PR_ORDER_SORT {
  int O_CUSTKEY;
  int O_ORDERKEY;
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q7_PR_ORDER_SORT& output);
};

class q7_converter_PR_ORDER_SORT {
 public:
  typedef Q7_PR_ORDER_SORT key_type;

  q7_converter_PR_ORDER_SORT() {}
  inline key_type convert(const ORDER& w) const {
    key_type localVariable;
    localVariable.O_ORDERKEY = w.O_ORDERKEY;
    localVariable.O_CUSTKEY = w.O_CUSTKEY;

    return localVariable;
  }
  inline key_type operator()(const ORDER& w) const { return convert(w); }
};

class q7_extractor_PR_ORDER_SORT {
 public:
  typedef int key_type;

  q7_extractor_PR_ORDER_SORT(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q7_PR_ORDER_SORT& w) const {
    return w.O_CUSTKEY;
  }
  inline key_type operator()(const Q7_PR_ORDER_SORT& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

class q7_join2_combinator_pr_sort_orders_result {
 public:
  typedef Q7_JOIN2_STRUCT_ORDER_RESULT record_type;

  record_type operator()(const Q7_PR_ORDER_SORT& l,
                         const Q7_JOIN1_STRUCT_CUSTOMER_NATION& r) const {
    record_type output;
    output.o_orderkey = l.O_ORDERKEY;
    strcpy(output.n_name, r.n_name);
    output.n_nationkey = r.n_nationkey;

    return output;
  }
};

// *********** PROJECTED SORTED FILE QUERY7 END *************

struct Q7_JOIN3_STRUCT_LINEITEM_RESULT {
  int l_suppkey;
  double l_extendedprice;
  double l_discount;
  char l_shipdate[11];
  char n_name[25];
  int n_nationkey;

  friend std::ostream& operator<<(
      std::ostream& os, const Q7_JOIN3_STRUCT_LINEITEM_RESULT& output);
};

class q7_join3_combinator_lineitem_result {
 public:
  typedef Q7_JOIN3_STRUCT_LINEITEM_RESULT record_type;

  record_type operator()(const LINEITEM& l,
                         const Q7_JOIN2_STRUCT_ORDER_RESULT& r) const {
    record_type output;
    output.l_suppkey = l.L_SUPPKEY;
    output.l_extendedprice = l.L_EXTENDEDPRICE;
    output.l_discount = l.L_DISCOUNT;
    strcpy(output.l_shipdate, l.L_SHIPDATE);

    strcpy(output.n_name, r.n_name);
    output.n_nationkey = r.n_nationkey;

    return output;
  }
};

// *********** PROJECTED FILE QUERY7 START *************
struct Q7_PR_LINEITEM {
  char L_SHIPDATE[11];
  int L_SUPPKEY;
  double L_EXTENDEDPRICE;
  double L_DISCOUNT;
  int L_ORDERKEY;

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q7_PR_LINEITEM& output);
};

class q7_converter_PR_LINEITEM {
 public:
  typedef Q7_PR_LINEITEM key_type;

  q7_converter_PR_LINEITEM() {}
  inline key_type convert(const LINEITEM& w) const {
    key_type localVariable;
    localVariable.L_EXTENDEDPRICE = w.L_EXTENDEDPRICE;
    localVariable.L_DISCOUNT = w.L_DISCOUNT;
    localVariable.L_SUPPKEY = w.L_SUPPKEY;
    localVariable.L_ORDERKEY = w.L_ORDERKEY;
    strcpy(localVariable.L_SHIPDATE, w.L_SHIPDATE);

    return localVariable;
  }
  inline key_type operator()(const LINEITEM& w) const { return convert(w); }
};

class q7_extractor_PR_LINEITEM {
 public:
  typedef int key_type;

  q7_extractor_PR_LINEITEM(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q7_PR_LINEITEM& w) const {
    return w.L_ORDERKEY;
  }
  inline key_type operator()(const Q7_PR_LINEITEM& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

class q7_combinator_pr_lineitem_other {
 public:
  typedef Q7_JOIN3_STRUCT_LINEITEM_RESULT record_type;

  record_type operator()(const Q7_PR_LINEITEM& l,
                         const Q7_JOIN2_STRUCT_ORDER_RESULT& r) const {
    record_type output;

    output.l_suppkey = l.L_SUPPKEY;
    output.l_extendedprice = l.L_EXTENDEDPRICE;
    output.l_discount = l.L_DISCOUNT;
    strcpy(output.l_shipdate, l.L_SHIPDATE);

    strcpy(output.n_name, r.n_name);
    output.n_nationkey = r.n_nationkey;

    return output;
  }
};

// *********** PROJECTED FILE QUERY7 END *************

class q7_join3_extractor_result {
 public:
  typedef int key_type;

  q7_join3_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q7_JOIN3_STRUCT_LINEITEM_RESULT& w) const {
    return w.l_suppkey;
  }
  inline key_type operator()(const Q7_JOIN3_STRUCT_LINEITEM_RESULT& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

struct Q7_JOIN4_STRUCT_SUPPLIER_RESULT {
  double l_extendedprice;
  double l_discount;
  char l_shipdate[11];
  char n_name[25];
  int n_nationkey;

  friend std::ostream& operator<<(
      std::ostream& os, const Q7_JOIN4_STRUCT_SUPPLIER_RESULT& output);
};

class q7_join4_combinator_supplier_result {
 public:
  typedef Q7_JOIN4_STRUCT_SUPPLIER_RESULT record_type;

  record_type operator()(const SUPPLIER& l,
                         const Q7_JOIN3_STRUCT_LINEITEM_RESULT& r) const {
    record_type output;

    output.n_nationkey = l.S_NATIONKEY;

    output.l_extendedprice = r.l_extendedprice;
    output.l_discount = r.l_discount;
    strcpy(output.l_shipdate, r.l_shipdate);
    strcpy(output.n_name, r.n_name);

    return output;
  }
};

class q7_join4_extractor_result {
 public:
  typedef int key_type;

  q7_join4_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q7_JOIN4_STRUCT_SUPPLIER_RESULT& w) const {
    return w.n_nationkey;
  }
  inline key_type operator()(const Q7_JOIN4_STRUCT_SUPPLIER_RESULT& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

struct Q7_JOIN5_STRUCT_NATION1_RESULT {
  double l_extendedprice;
  double l_discount;
  char l_shipdate[11];
  char n_name[25];
  int n_nationkey;

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q7_JOIN5_STRUCT_NATION1_RESULT& output);
};

class q7_join5_combinator_nation1_result {
 public:
  typedef Q7_JOIN5_STRUCT_NATION1_RESULT record_type;

  record_type operator()(const NATION& l,
                         const Q7_JOIN4_STRUCT_SUPPLIER_RESULT& r) const {
    record_type output;

    (void)l;

    output.l_extendedprice = r.l_extendedprice;
    output.l_discount = r.l_discount;
    strcpy(output.l_shipdate, r.l_shipdate);
    strcpy(output.n_name, r.n_name);
    output.n_nationkey = r.n_nationkey;

    return output;
  }
};

class q7_join5_extractor_result {
 public:
  typedef int key_type;

  q7_join5_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q7_JOIN5_STRUCT_NATION1_RESULT& w) const {
    return w.n_nationkey;
  }
  inline key_type operator()(const Q7_JOIN5_STRUCT_NATION1_RESULT& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

//###################

// Query3

struct Q3_JOIN1_STRUCT_ORDER_CUSTOMER {
  char o_orderdate[11];
  int o_shippriority;
  int o_orderkey;

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q3_JOIN1_STRUCT_ORDER_CUSTOMER& output);
};

class q3_join1_combinator_order_customer {
 public:
  typedef Q3_JOIN1_STRUCT_ORDER_CUSTOMER record_type;

  record_type operator()(const ORDER& l, const CUSTOMER& r) const {
    record_type output;
    strcpy(output.o_orderdate, l.O_ORDERDATE);
    output.o_shippriority = l.O_SHIPPRIORITY;
    output.o_orderkey = l.O_ORDERKEY;
    (void)r;
    return output;
  }
};

class q3_join1_extractor_result {
 public:
  typedef int key_type;

  q3_join1_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q3_JOIN1_STRUCT_ORDER_CUSTOMER& w) const {
    return w.o_orderkey;
  }
  inline key_type operator()(const Q3_JOIN1_STRUCT_ORDER_CUSTOMER& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// *********** PROJECTED FILE QUERY3 Join2 START *************
// First table
struct Q3_PR_ORDER {
  char O_ORDERDATE[11];
  int O_SHIPPRIORITY;
  int O_ORDERKEY;
  int O_CUSTKEY;
  friend std::ostream& operator<<(std::ostream& os, const Q3_PR_ORDER& output);
};

class q3_converter_PR_ORDER {
 public:
  typedef Q3_PR_ORDER key_type;

  q3_converter_PR_ORDER() {}
  inline key_type convert(const ORDER& w) const {
    key_type localVariable;

    strcpy(localVariable.O_ORDERDATE, w.O_ORDERDATE);
    localVariable.O_SHIPPRIORITY = w.O_SHIPPRIORITY;
    localVariable.O_ORDERKEY = w.O_ORDERKEY;
    localVariable.O_CUSTKEY = w.O_CUSTKEY;

    return localVariable;
  }
  inline key_type operator()(const ORDER& w) const { return convert(w); }
};

class q3_extractor_PR_ORDER {
 public:
  typedef int key_type;

  q3_extractor_PR_ORDER(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q3_PR_ORDER& w) const { return w.O_CUSTKEY; }
  inline key_type operator()(const Q3_PR_ORDER& w) const { return extract(w); }

 private:
  size_t m_column;
};

// Second table
struct Q3_PR_CUSTOMER {
  int C_CUSTKEY;
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q3_PR_CUSTOMER& output);
};

class q3_converter_PR_CUSTOMER {
 public:
  typedef Q3_PR_CUSTOMER key_type;

  q3_converter_PR_CUSTOMER() {}
  inline key_type convert(const CUSTOMER& w) const {
    key_type localVariable;
    localVariable.C_CUSTKEY = w.C_CUSTKEY;
    return localVariable;
  }
  inline key_type operator()(const CUSTOMER& w) const { return convert(w); }
};

class q3_extractor_PR_CUSTOMER {
 public:
  typedef int key_type;

  q3_extractor_PR_CUSTOMER(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q3_PR_CUSTOMER& w) const { return w.C_CUSTKEY; }
  inline key_type operator()(const Q3_PR_CUSTOMER& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

class q3_combinator_pr_order_pr_customer {
 public:
  typedef Q3_JOIN1_STRUCT_ORDER_CUSTOMER record_type;

  record_type operator()(const Q3_PR_ORDER& l, const Q3_PR_CUSTOMER& r) const {
    record_type output;

    strcpy(output.o_orderdate, l.O_ORDERDATE);
    output.o_shippriority = l.O_SHIPPRIORITY;
    output.o_orderkey = l.O_ORDERKEY;
    (void)r;
    return output;
  }
};

// *********** PROJECTED FILE QUERY3 Join2 END *************

struct Q3_JOIN2_STRUCT_LINEITEM_RESULT {
  char o_orderdate[11];
  int o_shippriority;
  int l_orderkey;
  double l_extendedprice;
  double l_discount;

  friend std::ostream& operator<<(
      std::ostream& os, const Q3_JOIN2_STRUCT_LINEITEM_RESULT& output);
};

class q3_join2_combinator_lineiten_result {
 public:
  typedef Q3_JOIN2_STRUCT_LINEITEM_RESULT record_type;

  record_type operator()(const LINEITEM& l,
                         const Q3_JOIN1_STRUCT_ORDER_CUSTOMER& r) const {
    record_type output;
    strcpy(output.o_orderdate, r.o_orderdate);
    output.o_shippriority = r.o_shippriority;
    output.l_orderkey = r.o_orderkey;
    output.l_extendedprice = l.L_EXTENDEDPRICE;
    output.l_discount = l.L_DISCOUNT;
    return output;
  }
};

class q3_join2_extractor_result {
 public:
  typedef double key_type;

  q3_join2_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q3_JOIN2_STRUCT_LINEITEM_RESULT& w) const {
    return w.l_extendedprice * (1 - w.l_discount);
  }
  inline key_type operator()(const Q3_JOIN2_STRUCT_LINEITEM_RESULT& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// *********** PROJECTED FILE QUERY3 Join3 START *************

// Third table
struct Q3_PR_LINEITEM {
  double L_EXTENDEDPRICE;
  double L_DISCOUNT;
  int L_ORDERKEY;
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q3_PR_LINEITEM& output);
};

class q3_converter_PR_LINEITEM {
 public:
  typedef Q3_PR_LINEITEM key_type;

  q3_converter_PR_LINEITEM() {}
  inline key_type convert(const LINEITEM& w) const {
    key_type localVariable;
    localVariable.L_EXTENDEDPRICE = w.L_EXTENDEDPRICE;
    localVariable.L_DISCOUNT = w.L_DISCOUNT;
    localVariable.L_ORDERKEY = w.L_ORDERKEY;
    return localVariable;
  }
  inline key_type operator()(const LINEITEM& w) const { return convert(w); }
};

class q3_extractor_PR_LINEITEM {
 public:
  typedef int key_type;

  q3_extractor_PR_LINEITEM(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q3_PR_LINEITEM& w) const {
    return w.L_ORDERKEY;
  }
  inline key_type operator()(const Q3_PR_LINEITEM& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

class q3_combinator_pr_lineitem_other {
 public:
  typedef Q3_JOIN2_STRUCT_LINEITEM_RESULT record_type;

  record_type operator()(const Q3_PR_LINEITEM& l,
                         const Q3_JOIN1_STRUCT_ORDER_CUSTOMER& r) const {
    record_type output;

    strcpy(output.o_orderdate, r.o_orderdate);
    output.o_shippriority = r.o_shippriority;
    output.l_orderkey = r.o_orderkey;
    output.l_extendedprice = l.L_EXTENDEDPRICE;
    output.l_discount = l.L_DISCOUNT;
    return output;
  }
};

// *********** PROJECTED FILE QUERY3 END *************

// *********** PROJECTED FILE QUERY17 START *************
// Query 17

// First table
struct Q17_PR_LINEITEM {
#ifdef UNIFIED
  double L_EXTENDEDPRICE;
  double L_QUANTITY;
  double L_DISCOUNT;
  int L_PARTKEY;
  int L_SUPPKEY;
  int L_ORDERKEY;
#else
  double L_EXTENDEDPRICE;
  double L_QUANTITY;
  int L_PARTKEY;
#endif

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q17_PR_LINEITEM& output);
};

class q17_extractor_PR_LINEITEM {
 public:
  typedef int key_type;

  q17_extractor_PR_LINEITEM(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q17_PR_LINEITEM& w) const {
    return w.L_PARTKEY;
  }
  inline key_type operator()(const Q17_PR_LINEITEM& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

class q17_converter_PR_LINEITEM {
 public:
  typedef Q17_PR_LINEITEM key_type;

  q17_converter_PR_LINEITEM() {}
  inline key_type convert(const LINEITEM& w) const {
    key_type localVariable;
#ifdef UNIFIED
    localVariable.L_EXTENDEDPRICE = w.L_EXTENDEDPRICE;
    localVariable.L_DISCOUNT = w.L_DISCOUNT;
    localVariable.L_PARTKEY = w.L_PARTKEY;
    localVariable.L_ORDERKEY = w.L_ORDERKEY;
    localVariable.L_SUPPKEY = w.L_SUPPKEY;
    localVariable.L_DISCOUNT = w.L_DISCOUNT;
#else
    localVariable.L_EXTENDEDPRICE = w.L_EXTENDEDPRICE;
    localVariable.L_PARTKEY = w.L_PARTKEY;
    localVariable.L_QUANTITY = w.L_QUANTITY;
#endif
    return localVariable;
  }
  inline key_type operator()(const LINEITEM& w) const { return convert(w); }
};

// Second table
struct Q17_PR_PART {
  int P_PARTKEY;

  friend std::ostream& operator<<(std::ostream& os, const Q17_PR_PART& output);
};

class q17_extractor_PR_PART {
 public:
  typedef int key_type;

  q17_extractor_PR_PART(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q17_PR_PART& w) const { return w.P_PARTKEY; }
  inline key_type operator()(const Q17_PR_PART& w) const { return extract(w); }

 private:
  size_t m_column;
};

class q17_converter_PR_PART {
 public:
  typedef Q17_PR_PART key_type;

  q17_converter_PR_PART() {}
  inline key_type convert(const PART& w) const {
    key_type localVariable;
    localVariable.P_PARTKEY = w.P_PARTKEY;

    return localVariable;
  }
  inline key_type operator()(const PART& w) const { return convert(w); }
};

class q17_combinator_part_pr_lineitem {
 public:
  typedef Q9_JOIN1_STRUCT_PART_LINEITEM record_type;

  record_type operator()(const PART& l, const Q17_PR_LINEITEM& r) const {
    record_type output;

    output.l_partkey = l.P_PARTKEY;
    output.l_orderkey = 0;
    output.l_suppkey = 0;
    output.l_extendedprice = r.L_EXTENDEDPRICE;
    output.l_discount = 0;
    output.l_quantity = r.L_QUANTITY;

    return output;
  }
};

struct Q17_JOIN1_STRUCT_PART_LINEITEM {
  double l_extendedprice;

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q17_JOIN1_STRUCT_PART_LINEITEM& output);
};

class q17_combinator_pr_part_pr_lineitem {
 public:
  typedef Q9_JOIN1_STRUCT_PART_LINEITEM record_type;

  record_type operator()(const Q17_PR_PART& l, const Q17_PR_LINEITEM& r) const {
    record_type output;
    (void)l;
    output.l_extendedprice = r.L_EXTENDEDPRICE;

    return output;
  }
};

// *********** PROJECTED FILE QUERY17 END *************

// Query 18

struct Q18_JOIN1_STRUCT_INT_ORDER {
  int O_ORDERKEY;
  int O_SHIPPRIORITY;
  int O_CUSTKEY;
  double O_TOTALPRICE;

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q18_JOIN1_STRUCT_INT_ORDER& output);
};

class q18_extractor_int {
 public:
  typedef int key_type;

  q18_extractor_int(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const int w) const { return w; }
  inline key_type operator()(int w) const { return extract(w); }

 private:
  size_t m_column;
};

class q18_combinator_int_orders {
 public:
  typedef Q18_JOIN1_STRUCT_INT_ORDER record_type;

  record_type operator()(const int l, const ORDER& r) const {
    record_type output;

    output.O_ORDERKEY = l;
    output.O_SHIPPRIORITY = r.O_SHIPPRIORITY;
    output.O_CUSTKEY = r.O_CUSTKEY;
    output.O_TOTALPRICE = r.O_TOTALPRICE;

    return output;
  }
};

// *********** PROJECTED FILE QUERY18 Node0 START *************
struct Q18_PR_ORDER {
#ifdef UNIFIED
  char O_ORDERPRIORITY[16];
  double O_TOTALPRICE;
  int O_CUSTKEY;
  int O_ORDERKEY;
  int O_SHIPPRIORITY;
#else
  double O_TOTALPRICE;
  int O_CUSTKEY;
  int O_ORDERKEY;
  int O_SHIPPRIORITY;
#endif

  friend std::ostream& operator<<(std::ostream& os, const Q18_PR_ORDER& output);
};

class q18_converter_PR_ORDER {
 public:
  typedef Q18_PR_ORDER key_type;

  q18_converter_PR_ORDER() {}
  inline key_type convert(const ORDER& w) const {
    key_type localVariable;

#ifdef UNIFIED
    localVariable.O_ORDERKEY = w.O_ORDERKEY;
    strcpy(localVariable.O_ORDERPRIORITY, w.O_ORDERPRIORITY);
    localVariable.O_SHIPPRIORITY = w.O_SHIPPRIORITY;
    localVariable.O_CUSTKEY = w.O_CUSTKEY;
    localVariable.O_TOTALPRICE = w.O_TOTALPRICE;
#else
    localVariable.O_ORDERKEY = w.O_ORDERKEY;
    localVariable.O_SHIPPRIORITY = w.O_SHIPPRIORITY;
    localVariable.O_CUSTKEY = w.O_CUSTKEY;
    localVariable.O_TOTALPRICE = w.O_TOTALPRICE;
#endif

    return localVariable;
  }
  inline key_type operator()(const ORDER& w) const { return convert(w); }
};

class q18_combinator_int_pr_orders {
 public:
  typedef Q18_JOIN1_STRUCT_INT_ORDER record_type;

  record_type operator()(const int l, const Q18_PR_ORDER& r) const {
    record_type output;

    (void)r;
    output.O_ORDERKEY = l;
    output.O_SHIPPRIORITY = r.O_SHIPPRIORITY;
    output.O_CUSTKEY = r.O_CUSTKEY;
    output.O_TOTALPRICE = r.O_TOTALPRICE;

    return output;
  }
};

class q18_extractor_PR_ORDER {
 public:
  typedef int key_type;

  q18_extractor_PR_ORDER(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q18_PR_ORDER& w) const { return w.O_ORDERKEY; }
  inline key_type operator()(const Q18_PR_ORDER& w) const { return extract(w); }

 private:
  size_t m_column;
};
// *********** PROJECTED FILE QUERY18 Node 0 END *************

class q18_extractor_join1_result {
 public:
  typedef int key_type;

  q18_extractor_join1_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q18_JOIN1_STRUCT_INT_ORDER& w) const {
    return w.O_CUSTKEY;
  }
  inline key_type operator()(const Q18_JOIN1_STRUCT_INT_ORDER& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

struct Q18_JOIN2_STRUCT_CUSTOMER_RESULT {
  int O_ORDERKEY;
  int O_SHIPPRIORITY;
  int O_CUSTKEY;
  double O_TOTALPRICE;

  friend std::ostream& operator<<(
      std::ostream& os, const Q18_JOIN2_STRUCT_CUSTOMER_RESULT& output);
};

class q18_combinator_join2_customer_result {
 public:
  typedef Q18_JOIN2_STRUCT_CUSTOMER_RESULT record_type;

  record_type operator()(const CUSTOMER& l,
                         const Q18_JOIN1_STRUCT_INT_ORDER& r) const {
    record_type output;
    (void)l;
    output.O_ORDERKEY = r.O_ORDERKEY;
    output.O_SHIPPRIORITY = r.O_SHIPPRIORITY;
    output.O_CUSTKEY = r.O_CUSTKEY;
    output.O_TOTALPRICE = r.O_TOTALPRICE;

    return output;
  }
};

class q18_extractor_join2_result {
 public:
  typedef int key_type;

  q18_extractor_join2_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q18_JOIN2_STRUCT_CUSTOMER_RESULT& w) const {
    return w.O_ORDERKEY;
  }
  inline key_type operator()(const Q18_JOIN2_STRUCT_CUSTOMER_RESULT& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

struct Q18_JOIN3_STRUCT_RESULT_LINEITEM {
  int O_ORDERKEY;
  int O_SHIPPRIORITY;
  int O_CUSTKEY;
  double O_TOTALPRICE;

  friend std::ostream& operator<<(
      std::ostream& os, const Q18_JOIN3_STRUCT_RESULT_LINEITEM& output);
};

class q18_combinator_join3_result_lineitem {
 public:
  typedef Q18_JOIN3_STRUCT_RESULT_LINEITEM record_type;

  record_type operator()(const Q18_JOIN2_STRUCT_CUSTOMER_RESULT& l,
                         const LINEITEM& r) const {
    record_type output;
    (void)r;
    output.O_ORDERKEY = l.O_ORDERKEY;
    output.O_SHIPPRIORITY = l.O_SHIPPRIORITY;
    output.O_CUSTKEY = l.O_CUSTKEY;
    output.O_TOTALPRICE = l.O_TOTALPRICE;

    return output;
  }
};

class q18_extractor_join3_result {
 public:
  typedef double key_type;

  q18_extractor_join3_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q18_JOIN3_STRUCT_RESULT_LINEITEM& w) const {
    return w.O_TOTALPRICE;
  }
  inline key_type operator()(const Q18_JOIN3_STRUCT_RESULT_LINEITEM& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// *********** PROJECTED FILE QUERY18 START *************
struct Q18_PR_LINEITEM {
#ifdef UNIFIED
  double L_EXTENDEDPRICE;
  double L_DISCOUNT;
  double L_QUANTITY;
  int L_SUPPKEY;
  int L_ORDERKEY;
#else
  double L_QUANTITY;
  int L_ORDERKEY;
#endif
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q18_PR_LINEITEM& output);
};

class q18_converter_PR_LINEITEM {
 public:
  typedef Q18_PR_LINEITEM key_type;

  q18_converter_PR_LINEITEM() {}
  inline key_type convert(const LINEITEM& w) const {
    key_type localVariable;

#ifdef UNIFIED
    localVariable.L_EXTENDEDPRICE = w.L_EXTENDEDPRICE;
    localVariable.L_DISCOUNT = w.L_DISCOUNT;
    localVariable.L_QUANTITY = w.L_QUANTITY;
    localVariable.L_SUPPKEY = w.L_SUPPKEY;
    localVariable.L_ORDERKEY = w.L_ORDERKEY;
#else
    localVariable.L_ORDERKEY = w.L_ORDERKEY;
    localVariable.L_QUANTITY = w.L_QUANTITY;
#endif

    return localVariable;
  }
  inline key_type operator()(const LINEITEM& w) const { return convert(w); }
};

class q18_combinator_part_pr_lineitem {
 public:
  typedef Q18_JOIN3_STRUCT_RESULT_LINEITEM record_type;

  record_type operator()(const Q18_JOIN2_STRUCT_CUSTOMER_RESULT& l,
                         const Q18_PR_LINEITEM& r) const {
    record_type output;

    (void)r;
    output.O_ORDERKEY = l.O_ORDERKEY;
    output.O_SHIPPRIORITY = l.O_SHIPPRIORITY;
    output.O_CUSTKEY = l.O_CUSTKEY;
    output.O_TOTALPRICE = l.O_TOTALPRICE;

    return output;
  }
};

class q18_extractor_PR_LINEITEM {
 public:
  typedef int key_type;

  q18_extractor_PR_LINEITEM(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q18_PR_LINEITEM& w) const {
    return w.L_ORDERKEY;
  }
  inline key_type operator()(const Q18_PR_LINEITEM& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};
// *********** PROJECTED FILE QUERY18 END *************

// *********** PROJECTED FILE QUERY2 START *************

// First table

struct Q2_PR_PARTSUPP {
  double PS_SUPPLYCOST;
  int PS_SUPPKEY;
  int PS_PARTKEY;
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q2_PR_PARTSUPP& output);
};

class q2_converter_PR_PARTSUPP {
 public:
  typedef Q2_PR_PARTSUPP key_type;

  q2_converter_PR_PARTSUPP() {}
  inline key_type convert(const PARTSUPP& w) const {
    key_type localVariable;
    localVariable.PS_SUPPKEY = w.PS_SUPPKEY;
    localVariable.PS_SUPPLYCOST = w.PS_SUPPLYCOST;
    localVariable.PS_PARTKEY = w.PS_PARTKEY;

    return localVariable;
  }
  inline key_type operator()(const PARTSUPP& w) const { return convert(w); }
};

class q2_extractor_PR_PARTSUPP {
 public:
  typedef int key_type;

  q2_extractor_PR_PARTSUPP(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q2_PR_PARTSUPP& w) const {
    switch (m_column) {
      case 0:
        return w.PS_PARTKEY;
      case 1:
        return w.PS_SUPPKEY;
      default:
        return w.PS_PARTKEY;
    }
  }
  inline key_type operator()(const Q2_PR_PARTSUPP& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// second table

struct Q2_PR_PART {
  char P_MFGR[26];
  int P_PARTKEY;
  friend std::ostream& operator<<(std::ostream& os, const Q2_PR_PART& output);
};

class q2_converter_PR_PART {
 public:
  typedef Q2_PR_PART key_type;

  q2_converter_PR_PART() {}
  inline key_type convert(const PART& w) const {
    key_type localVariable;

    //    p_partkey,
    //    p_mfgr,
    strcpy(localVariable.P_MFGR, w.P_MFGR);
    localVariable.P_PARTKEY = w.P_PARTKEY;

    return localVariable;
  }
  inline key_type operator()(const PART& w) const { return convert(w); }
};

class q2_extractor_PR_PART {
 public:
  typedef int key_type;

  q2_extractor_PR_PART(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q2_PR_PART& w) const { return w.P_PARTKEY; }
  inline key_type operator()(const Q2_PR_PART& w) const { return extract(w); }

 private:
  size_t m_column;
};

class q2_combinator_supplier_pr_partsupp {
 public:
  typedef Q2_JOIN1_STRUCT_SUPPLIER_PARTSUPP record_type;

  record_type operator()(const SUPPLIER& l, const Q2_PR_PARTSUPP& r) const {
    record_type output;

    output.s_acctbal = l.S_ACCTBAL;
    output.s_suppkey = r.PS_SUPPKEY;
    output.ps_supplycost = r.PS_SUPPLYCOST;
    strcpy(output.s_name, l.S_NAME);
    output.s_nationkey = l.S_NATIONKEY;
    (void)l;

    return output;
  }
};

// class q2_combinator_pr_partsupp_part {
// public:
//  typedef Q2_JOIN4_STRUCT_PARTSUPP_PART record_type;
//
//  record_type operator()(const Q2_PR_PARTSUPP& l, const PART& r) const {
//    record_type output;
//    output.ps_supplycost = l.PS_SUPPLYCOST;
//    output.ps_suppkey = l.PS_SUPPKEY;
//    (void) r;
//
//    return output;
//  }
//};

struct Q2_PR_PARTSUPP_NODE4 {
  double PS_SUPPLYCOST;
  int PS_SUPPKEY;
  int PS_PARTKEY;

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q2_PR_PARTSUPP_NODE4& output);
};

class q2_converter_PR_PARTSUPP_NODE4 {
 public:
  typedef Q2_PR_PARTSUPP_NODE4 key_type;

  q2_converter_PR_PARTSUPP_NODE4() {}
  inline key_type convert(const PARTSUPP& w) const {
    key_type localVariable;
    localVariable.PS_SUPPKEY = w.PS_SUPPKEY;
    localVariable.PS_SUPPLYCOST = w.PS_SUPPLYCOST;
    localVariable.PS_PARTKEY = w.PS_PARTKEY;

    return localVariable;
  }
  inline key_type operator()(const PARTSUPP& w) const { return convert(w); }
};

class q2_extractor_PR_PARTSUPP_NODE4 {
 public:
  typedef int key_type;

  q2_extractor_PR_PARTSUPP_NODE4(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q2_PR_PARTSUPP_NODE4& w) const {
    switch (m_column) {
      case 0:
        return w.PS_PARTKEY;
      case 1:
        return w.PS_SUPPKEY;
      default:
        return w.PS_PARTKEY;
    }
  }
  inline key_type operator()(const Q2_PR_PARTSUPP_NODE4& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

class q2_combinator_pr_partsupp_node4_pr_part {
 public:
  typedef Q2_JOIN4_STRUCT_PARTSUPP_PART record_type;

  record_type operator()(const Q2_PR_PARTSUPP_NODE4& l,
                         const Q2_PR_PART& r) const {
    record_type output;
    output.ps_supplycost = l.PS_SUPPLYCOST;
    output.ps_suppkey = l.PS_SUPPKEY;
    strcpy(output.P_MFGR, r.P_MFGR);

    return output;
  }
};

// *********** PROJECTED FILE QUERY2 END *************

// Query4

struct Q4_JOIN1_STRUCT_ORDER_LINEITEM {
  char o_orderpriority[16];

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q4_JOIN1_STRUCT_ORDER_LINEITEM& output);
};

class q4_join1_combinator_order_lineitem {
 public:
  typedef Q4_JOIN1_STRUCT_ORDER_LINEITEM record_type;

  record_type operator()(const ORDER& l, const LINEITEM& r) const {
    record_type output;

    strcpy(output.o_orderpriority, l.O_ORDERPRIORITY);
    (void)r;
    return output;
  }
};

struct Q4_GROUP_STRUCT_ORDER_LINEITEM {
  char o_orderpriority[16];
  int order_count;

  friend std::ostream& operator<<(std::ostream& os,
                                  const Q4_GROUP_STRUCT_ORDER_LINEITEM& output);
};

class q4_group_extractor_result {
 public:
  typedef std::string key_type;

  q4_group_extractor_result(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q4_GROUP_STRUCT_ORDER_LINEITEM& w) const {
    return std::string(w.o_orderpriority);
  }
  inline key_type operator()(const Q4_GROUP_STRUCT_ORDER_LINEITEM& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// *********** PROJECTED FILE QUERY4 START *************
struct Q4_PR_LINEITEM {
  int L_ORDERKEY;
  friend std::ostream& operator<<(std::ostream& os,
                                  const Q4_PR_LINEITEM& output);
};

class q4_converter_PR_LINEITEM {
 public:
  typedef Q4_PR_LINEITEM key_type;

  q4_converter_PR_LINEITEM() {}
  inline key_type convert(const LINEITEM& w) const {
    key_type localVariable;
    localVariable.L_ORDERKEY = w.L_ORDERKEY;
    return localVariable;
  }
  inline key_type operator()(const LINEITEM& w) const { return convert(w); }
};

class q4_extractor_PR_LINEITEM {
 public:
  typedef int key_type;

  q4_extractor_PR_LINEITEM(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q4_PR_LINEITEM& w) const {
    return w.L_ORDERKEY;
  }
  inline key_type operator()(const Q4_PR_LINEITEM& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

// Second table
struct Q4_PR_ORDER {
  int O_ORDERKEY;
  char O_ORDERPRIORITY[16];
  friend std::ostream& operator<<(std::ostream& os, const Q4_PR_ORDER& output);
};

class q4_converter_PR_ORDER {
 public:
  typedef Q4_PR_ORDER key_type;

  q4_converter_PR_ORDER() {}
  inline key_type convert(const ORDER& w) const {
    key_type localVariable;
    localVariable.O_ORDERKEY = w.O_ORDERKEY;
    strcpy(localVariable.O_ORDERPRIORITY, w.O_ORDERPRIORITY);
    return localVariable;
  }
  inline key_type operator()(const ORDER& w) const { return convert(w); }
};

class q4_extractor_PR_ORDER {
 public:
  typedef int key_type;

  q4_extractor_PR_ORDER(size_t col) : m_column(col) { (void)m_column; }
  inline key_type extract(const Q4_PR_ORDER& w) const { return w.O_ORDERKEY; }
  inline key_type operator()(const Q4_PR_ORDER& w) const { return extract(w); }

 private:
  size_t m_column;
};

class q4_combinator_pr_order_pr_lineitem {
 public:
  typedef Q4_JOIN1_STRUCT_ORDER_LINEITEM record_type;

  record_type operator()(const Q4_PR_ORDER& l, const Q4_PR_LINEITEM& r) const {
    record_type output;

    strcpy(output.o_orderpriority, l.O_ORDERPRIORITY);
    (void)r;

    return output;
  }
};

// *********** PROJECTED FILE QUERY4 END *************

// Transformer

struct TRANSFORMED_LINEITEM_STRUCT {
  int l_partkey;
  int tag;
  double l_quantity;
  double l_extendedprice;

  friend std::ostream& operator<<(std::ostream& os,
                                  const TRANSFORMED_LINEITEM_STRUCT& output);
};

class extractor_TRANSFORMED_LINEITEM_STRUCT {
 public:
  typedef int key_type;

  extractor_TRANSFORMED_LINEITEM_STRUCT(size_t col) : m_column(col){
    (void) m_column;
  };

  inline key_type extract(const TRANSFORMED_LINEITEM_STRUCT& w) const {
    return w.l_partkey;
  }
  inline key_type operator()(const TRANSFORMED_LINEITEM_STRUCT& w) const {
    return extract(w);
  }

 private:
  size_t m_column;
};

class transformer_lineitem {
 public:
  typedef TRANSFORMED_LINEITEM_STRUCT record_type;

  record_type operator()(const LINEITEM& l) const {
    record_type output;
    output.l_partkey = l.L_PARTKEY;
    output.tag = 0;
    output.l_quantity = l.L_QUANTITY;
    output.l_extendedprice = l.L_EXTENDEDPRICE;

    return output;
  };
};

class transformer_part {
 public:
  typedef TRANSFORMED_LINEITEM_STRUCT record_type;

  record_type operator()(const PART& l) const {
    record_type output;
    output.l_partkey = l.P_PARTKEY;
    output.tag = 1;
    output.l_quantity = 0;
    output.l_extendedprice = 0;

    return output;
  };
};

class my_combinator_transformed {
 public:
  typedef TRANSFORMED_LINEITEM_STRUCT record_type;

  record_type operator()(const TRANSFORMED_LINEITEM_STRUCT& l,
                         const TRANSFORMED_LINEITEM_STRUCT& r) const {
    record_type output;
    //    output.type = r.P_TYPE;

    output.l_partkey = l.l_partkey;
    output.l_extendedprice = l.l_extendedprice;
    output.l_quantity = l.l_quantity;

    output.tag = r.tag;

    return output;
  }
};

////Attributes
//
std::string generateFileName(const std::string& name = "join");

unsigned int generateNodeTreeId();

unsigned int generateQueryId();
}
}

#endif /* DEFINITIONS_HH_ */
