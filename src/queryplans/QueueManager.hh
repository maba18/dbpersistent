/*
 * QueueManager.hh
 *
 *  Created on: 12 Feb 2016
 *      Author: mike
 */

#ifndef QUERYPLANS_QUEUEMANAGER_HH_
#define QUERYPLANS_QUEUEMANAGER_HH_

#include "../storage/IterableQueue.hh"
#include "../storage/BufferManager.hh"

namespace deceve {

namespace storage {
class BufferManager;
}

namespace queries {

class QOperator;


class QueueManager {
 public:
  QueueManager();
  virtual ~QueueManager();

  void addQuery(QOperator *query);
  QOperator *getFirstQuery();
  void removeFirstQuery();
  size_t size();
  iterable_queue<QOperator *> *getQueue();
  void analyseQueries(unsigned int& dataStructureId);

 private:
  iterable_queue<QOperator *> queueWithTasks;
  deceve::storage::BufferManager *bm {nullptr};
};
}
} /* namespace deceve */

#endif /* QUERYPLANS_QUEUEMANAGER_HH_ */
