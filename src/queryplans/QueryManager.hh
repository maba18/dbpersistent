/*
 * QueryManager.hh
 *
 *  Created on: Jan 20, 2015
 *      Author: maba18
 */

#ifndef QUERYMANAGER_HH_
#define QUERYMANAGER_HH_

#include "../utils/types.hh"
#include <iostream>
#include <queue>
#include <vector>
#include "PoolNode.hh"
#include "QOperator.hh"

namespace deceve {
namespace queries {

class QueryManager {

private:
	std::queue<Pool_Node> all_queries;
	unsigned int dataStructureId;

public:

	QueryManager() {
		dataStructureId=0;
	}

	~QueryManager() {
		for (std::vector<QOperator *>::iterator it = query_pool.begin();
				it != query_pool.end(); ++it) {
			delete (*it);
		}
		query_pool.clear();

	}

	std::vector<QOperator *> query_pool;

	void addQuery(Pool_Node node);
	std::queue<Pool_Node>& getQueries() {
		return all_queries;
	}

	std::queue<Pool_Node>& getQuery_pool() {
		return all_queries;
	}

	void analyse_query_pool();
	void analyse_query_pool_window(size_t index, size_t window_size);
	void analyseQueries(NodeTree *t, size_t index, size_t window_size);

	void setDataStructureId(unsigned int id){
		dataStructureId = id;
	}

	unsigned int getDataStructureId(){
		return dataStructureId;
	}

	void incrementDataStructureId(){
		dataStructureId++;
	}

};

}
}

#endif /* QUERYMANAGER_HH_ */
