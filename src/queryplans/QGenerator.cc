/*
 * QGenerator.cc
 *
 *  Created on: 26 Feb 2016
 *      Author: mike
 */

#include "QGenerator.hh"

#include "../queryplans/QOperator.hh"
#include "../queryplans/QueryManager.hh"
#include "../storage/BufferManager.hh"

using namespace std;

namespace deceve {
namespace queries {

QGenerator::QGenerator(deceve::storage::BufferManager *bmr,
                       deceve::queries::QueryManager *qmr, size_t numQueries,
                       GENERATOR genMode)
    : bm(bmr), qm(qmr), totalNumberOfQueries(numQueries), mode(genMode) {
  std::cout << "Call constructor generator" << std::endl;
  generateQueries();
}

QGenerator::~QGenerator() {}

void QGenerator::generateQueries() {
  switch (mode) {
    case RANDOMLY:
      return generateRandomQueries();
    case FROM_FILE:
      return generateQueriesFromFile();
    case SKEWED:
      return generateRandomSkewedQueries();
    default:
      return generateRandomQueries();
  }
}

void QGenerator::generateRandomQueries() {
  srand(2);
  unsigned int seed = 4;
  std::vector<size_t> generatedRandomQueries;

  std::cout << "Query Stream: ";

  int counterQ = 0;
  for (size_t i = 0; i < totalNumberOfQueries; i++) {
    counterQ++;
    size_t q = rand_r(&seed) % availableTpchQueries + 1;
    generatedRandomQueries.push_back(q);
  }

  std::random_shuffle(generatedRandomQueries.begin(),
                      generatedRandomQueries.end());

  std::map<size_t, size_t> distribution;

  for (auto qId : generatedRandomQueries) {
    std::cout << "qId: " << qId << " ";
    // remember to change this
    randomQueryIds.push(qId);
    randomPauses.push(1);
    auto it = distribution.find(qId);
    if (it != distribution.end()) {
      it->second += 1;
    } else {
      distribution.insert(std::make_pair(qId, 1));
    }
  }

  std::cout << "Query Workload Details " << std::endl;
  for (const auto &mIt : distribution) {
    std::cout << "Query: " << mIt.first << " times: " << mIt.second << std::endl;
  }

  std::cout << "Total Number of queries: (random)" << randomQueryIds.size()
            << std::endl;
}

void QGenerator::generateRandomSkewedQueries() {
  srand(2);
  unsigned int seed = 4;
  std::vector<size_t> generatedRandomQueries;

  std::cout << "Query Stream: ";

  int counterQ = 0;
  for (size_t i = 0; i < totalNumberOfQueries; i++) {
    counterQ++;
    size_t q = rand_r(&seed) % availableTpchQueries + 1;

    generatedRandomQueries.push_back(q);

    if (q == 8 || q == 14 || q == 17) {
      for (int i = 0; i < 2; i++) {
        generatedRandomQueries.push_back(q);
        randomPauses.push(0);
      }
    } else if (q == 9) {
      for (int i = 0; i < 2; i++) {
        generatedRandomQueries.push_back(q);
        randomPauses.push(0);
      }
    } else if (q == 5 || q == 18) {
      for (int i = 0; i < 2; i++) {
        generatedRandomQueries.push_back(q);
        randomPauses.push(0);
      }
    } else if (q == 7 || q == 13 || q == 12) {
      for (int i = 0; i < 2; i++) {
        generatedRandomQueries.push_back(q);
        randomPauses.push(0);
      }
    } else if (q == 11 || q == 16 || q == 2) {
      for (int i = 0; i < 2; i++) {
        generatedRandomQueries.push_back(q);
        randomPauses.push(0);
      }
    } else {
      for (int i = 0; i < 2; i++) {
        generatedRandomQueries.push_back(q);
        randomPauses.push(0);
      }
    }
  }

  std::random_shuffle(generatedRandomQueries.begin(),
                      generatedRandomQueries.end());

  std::map<size_t, size_t> distribution;

  for (auto qId : generatedRandomQueries) {
    std::cout << "qId: " << qId << " ";
    // remember to change this
    randomQueryIds.push(qId);
    randomPauses.push(1);
    auto it = distribution.find(qId);
    if (it != distribution.end()) {
      it->second += 1;
    } else {
      distribution.insert(std::make_pair(qId, 1));
    }
  }

  std::cout << "Query Workload Details " << std::endl;
  for (const auto &mIt : distribution) {
    std::cout << "Query: " << mIt.first << " " << mIt.second << std::endl;
  }

  std::cout << "Total Number of queries: (skewed)" << randomQueryIds.size()
            << std::endl;
}

void QGenerator::generateQueriesFromFile() {
  ifstream inputFile("../runs/queries.txt");
  size_t qId;

  while (inputFile >> qId) {
    std::cout << "Query: " << qId << std::endl;
    randomQueryIds.push(qId);
    randomPauses.push(1);
  }
  totalNumberOfQueries = randomQueryIds.size();
  inputFile.close();

  std::cout << "\n";

  std::cout << "Total Number of queries (fromFile): " << randomQueryIds.size()
            << std::endl;
}

deceve::queries::QOperator *QGenerator::getNextQuery() {
  //  std::lock_guard<std::mutex> guard(queryGeneratorMutex);
  if (!randomQueryIds.empty()) {
    size_t randQueryId = randomQueryIds.front();
//    std::cout << "getNextQuery with id: " << randQueryId << "\n";
    randomQueryIds.pop();
    return getQueryWithId(randQueryId);
  }

  return nullptr;
}

deceve::queries::QOperator *QGenerator::getDummyQuery() {
  return getQueryWithId(0);
}

deceve::queries::QOperator *QGenerator::getQueryWithId(int qId) {
  switch (qId) {
    case 0:
      return new deceve::queries::MyQuery(bm, qm);
    case 1:
      return new deceve::queries::MyQuery1(bm, qm);
    case 2:
      return new deceve::queries::MyQuery2(bm, qm);
    case 3:
      return new deceve::queries::MyQuery3(bm, qm);
    case 4:
      return new deceve::queries::MyQuery4(bm, qm);
    case 5:
      return new deceve::queries::MyQuery5(bm, qm);
    case 6:
      return new deceve::queries::MyQuery6(bm, qm);
    case 7:
      return new deceve::queries::MyQuery7(bm, qm);
    case 8:
      return new deceve::queries::MyQuery8(bm, qm);
    case 9:
      return new deceve::queries::MyQuery9(bm, qm);
    case 10:
      return new deceve::queries::MyQuery10(bm, qm);
    case 11:
      return new deceve::queries::MyQuery11(bm, qm);
    case 12:
      return new deceve::queries::MyQuery12(bm, qm);
    case 13:
      return new deceve::queries::MyQuery13(bm, qm);
    case 14:
      return new deceve::queries::MyQuery14(bm, qm);
    case 15:
      return new deceve::queries::MyQuery15(bm, qm);
    case 16:
      return new deceve::queries::MyQuery16(bm, qm);
    case 17:
      return new deceve::queries::MyQuery17(bm, qm);
    case 18:
      return new deceve::queries::MyQuery18(bm, qm);
    case 19:
      return new deceve::queries::MyQuery19(bm, qm);
    case 20:
      return new deceve::queries::MyQueryJoin(bm);
    case 21:
      return new deceve::queries::MyQuery(bm, qm);
    default:
      return new deceve::queries::MyQuery1(bm, qm);
  }
}

bool QGenerator::areThereMoreQueriesLeft() {
  //  std::lock_guard<std::mutex> guard(queryGeneratorMutex);
  return !randomQueryIds.empty();
}

size_t QGenerator::getTotalNumberOfQueries() { return totalNumberOfQueries; }

}  // namespace queries
}  // namespace deceve
