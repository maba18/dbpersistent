/*
 * QOperator.h
 *
 *  Created on: Jan 20, 2015
 *      Author: maba18
 */

#ifndef QOPERATOR_HH_
#define QOPERATOR_HH_

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <time.h>

#include <string>
#include <iostream>
#include <map>
#include <iomanip>
#include <cstdio>
#include <fstream>
#include <cstring>
#include <vector>
#include <set>

#include <atomic>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <chrono>

#include <tuple>
#include <algorithm>

#include "../storage/IterableQueue.hh"

// include global functions
#include "../utils/types.hh"
#include "../utils/defs.hh"
#include "../utils/global.hh"
#include "../queryplans/definitions.hh"

// include storage classes
#include "../storage/BufferManager.hh"
#include "../storage/BufferPool.hh"
#include "../storage/StorageManager.hh"
// include queries classes
#include "../queryplans/Knapsack.hh"
// include queries classes
#include "../queryplans/NodeTree.hh"

namespace deceve {
namespace queries {

class QueueManager;

class QOperator {
 public:
  explicit QOperator(sto::BufferManager *bmr)
      : bm(bmr),
        rootNode(NULL),
        query_id(bmr->getSM().generateUniqueQueryId()),
        cleverFlag(),
        queryIndex(),
        windowSize(),
        dataStructureId() {}

  virtual ~QOperator() {}

  // The functions are sorted according to the following query execution flow
  // 0) , setQueueAsInput(), setCleverFlag()
  // 1) analyseQueries()
  //    a) for each exportQueryDetails()
  //    b) estimateCostOfQuery()
  // 2) reorderQueries()
  //    a) extractTable_keys()
  // 3) rewriteQueryPlan()
  //    a) visitAndChangeNode()
  //        i)   requestPersistencePermanent()
  //        ii)  requestPersistenceTemporal()
  //        iii) updateScores()
  // 4) executeQueryPlan()

  void setQueueAsInput(size_t flag) { cleverFlag = flag; }
  void setQueryIndex(iterable_queue<QOperator *> *qQueue) {
    queryQueue = qQueue;
  }

  // use this function for calling all calculating functions
  double calculateFullCost(const sto::table_key &tk,
                           bool isCandidate,
                           sto::FILE_CLASSIFIER fileClassifier);
  // before executing a query analyse
  // the current state of the task queue
  // and export details in the future references map
  void analyseQueries();
  void analyseQueries(NodeTree *root, NodeTree *t);
  double analyseQueueCosts(const sto::table_key &tk, bool isCandidate);

  // Traverses the queue and exports details about
  // data structures in a global persistedFutureFileReferences map
  void exportQueryDetails(unsigned int &dataStructureId);
  void exportQueryDetails(NodeTree *t, unsigned int &dataStructureId);
  // USEME check if this function is really used
  void exportQueryDetails(NodeTree *t, unsigned int &dataStructureId,
                          NodeTree *skipNode, bool startCounting = true);
  void exportJoinDetails(NodeTree *t, unsigned int &dataStructureId);
  void exportSortDetails(NodeTree *t, unsigned int &dataStructureId);

  // check if the query contains a specific operator
  bool doesQueryContainDS(const sto::table_key &candidateTk);
  bool doesQueryContainDS(NodeTree *t, const sto::table_key &candidateTk);

  // Cost functions
  double estimateCostOfQuery(double candidateDirtyPages,
                             const sto::table_key &candidateTk,
                             const std::string &message);
  double estimateCostOfQuery(NodeTree *t, double candidateDirtyPages,
                             const sto::table_key &candidateTk);

  // check if re-ordering the queries in the task queue
  // can improve the overall performance of the system
  void reorderQueries();

  // EXPLAINME Explain in comments what does this function do.
  void extractTable_keys(std::vector<sto::table_key> *vect);
  void extractTable_keys(NodeTree *t, std::vector<sto::table_key> *vect);

  // Before executing each query take the decision if results should be
  // materialised.
  // This operation is done by a single thread responsible for sharing. It is
  // called from startDatabase.hh file
  void rewriteQueryPlan();
  void rewriteQueryPlan(NodeTree *t);
  void visitAndChangeNode(NodeTree *t);
  // USEME Check if this function is necessary.
  void visitAndJustChangeNode(NodeTree *t);

  // All operations that  decide if the node should
  // persist results
  sto::table_classifier requestPersistence(const sto::table_key &tk,
                                           NodeTree *t);
  sto::table_classifier requestPersistenceReverse(const sto::table_key &tk,
                                                  NodeTree *t);
  bool requestTemporalPersistence(const sto::table_key &tk, NodeTree *t);
  bool requestPermanentPersistence(const sto::table_key &tk, NodeTree *t);
  void updateScores();

  // ----- QUERY EXECUTION SECTION ------
  // run query-plan operations
  virtual void run() = 0;
  void executeQueryPlan();
  // call this function for every node operator
  void executeQueryPlan(NodeTree *t);

  // check which data structures don't have
  // any more references in the future and
  // decide if should delete them or if
  // should classify them as permanent and
  // write to persistent memory
  void checkNodesThatShouldBeDeleted(
      NodeTree *currentNode, std::vector<sto::table_key> *nodesToBeDeleted);
  void checkDeleteOfKey(const sto::table_key &tk,
                        std::vector<sto::table_key> *nodesToBeDeleted);

  void unpinFiles(NodeTree *currentNode);

  // utility functions
  unsigned int getQueryId() { return query_id; }
  unsigned int getTpchId() { return tpchId; }

  // Prints all node details of a query
  void printQueryPlan();
  void printQueryPlan(NodeTree *t, int indent = 0);
  void printNewPersistedFiles();
  void printEverything(const std::vector<sto::scoreTuple> &scores);
  void printTableKeys();
  void printTableKeys(NodeTree *t);

 protected:
  sto::BufferManager *bm;
  NodeTree *rootNode;
  unsigned int query_id;
  size_t cleverFlag;
  size_t queryIndex;
  size_t windowSize;
  iterable_queue<QOperator *> *queryQueue;
  unsigned int dataStructureId;
  unsigned int tpchId{0};
  std::map<sto::table_key, NodeTree *> newPersistedFiles;
  QueueManager *queueManager{nullptr};

 private:
  int calculateAvailableFreeBudget(int totalBudgetAvailable);
  void changeTemporalToPermanent(const sto::table_key &tk);
};
}  // namespace queries
}  // namespace deceve

#endif /* QOPERATOR_HH_ */
