/*******************************************************************************
 * Copyright (c) 2016 Michail Basios and Stratis Viglas.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michail Basios - initial API and implementation
 *     Stratis Viglas - initial API and implementation
 *******************************************************************************/

#include "QOperator.hh"

#include <stddef.h>
#include <cstdbool>
#include <cstdint>
#include <deque>
#include <iterator>
#include <queue>
#include <utility>

#include "QueueManager.hh"

namespace deceve {
namespace queries {

double QOperator::calculateFullCost(const sto::table_key &tk, bool isCandidate,
                                    sto::FILE_CLASSIFIER fileClassifier) {
  if (fileClassifier == sto::PERMANENT) {
    return bm->calculateTotalScore(tk, isCandidate, fileClassifier);
  } else {
    // use pessimistic way of calculating the cost of operation
    if (bm->costCalculatorCase == 0) {
      return bm->calculateTotalScore(tk, isCandidate, fileClassifier);
    } else {
      // analyse current state of the queue
      return analyseQueueCosts(tk, isCandidate);
    }
  }
}

void QOperator::analyseQueries(NodeTree *root, NodeTree *currentNode) {
  //  std::cout << "analyseQueries: (root) " << root->tk << std::endl;
  //  std::cout << "analyseQueries: (currentNode) " << currentNode->tk <<
  //  std::endl;
  //  bm->getSM().printPersistentFutureFileReferences();
  //  bm->getSM().printFutureOrderQueries();

  // export details for the remaining operators of this query
  exportQueryDetails(root, dataStructureId, currentNode, true);

  std::lock_guard<std::recursive_mutex> lockAnalyser(
      bm->getSM().taskQueueMutex);
  for (iterable_queue<QOperator *>::iterator it = queryQueue->begin();
       it != queryQueue->end(); ++it) {
    (*it)->exportQueryDetails(dataStructureId);
    //    costOfQueries +=
    //        (*it)->estimateCostOfQuery();
  }

  //  queueManager->analyseQueries(dataStructureId);

  //  bm->getSM().printPersistentFutureFileReferences();
  //  bm->getSM().printFutureOrderQueries();

  dataStructureId = 0;
}

double QOperator::analyseQueueCosts(const sto::table_key &tk,
                                    bool isCandidate) {
  // export details for the remaining operators of this query

  std::lock_guard<std::recursive_mutex> lockAnalyser(
      bm->getSM().taskQueueMutex);

  if (!bm->isDatastructure(tk)) {
    return 0.0;
  }

  //  std::cout << "###################### ANALYSE QUEUE
  //  MIKE#####################"
  //            << std::endl;
  //  std::cout << "analyseQueueCosts for: " << tk << std::endl;
  //  std::cout << "queue length: " << queryQueue->size() << std::endl;

  double totalCostOfQueryWithoutDS = 0.0;
  double totalCostOfQueryWithDS = 0.0;

  sto::table_key dumpKey;

  double fileSizeProjected =
      bm->getSM().getPrimaryFileSizeInPagesForProjectedRelation(tk);

  bool didDSappeared = false;
  for (iterable_queue<QOperator *>::const_reverse_iterator it =
           queryQueue->rbegin();
       it != queryQueue->rend(); ++it) {
    deceve::queries::QOperator &query = *(*it);

    //    std::cout << "getQueryId(): " << query.getQueryId()
    //              << " tpchId: " << query.getTpchId() << std::endl;

    didDSappeared = (didDSappeared | query.doesQueryContainDS(tk));

    if (isCandidate) {
      double tmpCostWithoutDS = query.estimateCostOfQuery(
          0, dumpKey, "Candidate-Without-DS: " + tk.printFriendly());

      double tmpCostWithDS = 0;
      if (didDSappeared) {
        tmpCostWithDS = query.estimateCostOfQuery(
            fileSizeProjected, tk, "Candidate-With-DS: " + tk.printFriendly());
      } else {
        tmpCostWithDS = tmpCostWithoutDS;
      }

      //      std::cout << "Candidate: cost for query (tpch): " <<
      //      query.getTpchId()
      //                << " id: " << query.getQueryId() << std::endl;
      //      std::cout << "Candidate: tmpCost-DS: " << tmpCostWithoutDS <<
      //      std::endl;
      //      std::cout << "Candidate: tmpCost+DS: " << tmpCostWithDS <<
      //      std::endl;

      totalCostOfQueryWithoutDS += tmpCostWithoutDS;
      totalCostOfQueryWithDS += tmpCostWithDS;

    } else {
      // if the data structure it is not a candidate ds,
      // it means that it is already materialised and
      // for this reason its fileSize should be removed
      // from the space it occupies in the bufferpool

      double tmpCostWithoutDS = 0.0;
      double tmpCostWithDS = 0.0;
      double dirtyPagesOfTk = bm->getBufferPool().dirtyPagesForDS(tk) + 1;

      tmpCostWithoutDS = query.estimateCostOfQuery(
          -dirtyPagesOfTk, tk,
          "Materialised-Without-DS: " + tk.printFriendly());

      if (didDSappeared) {
        tmpCostWithDS = query.estimateCostOfQuery(
            0, dumpKey, "Materialised-With-DS: " + tk.printFriendly());
      } else {
        tmpCostWithDS = tmpCostWithoutDS;
      }

      //      std::cout << "Materialised: cost for query (tpch): " <<
      //      query.getTpchId()
      //                << " id: " << query.getQueryId() << std::endl;
      //      std::cout << "Materialised: tmpCost-DS: " << tmpCostWithoutDS
      //                << std::endl;
      //      std::cout << "Materialised: tmpCost+DS: " << tmpCostWithDS <<
      //      std::endl;

      totalCostOfQueryWithoutDS += tmpCostWithoutDS;
      totalCostOfQueryWithDS += tmpCostWithDS;
    }
  }

  //  std::cout << "final analyseQueueCosts: tk" << tk << std::endl;
  //  std::cout << "final alyseQueueCosts: isCandidate: " << isCandidate
  //            << std::endl;
  //  std::cout << "final analyseQueueCosts costOfQuery-DS: "
  //            << totalCostOfQueryWithoutDS + 1 << std::endl;
  //  std::cout << "final analyseQueueCosts costOfQuery+DS: "
  //            << totalCostOfQueryWithDS << std::endl;

  double finalCostOfOperator =
      totalCostOfQueryWithoutDS + 1 - totalCostOfQueryWithDS;
  //  std::cout << "FINAL COST: " << finalCostOfOperator << std::endl;

  return finalCostOfOperator;
}

void QOperator::analyseQueries() {
  std::lock_guard<std::recursive_mutex> lockAnalyser(
      bm->getSM().taskQueueMutex);
  for (iterable_queue<QOperator *>::iterator it = queryQueue->begin();
       it != queryQueue->end(); ++it) {
    (*it)->exportQueryDetails(dataStructureId);
  }
  //  bm->getSM().printPersistentFutureFileReferences();
  //  bm->getStorageManager().printFutureOrderQueries();

  dataStructureId = 0;
}

void QOperator::exportQueryDetails(unsigned int &dataStructureId) {
  exportQueryDetails(rootNode, dataStructureId);
}

void QOperator::exportQueryDetails(NodeTree *t, unsigned int &dataStructureId) {
  if (t == NULL) {
    return;
  }
  if (t->leftNode != NULL) {
    exportQueryDetails((t->leftNode), dataStructureId);
  }
  if (t->rightNode != NULL) {
    exportQueryDetails((t->rightNode), dataStructureId);
  }

  // keep information only about sort and join operations
  if (!bm->isDatastructure(t->tk)) {
    return;
  }

  if (t->tk.version == sto::SORT) {
    exportSortDetails(t, dataStructureId);
  } else if (t->tk.version == sto::HASHJOIN) {
    // Add each join table alone to the persisted_file_references.
    exportJoinDetails(t, dataStructureId);
  }
}

void QOperator::exportQueryDetails(NodeTree *t, unsigned int &dataStructureId,
                                   NodeTree *skipNode, bool startCounting) {
  if (t == NULL) return;
  // COUT << "t->id vs skipNode->id " << t->id << " " << skipNode->id <<
  // "\n";
  if (t->id <= skipNode->id) {
    // COUT << "Stop counting from now on \n";
    startCounting = false;
    return;
  }

  if (t->leftNode != NULL) {
    exportQueryDetails((t->leftNode), dataStructureId, skipNode, startCounting);
  }
  if (t->rightNode != NULL) {
    exportQueryDetails((t->rightNode), dataStructureId, skipNode,
                       startCounting);
  }

  if (!startCounting) return;

  // keep information only about sort and join operations
  // add check that the file the operator refers is a primary file
  //  if (t->tk.version != sto::SORT &&
  //      t->tk.version != sto::HASHJOIN)
  //    return;

  if (!bm->isDatastructure(t->tk)) {
    return;
  }

  if (t->tk.version == sto::SORT) {
    exportSortDetails(t, dataStructureId);
  } else if (t->tk.version == sto::HASHJOIN) {
    exportJoinDetails(t, dataStructureId);
  }
}

void QOperator::exportJoinDetails(NodeTree *t, unsigned int &dataStructureId) {
  // Add each join table alone to the persisted_file_references.
  auto lit1 = bm->getSM().getIterOfPersistedFutureFileReferences(t->left_tk);
  //  if (t->left_tk.version == sto::HASHJOIN ||
  //      t->left_tk.version == sto::SORT) {

  if (bm->isDatastructure(t->left_tk) &&
      t->left_tk.table_name != sto::OTHER_TABLENAME) {
    if (!bm->getSM().isInPersistedFutureFileReferences(t->left_tk)) {
      bm->getSM().insertInPersistedFutureFileReferences(
          std::make_pair(t->left_tk, std::make_pair(dataStructureId++, t->id)));
    } else {
      lit1->second.push_back(std::make_pair(dataStructureId++, t->id));
    }
  }
  auto rit1 = bm->getSM().getIterOfPersistedFutureFileReferences(t->right_tk);
  //  if (t->right_tk.version == sto::HASHJOIN ||
  //      t->right_tk.version == sto::SORT) {
  if (bm->isDatastructure(t->right_tk) &&
      t->right_tk.table_name != sto::OTHER_TABLENAME) {
    if (!bm->getSM().isInPersistedFutureFileReferences(t->right_tk)) {
      bm->getSM().insertInPersistedFutureFileReferences(std::make_pair(
          t->right_tk, std::make_pair(dataStructureId++, t->id)));
    } else {
      rit1->second.push_back(std::make_pair(dataStructureId++, t->id));
    }
  }
}

void QOperator::exportSortDetails(NodeTree *t, unsigned int &dataStructureId) {
  auto it1 = bm->getSM().getIterOfPersistedFutureFileReferences(t->tk);
  if (!bm->getSM().isInPersistedFutureFileReferences(t->tk)) {
    bm->getSM().insertInPersistedFutureFileReferences(
        std::make_pair(t->tk, std::make_pair(dataStructureId++, t->id)));
  } else {
    it1->second.push_back(std::make_pair(dataStructureId++, t->id));
  }
}

bool QOperator::doesQueryContainDS(const sto::table_key &candidateTk) {
  bool checkFlag = doesQueryContainDS(rootNode, candidateTk);
  //  std::cout << "Query: " << getTpchId() << " doesQueryContainDS: " <<
  //  checkFlag
  //            << std::endl;
  return checkFlag;
}

bool QOperator::doesQueryContainDS(NodeTree *t,
                                   const sto::table_key &candidateTk) {
  if (t == NULL) {
    return false;
  }

  //  std::cout << t->tk << std::endl;
  //  std::cout << candidateTk << std::endl;
  bool check = false;
  bool check1 = false, check2 = false;
  if (bm->isDatastructure(t->tk)) {
    // check if any of the two data structures of the
    // hashjoin operation is equal to the candidateTk
    if (t->tk.version == sto::HASHJOIN) {
      //      std::cout << "left_tk: " << t->left_tk << std::endl;
      //      std::cout << "right_tk: " << t->right_tk << std::endl;
      check1 = (t->left_tk == candidateTk) ? true : false;
      check2 = (t->right_tk == candidateTk) ? true : false;
      check = check1 | check2;
    }
    if (t->tk.version == sto::SORT) {
      check = (t->tk == candidateTk) ? true : false;
    }
  }

  return (check | doesQueryContainDS(t->leftNode, candidateTk) |
          doesQueryContainDS(t->rightNode, candidateTk));
}

double QOperator::estimateCostOfQuery(double candidateDirtyPages,
                                      const sto::table_key &candidateTk,
                                      const std::string &message) {
  (void) message;
  //  std::cout << "----- BEGIN ----- " << message << std::endl;
  double costOfOperation =
      estimateCostOfQuery(rootNode, candidateDirtyPages, candidateTk);
  //  std::cout << "Query: " << getTpchId() << " for: " << candidateTk
  //            << " \n for candidateDirtyPages: " << candidateDirtyPages
  //            << " costOfOperation: " << costOfOperation << std::endl;
  //  std::cout << "----- END ----- " << message << " " << candidateTk <<
  //  std::endl;

  return costOfOperation;
}

double QOperator::estimateCostOfQuery(NodeTree *t, double candidateDirtyPages,
                                      const sto::table_key &candidateTk) {
  if (t == NULL) {
    return 0.0;
  }
  double leftCost = 0.0;
  double rightCost = 0.0;

  if (t->leftNode != NULL) {
    leftCost =
        estimateCostOfQuery(t->leftNode, candidateDirtyPages, candidateTk);
  }
  if (t->rightNode != NULL) {
    rightCost =
        estimateCostOfQuery(t->rightNode, candidateDirtyPages, candidateTk);
  }

  return t->getEstimatedCost(candidateDirtyPages, candidateTk) + leftCost +
         rightCost;
}

// CHANGEME This function is complicated and huge, try to use smaller functions.
void QOperator::reorderQueries() {
  //  std::cout << "TRY TO reorderQueries" << std::endl;

  //  std::vector<std::vector<sto::ScoreKeyTuple> *>
  //  orderedTuples;
  std::vector<std::pair<double, QOperator *> > orderedTotalScoresMerged;
  std::vector<std::pair<double, QOperator *> > potentialScores;
  std::vector<std::pair<double, QOperator *> > currentScores;

  //******* EXTRACT KEYS FROM QUERIES ********
  // int localCounter = 0;
  //  for (iterable_queue<QOperator *>::iterator query = queryQueue->begin();
  //       query != queryQueue->end(); ++query) {
  std::lock_guard<std::recursive_mutex> lockAnalyser(
      bm->getSM().taskQueueMutex);

  //  bm->getSM().printPersistentFileCatalog();
  //  bm->getSM().printPersistentFutureFileReferences();

  iterable_queue<QOperator *> localQueue = *queryQueue;

  for (auto query : localQueue) {
    std::vector<sto::table_key> localVector;

    //  std::cout << "Extract keys from query (tpchID): " <<
    //  query->getTpchId()
    //  << std::endl;
    //  std::cout << "Local Counter:" << localCounter++ << std::endl;

    query->extractTable_keys(&localVector);

    double scoreOfQuery = 0.0;
    double scoreFromCurrentResults = 0.0;
    double potentialFutureScore = 0.0;
    //  std::cout << "QUERY id: " << query->getQueryId() << std::endl;
    //  std::cout << "QUERY TPCH-ID: " << query->getTpchId() << std::endl;

    for (const auto &localIterator : localVector) {
      if (bm->getSM().isInPersistedFileCatalog(localIterator)) {
        //        double futureScoreOfOperator =
        //            bm->calculateFutureScore(localIterator.second, false,
        //            false);
        //

        double futureScoreOfOperator =
            bm->calculateFutureScorePessimistic(localIterator, false, false);

        //     std::cout << "futureScoreOfOperator for not candidate: "
        //     <<
        //     futureScoreOfOperator << std::endl;
        scoreFromCurrentResults += futureScoreOfOperator;
      } else {
        //        double futureScoreOfOperator =
        //            bm->calculateFutureScore(localIterator.second, true,
        //            false);

        double futureScoreOfOperator =
            bm->calculateFutureScorePessimistic(localIterator, true, false);

        //    std::cout << "futureScoreOfOperator for candidate: " <<
        //    futureScoreOfOperator << std::endl;
        potentialFutureScore += futureScoreOfOperator;
      }
    }

    scoreOfQuery = scoreFromCurrentResults + potentialFutureScore;
    // std::cout << std::endl;

    // std::cout << "QUERY TOTAL SCORE IS: " << scoreOfQuery << std::endl;
    // std::cout << "QUERY scoreFromCurrentResults IS: " <<
    // scoreFromCurrentResults << std::endl;
    //  std::cout << "QUERY potentialFutureScore IS: " <<
    //  potentialFutureScore
    //  << std::endl;
    orderedTotalScoresMerged.push_back(std::make_pair(scoreOfQuery, query));
    currentScores.push_back(std::make_pair(scoreFromCurrentResults, query));
    potentialScores.push_back(std::make_pair(potentialFutureScore, query));

    localVector.erase(localVector.begin(), localVector.end());
  }

  //    std::cout << "UNSORTED FINAL" << std::endl;
  //    for (const auto &queryVector : orderedTuples) {
  //      std::cout << "-------- " << "TPCH-ID: " <<
  //      queryVector.second->getTpchId() << "---------- qId: "
  //                << queryVector.second->getQueryId() << std::endl;
  //      std::cout << " SCORE: " << queryVector.first << " " << std::endl;
  //      queryVector.second->printTableKeys();
  //      std::cout << std::endl;
  //    }

  std::sort(orderedTotalScoresMerged.begin(), orderedTotalScoresMerged.end(),
            [](const std::pair<double, QOperator *> &a,
               const std::pair<double, QOperator *> &b) {
              if (a.first != b.first) return a.first > b.first;
              return a.second->getTpchId() < b.second->getTpchId();
            });

  std::sort(currentScores.begin(), currentScores.end(),
            [](const std::pair<double, QOperator *> &a,
               const std::pair<double, QOperator *> &b) {
              if (a.first != b.first) return a.first > b.first;
              return a.second->getTpchId() < b.second->getTpchId();
            });

  std::sort(potentialScores.begin(), potentialScores.end(),
            [](const std::pair<double, QOperator *> &a,
               const std::pair<double, QOperator *> &b) {
              if (a.first != b.first) return a.first > b.first;
              return a.second->getTpchId() < b.second->getTpchId();
            });

  std::cout << "SORTED orderedTotalScoresMerged" << std::endl;
  for (const auto &queryVector : orderedTotalScoresMerged) {
    std::cout << "-------- "
              << "TPCH-ID: " << queryVector.second->getTpchId()
              << "---------- qId: " << queryVector.second->getQueryId()
              << std::endl;
    std::cout << " SCORE: " << queryVector.first << " " << std::endl;
    queryVector.second->printTableKeys();
    std::cout << std::endl;
  }

  std::cout << "SORTED currentScores" << std::endl;
  for (const auto &queryVector : currentScores) {
    std::cout << "-------- "
              << "TPCH-ID: " << queryVector.second->getTpchId()
              << "---------- qId: " << queryVector.second->getQueryId()
              << std::endl;
    std::cout << " SCORE: " << queryVector.first << " " << std::endl;
    queryVector.second->printTableKeys();
    std::cout << std::endl;
  }

  std::cout << "SORTED potentialScores" << std::endl;
  for (const auto &queryVector : potentialScores) {
    std::cout << "-------- "
              << "TPCH-ID: " << queryVector.second->getTpchId()
              << "---------- qId: " << queryVector.second->getQueryId()
              << std::endl;
    std::cout << " SCORE: " << queryVector.first << " " << std::endl;
    queryVector.second->printTableKeys();
    std::cout << std::endl;
  }
  //
  //  std::cout << "---QUEUE STATE----" << std::endl;
  //  for (auto query : (*queryQueue)) {
  //    std::cout << "query: " << query->getTpchId() << std::endl;
  //  }

  int numberOfQueries = 0;
  //  std::cout << "SORTED" << std::endl;

  //  for (const auto &queryVector : orderedTuples) {
  //    (*queryQueue).push(queryVector.second);
  //    (*queryQueue).pop();
  //    numberOfQueries++;
  //  }

  //  std::cout << "----FINAL QUEUE STATE BEGIN----" << std::endl;
  //  for (auto query : (*queryQueue)) {
  //    std::cout << "query: " << query->getTpchId() << std::endl;
  //  }

  //  std::cout << "SORTED currentScores " << std::endl;
  //  for (const auto &queryVector : currentScores) {
  //    std::cout << queryVector.second->getTpchId() << " " << queryVector.first
  //              << std::endl;
  //  }
  //  std::cout << "SORTED potentialScores " << std::endl;
  //  for (const auto &queryVector : potentialScores) {
  //    std::cout << queryVector.second->getTpchId() << " " << queryVector.first
  //              << std::endl;
  //  }
  //  std::cout << std::endl;

  std::vector<std::pair<double, QOperator *> > mergedScores1;
  std::vector<std::pair<double, QOperator *> > mergedScores2;
  std::vector<std::pair<double, QOperator *> > finalMergedScores;

  //  std::cout << "Start meging " << std::endl;
  for (auto &queryVector : currentScores) {
    //    std::cout << queryVector.second->getTpchId() << " " <<
    //    queryVector.first
    //              << std::endl;
    double firstScore = queryVector.first;
    double secondScore = 0.0;
    //  int secondCounter = 0;
    //    for (const auto &queryVector1 : potentialScores) {
    //      if (queryVector.second->getTpchId() ==
    //      queryVector1.second->getTpchId()) {
    //        secondScore = queryVector1.first;
    //        break;
    //      }
    //    }
    //    std::pair<double, QOperator *> pair2;
    for (size_t i = 0; i < potentialScores.size(); i++) {
      // std::cout << "queryVector." << queryVector.second->getTpchId() <<
      // " "
      //           << potentialScores[i].second->getTpchId() << std::endl;
      if (queryVector.second->getTpchId() ==
          potentialScores[i].second->getTpchId()) {
        secondScore = potentialScores[i].first;
        //          pair2.first= potentialScores[i].first;
        //          pair2.second= potentialScores[i].second;
        break;
      }
    }

    //    std::cout << firstScore << " vs " << secondScore << std::endl;
    if (firstScore >= secondScore) {
      mergedScores1.push_back(queryVector);
    } else {
      queryVector.first = secondScore;
      mergedScores2.push_back(queryVector);
    }
  }

  //  std::cout << "SORTED merged1 " << std::endl;
  //  for (const auto &queryVector : mergedScores1) {
  //    std::cout << queryVector.second->getTpchId() << " " << queryVector.first
  //              << std::endl;
  //  }
  //
  //  std::cout << "SORTED merged2 " << std::endl;
  //  for (const auto &queryVector : mergedScores2) {
  //    std::cout << queryVector.second->getTpchId() << " " << queryVector.first
  //              << std::endl;
  //  }

  // sort them in the reverse order
  std::sort(mergedScores2.begin(), mergedScores2.end(),
            [](const std::pair<double, QOperator *> &a,
               const std::pair<double, QOperator *> &b) {
              if (a.first != b.first) return a.first > b.first;
              return a.second->getTpchId() < b.second->getTpchId();
              //    return a.first > b.first;
            });

  //  std::cout << "Final SORTED merged2 " << std::endl;
  //  for (const auto &queryVector : mergedScores2) {
  //    std::cout << queryVector.second->getTpchId() << " " << queryVector.first
  //              << std::endl;
  //  }

  for (const auto &queryVector : mergedScores1) {
    finalMergedScores.push_back(queryVector);
  }

  //  for (auto it = mergedScores2.rbegin(); it != mergedScores2.rend(); ++it)
  //  {
  //    finalMergedScores.push_back(*it);
  //  }

  for (auto it = mergedScores2.rbegin(); it != mergedScores2.rend(); ++it) {
    finalMergedScores.push_back(*it);
  }

  //  std::cout << "SORTED merged " << std::endl;
  //  for (const auto &queryVector : finalMergedScores) {
  //    std::cout << queryVector.second->getTpchId() << " " << queryVector.first
  //              << std::endl;
  //  }

  // Default behaviour
  //  if (bm->getStorageManager().getOrderFlag() == 0) {orderedTuples
  if (bm->getSM().getNumberOfThreads() == 1) {
    //    std::cout << "ORDER WITHOUT MERGING orderedTuples" << std::endl;
    for (const auto &queryVector : orderedTotalScoresMerged) {
      (*queryQueue).push(queryVector.second);
      (*queryQueue).pop();
      numberOfQueries++;
    }
  } else {
    //    std::cout << "ORDER WITH MERGING finalMergedScores" << std::endl;
    for (const auto &queryVector : finalMergedScores) {
      (*queryQueue).push(queryVector.second);
      (*queryQueue).pop();
      numberOfQueries++;
    }
  }

  std::cout << "----FINAL QUEUE STATE BEGIN----" << std::endl;
  for (auto query : (*queryQueue)) {
    std::cout << "query: " << query->getTpchId() << std::endl;
  }

  std::cout << "----FINAL QUEUE STATE END----" << std::endl;
  //
  //  std::cout << "----------" << std::endl;
  //
  //  bm->getSM().printPersistentFileCatalog();
  //  bm->getSM().printPersistentFutureFileReferences();

  orderedTotalScoresMerged.clear();
  currentScores.clear();
  potentialScores.clear();
  mergedScores1.clear();
  mergedScores2.clear();
  finalMergedScores.clear();
}

void QOperator::extractTable_keys(std::vector<sto::table_key> *vect) {
  extractTable_keys(rootNode, vect);
}

void QOperator::extractTable_keys(NodeTree *t,
                                  std::vector<sto::table_key> *vect) {
  if (t == NULL) {
    return;
  }

  if (t->leftNode != NULL) {
    extractTable_keys((t->leftNode), vect);
  }
  if (t->rightNode != NULL) {
    extractTable_keys((t->rightNode), vect);
  }

  // keep information only about sort and join operations
  if (!bm->isDatastructure(t->tk)) {
    return;
  }

  if (t->tk.version == sto::SORT) {
    vect->push_back(t->tk);
  } else if (t->tk.version == sto::HASHJOIN) {
    if (t->left_tk.version == sto::HASHJOIN ||
        t->left_tk.version == sto::SORT) {
      vect->push_back(t->left_tk);
    }
    //    }
    if (t->right_tk.version == sto::HASHJOIN ||
        t->right_tk.version == sto::SORT) {
      vect->push_back(t->right_tk);
    }
  }
}

void QOperator::rewriteQueryPlan() {
  // std::cout << "Start rewriteQueryPlan: " << getTpchId()
  //           << " for id: " << getQueryId() << std::endl;
  //  bm->getStorageManager().reorderCounter++;
  rewriteQueryPlan(rootNode);
}

void QOperator::rewriteQueryPlan(NodeTree *currentNode) {
  if (currentNode == nullptr) return;

  if (currentNode->leftNode != nullptr) {
    rewriteQueryPlan((currentNode->leftNode));
  }
  if (currentNode->rightNode != nullptr) {
    rewriteQueryPlan((currentNode->rightNode));
  }

  if (cleverFlag) {
    //    if (currentNode->tk.version == sto::HASHJOIN ||
    //        currentNode->tk.version == sto::SORT) {

    if (bm->isDatastructure(currentNode->tk)) {
      //      std::lock_guard<std::mutex> lockAnalyser(
      //          bm->getStorageManager().queryOperatorsMutex);

      //      std::cout << "RewriteQueryPlan:  for node" << currentNode->tk
      //                << " for id: " << getQueryId() << std::endl;
      dataStructureId = 0;

      // Analyse current state of the task queue
      //      std::cout << "ANALYSE FUTURE QUERIES" << std::endl;

      analyseQueries(rootNode, currentNode);

      // Decide if should change the node
      //      std::cout << "--------- VISIT AND CHANGE QUERY START: ------------
      //      "
      //                << "tpch: " << getTpchId() << " id:" << getQueryId()
      //                << std::endl
      //                << std::endl;
      visitAndChangeNode(currentNode);
      //      std::cout << "--------- VISIT AND CHANGE QUERY END: ------------ "
      //                << getTpchId() << std::endl
      //                << std::endl;

      {
// If an operator uses results, check if the result should be
// still kept
//        std::lock_guard<std::mutex> lockAnalyser(
//            bm->getSM().bufferManagerOperationMutex);

#ifdef MULTI_THREADED
        std::lock_guard<std::mutex> lockAnalyser(
            bm->getSM().bufferManagerOperationMutex);
#endif

        //        std::cout << "update scores" << std::endl;
        updateScores();

        // keep information about future references
        // inside the node store
        // std::cout << "clear references" << std::endl;
        if (currentNode->tk.version == sto::SORT) {
          if (bm->getSM().isInPersistedFutureFileReferences(currentNode->tk)) {
            currentNode->futureAppearances.insert(currentNode->tk);
          }
        } else if (currentNode->tk.version == sto::HASHJOIN) {
          if (bm->getSM().isInPersistedFutureFileReferences(
                  currentNode->left_tk)) {
            currentNode->futureAppearances.insert(currentNode->left_tk);
          }
          if (bm->getSM().isInPersistedFutureFileReferences(
                  currentNode->right_tk)) {
            currentNode->futureAppearances.insert(currentNode->right_tk);
          }
        }

        // Silly right now as I need to analyse and clear the task queue
        // for every operator
        bm->getSM().clearPersistedFutureFileReferences();
      }
    }
  }
}

void QOperator::visitAndChangeNode(NodeTree *t) {
  if (t == NULL) return;

  // ######### Check if can use results #########
  if (t->tk.version == sto::SORT) {
    auto git = bm->getSM().getIterOfPersistedFileCatalog(t->tk);
    // Check if file is already persisted
    if (bm->isMaterialised(t->tk)) {
      t->usePersistedResult(git->second, 0);
      git->second.pins++;
      return;
    } else if (bm->isBeingGenerated(t->tk) &&
               getTpchId() !=
                   11) {  // Check if file is currently being generated
                          //  by other thread and wait until
      unique_lock<mutex> lock(bm->getBufferPool().condMutex);
      // wait until result is ready
      bm->getBufferPool().condVariable.wait(
          lock, [&]() { return !(bm->isBeingGenerated(t->tk)); });

      git = bm->getSM().getIterOfPersistedFileCatalog(t->tk);
      t->usePersistedResult(git->second, 0);
      git->second.pins++;
      return;
    }
  }

  if (t->tk.version == sto::HASHJOIN) {
    if (getTpchId() != 11 && bm->isBeingGenerated(t->left_tk)) {
      unique_lock<mutex> lock(bm->getBufferPool().condMutex);
      bm->getBufferPool().condVariable.wait(
          lock, [&]() { return !(bm->isBeingGenerated(t->left_tk)); });
    }

    if (getTpchId() != 11 && bm->isBeingGenerated(t->right_tk)) {
      unique_lock<mutex> lock(bm->getBufferPool().condMutex);
      bm->getBufferPool().condVariable.wait(
          lock, [&]() { return !(bm->isBeingGenerated(t->right_tk)); });
    }

    // get join left file details
    auto lgit = bm->getSM().getIterOfPersistedFileCatalog(t->left_tk);

    // get join right file details
    auto rgit = bm->getSM().getIterOfPersistedFileCatalog(t->right_tk);

    // check if both nodes exist in catalog;
    if (bm->isMaterialised(t->left_tk) && bm->isMaterialised(t->right_tk)) {
      lgit->second.pins++;
      rgit->second.pins++;
      t->usePersistedResult(lgit->second, rgit->second, 0);
      return;
    }
    // check if only the left file is materialised
    if (bm->isMaterialised(t->left_tk) && !bm->isMaterialised(t->right_tk)) {
      lgit->second.pins++;
      t->useLeftPersistedResult(lgit->second, 1);
      // try to persist result for right node
      sto::table_classifier rightDecision;
      rightDecision = requestPersistence(t->right_tk, t);

      if (rightDecision.decisionFlag) {
        if (!bm->isBeingGenerated(t->right_tk)) {
          bm->getSM().insertInPersistedTmpFileSet(t->right_tk,
                                                  rightDecision.fileClassifier);
          t->persistRightResult(t->right_tk, 2);
        }
      }
      return;
    }
    // check if only the right file is materialised
    if (!bm->isMaterialised(t->left_tk) && bm->isMaterialised(t->right_tk)) {
      rgit->second.pins++;
      t->useRightPersistedResult(rgit->second, 2);

      // try to persist result for right node
      sto::table_classifier leftDecision;
      leftDecision = requestPersistence(t->left_tk, t);

      if (leftDecision.decisionFlag) {
        if (!bm->isBeingGenerated(t->left_tk)) {
          bm->getSM().insertInPersistedTmpFileSet(t->left_tk,
                                                  leftDecision.fileClassifier);
          t->persistLeftResult(t->left_tk, 1);
        }
      }
      return;
    }
  }

  // ######### Check if should persist results #########

  if (t->tk.version == sto::SORT) {
    // COUT << "TRY TO PERSIST SORT RESULT FOR NODE :" << it->second <<
    // "\n";
    // Check that no-other thread is currently creating the same persistent
    // file
    if (!bm->isBeingGenerated(t->tk)) {
      sto::table_classifier decision = requestPersistence(t->tk, t);

      if (decision.decisionFlag) {
        //        COUT << "DECISION: TRUE, PERSIST SORT RESULTS"
        //             << "\n";
        bm->getSM().insertInPersistedTmpFileSet(t->tk, decision.fileClassifier);
        //        t->persistResult(0);

        // I think that the following line is not correct
        t->persistResult(t->left_tk, t->right_tk, 0);
        //        candidateNodes.push_back(t);
      }
      // else {
      //   COUT << "DECISION: FALSE." << "\n";
      // }
    }

  } else if (t->tk.version == sto::HASHJOIN) {
    // the following variable determines what files to persist (0 : for
    // both,
    // 1
    // : for left, and 2: for right)
    //    int left_matches_counter = 0;
    //    int right_matches_counter = 0;
    //    COUT << "TRY TO PERSIST JOIN RESULT FOR NODE :" << it->second <<
    //    "\n";

    sto::table_classifier leftDecision;
    sto::table_classifier rightDecision;

    leftDecision = requestPersistence(t->left_tk, t);
    rightDecision = requestPersistence(t->right_tk, t);

    if (leftDecision.decisionFlag && rightDecision.decisionFlag) {
      // COUT << "Final Decision: PERSIST BOTH LEFT AND RIGHT FILES" <<
      // "\n";

      if (!bm->isBeingGenerated(t->left_tk) &&
          !bm->isBeingGenerated(t->right_tk)) {
        bm->getSM().insertInPersistedTmpFileSet(t->left_tk,
                                                leftDecision.fileClassifier);
        bm->getSM().insertInPersistedTmpFileSet(t->right_tk,
                                                rightDecision.fileClassifier);

        t->persistResult(t->left_tk, t->right_tk, 0);

      } else if (!bm->isBeingGenerated(t->left_tk) &&
                 bm->isBeingGenerated(t->right_tk)) {
        bm->getSM().insertInPersistedTmpFileSet(t->left_tk,
                                                leftDecision.fileClassifier);
        t->persistLeftResult(t->left_tk, 1);
      } else if (bm->isBeingGenerated(t->left_tk) &&
                 !bm->isBeingGenerated(t->right_tk)) {
        bm->getSM().insertInPersistedTmpFileSet(t->right_tk,
                                                rightDecision.fileClassifier);

        t->persistRightResult(t->right_tk, 2);
      }
    } else if (leftDecision.decisionFlag) {
      // COUT << "Final Decision: PERSIST LEFT FILE ONLY" << "\n";

      if (!bm->isBeingGenerated(t->left_tk)) {
        bm->getSM().insertInPersistedTmpFileSet(t->left_tk,
                                                leftDecision.fileClassifier);
        //        t->persistResult(1);
        // 30032015
        t->persistLeftResult(t->left_tk, 1);
      }
    } else if (rightDecision.decisionFlag) {
      // COUT << "Final Decision: PERSIST RIGH FILE ONLY" << "\n";
      if (!bm->isBeingGenerated(t->right_tk)) {
        bm->getSM().insertInPersistedTmpFileSet(t->right_tk,
                                                rightDecision.fileClassifier);
        //        t->persistResult(2);
        // 30032015
        t->persistRightResult(t->right_tk, 2);
      }
    } else {
      // COUT << "DON'T PERSIST ANYTHING" << "\n";
    }
  }
}

sto::table_classifier QOperator::requestPersistence(const sto::table_key &tk,
                                                    NodeTree *t) {
  //    return requestPersistenceReverse(tk, t);
  bool requestFlag = false;

  // std::cout << "requestPersistence for: " << tk << std::endl;
  //  std::cout << "calculateFullCostOfOperation: "
  //            << bm->calculateFullCostOfOperation(tk) << std::endl;

  sto::HistoryMode historyMode = bm->getSM().getHistoryFlag();
  if (historyMode == sto::PAST_ONLY || historyMode == sto::PAST_FUTURE) {
    requestFlag = requestPermanentPersistence(tk, t);
    if (requestFlag) {
      // std::cout << "PERMANENT TRUE: " << tk << std::endl;
      return {true, sto::PERMANENT};
    } else {
      if (bm->isDatastructure(tk)) {
        // std::cout << "PERMANENT FALSE: " << tk << std::endl;
      }
    }
  }
  if (historyMode == sto::FUTURE_ONLY || historyMode == sto::PAST_FUTURE) {
    requestFlag = requestTemporalPersistence(tk, t);
    if (requestFlag) {
      int candidateSize = static_cast<int>(
          bm->getSM().getPrimaryFileSizeInPagesForProjectedRelation(tk));

      // std::cout << "Candidate size for tk: " << tk << " : " <<
      // candidateSize
      // << std::endl;
      // Increase counter of dirty pages from intermediate results to DRAM
      double costToBeSaved =
          candidateSize * (bm->getSM().getStorageWriteDelay() +
                           bm->getSM().getStorageReadDelay());
      //      std::cout << "candidateSize: " << candidateSize <<
      //      std::endl;
      //      std::cout << "costToBeSaved: " << costToBeSaved <<
      //      std::endl;
      bm->getBufferPool().increaseCostIntermediateCache(costToBeSaved);
      bm->getBufferPool().initialiseCostOfRegion();
      bm->getBufferPool().addToTotalNumberOfDirtyIntermediatePages(
          candidateSize);
      bm->getBufferPool().addFlagNumberForIntermediatePages(candidateSize);

      //      std::cout << "TEMPORAL TRUE: " << tk << std::endl;
      return {true, sto::TEMPORAL};
    } else {
      if (bm->isDatastructure(tk)) {
        //        std::cout << "TEMPORAL FALSE: " << tk << std::endl;
      }
    }
  }
  return {false, sto::NOTCLASSIFIED};
}

sto::table_classifier QOperator::requestPersistenceReverse(
    const sto::table_key &tk, NodeTree *t) {
  bool requestFlag = false;

  sto::HistoryMode historyMode = bm->getSM().getHistoryFlag();
  if (historyMode == sto::FUTURE_ONLY || historyMode == sto::PAST_FUTURE) {
    requestFlag = requestTemporalPersistence(tk, t);
    if (requestFlag) {
      int candidateSize = static_cast<int>(
          bm->getSM().getPrimaryFileSizeInPagesForProjectedRelation(tk));
      // Increase counter of dirty pages from intermediate results to DRAM
      double costToBeSaved =
          candidateSize * (bm->getSM().getStorageWriteDelay() +
                           bm->getSM().getStorageReadDelay());
      //      std::cout << "candidateSize: " << candidateSize <<
      //      std::endl;
      //      std::cout << "costToBeSaved: " << costToBeSaved <<
      //      std::endl;
      bm->getBufferPool().increaseCostIntermediateCache(costToBeSaved);
      bm->getBufferPool().initialiseCostOfRegion();
      bm->getBufferPool().addToTotalNumberOfDirtyIntermediatePages(
          candidateSize);
      bm->getBufferPool().addFlagNumberForIntermediatePages(candidateSize);

      //   std::cout << "TEMPORAL TRUE: " << tk << std::endl;
      return {true, sto::TEMPORAL};
    }
  }
  if (historyMode == sto::PAST_ONLY || historyMode == sto::PAST_FUTURE) {
    requestFlag = requestPermanentPersistence(tk, t);
    if (requestFlag) {
      //     std::cout << "PERMANENT TRUE: " << tk << std::endl;
      return {true, sto::PERMANENT};
    }
  }
  return {false, sto::NOTCLASSIFIED};
}

bool QOperator::requestTemporalPersistence(const sto::table_key &tk,
                                           NodeTree *t) {
  //  return false;

  (void)t;

  if (!bm->isDatastructure(tk)) {
    return false;
  }

  //  bm->getSM().printPersistentFileCatalog();
  //  bm->getSM().printPersistentFutureFileReferences();

  if (!(bm->getSM().isInPersistedFutureFileReferences(tk))) {
    //    std::cout << "requestTemporalPersistence: FALSE TEMPORAL (NO FUTURE "
    //                 "REFERENCES) PERSISTENCE: "
    //              << getTpchId() << " for key: " << tk << std::endl;
    return false;
  }

  //############################ SECTION 1
  //########################################
  //#### Exclude Files that are currently generated (newPersistedFiles) from
  // the
  // Budget Size
  int candidateSize = static_cast<int>(
      bm->getSM().getPrimaryFileSizeInPagesForProjectedRelation(tk));

  int totalBudgetAvailable =
      static_cast<int>(bm->getSM().getBudgetPermanentSize());

  if (candidateSize > totalBudgetAvailable) {
    return false;
  }

  int spaceOfNewPersistedFiles =
      bm->getSM().calculateSpaceOfNewPersistedFiles();
  totalBudgetAvailable = totalBudgetAvailable - spaceOfNewPersistedFiles;

  double totalSpaceOccupiedByPinnedFiles =
      bm->getSM().calculateTotalSpaceOfPersistedFiles();

  totalBudgetAvailable = totalBudgetAvailable - totalSpaceOccupiedByPinnedFiles;

  //  std::cout << "totalBudgetAvailable: " << totalBudgetAvailable <<
  //  std::endl;
  // int spaceOfPersistedFiles = bm->getStorageManager()
  //     .getTotalSpaceOfPersistedFiles();
  //  for (auto it =
  //  bm->getStorageManager().getIteratorPersistedCatalogBegin();
  //      it != bm->getStorageManager().getIteratorPersistedCatalogEnd();
  //      ++it)
  //      {
  //    if (it->second.pins > 0) {
  //      spaceOfPersistedFiles += (int) bm->getStorageManager()
  //          .getPrimaryFileSizeInPages(it->first);
  //    }
  //  }
  // totalBudgetAvailable = totalBudgetAvailable - spaceOfPersistedFiles;

  //  std::cout << "temporalBudget: " << totalBudgetAvailable << std::endl;

  //############################ SECTION 2
  //########################################
  //############################ Delete old scores #####################
  std::vector<sto::scoreTuple> scoresForAllPersistedFiles;

  //############################ SECTION 3
  //########################################
  //############################ CALCULATE NEW SCORES
  //########################################

  for (auto it = bm->getSM().getIteratorPersistedCatalogBegin();
       it != bm->getSM().getIteratorPersistedCatalogEnd(); ++it) {
    if (it->second.pins > 0) {
      continue;
    }

    bool isCandidate = false;
    bool isWeightImportant = true;
    (void) isWeightImportant;
    //    double score =
    //        bm->calculateFutureScore(it->first, isCandidate,
    //        isWeightImportant);
    double score =
        calculateFullCost(it->first, isCandidate, deceve::storage::TEMPORAL);

    scoresForAllPersistedFiles.push_back({it->first, score, 0, 0});

    //    std::cout << "materialisedTK: " << it->first << std::endl;
    //    std::cout << "SCORE FUTURE: " << score << std::endl;
    //    std::cout << "queueScores ds score: " << it->first << " " << score
    //              << std::endl;
    //    std::cout
    //        << "###################### ANALYSE QUEUE END
    //        #####################"
    //        << std::endl;
  }

  //############################ SECTION 4
  //########################################
  // COUT << "----------- FIND CANDIDATE SCORE ----------------" << "\n";
  bool isCandidate = true;
  bool isWeightImportant = true;
  //  double candidateScore =
  //      bm->calculateFutureScore(tk, isCandidate, isWeightImportant);
  //
  // HOT 02/02/2016 analyseQueueCosts
  double candidateScore =
      calculateFullCost(tk, isCandidate, deceve::storage::TEMPORAL);

  //  std::cout << "CANDIDATE TEMPORAL Score: " << candidateScore << " "
  //            << getTpchId() << " id:" << getQueryId() << std::endl;

  scoresForAllPersistedFiles.push_back({tk, candidateScore, 0, 0});

  //  std::cout << "candidate TK: " << tk << std::endl;
  //  std::cout << "SCORE FUTURE: " << candidateScore << std::endl;
  //  std::cout << "###################### ANALYSE QUEUE END
  //  #####################"
  //            << std::endl;

  //  for (const auto &it : scoresForAllPersistedFiles) {
  //    std::cout << "Key: " << it.tk << " "
  //              << "references: " << it.numberOfReferences << std::endl;
  //  }

  //############################ USE GREEDY ALGORITHM
  //#######################################
  // COUT << "CANDIDATE SCORE: " << candidateScore << "\n";
  //########################## DATA STRUCTURE REPLACEMENT
  //#############################

  int numberOfItems = scoresForAllPersistedFiles.size();

  //  Knapsack kn(nItems + 1, budget + 1);
  Knapsack kn;

  std::vector<double> weights_filesizes;
  std::vector<double> values;

  // int fileSizeSort =
  //    static_cast<int>(bm->getStorageManager().getPrimaryFileSizeInPages(tk));

  //  std::cout << "fileSizeSort: " << fileSizeSort << std::endl;

  //  std::cout << "Calculate weight for all files: " << std::endl;
  //############## -- Add results to knapsack -- #########################
  //  std::cout << "---- SCORE FOR OTHER RESULTS -----" << getTpchId()
  //            << " id:" << getQueryId() << std::endl;
  for (size_t i = 0; i < scoresForAllPersistedFiles.size(); i++) {
    double fileSizeTemp =
        bm->getSM().getPrimaryFileSizeInPagesForProjectedRelation(
            scoresForAllPersistedFiles[i].tk);

    double fileScore = scoresForAllPersistedFiles[i].score;

    //    std::cout << "fileScore: " << fileScore << std::endl;

    weights_filesizes.push_back(fileSizeTemp);
    values.push_back(fileScore);

    //    std::cout << "Future score for: " << scoresForAllPersistedFiles[i].tk
    //              << " is: " << scoresForAllPersistedFiles[i].score <<
    //              std::endl;
  }

  std::set<int> results = kn.greedyKnapsack(numberOfItems, totalBudgetAvailable,
                                            weights_filesizes, values);

  size_t candidateId = 0;

  std::set<sto::table_key> resultsNotSelected;

  int totalSpaceLeftInBudgetForPersistentResult = totalBudgetAvailable;

  //  std::cout << "totalSpaceLeftInBudgetForPersistentResult: "
  //            << totalSpaceLeftInBudgetForPersistentResult << std::endl;
  //
  //  std::cout << "RUN KNAPSACK FINISHED" << std::endl;

  for (size_t i = 0; i < scoresForAllPersistedFiles.size(); i++) {
    if (scoresForAllPersistedFiles[i].tk == tk) {
      //   std::cout << "CANDIDATE IN NEW SCORES" << std::endl;
      candidateId = i;
      continue;
    }

    auto search = results.find(i);
    if (search == results.end()) {
      if (bm->getSM().isInPersistedFileCatalog(
              scoresForAllPersistedFiles[i].tk)) {
        //        bm->removeCompletelyPersistentFile(localScores[i].tk);

        auto temporalIterator = bm->getSM().getIterOfPersistedFileCatalog(
            scoresForAllPersistedFiles[i].tk);

        if (temporalIterator->second.fileClassifier == sto::TEMPORAL) {
          //          std::cout << "removeCompletelyPersistentFile
          //          TEMPORAL: "
          //                    << scoresForAllPersistedFiles[i].tk <<
          //                    std::endl;
          bm->removeCompletelyPersistentFile(scoresForAllPersistedFiles[i].tk);
          //          std::cout << "reqeustPersistence: removeResult: "
          //                    << scoresForAllPersistedFiles[i].tk <<
          //                    std::endl;
        } else {
          //          std::cout << "Add to Not Selected result list :
          //          "
          //                    << scoresForAllPersistedFiles[i].tk <<
          //                    std::endl;
          resultsNotSelected.insert(scoresForAllPersistedFiles[i].tk);
        }
      }
    } else {
      // Find available space left in budget
      totalSpaceLeftInBudgetForPersistentResult -= static_cast<int>(
          bm->getSM().getPrimaryFileSizeInPagesForProjectedRelation(
              scoresForAllPersistedFiles[i].tk));
      //    std::cout << "Result tk: " << scoresForAllPersistedFiles[i].tk
      //    <<
      //    "
      //    in, so reduce totalSpaceLeft to: "
      //             << totalSpaceLeftInBudgetForPersistentResult <<
      //             std::endl;
    }
  }

  // Seach for candidate node which is added last in the scores vector
  auto searchCandidate = results.find(candidateId);

  if (searchCandidate != results.end()) {
    totalSpaceLeftInBudgetForPersistentResult -= candidateSize;
    //    std::cout << "Candidate tk: " << tk << " in, so reduce
    //    totalSpaceLeft
    //    "
    //              << totalSpaceLeftInBudgetForPersistentResult <<
    //              std::endl;
  }

  // std::cout << std::endl;

  std::vector<sto::scoreTuple> *tmpScores =
      &bm->getBufferPool().permanentScores;
  for (auto it = tmpScores->rbegin(); it != tmpScores->rend(); ++it) {
    // std::cout << "Sorted Permanent Score tk: " << it->tk << " score: " <<
    // it->score << std::endl;
    auto fit = resultsNotSelected.find(it->tk);
    if (fit != resultsNotSelected.end()) {
      //   std::cout << "Result tk: " << it->tk << " with past_score: " <<
      //   it->score << " not selected so delete it "
      //              << std::endl;

      int fileSize = static_cast<int>(
          bm->getSM().getPrimaryFileSizeInPagesForProjectedRelation(it->tk));

      //      std::cout << "fileSize: " << it->tk << " " << fileSize <<
      //      std::endl;

      if (totalSpaceLeftInBudgetForPersistentResult >= fileSize) {
        totalSpaceLeftInBudgetForPersistentResult -= fileSize;
        //      std::cout << "Add it back NOT IN RESULT BUT KEEP IT: "
        //      <<
        //      it->tk
        //      << " and reduce space to: "
        //                 << totalSpaceLeftInBudgetForPersistentResult
        //                 <<
        //                 std::endl;
      } else {
        //      std::cout << "Remove permantly as it does not fit" <<
        //      std::endl;
        bm->removeCompletelyPersistentFile(it->tk);
      }
    }
  }

  if (searchCandidate == results.end()) {
    //    std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!request TEMPORAL
    //    Persistence
    //    END "
    //                 "FALSE !!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    //              << tk << "score: " << candidateScore << std::endl;

    //    std::cout << "requestTemporalPersistence: FALSE (NOT IN RESULTS)
    //    TEMPORAL "
    //                 "PERSISTENCE: " << getTpchId() << " for key: " << tk
    //              << std::endl;

    return false;
  } else {
    //    std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!request TEMPORAL
    //    Persistence
    //    END "
    //                 "TRUE !!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    //              << tk << "score: " << candidateScore << std::endl;
    bm->getSM().filesPersistedCounterFuture += 1;
    bm->getSM().filesPersistedCostFuture += candidateScore;
    auto futureIterator = bm->getSM().futurePersistedStatisticsCounter.find(tk);
    if (futureIterator == bm->getSM().futurePersistedStatisticsCounter.end()) {
      bm->getSM().futurePersistedStatisticsCounter.insert(
          std::make_pair(tk, 1));
    } else {
      futureIterator->second = futureIterator->second + 1;
    }

    //    std::cout << "requestTemporalPersistence: TRUE TEMPORAL PERSISTENCE: "
    //              << getTpchId() << " for key: " << tk << std::endl;

    return true;
  }
}

// @HOT check the functionality of this function
bool QOperator::requestPermanentPersistence(const sto::table_key &tk,
                                            NodeTree *t) {
  (void)t;

  //  std::cout << "requestPermanentPersistence: " << tk << std::endl;

  if (!bm->isDatastructure(tk)) {
    return false;
  }

  //  std::cout << "requestPermanentPersistence: REQUEST PERMANENT PERISTENCE "
  //               "FOR QUERY: " << getTpchId() << std::endl;
  //  std::cout << "requestPermanentPersistence: tk: " << tk << std::endl;

  int candidateSize = static_cast<int>(
      bm->getSM().getPrimaryFileSizeInPagesForProjectedRelation(tk));
  int totalBudgetAvailable = bm->getSM().getBudgetPermanentSize();

  //  std::cout << "requestPermanentPersistence: candidateSize: " <<
  //  candidateSize
  //            << std::endl;
  //  std::cout << "requestPermanentPersistence: totalInitialBudget: "
  //            << totalBudgetAvailable << std::endl;

  if (candidateSize > totalBudgetAvailable) {
    return false;
  }

  // Space occupied by results that are currently being materialised
  totalBudgetAvailable = calculateAvailableFreeBudget(totalBudgetAvailable);
  // bm->getStorageManager().printPersistentFileCatalog();

  std::vector<sto::scoreTuple> localScores;

  for (auto it = bm->getSM().getIteratorPersistedCatalogBegin();
       it != bm->getSM().getIteratorPersistedCatalogEnd(); ++it) {
    if (it->second.pins > 0) {
      continue;
    }

    if (it->second.fileClassifier == sto::PERMANENT) {
      bool isCandidate = false;
      //      double score =
      //          bm->calculateTotalScore(it->first, isCandidate,
      //          sto::PERMANENT);
      double score = calculateFullCost(it->first, isCandidate, sto::PERMANENT);
      // std::cout << "Persisted result: score: " << score << std::endl;
      localScores.push_back({it->first, score, 0, 0});
    }
  }

  //############################ SECTION 4
  //########################################
  // COUT << "----------- FIND CANDIDATE SCORE ----------------" << "\n";
  bool isCandidate = true;
  //  double candidateScore =
  //      bm->calculateTotalScore(tk, isCandidate, sto::PERMANENT);
  double candidateScore = calculateFullCost(tk, isCandidate, sto::PERMANENT);

  localScores.push_back({tk, candidateScore, 0, 0});
  // std::cout << "candidateScore result: score: " << candidateScore <<
  // std::endl;
  //  std::cout << "Candidate PERMANENT Score: " << candidateScore << std::endl;

  //########################## DATA STRUCTURE REPLACEMENT

  int numberOfItems = localScores.size();
  //  Knapsack kn(nItems + 1, budget + 1);
  Knapsack kn;
  std::vector<double> weights_filesizes, values;

  // std::cout << "SCORES FILES" << "\n";
  for (size_t i = 0; i < localScores.size(); i++) {
    weights_filesizes.push_back(
        bm->getSM().getPrimaryFileSizeInPagesForProjectedRelation(
            localScores[i].tk));
    values.push_back(localScores[i].score);
  }

  std::set<int> results = kn.greedyKnapsack(numberOfItems, totalBudgetAvailable,
                                            weights_filesizes, values);

  size_t candidateId = 0;

  for (size_t i = 0; i < localScores.size(); i++) {
    if (localScores[i].tk == tk) {
      candidateId = i;
      continue;
    }

    auto search = results.find(i);
    if (search == results.end()) {
      // std::cout << "Not necessary anymore: " << localScores[i].tk <<
      // "\n";
      //  auto findIter = newPersistedFiles.find(scores[i].tk);
      // Check if another node of the query is chosen to be persistent
      //      if (findIter == newPersistedFiles.end()) {

      if (bm->getSM().isInPersistedFileCatalog(localScores[i].tk)) {
        //        std::cout << "Not necessary anymore so delete it: " <<
        //        localScores[i].tk << " score: " <<
        //        localScores[i].score
        //                  << std::endl;
        bm->removeCompletelyPersistentFile(localScores[i].tk);
      }
      //      } else {
      //        //  //Need to unpersist node
      //        COUT << "Unpersist result: " << scores[i].tk << "\n";
      //        findIter->second->unPersistResult(scores[i].tk);
      //        newPersistedFiles.erase(findIter);
      //      }
    }
    // else {
    //   COUT << "Necessary: " << localScores[i].tk << "\n";
    // }
  }

  // Seach for candidate node which is added last in the scores vector
  auto searchCandidate = results.find(candidateId);
  // std::cout << "Candidate: " << localScores[candidateId].tk << "\n";

  if (searchCandidate == results.end()) {
    // std::cout << "Not in results" << "\n";
    //    std::cout << " !!!!!!!!!!!!!!!!!!!! REQUEST PERMANENT PERSISTENCE
    //    END
    //    "
    //                 "!!!!!!!!!!!!!!!!!!!!!!! \n";
    //  std::cout << "$$$$$$$$$$$$$ requestPermanentPersistence RESPONSE
    //  FALSE
    //  ";
    //                 "better results exist "
    //              << "\n";
    //  std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!requestPermanentPersistence
    //  END
    //  FALSE:!!!!!!!!!!!!!!!!!!!!!!!!!!!!  " << tk
    //            << std::endl;

    //    std::cout << "FALSE : REQUEST PERMANENT (NOT IN RESULTS): " <<
    //    getTpchId()
    //              << " for key: " << tk << std::endl;

    return false;
  } else {
    // std::cout << "In results" << "\n";
    //    std::cout << " !!!!!!!!!!!!!!!!!!!! REQUEST PERMANENT PERSISTENCE
    //    END
    //    "
    //                 "!!!!!!!!!!!!!!!!!!!!!!! \n";
    //    std::cout << "$$$$$$$$$$$$$ requestPermanentPersistence RESPONSE
    //    TRUE;
    //    "
    //              << "\n";
    bm->getSM().filesPersistedCounterPast += 1;
    bm->getSM().filesPersistedCostPast += candidateScore;
    //   std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!requestPermanentPersistence
    //   END
    //   TRUE:!!!!!!!!!!!!!!!!!!!!!!!!!!!!  " << tk
    //             << std::endl;

    // std::cout << "$$$$$$$$$$$$$ requestPermanentPersistence RESPONSE TRUE
    // ";
    //    std::cout << "TRUE : REQUEST PERMANENT (NOT IN RESULTS): " <<
    //    getTpchId()
    //              << " for key: " << tk << std::endl;
    return true;
  }
}

void QOperator::updateScores() {
  // std::cout << "~~~~~~~~~~ SLIDER COUNT: "
  //          << bm->getBufferPool().getSliderCount() << " ~~~~~~~~~~~~~~
  //          \n";
  std::vector<sto::scoreTuple> &permanentScores =
      bm->getBufferPool().permanentScores;
  std::vector<sto::scoreTuple> &temporalScores =
      bm->getBufferPool().temporalScores;

  permanentScores.erase(permanentScores.begin(), permanentScores.end());
  temporalScores.erase(temporalScores.begin(), temporalScores.end());

  double &totalSizeOfFiles = bm->getSM().totalFileSizes;
  double &totalNumberOfReferences = bm->getSM().totalNumberOfReferences;

  totalSizeOfFiles = 0;
  totalNumberOfReferences = 0;
  // Calculate scores for persisted results
  for (auto pIt = bm->getSM().getIteratorPersistedCatalogBegin();
       pIt != bm->getSM().getIteratorPersistedCatalogEnd(); ++pIt) {
    // Calculate score based on the classifier
    //    double score =
    //        bm->calculateTotalScore(pIt->first, false,
    //        pIt->second.fileClassifier);

    double score =
        calculateFullCost(pIt->first, false, pIt->second.fileClassifier);

    double fileSize = static_cast<double>((
        bm->getSM().getPrimaryFileSizeInPagesForProjectedRelation(pIt->first)));
    double fileNumberOfReferences = 0;
    auto statisticsIt = bm->getSM().getIterOfPersistentStatistics(pIt->first);
    if (statisticsIt != bm->getSM().getIteratorPersistentStatisticsEnd()) {
      fileNumberOfReferences = statisticsIt->second.references;
    }

    if (pIt->second.fileClassifier == sto::PERMANENT) {
      permanentScores.push_back(
          {pIt->first, score, fileSize, fileNumberOfReferences});
    } else if (pIt->second.fileClassifier == sto::TEMPORAL) {
      temporalScores.push_back(
          {pIt->first, score, fileSize, fileNumberOfReferences});
    }

    totalSizeOfFiles += fileSize;
    totalNumberOfReferences += fileNumberOfReferences;
  }

  // calculate scores for currently persisted results
  for (auto tIt = bm->getSM().getIteratorRecordFromTmpFileSetBegin();
       tIt != bm->getSM().getIteratorRecordFromTmpFileSetEnd(); ++tIt) {
    sto::table_key tempTk = tIt->first;
    sto::FILE_CLASSIFIER fileClassifier = tIt->second;

    //    double score = bm->calculateTotalScore(tempTk, true, fileClassifier);

    double score = calculateFullCost(tempTk, true, fileClassifier);

    double fileSize = static_cast<double>(
        (bm->getSM().getPrimaryFileSizeInPagesForProjectedRelation(tempTk)));
    double fileNumberOfReferences = 0;
    auto statisticsIt = bm->getSM().getIterOfPersistentStatistics(tempTk);
    if (statisticsIt != bm->getSM().getIteratorPersistentStatisticsEnd()) {
      fileNumberOfReferences = statisticsIt->second.references;
    }

    if (fileClassifier == sto::PERMANENT) {
      permanentScores.push_back(
          {tempTk, score, fileSize, fileNumberOfReferences});
    } else if (fileClassifier == sto::TEMPORAL) {
      temporalScores.push_back(
          {tempTk, score, fileSize, fileNumberOfReferences});
    }

    totalSizeOfFiles += fileSize;
    totalNumberOfReferences += fileNumberOfReferences;
  }

  // The following line of code determines if a page should be firstly taken
  // from the data structure
  // with the biggest or the smallest score

  //  sort(scores.begin(), scores.end(),
  //       std::greater<sto::scoreTuple>());

  sort(permanentScores.begin(), permanentScores.end());
  sort(temporalScores.begin(), temporalScores.end());

  // for (auto it : permanentScores) {
  //   std::cout << "PERMANENT SCORE: " << it.tk << " " << it.score <<
  //   std::endl;
  // }

  // for (auto it : temporalScores) {
  //   std::cout << "TEMPORAL SCORE: " << it.tk << " " << it.score <<
  //   std::endl;
  // }
}

void QOperator::executeQueryPlan() {
  bm->getSM().reorderCounter++;
  executeQueryPlan(rootNode);
}

void QOperator::executeQueryPlan(NodeTree *currentNode) {
  if (currentNode == nullptr) return;

  if (currentNode->leftNode != nullptr) {
    executeQueryPlan((currentNode->leftNode));
  }
  if (currentNode->rightNode != nullptr) {
    executeQueryPlan((currentNode->rightNode));
  }

  std::vector<sto::table_key> *nodesToBeDeleted = nullptr;
  //##################### Analyse and change Node #####################
  if (cleverFlag) {
    if (bm->isDatastructure(currentNode->tk)) {
      nodesToBeDeleted = new std::vector<sto::table_key>();

      //      std::lock_guard<std::mutex> lockAnalyser(
      //          bm->getStorageManager().queryOperatorsMutex);
      //
      //      dataStructureId = 0;
      //
      //      // Analyse current state of the task queue
      //      //      std::cout << "ANALYSE FUTURE QUERIES" << std::endl;
      //
      //      analyseQueries(rootNode, currentNode);
      //
      //      // Decide if should change the node
      //      std::cout << "--------- VISIT AND CHANGE QUERY START:
      //      ------------
      //      "
      //                << "tpch: " << getTpchId() << " id:" <<
      //                getQueryId()
      //                << std::endl
      //                << std::endl;
      //      visitAndChangeNode(currentNode);
      //      std::cout << "--------- VISIT AND CHANGE QUERY END:
      //      ------------ "
      //                << getTpchId() << std::endl
      //                << std::endl;

      {
// If an operator uses results, check if the result should be
// still kept
//        std::lock_guard<std::mutex> lockAnalyser(
//            bm->getSM().bufferManagerOperationMutex);

#ifdef MULTI_THREADED
        std::lock_guard<std::mutex> lockAnalyser(
            bm->getSM().bufferManagerOperationMutex);
#endif

        if (currentNode->check_usePersistedResult_flag) {
          //          std::cout << "USE PERSISTED RESULTS: tpch: " << tpchId
          //                    << " qid: " << getQueryId() << " tk: " <<
          //                    currentNode->tk
          //                    << std::endl;
          //          std::cout << "left_tk: " << currentNode->left_tk <<
          //          std::endl;
          //          std::cout << "right_tk: " << currentNode->right_tk <<
          //          std::endl;

          checkNodesThatShouldBeDeleted(currentNode, nodesToBeDeleted);
        }

        //        updateScores();
        // Silly right now as I need to analyse and clear the task queue
        // for
        // every operator
        //        bm->getStorageManager().clearPersistedFutureFileReferences();
      }
    }
  }

  currentNode->execute();

  //##################### Pin files #####################
  std::lock_guard<std::mutex> lockAnalyser(bm->getSM().pinFileMutex);

  unpinFiles(currentNode);

  if (cleverFlag) {
    //    if (currentNode->tk.version == sto::HASHJOIN ||
    //        currentNode->tk.version == sto::SORT) {
    //
    if (bm->isDatastructure(currentNode->tk)) {
      for (auto it : (*nodesToBeDeleted)) {
        auto fit = bm->getSM().getIterOfPersistedFileCatalog(it);
        if (fit->second.pins < 1 &&
            fit->second.fileClassifier == sto::TEMPORAL) {
          //          double d = bm->getBufferPool().dirtyPagesForDS(it);
          //          std::cout << "remove file : " << it << " dirty pages: " <<
          //          d
          //                    << std::endl;
          bm->removeCompletelyPersistentFile(it);
        }
      }
      nodesToBeDeleted->erase(nodesToBeDeleted->begin(),
                              nodesToBeDeleted->end());
      delete nodesToBeDeleted;
    }
  }
}

// HOT 08/02/2016 Am I really working well?
void QOperator::checkNodesThatShouldBeDeleted(
    NodeTree *currentNode, std::vector<sto::table_key> *nodesToBeDeleted) {
  // std::cout << "checkNodesThatShouldBeDeleted start" << std::endl;
  bm->getSM().clearPersistedFutureFileReferences();
  analyseQueries(rootNode, currentNode);

  // check for hash-join operator first
  if (currentNode->tk.version == sto::HASHJOIN) {
    // if both left and right relations refer to data structure
    if (currentNode->result_id == 0) {
      //      if (currentNode->isInFutureAppearances(currentNode->left_tk)) {
      //
      //      }
      checkDeleteOfKey(currentNode->left_tk, nodesToBeDeleted);
      checkDeleteOfKey(currentNode->right_tk, nodesToBeDeleted);
    } else if (currentNode->result_id == 1) {
      checkDeleteOfKey(currentNode->left_tk, nodesToBeDeleted);
    } else if (currentNode->result_id == 2) {
      checkDeleteOfKey(currentNode->right_tk, nodesToBeDeleted);
    }
  } else if (currentNode->tk.version == sto::SORT) {
    checkDeleteOfKey(currentNode->tk, nodesToBeDeleted);
  }

  bm->getSM().clearPersistedFutureFileReferences();
  //  std::cout << "checkNodesThatShouldBeDeleted end" << std::endl;
}

void QOperator::checkDeleteOfKey(
    const sto::table_key &tk, std::vector<sto::table_key> *nodesToBeDeleted) {
  //  if (bm->getSM().isInPersistedFutureFileReferences(tk)) {
  //    return;
  //  }

  // check when pessimistic approach not to delete
  // the result if it is still refered in the future
  if (bm->costCalculatorCase == 0) {
    auto searchIt = rootNode->futureAppearances.find(tk);
    if (searchIt != rootNode->futureAppearances.end()) {
      // std::cout << "tk: " << tk << " is refered in the future so dont delete
      // it"
      //           << std::endl;
      return;
    }
  }

  bool isWeightImportant = true;
  (void) isWeightImportant;
  //  double score = bm->calculateFutureScore(tk, false, isWeightImportant);
  //  double score = bm->calculateTotalScore(tk, false,
  //  deceve::storage::TEMPORAL);
  double score = calculateFullCost(tk, false, deceve::storage::TEMPORAL);
  //  std::cout << "checkDeleteOfKey: calculate score for " << tk
  //            << " scoreFuture: " << score << std::endl;

  double d = bm->getBufferPool().dirtyPagesForDS(tk);
  double costOfWritingInPersistentMemory =
      d * bm->getSM().getStorageWriteDelay();
  // 0 number of dirty pages assuming that the ds will not be
  // materialised
  double costOfOperation = rootNode->getEstimatedCost(0, tk);
  double costOfReadingRelation =
      bm->getSM().getPrimaryFileSizeInPagesForRelation(tk) *
      bm->getSM().getStorageReadDelay();

  //  std::cout << "remove file : " << tk << " numberOfDirtyPages    : " << d
  //            << std::endl;
  //  std::cout << "remove file : " << tk
  //            << " costOfWritingInPersistentMemory    : "
  //            << costOfWritingInPersistentMemory << std::endl;
  //  std::cout << "remove file : " << tk
  //            << " cost of reading initial relation    : "
  //            << bm->getSM().getPrimaryFileSizeInPagesForRelation(tk) *
  //                   bm->getSM().getStorageReadDelay() << std::endl;
  //  std::cout << "remove file : " << tk << " costOfOperation: " <<
  //  costOfOperation
  //            << std::endl;

  if (d < 500) {
    changeTemporalToPermanent(tk);
  } else if (score <= 1) {
    nodesToBeDeleted->push_back(tk);
  }
  //    else {
  //      changeTemporalToPermanent(tk);
  //    }
}

void QOperator::unpinFiles(NodeTree *currentNode) {
  // std::cout << "Finished query execution for query: " << getQueryId()
  //    << " NODE: " << currentNode->id << "\n";
  //##################### Pin files #####################
  if (cleverFlag && currentNode->check_usePersistedResult_flag) {
    auto git = bm->getSM().getIterOfPersistedFileCatalog(currentNode->tk);

    if (currentNode->tk.version == sto::SORT) {
      if (bm->getSM().isInPersistedFileCatalog(currentNode->tk)) {
        git->second.pins--;
      }
    }

    // File Unpinning for hash operators is done after the query execution
    // in
    // each query seperately
    //    if (t->tk.version == sto::HASHJOIN) {
    //
    //      //get join left file details
    //      sto::global_table_map::iterator lgit =
    // bm->getStorageManager().getIteratorRecordFromPersistedFileCatalog(
    //              t->left_tk);
    //      //get join right file details
    //      sto::global_table_map::iterator rgit =
    // bm->getStorageManager().getIteratorRecordFromPersistedFileCatalog(
    //              t->right_tk);
    //      //check if both nodes exist in catalog;
    //      if
    //      (bm->getStorageManager().isInPersistedFileCatalog(t->left_tk))
    //      {
    //        std::cout << "DECREASE lgit before: " << t->left_tk << " "
    //            << lgit->second << "\n";
    //        lgit->second.pins--;
    //        std::cout << "DECREASE lgit after: " << t->left_tk << " "
    //            << lgit->second << "\n";
    //      }
    //      //check if both nodes exist in catalog;
    //      if
    //      (bm->getStorageManager().isInPersistedFileCatalog(t->right_tk))
    //      {
    //        std::cout << "rgit before: " << t->right_tk << " "
    //            << rgit->second << "\n";
    //        rgit->second.pins--;
    //        std::cout << "rgit before: " << t->right_tk << " "
    //            << rgit->second << "\n";
    //      }
    //    }
  }
}

void QOperator::printQueryPlan() {
  std::cout << "printQueryPlan: START"
            << "\n";
  printQueryPlan(rootNode);
  std::cout << "printQueryPlan: END"
            << "\n";
}
void QOperator::printQueryPlan(NodeTree *t, int indent) {
  if (t == NULL) return;
  if (t->leftNode != NULL) {
    printQueryPlan((t->leftNode), indent + 4);
  }
  if (t->rightNode != NULL) {
    printQueryPlan((t->rightNode), indent + 4);
  }
  if (indent) {
    COUT << std::setw(indent) << ' ';
  }
  t->printId();
}

void QOperator::printNewPersistedFiles() {
  COUT << "printNewPersistedFiles"
       << "\n";
  for (auto it = newPersistedFiles.begin(); it != newPersistedFiles.end();
       ++it) {
    COUT << "tk: " << it->first << "  t: " << it->second->id << "\n";
  }
}

void QOperator::printEverything(const std::vector<sto::scoreTuple> &scores) {
  COUT << "############################ Delete old scores BEGIN "
          "##################### \n";
  for (auto it = scores.begin(); it != scores.end(); ++it) {
    COUT << it->tk << " " << it->score << "\n";
  }
  COUT << "############################ Delete old scores END "
          "##################### \n";
  COUT << "############################ PERSISTED FILE BEGIN "
          "##################### \n";
  for (auto it = bm->getSM().getIteratorPersistedCatalogBegin();
       it != bm->getSM().getIteratorPersistedCatalogEnd(); ++it) {
    COUT << it->first << " " << it->second << "\n";
  }
  COUT << "############################ PERSISTED FILE BEGIN "
          "##################### \n";
  COUT << "############################ NEW FILE BEGIN "
          "##################### "
          "\n";
  for (auto it = newPersistedFiles.begin(); it != newPersistedFiles.end();
       ++it) {
    COUT << it->first << "\n";
  }
  COUT << "############################ NEW FILE END ##################### "
          "\n";
}

void QOperator::printTableKeys() {
  //  std::cout << "------ query " << tpchId << " keys start ----------"
  //            << std::endl;
  printTableKeys(rootNode);
  //  std::cout << "------ query " << tpchId << " keys end ----------" <<
  //  std::endl;
}

void QOperator::printTableKeys(NodeTree *t) {
  if (t == NULL) {
    return;
  }

  if (t->leftNode != NULL) {
    printTableKeys((t->leftNode));
  }
  if (t->rightNode != NULL) {
    printTableKeys((t->rightNode));
  }

  if (t->tk.version != sto::SORT && t->tk.version != sto::HASHJOIN) {
    return;
  }

  if (t->tk.version == sto::SORT) {
    if (t->tk.table_name != sto::OTHER_TABLENAME) {
      std::cout << "{ t:[" << t->tk.table_name << "] f:[" << t->tk.field
                << "] v:[" << t->tk.version << "] }, ";
    }
  }

  if (t->tk.version == sto::HASHJOIN) {
    if (t->left_tk.version == sto::HASHJOIN ||
        t->left_tk.version == sto::SORT) {
      if (t->left_tk.table_name != sto::OTHER_TABLENAME) {
        std::cout << "{ t:[" << t->left_tk.table_name << "] f:["
                  << t->left_tk.field << "] v:[" << t->left_tk.version
                  << "] }, ";
      }
    }
    if (t->right_tk.version == sto::HASHJOIN ||
        t->right_tk.version == sto::SORT) {
      if (t->right_tk.table_name != sto::OTHER_TABLENAME) {
        std::cout << "{ t:[" << t->right_tk.table_name << "] f:["
                  << t->right_tk.field << "] v:[" << t->right_tk.version
                  << "] }, ";
      }
    }
  }
}

int QOperator::calculateAvailableFreeBudget(int totalBudgetAvailable) {
  // Space occupied by results that are currently being materialised
  int spaceOfNewPersistedFiles =
      bm->getSM().calculateSpaceOfNewPersistedFiles();
  totalBudgetAvailable = totalBudgetAvailable - spaceOfNewPersistedFiles;

  double totalSpaceOccupiedByPinnedFiles =
      bm->getSM().calculateTotalSpaceOfPersistedFiles();

  totalBudgetAvailable = totalBudgetAvailable - totalSpaceOccupiedByPinnedFiles;
  int futureSize =
      bm->getSM().calculateSpaceOfFuturePersistedFiles(sto::TEMPORAL);
  totalBudgetAvailable = totalBudgetAvailable - futureSize;
  //  std::cout << "requestPermanentPersistence: totalBudgetAvailable: "
  //            << totalBudgetAvailable << std::endl;
  return totalBudgetAvailable;
}

void QOperator::changeTemporalToPermanent(const sto::table_key &tk) {
  if (bm->getSM().isInPersistedFileCatalog(tk)) {
    //    std::cout << "changeTemporalToPermanent: " << tk << std::endl;
    auto fCatalogIt = bm->getSM().getIterOfPersistedFileCatalog(tk);
    fCatalogIt->second.fileClassifier = sto::PERMANENT;
  }
}

}  // namespace queries
}  // namespace deceve
