/*******************************************************************************
 * Copyright (c) 2015 Michail Basios and Stratis Viglas.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michail Basios - initial API and implementation
 *     Stratis Viglas - initial API and implementation
 *******************************************************************************/

#include "NodeTree.hh"

namespace deceve {
namespace queries {

void NodeTree::execute() {
  // COUT << "Node input name: " << node_input_name << "\n";

  if (tk.version == sto::HASHJOIN) {
    if (leftNode != NULL && rightNode != NULL) {
      node_left_name = leftNode->node_output_name;
      node_right_name = rightNode->node_output_name;
    }

    // node_output_name = generateFileName();
    node_output_name = bm->getSM().generateUniqueFileName("join");

    // COUT << "Execute node " << id << " with two parameters, print left: "
    // << node_left_name << " right: " << node_right_name
    // << "\n Generated output_name: " << node_output_name << "\n";

    // COUT << "Execute A Join Operation" << "\n";
    run(bm, node_left_name, node_right_name, node_output_name);
  } else {
    if (node_input_name == "nothing") {
      if (leftNode != NULL) {
        node_input_name = leftNode->node_output_name;
      } else {
        if (rightNode != NULL) {
          // COUT << "5" << "\n";
          node_input_name = rightNode->node_output_name;
        }
      }
    }

    if (tk.version == sto::SORT) {
      // COUT << "Execute A SORT Operation" << "\n";
      // node_output_name = generateFileName("sort");
      node_output_name = bm->getSM().generateUniqueFileName("sort");

    } else {
      // COUT << "Execute A SELECT Operation" << "\n";
      // node_output_name =
      // generateFileName(node_input_name);
      node_output_name = bm->getSM().generateUniqueFileName(node_input_name);
    }
    run(bm, node_input_name, node_output_name);
  }
}

void NodeTree::usePersistedResult(const sto::table_value tv, int r_id) {
//  std::cout << "&&&&& usePersistedResult for: " << tk << " " << tv << r_id
//            << std::endl;
  information_about_persisted_files = tv;
  check_usePersistedResult_flag = true;
  result_id = r_id;
}

void NodeTree::useLeftPersistedResult(const sto::table_value tv, int r_id) {
//  std::cout << "&&&&& useLeftPersistedResult for: " << left_tk << " " << tv
//            << r_id << std::endl;
  information_about_left_persisted_file = tv;
  check_usePersistedResult_flag = true;
  result_id = r_id;
}

void NodeTree::useRightPersistedResult(const sto::table_value tv, int r_id) {
//  std::cout << "&&&&& USE-RightPersistedResult for: " << right_tk << " " << tv
//            << r_id << std::endl;
  information_about_right_persisted_file = tv;
  check_usePersistedResult_flag = true;
  result_id = r_id;
}

void NodeTree::usePersistedResult(const sto::table_value left_tv,
                                  const sto::table_value right_tv, int r_id) {
//  std::cout << "&&&&& USE-usePersistedResult for: " << left_tk << " "
//            << right_tk << " " << tv << r_id << std::endl;
  information_about_left_persisted_file = left_tv;
  information_about_right_persisted_file = right_tv;
  check_usePersistedResult_flag = true;
  result_id = r_id;
}

void NodeTree::persistResult(int r_id) {
  // sorter->persistResult();

  // COUT << "PersistResult" << "\n";
  check_PersistResult_flag = true;
  result_id = r_id;
}

void NodeTree::persistLeftResult(const sto::table_key tk, int r_id) {
  // COUT << "PersistLeftResult" << "\n";
  check_PersistResult_flag = true;
  result_id = r_id;
  key_about_left_persisted_file = tk;
}

void NodeTree::persistRightResult(const sto::table_key tk, int r_id) {
  // COUT << "PersistRigtResult" << "\n";
  check_PersistResult_flag = true;
  result_id = r_id;
  key_about_right_persisted_file = tk;
}

void NodeTree::persistResult(const sto::table_key ltk, sto::table_key rtk,
                             int r_id) {
  // COUT << "Persist Both files" << "\n";
  check_PersistResult_flag = true;
  result_id = r_id;
  key_about_left_persisted_file = ltk;
  key_about_right_persisted_file = rtk;
}

void NodeTree::buildLeftFileName(const std::string& left_file,
                                 std::string& left) {
  if (left_file_mode == sto::INTERMEDIATE) {
    left = constructInputTmpFullPath(leftNode->node_output_name);
  } else {
    left = constructFullPath(left_file);
  }
}

void NodeTree::buildLeftFilePath(const std::string& left_file,
                                 std::string& left) {
  if (left_file_path == sto::INTERMEDIATE_PATH) {
    left = constructInputTmpFullPath(leftNode->node_output_name);
  } else {
    left = constructFullPath(left_file);
  }
}

void NodeTree::buildRightFileName(const std::string& right_file,
                                  std::string& right) {
  if (right_file_mode == sto::INTERMEDIATE) {
    right = constructInputTmpFullPath(rightNode->node_output_name);
  } else {
    right = constructFullPath(right_file);
  }
}

void NodeTree::buildRightFilePath(const std::string& right_file,
                                  std::string& right) {
  if (right_file_path == sto::INTERMEDIATE_PATH) {
    right = constructInputTmpFullPath(rightNode->node_output_name);
  } else {
    right = constructFullPath(right_file);
  }
}

void NodeTree::addToStatistics(sto::BufferManager* bm, int persisted_result_id,
                               sto::table_key leftKey,
                               sto::table_key rightKey) {
  if (leftKey.version == sto::HASHJOIN || leftKey.version == sto::SORT) {
    if (!bm->getSM().isContainedInPersistentStatistics(leftKey)) {
      // Add information to persistent_file_statistics
      sto::table_statistics ts;
      ts.references = 0;
      ts.hits = 0;
      ts.timesPersisted = 0;
      ts.cost = bm->calculateOperatorCost(leftKey);
      ts.timestamp = bm->getSM().getNextPersistentTime();

      bm->getSM().insertToPersistentStatistics(std::make_pair(leftKey, ts));
    }
  }

  if (rightKey.version == sto::HASHJOIN || rightKey.version == sto::SORT) {
    if (!bm->getSM().isContainedInPersistentStatistics(rightKey)) {
      // Add information to persistent_file_statistics
      sto::table_statistics ts;
      ts.references = 0;
      ts.hits = 0;
      ts.timesPersisted = 0;
      ts.cost = bm->calculateOperatorCost(rightKey);
      ts.timestamp = bm->getSM().getNextPersistentTime();

      bm->getSM().insertToPersistentStatistics(std::make_pair(rightKey, ts));
    }
  }

  bool statisticsPersistentFlagLeft = false;
  bool statisticsPersistentFlagRight = false;
  if (check_PersistResult_flag &&
      (persisted_result_id == 0 || persisted_result_id == 1))
    statisticsPersistentFlagLeft = true;

  if (check_PersistResult_flag &&
      (persisted_result_id == 0 || persisted_result_id == 2))
    statisticsPersistentFlagRight = true;

  if (leftKey.version == sto::HASHJOIN || leftKey.version == sto::SORT) {
    bm->getSM().incrementPersistentStatisticsReferences(
        leftKey, statisticsPersistentFlagLeft);
  }

  if (rightKey.version == sto::HASHJOIN || rightKey.version == sto::SORT) {
    bm->getSM().incrementPersistentStatisticsReferences(
        rightKey, statisticsPersistentFlagRight);
  }
}

void NodeTree::deletePreviousFilesJoin(sto::BufferManager* bm,
                                       const std::string& left,
                                       const std::string& right) {
  // std::cout << "Left file: " << left << "\n";
  // std::cout << "Right file: " << right << "\n";
  //############################ Delete Previous File
  //####################################
  // check if the input is a primary table and not an intermediate results
  if (!bm->getSM().isInTableCatalog(left) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    // std::cout
    // << "DELETE PREVIOUS file which is not in the table catalog nor is
    // persistent"
    // << "\n";
    bm->removeFile(left);
  }
  if (!bm->getSM().isInTableCatalog(right) &&
      !bm->getSM().isInPersistedFileCatalog(rightNode->tk)) {
    // std::cout

    // << "DELETE PREVIOUS file which is not in the table catalog nor is
    // persistent"
    // << "\n";
    bm->removeFile(right);
  }

  std::lock_guard<std::mutex> lockAnalyser(bm->getSM().pinFileMutex);
  // get join left file details
  sto::global_table_map::iterator lgit =
      bm->getSM().getIterOfPersistedFileCatalog(left_tk);
  // get join right file details
  sto::global_table_map::iterator rgit =
      bm->getSM().getIterOfPersistedFileCatalog(right_tk);
  // check if both nodes exist in catalog;
  if (bm->getSM().isInPersistedFileCatalog(left_tk)) {
    lgit->second.pins--;
  }
  // check if both nodes exist in catalog;
  if (bm->getSM().isInPersistedFileCatalog(right_tk)) {
    rgit->second.pins--;
  }
}

void NodeTree::addFilesToCatalogs(
    sto::BufferManager* bm, int tmp_result_id, const std::string& left,
    const std::string& right, unsigned int numOfPartitionFiles,
    const std::string& runNamePrefixOfPersistentFile) {
  bm->getSM().deleteFromPersistedTmpFileSet(tk);
  // COUT << "################### RESULT ID IS: ###################"
  // << tmp_result_id << "\n";
  if (tmp_result_id == 0) {
    left_tv.num_files = numOfPartitionFiles;
    right_tv.num_files = numOfPartitionFiles;

    left_tv.output_name = left;
    right_tv.output_name = right;

    left_tv.full_output_path = runNamePrefixOfPersistentFile;
    right_tv.full_output_path = runNamePrefixOfPersistentFile;
    left_tv.pins = 1;
    right_tv.pins = 1;

    // COUT << "PERSIST: JOINNNNNNNNNN: " << left_tv << "\n";
    // COUT << "PERSIST: JOINNNNNNNNNN: " << right_tv << "\n";

    bm->getSM().insertInPersistedFileCatalog(std::make_pair(left_tk, left_tv));
    bm->getSM().insertInPersistedFileCatalog(
        std::make_pair(right_tk, right_tv));

    bm->getSM().deleteFromPersistedTmpFileSet(left_tk);
    bm->getSM().deleteFromPersistedTmpFileSet(right_tk);

  } else if (tmp_result_id == 1) {
    left_tv.num_files = numOfPartitionFiles;
    left_tv.output_name = left;
    left_tv.full_output_path = runNamePrefixOfPersistentFile;
    left_tv.pins = 1;

    bm->getSM().insertInPersistedFileCatalog(std::make_pair(left_tk, left_tv));

    bm->getSM().deleteFromPersistedTmpFileSet(left_tk);

  } else if (tmp_result_id == 2) {
    right_tv.num_files = numOfPartitionFiles;
    right_tv.output_name = right;
    right_tv.full_output_path = runNamePrefixOfPersistentFile;
    right_tv.pins = 1;

    bm->getSM().insertInPersistedFileCatalog(
        std::make_pair(right_tk, right_tv));

    bm->getSM().deleteFromPersistedTmpFileSet(right_tk);
  }

  bm->getBufferPool().condVariable.notify_all();
}

// try to estimate the number of writes that will occur
// for this operator
double NodeTree::getEstimatedCost(double candidateDirtyPages,
                                  const sto::table_key& candidateTk) {
  //  std::cout << "getEstimatedCost: tk: " << tk << " \n left_tk: " << left_tk
  //            << "\n right_tk: " << right_tk << std::endl;

  //  std::cout << "getEstimatedCost: candidateTk: " << candidateTk
  //            << " and size: " << candidateDirtyPages << std::endl;

  // Cost estimation for a full scan only for primary table.
  // Keep track also an estimation of the write factor.
  if (tk.version == sto::SELECT) {
    if (input_file_path == sto::PRIMARY_PATH) {
      double fileSizeInTuples = static_cast<double>(
          bm->getSM().getPrimaryFileSizeInTuplesForRelation(tk));

      size_t tuplesPerPage =
          static_cast<size_t>(sto::PAGE_SIZE_PERSISTENT /
                              ((outputTupleSize == 0) ? 1 : outputTupleSize));
      size_t projectedFileSizeInPages = (outputWriteFactor * fileSizeInTuples) /
                                        static_cast<double>(tuplesPerPage);

      //      std::cout << "getEstimatedCost fileSizeInTuples: " <<
      //      fileSizeInTuples
      //                << std::endl;
      //      std::cout << "getEstimatedCost projectedFileSizeInPages: "
      //                << projectedFileSizeInPages << std::endl;

      double result = bm->estimateDirtyWrites(projectedFileSizeInPages,
                                              candidateDirtyPages);
      //      std::cout << "Estimated select : " << result << std::endl;
      return result * bm->getSM().getStorageWriteDelay();
      //      return fileSize + fileSize * writeFactor;
    } else {
      return 0.0;
    }
  } else if (tk.version == sto::SORT) {
    if (input_file_path == sto::PRIMARY_PATH) {
      double fileSize = bm->getSM().getPrimaryFileSizeInPagesForRelation(tk);

      double numOfTuples = numberOfTuplesForFile(tk, 1);

      double numberOfReads = calculateReads(
          tk, numOfTuples, inputWriteFactor, inputTupleSizeProjected,
          candidateTk, candidateDirtyPages, fileSize);

      double projectedSize =
          bm->getSM().getPrimaryFileSizeInPagesForProjectedRelation(tk);

      double result = bm->estimateDirtyWrites(projectedSize * outputWriteFactor,
                                              candidateDirtyPages);
      //      std::cout << "Estimated sort : " << result << std::endl;
      return result * bm->getSM().getStorageWriteDelay() +
             numberOfReads * bm->getSM().getStorageReadDelay();
    } else {
      return 0.0;
    }
  } else if (tk.version == sto::HASHJOIN) {
    //    std::cout << "Estimated cost for hashjoin " << std::endl;
    double dirtyEstimates =
        getEstimatedCostHashJoin(candidateDirtyPages, candidateTk);
    //    return leftDirtyEstimates + rightDirtyEstimates;
    return dirtyEstimates;
  }

  return 0.0;
}

double NodeTree::getEstimatedCostHashJoin(double candidateDirtyPages,
                                          const sto::table_key& candidateTk) {
  //  std::cout << "getEstimatedCostHashJoin candidateDirtyPages"
  //            << candidateDirtyPages << " \n candidateTk" << candidateTk
  //            << std::endl;
  // calculate number of tuples for the two relations
  double leftNumOfTuples =
      numberOfTuplesForFile(left_tk, manualNumberOfLeftTuples);
  double rightNumOfTuples =
      numberOfTuplesForFile(right_tk, manualNumberOfRightTuples);

  //  std::cout << "getEstimatedCostHashJoin: leftNumOfTuples " <<
  //  leftNumOfTuples
  //            << std::endl;
  //  std::cout << "getEstimatedCostHashJoin: rightNumOfTuples " <<
  //  rightNumOfTuples
  //            << std::endl;

  size_t leftSizeInPages =
      leftNumOfTuples / (sto::PAGE_SIZE_PERSISTENT / leftTupleSize);
  size_t rightSizeInPages =
      rightNumOfTuples / (sto::PAGE_SIZE_PERSISTENT / rightTupleSize);

  double leftReads = calculateReads(left_tk, leftNumOfTuples, leftWriteFactor,
                                    leftTupleSizeProjected, candidateTk,
                                    candidateDirtyPages, leftSizeInPages);
  double rightReads = calculateReads(
      right_tk, rightNumOfTuples, rightWriteFactor, rightTupleSizeProjected,
      candidateTk, candidateDirtyPages, rightSizeInPages);

  // if size of CandidateDirtyPages is smaller than zero
  // it means that this function was called from a materialised
  // data structure.

  // Calculate correctly the number of projected dirty pages.
  //  std::cout << "--------------- leftProjectedPages ------------" <<
  //  std::endl;
  double leftProjectedPages = projectedFinalSize(
      left_tk, leftNumOfTuples, leftWriteFactor, leftTupleSizeProjected,
      candidateTk, candidateDirtyPages);
  //  std::cout << "--------------- rightProjectedPages ------------" <<
  //  std::endl;
  double rightProjectedPages = projectedFinalSize(
      right_tk, rightNumOfTuples, rightWriteFactor, rightTupleSizeProjected,
      candidateTk, candidateDirtyPages);

  //  std::cout << "NodeTree: candidateDirtyPages " << candidateDirtyPages
  //            << std::endl;
  //
  //  std::cout << "-------- LEFT FILE --------------" << std::endl;
  //  std::cout << "NodeTree: left_tk " << left_tk << std::endl;
  //  std::cout << "NodeTree: leftSizeInPages " << leftSizeInPages << std::endl;
  //  std::cout << "NodeTree: leftFilesizeInPagesProjected " <<
  //  leftProjectedPages
  //            << std::endl;
  //  std::cout << "NodeTree: leftReads " << leftReads << std::endl;
  //
  //
  //  std::cout << "-------- RIGHT FILE --------------" << std::endl;
  //  std::cout << "NodeTree: right_tk " << right_tk << std::endl;
  //  std::cout << "NodeTree: rightSizeInPages " << rightSizeInPages <<
  //  std::endl;
  //  std::cout << "NodeTree: rightFilesizeInPagesProjected " <<
  //  rightProjectedPages
  //            << std::endl;
  //  std::cout << "NodeTree: rightReads " << rightReads << std::endl;

  size_t outputNumOfTuples =
      ((leftNumOfTuples > rightNumOfTuples) ? leftNumOfTuples
                                            : rightNumOfTuples) *
      outputWriteFactor;

  size_t outputSizeInPages =
      outputNumOfTuples / (sto::PAGE_SIZE_PERSISTENT / outputTupleSize);

  //  std::cout << "-------- OUTPUT FILE --------------" << std::endl;
  //  std::cout << "NodeTree: writeFactor " << outputWriteFactor << std::endl;
  //  std::cout << "NodeTree: outputSizeInPages " << outputSizeInPages <<
  //  std::endl;

  double pagesToBeWritten =
      leftProjectedPages + rightProjectedPages + outputSizeInPages / 2;
  double dirtyEstimates =
      bm->estimateDirtyWrites(pagesToBeWritten, candidateDirtyPages);

  //  std::cout << "Estimated dirty pages for hash operation: " <<
  //  dirtyEstimates
  //            << std::endl;
  //
  //  std::cout << "Estimated read pages for hash operation: "
  //            << leftReads + rightReads;

  double totalCost =
      dirtyEstimates * bm->getSM().getStorageWriteDelay() +
      (leftReads + rightReads) * bm->getSM().getStorageReadDelay();

  //  std::cout << "###### TOTAL COST ####### (totalCost): " << totalCost
  //            << std::endl;

  return totalCost;
}

double NodeTree::numberOfTuplesForFile(const sto::table_key& tmpTk,
                                       double tmpManualNumberOfTuples) {
  double tuples = static_cast<double>(
      bm->getSM().getPrimaryFileSizeInTuplesForRelation(tmpTk));
  if (tmpTk.table_name == sto::OTHER_TABLENAME) {
    double lineitemTuples = bm->getSM().getNumberOfTuplesForLineitem();
    tuples = tmpManualNumberOfTuples * lineitemTuples;
  }

  return tuples;
}

double NodeTree::calculateReads(const sto::table_key& tmpTk,
                                double numberOfTuples, double writeFactor,
                                size_t tupleSize,
                                const sto::table_key& candidateTk,
                                double dirtyPagesOfCandidate,
                                size_t fileSizeInPages) {
  if (dirtyPagesOfCandidate == 0 || !bm->isDatastructure(tmpTk)) {
    // default behaviour
    return calculateReadsDefault(tmpTk, numberOfTuples, writeFactor, tupleSize,
                                 candidateTk, fileSizeInPages);
  } else if (dirtyPagesOfCandidate > 0) {
    // is candidate
    return calculateReadsForCandidate(tmpTk, numberOfTuples, writeFactor,
                                      tupleSize, candidateTk, fileSizeInPages);
  } else {
    // is data structure
    return calculateReadsForMaterialisedDS(tmpTk, numberOfTuples, writeFactor,
                                           tupleSize, candidateTk,
                                           fileSizeInPages);
  }
}

double NodeTree::calculateReadsDefault(const sto::table_key& tmpTk,
                                       double numberOfTuples,
                                       double writeFactor, size_t tupleSize,
                                       const sto::table_key& candidateTk,
                                       size_t fileSizeInPages) {

  (void) candidateTk;
  double finalNumberOfReads = 0.0;

  // std::cout << "calculateReadsDefault: " << tmpTk << std::endl;

  if (bm->getSM().isInPersistedFileCatalog(tmpTk)) {
    finalNumberOfReads =
        getSizeOfProjectedFile(tmpTk, numberOfTuples * writeFactor, tupleSize);
  } else {
    finalNumberOfReads =
        fileSizeInPages +
        getSizeOfProjectedFile(tmpTk, numberOfTuples * writeFactor, tupleSize);
  }

  //  std::cout << "calculateReadsDefault: finalNumberOfReads "
  //            << finalNumberOfReads << std::endl;
  return finalNumberOfReads;
}

double NodeTree::calculateReadsForCandidate(const sto::table_key& tmpTk,
                                            double numberOfTuples,
                                            double writeFactor,
                                            size_t tupleSize,
                                            const sto::table_key& candidateTk,
                                            size_t fileSizeInPages) {
  double finalNumberOfReads = 0.0;

  bool isThisACandidateNode = false;
  if (tmpTk.table_name != sto::OTHER_TABLENAME && tmpTk == candidateTk) {
    isThisACandidateNode = true;
  }

  if (isThisACandidateNode || bm->getSM().isInPersistedFileCatalog(tmpTk)) {
    finalNumberOfReads =
        getSizeOfProjectedFile(tmpTk, numberOfTuples * writeFactor, tupleSize);
  } else {
    finalNumberOfReads =
        fileSizeInPages +
        getSizeOfProjectedFile(tmpTk, numberOfTuples * writeFactor, tupleSize);
  }

  // std::cout << "calculateReadsForCandidate: finalNumberOfReads "
  //         << finalNumberOfReads << std::endl;
  return finalNumberOfReads;
}

double NodeTree::calculateReadsForMaterialisedDS(
    const sto::table_key& tmpTk, double numberOfTuples, double writeFactor,
    size_t tupleSize, const sto::table_key& candidateTk,
    size_t fileSizeInPages) {
  double finalNumberOfReads = 0.0;

  bool shouldBeThisNodeExcluded = false;
  if (candidateTk.table_name != sto::OTHER_TABLENAME && tmpTk == candidateTk) {
    shouldBeThisNodeExcluded = true;
  }

  if (!shouldBeThisNodeExcluded &&
      bm->getSM().isInPersistedFileCatalog(tmpTk)) {
    finalNumberOfReads =
        getSizeOfProjectedFile(tmpTk, numberOfTuples * writeFactor, tupleSize);
  } else {
    finalNumberOfReads =
        fileSizeInPages +
        getSizeOfProjectedFile(tmpTk, numberOfTuples * writeFactor, tupleSize);
  }
  //  std::cout << "calculateReadsForCandidate: finalNumberOfReads "
  //            << finalNumberOfReads << std::endl;
  return finalNumberOfReads;
}

// HOT 02/02/2016 projectedFinalSize
// We have four cases:
//  1) candidate without materialisation
//  2) candidate with materialisation
//  3) data structure without materialisation
//  4) data structure with materialisation
double NodeTree::projectedFinalSize(const sto::table_key& tmpTk,
                                    double numberOfTuples, double writeFactor,
                                    size_t tupleSize,
                                    const sto::table_key& candidateTk,
                                    double dirtyPagesOfCandidate) {
  //  std::cout << "projectedFinalSize: tmpTk " << tmpTk << std::endl;
  //  std::cout << "projectedFinalSize: candidateTk " << candidateTk <<
  //  std::endl;
  //  std::cout << "projectedFinalSize: numberOfTuples " << numberOfTuples
  //            << std::endl;
  //  std::cout << "projectedFinalSize: writeFactor " << writeFactor <<
  //  std::endl;
  //  std::cout << "projectedFinalSize: tupleSize " << tupleSize << std::endl;
  //  std::cout << "projectedFinalSize: dirtyPagesOfCandidate "
  //            << dirtyPagesOfCandidate << std::endl;

  if (dirtyPagesOfCandidate == 0 || !bm->isDatastructure(tmpTk)) {
    // default behaviour
    return projectedFinalSizeDefault(tmpTk, numberOfTuples, writeFactor,
                                     tupleSize, candidateTk);
  } else if (dirtyPagesOfCandidate > 0) {
    // is candidate
    return projectedFinalSizeForCandidate(tmpTk, numberOfTuples, writeFactor,
                                          tupleSize, candidateTk);
  } else {
    // is data structure
    return projectedFinalSizeForMaterialisedDS(
        tmpTk, numberOfTuples, writeFactor, tupleSize, candidateTk);
  }
}

double NodeTree::projectedFinalSizeDefault(const sto::table_key& tmpTk,
                                           double numberOfTuples,
                                           double writeFactor, size_t tupleSize,
                                           const sto::table_key& candidateTk) {
  (void) candidateTk;
  double finalProjectedSize = 0.0;

//  std::cout << "projectedFinalSizeDefault: " << tmpTk << std::endl;

  // If a data structure is already materialised it
  // means that the data structure will be just read
  // and there are no writes happening
  if (bm->getSM().isInPersistedFileCatalog(tmpTk)) {
    finalProjectedSize = 1;
  } else {
    finalProjectedSize =
        getSizeOfProjectedFile(tmpTk, numberOfTuples * writeFactor, tupleSize);
  }

  // std::cout << "projectedFinalSizeDefault: finalProjectedSize "
  //           << finalProjectedSize << std::endl;
  return finalProjectedSize;
}

double NodeTree::projectedFinalSizeForCandidate(
    const sto::table_key& tmpTk, double numberOfTuples, double writeFactor,
    size_t tupleSize, const sto::table_key& candidateTk) {
  double finalProjectedSize = 0.0;

  bool isThisACandidateNode = false;
  if (tmpTk.table_name != sto::OTHER_TABLENAME && tmpTk == candidateTk) {
    isThisACandidateNode = true;
  }

  // std::cout << "isThisACandidateNode: " << isThisACandidateNode << std::endl;

  // If a data structure is already materialised it
  // means that the data structure will be just read
  // and there are no writes happening
  if (isThisACandidateNode || bm->getSM().isInPersistedFileCatalog(tmpTk)) {
    finalProjectedSize = 1;
  } else {
    finalProjectedSize =
        getSizeOfProjectedFile(tmpTk, numberOfTuples * writeFactor, tupleSize);
  }

  // std::cout << "projectedFinalSizeForCandidate: finalProjectedSize "
  //         << finalProjectedSize << std::endl;
  return finalProjectedSize;
}

double NodeTree::projectedFinalSizeForMaterialisedDS(
    const sto::table_key& tmpTk, double numberOfTuples, double writeFactor,
    size_t tupleSize, const sto::table_key& candidateTk) {
  double finalProjectedSize = 0.0;

  bool shouldBeThisNodeExcluded = false;
  if (candidateTk.table_name != sto::OTHER_TABLENAME && tmpTk == candidateTk) {
    shouldBeThisNodeExcluded = true;
  }

  //  std::cout << "shouldBeThisNodeExcluded: " << shouldBeThisNodeExcluded
  //            << std::endl;

  if (!shouldBeThisNodeExcluded &&
      bm->getSM().isInPersistedFileCatalog(tmpTk)) {
    finalProjectedSize = 1;
  } else {
    finalProjectedSize =
        getSizeOfProjectedFile(tmpTk, numberOfTuples * writeFactor, tupleSize);
  }
  //  std::cout << "projectedFinalSizeForMaterialisedDS: finalProjectedSize "
  //            << finalProjectedSize << std::endl;
  return finalProjectedSize;
}

double NodeTree::getSizeOfProjectedFile(const sto::table_key& tmpTk,
                                        double numberOfTuples,
                                        size_t tupleSize) {
  double size = 0;
  if (tmpTk.version == sto::HASHJOIN) {
    size = bm->getSM().getPrimaryFileSizeInPagesForProjectedRelation(tmpTk);
  } else {
    size_t tuplesPerPage =
        static_cast<size_t>(sto::PAGE_SIZE_PERSISTENT / tupleSize);
    size = numberOfTuples / static_cast<double>(tuplesPerPage);
  }

  return size;
}

void NodeTree::leftFileDetails(double writeFactor, size_t tupleSize,
                               size_t projectedTupleSize,
                               double manualNumOfTuples) {
  leftWriteFactor = writeFactor;
  leftTupleSize = tupleSize;
  leftTupleSizeProjected = projectedTupleSize;
  manualNumberOfLeftTuples = manualNumOfTuples;
}

void NodeTree::rightFileDetails(double writeFactor, size_t tupleSize,
                                size_t projectedTupleSize,
                                double manualNumOfTuples) {
  rightWriteFactor = writeFactor;
  rightTupleSize = tupleSize;
  rightTupleSizeProjected = projectedTupleSize;
  manualNumberOfRightTuples = manualNumOfTuples;
}

void NodeTree::outputFileDetails(double writeFactor, double tupleSize) {
  outputWriteFactor = writeFactor;
  outputTupleSize = tupleSize;
}

bool NodeTree::isInFutureAppearances(const sto::table_key& tk) {
  return futureAppearances.find(tk) != futureAppearances.end();
}

}  // namespace queries
}  // namespace deceve
