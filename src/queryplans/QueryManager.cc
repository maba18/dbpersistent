/*******************************************************************************
 * Copyright (c) 2015 Michail Basios and Stratis Viglas.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michail Basios - initial API and implementation
 *     Stratis Viglas - initial API and implementation
 *******************************************************************************/


#include "QueryManager.hh"

namespace deceve {
namespace queries {

void QueryManager::addQuery(Pool_Node node) { all_queries.push(node); }

// void QueryManager::analyse_query_pool() {
//
//	COUT
//			<< "------------------------------- Analyse_query_pool START
//------------------------------- "
//			<< "\n";
//
//	for (std::vector<QOperator *>::iterator it = query_pool.begin();
//			it != query_pool.end(); ++it) {
//
//		COUT << "Query Id: " << (*it)->getQueryId() << "\n";
//
//		(*it)->printQueryPlan();
//
//		(*it)->exportQueryDetails();
//
//	}
//
//	COUT
//			<< "------------------------------- Analyse_query_pool END
//------------------------------- "
//			<< "\n";
//
//}

// void QueryManager::analyse_query_pool_window(size_t index, size_t
// window_size) {
//
//	COUT
//			<< "------------------------------- Analyse_query_pool START
//------------------------------- "
//			<< "\n";
//
//	if (index + window_size >= query_pool.size()) {
//		window_size = query_pool.size() - index;
//	}
//
//	size_t limit = index + window_size;
//
//
//	for (size_t i = index; i < limit; i++) {
//
//		COUT << "i: " << i << " ";
//		COUT << "Query Id: " << query_pool[i]->getQueryId() << "\n";
//
////		query_pool[i]->printQueryPlan();
//
//		query_pool[i]->exportQueryDetails(dataStructureId);
//
//	}
//
//	dataStructureId=0;
//
//	COUT
//			<< "------------------------------- Analyse_query_pool END
//------------------------------- "
//			<< "\n";
//
//}

// void QueryManager::analyseQueries(NodeTree *t, size_t index, size_t
// window_size) {
//
//	COUT
//			<< "------------------------------- Analyse_query_pool START
//------------------------------- "
//			<< "\n";
//
//	if (index + window_size >= query_pool.size()) {
//		window_size = query_pool.size() - index;
//	}
//
//	size_t limit = index + window_size;
//
//
//	for (size_t i = index; i < limit; i++) {
//
//		COUT << "i: " << i << " ";
//		COUT << "Query Id: " << query_pool[i]->getQueryId() << "\n";
//
////		query_pool[i]->printQueryPlan();
//
//		query_pool[i]->exportQueryDetails(t, dataStructureId);
//
//	}
//
//	dataStructureId=0;
//
//	COUT
//			<< "------------------------------- Analyse_query_pool END
//------------------------------- "
//			<< "\n";
//
//}
}
}
