/*
 * Query11Nodes.cc
 *
 *  Created on: Apr 20, 2015
 *      Author: maba18
 */

#include "Query11Nodes.hh"
#include <string>

namespace deceve {
namespace queries {

bool query11Counter = true;

void Query11Node0::run(deceve::storage::BufferManager* bm,
                       const std::string& input_name,
                       const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################
  //  COUT
  //      << "$$$$$$$$$$$$$$$--- Query11Node0 Execution (Select Nation)
  //      ---$$$$$$$$$$$$$$$"
  //      << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<NATION> reader(bm, full_input_name,
                                      bm->getFileMode(full_input_name));

  deceve::bama::Writer<NATION> res_writer(bm, output_full_path,
                                          deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  NATION item;
  srand(time(NULL));
  std::string nationNames[] = {
      "ALGERIA",    "ARGENTINA", "BRAZIL",        "CANADA",  "EGYPT",
      "ETHIOPIA",   "FRANCE",    "GERMANY",       "INDIA",   "INDONESIA",
      "IRAN",       "JAPAN",     "JORDAN",        "KENYA",   "MOROCCO",
      "MOZAMBIQUE", "PERU",      "CHINA",         "ROMANIA", "SAUDI ARABIA",
      "VIETNAM",    "RUSSIA",    "UNITED KINGDOM"};

  /* Generate random date and subtract it from the current date */
  std::string randomNationName = nationNames[rand() % 23];
  int counter = 0;
  while (reader.hasNext()) {
    item = reader.nextRecord();

    if (strcmp(item.N_NAME, randomNationName.c_str()) == 0) {
      res_writer.write(item);
      counter++;
    }
  }
  reader.close();
  res_writer.close();
}

void Query11Node1::run(deceve::storage::BufferManager* bm,
                       const std::string& left_file,
                       const std::string& right_file,
                       const std::string& simple_output_name) {
  // COUT << "Query11Node1 Execution (Join)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_supplier left_extractor(1);
  column_extractor_nation right_extractor(1);
  q11_join1_combinator_supplier_nation combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  joiner =
      new deceve::bama::GraceJoin<SUPPLIER, NATION, column_extractor_supplier,
                                  column_extractor_nation,
                                  q11_join1_combinator_supplier_nation>(
          bm, left, right, output_full_path, left_extractor, right_extractor,
          combinator, partitions, joinMode);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
}

void Query11Node2::run(deceve::storage::BufferManager* bm,
                       const std::string& left_file,
                       const std::string& right_file,
                       const std::string& simple_output_name) {
  // COUT << "Query11Node2 Execution (Join)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_partsupp left_extractor(1);
  q11_join2_extractor_result right_extractor(0);
  // q11_join2_combinator_partsupp_result combinator;

  q11_converter_PR_PARSTSUPP pr_left_converter;
  q11_extractor_PR_PARTSUPP pr_left_extractor(0);

  q11_converter_JOIN1_STRUCT_SUPPLIER_NATION pr_right_converter;

  q11_combinator_pr_supplier_other pr_final_combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  //  joiner = new deceve::bama::GraceJoin<
  //      PARTSUPP, Q11_JOIN1_STRUCT_SUPPLIER_NATION, column_extractor_partsupp,
  //      q11_join2_extractor_result, q11_join2_combinator_partsupp_result>(
  //      bm, left, right, output_full_path, left_extractor, right_extractor,
  //      combinator, partitions, joinMode);
  //
  //  joiner = new deceve::bama::GraceJoinConvertLeft<
  //      PARTSUPP, Q11_JOIN1_STRUCT_SUPPLIER_NATION, Q11_PR_PARSTSUPP,
  //      column_extractor_partsupp, q11_join2_extractor_result,
  //      q11_converter_PR_PARSTSUPP, q11_extractor_PR_PARTSUPP,
  //      q11_combinator_pr_supplier_other>(
  //      bm, left, right, output_full_path, left_extractor, right_extractor,
  //      pr_converter, pr_extractor, pr_combinator, partitions, joinMode,
  //      deceve::storage::LEFT_PROJECT);

  joiner = new deceve::bama::GraceJoinConvertBoth<
      PARTSUPP, Q11_PR_PARSTSUPP, Q11_JOIN1_STRUCT_SUPPLIER_NATION,
      Q11_JOIN1_STRUCT_SUPPLIER_NATION, column_extractor_partsupp,
      q11_join2_extractor_result, q11_converter_PR_PARSTSUPP,
      q11_converter_JOIN1_STRUCT_SUPPLIER_NATION, q11_extractor_PR_PARTSUPP,
      q11_join2_extractor_result, q11_combinator_pr_supplier_other>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      pr_left_converter, pr_right_converter, pr_left_extractor, right_extractor,
      pr_final_combinator, partitions, joinMode, deceve::storage::LEFT_PROJECT);

  joiner->setJoinTableKeys(left_tk, right_tk);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
}

void Query11Node3::run(deceve::storage::BufferManager* bm,
                       const std::string& input_name,
                       const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################
  //  COUT
  //      << "$$$$$$$$$$$$$$$--- Query11Node3 Execution (Sum Results)
  //      ---$$$$$$$$$$$$$$$"
  //      << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<Q11_JOIN2_STRUCT_PARTSUPP_RESULT> reader(
      bm, full_input_name, bm->getFileMode(full_input_name));

  deceve::bama::Writer<Q11_RESULT3_STRUCT_RESULT> res_writer(
      bm, output_full_path, deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  Q11_JOIN2_STRUCT_PARTSUPP_RESULT item;
  Q11_RESULT3_STRUCT_RESULT outputTuple;
  int counter = 0;
  while (reader.hasNext()) {
    item = reader.nextRecord();
    outputTuple = {item.ps_partkey, item.ps_availqty * item.ps_supplycost};
    counter++;
    res_writer.write(outputTuple);
  }
  reader.close();
  res_writer.close();

  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }

  if (query11Counter) {
    query11Counter = false;
    bm->removeFile(output_full_path);
  } else {
    query11Counter = true;
  }
}

void Query11Node4::run(deceve::storage::BufferManager* bm,
                       const std::string& input_name,
                       const std::string& simple_output_name) {
  //####################### Sorting Template
  //####################################

  //  std::cout
  //      << "******************** -Query11Node4 Execution (Sort)-
  //      ********************"
  //      << "\n";
  std::string output_full_path;
  /*
   * EXECUTE USING PERSISTED FILES
   */
  if (check_usePersistedResult_flag) {
    node_output_name = information_about_persisted_files.output_name;
    bm->getSM().incrementPersistentStatisticsHits(tk);
    // Inform next operator to unpin file on the persisted tmp catalog

  } else {
    //####################### Construct Filenames
    //####################################
    std::string full_input_name;

    if (input_file_path == deceve::storage::PRIMARY_PATH) {
      full_input_name = constructFullPath(input_name);
    } else {
      full_input_name = constructInputTmpFullPath(node_input_name);
    }

    output_full_path = constructOutputTmpFullPath(simple_output_name);

    //################################ OPERATION
    //#####################################
    q11_joinLast_extractor_result extractor(0);

    deceve::storage::FileMode sorterMode = deceve::storage::PRIMARY;
    if (check_PersistResult_flag) {
      sorterMode = deceve::storage::INTERMEDIATE;
    }

    sorter = new deceve::bama::ReplacementSort<Q11_RESULT3_STRUCT_RESULT,
                                               q11_joinLast_extractor_result>(
        bm, full_input_name, output_full_path, extractor, POOLSIZE, sorterMode);

    // EXECUTE AND PERSIST RESULTS
    if (check_PersistResult_flag) {
      sorter->persistResult();
      sorter->sort();

      if (!bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }

      // REMOVE FROM TMPFILESET
      bm->getSM().deleteFromPersistedTmpFileSet(tk);

      // ADD TO PERSISTED FILE CATALOG
      tv.num_files = sorter->getNumberOfRuns();
      tv.output_name = simple_output_name;
      tv.full_output_path = output_full_path;
      tv.pins = 0;
      bm->getSM().insertInPersistedFileCatalog(std::make_pair(tk, tv));
    } else {
      sorter->sort();

      if (!bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }
    }

    delete sorter;
    //############################ Delete Previous File
    //####################################
    if (!bm->getSM().isInTableCatalog(full_input_name) &&
        !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
      bm->removeFile(full_input_name);
    }
  }

  bm->removeFile(output_full_path);
}
}
/* namespace deceve */
}
