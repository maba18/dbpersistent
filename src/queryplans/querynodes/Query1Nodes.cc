/*
 * Query1Nodes.cc
 *
 *  Created on: 11 Mar 2015
 *      Author: michail
 */

#include "Query1Nodes.hh"

namespace deceve {
namespace queries {

void Query1Node1::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  //  COUT << "******************** -Query1Node1 Execution (Select)- "
  //          "********************"
  //       << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<LINEITEM> reader(bm, full_input_name,
                                        bm->getFileMode(full_input_name));

  deceve::bama::Writer<q1_struct> res_writer(bm, output_full_path,
                                             deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  LINEITEM item;
  /* Generate random date and subtract it from the current date */
  //  srand(time(NULL));
  unsigned int seed = time(NULL);
  int delta = 60 + rand_r(&seed) % 60 + 1;
  date d(1998, 12, 1);
  long num_days = gday(d);
  num_days = num_days - delta;
  date rdate = dtf(num_days);

  //################################ OPERATION1
  //#####################################
  q1_struct os;
  std::map<std::pair<char, char>, q1_struct> memoryMap;

  while (reader.hasNext()) {
    item = reader.nextRecord();
    date tmpdate(item.L_SHIPDATE);
    if (tmpdate <= rdate) {
      // res_writer.write(item);

      std::pair<char, char> group_key =
          std::make_pair(item.L_RETURNFLAG, item.L_LINESTATUS);
      os.l_returnflag = item.L_RETURNFLAG;
      os.l_linestatus = item.L_LINESTATUS;
      os.sum_qty = item.L_QUANTITY;
      os.sum_base_price = item.L_EXTENDEDPRICE;
      os.sum_disc_price = item.L_EXTENDEDPRICE * (1 - item.L_DISCOUNT);
      os.sum_charge =
          item.L_EXTENDEDPRICE * (1 - item.L_DISCOUNT) * (1 + item.L_TAX);
      os.count_order = 1;
      os.avg_qty = item.L_QUANTITY;
      os.avg_price = item.L_EXTENDEDPRICE;
      os.tmp_sum_disc = item.L_DISCOUNT;
      os.avg_disc = item.L_DISCOUNT;
      std::map<std::pair<char, char>, q1_struct>::iterator it =
          memoryMap.find(group_key);
      if (it == memoryMap.end()) {
        memoryMap[group_key] = os;
      } else {
        it->second.sum_qty += os.sum_qty;
        it->second.sum_base_price += os.sum_base_price;
        it->second.sum_disc_price += os.sum_disc_price;
        it->second.sum_charge += os.sum_charge;
        it->second.count_order += 1;
        it->second.avg_qty = it->second.sum_qty / it->second.count_order;
        it->second.avg_price =
            it->second.sum_base_price / it->second.count_order;
        it->second.tmp_sum_disc += os.tmp_sum_disc;
        it->second.avg_disc = it->second.tmp_sum_disc / it->second.count_order;

        //      q1_combineStructs((q1_struct &) it->second, os);
      }
    }
  }

  for (std::map<std::pair<char, char>, q1_struct>::iterator it =
           memoryMap.begin();
       it != memoryMap.end(); ++it) {
    q1_column_extractor extractor(0);
    res_writer.write(it->second);
  }

  reader.close();
  res_writer.close();

  bm->removeFile(output_full_path);

}

void Query1Node2::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Group Template ####################################

  //  COUT
  //      << "******************** -Query1Node2 Execution (Group)-
  //      ********************"
  //      << "\n";

  //####################### Construct Filenames
  //####################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //################################
  deceve::bama::Reader<LINEITEM> reader(bm, full_input_name,
                                        bm->getFileMode(full_input_name));

  deceve::bama::Writer<q1_struct> res_writer(bm, output_full_path,
                                             deceve::storage::PRIMARY);

  //################################ OPERATION
  //#####################################
  q1_struct os;
  std::map<std::pair<char, char>, q1_struct> memoryMap;
  while (reader.hasNext()) {
    LINEITEM item = reader.nextRecord();
    std::pair<char, char> group_key =
        std::make_pair(item.L_RETURNFLAG, item.L_LINESTATUS);
    os.l_returnflag = item.L_RETURNFLAG;
    os.l_linestatus = item.L_LINESTATUS;
    os.sum_qty = item.L_QUANTITY;
    os.sum_base_price = item.L_EXTENDEDPRICE;
    os.sum_disc_price = item.L_EXTENDEDPRICE * (1 - item.L_DISCOUNT);
    os.sum_charge =
        item.L_EXTENDEDPRICE * (1 - item.L_DISCOUNT) * (1 + item.L_TAX);
    os.count_order = 1;
    os.avg_qty = item.L_QUANTITY;
    os.avg_price = item.L_EXTENDEDPRICE;
    os.tmp_sum_disc = item.L_DISCOUNT;
    os.avg_disc = item.L_DISCOUNT;
    std::map<std::pair<char, char>, q1_struct>::iterator it =
        memoryMap.find(group_key);
    if (it == memoryMap.end()) {
      memoryMap[group_key] = os;
    } else {
      it->second.sum_qty += os.sum_qty;
      it->second.sum_base_price += os.sum_base_price;
      it->second.sum_disc_price += os.sum_disc_price;
      it->second.sum_charge += os.sum_charge;
      it->second.count_order += 1;
      it->second.avg_qty = it->second.sum_qty / it->second.count_order;
      it->second.avg_price = it->second.sum_base_price / it->second.count_order;
      it->second.tmp_sum_disc += os.tmp_sum_disc;
      it->second.avg_disc = it->second.tmp_sum_disc / it->second.count_order;

      // q1_combineStructs((q1_struct &) it->second, os);
    }
  }
  reader.close();

  for (std::map<std::pair<char, char>, q1_struct>::iterator it =
           memoryMap.begin();
       it != memoryMap.end(); ++it) {
    q1_column_extractor extractor(0);
    res_writer.write(it->second);
  }
  res_writer.close();

  //############################ Delete Previous File
  //####################################

  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }



}

void Query1Node3::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Sorting Template
  //####################################

  //  COUT
  //      << "******************** -Query1Node3 Execution (Sort)-
  //      ********************"
  //      << "\n";
  std::string output_full_path;
  /*
   * EXECUTE USING PERSISTED FILES
   */
  if (check_usePersistedResult_flag) {
    node_output_name = information_about_persisted_files.output_name;
    bm->getSM().incrementPersistentStatisticsHits(tk);
    // Inform next operator to unpin file on the persisted tmp catalog

  } else {
    //####################### Construct Filenames
    //####################################
    std::string full_input_name;

    if (input_file_path == deceve::storage::PRIMARY_PATH) {
      full_input_name = constructFullPath(input_name);
    } else {
      full_input_name = constructInputTmpFullPath(node_input_name);
    }

    output_full_path = constructOutputTmpFullPath(simple_output_name);

    //################################ OPERATION
    //#####################################
    q1_column_extractor extractor(0);

    deceve::storage::FileMode sorterMode = deceve::storage::PRIMARY;
    if (check_PersistResult_flag) {
      sorterMode = deceve::storage::INTERMEDIATE;
    }

    sorter = new deceve::bama::ReplacementSort<q1_struct, q1_column_extractor>(
        bm, full_input_name, output_full_path, extractor, POOLSIZE, sorterMode);

    // EXECUTE AND PERSIST RESULTS
    if (check_PersistResult_flag) {
      sorter->persistResult();
      sorter->sort();

      if (!bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }

      // REMOVE FROM TMPFILESET
      bm->getSM().deleteFromPersistedTmpFileSet(tk);

      // ADD TO PERSISTED FILE CATALOG
      tv.num_files = sorter->getNumberOfRuns();
      tv.output_name = simple_output_name;
      tv.full_output_path = output_full_path;
      tv.pins = 0;
      bm->getSM().insertInPersistedFileCatalog(std::make_pair(tk, tv));
    } else {
      // SIMPLY EXECUTE
      sorter->sort();

      if (tk.version == deceve::storage::SORT &&
          !bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }
    }

    delete sorter;
    //############################ Delete Previous File
    //####################################
    if (!bm->getSM().isInTableCatalog(full_input_name) &&
        !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
      bm->removeFile(full_input_name);
    }
  }
}

void Query1Node4::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  //  COUT
  //      << "******************** -Query1Node4 Execution (Select)-
  //      ********************"
  //      << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################
  deceve::bama::Reader<q1_struct> reader(bm, full_input_name,
                                         bm->getFileMode(full_input_name));
  //################################ OPERATION
  //########################################

  //############################ Delete Previous File
  //####################################
  // check if the input is a primary table and not an intermediate results
  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }

  bm->removeFile(output_full_path);
}


}  // namespace queries
}  // namespace deceve
