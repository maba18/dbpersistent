/*
 * Query8Nodes.hh
 *
 *  Created on: 17 Mar 2015
 *      Author: michail
 */

#ifndef QUERY8NODES_HH_
#define QUERY8NODES_HH_

#include <sstream>

// Include General Functions
#include "../definitions.hh"
#include "../../utils/global.hh"

// Include base class
#include "../NodeTree.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

// Include Algorithm Classes
#include "../../algorithms/replacementsort.hh"
#include "../../algorithms/gracejoin.hh"
#include "../../algorithms/gracejoinConvertRight.hh"
#include "../../algorithms/gracejoinConvertBoth.hh"

#include <chrono>

#include <vector>

namespace deceve {
namespace queries {

struct comparatorQ81Left {
  comparatorQ81Left() {
    types1 = {"STANDARD", "SMALL", "MEDIUM", "LARGE", "ECONOMY", "PROMO"};
    types2 = {"ANODIZED", "BURNISHED", "PLATED", "POLISHED", "BRUSHED"};
    types3 = {"TIN", "NICKEL", "BRASS", "STEEL", "COPPER"};

    srand(time(NULL));
    std::stringstream s;

    randomIndex = rand() % types1.size();
    s << types1[randomIndex] << " ";
    randomIndex = rand() % types2.size();
    s << types2[randomIndex] << " ";
    randomIndex = rand() % types3.size();
    s << types2[randomIndex] << " ";
    type = s.str();
  }

  bool operator()(PART item) {
    std::string typeTuple = string(item.P_TYPE);
    //    std::cout<<typeTuple<<" "<<type<<std::endl;
    if (typeTuple == type) {
      return true;
    }
    return false;
  }

  std::vector<std::string> types1;
  std::vector<std::string> types2;
  std::vector<std::string> types3;
  std::string type;
  int randomIndex;
};

struct comparatorQ83Right {
  comparatorQ83Right() {
    /* Generate random date and subtract it from the current date */
    d1 = {1995, 1, 1};
    d2 = {1996, 12, 31};
  }

  bool operator()(ORDER item) {
    date tmpdate(item.O_ORDERDATE);
    if (d1 <= tmpdate && tmpdate <= d2) {
      return true;
    }
    return false;
  }

  date d1;
  date d2;
};

class Query8Node0 : public NodeTree {
 public:
  Query8Node0(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    // P_TYPE
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query8Node1 : public NodeTree {
 public:
  Query8Node1(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;
    //		left_tk.table_name = "part";
    //		right_tk.table_name = "lineitem";
    //		left_tk.field = "P_PARTKEY";
    //		right_tk.field = "L_PARTKEY";
    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::HASHJOIN;
    left_tk.table_name = deceve::storage::PART;
    right_tk.table_name = deceve::storage::LINEITEM;
    left_tk.field = deceve::storage::OTHER_FIELD;

    //    right_tk.field = bm->getStorageManager().getRandomPartKeyField(0);
    right_tk.field = deceve::storage::L_PARTKEY;

#ifdef UNIFIED
    right_tk.projected_field = deceve::storage::LINEITEM_UNIFIED;
#else
    right_tk.projected_field = deceve::storage::LINEITEM_Q8;
#endif

    right_tk.attributes = {sto::L_PARTKEY, sto::L_ORDERKEY, sto::L_SUPPKEY,
                           sto::L_EXTENDEDPRICE, sto::L_DISCOUNT};

    //        localVariable.L_PARTKEY = w.L_PARTKEY;
    //         localVariable.L_ORDERKEY = w.L_ORDERKEY;
    //         localVariable.L_SUPPKEY = w.L_SUPPKEY;
    //         localVariable.L_EXTENDEDPRICE = w.L_EXTENDEDPRICE;
    //         localVariable.L_DISCOUNT = w.L_DISCOUNT;

    leftFileDetails(0.000, sizeof(PART), sizeof(Q8_PR_PART));
    rightFileDetails(1, sizeof(LINEITEM), sizeof(Q8_PR_LINEITEM));
    outputFileDetails(0.000, sizeof(Q8_JOIN1_STRUCT_PART_LINEITEM));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  //  deceve::bama::GraceJoin<PART, LINEITEM, column_extractor_part,
  //                          column_extractor_lineitem,
  //                          q8_combinator_part_line,
  //                          std::less<int>, comparatorQ81Left,
  //                          deceve::bama::DefaultComparator<LINEITEM> >
  //                          *joiner;

  //  deceve::bama::GraceJoinConvert<PART, LINEITEM, Q8_PR_LINEITEM,
  //  column_extractor_part, column_extractor_lineitem,
  //      q8_converter_PR_LINEITEM, q8_extractor_PR_LINEITEM,
  //      q8_combinator_part_pr_lineitem, std::less<int>,
  //      comparatorQ81Left, deceve::bama::DefaultComparator<LINEITEM> >
  //      *joiner1;

  deceve::bama::GraceJoinConvertBoth<
      PART, Q8_PR_PART, LINEITEM, Q8_PR_LINEITEM, column_extractor_part,
      column_extractor_lineitem, q8_converter_PR_PART, q8_converter_PR_LINEITEM,
      q8_extractor_PR_PART, q8_extractor_PR_LINEITEM,
      q8_combinator_pr_part_pr_lineitem, std::less<int>, comparatorQ81Left,
      deceve::bama::DefaultComparator<LINEITEM> > *joiner;
};

class Query8Node2 : public NodeTree {
 public:
  Query8Node2(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    // O_ORDERDATE
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query8Node3 : public NodeTree {
 public:
  Query8Node3(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;
    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::OTHER_TABLENAME;
    right_tk.table_name = deceve::storage::ORDERS;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::OTHER_FIELD;

    leftFileDetails(0.00, sizeof(Q8_JOIN1_STRUCT_PART_LINEITEM),
                    sizeof(Q8_JOIN1_STRUCT_PART_LINEITEM), 0.01);
    rightFileDetails(0.25, sizeof(ORDER), sizeof(Q8_PR_ORDER));
    outputFileDetails(0.00, sizeof(Q8_JOIN2_STRUCT_RESULT_ORDERS));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  //  deceve::bama::GraceJoin<Q8_JOIN1_STRUCT_PART_LINEITEM, ORDER,
  //  q8_join2_extractor_result, column_extractor_order,
  //      q8_combinator_result_orders, std::less<int>,
  //      deceve::bama::DefaultComparator<Q8_JOIN1_STRUCT_PART_LINEITEM>,
  //      comparatorQ83Right> *joiner1;

  deceve::bama::GraceJoinConvertRight<
      Q8_JOIN1_STRUCT_PART_LINEITEM, ORDER, Q8_PR_ORDER,
      q8_join2_extractor_result, column_extractor_order, q8_converter_PR_ORDER,
      q8_extractor_PR_ORDER, q8_combinator_other_pr_order, std::less<int>,
      deceve::bama::DefaultComparator<Q8_JOIN1_STRUCT_PART_LINEITEM>,
      comparatorQ83Right> *joiner;
};

class Query8Node4 : public NodeTree {
 public:
  Query8Node4(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
    tk.version = deceve::storage::HASHJOIN;
    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::HASHJOIN;
    left_tk.table_name = deceve::storage::OTHER_TABLENAME;
    right_tk.table_name = deceve::storage::CUSTOMER;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::C_CUSTKEY;

    leftFileDetails(0.054, sizeof(Q8_JOIN2_STRUCT_RESULT_ORDERS),
                    sizeof(Q8_JOIN2_STRUCT_RESULT_ORDERS), 0.01);
    rightFileDetails(1, sizeof(CUSTOMER), sizeof(Q8_PR_CUSTOMER));
    outputFileDetails(0.001, sizeof(Q8_JOIN3_STRUCT_RESULT_CUSTOMER));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  //  deceve::bama::GraceJoin<Q8_JOIN2_STRUCT_RESULT_ORDERS, CUSTOMER,
  //  q8_join3_extractor_result, column_extractor_customer,
  //      q8_combinator_result_customers> *joiner1;

  deceve::bama::GraceJoinConvertRight<
      Q8_JOIN2_STRUCT_RESULT_ORDERS, CUSTOMER, Q8_PR_CUSTOMER,
      q8_join3_extractor_result, column_extractor_customer,
      q8_converter_PR_CUSTOMER, q8_extractor_PR_CUSTOMER,
      q8_combinator_other_pr_customer> *joiner;
};

class Query8Node5 : public NodeTree {
 public:
  Query8Node5(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    tk.version = deceve::storage::HASHJOIN;
    //		left_tk.table_name = "result";
    //		right_tk.table_name = "nation";
    //		left_tk.field = "C_NATIONKEY";
    //		right_tk.field = "N_NATIONKEY";
    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::OTHER_TABLENAME;
    right_tk.table_name = deceve::storage::OTHER_TABLENAME;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::OTHER_FIELD;

    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::GraceJoin<Q8_JOIN3_STRUCT_RESULT_CUSTOMER, NATION,
                          q8_join4_extractor_result, column_extractor_nation,
                          q8_combinator_result_nation> *joiner;
};

class Query8Node6 : public NodeTree {
 public:
  Query8Node6(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    //		tk.table_name = "region";
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    //		tk.field = "R_NAME";
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query8Node7 : public NodeTree {
 public:
  Query8Node7(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    tk.version = deceve::storage::HASHJOIN;
    //		left_tk.table_name = "result";
    //		right_tk.table_name = "region";
    //		left_tk.field = "N_REGIONKEY";
    //		right_tk.field = "R_REGIONKEY";
    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::OTHER_TABLENAME;
    right_tk.table_name = deceve::storage::OTHER_TABLENAME;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::OTHER_FIELD;

    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::GraceJoin<Q8_JOIN4_STRUCT_RESULT_NATION, REGION,
                          q8_join5_extractor_result, column_extractor_region,
                          q8_combinator_result_region> *joiner;
};

class Query8Node8 : public NodeTree {
 public:
  Query8Node8(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    tk.version = deceve::storage::HASHJOIN;
    //		left_tk.table_name = "result";
    //		right_tk.table_name = "supplier";
    //		left_tk.field = "L_SUPPKEY";
    //		right_tk.field = "S_SUPPKEY";
    left_tk.version = deceve::storage::OTHER;
    // SUPPLIER_USAGE
    //    right_tk.version = deceve::storage::HASHJOIN;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::OTHER_TABLENAME;
    right_tk.table_name = deceve::storage::SUPPLIER;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::S_SUPPKEY;

    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::GraceJoin<Q8_JOIN5_STRUCT_RESULT_REGION, SUPPLIER,
                          q8_join6_extractor_result, column_extractor_supplier,
                          q8_combinator_result_supplier> *joiner;
};

class Query8Node9 : public NodeTree {
 public:
  Query8Node9(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    tk.version = deceve::storage::HASHJOIN;
    //		left_tk.table_name = "result";
    //		right_tk.table_name = "nation";
    //		left_tk.field = "N_NATIONKEY";
    //		right_tk.field = "N_NATIONKEY";
    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::OTHER_TABLENAME;
    right_tk.table_name = deceve::storage::OTHER_TABLENAME;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::OTHER_FIELD;

    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::GraceJoin<Q8_JOIN6_STRUCT_RESULT_SUPPLIER, NATION,
                          q8_join7_extractor_result, column_extractor_nation,
                          q8_combinator_result_nation1> *joiner;
};

class Query8Node10 : public NodeTree {
 public:
  Query8Node10(deceve::storage::BufferManager *bmr,
               const std::string &input_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query8Node11 : public NodeTree {
 public:
  Query8Node11(deceve::storage::BufferManager *bmr,
               const std::string &input_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    //		tk.table_name = "result_group_sort";
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;
    sorter = NULL;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);

 private:
  deceve::bama::ReplacementSort<Q8_JOIN7_STRUCT_RESULT_NATION1,
                                q8_node11_extractor_result> *sorter;
};
}
} /* namespace deceve */

#endif /* QUERY8NODES_HH_ */
