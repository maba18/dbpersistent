/*
 * Query3Nodes.cc
 *
 *  Created on: 2 Jun 2015
 *      Author: mike
 */

#include "Query3Nodes.hh"

namespace deceve {
namespace queries {

void Query3Node1::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  //  COUT << "Query3Node1 Execution (Select)" << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }
  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<CUSTOMER> reader(bm, full_input_name,
                                        bm->getFileMode(full_input_name));

  deceve::bama::Writer<CUSTOMER> res_writer(bm, output_full_path,
                                            deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  /* Generate random date and substract it from the current date */
  srand(time(NULL));
  CUSTOMER item;
  std::string segments[] = {"AUTOMOBILE", "BUILDING", "FURNITURE", "MACHINERY",
                            "HOUSEHOLD"};
  std::string randomSegment = segments[rand() % 5];
  while (reader.hasNext()) {
    item = reader.nextRecord();
    //    if (strcmp(item.C_MKTSEGMENT, randomSegment.c_str()) == 0) {
    res_writer.write(item);
    //    }
  }
  reader.close();
  res_writer.close();

  //############################ Delete Previous File
  //####################################
  //  check if the input is a primary table and not an intermediate results
  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }
}

void Query3Node2::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  //  COUT << "Query3Node2 Execution (Select)" << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }
  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<ORDER> reader(bm, full_input_name,
                                     bm->getFileMode(full_input_name));

  deceve::bama::Writer<ORDER> res_writer(bm, output_full_path,
                                         deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  /* Generate random date and substract it from the current date */
  srand(time(NULL));
  int randomDay = rand() % 31 + 1;

  ORDER item;
  date d1(1995, 03, randomDay);

  while (reader.hasNext()) {
    item = reader.nextRecord();
    date tmpdate(item.O_ORDERDATE);

    //    if (tmpdate < d1) {
    res_writer.write(item);
    //    }
  }
  reader.close();
  res_writer.close();

  //############################ Delete Previous File
  //####################################
  //  check if the input is a primary table and not an intermediate results
  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }
}

void Query3Node3::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  //  COUT << "Query3Node3 Execution (Join)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_order left_extractor(0);
  column_extractor_customer right_extractor(0);
  // q3_join1_combinator_order_customer combinator;

  q3_converter_PR_ORDER pr_left_converter;
  q3_extractor_PR_ORDER pr_left_extractor(0);

  q3_converter_PR_CUSTOMER pr_right_converter;
  q3_extractor_PR_CUSTOMER pr_right_extractor(0);

  q3_combinator_pr_order_pr_customer pr_final_combinator;

  //  comparatorQ33Left leftComparator;
  //  comparatorQ33Right rightComparator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  //  joiner = new deceve::bama::GraceJoin<
  //      ORDER, CUSTOMER, column_extractor_order, column_extractor_customer,
  //      q3_join1_combinator_order_customer, std::less<int>, comparatorQ33Left,
  //      comparatorQ33Right>(bm, left, right, output_full_path, left_extractor,
  //                          right_extractor, combinator, partitions,
  //                          joinMode);

  joiner = new deceve::bama::GraceJoinConvertBoth<
      ORDER, Q3_PR_ORDER, CUSTOMER, Q3_PR_CUSTOMER, column_extractor_order,
      column_extractor_customer, q3_converter_PR_ORDER,
      q3_converter_PR_CUSTOMER, q3_extractor_PR_ORDER, q3_extractor_PR_CUSTOMER,
      q3_combinator_pr_order_pr_customer, std::less<int>, comparatorQ33Left,
      comparatorQ33Right>(bm, left, right, output_full_path, left_extractor,
                          right_extractor, pr_left_converter,
                          pr_right_converter, pr_left_extractor,
                          pr_right_extractor, pr_final_combinator, partitions,
                          joinMode, deceve::storage::BOTH_PROJECT);

  joiner->setJoinTableKeys(left_tk, right_tk);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);

  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
}

void Query3Node4::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  //  COUT << "Query3Node4 Execution (Select)" << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }
  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<LINEITEM> reader(bm, full_input_name,
                                        bm->getFileMode(full_input_name));

  deceve::bama::Writer<LINEITEM> res_writer(bm, output_full_path,
                                            deceve::storage::PRIMARY);
  //################################ OPERATION
  //########################################

  /* Generate random date and substract it from the current date */
  srand(time(NULL));
  int randomDay = rand() % 31 + 1;

  LINEITEM item;
  date d1(1995, 03, randomDay);
  while (reader.hasNext()) {
    item = reader.nextRecord();
    date tmpdate(item.L_SHIPDATE);
    if (d1 < tmpdate) {
      res_writer.write(item);
    }
  }
  reader.close();
  res_writer.close();

  //############################ Delete Previous File
  //####################################
  //  check if the input is a primary table and not an intermediate results
  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }
}

void Query3Node5::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  // COUT << "Query3Node5 Execution (Join)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_lineitem left_extractor(1);
  q3_join1_extractor_result right_extractor(0);
  // q3_join2_combinator_lineiten_result combinator;

  q3_converter_PR_LINEITEM pr_converter;
  q3_extractor_PR_LINEITEM pr_extractor(0);
  q3_combinator_pr_lineitem_other pr_combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  //  joiner = new deceve::bama::GraceJoin<
  //      LINEITEM, Q3_JOIN1_STRUCT_ORDER_CUSTOMER, column_extractor_lineitem,
  //      q3_join1_extractor_result, q3_join2_combinator_lineiten_result,
  //      std::less<int>, comparatorQ34Left,
  //      deceve::bama::DefaultComparator<Q3_JOIN1_STRUCT_ORDER_CUSTOMER> >(
  //      bm, left, right, output_full_path, left_extractor, right_extractor,
  //      combinator, partitions, joinMode);

  joiner = new deceve::bama::GraceJoinConvertLeft<
      LINEITEM, Q3_JOIN1_STRUCT_ORDER_CUSTOMER, Q3_PR_LINEITEM,
      column_extractor_lineitem, q3_join1_extractor_result,
      q3_converter_PR_LINEITEM, q3_extractor_PR_LINEITEM,
      q3_combinator_pr_lineitem_other, std::less<int>, comparatorQ34Left,
      deceve::bama::DefaultComparator<Q3_JOIN1_STRUCT_ORDER_CUSTOMER> >(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      pr_converter, pr_extractor, pr_combinator, partitions, joinMode,
      deceve::storage::LEFT_PROJECT);

  joiner->setJoinTableKeys(left_tk, right_tk);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);

  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
}

void Query3Node6::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  // COUT << "Query3Node6 Execution (Group)" << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }
  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<Q3_JOIN2_STRUCT_LINEITEM_RESULT> reader(
      bm, full_input_name, bm->getFileMode(full_input_name));

  deceve::bama::Writer<Q3_JOIN2_STRUCT_LINEITEM_RESULT> res_writer(
      bm, output_full_path, deceve::storage::PRIMARY);
  //################################ OPERATION
  //########################################

  /* Generate random date and substract it from the current date */
  srand(time(NULL));

  // Write back a random percentage of the initial file
  // Around 60% should be fine
  int randNumber = rand() % 10;

  Q3_JOIN2_STRUCT_LINEITEM_RESULT item;
  while (reader.hasNext()) {
    randNumber = rand() % 10;
    item = reader.nextRecord();
    if (randNumber < 6) {
      res_writer.write(item);
    }
  }
  reader.close();
  res_writer.close();

  //############################ Delete Previous File
  //####################################
  //  check if the input is a primary table and not an intermediate results
  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }
}

void Query3Node7::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Sorting Template
  //####################################

  //  COUT
  //      << "******************** -Query3Node7 Execution (Sort)-
  //      ********************"
  //      << "\n";
  std::string output_full_path;
  /*
   * EXECUTE USING PERSISTED FILES
   */
  if (check_usePersistedResult_flag) {
    node_output_name = information_about_persisted_files.output_name;
    bm->getSM().incrementPersistentStatisticsHits(tk);

  } else {
    //####################### Construct Filenames
    //####################################
    std::string full_input_name;

    if (input_file_path == deceve::storage::PRIMARY_PATH) {
      full_input_name = constructFullPath(input_name);
    } else {
      full_input_name = constructInputTmpFullPath(node_input_name);
    }

    output_full_path = constructOutputTmpFullPath(simple_output_name);

    //################################ OPERATION
    //#####################################
    q3_join2_extractor_result extractor(0);

    deceve::storage::FileMode sorterMode = deceve::storage::PRIMARY;
    if (check_PersistResult_flag) {
      sorterMode = deceve::storage::INTERMEDIATE;
    }

    sorter = new deceve::bama::ReplacementSort<Q3_JOIN2_STRUCT_LINEITEM_RESULT,
                                               q3_join2_extractor_result>(
        bm, full_input_name, output_full_path, extractor, POOLSIZE, sorterMode);

    // EXECUTE AND PERSIST RESULTS
    if (check_PersistResult_flag) {
      sorter->persistResult();
      sorter->sort();

      if (!bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }

      // REMOVE FROM TMPFILESET
      bm->getSM().deleteFromPersistedTmpFileSet(tk);

      // ADD TO PERSISTED FILE CATALOG
      tv.num_files = sorter->getNumberOfRuns();
      tv.output_name = simple_output_name;
      tv.full_output_path = output_full_path;
      tv.pins = 0;
      bm->getSM().insertInPersistedFileCatalog(std::make_pair(tk, tv));
    } else {
      // SIMPLY EXECUTE
      sorter->sort();

      if (tk.version == deceve::storage::SORT &&
          !bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }
    }

    delete sorter;
    //############################ Delete Previous File
    //####################################
    if (!bm->getSM().isInTableCatalog(full_input_name) &&
        !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
      bm->removeFile(full_input_name);
    }
  }

  // scan<Q3_JOIN2_STRUCT_LINEITEM_RESULT>(output_full_path);

  bm->removeFile(output_full_path);
}
}  //  namespace queries
}  //  namespace deceve
