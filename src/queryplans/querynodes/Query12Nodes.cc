/*
 * Query12Nodes.cc
 *
 *  Created on: 31 Jul 2015
 *      Author: mike
 */

#include "Query12Nodes.hh"

namespace deceve {
namespace queries {

void Query12Node1::run(deceve::storage::BufferManager* bm,
                       const std::string& left_file,
                       const std::string& right_file,
                       const std::string& simple_output_name) {
  //#################################### Construct full paths for left and right
  // file ######################################

  // std::cout << "Query12Node1" << std::endl;

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_lineitem left_extractor(1);
  column_extractor_order right_extractor(1);
  //q12_combinator_lineitem_order combinator;

  q12_converter_PR_LINEITEM pr_left_converter;
  q12_extractor_PR_LINEITEM pr_left_extractor(0);

  q12_converter_PR_ORDER pr_right_converter;
  q12_extractor_PR_ORDER pr_right_extractor(0);

  q12_combinator_pr_lineitem_pr_order pr_final_combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  //  joiner = new deceve::bama::GraceJoin<
  //      LINEITEM, ORDER, column_extractor_lineitem, column_extractor_order,
  //      q12_combinator_lineitem_order, std::less<int>, comparatorQ121Left,
  //      deceve::bama::DefaultComparator<ORDER> >(
  //      bm, left, right, output_full_path, left_extractor, right_extractor,
  //      combinator, partitions, joinMode);

  joiner = new deceve::bama::GraceJoinConvertBoth<
      LINEITEM, Q12_PR_LINEITEM, ORDER, Q12_PR_ORDER, column_extractor_lineitem,
      column_extractor_order, q12_converter_PR_LINEITEM, q12_converter_PR_ORDER,
      q12_extractor_PR_LINEITEM, q12_extractor_PR_ORDER,
      q12_combinator_pr_lineitem_pr_order, std::less<int>, comparatorQ121Left,
      deceve::bama::DefaultComparator<ORDER>>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      pr_left_converter, pr_right_converter, pr_left_extractor,
      pr_right_extractor, pr_final_combinator, partitions, joinMode,
      deceve::storage::BOTH_PROJECT);

  joiner->setJoinTableKeys(left_tk, right_tk);

  //#################################### Decide to persist or use persisted
  // results ##################################
  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);

  //  scan<Q12_JOIN1_STRUCT_LINEITEM_ORDER>(output_full_path);
}

void Query12Node2::run(deceve::storage::BufferManager* bm,
                       const std::string& input_name,
                       const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  //####################### Construct Filenames
  //###################################
  // std::cout << "Query12Node2" << std::endl;
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }
  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<Q12_JOIN1_STRUCT_LINEITEM_ORDER> reader(
      bm, full_input_name, bm->getFileMode(full_input_name));

  deceve::bama::Writer<Q12_JOIN1_STRUCT_LINEITEM_ORDER> res_writer(
      bm, output_full_path, deceve::storage::PRIMARY);
  //################################ OPERATION
  //########################################

  /* Generate random date and substract it from the current date */
  srand(time(NULL));

  // Write back a random percentage of the initial file
  std::map<std::string, int> groupingMap;
  Q12_JOIN1_STRUCT_LINEITEM_ORDER item;
  while (reader.hasNext()) {
    item = reader.nextRecord();
    std::string key = std::string(item.l_shipmode);

    auto it = groupingMap.find(key);
    if (it == groupingMap.end()) {
      groupingMap.insert(std::make_pair(key, 1));
    } else {
      it->second++;
    }
  }
  reader.close();

  Q12_JOIN1_STRUCT_LINEITEM_ORDER tempItem;
  for (auto it : groupingMap) {
    strcpy(tempItem.l_shipmode, it.first.c_str());
    res_writer.write(tempItem);
  }

  res_writer.close();

  //############################ Delete Previous File
  //####################################
  //  check if the input is a primary table and not an intermediate results
  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }

  //  scan<Q12_JOIN1_STRUCT_LINEITEM_ORDER>(output_full_path);
}

void Query12Node3::run(deceve::storage::BufferManager* bm,
                       const std::string& input_name,
                       const std::string& simple_output_name) {
  //####################### Sorting Template
  //####################################

  //  COUT
  //      << "******************** -Query1Node3 Execution (Sort)-
  //      ********************"
  //      << "\n";
  // std::cout << "Query12Node3" << std::endl;

  std::string output_full_path;
  /*
   * EXECUTE USING PERSISTED FILES
   */
  if (check_usePersistedResult_flag) {
    node_output_name = information_about_persisted_files.output_name;
    bm->getSM().incrementPersistentStatisticsHits(tk);
  } else {
    //####################### Construct Filenames
    //####################################
    std::string full_input_name;

    if (input_file_path == deceve::storage::PRIMARY_PATH) {
      full_input_name = constructFullPath(input_name);
    } else {
      full_input_name = constructInputTmpFullPath(node_input_name);
    }

    output_full_path = constructOutputTmpFullPath(simple_output_name);

    //################################ OPERATION
    //#####################################
    q12_extractor_final_result extractor(0);

    deceve::storage::FileMode sorterMode = deceve::storage::PRIMARY;
    if (check_PersistResult_flag) {
      sorterMode = deceve::storage::INTERMEDIATE;
    }

    sorter = new deceve::bama::ReplacementSort<Q12_JOIN1_STRUCT_LINEITEM_ORDER,
                                               q12_extractor_final_result>(
        bm, full_input_name, output_full_path, extractor, POOLSIZE, sorterMode);

    // EXECUTE AND PERSIST RESULTS
    if (check_PersistResult_flag) {
      sorter->persistResult();
      sorter->sort();

      if (!bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(
            std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }

      // REMOVE FROM TMPFILESET
      bm->getSM().deleteFromPersistedTmpFileSet(tk);

      // ADD TO PERSISTED FILE CATALOG
      tv.num_files = sorter->getNumberOfRuns();
      tv.output_name = simple_output_name;
      tv.full_output_path = output_full_path;
      tv.pins = 0;
      bm->getSM().insertInPersistedFileCatalog(
          std::make_pair(tk, tv));
    } else {
      // SIMPLY EXECUTE
      sorter->sort();
      if (tk.version == deceve::storage::SORT &&
          !bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(
            std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }
    }

    delete sorter;
    //############################ Delete Previous File
    //####################################
    if (!bm->getSM().isInTableCatalog(full_input_name) &&
        !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
      bm->removeFile(full_input_name);
    }

    //    scan<Q12_JOIN1_STRUCT_LINEITEM_ORDER>(output_full_path);
    //    scan<Q12_JOIN1_STRUCT_LINEITEM_ORDER>(output_full_path);
    bm->removeFile(output_full_path);
  }
}
}  // namespace queries
}  // namespace deceve
