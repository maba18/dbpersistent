/*
 * Query2Nodes.hh
 *
 *  Created on: Apr 29, 2015
 *      Author: maba18
 */

#ifndef QUERYPLANS_QUERYNODES_QUERY2NODES_HH_
#define QUERYPLANS_QUERYNODES_QUERY2NODES_HH_

// Include General Functions
#include "../definitions.hh"
#include "../../utils/global.hh"

// Include base class
#include "../NodeTree.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

// Include Algorithm Classes
#include "../../algorithms/replacementsort.hh"
#include "../../algorithms/gracejoin.hh"
#include "../../algorithms/gracejoinConvertRight.hh"
#include "../../algorithms/gracejoinConvertLeft.hh"
#include "../../algorithms/gracejoinConvertBoth.hh"
#include "../../algorithms/nestedloopsjoin.hh"
#include "../../algorithms/hashjoin.hh"

namespace deceve {
namespace queries {

struct comparatorQ24Right {
  comparatorQ24Right() {
    containers = {"STANDARD ANODIZED TIN",  "SMALL BURNISHED NICKEL",
                  "MEDIUM PLATED BRASS",    "LARGE POLISHED STEEL",
                  "ECONOMY BRUSHED COPPER", "PROMO"};

    srand(time(NULL));
    randomIndex = rand() % 6;
  }

  bool operator()(PART item) {
    std::string type = string(item.P_TYPE);
    if (type == containers[randomIndex]) {
      return true;
    }
    return false;
  }

  std::vector<std::string> containers;
  int randomIndex;
};

// Query2Node1(bm, SUPPLIER_PATH,PARTSUPP_PATH);  // Right AUXILIARY (GRACE) -

class Query2Node1 : public NodeTree {
 public:
  Query2Node1(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;

    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::HASHJOIN;
    left_tk.table_name = deceve::storage::SUPPLIER;
    right_tk.table_name = deceve::storage::PARTSUPP;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::PS_SUPPKEY;

    right_tk.projected_field = deceve::storage::PARTSUPP_Q2;

    leftFileDetails(1, sizeof(SUPPLIER), sizeof(SUPPLIER));
    rightFileDetails(1, sizeof(PARTSUPP), sizeof(Q2_PR_PARTSUPP));
    outputFileDetails(0, sizeof(Q2_JOIN1_STRUCT_SUPPLIER_PARTSUPP));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::GraceJoinConvertRight<
      SUPPLIER, PARTSUPP, Q2_PR_PARTSUPP, column_extractor_supplier,
      column_extractor_partsupp, q2_converter_PR_PARTSUPP,
      q2_extractor_PR_PARTSUPP, q2_combinator_supplier_pr_partsupp,
      std::less<int>, deceve::bama::DefaultComparator<SUPPLIER>,
      deceve::bama::DefaultComparator<PARTSUPP> > *joiner;
};

class Query2Node2 : public NodeTree {
 public:
  Query2Node2(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    tk.version = deceve::storage::HASHJOIN;

    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::NATION;
    right_tk.table_name = deceve::storage::OTHER_TABLENAME;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::OTHER_FIELD;

    if (l_name == "nothing") {
      //      left_file_mode = deceve::storage::INTERMEDIATE;
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      //      right_file_mode = deceve::storage::INTERMEDIATE;
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::NestedLoopsJoin<
      NATION, Q2_JOIN1_STRUCT_SUPPLIER_PARTSUPP, column_extractor_nation,
      q2_join1_extractor_result, q2_join2_combinator_nation_result> *joiner;
};

class Query2Node3RegionSelection : public NodeTree {
 public:
  Query2Node3RegionSelection(deceve::storage::BufferManager *bmr,
                             const std::string &input_name,
                             NodeTree *lNode = NULL, NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    // name field
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query2Node3 : public NodeTree {
 public:
  Query2Node3(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    tk.version = deceve::storage::HASHJOIN;

    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::OTHER_TABLENAME;
    right_tk.table_name = deceve::storage::OTHER_TABLENAME;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::OTHER_FIELD;

    if (l_name == "nothing") {
      //      left_file_mode = deceve::storage::INTERMEDIATE;
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      //      right_file_mode = deceve::storage::INTERMEDIATE;
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::HashJoin<REGION, Q2_JOIN2_STRUCT_SUPPLIER_RESULT,
                         column_extractor_region, q2_join2_extractor_result,
                         q2_join3_combinator_region_result> *joiner;
};

class Query2Node4 : public NodeTree {
 public:
  Query2Node4(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;

    left_tk.version = deceve::storage::HASHJOIN;
    left_tk.table_name = deceve::storage::PARTSUPP;
    left_tk.field = deceve::storage::PS_PARTKEY;

    right_tk.version = deceve::storage::OTHER;
    right_tk.table_name = deceve::storage::OTHER_TABLENAME;
    right_tk.field = deceve::storage::OTHER_FIELD;

#ifdef UNIFIED
    left_tk.projected_field = deceve::storage::PARTSUPP_UNIFIED_PARTKEY;
#else
    left_tk.projected_field = deceve::storage::PARTSUPP_Q2;
#endif


    leftFileDetails(1, sizeof(PARTSUPP), sizeof(Q2_PR_PARTSUPP));
    rightFileDetails(0.006, sizeof(PART), sizeof(Q2_PR_PART));
    outputFileDetails(0.006, sizeof(Q2_JOIN4_STRUCT_PARTSUPP_PART));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  //  deceve::bama::GraceJoin<
  //      PARTSUPP, PART, column_extractor_partsupp, column_extractor_part,
  //      q2_join4_combinator_partsupp_part, std::less<int>,
  //      deceve::bama::DefaultComparator<PARTSUPP>, comparatorQ24Right>
  //      *joiner;

  deceve::bama::GraceJoinConvertBoth<
      PARTSUPP, Q2_PR_PARTSUPP_NODE4, PART, Q2_PR_PART, column_extractor_partsupp,
      column_extractor_part, q2_converter_PR_PARTSUPP_NODE4, q2_converter_PR_PART,
      q2_extractor_PR_PARTSUPP_NODE4, q2_extractor_PR_PART,
      q2_combinator_pr_partsupp_node4_pr_part, std::less<int>,
      deceve::bama::DefaultComparator<PARTSUPP>, comparatorQ24Right> *joiner;

  //    deceve::bama::GraceJoinConvertLeft<LINEITEM, PART, Q14_PR_LINEITEM,
  //    column_extractor_lineitem, column_extractor_part,
  //        q14_converter_PR_LINEITEM, q14_extractor_PR_LINEITEM,
  //        q14_combinator_part_pr_lineitem ,std::less<int>,
  //        deceve::bama::DefaultComparator<LINEITEM>,
  //        deceve::bama::DefaultComparator<PART> > *joiner2;
};

class Query2Node5 : public NodeTree {
 public:
  Query2Node5(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    tk.version = deceve::storage::HASHJOIN;
    //    left_tk.table_name = "part";
    //    right_tk.table_name = "lineitem";
    //    left_tk.field = "P_PARTKEY";
    //    right_tk.field = "L_PARTKEY";
    // SUPPLIER_USAGE
    //    left_tk.version = deceve::storage::HASHJOIN;
    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::SUPPLIER;
    right_tk.table_name = deceve::storage::OTHER_TABLENAME;
    left_tk.field = deceve::storage::S_SUPPKEY;
    right_tk.field = deceve::storage::OTHER_FIELD;

    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::GraceJoin<
      SUPPLIER, Q2_JOIN4_STRUCT_PARTSUPP_PART, column_extractor_supplier,
      q2_join4_extractor_result, q2_join5_combinator_supplier_result,
      std::less<int>, deceve::bama::DefaultComparator<SUPPLIER>,
      deceve::bama::DefaultComparator<Q2_JOIN4_STRUCT_PARTSUPP_PART> > *joiner;
};

class Query2Node6 : public NodeTree {
 public:
  Query2Node6(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    tk.version = deceve::storage::HASHJOIN;
    //    left_tk.table_name = "part";
    //    right_tk.table_name = "lineitem";
    //    left_tk.field = "P_PARTKEY";
    //    right_tk.field = "L_PARTKEY";
    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::OTHER_TABLENAME;
    right_tk.table_name = deceve::storage::OTHER_TABLENAME;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::OTHER_FIELD;

    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::HashJoin<Q2_JOIN5_STRUCT_SUPPLIER_RESULT,
                         Q2_JOIN3_STRUCT_NATION_RESULT,
                         q2_join5_extractor_result, q2_join3_extractor_result,
                         q2_join6_combinator_result_result> *joiner;
};
}
} /* namespace deceve */
#endif /* QUERYPLANS_QUERYNODES_QUERY2NODES_HH_ */
