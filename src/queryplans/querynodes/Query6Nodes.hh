/*
 * Query6Nodes.hh
 *
 *  Created on: Mar 12, 2015
 *      Author: maba18
 */

#ifndef QUERY6NODES_HH_
#define QUERY6NODES_HH_

// Include General Functions
#include "../definitions.hh"
#include "../../utils/global.hh"

// Include base class
#include "../NodeTree.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

// Include Algorithm Classes
#include "../../algorithms/replacementsort.hh"

namespace deceve {
namespace queries {

class Query6Node1 : public NodeTree {
 public:
  Query6Node1(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
    tk.table_name = deceve::storage::LINEITEM;
    tk.version = deceve::storage::SELECT;
    tk.field = deceve::storage::OTHER_FIELD;

    outputFileDetails(0, sizeof(LINEITEM));





  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};
}
} /* namespace deceve */

#endif /* QUERY6NODES_HH_ */
