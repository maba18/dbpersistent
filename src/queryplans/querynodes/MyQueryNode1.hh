/*
 * MyQyeryNode1.hh
 *
 *  Created on: Jan 20, 2015
 *      Author: maba18
 */

#ifndef MYQUERYNODE1_HH_
#define MYQUERYNODE1_HH_

// Include General Functions
#include "../definitions.hh"
#include "../../utils/global.hh"

// Include base class
#include "../NodeTree.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

namespace deceve {
namespace queries {


// WriteRandom Pages
class Query0NodeWrite : public NodeTree {
 public:
  Query0NodeWrite(deceve::storage::BufferManager *bmr,
                  const std::string &input_name, NodeTree *lNode = NULL,
                  NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::LINEITEM;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;

    // cost_estimation variables
    outputFileDetails(0, sizeof(LINEITEM));

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);

 private:
  bool firstTime{true};
};

// Query 1 Test Plan
class MyQueryNode1 : public NodeTree {
 public:
  MyQueryNode1(deceve::storage::BufferManager *bmr,
               const std::string &input_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    //    tk.table_name = "lineitem";
    //    tk.version = deceve::storage::SELECT;
    //    tk.field = "L_SHIPDATE";

    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
  }

  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};
}
}

#endif /* MYQYERYNODE1_HH_ */
