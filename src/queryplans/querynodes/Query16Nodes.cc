/*
 * Query16Nodes.cc
 *
 *  Created on: 12 Oct 2015
 *      Author: mike
 */

#include "Query16Nodes.hh"

namespace deceve {
namespace queries {

void Query16Node1::run(deceve::storage::BufferManager* bm,
                       const std::string& left_file,
                       const std::string& right_file,
                       const std::string& simple_output_name) {
  //  std::cout << "Query16Node1" << std::endl;

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_part left_extractor(0);
  column_extractor_partsupp right_extractor(0);
  //  q16_join1_combinator_part_partsupp combinator;

  q16_converter_PR_PART pr_left_converter;
  q16_extractor_PR_PART pr_left_extractor(0);

  q16_converter_PR_PARTSUPP pr_right_converter;
  q16_extractor_PR_PARTSUPP pr_right_extractor(0);

  q16_join1_combinator_pr_part_pr_partsupp pr_final_combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  //  joiner = new deceve::bama::GraceJoin<
  //      PART, PARTSUPP, column_extractor_part, column_extractor_partsupp,
  //      q16_join1_combinator_part_partsupp, std::less<int>,
  //      comparatorQ161Left,
  //      deceve::bama::DefaultComparator<PARTSUPP> >(
  //      bm, left, right, output_full_path, left_extractor, right_extractor,
  //      combinator, partitions, joinMode);

  joiner = new deceve::bama::GraceJoinConvertBoth<
      PART, Q16_PR_PART, PARTSUPP, Q16_PR_PARTSUPP, column_extractor_part,
      column_extractor_partsupp, q16_converter_PR_PART,
      q16_converter_PR_PARTSUPP, q16_extractor_PR_PART,
      q16_extractor_PR_PARTSUPP, q16_join1_combinator_pr_part_pr_partsupp,
      std::less<int>, comparatorQ161Left,
      deceve::bama::DefaultComparator<PARTSUPP>>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      pr_left_converter, pr_right_converter, pr_left_extractor,
      pr_right_extractor, pr_final_combinator, partitions, joinMode,
      deceve::storage::BOTH_PROJECT);
  //  joiner->setStreamFlag(true);

  joiner->setJoinTableKeys(left_tk, right_tk);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }

  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);

  bm->removeFile(output_full_path);

  //  scan<Q9_JOIN1_STRUCT_PART_LINEITEM>(output_full_path);
}
}  // namespace queries
}  // namespace deceve
