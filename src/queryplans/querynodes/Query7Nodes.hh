/*
 * Query7Nodes.hh
 *
 *  Created on: 2 Jun 2015
 *      Author: mike
 */

#ifndef QUERY7NODES_HH_
#define QUERY7NODES_HH_

// Include General Functions
#include "../definitions.hh"
#include "../../utils/global.hh"

// Include base class
#include "../NodeTree.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

// Include Algorithm Classes
#include "../../algorithms/replacementsort.hh"
#include "../../algorithms/gracejoin.hh"
#include "../../algorithms/gracejoinConvertLeft.hh"
#include "../../algorithms/mergesortjoin.hh"
#include "../../algorithms/mergesortjoinConvertLeft.hh"

namespace deceve {
namespace queries {

struct comparatorQ74Left {
  comparatorQ74Left() {
    d1 = date(1995, 01, 01);
    d2 = date(1996, 12, 31);
    counter = 0;
    srand(time(NULL));
  }

  bool operator()(LINEITEM item) {
    date tmpdate(item.L_SHIPDATE);
    //    if(counter % 100000 == 0){
    //      std::cout<<tmpdate<<std::endl;
    //      std::cout<<"d1: "<<d1<<std::endl;
    //      std::cout<<"d2: "<<d2<<std::endl;
    //    }
    //    counter++;
    if (d1 <= tmpdate && tmpdate < d2) {
      //      std::cout<<"true:"<< tmpdate<<std::endl;
      return true;
    }
    return false;
  }

  date d1;
  date d2;
  int counter;
};

class Query7Node1 : public NodeTree {
 public:
  Query7Node1(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }

  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);

 public:
  std::string getNationName() { return generatedNationName; }

 public:
  std::string generatedNationName;
};

class Query7Node2 : public NodeTree {
 public:
  Query7Node2(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;

    //    left_tk.table_name = "nation";
    //    right_tk.table_name = "region";
    //    left_tk.field = "N_REGIONKEY";
    //    right_tk.field = "R_REGIONKEY";
    left_tk.version = deceve::storage::HASHJOIN;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::CUSTOMER;
    right_tk.table_name = deceve::storage::NATION;
    left_tk.field = deceve::storage::C_NATIONKEY;
    right_tk.field = deceve::storage::OTHER_FIELD;

    left_tk.projected_field = deceve::storage::CUSTOMER_Q7;

    leftFileDetails(1, sizeof(CUSTOMER), sizeof(Q7_PR_CUSTOMER));
    rightFileDetails(1, sizeof(NATION), sizeof(NATION));
    outputFileDetails(0.001, sizeof(Q7_JOIN1_STRUCT_CUSTOMER_NATION));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  //  deceve::bama::GraceJoin<CUSTOMER, NATION, column_extractor_customer,
  //  column_extractor_nation,
  //      q7_join1_combinator_customer_nation> *joiner1;

  deceve::bama::GraceJoinConvertLeft<
      CUSTOMER, NATION, Q7_PR_CUSTOMER, column_extractor_customer,
      column_extractor_nation, q7_converter_PR_CUSTOMER,
      q7_extractor_PR_CUSTOMER, q7_combinator_pr_customer_nation> *joiner;
};

// class Query7Node3 : public NodeTree {
// public:
//  Query7Node3(deceve::storage::BufferManager *bmr, const std::string &l_name,
//              const std::string &r_name, NodeTree *lNode = NULL,
//              NodeTree *rNode = NULL)
//      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
//    tk.version = deceve::storage::HASHJOIN;
//
//    //    left_tk.table_name = "result_left_1";
//    //    right_tk.table_name = "customer";
//    //    left_tk.field = "RESULT_NATIONKEY";
//    //    right_tk.field = "C_NATIONKEY";
//
//    left_tk.version = deceve::storage::HASHJOIN;
//    right_tk.version = deceve::storage::OTHER;
//    left_tk.table_name = deceve::storage::ORDERS;
//    right_tk.table_name = deceve::storage::OTHER_TABLENAME;
//    left_tk.field = deceve::storage::O_CUSTKEY;
//    right_tk.field = deceve::storage::C_CUSTKEY;
//
//    if (l_name == "nothing") {
//      left_file_path = deceve::storage::INTERMEDIATE_PATH;
//    };
//    if (r_name == "nothing") {
//      right_file_path = deceve::storage::INTERMEDIATE_PATH;
//    }
//  }
//
//  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
//           const std::string &right_file,
//           const std::string &simple_output_name);
//
// private:
//  deceve::bama::GraceJoin<ORDER, Q7_JOIN1_STRUCT_CUSTOMER_NATION,
//                          column_extractor_order, q7_join1_extractor_result,
//                          q7_join2_combinator_orders_result> *joiner;
//};

class Query7Node3 : public NodeTree {
 public:
  Query7Node3(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;

    //    left_tk.table_name = "result_left_1";
    //    right_tk.table_name = "customer";
    //    left_tk.field = "RESULT_NATIONKEY";
    //    right_tk.field = "C_NATIONKEY";

    left_tk.version = deceve::storage::SORT;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::ORDERS;
    right_tk.table_name = deceve::storage::OTHER_TABLENAME;
    left_tk.field = deceve::storage::O_CUSTKEY;
    right_tk.field = deceve::storage::C_CUSTKEY;

    left_tk.projected_field = deceve::storage::ORDER_Q7_SORTED;

    leftFileDetails(1, sizeof(ORDER), sizeof(Q7_PR_ORDER_SORT));
    rightFileDetails(1, sizeof(Q7_JOIN1_STRUCT_CUSTOMER_NATION),
                     sizeof(Q7_JOIN1_STRUCT_CUSTOMER_NATION));
    outputFileDetails(0.001, sizeof(Q7_JOIN2_STRUCT_ORDER_RESULT));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  //  deceve::bama::MergeSortJoin<ORDER, Q7_JOIN1_STRUCT_CUSTOMER_NATION,
  //  column_extractor_order, q7_join1_extractor_result,
  //      q7_join2_combinator_orders_result> *joiner;

  deceve::bama::MergeSortJoinConvertLeft<
      ORDER, Q7_PR_ORDER_SORT, Q7_JOIN1_STRUCT_CUSTOMER_NATION,
      column_extractor_order, q7_extractor_PR_ORDER_SORT,
      q7_join1_extractor_result, q7_converter_PR_ORDER_SORT,
      q7_join2_combinator_pr_sort_orders_result> *joiner;

  //  deceve::bama::MergeSortJoinConvertRight<CUSTOMER, ORDER,
  //  Q13_PR_ORDER_SORT, column_extractor_customer,
  //                              column_extractor_order,
  //                              q13_extractor_PR_ORDER_SORT,
  //                              q13_converter_PR_ORDER_SORT,
  //                              q13_combinator_customer_pr_sort_order>
  //                              *joiner;

  //  deceve::bama::GraceJoin<ORDER, Q7_JOIN1_STRUCT_CUSTOMER_NATION,
  //                          column_extractor_order, q7_join1_extractor_result,
  //                          q7_join2_combinator_orders_result> *joiner;
};

class Query7Node4 : public NodeTree {
 public:
  Query7Node4(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query7Node5 : public NodeTree {
 public:
  Query7Node5(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
    tk.version = deceve::storage::HASHJOIN;

    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::LINEITEM;
    right_tk.table_name = deceve::storage::OTHER_TABLENAME;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::OTHER_FIELD;

    leftFileDetails(0.3, sizeof(LINEITEM), sizeof(Q7_PR_LINEITEM));
    rightFileDetails(0, sizeof(Q7_JOIN2_STRUCT_ORDER_RESULT),
                     sizeof(Q7_JOIN2_STRUCT_ORDER_RESULT));
    outputFileDetails(0.0, sizeof(Q7_JOIN3_STRUCT_LINEITEM_RESULT));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  //  deceve::bama::GraceJoin<LINEITEM, Q7_JOIN2_STRUCT_ORDER_RESULT,
  //  column_extractor_lineitem, q7_join2_extractor_result,
  //      q7_join3_combinator_lineitem_result, std::less<int>,
  //      comparatorQ74Left,
  //      deceve::bama::DefaultComparator<Q7_JOIN2_STRUCT_ORDER_RESULT> >
  //      *joiner1;

  deceve::bama::GraceJoinConvertLeft<
      LINEITEM, Q7_JOIN2_STRUCT_ORDER_RESULT, Q7_PR_LINEITEM,
      column_extractor_lineitem, q7_join2_extractor_result,
      q7_converter_PR_LINEITEM, q7_extractor_PR_LINEITEM,
      q7_combinator_pr_lineitem_other, std::less<int>, comparatorQ74Left,
      deceve::bama::DefaultComparator<Q7_JOIN2_STRUCT_ORDER_RESULT> > *joiner;
};

class Query7Node6 : public NodeTree {
 public:
  Query7Node6(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {

    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;

    // SUPPLIER_USAGE
    //    left_tk.version = deceve::storage::HASHJOIN;
    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::SUPPLIER;
    right_tk.table_name = deceve::storage::OTHER_TABLENAME;
    left_tk.field = deceve::storage::S_SUPPKEY;
    right_tk.field = deceve::storage::L_SUPPKEY;


    leftFileDetails(1, sizeof(SUPPLIER), sizeof(SUPPLIER));
     rightFileDetails(0, sizeof(Q7_JOIN3_STRUCT_LINEITEM_RESULT),
                      sizeof(Q7_JOIN3_STRUCT_LINEITEM_RESULT));
     outputFileDetails(0.0, sizeof(Q7_JOIN4_STRUCT_SUPPLIER_RESULT));

  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::GraceJoin<SUPPLIER, Q7_JOIN3_STRUCT_LINEITEM_RESULT,
                          column_extractor_supplier, q7_join3_extractor_result,
                          q7_join4_combinator_supplier_result> *joiner;
};

class Query7Node7 : public NodeTree {
 public:
  Query7Node7(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query7Node8 : public NodeTree {
 public:
  Query7Node8(deceve::storage::BufferManager *bmr,
              const std::string &input_name,
              const std::string &nationNameBanned, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    // name field
    tk.field = deceve::storage::OTHER_FIELD;
    otherNationName = nationNameBanned;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }

  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);

 private:
  std::string otherNationName;
};

class Query7Node9 : public NodeTree {
 public:
  Query7Node9(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    tk.version = deceve::storage::HASHJOIN;

    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::OTHER_TABLENAME;
    right_tk.table_name = deceve::storage::OTHER_TABLENAME;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::OTHER_FIELD;

    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::GraceJoin<NATION, Q7_JOIN4_STRUCT_SUPPLIER_RESULT,
                          column_extractor_nation, q7_join4_extractor_result,
                          q7_join5_combinator_nation1_result> *joiner;
};

class Query7Node10 : public NodeTree {
 public:
  Query7Node10(deceve::storage::BufferManager *bmr,
               const std::string &input_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;
    sorter = NULL;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);

 private:
  deceve::bama::ReplacementSort<Q7_JOIN5_STRUCT_NATION1_RESULT,
                                q7_join5_extractor_result> *sorter;
};
}
} /* namespace deceve */

#endif /* QUERY5NODES_HH_ */
