/*
 * Query17Nodes.cc
 *
 *  Created on: Mar 31, 2015
 *      Author: maba18
 */

#include "Query17Nodes.hh"

namespace deceve {
namespace queries {

void Query17Node00::run(deceve::storage::BufferManager* bm,
                        const std::string& input_name,
                        const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  //  COUT << "******************** -Query1Node1 Execution (Select)- "
  //          "********************"
  //       << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  final_full_output_filename = output_full_path;

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<LINEITEM> reader(bm, full_input_name,
                                        bm->getFileMode(full_input_name));

  deceve::bama::Writer<Q17_PR_LINEITEM> res_writer(bm, output_full_path,
                                                   deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  LINEITEM item;
  //################################ OPERATION1
  //#####################################
  Q17_PR_LINEITEM projectedLineitem;

  while (reader.hasNext()) {
    item = reader.nextRecord();
    projectedLineitem.L_PARTKEY = item.L_PARTKEY;
    projectedLineitem.L_EXTENDEDPRICE = item.L_EXTENDEDPRICE;
    projectedLineitem.L_QUANTITY = item.L_QUANTITY;
    res_writer.write(projectedLineitem);
  }

  reader.close();
  res_writer.close();

}

void Query17Node1::run(deceve::storage::BufferManager* bm,
                       const std::string& left_file,
                       const std::string& right_file,
                       const std::string& simple_output_name) {
  //  COUT << "Query5Node2 Execution (Join)" << "\n";
  //#################################### Construct full paths for left and
  // right
  // file ######################################
  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);


  column_extractor_part left_extractor(0);
  column_extractor_lineitem right_extractor(0);
  //  q9_combinator_part_line combinator;

  q17_converter_PR_PART pr_left_converter;
  q17_extractor_PR_PART pr_left_extractor(0);

  q17_converter_PR_LINEITEM pr_right_converter;
  q17_extractor_PR_LINEITEM pr_right_extractor(0);

  q17_combinator_pr_part_pr_lineitem pr_final_combinator;
  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }



  //  joiner = new deceve::bama::GraceJoinConvertRight<
  //      PART, LINEITEM, Q17_PR_LINEITEM, column_extractor_part,
  //      column_extractor_lineitem, q17_converter_PR_LINEITEM,
  //      q17_extractor_PR_LINEITEM, q17_combinator_part_pr_lineitem,
  //      std::less<int>, deceve::bama::DefaultComparator<PART>,
  //      deceve::bama::DefaultComparator<LINEITEM>>(
  //      bm, left, right, output_full_path, left_extractor, right_extractor,
  //      pr_right_converter, pr_right_extractor, pr_combinator, partitions,
  //      joinMode, deceve::storage::RIGHT_PROJECT);

  joiner = new deceve::bama::GraceJoinConvertBoth<
      PART, Q17_PR_PART, LINEITEM, Q17_PR_LINEITEM, column_extractor_part,
      column_extractor_lineitem, q17_converter_PR_PART,
      q17_converter_PR_LINEITEM, q17_extractor_PR_PART,
      q17_extractor_PR_LINEITEM, q17_combinator_pr_part_pr_lineitem,
      std::less<int>, deceve::bama::DefaultComparator<PART>,
      deceve::bama::DefaultComparator<LINEITEM>>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      pr_left_converter, pr_right_converter, pr_left_extractor,
      pr_right_extractor, pr_final_combinator, partitions, joinMode,
      deceve::storage::BOTH_PROJECT);

  joiner->setStreamFlag(true);
  // test
  joiner->setJoinTableKeys(left_tk, right_tk);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }

  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);

  bm->removeFile(output_full_path);
  //  scan<Q9_JOIN1_STRUCT_PART_LINEITEM>(output_full_path);
}

void Query17Node2::run(deceve::storage::BufferManager* bm,
                       const std::string& left_file,
                       const std::string& right_file,
                       const std::string& simple_output_name) {
  //  COUT << "Query5Node2 Execution (Join)" << "\n";
  //#################################### Construct full paths for left and
  // right
  // file ######################################
  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);


  column_extractor_part left_extractor(0);
  q17_extractor_PR_LINEITEM right_extractor(0);
  q17_combinator_part_pr_lineitem combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  joiner = new deceve::bama::GraceJoin<
      PART, Q17_PR_LINEITEM, column_extractor_part, q17_extractor_PR_LINEITEM,
      q17_combinator_part_pr_lineitem, std::less<int>,
      deceve::bama::DefaultComparator<PART>,
      deceve::bama::DefaultComparator<Q17_PR_LINEITEM>>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      combinator, partitions, joinMode);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }

  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {

      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);

  bm->removeFile(output_full_path);
  //  scan<Q9_JOIN1_STRUCT_PART_LINEITEM>(output_full_path);
}

void Query17Node1MapReduce::run(deceve::storage::BufferManager* bm,
                                const std::string& input_name,
                                const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  //  COUT << "Query17Node1 Execution (Select)"
  //       << "\n";
  //####################### Construct Filenames
  //###################################
  //####################### Selection Template
  //####################################

  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_mode == deceve::storage::PRIMARY) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  // deceve::bama::Reader<LINEITEM> reader(bm, full_input_name,
  // bm->getStorageManager().getFileModeFromCatalog(full_input_name));

  //################################ OPERATION
  //########################################

  //###################### Map Phase ##########################
  unsigned int numberOfNodePartitions = 10;

  // ----------left file variables-------
  transformer_lineitem transformer_left;
  column_extractor_lineitem extractor_left(0);

  // ----------right file variables-------
  transformer_part transformer_right;

  column_extractor_part extractor_right(0);

  // Silent some variables that I may use in the future
  (void)transformer_right;
  (void)numberOfNodePartitions;

  // ----------output file variables-------
  extractor_TRANSFORMED_LINEITEM_STRUCT extractor_ouput(0);

  //-------------- Partition Files ---------------------------
  deceve::bama::PartitionMapReduce<
      LINEITEM, TRANSFORMED_LINEITEM_STRUCT, transformer_lineitem,
      column_extractor_lineitem, extractor_TRANSFORMED_LINEITEM_STRUCT>
      lpartitioner(bm, full_input_name, output_full_path, transformer_left,
                   extractor_left, extractor_ouput, numberOfNodePartitions,
                   deceve::storage::PRIMARY);

  lpartitioner.partition();

  // deceve::bama::GraceJoinMapReduce<LINEITEM, PART,
  // TRANSFORMED_LINEITEM_STRUCT, transformer_lineitem, transformer_part ,
  // column_extractor_lineitem, column_extractor_part,
  // extractor_TRANSFORMED_LINEITEM_STRUCT> joiner(bm,)

  //##################### Reduce Phase ########################

  //  for (unsigned int i = 0; i < numberOfNodePartitions; i++) {
  //    std::stringstream s;
  //    s << output_full_path << "." << i << ".sorted";
  //    COUT << "Sorted File: " << s.str() << "\n";
  //
  //  deceve::bama::Reader<LINEITEM>* reader = new
  //  deceve::bama::Reader<LINEITEM>(
  //      bm, s.str(), bm->getStorageManager().getFileModeFromCatalog(s.str()));
  //
  //    deceve::bama::Writer<int>* writer = new deceve::bama::Writer<int>(
  //        bm, s.str() + ".output", deceve::storage::PRIMARY);
  //
  //    int previousKey = 0;
  //    double value = 0;
  //    LINEITEM item;
  //    while (reader->hasNext()) {
  //      item = reader->nextRecord();
  //      COUT << "Key: " << item.L_PARTKEY << " Total value: " <<
  //      item.L_DISCOUNT
  //           << "\n";
  //      if (item.L_PARTKEY == previousKey) {
  //        value += item.L_QUANTITY;
  //        COUT << "Sum to value: " << value << "\n";
  //      } else {
  //        if (previousKey != 0) {
  //          writer->write(value);
  //          COUT << "Final Key: " << item.L_PARTKEY << "
  //              Total value : "
  //               << value << "\n";
  //        }
  //        previousKey = item.L_PARTKEY;
  //        value = 0;
  //      }
  //    }
  //
  //    reader->close();
  //    writer->close();
  //    delete reader;
  //    delete writer;
  //  }
}

void Query17Node2MapReduce::run(deceve::storage::BufferManager* bm,
                                const std::string& left_file,
                                const std::string& right_file,
                                const std::string& simple_output_name) {
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  if (left_file_mode == deceve::storage::INTERMEDIATE) {
    left = constructInputTmpFullPath(left_file);
  } else {
    left = constructFullPath(left_file);
  }

  std::string right;
  if (right_file_mode == deceve::storage::INTERMEDIATE) {
    right = constructInputTmpFullPath(right_file);
  } else {
    right = constructFullPath(right_file);
  }

  //  COUT << "Left file: " << left << "\n";
  //  COUT << "File size: "
  //       << bm->getFileSizeFromCatalog(left) / deceve::storage::PAGE_SIZE_PERSISTENT <<
  //       "\n";
  //  COUT << "Right file: " << right << "\n";

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  size_t partitions =
      (size_t)(1.2 * (bm->getFileSizeFromCatalog(left) /
                      ((POOLSIZE - 2) * deceve::storage::PAGE_SIZE_PERSISTENT)));

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  //  unsigned int numberOfNodePartitions = 10;

  // ----------left file variables-------
  transformer_lineitem transformer_left;
  column_extractor_lineitem extractor_left(0);

  // ----------right file variables-------
  transformer_part transformer_right;
  column_extractor_part extractor_right(0);

  // ----------output file variables-------
  extractor_TRANSFORMED_LINEITEM_STRUCT extractor_ouput(0);

  my_combinator_part_lineitem combinator;
  my_combinator_transformed transformedCombinator;

  (void)combinator;

  //-------------- Partition Files ---------------------------
  // deceve::bama::PartitionMapReduce<LINEITEM, TRANSFORMED_LINEITEM_STRUCT,
  // transformer_lineitem,
  // column_extractor_lineitem,
  // extractor_TRANSFORMED_LINEITEM_STRUCT>
  // lpartitioner(
  // bm, full_input_name, output_full_path, transformer_left,
  // extractor_left, extractor_ouput,
  // numberOfNodePartitions, deceve::storage::PRIMARY);

  // lpartitioner.partition();

  // joiner = new deceve::bama::GraceJoinMapReduce<PART, LINEITEM,
  // column_extractor_part, column_extractor_lineitem,
  // my_combinator_part_lineitem>(bm, left, right, output_full_path,
  // left_extractor, right_extractor, combinator, partitions,
  // joinMode);

  joiner = new deceve::bama::GraceJoinMapReduce<
      LINEITEM, PART, TRANSFORMED_LINEITEM_STRUCT, transformer_lineitem,
      transformer_part, column_extractor_lineitem, column_extractor_part,
      extractor_TRANSFORMED_LINEITEM_STRUCT, my_combinator_transformed>(
      bm, left, right, output_full_path, transformer_left, transformer_right,
      extractor_left, extractor_right, extractor_ouput, transformedCombinator,
      partitions, joinMode);

  //#################################### Decide to persist or use persisted
  // results ##################################

  //  COUT << "check_PersistResult_flag at node operator: "
  //       << check_PersistResult_flag << "\n";

  if (check_PersistResult_flag) {
    // joiner->persistResult(result_id);

    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      //      COUT << "JOINNNNNNNNNN: " << information_about_left_persisted_file
      //           << "\n";
      //      COUT << "JOINNNNNNNNNN: " <<
      //      information_about_right_persisted_file
      //           << "\n";
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
    } else if (result_id == 1) {
      COUT << "JOINNNNNNNNNN left: " << information_about_left_persisted_file
           << "\n";
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
    } else if (result_id == 2) {
      COUT << "JOINNNNNNNNNN right: " << information_about_right_persisted_file
           << "\n";
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  if (check_PersistResult_flag) {
    bm->getSM().deleteFromPersistedTmpFileSet(tk);

    COUT << "################### RESULT ID IS: ###################"
         << joiner->getResultId() << "\n";

    int tmp_result_id = joiner->getResultId();
    if (tmp_result_id == 0) {
      left_tv.num_files = joiner->getNumberOfPartitions();
      right_tv.num_files = joiner->getNumberOfPartitions();

      left_tv.output_name = left;
      right_tv.output_name = right;

      left_tv.full_output_path = joiner->getRunNamePrefixOfPersistentFile();
      right_tv.full_output_path = joiner->getRunNamePrefixOfPersistentFile();

      COUT << "joiner->getNumberOfPartitions(): "
           << joiner->getNumberOfPartitions() << "\n";
      COUT << "PERSIST: JOINNNNNNNNNN: " << left_tv << "\n";
      COUT << "PERSIST: JOINNNNNNNNNN: " << right_tv << "\n";

      bm->getSM().insertInPersistedFileCatalog(
          std::make_pair(left_tk, left_tv));
      bm->getSM().insertInPersistedFileCatalog(
          std::make_pair(right_tk, right_tv));

      if (!bm->getSM().isContainedInPersistentStatistics(left_tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(left_tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();

        bm->getSM().insertToPersistentStatistics(
            std::make_pair(left_tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(
            left_tk);
      }

      if (!bm->getSM().isContainedInPersistentStatistics(
              right_tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(right_tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();

        bm->getSM().insertToPersistentStatistics(
            std::make_pair(right_tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(
            right_tk);
      }

      bm->getSM().deleteFromPersistedTmpFileSet(left_tk);
      bm->getSM().deleteFromPersistedTmpFileSet(right_tk);
    } else if (tmp_result_id == 1) {
      left_tv.num_files = joiner->getNumberOfPartitions();
      left_tv.output_name = left;
      left_tv.full_output_path = joiner->getRunNamePrefixOfPersistentFile();

      bm->getSM().insertInPersistedFileCatalog(
          std::make_pair(left_tk, left_tv));

      if (!bm->getSM().isContainedInPersistentStatistics(left_tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(left_tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();

        bm->getSM().insertToPersistentStatistics(
            std::make_pair(left_tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(
            left_tk);
      }

      bm->getSM().deleteFromPersistedTmpFileSet(left_tk);

    } else if (tmp_result_id == 2) {
      right_tv.num_files = joiner->getNumberOfPartitions();
      right_tv.output_name = right;
      right_tv.full_output_path = joiner->getRunNamePrefixOfPersistentFile();

      bm->getSM().insertInPersistedFileCatalog(
          std::make_pair(right_tk, right_tv));

      if (!bm->getSM().isContainedInPersistentStatistics(
              right_tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(right_tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();

        bm->getSM().insertToPersistentStatistics(
            std::make_pair(right_tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(
            right_tk);
      }
      bm->getSM().deleteFromPersistedTmpFileSet(right_tk);
    }
  }
  delete joiner;

  // checkResult<query14_struct>(output_full_path);
}

void Query17Node3MapReduce::run(deceve::storage::BufferManager* bm,
                                const std::string& input_name,
                                const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  COUT << "Query17Node3 Execution (TRANSFORM)"
       << "\n";
  //####################### Construct Filenames
  //###################################
  //####################### Selection Template
  //####################################

  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_mode == deceve::storage::PRIMARY) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  //###################### Map Phase ##########################
  column_extractor_lineitem extractor(0);

  transformer_lineitem transformer;

  deceve::bama::Transformer<LINEITEM, TRANSFORMED_LINEITEM_STRUCT,
                            transformer_lineitem> executer(bm, full_input_name,
                                                           output_full_path,
                                                           transformer);
  //-------------- Partition Files ---------------------------

  executer.transform();

  scan<TRANSFORMED_LINEITEM_STRUCT>(output_full_path);

  //##################### Reduce Phase ########################
}
}  // namespace queries
}  // namespace deceve
