/*
 * Query14Nodes.hh
 *
 *  Created on: Mar 12, 2015
 *      Author: maba18
 */

#ifndef QUERY14NODES_HH_
#define QUERY14NODES_HH_

// Include General Functions
#include "../definitions.hh"
#include "../../utils/global.hh"

// Include base class
#include "../NodeTree.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

// Include Algorithm Classes
#include "../../algorithms/replacementsort.hh"
#include "../../algorithms/gracejoin.hh"
#include "../../algorithms/gracejoinConvertLeft.hh"
#include "../../algorithms/gracejoinConvertRight.hh"
#include "../../algorithms/gracejoinConvertBoth.hh"
#include "../../algorithms/mergesortjoin.hh"

namespace deceve {
namespace queries {

struct comparatorQ142Left {
  comparatorQ142Left() {
    srand(time(NULL));

    int randomYear = 1993 + rand() % 5;
    // randomYear = 1995;

    int randomMonth = 1 + rand() % 12;
    // randomMonth = 9;

    d1 = {randomYear, randomMonth, 1};
    d2 = d1.plus(1);
    //    d2 = {randomYear, randomMonth + 1, 1};
  }

  bool operator()(LINEITEM item) {
    date l_shipdate(item.L_SHIPDATE);
    if (l_shipdate >= d1 && l_shipdate < d2) {
      return true;
    }
    return false;
  }

  date d1;
  date d2;
};

class Query14Node1 : public NodeTree {
 public:
  Query14Node1(deceve::storage::BufferManager *bmr,
               const std::string &input_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    //		tk.table_name = "lineitem";
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    //		tk.field = "L_SHIPDATE";
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_mode = deceve::storage::INTERMEDIATE;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

// class Query14Node2 : public NodeTree {
// public:
//  Query14Node2(deceve::storage::BufferManager *bmr, const std::string &l_name,
//               const std::string &r_name, NodeTree *lNode = NULL,
//               NodeTree *rNode = NULL)
//      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
//    tk.version = deceve::storage::HASHJOIN;
//    //		left_tk.table_name = "lineitem";
//    //		right_tk.table_name = "part";
//    left_tk.table_name = deceve::storage::LINEITEM;
//    right_tk.table_name = deceve::storage::PART;
//    left_tk.version = deceve::storage::OTHER;
//    right_tk.version = deceve::storage::HASHJOIN;
//    //		left_tk.field = "L_PARTKEY";
//    //		right_tk.field = "P_PARTKEY";
//    left_tk.field = deceve::storage::L_PARTKEY;
//    right_tk.field = deceve::storage::P_PARTKEY;
//
//    if (l_name == "nothing") {
//      left_file_path = deceve::storage::INTERMEDIATE_PATH;
//    };
//    if (r_name == "nothing") {
//      right_file_path = deceve::storage::INTERMEDIATE_PATH;
//    }
//  }
//
//  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
//           const std::string &right_file,
//           const std::string &simple_output_name);
//
// private:
//  deceve::bama::GraceJoin<LINEITEM, PART, column_extractor_lineitem,
//                          column_extractor_part, q14_combinator_part_line,
//                          std::less<int>, comparatorQ142Left,
//                          deceve::bama::DefaultComparator<PART> > *joiner;
//};

class Query14Node2 : public NodeTree {
 public:
  Query14Node2(deceve::storage::BufferManager *bmr, const std::string &l_name,
               const std::string &r_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;
    //    left_tk.table_name = "part";
    //    right_tk.table_name = "lineitem";
    //    left_tk.field = "P_PARTKEY";
    //    right_tk.field = "L_PARTKEY";
    left_tk.version = deceve::storage::HASHJOIN;
    left_tk.table_name = deceve::storage::LINEITEM;
    left_tk.field = deceve::storage::L_PARTKEY;


#ifdef UNIFIED
    left_tk.projected_field = deceve::storage::LINEITEM_UNIFIED;
#else
    left_tk.projected_field = deceve::storage::LINEITEM_Q14;
#endif

    right_tk.version = deceve::storage::OTHER;
    right_tk.table_name = deceve::storage::PART;
    right_tk.field = deceve::storage::OTHER_FIELD;

    //    leftWriteFactor = 1;
    //    leftTupleSize = sizeof(LINEITEM);
    //    leftTupleSizeProjected = sizeof(Q14_PR_LINEITEM);
    //
    //
    //    rightWriteFactor = 1;
    //    rightTupleSize = sizeof(PART);
    //    rightTupleSizeProjected = sizeof(Q14_PR_PART);
    //
    //    outputWriteFactor = 0;
    //    outputTupleSize = sizeof(query14_struct);

    leftFileDetails(1, sizeof(LINEITEM), sizeof(Q14_PR_LINEITEM));
    rightFileDetails(1, sizeof(PART), sizeof(Q14_PR_PART));
    outputFileDetails(0, sizeof(query14_struct));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  //  deceve::bama::GraceJoin<LINEITEM, PART, column_extractor_lineitem,
  //                          column_extractor_part,
  //                          q14_combinator_part_line> *joiner;

  deceve::bama::GraceJoinConvertBoth<
      LINEITEM, Q14_PR_LINEITEM, PART, Q14_PR_PART, column_extractor_lineitem,
      column_extractor_part, q14_converter_PR_LINEITEM, q14_converter_PR_PART,
      q14_extractor_PR_LINEITEM, q14_extractor_PR_PART,
      q14_combinator_pr_lineitem_pr_part, std::less<int>,
      deceve::bama::DefaultComparator<LINEITEM>,
      deceve::bama::DefaultComparator<PART> > *joiner;

  //  deceve::bama::GraceJoinConvert<PART, LINEITEM, Q9_PR_LINEITEM,
  //  column_extractor_part, column_extractor_lineitem,
  //      q9_converter_PR_LINEITEM, q9_extractor_PR_LINEITEM,
  //      q9_combinator_part_pr_lineitem, std::less<int>,
  //      comparatorQ91Left, deceve::bama::DefaultComparator<LINEITEM> >
  //      *joiner;
};

class Query14Node3 : public NodeTree {
 public:
  Query14Node3(deceve::storage::BufferManager *bmr,
               const std::string &input_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    //		tk.table_name = "join_select";
    tk.table_name = deceve::storage::OTHER_TABLENAME;

    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};
}
} /* namespace deceve */

#endif /* QUERY14NODES_HH_ */
