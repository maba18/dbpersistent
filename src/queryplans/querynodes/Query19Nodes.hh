/*
 * Query18Nodes.hh
 *
 *  Created on: 14 Oct 2015
 *      Author: mike
 */

#ifndef QUERYPLANS_QUERYNODES_QUERY19NODES_HH_
#define QUERYPLANS_QUERYNODES_QUERY19NODES_HH_

// Include General Functions
#include "../definitions.hh"
#include "../../utils/global.hh"

#include <sstream>

// Include base class
#include "../NodeTree.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

// Include Algorithm Classes
#include "../../algorithms/replacementsort.hh"
#include "../../algorithms/gracejoin.hh"
#include "../../algorithms/gracejoinConvertBoth.hh"

#include "../../storage/identity.hh"

#include "../../algorithms/transformer.hh"

namespace deceve {
namespace queries {

struct comparatorQ191Left {
  comparatorQ191Left() {
    containers1 = {"SM", "LG", "MED", "JUMBO", "WRAP"};
    containers2 = {"CASE", "BOX", "BAG", "JAR", "PKG", "PACK", "CAN", "DRUM"};

    //    p_container in ( ‘SM CASE’, ‘SM BOX’, ‘SM PACK’, ‘SM PKG’)

    srand(time(NULL));
    int N = 1 + rand() % 5;
    int M = 1 + rand() % 5;
    std::stringstream s;
    s << "BRAND#" << N << M;
    brand = s.str();
    randomIndex = rand() % containers1.size();

    std::stringstream c;
    c << containers1[randomIndex] << " ";
    randomIndex = rand() % containers2.size();
    c << containers2[randomIndex];
    container = c.str();
  }

  bool operator()(PART item) {
    std::string containerTuple = string(item.P_CONTAINER);
    std::string brandnameTuple = string(item.P_BRAND);

    //    std::cout<<containerTuple<<" vs "<<container<<std::endl;
    //    std::cout<<brandnameTuple<<" vs "<<brand<<std::endl;

    if (containerTuple == container || brandnameTuple == brand) {
      return true;
    }
    return false;
  }

  std::vector<std::string> containers1;
  std::vector<std::string> containers2;
  std::string brand;
  std::string container;
  int randomIndex;
};

struct comparatorQ191Right {
  comparatorQ191Right() {
    //    p_container in ( ‘SM CASE’, ‘SM BOX’, ‘SM PACK’, ‘SM PKG’)
    quantity = 1;
    srand(time(NULL));
    quantity1 = 1 + rand() % 10;
    quantity2 = 11 + rand() % 10;
    quantity3 = 21 + rand() % 10;
    randomIndex = rand() % 3;
  }

  bool operator()(LINEITEM item) {
    if (randomIndex == 0) {
      quantity = quantity1;
    } else if (randomIndex == 1) {
      quantity = quantity2;
    } else {
      quantity = quantity3;
    }
    if (item.L_QUANTITY >= quantity && item.L_QUANTITY <= quantity + 10) {
      //      std::cout<<"True lineitem"<<std::endl;
      return true;
    }
    return false;
  }

  int quantity;
  int quantity1;
  int quantity2;
  int quantity3;
  int randomIndex;
};

// node1 = new Query19Node1(bm, PART_PATH, LINE_ITEM_PATH);
class Query19Node1 : public NodeTree {
 public:
  Query19Node1(deceve::storage::BufferManager *bmr, const std::string &l_name,
               const std::string &r_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;

    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::PART;
    right_tk.table_name = deceve::storage::LINEITEM;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::OTHER_FIELD;

    leftFileDetails(0.024, sizeof(PART), sizeof(Q19_PR_PART));
    rightFileDetails(0.22, sizeof(LINEITEM), sizeof(Q19_PR_LINEITEM));
    outputFileDetails(0, sizeof(query19_struct));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  //  deceve::bama::GraceJoin<
  //      PART, LINEITEM, column_extractor_part, column_extractor_lineitem,
  //      my_combinator_part_lineitem, std::less<int>, comparatorQ191Left,
  //      deceve::bama::DefaultComparator<LINEITEM> > *joiner;

  deceve::bama::GraceJoinConvertBoth<
      PART, Q19_PR_PART, LINEITEM, Q19_PR_LINEITEM, column_extractor_part,
      column_extractor_lineitem, q19_converter_PR_PART,
      q19_converter_PR_LINEITEM, q19_extractor_PR_PART,
      q19_extractor_PR_LINEITEM, q19_combinator_pr_part_pr_lineitem,
      std::less<int>, comparatorQ191Left, comparatorQ191Right> *joiner;
};
}
} /* namespace deceve */

#endif /* QUERYPLANS_QUERYNODES_QUERY19NODES_HH_ */
