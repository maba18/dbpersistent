/*
 * Query9Nodes.cc
 *
 *  Created on: Mar 17, 2015
 *      Author: maba18
 */

#include "Query9Nodes.hh"

#include <string>
#include <set>

namespace deceve {
namespace queries {

void Query9Node0::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  // std::cout << "Query9Node0 Execution (Select Part on name)" << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<PART> reader(bm, full_input_name,
                                    bm->getFileMode(full_input_name));

  deceve::bama::Writer<PART> res_writer(bm, output_full_path,
                                        deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  std::string types[] = {
      "almond",    "antique",   "aquamarine", "azure",      "beige",
      "bisque",    "black",     "blanched",   "blue",       "blush",
      "brown",     "burlywood", "burnished",  "chartreuse", "chiffon",
      "chocolate", "coral",     "cornflower", "cornsilk",   "cream",
      "cyan",      "dark",      "deep",       "dim",        "dodger",
      "drab",      "firebrick", "floral",     "forest",     "frosted",
      "gainsboro", "ghost",     "goldenrod",  "green",      "grey",
      "honeydew",  "hot",       "indian",     "ivory",      "khaki",
      "lace",      "lavender",  "lawn",       "lemon",      "light",
      "lime",      "linen",     "magenta",    "maroon",     "medium",
      "metallic",  "midnight",  "mint",       "misty",      "moccasin",
      "navajo",    "navy",      "olive",      "orange",     "orchid",
      "pale",      "papaya",    "peach",      "peru",       "pink",
      "plum",      "powder",    "puff",       "purple",     "red",
      "rose",      "rosy",      "royal",      "saddle",     "salmon",
      "sandy",     "seashell",  "sienna",     "sky",        "slate",
      "smoke",     "snow",      "spring",     "steel",      "tan",
      "thistle",   "tomato",    "turquoise",  "violet",     "wheat",
      "white",     "yellow"};

  PART item;
  int randomIndex = rand() % 92;
  int counter = 0;
  while (reader.hasNext()) {
    item = reader.nextRecord();
    std::string P_NAME = string(item.P_NAME);
    std::size_t found;
    found = P_NAME.find(types[randomIndex]);
    if (found != std::string::npos) {
      res_writer.write(item);
    }
    counter++;
  }
  reader.close();
  res_writer.close();

  //############################ Delete Previous File
  //####################################
  //	check if the input is a primary table and not an intermediate results
  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }
}

void Query9Node1::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  // std::cout << "Query9Node1 Execution (Join <PART vs LINEITEM>)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_part left_extractor(0);
  column_extractor_lineitem right_extractor(0);
  //  q9_combinator_part_line combinator;
  //  q9_converter_PR_LINEITEM pr_converter;
  //  q9_extractor_PR_LINEITEM pr_extractor(0);

  q9_converter_PR_PART pr_left_converter;
  q9_extractor_PR_PART pr_left_extractor(0);

  q9_converter_PR_LINEITEM pr_right_converter;
  q9_extractor_PR_LINEITEM pr_right_extractor(0);

  q9_combinator_pr_part_pr_lineitem pr_final_combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  //  joiner = new deceve::bama::GraceJoin<PART, LINEITEM,
  //  column_extractor_part, column_extractor_lineitem,
  //      q9_combinator_part_line, std::less<int>, comparatorQ91Left,
  //      deceve::bama::DefaultComparator<LINEITEM> >(
  //      bm, left, right, output_full_path, left_extractor, right_extractor,
  //      combinator, partitions, joinMode);

  //  joiner = new deceve::bama::GraceJoinConvert<PART, LINEITEM,
  //  Q9_PR_LINEITEM, column_extractor_part,
  //      column_extractor_lineitem, q9_converter_PR_LINEITEM,
  //      q9_extractor_PR_LINEITEM, q9_combinator_part_pr_lineitem,
  //      std::less<int>, comparatorQ91Left,
  //      deceve::bama::DefaultComparator<LINEITEM> >(bm, left, right,
  //      output_full_path,
  //                                                                                     left_extractor, right_extractor,
  //                                                                                     pr_converter, pr_extractor,
  //                                                                                     pr_combinator, partitions,
  //                                                                                     joinMode,
  //                                                                                     deceve::storage::RIGHT_PROJECT);

  joiner = new deceve::bama::GraceJoinConvertBoth<
      PART, Q9_PR_PART, LINEITEM, Q9_PR_LINEITEM, column_extractor_part,
      column_extractor_lineitem, q9_converter_PR_PART, q9_converter_PR_LINEITEM,
      q9_extractor_PR_PART, q9_extractor_PR_LINEITEM,
      q9_combinator_pr_part_pr_lineitem, std::less<int>, comparatorQ91Left,
      deceve::bama::DefaultComparator<LINEITEM> >(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      pr_left_converter, pr_right_converter, pr_left_extractor,
      pr_right_extractor, pr_final_combinator, partitions, joinMode,
      deceve::storage::BOTH_PROJECT);

  joiner->setJoinTableKeys(left_tk, right_tk);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
}

void Query9Node2::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  // std::cout << "Query9Node2 Execution (Join <RESULT vs SUPPLIER>)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  q9_join2_extractor_result left_extractor(0);
  column_extractor_supplier right_extractor(0);
  q9_combinator_result_supplier combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  joiner = new deceve::bama::GraceJoin<
      Q9_JOIN1_STRUCT_PART_LINEITEM, SUPPLIER, q9_join2_extractor_result,
      column_extractor_supplier, q9_combinator_result_supplier>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      combinator, partitions, joinMode);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
}

void Query9Node3::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  // std::cout << "Query9Node3 Execution (Join <RESULT vs NATION>)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  q9_join3_extractor_result left_extractor(0);
  column_extractor_nation right_extractor(1);
  q9_combinator_result_nation combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  joiner = new deceve::bama::GraceJoin<
      Q9_JOIN2_STRUCT_RESULT_SUPPLIER, NATION, q9_join3_extractor_result,
      column_extractor_nation, q9_combinator_result_nation>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      combinator, partitions, joinMode);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
}

void Query9Node4::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Sorting Template
  //####################################
  //
  //  std::cout
  //      << "******************** -Query9Node4 Execution (Sort)-
  //      ********************"
  //      << "\n";
  std::string output_full_path;
  /*
   * EXECUTE USING PERSISTED FILES
   */
  if (check_usePersistedResult_flag) {
    node_output_name = information_about_persisted_files.output_name;
    bm->getSM().incrementPersistentStatisticsHits(tk);
    // Inform next operator to unpin file on the persisted tmp catalog

  } else {
    //####################### Construct Filenames
    //####################################
    std::string full_input_name;

    if (input_file_path == deceve::storage::PRIMARY_PATH) {
      full_input_name = constructFullPath(input_name);
    } else {
      full_input_name = constructInputTmpFullPath(node_input_name);
    }

    output_full_path = constructOutputTmpFullPath(simple_output_name);

    //################################ OPERATION
    //#####################################
    q9_sort4_extractor_result extractor(0);

    deceve::storage::FileMode sorterMode = deceve::storage::PRIMARY;
    if (check_PersistResult_flag) {
      sorterMode = deceve::storage::INTERMEDIATE;
    }

    sorter = new deceve::bama::ReplacementSort<Q9_JOIN3_STRUCT_RESULT_NATION,
                                               q9_sort4_extractor_result>(
        bm, full_input_name, output_full_path, extractor, POOLSIZE, sorterMode);

    // EXECUTE AND PERSIST RESULTS
    if (check_PersistResult_flag) {
      sorter->persistResult();
      sorter->sort();

      if (!bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }

      // REMOVE FROM TMPFILESET
      bm->getSM().deleteFromPersistedTmpFileSet(tk);

      // ADD TO PERSISTED FILE CATALOG
      tv.num_files = sorter->getNumberOfRuns();
      tv.output_name = simple_output_name;
      tv.full_output_path = output_full_path;
      tv.pins = 0;
      bm->getSM().insertInPersistedFileCatalog(std::make_pair(tk, tv));
    } else {
      sorter->sort();

      if (!bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }
    }

    delete sorter;

    //############################ Delete Previous File
    //####################################
    if (!bm->getSM().isInTableCatalog(full_input_name) &&
        !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
      bm->removeFile(full_input_name);
    }
  }
}

void Query9Node5::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  //  std::cout << "Query9Node5 Execution (Join <RESULT vs ORDER>)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  q9_sort4_extractor_result left_extractor(0);
  column_extractor_order right_extractor(1);
  q9_combinator_result_order combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  joiner = new deceve::bama::MergeSortJoin<
      Q9_JOIN3_STRUCT_RESULT_NATION, ORDER, q9_sort4_extractor_result,
      column_extractor_order, q9_combinator_result_order>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      combinator, partitions, joinMode, deceve::storage::NO_FILE);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################

  deletePreviousFilesJoin(bm, left, right);
}

void Query9Node6::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  //  std::cout << "Query9Node6 Execution (Join <RESULT vs PARTSUPP>)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;

  if (left_file_path == deceve::storage::INTERMEDIATE_PATH) {
    left = constructInputTmpFullPath(leftNode->node_output_name);
  } else {
    left = constructFullPath(left_file);
  }

  std::string right;
  if (right_file_path == deceve::storage::INTERMEDIATE_PATH)
    right = constructInputTmpFullPath(rightNode->node_output_name);
  else {
    right = constructFullPath(right_file);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  q9_join5_extractor_result left_extractor(0);
  column_extractor_partsupp right_extractor(0);
  q9_combinator_result_partsupp combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  joiner = new deceve::bama::MergeSortJoin<
      Q9_JOIN4_STRUCT_RESULT_ORDER, PARTSUPP, q9_join5_extractor_result,
      column_extractor_partsupp, q9_combinator_result_partsupp>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      combinator, 1000, joinMode, deceve::storage::NO_FILE);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);

  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
}

void Query9Node7::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  //  std::cout << "Query9Node7 Execution (gROUP Part)" << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<Q9_JOIN5_STRUCT_RESULT_PARTSUPP> reader(
      bm, full_input_name, bm->getFileMode(full_input_name));

  deceve::bama::Writer<Q9_JOIN5_STRUCT_RESULT_PARTSUPP> res_writer(
      bm, output_full_path, deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  Q9_JOIN5_STRUCT_RESULT_PARTSUPP item;
  /* Generate random date and subtract it from the current date */

  std::map<std::string, int> xartis;

  int counter = 0;
  while (reader.hasNext()) {
    item = reader.nextRecord();

    if (xartis.find(string(item.n_name)) == xartis.end()) {
      xartis.insert(std::make_pair(item.n_name, 1));
      res_writer.write(item);
      counter++;
    }
  }
  reader.close();
  res_writer.close();

  //############################ Delete Previous File
  //####################################
  //	check if the input is a primary table and not an intermediate results
  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }

  bm->removeFile(output_full_path);
}

void Query9Node8::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Sorting Template
  //####################################

  //  std::cout
  //      << "******************** -Query8Node11 Execution (Sort)-
  //      ********************"
  //      << "\n";
  std::string output_full_path;
  /*
   * EXECUTE USING PERSISTED FILES
   */
  if (check_usePersistedResult_flag) {
    node_output_name = information_about_persisted_files.output_name;
    bm->getSM().incrementPersistentStatisticsHits(tk);
    // Inform next operator to unpin file on the persisted tmp catalog
  } else {
    //####################### Construct Filenames
    //####################################
    std::string full_input_name;

    if (input_file_path == deceve::storage::PRIMARY_PATH) {
      full_input_name = constructFullPath(input_name);
    } else {
      full_input_name = constructInputTmpFullPath(node_input_name);
    }

    output_full_path = constructOutputTmpFullPath(simple_output_name);

    //################################ OPERATION
    //#####################################
    q9_node8_extractor_result extractor(0);

    deceve::storage::FileMode sorterMode = deceve::storage::PRIMARY;
    if (check_PersistResult_flag) {
      sorterMode = deceve::storage::INTERMEDIATE;
    }

    sorter = new deceve::bama::ReplacementSort<Q9_JOIN5_STRUCT_RESULT_PARTSUPP,
                                               q9_node8_extractor_result>(
        bm, full_input_name, output_full_path, extractor, POOLSIZE, sorterMode);

    // EXECUTE AND PERSIST RESULTS
    if (check_PersistResult_flag) {
      sorter->persistResult();
      sorter->sort();

      if (!bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }

      // REMOVE FROM TMPFILESET
      bm->getSM().deleteFromPersistedTmpFileSet(tk);

      // ADD TO PERSISTED FILE CATALOG
      tv.num_files = sorter->getNumberOfRuns();
      tv.output_name = simple_output_name;
      tv.full_output_path = output_full_path;
      tv.pins = 0;
      bm->getSM().insertInPersistedFileCatalog(std::make_pair(tk, tv));
    } else {
      sorter->sort();

      if (!bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }
    }

    delete sorter;
    //############################ Delete Previous File
    //####################################
    if (!bm->getSM().isInTableCatalog(full_input_name) &&
        !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
      bm->removeFile(full_input_name);
    }
  }

  // scan<Q9_JOIN5_STRUCT_RESULT_PARTSUPP>(output_full_path);

  bm->removeFile(output_full_path);
}
}
}
