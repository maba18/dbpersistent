/*
 * Query7Nodes.cc
 *
 *  Created on: 2 Jun 2015
 *      Author: mike
 */

#include "Query7Nodes.hh"

namespace deceve {
namespace queries {

void Query7Node1::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################
  // COUT << "$$$$$$$$$$$$$$$--- Query7Node1 Execution (Select Nation2) "
  //          "---$$$$$$$$$$$$$$$"
  //       << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<NATION> reader(bm, full_input_name,
                                      bm->getFileMode(full_input_name));

  deceve::bama::Writer<NATION> res_writer(bm, output_full_path,
                                          deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  NATION item;
  srand(time(NULL));
  std::string nationNames[] = {
      "ALGERIA",    "ARGENTINA", "BRAZIL",        "CANADA",  "EGYPT",
      "ETHIOPIA",   "FRANCE",    "GERMANY",       "INDIA",   "INDONESIA",
      "IRAN",       "JAPAN",     "JORDAN",        "KENYA",   "MOROCCO",
      "MOZAMBIQUE", "PERU",      "CHINA",         "ROMANIA", "SAUDI ARABIA",
      "VIETNAM",    "RUSSIA",    "UNITED KINGDOM"};

  /* Generate random date and subtract it from the current date */
  generatedNationName = nationNames[rand() % 23];

  while (reader.hasNext()) {
    item = reader.nextRecord();
    if (strcmp(item.N_NAME, generatedNationName.c_str()) == 0) {
      res_writer.write(item);
    }
  }
  reader.close();
  res_writer.close();

  //############################ Delete Previous File
  //####################################
  //  check if the input is a primary table and not an intermediate results
  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }
}

void Query7Node2::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  //  COUT << "Query7Node2 Execution (Join)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_customer left_extractor(1);
  column_extractor_nation right_extractor(1);
  //  q7_join1_combinator_customer_nation combinator;

  q7_converter_PR_CUSTOMER pr_converter;
  q7_extractor_PR_CUSTOMER pr_extractor(0);
  q7_combinator_pr_customer_nation pr_combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  //  joiner =
  //      new deceve::bama::GraceJoin<CUSTOMER, NATION,
  //      column_extractor_customer,
  //                                  column_extractor_nation,
  //                                  q7_join1_combinator_customer_nation>(
  //          bm, left, right, output_full_path, left_extractor,
  //          right_extractor,
  //          combinator, partitions, joinMode);

  joiner = new deceve::bama::GraceJoinConvertLeft<
      CUSTOMER, NATION, Q7_PR_CUSTOMER, column_extractor_customer,
      column_extractor_nation, q7_converter_PR_CUSTOMER,
      q7_extractor_PR_CUSTOMER, q7_combinator_pr_customer_nation>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      pr_converter, pr_extractor, pr_combinator, partitions, joinMode,
      deceve::storage::LEFT_PROJECT);

  joiner->setJoinTableKeys(left_tk, right_tk);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);

  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
}

void Query7Node3::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  //  COUT << "Query7Node3 Execution (Join)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_order left_extractor(0);
  q7_join1_extractor_result right_extractor(0);
  // q7_join2_combinator_orders_result combinator;

  q7_converter_PR_ORDER_SORT pr_left_converter;
  q7_extractor_PR_ORDER_SORT pr_left_extractor(0);
  q7_join2_combinator_pr_sort_orders_result pr_combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  //  joiner = new deceve::bama::MergeSortJoin<ORDER,
  //  Q7_JOIN1_STRUCT_CUSTOMER_NATION, column_extractor_order,
  //      q7_join1_extractor_result, q7_join2_combinator_orders_result>(bm,
  //      left, right, output_full_path, left_extractor,
  //                                                                    right_extractor,
  //                                                                    combinator,
  //                                                                    partitions,
  //                                                                    joinMode,
  //                                                                    deceve::storage::LEFT_FILE_ONLY);

  joiner = new deceve::bama::MergeSortJoinConvertLeft<
      ORDER, Q7_PR_ORDER_SORT, Q7_JOIN1_STRUCT_CUSTOMER_NATION,
      column_extractor_order, q7_extractor_PR_ORDER_SORT,
      q7_join1_extractor_result, q7_converter_PR_ORDER_SORT,
      q7_join2_combinator_pr_sort_orders_result>(
      bm, left, right, output_full_path, left_extractor, pr_left_extractor,
      right_extractor, pr_left_converter, pr_combinator, partitions, joinMode,
      deceve::storage::LEFT_FILE_ONLY);

  joiner->setJoinTableKeys(left_tk, right_tk);

  //    joiner = new deceve::bama::GraceJoin<
  //        ORDER, Q7_JOIN1_STRUCT_CUSTOMER_NATION, column_extractor_order,
  //        q7_join1_extractor_result, q7_join2_combinator_orders_result>(
  //        bm, left, right, output_full_path, left_extractor, right_extractor,
  //        combinator, partitions, joinMode);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);

  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
}

void Query7Node4::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################
  //  COUT
  //      << "$$$$$$$$$$$$$$$--- Query7Node4 Execution (Select Lineitem)
  //      ---$$$$$$$$$$$$$$$"
  //      << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<LINEITEM> reader(bm, full_input_name,
                                        bm->getFileMode(full_input_name));

  deceve::bama::Writer<LINEITEM> res_writer(bm, output_full_path,
                                            deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  LINEITEM item;
  date d1(1995, 01, 01);
  date d2(1996, 12, 31);
  srand(time(NULL));

  while (reader.hasNext()) {
    item = reader.nextRecord();
    date tmpdate(item.L_SHIPDATE);
    if (d1 <= tmpdate && tmpdate <= d2) {
      res_writer.write(item);
    }
  }
  reader.close();
  res_writer.close();

  //############################ Delete Previous File
  //####################################
  //  check if the input is a primary table and not an intermediate results
  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }
}

void Query7Node5::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  //  COUT << "Query7Node5 Execution (Join)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_lineitem left_extractor(1);
  q7_join2_extractor_result right_extractor(0);
  // q7_join3_combinator_lineitem_result combinator;

  q7_converter_PR_LINEITEM pr_converter;
  q7_extractor_PR_LINEITEM pr_extractor(0);
  q7_combinator_pr_lineitem_other pr_combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  //  joiner = new deceve::bama::GraceJoin<LINEITEM,
  //  Q7_JOIN2_STRUCT_ORDER_RESULT, column_extractor_lineitem,
  //      q7_join2_extractor_result, q7_join3_combinator_lineitem_result,
  //      std::less<int>, comparatorQ74Left,
  //      deceve::bama::DefaultComparator<Q7_JOIN2_STRUCT_ORDER_RESULT> >(bm,
  //      left, right, output_full_path, left_extractor,
  //                                                                      right_extractor,
  //                                                                      combinator,
  //                                                                      partitions,
  //                                                                      joinMode);

  joiner = new deceve::bama::GraceJoinConvertLeft<
      LINEITEM, Q7_JOIN2_STRUCT_ORDER_RESULT, Q7_PR_LINEITEM,
      column_extractor_lineitem, q7_join2_extractor_result,
      q7_converter_PR_LINEITEM, q7_extractor_PR_LINEITEM,
      q7_combinator_pr_lineitem_other, std::less<int>, comparatorQ74Left,
      deceve::bama::DefaultComparator<Q7_JOIN2_STRUCT_ORDER_RESULT> >(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      pr_converter, pr_extractor, pr_combinator, partitions, joinMode,
      deceve::storage::LEFT_PROJECT);

  joiner->setJoinTableKeys(left_tk, right_tk);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);

  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
}

void Query7Node6::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  // COUT << "Query7Node6 Execution (Join)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_supplier left_extractor(0);
  q7_join3_extractor_result right_extractor(0);
  q7_join4_combinator_supplier_result combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  joiner = new deceve::bama::GraceJoin<
      SUPPLIER, Q7_JOIN3_STRUCT_LINEITEM_RESULT, column_extractor_supplier,
      q7_join3_extractor_result, q7_join4_combinator_supplier_result>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      combinator, partitions, joinMode);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);

  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
}

void Query7Node7::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################
  //  COUT
  //      << "$$$$$$$$$$$$$$$--- Query7Node7 Execution (Hash Grouping)
  //      ---$$$$$$$$$$$$$$$"
  //      << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<Q7_JOIN4_STRUCT_SUPPLIER_RESULT> reader(
      bm, full_input_name, bm->getFileMode(full_input_name));

  deceve::bama::Writer<Q7_JOIN4_STRUCT_SUPPLIER_RESULT> res_writer(
      bm, output_full_path, deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  Q7_JOIN4_STRUCT_SUPPLIER_RESULT item;

  while (reader.hasNext()) {
    item = reader.nextRecord();
    res_writer.write(item);
  }
  reader.close();
  res_writer.close();

  //############################ Delete Previous File
  //####################################
  //  check if the input is a primary table and not an intermediate results
  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }
}

void Query7Node8::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################
  //  COUT
  //      << "$$$$$$$$$$$$$$$--- Query7Node8 Execution (Select Nation2)
  //      ---$$$$$$$$$$$$$$$"
  //      << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<NATION> reader(bm, full_input_name,
                                      bm->getFileMode(full_input_name));

  deceve::bama::Writer<NATION> res_writer(bm, output_full_path,
                                          deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  NATION item;
  srand(time(NULL));
  std::string nationNames[] = {
      "ALGERIA",    "ARGENTINA", "BRAZIL",        "CANADA",  "EGYPT",
      "ETHIOPIA",   "FRANCE",    "GERMANY",       "INDIA",   "INDONESIA",
      "IRAN",       "JAPAN",     "JORDAN",        "KENYA",   "MOROCCO",
      "MOZAMBIQUE", "PERU",      "CHINA",         "ROMANIA", "SAUDI ARABIA",
      "VIETNAM",    "RUSSIA",    "UNITED KINGDOM"};

  /* Generate random date and subtract it from the current date */
  std::string randomNationName = nationNames[rand() % 23];

  while (randomNationName == otherNationName) {
    randomNationName = nationNames[rand() % 23];
  }

  while (reader.hasNext()) {
    item = reader.nextRecord();
    if (strcmp(item.N_NAME, randomNationName.c_str()) == 0) {
      res_writer.write(item);
    }
  }
  reader.close();
  res_writer.close();

  //############################ Delete Previous File
  //####################################
  //  check if the input is a primary table and not an intermediate results
  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }
}

void Query7Node9::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  //  COUT << "Query7Node9 Execution (Join)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_nation left_extractor(1);
  q7_join4_extractor_result right_extractor(0);
  q7_join5_combinator_nation1_result combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  joiner = new deceve::bama::GraceJoin<
      NATION, Q7_JOIN4_STRUCT_SUPPLIER_RESULT, column_extractor_nation,
      q7_join4_extractor_result, q7_join5_combinator_nation1_result>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      combinator, partitions, joinMode);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);

  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);

  bm->removeFile(output_full_path);
}

void Query7Node10::run(deceve::storage::BufferManager* bm,
                       const std::string& input_name,
                       const std::string& simple_output_name) {
  //####################### Sorting Template
  //####################################

  //  COUT
  //      << "******************** -Query7Node10 Execution (Sort)-
  //      ********************"
  //      << "\n";
  std::string output_full_path;
  /*
   * EXECUTE USING PERSISTED FILES
   */
  if (check_usePersistedResult_flag) {
    node_output_name = information_about_persisted_files.output_name;
    bm->getSM().incrementPersistentStatisticsHits(tk);
    // Inform next operator to unpin file on the persisted tmp catalog

  } else {
    //####################### Construct Filenames
    //####################################
    std::string full_input_name;

    if (input_file_path == deceve::storage::PRIMARY_PATH) {
      full_input_name = constructFullPath(input_name);
    } else {
      full_input_name = constructInputTmpFullPath(node_input_name);
    }

    output_full_path = constructOutputTmpFullPath(simple_output_name);

    //################################ OPERATION
    //#####################################
    q7_join5_extractor_result extractor(0);

    deceve::storage::FileMode sorterMode = deceve::storage::PRIMARY;
    if (check_PersistResult_flag) {
      sorterMode = deceve::storage::INTERMEDIATE;
    }

    sorter = new deceve::bama::ReplacementSort<Q7_JOIN5_STRUCT_NATION1_RESULT,
                                               q7_join5_extractor_result>(
        bm, full_input_name, output_full_path, extractor, POOLSIZE, sorterMode);

    // EXECUTE AND PERSIST RESULTS
    if (check_PersistResult_flag) {
      sorter->persistResult();
      sorter->sort();

      if (!bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }

      // REMOVE FROM TMPFILESET
      bm->getSM().deleteFromPersistedTmpFileSet(tk);

      // ADD TO PERSISTED FILE CATALOG
      tv.num_files = sorter->getNumberOfRuns();
      tv.output_name = simple_output_name;
      tv.full_output_path = output_full_path;
      tv.pins = 0;
      bm->getSM().insertInPersistedFileCatalog(std::make_pair(tk, tv));
    } else {
      sorter->sort();

      if (tk.version == deceve::storage::SORT &&
          !bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }
    }

    delete sorter;
    //############################ Delete Previous File
    //####################################
    if (!bm->getSM().isInTableCatalog(full_input_name) &&
        !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
      bm->removeFile(full_input_name);
    }
  }

  //  scan<Q7_JOIN5_STRUCT_NATION1_RESULT>(output_full_path);

  bm->removeFile(output_full_path);
}
}  // namespace queries
}  // namespace deceve
