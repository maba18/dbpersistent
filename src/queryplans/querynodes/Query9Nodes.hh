/*
 * Query9Nodes.hh
 *
 *  Created on: Mar 17, 2015
 *      Author: maba18
 */

#ifndef QUERY9NODES_HH_
#define QUERY9NODES_HH_

// Include General Functions
#include "../definitions.hh"
#include "../../utils/global.hh"

// Include base class
#include "../NodeTree.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

// Include Algorithm Classes
#include "../../algorithms/replacementsort.hh"
#include "../../algorithms/gracejoin.hh"
#include "../../algorithms/gracejoinConvertRight.hh"
#include "../../algorithms/gracejoinConvertBoth.hh"
#include "../../algorithms/mergesortjoin.hh"

#include <vector>

namespace deceve {
namespace queries {

struct comparatorQ91Left {
  comparatorQ91Left() {
    types = {"almond",    "antique",   "aquamarine", "azure",      "beige",
             "bisque",    "black",     "blanched",   "blue",       "blush",
             "brown",     "burlywood", "burnished",  "chartreuse", "chiffon",
             "chocolate", "coral",     "cornflower", "cornsilk",   "cream",
             "cyan",      "dark",      "deep",       "dim",        "dodger",
             "drab",      "firebrick", "floral",     "forest",     "frosted",
             "gainsboro", "ghost",     "goldenrod",  "green",      "grey",
             "honeydew",  "hot",       "indian",     "ivory",      "khaki",
             "lace",      "lavender",  "lawn",       "lemon",      "light",
             "lime",      "linen",     "magenta",    "maroon",     "medium",
             "metallic",  "midnight",  "mint",       "misty",      "moccasin",
             "navajo",    "navy",      "olive",      "orange",     "orchid",
             "pale",      "papaya",    "peach",      "peru",       "pink",
             "plum",      "powder",    "puff",       "purple",     "red",
             "rose",      "rosy",      "royal",      "saddle",     "salmon",
             "sandy",     "seashell",  "sienna",     "sky",        "slate",
             "smoke",     "snow",      "spring",     "steel",      "tan",
             "thistle",   "tomato",    "turquoise",  "violet",     "wheat",
             "white",     "yellow"};

    randomIndex = rand() % 92;
  }

  bool operator()(PART item) {
    std::string P_NAME = string(item.P_NAME);
    std::size_t found;
    found = P_NAME.find(types[randomIndex]);
    if (found != std::string::npos) {
      return true;
    }
    return false;
  }

  std::vector<std::string> types;
  int randomIndex;
};

class Query9Node0 : public NodeTree {
 public:
  Query9Node0(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    // P_NAME
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query9Node1 : public NodeTree {
 public:
  Query9Node1(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
    tk.version = deceve::storage::HASHJOIN;
    left_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::PART;
    left_tk.field = deceve::storage::OTHER_FIELD;

    right_tk.version = deceve::storage::HASHJOIN;
    right_tk.table_name = deceve::storage::LINEITEM;
    //        right_tk.field = bm->getStorageManager().getRandomPartKeyField(1);
    right_tk.field = deceve::storage::L_PARTKEY;
#ifdef UNIFIED
    right_tk.projected_field = deceve::storage::LINEITEM_UNIFIED;
#else
    right_tk.projected_field = deceve::storage::LINEITEM_Q9;
#endif

    leftFileDetails(0.054, sizeof(PART), sizeof(Q9_PR_PART));
    rightFileDetails(1, sizeof(LINEITEM), sizeof(Q9_PR_LINEITEM));
    outputFileDetails(0.052, sizeof(Q9_JOIN1_STRUCT_PART_LINEITEM));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  //  deceve::bama::GraceJoin<PART, LINEITEM, column_extractor_part,
  //  column_extractor_lineitem, q9_combinator_part_line,
  //      std::less<int>, comparatorQ91Left,
  //      deceve::bama::DefaultComparator<LINEITEM> > *joiner;

  //  deceve::bama::GraceJoinConvert<PART, LINEITEM, Q9_PR_LINEITEM,
  //  column_extractor_part, column_extractor_lineitem,
  //      q9_converter_PR_LINEITEM, q9_extractor_PR_LINEITEM,
  //      q9_combinator_part_pr_lineitem, std::less<int>,
  //      comparatorQ91Left, deceve::bama::DefaultComparator<LINEITEM> >
  //      *joiner1;

  deceve::bama::GraceJoinConvertBoth<
      PART, Q9_PR_PART, LINEITEM, Q9_PR_LINEITEM, column_extractor_part,
      column_extractor_lineitem, q9_converter_PR_PART, q9_converter_PR_LINEITEM,
      q9_extractor_PR_PART, q9_extractor_PR_LINEITEM,
      q9_combinator_pr_part_pr_lineitem, std::less<int>, comparatorQ91Left,
      deceve::bama::DefaultComparator<LINEITEM> > *joiner;
};

class Query9Node2 : public NodeTree {
 public:
  Query9Node2(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;
    left_tk.version = deceve::storage::OTHER;

    // SUPPLIER_USAGE
    right_tk.version = deceve::storage::OTHER;

    left_tk.table_name = deceve::storage::OTHER_TABLENAME;
    right_tk.table_name = deceve::storage::SUPPLIER;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::S_SUPPKEY;

    leftFileDetails(0.052, sizeof(Q9_JOIN1_STRUCT_PART_LINEITEM),
                    sizeof(Q9_JOIN1_STRUCT_PART_LINEITEM), 0.02);
    rightFileDetails(1, sizeof(SUPPLIER), sizeof(SUPPLIER));
    outputFileDetails(0.002, sizeof(Q9_JOIN2_STRUCT_RESULT_SUPPLIER));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::GraceJoin<Q9_JOIN1_STRUCT_PART_LINEITEM, SUPPLIER,
                          q9_join2_extractor_result, column_extractor_supplier,
                          q9_combinator_result_supplier> *joiner;
};

class Query9Node3 : public NodeTree {
 public:
  Query9Node3(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;
    //		left_tk.table_name = "result";
    //		right_tk.table_name = "nation";
    //		left_tk.field = "S_NATIONKEY";
    //		right_tk.field = "N_NATIONKEY";
    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::OTHER_TABLENAME;
    right_tk.table_name = deceve::storage::OTHER_TABLENAME;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::OTHER_FIELD;
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::GraceJoin<Q9_JOIN2_STRUCT_RESULT_SUPPLIER, NATION,
                          q9_join3_extractor_result, column_extractor_nation,
                          q9_combinator_result_nation> *joiner;
};

class Query9Node4 : public NodeTree {
 public:
  Query9Node4(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    //		tk.table_name = "result_sort";
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    //		tk.field = "L_ORDERKEY";
    tk.field = deceve::storage::OTHER_FIELD;
    sorter = NULL;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);

 private:
  deceve::bama::ReplacementSort<Q9_JOIN3_STRUCT_RESULT_NATION,
                                q9_sort4_extractor_result> *sorter;
};

class Query9Node5 : public NodeTree {
 public:
  Query9Node5(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;
    //		left_tk.table_name = "result";
    //		right_tk.table_name = "orders";
    //		left_tk.field = "O_ORDERKEY";
    //		right_tk.field = "L_ORDERKEY";
    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::OTHER_TABLENAME;
    right_tk.table_name = deceve::storage::OTHER_TABLENAME;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::OTHER_FIELD;
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::MergeSortJoin<Q9_JOIN3_STRUCT_RESULT_NATION, ORDER,
                              q9_sort4_extractor_result, column_extractor_order,
                              q9_combinator_result_order> *joiner;
};

class Query9Node6 : public NodeTree {
 public:
  Query9Node6(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;
    //		left_tk.table_name = "result";
    //		right_tk.table_name = "partsupp";
    //		left_tk.field = "P_PARTKEY";
    //		right_tk.field = "PS_PARTKEY";
    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::OTHER_TABLENAME;
    right_tk.table_name = deceve::storage::OTHER_TABLENAME;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::OTHER_FIELD;
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::MergeSortJoin<
      Q9_JOIN4_STRUCT_RESULT_ORDER, PARTSUPP, q9_join5_extractor_result,
      column_extractor_partsupp, q9_combinator_result_partsupp> *joiner;
};

class Query9Node7 : public NodeTree {
 public:
  Query9Node7(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    // YEAR
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query9Node8 : public NodeTree {
 public:
  Query9Node8(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    //		tk.table_name = "result_group_sort";
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    //		tk.field = "q9_n8_column_extractor";
    tk.field = deceve::storage::OTHER_FIELD;
    sorter = NULL;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);

 private:
  deceve::bama::ReplacementSort<Q9_JOIN5_STRUCT_RESULT_PARTSUPP,
                                q9_node8_extractor_result> *sorter;
};
}
}

#endif /* QUERY9NODES_HH_ */
