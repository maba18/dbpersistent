/*
 * Query1Nodes.cc
 *
 *  Created on: 11 Mar 2015
 *      Author: michail
 */

#include "Query6Nodes.hh"

namespace deceve {
namespace queries {

void Query6Node1::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  //	COUT << "Query6Node1 Execution (Select)" << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }
  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<LINEITEM> reader(bm, full_input_name,
                                        bm->getFileMode(full_input_name));

  //################################ OPERATION
  //########################################

  /* Generate random date and substract it from the current date */
  srand(time(NULL));
  int randomYear = 1993 + rand() % 5;
  double randomDiscount = fRand(0.02, 0.07);
  double randomQuantity = fRand(24, 25);
  //	randomYear = 1994;
  //	randomDiscount = 0.06;
  //	randomQuantity = 24;
  date d1(randomYear, 1, 1);
  date d2(randomYear + 1, 1, 1);
  LINEITEM item;
  double total_sum = 0;
  int counter = 0;
  while (reader.hasNext()) {
    item = reader.nextRecord();

    date l_shipdate(item.L_SHIPDATE);
    if ((l_shipdate >= d1 && l_shipdate < d2 &&
         item.L_DISCOUNT > randomDiscount - 0.01 &&
         item.L_DISCOUNT < randomDiscount + 0.01 &&
         item.L_QUANTITY < randomQuantity)) {
      total_sum = total_sum + item.L_EXTENDEDPRICE * item.L_DISCOUNT;
      counter++;
    }
  }

  reader.close();
}
}
} /* namespace deceve */
