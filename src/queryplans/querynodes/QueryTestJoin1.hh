/*
 * QueryTestJoin1.hh
 *
 *  Created on: Jan 23, 2015
 *      Author: maba18
 */

#ifndef QUERYTESTJOIN1_HH_
#define QUERYTESTJOIN1_HH_

//Include General Functions
#include "../definitions.hh"
#include "../../utils/global.hh"

//Include base class
#include "../NodeTree.hh"

//Include Storage Classes
#include "../../storage/BufferManager.hh"

//Include Algorithm Classes
#include "../../algorithms/hashjoin.hh"
#include "../../algorithms/gracejoin.hh"

namespace deceve {
namespace queries {

class QueryTestJoin1: public NodeTree {

public:

	QueryTestJoin1(deceve::storage::BufferManager *bmr,
			const std::string& l_name, const std::string& r_name,
			NodeTree *lNode = NULL, NodeTree *rNode = NULL) :
			NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {

		tk.version = deceve::storage::HASHJOIN;
//		left_tk.table_name = "lineitem";
//		right_tk.table_name = "part";
//		left_tk.field = "L_PARTKEY";
//		right_tk.field = "P_PARTKEY";

		left_tk.version = deceve::storage::HASHJOIN;
		right_tk.version = deceve::storage::HASHJOIN;
		left_tk.table_name = deceve::storage::LINEITEM;
		right_tk.table_name = deceve::storage::PART;
		left_tk.field = deceve::storage::L_PARTKEY;
		right_tk.field = deceve::storage::P_PARTKEY;

		if (l_name == "nothing") {
			left_file_mode = deceve::storage::INTERMEDIATE;
		}
		if (r_name == "nothing") {
			right_file_mode = deceve::storage::INTERMEDIATE;
		}

	}

	void run(deceve::storage::BufferManager *bm, const std::string & left_file,
			const std::string& right_file,
			const std::string& simple_output_name);

private:

	deceve::bama::GraceJoin<LINEITEM, PART, column_extractor_lineitem,
			column_extractor_part, q14_combinator_part_line> *joiner;

	deceve::bama::GraceJoin<LINEITEM, PART, column_extractor_lineitem,
			column_extractor_part, q14_combinator_part_line> *graceJoiner;

};

}
}

#endif /* QUERYTESTJOIN1_HH_ */
