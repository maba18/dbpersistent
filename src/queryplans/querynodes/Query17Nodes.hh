/*
 // * Query17Nodes.hh
 *
 *  Created on: Mar 31, 2015
 *      Author: maba18
 */

#ifndef QUERY17NODES_HH_
#define QUERY17NODES_HH_

// Include General Functions
#include "../definitions.hh"
#include "../../utils/global.hh"

#include <sstream>

// Include base class
#include "../NodeTree.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

// Include Algorithm Classes
#include "../../algorithms/replacementsort.hh"
#include "../../algorithms/gracejoin.hh"
#include "../../algorithms/gracejoinConvertRight.hh"
#include "../../algorithms/gracejoinConvertBoth.hh"

#include "../../algorithms/partitionMapReduce.hh"
#include "../../algorithms/gracejoinMapReduce.hh"
#include "../../storage/identity.hh"

#include "../../algorithms/transformer.hh"

namespace deceve {
namespace queries {

class Query17Node00 : public NodeTree {
 public:
  Query17Node00(deceve::storage::BufferManager *bmr,
                const std::string &input_name, NodeTree *lNode = NULL,
                NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

struct comparatorQ171Left {
  comparatorQ171Left() {
    //    containers = {"STANDARD ANODIZED TIN",  "SMALL BURNISHED NICKEL",
    //                  "MEDIUM PLATED BRASS",    "LARGE POLISHED STEEL",
    //                  "ECONOMY BRUSHED COPPER", "PROMO"};

    containers1 = {"SM", "LG", "MED", "JUMBO", "WRAP"};

    containers2 = {"CASE", "BOX", "BAG", "JAR", "PKG", "PACK", "CAN", "DRUM"};

    srand(time(NULL));
    int N = 1 + rand() % 5;
    int M = 1 + rand() % 5;
    std::stringstream s;
    s << "BRAND#" << N << M;
    brand = s.str();
    randomIndex = rand() % containers1.size();

    std::stringstream c;
    c << containers1[randomIndex] << " ";
    randomIndex = rand() % containers2.size();
    c << containers2[randomIndex];
    container = c.str();
  }

  bool operator()(PART item) {
    std::string containerTuple = string(item.P_CONTAINER);
    std::string brandnameTuple = string(item.P_BRAND);

    //    std::cout << containerTuple << " " << brandnameTuple << " vs " <<
    //    container
    //              << " " << brand << std::endl;
    if (containerTuple == container && brandnameTuple == brand) {
      //      std::cout << "TRUE" << std::endl;
      return true;
    }
    return false;
  }

  std::vector<std::string> containers1;
  std::vector<std::string> containers2;
  std::string brand;
  std::string container;
  int randomIndex;
};

class Query17Node1 : public NodeTree {
 public:
  Query17Node1(deceve::storage::BufferManager *bmr, const std::string &l_name,
               const std::string &r_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;

// NOT AUXILIARY
    left_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::PART;
    left_tk.field = deceve::storage::OTHER_FIELD;
    left_tk.projected_field = deceve::storage::PART_Q17_OTHER;


    // AUXILIARY ON LINITEM PARTKEY
    right_tk.version = deceve::storage::HASHJOIN;
    right_tk.table_name = deceve::storage::LINEITEM;
    right_tk.field = deceve::storage::L_PARTKEY;

#ifdef UNIFIED
    right_tk.projected_field = deceve::storage::LINEITEM_UNIFIED;
#else
    right_tk.projected_field = deceve::storage::LINEITEM_Q17;
#endif

    right_tk.attributes = {sto::L_EXTENDEDPRICE, sto::L_PARTKEY,
                           sto::L_QUANTITY};

    // std::cout << "right_tk: " << right_tk << std::endl;

    //    localVariable.L_EXTENDEDPRICE = w.L_EXTENDEDPRICE;
    //    localVariable.L_PARTKEY = w.L_PARTKEY;
    //    localVariable.L_QUANTITY = w.L_QUANTITY;

    // No ouput so writeFactor=0
    //    outputTupleSize = sizeof(Q9_JOIN1_STRUCT_PART_LINEITEM);
    //    outputWriteFactor = 0.2;

    leftFileDetails(1, sizeof(PART), sizeof(Q17_PR_PART));
    rightFileDetails(1, sizeof(LINEITEM), sizeof(Q17_PR_LINEITEM));
    outputFileDetails(0, sizeof(Q9_JOIN1_STRUCT_PART_LINEITEM));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  //  deceve::bama::GraceJoin<PART, LINEITEM, column_extractor_part,
  //                          column_extractor_lineitem,
  //                          q9_combinator_part_line,
  //                          std::less<int>, comparatorQ171Left,
  //                          deceve::bama::DefaultComparator<LINEITEM> >
  //                          *joiner;
  //  deceve::bama::GraceJoinConvertRight<PART, LINEITEM, Q17_PR_LINEITEM,
  //  column_extractor_part, column_extractor_lineitem,
  //      q17_converter_PR_LINEITEM, q17_extractor_PR_LINEITEM,
  //      q17_combinator_part_pr_lineitem, std::less<int>,
  //      deceve::bama::DefaultComparator<PART>,
  //      deceve::bama::DefaultComparator<LINEITEM>> *joiner;
  //
  deceve::bama::GraceJoinConvertBoth<
      PART, Q17_PR_PART, LINEITEM, Q17_PR_LINEITEM, column_extractor_part,
      column_extractor_lineitem, q17_converter_PR_PART,
      q17_converter_PR_LINEITEM, q17_extractor_PR_PART,
      q17_extractor_PR_LINEITEM, q17_combinator_pr_part_pr_lineitem,
      std::less<int>, deceve::bama::DefaultComparator<PART>,
      deceve::bama::DefaultComparator<LINEITEM>> *joiner;
};

class Query17Node2 : public NodeTree {
 public:
  Query17Node2(deceve::storage::BufferManager *bmr, const std::string &l_name,
               const std::string &r_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    tk.version = deceve::storage::HASHJOIN;

    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::HASHJOIN;
    left_tk.table_name = deceve::storage::OTHER_TABLENAME;
    right_tk.table_name = deceve::storage::LINEITEM;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::L_PARTKEY;
    //    right_tk.field = bm->getStorageManager().getRandomPartKeyField(3);

    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  //  deceve::bama::GraceJoin<PART, LINEITEM, column_extractor_part,
  //                          column_extractor_lineitem,
  //                          q9_combinator_part_line,
  //                          std::less<int>, comparatorQ171Left,
  //                          deceve::bama::DefaultComparator<LINEITEM> >
  //                          *joiner;
  deceve::bama::GraceJoin<
      PART, Q17_PR_LINEITEM, column_extractor_part, q17_extractor_PR_LINEITEM,
      q17_combinator_part_pr_lineitem, std::less<int>,
      deceve::bama::DefaultComparator<PART>,
      deceve::bama::DefaultComparator<Q17_PR_LINEITEM>> *joiner;
};

class Query17Node1MapReduce : public NodeTree {
 public:
  Query17Node1MapReduce(deceve::storage::BufferManager *bmr,
                        const std::string &input_name, NodeTree *lNode = NULL,
                        NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_mode = deceve::storage::INTERMEDIATE;
    };
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query17Node2MapReduce : public NodeTree {
 public:
  Query17Node2MapReduce(deceve::storage::BufferManager *bmr,
                        const std::string &l_name, const std::string &r_name,
                        NodeTree *lNode = NULL, NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    tk.version = deceve::storage::HASHJOIN;
    //		left_tk.table_name = "part";
    //		right_tk.table_name = "lineitem";
    //		left_tk.field = "P_PARTKEY";
    //		right_tk.field = "L_PARTKEY";
    left_tk.table_name = deceve::storage::PART;
    right_tk.table_name = deceve::storage::LINEITEM;
    left_tk.field = deceve::storage::P_PARTKEY;
    right_tk.field = deceve::storage::L_PARTKEY;
    left_tk.version = deceve::storage::HASHJOIN;
    right_tk.version = deceve::storage::HASHJOIN;

    if (l_name == "nothing") {
      left_file_mode = deceve::storage::INTERMEDIATE;
    };
    if (r_name == "nothing") {
      right_file_mode = deceve::storage::INTERMEDIATE;
    }
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  // deceve::bama::GraceJoinMapReduce<PART, LINEITEM, column_extractor_part,
  // 		column_extractor_lineitem, my_combinator_part_lineitem> *joiner;

  deceve::bama::GraceJoinMapReduce<
      LINEITEM, PART, TRANSFORMED_LINEITEM_STRUCT, transformer_lineitem,
      transformer_part, column_extractor_lineitem, column_extractor_part,
      extractor_TRANSFORMED_LINEITEM_STRUCT, my_combinator_transformed> *joiner;
};

class Query17Node3MapReduce : public NodeTree {
 public:
  Query17Node3MapReduce(deceve::storage::BufferManager *bmr,
                        const std::string &input_name, NodeTree *lNode = NULL,
                        NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_mode = deceve::storage::INTERMEDIATE;
    };
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};
}
} /* namespace deceve */
#endif /* QUERY17NODES_HH_ */
