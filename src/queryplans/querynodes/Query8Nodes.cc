/*
 * Query8Nodes.cc
 *
 *  Created on: 17 Mar 2015
 *      Author: michail
 */

#include "Query8Nodes.hh"

#include <string>
#include <set>

namespace deceve {
namespace queries {

void Query8Node0::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  // std::cout << "Query8Node0 Execution (Select Part)"
  //           << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<PART> reader(bm, full_input_name,
                                    bm->getFileMode(full_input_name));

  deceve::bama::Writer<PART> res_writer(bm, output_full_path,
                                        deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  std::string types[] = {"STANDARD ANODIZED TIN",  "SMALL BURNISHED NICKEL",
                         "MEDIUM PLATED BRASS",    "LARGE POLISHED STEEL",
                         "ECONOMY BRUSHED COPPER", "PROMO"};

  std::set<std::string> typeSet(types, types + 6);

  PART item;
  /* Generate random date and subtract it from the current date */
  srand(time(NULL));
  int randomIndex = rand() % 6;
  int counter = 0;
  while (reader.hasNext()) {
    item = reader.nextRecord();
    std::string type = string(item.P_TYPE);
    if (type == types[randomIndex]) {
      res_writer.write(item);
      counter++;
    }
  }
  reader.close();
  res_writer.close();

  //############################ Delete Previous File
  //####################################
  // check if the input is a primary table and not an intermediate results
  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }
}

void Query8Node1::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  //  std::cout << "Query8Node1 Execution (Join <PART vd LINEITEM>)"
  //           << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_part left_extractor(0);
  column_extractor_lineitem right_extractor(0);
  //  q8_combinator_part_line combinator;

  q8_converter_PR_PART pr_left_converter;
  q8_extractor_PR_PART pr_left_extractor(0);

  q8_converter_PR_LINEITEM pr_right_converter;
  q8_extractor_PR_LINEITEM pr_right_extractor(0);

  q8_combinator_pr_part_pr_lineitem pr_final_combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  //  joiner = new deceve::bama::GraceJoin<
  //      PART, LINEITEM, column_extractor_part, column_extractor_lineitem,
  //      q8_combinator_part_line, std::less<int>, comparatorQ81Left,
  //      deceve::bama::DefaultComparator<LINEITEM> >(
  //      bm, left, right, output_full_path, left_extractor, right_extractor,
  //      combinator, partitions, joinMode);

  //  joiner = new deceve::bama::GraceJoinConvert<PART, LINEITEM,
  //  Q8_PR_LINEITEM, column_extractor_part,
  //      column_extractor_lineitem, q8_converter_PR_LINEITEM,
  //      q8_extractor_PR_LINEITEM, q8_combinator_part_pr_lineitem,
  //      std::less<int>, comparatorQ81Left,
  //      deceve::bama::DefaultComparator<LINEITEM> >(bm, left, right,
  //      output_full_path,
  //                                                                                     left_extractor, right_extractor,
  //                                                                                     pr_converter, pr_extractor,
  //                                                                                     pr_combinator, partitions,
  //                                                                                     joinMode,
  //                                                                                     deceve::storage::RIGHT_PROJECT);

  joiner = new deceve::bama::GraceJoinConvertBoth<
      PART, Q8_PR_PART, LINEITEM, Q8_PR_LINEITEM, column_extractor_part,
      column_extractor_lineitem, q8_converter_PR_PART, q8_converter_PR_LINEITEM,
      q8_extractor_PR_PART, q8_extractor_PR_LINEITEM,
      q8_combinator_pr_part_pr_lineitem, std::less<int>, comparatorQ81Left,
      deceve::bama::DefaultComparator<LINEITEM> >(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      pr_left_converter, pr_right_converter, pr_left_extractor,
      pr_right_extractor, pr_final_combinator, partitions, joinMode,
      deceve::storage::BOTH_PROJECT);

  joiner->setJoinTableKeys(left_tk, right_tk);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }

  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
}

void Query8Node2::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  // std::cout << "Query8Node2 Execution (Select Orders)"
  //           << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<ORDER> reader(bm, full_input_name,
                                     bm->getFileMode(full_input_name));

  deceve::bama::Writer<ORDER> res_writer(bm, output_full_path,
                                         deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  ORDER item;
  /* Generate random date and subtract it from the current date */
  date d1(1995, 1, 1);
  date d2(1996, 12, 31);
  int counter = 0;
  while (reader.hasNext()) {
    item = reader.nextRecord();
    date tmpdate(item.O_ORDERDATE);
    counter++;
    if (d1 <= tmpdate && tmpdate <= d2) {
      res_writer.write(item);
    }
  }
  reader.close();
  res_writer.close();

  //############################ Delete Previous File
  //####################################
  // check if the input is a primary table and not an intermediate results
  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }
}

void Query8Node3::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  // std::cout << "Query8Node3 Execution (Join <Results vs Orders>)"
  //          << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  q8_join2_extractor_result left_extractor(0);
  column_extractor_order right_extractor(1);
  //  q8_combinator_result_orders combinator;

  q8_converter_PR_ORDER pr_converter;
  q8_extractor_PR_ORDER pr_extractor(0);
  q8_combinator_other_pr_order pr_combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  //  joiner = new deceve::bama::GraceJoin<Q8_JOIN1_STRUCT_PART_LINEITEM, ORDER,
  //  q8_join2_extractor_result,
  //      column_extractor_order, q8_combinator_result_orders, std::less<int>,
  //      deceve::bama::DefaultComparator<Q8_JOIN1_STRUCT_PART_LINEITEM>,
  //      comparatorQ83Right>(bm, left, right,
  //                                                                                          output_full_path,
  //                                                                                          left_extractor,
  //                                                                                          right_extractor, combinator,
  //                                                                                          partitions, joinMode);

  joiner = new deceve::bama::GraceJoinConvertRight<
      Q8_JOIN1_STRUCT_PART_LINEITEM, ORDER, Q8_PR_ORDER,
      q8_join2_extractor_result, column_extractor_order, q8_converter_PR_ORDER,
      q8_extractor_PR_ORDER, q8_combinator_other_pr_order, std::less<int>,
      deceve::bama::DefaultComparator<Q8_JOIN1_STRUCT_PART_LINEITEM>,
      comparatorQ83Right>(bm, left, right, output_full_path, left_extractor,
                          right_extractor, pr_converter, pr_extractor,
                          pr_combinator, partitions, joinMode,
                          deceve::storage::RIGHT_PROJECT);

  joiner->setJoinTableKeys(left_tk, right_tk);


  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }

  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
}

void Query8Node4::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  // std::cout << "Query8Node4 Execution (Join <Results vs Customer>)"
  //          << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  q8_join3_extractor_result left_extractor(0);
  column_extractor_customer right_extractor(0);
  // q8_combinator_result_customers combinator;

  q8_converter_PR_CUSTOMER pr_converter;
  q8_extractor_PR_CUSTOMER pr_extractor(0);
  q8_combinator_other_pr_customer pr_combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  //  joiner = new deceve::bama::GraceJoin<Q8_JOIN2_STRUCT_RESULT_ORDERS,
  //  CUSTOMER, q8_join3_extractor_result,
  //      column_extractor_customer, q8_combinator_result_customers>(bm, left,
  //      right, output_full_path, left_extractor,
  //                                                                 right_extractor,
  //                                                                 combinator,
  //                                                                 partitions,
  //                                                                 joinMode);

  joiner = new deceve::bama::GraceJoinConvertRight<
      Q8_JOIN2_STRUCT_RESULT_ORDERS, CUSTOMER, Q8_PR_CUSTOMER,
      q8_join3_extractor_result, column_extractor_customer,
      q8_converter_PR_CUSTOMER, q8_extractor_PR_CUSTOMER,
      q8_combinator_other_pr_customer>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      pr_converter, pr_extractor, pr_combinator, partitions, joinMode,
      deceve::storage::RIGHT_PROJECT);

  joiner->setJoinTableKeys(left_tk, right_tk);


  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }

  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
}

void Query8Node5::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  // std::cout << "Query8Node5 Execution (Join <Results vs Nation>)"
  //           << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  q8_join4_extractor_result left_extractor(0);
  column_extractor_nation right_extractor(1);
  q8_combinator_result_nation combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  joiner = new deceve::bama::GraceJoin<
      Q8_JOIN3_STRUCT_RESULT_CUSTOMER, NATION, q8_join4_extractor_result,
      column_extractor_nation, q8_combinator_result_nation>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      combinator, partitions, joinMode);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }

  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
}

void Query8Node6::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  // std::cout << "Query8Node6 Execution (Select Region)"
  //           << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<REGION> reader(bm, full_input_name,
                                      bm->getFileMode(full_input_name));

  deceve::bama::Writer<REGION> res_writer(bm, output_full_path,
                                          deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  REGION item;
  /* Generate random date and subtract it from the current date */
  // srand(time(NULL));
  std::string regions[] = {"AFRICA", "AMERICA", "ASIA", "EUROPE",
                           "MIDDLE EAST"};

  int regionIndex = rand() % 5;
  int counter = 0;
  while (reader.hasNext()) {
    item = reader.nextRecord();
    counter++;
    if (string(item.R_NAME) == regions[regionIndex]) {
      res_writer.write(item);
    }
  }
  reader.close();
  res_writer.close();

  //############################ Delete Previous File
  //####################################
  // check if the input is a primary table and not an intermediate results
  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }
}

void Query8Node7::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  // std::cout << "Query8Node7 Execution (Join <Results vs Nation>)"
  //           << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  q8_join5_extractor_result left_extractor(0);
  column_extractor_region right_extractor(1);
  q8_combinator_result_region combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  joiner = new deceve::bama::GraceJoin<
      Q8_JOIN4_STRUCT_RESULT_NATION, REGION, q8_join5_extractor_result,
      column_extractor_region, q8_combinator_result_region>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      combinator, partitions, joinMode);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }

  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  // std::cout << "DELETE LEFT FILE: " << left << std::endl;
  // std::cout << "DELETE RIGHT FILE: " << right << std::endl;
  deletePreviousFilesJoin(bm, left, right);
}

void Query8Node8::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  // std::cout << "Query8Node8 Execution (Join <Results vs Region>)"
  //           << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  q8_join6_extractor_result left_extractor(0);
  column_extractor_supplier right_extractor(0);
  q8_combinator_result_supplier combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  joiner = new deceve::bama::GraceJoin<
      Q8_JOIN5_STRUCT_RESULT_REGION, SUPPLIER, q8_join6_extractor_result,
      column_extractor_supplier, q8_combinator_result_supplier>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      combinator, partitions, joinMode);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }

  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);

  bm->removeFile(output_full_path);
  
}

void Query8Node9::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  // COUT << "Query8Node5 Execution (Join <Results vs Nation1>)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  q8_join7_extractor_result left_extractor(0);
  column_extractor_nation right_extractor(1);
  q8_combinator_result_nation1 combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  joiner = new deceve::bama::GraceJoin<
      Q8_JOIN6_STRUCT_RESULT_SUPPLIER, NATION, q8_join7_extractor_result,
      column_extractor_nation, q8_combinator_result_nation1>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      combinator, partitions, joinMode);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }

  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
}

void Query8Node10::run(deceve::storage::BufferManager* bm,
                       const std::string& input_name,
                       const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  // COUT << "Query8Node10 Execution (gROUP Part)" << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<Q8_JOIN7_STRUCT_RESULT_NATION1> reader(
      bm, full_input_name, bm->getFileMode(full_input_name));

  deceve::bama::Writer<Q8_JOIN7_STRUCT_RESULT_NATION1> res_writer(
      bm, output_full_path, deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  Q8_JOIN7_STRUCT_RESULT_NATION1 item;
  /* Generate random date and subtract it from the current date */

  std::map<int, int> xartis;

  int counter = 0;
  while (reader.hasNext()) {
    item = reader.nextRecord();
    date d(item.o_orderdate);

    if (xartis.find(d.year) == xartis.end()) {
      xartis.insert(std::make_pair(d.year, 1));
      res_writer.write(item);
      counter++;
    }
  }
  reader.close();
  res_writer.close();

  //############################ Delete Previous File
  //####################################
  // check if the input is a primary table and not an intermediate results
  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }

  bm->removeFile(output_full_path);
}

void Query8Node11::run(deceve::storage::BufferManager* bm,
                       const std::string& input_name,
                       const std::string& simple_output_name) {
  //####################### Sorting Template
  //####################################

  //  COUT
  //      << "******************** -Query8Node11 Execution (Sort)-
  //      ********************"
  //      << "\n";
  std::string output_full_path;
  /*
   * EXECUTE USING PERSISTED FILES
   */
  if (check_usePersistedResult_flag) {
    node_output_name = information_about_persisted_files.output_name;
    bm->getSM().incrementPersistentStatisticsHits(tk);
    // Inform next operator to unpin file on the persisted tmp catalog

  } else {
    //####################### Construct Filenames
    //####################################
    std::string full_input_name;

    if (input_file_path == deceve::storage::PRIMARY_PATH) {
      full_input_name = constructFullPath(input_name);
    } else {
      full_input_name = constructInputTmpFullPath(node_input_name);
    }

    output_full_path = constructOutputTmpFullPath(simple_output_name);

    //################################ OPERATION
    //#####################################
    q8_node11_extractor_result extractor(0);

    deceve::storage::FileMode sorterMode = deceve::storage::PRIMARY;
    if (check_PersistResult_flag) {
      sorterMode = deceve::storage::INTERMEDIATE;
    }

    sorter = new deceve::bama::ReplacementSort<Q8_JOIN7_STRUCT_RESULT_NATION1,
                                               q8_node11_extractor_result>(
        bm, full_input_name, output_full_path, extractor, POOLSIZE, sorterMode);

    // EXECUTE AND PERSIST RESULTS
    if (check_PersistResult_flag) {
      sorter->persistResult();
      sorter->sort();

      if (!bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }

      // REMOVE FROM TMPFILESET
      bm->getSM().deleteFromPersistedTmpFileSet(tk);

      // ADD TO PERSISTED FILE CATALOG
      tv.num_files = sorter->getNumberOfRuns();
      tv.output_name = simple_output_name;
      tv.full_output_path = output_full_path;
      tv.pins = 1;
      bm->getSM().insertInPersistedFileCatalog(std::make_pair(tk, tv));
    } else {
      sorter->sort();

      if (!bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }
    }

    delete sorter;
    //############################ Delete Previous File
    //####################################
    if (!bm->getSM().isInTableCatalog(full_input_name) &&
        !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
      bm->removeFile(full_input_name);
    }
  }

  bm->removeFile(output_full_path);
  //  std::this_thread::sleep_for(std::chrono::seconds(1));
  //  sleep(std::chrono::seconds(2));
}
}  // namespace queries
}  // namespace deceve
