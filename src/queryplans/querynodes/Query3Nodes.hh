/*
 * Query3Nodes.hh
 *
 *  Created on: 2 Jun 2015
 *      Author: mike
 */

#ifndef QUERYPLANS_QUERYNODES_QUERY3NODES_HH_
#define QUERYPLANS_QUERYNODES_QUERY3NODES_HH_

// Include General Functions
#include "../definitions.hh"
#include "../../utils/global.hh"

// Include base class
#include "../NodeTree.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

// Include Algorithm Classes
#include "../../algorithms/replacementsort.hh"
#include "../../algorithms/gracejoin.hh"
#include "../../algorithms/gracejoinConvertBoth.hh"
#include "../../algorithms/gracejoinConvertLeft.hh"

// Include comparators
#include "../../storage/identity.hh"

/* srand example */
#include <stdio.h>  /* printf, NULL */
#include <stdlib.h> /* srand, rand */
#include <time.h>   /* time */

namespace deceve {
namespace queries {

//############### Comparators ###########################
struct comparatorQ33Right {
  comparatorQ33Right() {
    srand(time(NULL));
    std::string segments[] = {"AUTOMOBILE", "BUILDING", "FURNITURE",
                              "MACHINERY", "HOUSEHOLD"};
    randomSegment = segments[rand() % 5];
  }

  bool operator()(CUSTOMER item) {
    if (strcmp(item.C_MKTSEGMENT, randomSegment.c_str()) == 0) {
      return true;
    }
    return false;
  }

  std::string randomSegment;
};

struct comparatorQ33Left {
  comparatorQ33Left() {
    srand(time(NULL));
    int randomDay = rand() % 31 + 1;
    d1 = {1995, 03, randomDay};
    //  std::cout<<"Date 33: "<<d1<<std::endl;
  }

  bool operator()(ORDER a) {
    date tmpdate(a.O_ORDERDATE);
    return tmpdate < d1;
  }

  date d1;
};

struct comparatorQ34Left {
  comparatorQ34Left() {
    srand(time(NULL));
    int randomDay = rand() % 31 + 1;
    // std::cout<<"Date 34: "<<d1<<std::endl;
    d1 = {1995, 03, randomDay};
  }

  bool operator()(LINEITEM item) {
    date tmpdate(item.L_SHIPDATE);
    if (d1 < tmpdate) {
      return true;
    }
    return false;
  }

  date d1;
};

class Query3Node1 : public NodeTree {
 public:
  Query3Node1(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query3Node2 : public NodeTree {
 public:
  Query3Node2(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query3Node3 : public NodeTree {
 public:
  Query3Node3(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
    tk.version = deceve::storage::HASHJOIN;
    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::ORDERS;
    right_tk.table_name = deceve::storage::CUSTOMER;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::OTHER_FIELD;

    leftFileDetails(0.48, sizeof(ORDER), sizeof(Q3_PR_ORDER));
    rightFileDetails(0.2, sizeof(CUSTOMER), sizeof(Q3_PR_CUSTOMER));
    outputFileDetails(0.097, sizeof(Q3_JOIN1_STRUCT_ORDER_CUSTOMER));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::GraceJoinConvertBoth<
      ORDER, Q3_PR_ORDER, CUSTOMER, Q3_PR_CUSTOMER, column_extractor_order,
      column_extractor_customer, q3_converter_PR_ORDER,
      q3_converter_PR_CUSTOMER, q3_extractor_PR_ORDER, q3_extractor_PR_CUSTOMER,
      q3_combinator_pr_order_pr_customer, std::less<int>, comparatorQ33Left,
      comparatorQ33Right> *joiner;
};

class Query3Node4 : public NodeTree {
 public:
  Query3Node4(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query3Node5 : public NodeTree {
 public:
  Query3Node5(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;
    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::LINEITEM;
    right_tk.table_name = deceve::storage::OTHER_TABLENAME;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::OTHER_FIELD;

    leftFileDetails(0.53, sizeof(LINEITEM), sizeof(Q3_PR_LINEITEM));
    rightFileDetails(1, sizeof(Q3_JOIN1_STRUCT_ORDER_CUSTOMER), sizeof(Q3_JOIN1_STRUCT_ORDER_CUSTOMER));
    outputFileDetails(0.0, sizeof(Q3_JOIN2_STRUCT_LINEITEM_RESULT));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  //  deceve::bama::GraceJoin<LINEITEM, Q3_JOIN1_STRUCT_ORDER_CUSTOMER,
  //  column_extractor_lineitem,
  //      q3_join1_extractor_result, q3_join2_combinator_lineiten_result,
  //      std::less<int>, comparatorQ34Left,
  //      deceve::bama::DefaultComparator<Q3_JOIN1_STRUCT_ORDER_CUSTOMER> >
  //      *joiner;

  deceve::bama::GraceJoinConvertLeft<
      LINEITEM, Q3_JOIN1_STRUCT_ORDER_CUSTOMER, Q3_PR_LINEITEM,
      column_extractor_lineitem, q3_join1_extractor_result,
      q3_converter_PR_LINEITEM, q3_extractor_PR_LINEITEM,
      q3_combinator_pr_lineitem_other, std::less<int>, comparatorQ34Left,
      deceve::bama::DefaultComparator<Q3_JOIN1_STRUCT_ORDER_CUSTOMER> > *joiner;
};

class Query3Node6 : public NodeTree {
 public:
  Query3Node6(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query3Node7 : public NodeTree {
 public:
  Query3Node7(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;
    sorter = NULL;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);

 private:
  deceve::bama::ReplacementSort<Q3_JOIN2_STRUCT_LINEITEM_RESULT,
                                q3_join2_extractor_result> *sorter;
};
}  // namespace queries
}  // namespace deceve

#endif /* QUERYPLANS_QUERYNODES_QUERY3NODES_HH_ */
