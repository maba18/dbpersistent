/*
 * QueryTestJoin1.cc
 *
 *  Created on: Jan 23, 2015
 *      Author: maba18
 */

#include "QueryTestJoin1.hh"

#include "../NodeTree.hh"
#include "../definitions.hh"
#include "../../utils/global.hh"
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

namespace deceve {
namespace queries {

void QueryTestJoin1::run(deceve::storage::BufferManager *bm,
		const std::string & left_file, const std::string& right_file,
		const std::string& simple_output_name) {

	//#################################### Construct full paths for left and right file ######################################

	std::string left;
	if (left_file_mode == deceve::storage::INTERMEDIATE)
		left = constructInputTmpFullPath(left_file);
	else {
		left = constructFullPath(left_file);
	}

	std::string right;
	if (right_file_mode == deceve::storage::INTERMEDIATE)
		right = constructInputTmpFullPath(right_file);
	else {
		right = constructFullPath(right_file);
	}

	COUT << "Left file: " << left << "\n";
	COUT << "File size: " << bm->getFileSizeFromCatalog(left) << "\n";
	COUT << "Right file: " << right << "\n";

	std::string output_full_path = constructOutputTmpFullPath(
			simple_output_name);

	column_extractor_lineitem left_extractor(0);
	column_extractor_part right_extractor(0);
	q14_combinator_part_line combinator;

	//#################################### Calculate number of partitions and initialize joiner ##################################

	size_t partitions = (size_t) (1.2
			* (bm->getFileSizeFromCatalog(left)
					/ ((POOLSIZE - 2) * deceve::storage::PAGE_SIZE_PERSISTENT)));

	deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

	if (check_PersistResult_flag) {
		joinMode = deceve::storage::INTERMEDIATE;
	}

	joiner = new deceve::bama::GraceJoin<LINEITEM, PART,
			column_extractor_lineitem, column_extractor_part,
			q14_combinator_part_line>(bm, left, right, output_full_path,
			left_extractor, right_extractor, combinator, partitions, joinMode);

	/*
	 *    db::GraceJoin<db::wisc_t, db::wisc_t,
	 column_extractor, column_extractor,
	 simple_combinator>
	 joiner(&bm, left, right, output,
	 left_extractor, right_extractor, combinator, partitions);
	 */
	// graceJoiner = new deceve::bama::GraceJoin<LINEITEM, PART,
	// 		column_extractor_lineitem, q14_column_extractor_part,
	// 		q14_combinator_part_line>(bm, left, right, output_full_path,
	// 		left_extractor, right_extractor, combinator, partitions);

//#################################### Decide to persist or use persisted results ##################################
	if (check_PersistResult_flag) {
		joiner->persistResult(result_id);
	}
	if (check_usePersistedResult_flag) {

		if (result_id == 0) {
			COUT << "JOINNNNNNNNNN: "
					<< information_about_left_persisted_file << "\n";
			COUT << "JOINNNNNNNNNN: "
					<< information_about_right_persisted_file << "\n";
			joiner->usePersistedResult(information_about_left_persisted_file,
					information_about_right_persisted_file, result_id);
		} else if (result_id == 1) {
			joiner->usePersistedResult(information_about_left_persisted_file,
					result_id);
		} else if (result_id == 2) {
			joiner->usePersistedResult(information_about_right_persisted_file,
					result_id);
		}
	}

//#################################### Execute Join ##################################
	joiner->join();

//#################################### Add information to file persistent catalog ##################################

	if (check_PersistResult_flag) {

		bm->getSM().deleteFromPersistedTmpFileSet(tk);

		COUT << "################### RESULT ID IS: ###################"
				<< joiner->getResultId() << "\n";

		int tmp_result_id = joiner->getResultId();
		if (tmp_result_id == 0) {

			left_tv.num_files = joiner->getNumberOfPartitions();
			right_tv.num_files = joiner->getNumberOfPartitions();

			left_tv.output_name = left;
			right_tv.output_name = right;

			left_tv.full_output_path =
					joiner->getRunNamePrefixOfPersistentFile();
			right_tv.full_output_path =
					joiner->getRunNamePrefixOfPersistentFile();
			;

			COUT << "PERSIST: JOINNNNNNNNNN: " << left_tv << "\n";
			COUT << "PERSIST: JOINNNNNNNNNN: " << right_tv << "\n";

			bm->getSM().insertInPersistedFileCatalog(
					std::make_pair(left_tk, left_tv));
			bm->getSM().insertInPersistedFileCatalog(
					std::make_pair(right_tk, right_tv));

			if (!bm->getSM().isContainedInPersistentStatistics(
					left_tk)) {
				//Add information to persistent_file_statistics
				deceve::storage::table_statistics ts;
				ts.references = 1;
				ts.hits = 0;
				ts.cost = bm->calculateOperatorCost(left_tk);
				ts.timestamp = bm->getSM().getNextPersistentTime();

				bm->getSM().insertToPersistentStatistics(
						std::make_pair(left_tk, ts));
			} else {
				//Update information to persistent_file_statistics
				bm->getSM().incrementPersistentStatisticsReferences(
						left_tk);
			}

			if (!bm->getSM().isContainedInPersistentStatistics(
					right_tk)) {
				//Add information to persistent_file_statistics
				deceve::storage::table_statistics ts;
				ts.references = 1;
				ts.hits = 0;
				ts.cost = bm->calculateOperatorCost(right_tk);
				ts.timestamp = bm->getSM().getNextPersistentTime();

				bm->getSM().insertToPersistentStatistics(
						std::make_pair(right_tk, ts));
			} else {
				//Update information to persistent_file_statistics
				bm->getSM().incrementPersistentStatisticsReferences(
						right_tk);
			}

			bm->getSM().deleteFromPersistedTmpFileSet(left_tk);
			bm->getSM().deleteFromPersistedTmpFileSet(right_tk);

		}

		else if (tmp_result_id == 1) {

			left_tv.num_files = joiner->getNumberOfPartitions();
			left_tv.output_name = left;
			left_tv.full_output_path =
					joiner->getRunNamePrefixOfPersistentFile();

			bm->getSM().insertInPersistedFileCatalog(
					std::make_pair(left_tk, left_tv));

			if (!bm->getSM().isContainedInPersistentStatistics(
					left_tk)) {
				//Add information to persistent_file_statistics
				deceve::storage::table_statistics ts;
				ts.references = 1;
				ts.hits = 0;
				ts.cost = bm->calculateOperatorCost(left_tk);
				ts.timestamp = bm->getSM().getNextPersistentTime();

				bm->getSM().insertToPersistentStatistics(
						std::make_pair(left_tk, ts));
			} else {
				//Update information to persistent_file_statistics
				bm->getSM().incrementPersistentStatisticsReferences(
						left_tk);
			}

			bm->getSM().deleteFromPersistedTmpFileSet(left_tk);

		} else if (tmp_result_id == 2) {

			right_tv.num_files = joiner->getNumberOfPartitions();
			right_tv.output_name = right;
			right_tv.full_output_path =
					joiner->getRunNamePrefixOfPersistentFile();
			;

			bm->getSM().insertInPersistedFileCatalog(
					std::make_pair(right_tk, tv));

			if (!bm->getSM().isContainedInPersistentStatistics(
					right_tk)) {
				//Add information to persistent_file_statistics
				deceve::storage::table_statistics ts;
				ts.references = 1;
				ts.hits = 0;
				ts.cost = bm->calculateOperatorCost(right_tk);
				ts.timestamp = bm->getSM().getNextPersistentTime();

				bm->getSM().insertToPersistentStatistics(
						std::make_pair(right_tk, ts));
			} else {
				//Update information to persistent_file_statistics
				bm->getSM().incrementPersistentStatisticsReferences(
						right_tk);
			}

			bm->getSM().deleteFromPersistedTmpFileSet(right_tk);

		}

	}

	delete joiner;
	// delete graceJoiner;

	// int record_counter = 0;
	// deceve::bama::Reader<query14_struct> counter_reader(bm, output_full_path);
	// while (counter_reader.hasNext()) {
	// 	counter_reader.nextRecord();
	// 	record_counter++;
	// }
	// counter_reader.close();
	// COUT << "Record count for the join result is: " << record_counter
	// 		<< "\n";

}

}
}
