/*
 * Query4Nodes.cc
 *
 *  Created on: 4 Jun 2015
 *      Author: mike
 */

#include "Query4Nodes.hh"

namespace deceve {
namespace queries {

void Query4Node1::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  //  deceve::bama::GraceJoin<ORDER, LINEITEM, column_extractor_order,
  //      column_extractor_lineitem, q4_join1_combinator_order_lineitem,
  //      std::less<int>, comparatorQ41Left, comparatorQ41Right> *joiner;

  //  std::cout << "Query4Node1 Execution (Join)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_order left_extractor(1);
  column_extractor_lineitem right_extractor(1);
  // q4_join1_combinator_order_lineitem combinator;

  q4_converter_PR_LINEITEM pr_right_converter;
  q4_extractor_PR_LINEITEM pr_right_extractor(0);

  q4_converter_PR_ORDER pr_left_converter;
  q4_extractor_PR_ORDER pr_left_extractor(0);

  q4_combinator_pr_order_pr_lineitem pr_final_combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################
  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  //  joiner = new deceve::bama::GraceJoin<
  //      ORDER, LINEITEM, column_extractor_order, column_extractor_lineitem,
  //      q4_join1_combinator_order_lineitem, std::less<int>, comparatorQ41Left,
  //      comparatorQ41Right>(bm, left, right, output_full_path, left_extractor,
  //                          right_extractor, combinator, partitions,
  //                          joinMode);

  joiner = new deceve::bama::GraceJoinConvertBoth<
      ORDER, Q4_PR_ORDER, LINEITEM, Q4_PR_LINEITEM, column_extractor_order,
      column_extractor_lineitem, q4_converter_PR_ORDER,
      q4_converter_PR_LINEITEM, q4_extractor_PR_ORDER, q4_extractor_PR_LINEITEM,
      q4_combinator_pr_order_pr_lineitem, std::less<int>, comparatorQ41Left,
      comparatorQ41Right>(bm, left, right, output_full_path, left_extractor,
                          right_extractor, pr_left_converter,
                          pr_right_converter, pr_left_extractor,
                          pr_right_extractor, pr_final_combinator, partitions,
                          joinMode, deceve::storage::BOTH_PROJECT);

  joiner->setJoinTableKeys(left_tk, right_tk);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);

  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
}

void Query4Node2::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Group Template ####################################

  //  std::cout
  //      << "******************** -Query4Node2 Execution (Group)-
  //      ********************"
  //      << "\n";

  //####################### Construct Filenames
  //####################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //################################
  deceve::bama::Reader<Q4_JOIN1_STRUCT_ORDER_LINEITEM> reader(
      bm, full_input_name, bm->getFileMode(full_input_name));

  deceve::bama::Writer<Q4_GROUP_STRUCT_ORDER_LINEITEM> res_writer(
      bm, output_full_path, deceve::storage::PRIMARY);

  //################################ OPERATION
  //#####################################
  std::map<std::string, int> memoryMap;
  Q4_GROUP_STRUCT_ORDER_LINEITEM tempGroupStruct;
  while (reader.hasNext()) {
    Q4_JOIN1_STRUCT_ORDER_LINEITEM item = reader.nextRecord();

    string key(item.o_orderpriority);

    auto it = memoryMap.find(key);
    if (it == memoryMap.end()) {
      memoryMap[key] = 1;
    } else {
      it->second += 1;
    }
  }
  reader.close();

  for (auto it = memoryMap.begin(); it != memoryMap.end(); ++it) {
    q1_column_extractor extractor(0);
    strcpy(tempGroupStruct.o_orderpriority, it->first.c_str());
    tempGroupStruct.order_count = it->second;
    res_writer.write(tempGroupStruct);
  }
  res_writer.close();

  //############################ Delete Previous File
  //####################################

  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }
}

void Query4Node3::run(deceve::storage::BufferManager* bm,
                      const std::string& input_name,
                      const std::string& simple_output_name) {
  //####################### Sorting Template
  //####################################

  //  COUT
  //      << "******************** -Query4Node3 Execution (Sort)-
  //      ********************"
  //      << "\n";
  std::string output_full_path;
  /*
   * EXECUTE USING PERSISTED FILES
   */
  if (check_usePersistedResult_flag) {
    node_output_name = information_about_persisted_files.output_name;
    bm->getSM().incrementPersistentStatisticsHits(tk);
    // Inform next operator to unpin file on the persisted tmp catalog

  } else {
    //####################### Construct Filenames
    //####################################
    std::string full_input_name;

    if (input_file_path == deceve::storage::PRIMARY_PATH) {
      full_input_name = constructFullPath(input_name);
    } else {
      full_input_name = constructInputTmpFullPath(node_input_name);
    }

    output_full_path = constructOutputTmpFullPath(simple_output_name);

    //################################ OPERATION
    //#####################################
    q4_group_extractor_result extractor(0);

    deceve::storage::FileMode sorterMode = deceve::storage::PRIMARY;
    if (check_PersistResult_flag) {
      sorterMode = deceve::storage::INTERMEDIATE;
    }

    sorter = new deceve::bama::ReplacementSort<Q4_GROUP_STRUCT_ORDER_LINEITEM,
                                               q4_group_extractor_result>(
        bm, full_input_name, output_full_path, extractor, POOLSIZE, sorterMode);

    // EXECUTE AND PERSIST RESULTS
    if (check_PersistResult_flag) {
      sorter->persistResult();
      sorter->sort();

      if (!bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }

      // REMOVE FROM TMPFILESET
      bm->getSM().deleteFromPersistedTmpFileSet(tk);

      // ADD TO PERSISTED FILE CATALOG
      tv.num_files = sorter->getNumberOfRuns();
      tv.output_name = simple_output_name;
      tv.full_output_path = output_full_path;
      tv.pins = 0;
      bm->getSM().insertInPersistedFileCatalog(std::make_pair(tk, tv));
    } else {
      sorter->sort();

      if (tk.version == deceve::storage::SORT &&
          !bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }
    }

    delete sorter;
    //############################ Delete Previous File
    //####################################
    if (!bm->getSM().isInTableCatalog(full_input_name) &&
        !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
      bm->removeFile(full_input_name);
    }
  }

  // scan<Q4_GROUP_STRUCT_ORDER_LINEITEM>(output_full_path);

  bm->removeFile(output_full_path);
}
}  // namespace queries
}  // namespace deceve
