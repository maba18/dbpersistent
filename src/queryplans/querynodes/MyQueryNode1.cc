/*
 * MyQyeryNode1.cc
 *
 *  Created on: Jan 20, 2015
 *      Author: maba18
 */

#include "MyQueryNode1.hh"

#include <tuple>
#include <algorithm>
#include <iostream>

#include "../../algorithms/partition.hh"
#include "../definitions.hh"

#include "../../storage/BTree.hh"

namespace deceve {
namespace queries {

using namespace std;

struct myType {
  int type;
  union {
    char char_value;
    short short_value;
    int int_value;
    long long_value;
    float float_value;
    double double_value;
    void* ptr_value;
  };
};

void init_float(struct myType* v, double initial_value) {
  v->type = 0;
  v->double_value = initial_value;
}

struct comparatorQTest {
  bool operator()(LINEITEM item) {
    date tmpdate(item.L_SHIPDATE);
    return tmpdate < d1;
  }

  void initialise() {
    srand(time(NULL));
    int randomYear = 1995;
    int randomMonth = 3;
    int randomDay = 1 + rand() % 31;
    d1 = {randomYear, randomMonth, randomDay};
  }

  date d1;
};

date plusDates(date d1, int numOfMonths) {
  int nYear, nMonth, nDay;
  nYear = d1.year;
  nMonth = d1.month;
  nDay = d1.day;

  int sumMonths = nMonth + numOfMonths;

  if (sumMonths > 12) {
    nMonth = sumMonths % 12;
    nYear = nYear + 1;
  } else {
    nMonth = d1.month + numOfMonths;
  }

  //        1993\12
  //        1994\7
  //        Date1: [1993, 12, 1]
  //        Date2: [1994, 7, 1]

  std::cout << d1.year << "\\" << d1.month << "\n";
  std::cout << nYear << "\\" << nMonth << "\n";
  return date(nYear, nMonth, nDay);
}

struct resultRecord {
  int rec1;
  double rec2;
};

void Query0NodeWrite::run(deceve::storage::BufferManager* bm,
                          const std::string& input_name,
                          const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  //  COUT
  //      << "******************** -Query1Node4 Execution (Select)-
  //      ********************"
  //      << "\n";
  //####################### Construct Filenames
  //###################################
//  std::string fullOutputName;
//
//  if (input_file_path == deceve::storage::PRIMARY_PATH) {
//    fullOutputName = constructFullPath(input_name);
//  } else {
//    fullOutputName = constructInputTmpFullPath(node_input_name);
//  }
//
//  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

//  int randomOperation = 0;
//  //  srand(time(NULL));
//
//  /* generate secret number between 1 and 10: */
//  randomOperation = rand() % 2;
//
//  std::cout << "random operation: " << randomOperation << std::endl;
//
//  size_t fileSize = bm->getSM().getFileSizeFromCatalog(fullOutputName) /
//                    deceve::bama::PAGE_SIZE_PERSISTENT;
//
//  size_t tuplesPerPage =
//      deceve::storage::PAGE_SIZE_PERSISTENT / sizeof(testRec);
//
//  size_t totalTuples = tuplesPerPage * fileSize;
//
//  std::cout << "filesize: " << fileSize << std::endl;
//  std::cout << "tuplesPerPage: " << tuplesPerPage << std::endl;
//  std::cout << "totalTuples: " << totalTuples << std::endl;

  // if (randomOperation == 0) {
  int numberOfRecords = 500000;

  //############## test 1 #####################
  //  deceve::bama::Writer<testRec> writer(bm, fullOutputName,
  //                                       deceve::storage::PRIMARY);
  //  for (int i = 0; i < numberOfRecords; i++) {
  //    testRec record;
  //    record.value1 = i;
  //    record.value2 = i;
  //    record.value3 = i;
  //    record.value4 = i;
  //    record.id = i;
  //    writer.write(record);
  //  }
  //  writer.close();

  std::cout<<"HELLO THERE"<<std::endl;
  //############## test 2 #####################
  std::string test1 = constructInputTmpFullPath("test1.table");
  deceve::bama::Writer<int> writer1(bm, test1, deceve::storage::PRIMARY);

  for (int i = 0; i < 10 * numberOfRecords; i++) {
    writer1.write(i);
  }
  writer1.close();

  //############## test 2 #####################
//  std::string test2 = constructInputTmpFullPath("test2.table");
//  deceve::bama::Writer<int> writer2(bm, test2, deceve::storage::PRIMARY);
//
//  for (int i = 0; i < 10 * numberOfRecords; i++) {
//    writer2.write(i);
//  }
//  writer2.close();

//  deceve::bama::Reader<int> test1Reader(bm, test1);
//
//  while (test1Reader.hasNext()) {
//    int res = test1Reader.nextRecord();
//  }
//  test1Reader.close();

  //  //############## test 3 read initial file again #####################
  //  deceve::bama::Reader<testRec> reader(bm, fullOutputName);
  //  while (reader.hasNext()) {
  //    testRec rec = reader.nextRecord();
  //  }
  //  reader.close();
  //
  //  deceve::bama::Reader<testRec> reader1(bm, fullOutputName);
  //
  //  while (reader1.hasNext()) {
  //    testRec rec = reader1.nextRecord();
  //  }
  //  reader1.close();
  //
  //
  //
  //  std::cout << "Before1" << std::endl;
  //  std::string indexFileName = constructFullPath("indexLineitem.table");
  //
  //  deceve::storage::BTree<int, LINEITEM> indexOfLineitem(bm, indexFileName);
  //
  //  std::cout << "AFter" << std::endl;
  //  for (int times = 0; times < 10000; times++) {
  //    //    std::cout << "inserting record... " << times << "\n";
  //
  //    LINEITEM lineitem;
  //    indexOfLineitem.insert(
  //        deceve::storage::makeRecord<int, LINEITEM>(times, lineitem));
  //  }
  //
  //  for (auto it = indexOfLineitem.find(200); it != indexOfLineitem.end();
  //  ++it) {
  //    LINEITEM lineitem = it.current().payload;
  //  }
  //
  //
  //  std::string stupidTable = constructFullPath("stupidTable.table");
  //
  //  deceve::bama::Writer<char[10]> writerString(bm, stupidTable,
  //                                                 deceve::storage::PRIMARY);
  //
  //
  //  writerString.close();

  //
  //  else {
  //    deceve::bama::Reader<testUpdatedRecord> reader(bm, fullOutputName,
  //                                                   deceve::storage::PRIMARY);
  //
  //    int randomDivisor = rand() % 9 + 1;
  //    int limit = totalTuples;
  //
  //    if (rand() % 2 == 0) {
  //      limit = totalTuples / randomDivisor;
  //    } else {
  //      reader.skip(totalTuples / randomDivisor);
  //    }
  //
  //    int counter = 0;
  //    while (reader.hasNext() && counter < limit) {
  //      testRec record;
  //      record = reader.nextRecord();
  //      counter++;
  //    }
  //
  //    std::cout << "reader counter" << counter << std::endl;
  //    reader.close();
  //  }
}

void MyQueryNode1::run(deceve::storage::BufferManager* bm,
                       const std::string& input_name,
                       const std::string& simple_output_name) {
  // std::lock_guard<std::mutex>
  // lock_BufferPool(bm->getStorageManager().BufferPoolLockMutex);

  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  //  date d1;
  //  date d2;
  //
  //  srand(time(NULL));
  //  for (int i = 0; i < 10; i++) {
  //    int randomYear = 1993 + rand() % 5;
  //    int randomMonth = rand() % 12 + 1;
  //    if (randomYear == 1997) randomMonth = rand() % 10 + 1;
  //    d1 = {randomYear, randomMonth, 1};
  //    d2 = d1.plus(3);
  //
  //    //    d2= plusDates(d1,3);
  //
  //    //    std::cout << "Date1: " << d1 << std::endl;
  //    //    std::cout << "Date2: " << d2 << std::endl;
  //    //    std::cout << "----------------------------"<<std::endl;
  //  }

  //  COUT << "MyQueryNode1: full_input_name: " << full_input_name << "\n";

  //  std::string output_full_path =
  //  constructOutputTmpFullPath(simple_output_name);
  //
  //  column_extractor_lineitem extractor(0);
  //
  //  comparatorQTest comparatorTest;
  //  comparatorTest.initialise();
  //
  //  std::cout << "Partition file" << std::endl;
  //  //-------------- Partition Files ---------------------------
  //  deceve::bama::Partition<LINEITEM, column_extractor_lineitem,
  //  comparatorQTest> partitioner(
  //      bm, full_input_name, output_full_path, extractor, 10,
  //      deceve::storage::PRIMARY, comparatorTest);

  //  partitioner.partition();

  //  bm->getStorageManager().printFiles(deceve::storage::PRIMARY);
  //  bm->getStorageManager().printFiles(deceve::storage::INTERMEDIATE);
  //  bm->getStorageManager().printFiles(deceve::storage::AUXILIARY);
  //
  //  COUT << "FILEMODE for full_input_name: " << full_input_name << " "
  //       << bm->getFileMode(full_input_name) << "\n";
  deceve::bama::Reader<LINEITEM> reader(bm, full_input_name,
                                        bm->getFileMode(full_input_name));
  //
  //  COUT << "Number of pages of file is: " << reader.numPages() << "\n";

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  deceve::bama::Writer<LINEITEM> res_writer(bm, output_full_path,
                                            deceve::storage::PRIMARY);
  //
  LINEITEM item;
  /* Generate random date and subtract it from the current date */
  //  srand(time(NULL));
  //  int randomYear = 1995;
  //  int randomMonth = 3;
  //  int randomDay = 1 + rand() % 31;
  //  date d1(randomYear, randomMonth, randomDay);

  size_t counter = 0;
  while (reader.hasNext()) {
    item = reader.nextRecord();
    //    date tmpdate(item.L_SHIPDATE);
    counter++;
    //    if (d1 <= tmpdate) {
    //      res_writer.write(item);
    //      if (counter % 10000 == 0) std::cout << "Mike: " << item << "\n";
    //    }
    if (counter == 2) break;
  }

  for (int i = 0; i < 100000; i++) {
    res_writer.write(item);
  }

  std::cout << "Random update query was run: File generated is: "
            << output_full_path << "\n";
  reader.close();
  res_writer.close();
  //
  //  check if the input is a primary table and not an intermediate
  //  results
  //  if (!bm->getStorageManager().isInTableCatalog(full_input_name) &&
  //      !bm->getStorageManager().isInPersistedFileCatalog(leftNode->tk)) {
  //    COUT << "DELETE PREVIOUS file which is not in table
  //            catalog and
  //        is not persistent so just delete it "
  //            << "\n";
  //    bm->removeFile(full_input_name);
  //  }
  //
  //  if (!bm->getStorageManager().isInPersistedFileCatalog(tk)) {
  //    bm->removeFile(output_full_path);
  //  }
}

}  // namespace queries
}  // namespace deceve
