/*
 * MyQueryNode2.cc
 *
 *  Created on: Jan 20, 2015
 *      Author: maba18
 */

#include "MyQueryNode2.hh"

#include "../NodeTree.hh"
#include "../definitions.hh"
#include "../../utils/global.hh"
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

namespace deceve {
namespace queries {

void MyQueryNode2::run(deceve::storage::BufferManager* bm,
                       const std::string& input_name,
                       const std::string& simple_output_name) {
  // SORT TEMPLATE
  /*
   * EXECUTE USING PERSISTED FILES
   */
  if (check_usePersistedResult_flag) {
    node_output_name = information_about_persisted_files.output_name;
    COUT << "Use persisted results, node_output_name: " << node_output_name
         << "\n";

    // Update information to persistent_file_statistics
    bm->getSM().incrementPersistentStatisticsHits(tk);

    // Inform next operator to unpin file on the persisted tmp catalog

  } else {
    std::string full_input_name;

    if (input_file_mode == deceve::storage::PRIMARY) {
      full_input_name = constructFullPath(input_name);
    } else {
      full_input_name = constructInputTmpFullPath(node_input_name);
    }

    COUT << "MyQueryNode2 (SORT): full_input_name: " << full_input_name << "\n";
    std::string output_full_path =
        constructOutputTmpFullPath(simple_output_name);

    column_extractor_lineitem extractor(1);
    q2_extractor_PR_LINEITEM_SORT pr_extractor(0);
    q2_converter_PR_LINEITEM_SORT converter;

    deceve::storage::FileMode sorterMode = deceve::storage::PRIMARY;
    if (check_PersistResult_flag) {
      sorterMode = deceve::storage::INTERMEDIATE;
    }

    sorter = new deceve::bama::ReplacementSortConvert<
        LINEITEM, Q2_PR_LINEITEM_SORT, column_extractor_lineitem,
        q2_extractor_PR_LINEITEM_SORT, q2_converter_PR_LINEITEM_SORT>(
        bm, full_input_name, output_full_path, extractor, pr_extractor,
        converter, POOLSIZE, sorterMode);

    // EXECUTE AND PERSIST RESULTS
    if (check_PersistResult_flag) {
      sorter->persistResult(tk);
      uint64_t start_time, end_time;
      start_time = deceve::bama::rdtsc();
      sorter->sort();
      end_time = deceve::bama::rdtsc();
      COUT << "SORT COST WITH PERSISTING RESULTS: " << end_time - start_time
           << "\n";
      COUT << "sorter.getNumberOfRuns(): " << sorter->getNumberOfRuns() << "\n";

      if (!bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();

        bm->getSM().insertToPersistentStatistics(
            std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }

      //		}
      //
      //		if (isGenerator) {

      bm->getSM().deleteFromPersistedTmpFileSet(tk);
      // Add persisted file information to persisted_file_catalog
      tv.num_files = sorter->getNumberOfRuns();
      tv.output_name = simple_output_name;
      tv.full_output_path = output_full_path;
      tv.pins = 0;

      bm->getSM().insertInPersistedFileCatalog(
          std::make_pair(tk, tv));

      bm->getBufferPool().condVariable.notify_all();

      //			bm->getStorageManager().persisted_file_catalog.insert(
      //					std::make_pair(tk, tv));
    }
    // SIMPLY EXECUTE
    else {
      uint64_t start_time, end_time;
      COUT << "Execute sorter"
           << "\n";
      start_time = deceve::bama::rdtsc();
      sorter->sort();
      end_time = deceve::bama::rdtsc();
      COUT << "End sorter"
           << "\n";
      COUT << "SORT COST: " << end_time - start_time << "\n";

      if (!bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        //				ts.cost = end_time - start_time;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();

        bm->getSM().insertToPersistentStatistics(
            std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }
    }

    delete sorter;

    //		scan<LINEITEM>(output_full_path);

    /*
     * Delete generated file of the previous operation only if they don't exist
     * in the persisted file catalog
     * and it is not one of the main tables of the database
     */
    //		if (!bm->getStorageManager().isInTableCatalog(full_input_name)
    //				&&
    //! bm->getStorageManager().isInPersistedFileCatalog(
    //						leftNode->tk)) {
    //
    //			COUT
    //					<< "DELETE PREVIOUS file which is not in
    // table
    // catalog
    // and
    // is
    // not persistent so just delete it"
    //					<< "\n";
    //			bm->removeFile(full_input_name);
    //		}
  }
}

void MyQueryNode3::run(deceve::storage::BufferManager* bm,
                       const std::string& input_name,
                       const std::string& simple_output_name) {
  // SORT TEMPLATE
  /*
   * EXECUTE USING PERSISTED FILES
   */
  if (check_usePersistedResult_flag) {
    node_output_name = information_about_persisted_files.output_name;
    COUT << "Use persisted results, node_output_name: " << node_output_name
         << "\n";

    // Update information to persistent_file_statistics
    bm->getSM().incrementPersistentStatisticsHits(tk);

    // Inform next operator to unpin file on the persisted tmp catalog

  } else {
    std::string full_input_name;

    if (input_file_mode == deceve::storage::PRIMARY) {
      full_input_name = constructFullPath(input_name);
    } else {
      full_input_name = constructInputTmpFullPath(node_input_name);
    }

    COUT << "MyQueryNode2 (SORT): full_input_name: " << full_input_name << "\n";
    std::string output_full_path =
        constructOutputTmpFullPath(simple_output_name);

    column_extractor_part extractor(0);

    deceve::storage::FileMode sorterMode = deceve::storage::PRIMARY;
    if (check_PersistResult_flag) {
      sorterMode = deceve::storage::INTERMEDIATE;
    }

    sorter = new deceve::bama::ReplacementSort<PART, column_extractor_part>(
        bm, full_input_name, output_full_path, extractor, POOLSIZE, sorterMode);

    // EXECUTE AND PERSIST RESULTS
    if (check_PersistResult_flag) {
      sorter->persistResult(tk);
      uint64_t start_time, end_time;
      start_time = deceve::bama::rdtsc();
      sorter->sort();
      end_time = deceve::bama::rdtsc();
      COUT << "SORT COST WITH PERSISTING RESULTS: " << end_time - start_time
           << "\n";
      COUT << "sorter.getNumberOfRuns(): " << sorter->getNumberOfRuns() << "\n";

      if (!bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();

        bm->getSM().insertToPersistentStatistics(
            std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }

      //		}
      //
      //		if (isGenerator) {

      bm->getSM().deleteFromPersistedTmpFileSet(tk);
      // Add persisted file information to persisted_file_catalog
      tv.num_files = sorter->getNumberOfRuns();
      tv.output_name = simple_output_name;
      tv.full_output_path = output_full_path;
      tv.pins = 0;

      bm->getSM().insertInPersistedFileCatalog(
          std::make_pair(tk, tv));

      bm->getBufferPool().condVariable.notify_all();

      //			bm->getStorageManager().persisted_file_catalog.insert(
      //					std::make_pair(tk, tv));
    }
    // SIMPLY EXECUTE
    else {
      uint64_t start_time, end_time;
      COUT << "Execute sorter"
           << "\n";
      start_time = deceve::bama::rdtsc();
      sorter->sort();
      end_time = deceve::bama::rdtsc();
      COUT << "End sorter"
           << "\n";
      COUT << "SORT COST: " << end_time - start_time << "\n";

      if (!bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        //				ts.cost = end_time - start_time;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();

        bm->getSM().insertToPersistentStatistics(
            std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }
    }

    delete sorter;

    //	scan<PART>(output_full_path);

    /*
     * Delete generated file of the previous operation only if they don't exist
     * in the persisted file catalog
     * and it is not one of the main tables of the database
     */
    //		if (!bm->getStorageManager().isInTableCatalog(full_input_name)
    //				&&
    //! bm->getStorageManager().isInPersistedFileCatalog(
    //						leftNode->tk)) {
    //
    //			COUT
    //					<< "DELETE PREVIOUS file which is not in
    // table
    // catalog
    // and
    // is
    // not persistent so just delete it"
    //					<< "\n";
    //			bm->removeFile(full_input_name);
    //		}
  }
}
}
}
