/*
 * Query4Nodes.hh
 *
 *  Created on: 4 Jun 2015
 *      Author: mike
 */

#ifndef QUERY4NODES_HH_
#define QUERY4NODES_HH_

#include "../definitions.hh"
#include "../../utils/global.hh"

// Include base class
#include "../NodeTree.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

// Include Algorithm Classes
#include "../../algorithms/replacementsort.hh"
#include "../../algorithms/gracejoin.hh"
#include "../../algorithms/gracejoinConvertBoth.hh"

namespace deceve {
namespace queries {

//############### Comparators ###########################
struct comparatorQ41Left {
  //. DATE = 1993-07-01. 1997-10-01
  comparatorQ41Left() {
    srand(time(NULL));

    int randomYear = 1993 + rand() % 5;
    int randomMonth = rand() % 12 + 1;
    if (randomYear == 1997) randomMonth = rand() % 10 + 1;
    d1 = {randomYear, randomMonth, 1};
    d2 = d1.plus(3);

    // std::cout << "Date1: " << d1 << std::endl;
    // std::cout << "Date2: " << d2 << std::endl;
  }

  bool operator()(ORDER item) {
    date tmpdate(item.O_ORDERDATE);
    if (d1 <= tmpdate && tmpdate <= d2) {
      return true;
    }
    return false;
  }

  date d1;
  date d2;
};

struct comparatorQ41Right {
  bool operator()(LINEITEM item) {
    date d1(item.L_COMMITDATE);
    date d2(item.L_RECEIPTDATE);
    return d1 < d2;
  }
};

class Query4Node1 : public NodeTree {
 public:
  Query4Node1(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;
    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::ORDERS;
    right_tk.table_name = deceve::storage::LINEITEM;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::OTHER_FIELD;

    leftFileDetails(0.04, sizeof(ORDER), sizeof(Q4_PR_ORDER));
    rightFileDetails(0.63, sizeof(LINEITEM), sizeof(Q4_PR_LINEITEM));
    outputFileDetails(0.03, sizeof(Q4_JOIN1_STRUCT_ORDER_LINEITEM));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  //  deceve::bama::GraceJoin<ORDER, LINEITEM, column_extractor_order,
  //                          column_extractor_lineitem,
  //                          q4_join1_combinator_order_lineitem,
  //                          std::less<int>,
  //                          comparatorQ41Left, comparatorQ41Right> *joiner;

  deceve::bama::GraceJoinConvertBoth<
      ORDER, Q4_PR_ORDER, LINEITEM, Q4_PR_LINEITEM, column_extractor_order,
      column_extractor_lineitem, q4_converter_PR_ORDER,
      q4_converter_PR_LINEITEM, q4_extractor_PR_ORDER, q4_extractor_PR_LINEITEM,
      q4_combinator_pr_order_pr_lineitem, std::less<int>, comparatorQ41Left,
      comparatorQ41Right> *joiner;
};

class Query4Node2 : public NodeTree {
 public:
  Query4Node2(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    //    tk.table_name = "lineitem_group";
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query4Node3 : public NodeTree {
 public:
  Query4Node3(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    //    tk.table_name = "lineitem_group_sort";
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    //    tk.field = "q1_column_extractor";
    tk.field = deceve::storage::OTHER_FIELD;
    sorter = NULL;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);

 private:
  deceve::bama::ReplacementSort<Q4_GROUP_STRUCT_ORDER_LINEITEM,
                                q4_group_extractor_result> *sorter;
};
}
}

#endif /* QUERY4NODES_HH_ */
