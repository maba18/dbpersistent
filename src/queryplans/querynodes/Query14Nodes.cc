/*******************************************************************************
 * Copyright (c) 2015 Michail Basios and Stratis Viglas.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michail Basios - initial API and implementation
 *     Stratis Viglas - initial API and implementation
 *******************************************************************************/
/*
 * Query14Nodes.cc
 *
 *  Created on: Mar 12, 2015
 *      Author: maba18
 */

#include "Query14Nodes.hh"

namespace deceve {
namespace queries {

void Query14Node1::run(deceve::storage::BufferManager* bm,
                       const std::string& input_name,
                       const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  // COUT<< "Query14Node1 Execution (Select)" << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }
  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<LINEITEM> reader(bm, full_input_name,
                                        bm->getFileMode(full_input_name));

  deceve::bama::Writer<LINEITEM> res_writer(bm, output_full_path,
                                            deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  srand(time(NULL));

  int randomYear = 1993 + rand() % 5;
  randomYear = 1995;

  int randomMonth = 1 + rand() % 12;
  randomMonth = 9;

  date d1(randomYear, randomMonth, 1);
  date d2(randomYear, randomMonth + 1, 1);

  LINEITEM item;

  int counter = 0;
  while (reader.hasNext()) {
    item = reader.nextRecord();
    date l_shipdate(item.L_SHIPDATE);

    if (l_shipdate >= d1 && l_shipdate < d2) {
      res_writer.write(item);
      counter++;
    }
  }

  reader.close();
  res_writer.close();

  //############################ Delete Previous File
  //####################################
  // check if the input is a primary table and not an intermediate results
  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }
}

// void Query14Node2::run(deceve::storage::BufferManager* bm,
//                       const std::string& left_file,
//                       const std::string& right_file,
//                       const std::string& simple_output_name) {
//  //#################################### Construct full paths for left and
//  right
//  // file ######################################
//
//  std::string left;
//  buildLeftFilePath(left_file, left);
//
//  std::string right;
//  buildRightFilePath(right_file, right);
//
//  std::string output_full_path =
//  constructOutputTmpFullPath(simple_output_name);
//
//  column_extractor_lineitem left_extractor(0);
//  column_extractor_part right_extractor(0);
//  q14_combinator_part_line combinator;
//
//  //#################################### Calculate number of partitions and
//  // initialize joiner ##################################
//
//  // not used
//  size_t partitions = 1;
//
//  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;
//
//  if (check_PersistResult_flag) {
//    joinMode = deceve::storage::INTERMEDIATE;
//  }
//
//  // <LINEITEM, PART, column_extractor_lineitem,
//  // column_extractor_part, q14_combinator_part_line, std::less<int>,
//  // comparatorQ142Left, deceve::bama::DefaultComparator<PART> >
//
//  joiner = new deceve::bama::GraceJoin<
//      LINEITEM, PART, column_extractor_lineitem, column_extractor_part,
//      q14_combinator_part_line, std::less<int>, comparatorQ142Left,
//      deceve::bama::DefaultComparator<PART> >(bm, left, right,
//      output_full_path,
//                                              left_extractor, right_extractor,
//                                              combinator, partitions,
//                                              joinMode);
//
//  //#################################### Decide to persist or use persisted
//  // results ##################################
//  //#################################### Decide to persist or use persisted
//  // results ##################################
//  if (check_PersistResult_flag) {
//    if (result_id == 0) {
//      joiner->persistResult(key_about_left_persisted_file,
//                            key_about_right_persisted_file, result_id);
//    } else if (result_id == 1) {
//      joiner->persistResult(key_about_left_persisted_file, result_id);
//    } else {
//      joiner->persistResult(key_about_right_persisted_file, result_id);
//    }
//  }
//  if (check_usePersistedResult_flag) {
//    if (result_id == 0) {
//      joiner->usePersistedResult(information_about_left_persisted_file,
//                                 information_about_right_persisted_file,
//                                 result_id);
//      // Update Statistics
//      bm->getStorageManager().incrementPersistentStatisticsHits(left_tk);
//      bm->getStorageManager().incrementPersistentStatisticsHits(right_tk);
//
//    } else if (result_id == 1) {
//      joiner->usePersistedResult(information_about_left_persisted_file,
//                                 result_id);
//      // Update Statistics
//      bm->getStorageManager().incrementPersistentStatisticsHits(left_tk);
//
//    } else if (result_id == 2) {
//      joiner->usePersistedResult(information_about_right_persisted_file,
//                                 result_id);
//      // Update Statistics
//      bm->getStorageManager().incrementPersistentStatisticsHits(right_tk);
//    }
//  }
//
//  //#################################### Execute Join
//  //##################################
//  joiner->join();
//  //#################################### Add information to file persistent
//  // catalog ##################################
//
//  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
//  std::string runNamePrefixOfPersistentFile =
//      joiner->getRunNamePrefixOfPersistentFile();
//  int tmp_result_id = joiner->getResultId();
//
//  if (check_PersistResult_flag) {
//    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
//                       runNamePrefixOfPersistentFile);
//  }
//  delete joiner;
//
//  //############################ Add infomation about files to Statistics
//  Keeper
//  //####################################
//  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
//  //############################ Delete Previous Files
//  //####################################
//  deletePreviousFilesJoin(bm, left, right);
//}

void Query14Node2::run(deceve::storage::BufferManager* bm,
                       const std::string& left_file,
                       const std::string& right_file,
                       const std::string& simple_output_name) {
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_lineitem left_extractor(0);
  column_extractor_part right_extractor(0);
  //  q14_combinator_part_line combinator;
  q14_converter_PR_LINEITEM pr_converter;
  q14_extractor_PR_LINEITEM pr_extractor(0);

  q14_converter_PR_LINEITEM pr_left_converter;
  q14_extractor_PR_LINEITEM pr_left_extractor(0);

  q14_converter_PR_PART pr_right_converter;
  q14_extractor_PR_PART pr_right_extractor(0);

  q14_combinator_pr_lineitem_pr_part pr_final_combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  //  LINEITEM, PART, column_extractor_lineitem, column_extractor_part,
  //      q14_combinator_part_line, std::less<int>, comparatorQ142Left,
  //      deceve::bama::DefaultComparator<PART>

  //  joiner = new deceve::bama::GraceJoinConvertLeft<LINEITEM, PART,
  //  Q14_PR_LINEITEM, column_extractor_lineitem,
  //      column_extractor_part, q14_converter_PR_LINEITEM,
  //      q14_extractor_PR_LINEITEM, q14_combinator_pr_lineitem_pr_part,
  //      std::less<int>, deceve::bama::DefaultComparator<LINEITEM>,
  //      deceve::bama::DefaultComparator<PART> >(
  //      bm, left, right, output_full_path, left_extractor, right_extractor,
  //      pr_converter, pr_extractor, pr_combinator,
  //      partitions, joinMode, deceve::storage::LEFT_PROJECT);

  joiner = new deceve::bama::GraceJoinConvertBoth<
      LINEITEM, Q14_PR_LINEITEM, PART, Q14_PR_PART, column_extractor_lineitem,
      column_extractor_part, q14_converter_PR_LINEITEM, q14_converter_PR_PART,
      q14_extractor_PR_LINEITEM, q14_extractor_PR_PART,
      q14_combinator_pr_lineitem_pr_part, std::less<int>,
      deceve::bama::DefaultComparator<LINEITEM>,
      deceve::bama::DefaultComparator<PART> >(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      pr_left_converter, pr_right_converter, pr_left_extractor,
      pr_right_extractor, pr_final_combinator, partitions, joinMode,
      deceve::storage::BOTH_PROJECT);

  joiner->setJoinTableKeys(left_tk, right_tk);
  joiner->setStreamFlag(true);

  //#################################### Decide to persist or use persisted
  // results ##################################

  //#################################### Decide to persist or use persisted
  // results ##################################
  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);

  bm->removeFile(output_full_path);
}

void Query14Node3::run(deceve::storage::BufferManager* bm,
                       const std::string& input_name,
                       const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  // COUT<<"Query14Node3 Execution (Select)" << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }
  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<query14_struct> reader(bm, full_input_name,
                                              bm->getFileMode(full_input_name));

  //################################ OPERATION
  //########################################

  query14_struct record;
  double total_sum1 = 0;
  double total_sum2 = 0;
  double total_sum = 0;

  while (reader.hasNext()) {
    record = reader.nextRecord();

    double tmpSum1;
    double tmpSum2;

    if (strncmp(record.type, "PROMO", strlen("PROMO")) == 0) {
      tmpSum1 = record.extended_price * (1 - record.discount);
    } else {
      tmpSum1 = 0;
    }
    total_sum1 = total_sum1 + tmpSum1;

    tmpSum2 = record.extended_price * (1 - record.discount);

    total_sum2 = total_sum2 + tmpSum2;
  }
  total_sum = 100 * total_sum1 / total_sum2;

  (void)total_sum;

  reader.close();

  //############################ Delete Previous File
  //####################################
  // check if the input is a primary table and not an intermediate results
  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }

  //  bm->removeFile(output_full_path);
}

}  // namespace queries
}  // namespace deceve
