/*******************************************************************************
 * Copyright (c) 2015 Michail Basios and Stratis Viglas.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michail Basios - initial API and implementation
 *     Stratis Viglas - initial API and implementation
 *******************************************************************************/

#include "Query15Nodes.hh"

namespace deceve {
namespace queries {

void Query15Node1::run(deceve::storage::BufferManager* bm,
                       const std::string& input_name,
                       const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  // COUT << "Query15Node1 Execution (Create View)" << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }
  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<LINEITEM> reader(bm, full_input_name,
                                        bm->getFileMode(full_input_name));

  deceve::bama::Writer<q15_struct> res_writer(bm, output_full_path,
                                              deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  srand(time(NULL));

  int randomYear = 1993 + rand() % 5;

  int randomMonth = 1 + rand() % 12;

  if (randomYear == 1997) {
    randomMonth = 1 + rand() % 10;
  }

  date d1(randomYear, randomMonth, 1);
  date d2(randomYear, randomMonth + 3, 1);

  std::map<int, double> memoryMap;
  LINEITEM item;
  int counter = 0;
  while (reader.hasNext()) {
    item = reader.nextRecord();
    date l_shipdate(item.L_SHIPDATE);
    if (l_shipdate >= d1 && l_shipdate < d2) {
      std::map<int, double>::iterator it = memoryMap.find(item.L_SUPPKEY);
      if (it == memoryMap.end()) {
        memoryMap[item.L_SUPPKEY] =
            item.L_EXTENDEDPRICE * (1 - item.L_DISCOUNT);
      } else {
        it->second = it->second + item.L_EXTENDEDPRICE * (1 - item.L_DISCOUNT);
      }
      counter++;
    }
  }

  reader.close();
  q15_struct q15_output;
  for (std::map<int, double>::iterator it = memoryMap.begin();
       it != memoryMap.end(); ++it) {
    q15_output.supplier_no = it->first;
    q15_output.total_revenue = it->second;
    res_writer.write(q15_output);
  }

//  for (size_t i = 0; i < 1000; i++) {
//    q15_struct s;
//    s.supplier_no = i;
//    s.total_revenue = i * 10;
//  }

  res_writer.close();
}

double Query15Node2::q15_find_max_revenue(deceve::storage::BufferManager* bm,
                                          const std::string& input_name) {
  /* * select
   max(total_revenue)
   from
   revenue[STREAM_ID]
   )
   */
  deceve::bama::Reader<q15_struct> reader(bm, input_name,
                                          bm->getFileMode(input_name));
  double max = 0;
  q15_struct record;
  while (reader.hasNext()) {
    record = reader.nextRecord();
    if (record.total_revenue > max) {
      max = record.total_revenue;
    }
  }
  reader.close();
  return max;
}

void Query15Node2::run(deceve::storage::BufferManager* bm,
                       const std::string& input_name,
                       const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  // COUT << "Query15Node2 Execution (Max revenue)" << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }
  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<q15_struct> reader(bm, full_input_name,
                                          bm->getFileMode(full_input_name));

  deceve::bama::Writer<q15_struct> res_writer(bm, output_full_path,
                                              deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  double max_revenue = q15_find_max_revenue(bm, full_input_name);

  q15_struct record;
  while (reader.hasNext()) {
    record = reader.nextRecord();
    if (record.total_revenue == max_revenue) {
      res_writer.write(record);
    }
  }
  reader.close();
  res_writer.close();

  //############################ Delete Previous File
  //####################################
  // check if the input is a primary table and not an intermediate results

  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }
}

void Query15Node3::run(deceve::storage::BufferManager* bm,
                       const std::string& left_file,
                       const std::string& right_file,
                       const std::string& simple_output_name) {
  // COUT << "Query15Node3 Execution (Join)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_supplier left_extractor(0);
  q15_column_extractor_revenue right_extractor(0);
  q15_combinator_supplier_revenue combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  joiner = new deceve::bama::MergeSortJoin<
      SUPPLIER, q15_struct, column_extractor_supplier,
      q15_column_extractor_revenue, q15_combinator_supplier_revenue>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      combinator, partitions, joinMode, deceve::storage::LEFT_FILE_ONLY);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
  // bm->getStorageManager().printPersistentFileCatalog();
}

void Query15Node4::run(deceve::storage::BufferManager* bm,
                       const std::string& input_name,
                       const std::string& simple_output_name) {
  //####################### Sorting Template
  //####################################

  // COUT << "Query15Node4 Execution (Sort)" << "\n";

  /*
   * EXECUTE USING PERSISTED FILES
   */
  if (check_usePersistedResult_flag) {
    node_output_name = information_about_persisted_files.output_name;

    bm->getSM().incrementPersistentStatisticsHits(tk);

    // Inform next operator to unpin file on the persisted tmp catalog

  } else {
    //####################### Construct Filenames
    //####################################
    std::string full_input_name;

    if (input_file_path == deceve::storage::PRIMARY_PATH) {
      full_input_name = constructFullPath(input_name);
    } else {
      full_input_name = constructInputTmpFullPath(node_input_name);
    }

    std::string output_full_path =
        constructOutputTmpFullPath(simple_output_name);

    //################################ OPERATION
    //#####################################
    q15_column_extractor_final_struct extractor(0);

    deceve::storage::FileMode sorterMode = deceve::storage::PRIMARY;
    if (check_PersistResult_flag) {
      sorterMode = deceve::storage::INTERMEDIATE;
    }

    sorter =
        new deceve::bama::ReplacementSort<q15_struct_final_output,
                                          q15_column_extractor_final_struct>(
            bm, full_input_name, output_full_path, extractor, POOLSIZE,
            sorterMode);

    // EXECUTE AND PERSIST RESULTS
    if (check_PersistResult_flag) {
      sorter->persistResult();
      sorter->sort();

      if (!bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }

      // REMOVE FROM TMPFILESET
      bm->getSM().deleteFromPersistedTmpFileSet(tk);

      // ADD TO PERSISTED FILE CATALOG
      tv.num_files = sorter->getNumberOfRuns();
      tv.output_name = simple_output_name;
      tv.full_output_path = output_full_path;
      tv.pins = 0;
      bm->getSM().insertInPersistedFileCatalog(std::make_pair(tk, tv));

    } else {
      // SIMPLY EXECUTE
      sorter->sort();

      if (!bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }
    }

    delete sorter;

    // bm->getStorageManager().printPersistentFileCatalog();
    //############################ Delete Previous File
    //####################################
    if (!bm->getSM().isInTableCatalog(full_input_name) &&
        !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
      bm->removeFile(full_input_name);
    }

    // scan<q15_struct_final_output>(output_full_path);
    bm->removeFile(output_full_path);
  }
}
}  // namespace queries
}  // namespace deceve
