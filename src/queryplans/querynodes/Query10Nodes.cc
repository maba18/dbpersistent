/*
 * Query10Nodes.cc
 *
 *  Created on: 20 Mar 2015
 *      Author: michail
 */

#include "Query10Nodes.hh"

namespace deceve {
namespace queries {

void Query10Node0::run(deceve::storage::BufferManager* bm,
                       const std::string& input_name,
                       const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  //  COUT
  //      << "******************** -Query10Node0 Execution (Select Orders)-
  //      ********************"
  //      << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<ORDER> reader(bm, full_input_name,
                                     bm->getFileMode(full_input_name));

  deceve::bama::Writer<ORDER> res_writer(bm, output_full_path,
                                         deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  ORDER item;
  /* Generate random date and subtract it from the current date */
  srand(time(NULL));

  int randomYear = 1993 + rand() % 3;
  int randomMonth = rand() % 12 + 1;
  if (randomYear == 1995) randomMonth = 1;
  if (randomYear == 1993 && randomMonth == 1) randomMonth = 2;
  date d1(randomYear, randomMonth, 1);
  date d2(randomYear, randomMonth + 3, 1);

  while (reader.hasNext()) {
    item = reader.nextRecord();
    date tmpdate(item.O_ORDERDATE);
    if (d1 <= tmpdate && tmpdate <= d2) {
      res_writer.write(item);
    }
  }
  reader.close();
  res_writer.close();

  //############################ Delete Previous File
  //####################################
  // check if the input is a primary table and not an intermediate results
  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }
}

void Query10Node1::run(deceve::storage::BufferManager* bm,
                       const std::string& input_name,
                       const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  //  COUT
  //      << "******************** -Query10Node1 Execution (Select Lineitem)-
  //      ********************"
  //      << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<LINEITEM> reader(bm, full_input_name,
                                        bm->getFileMode(full_input_name));

  deceve::bama::Writer<LINEITEM> res_writer(bm, output_full_path,
                                            deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  LINEITEM item;
  /* Generate random date and subtract it from the current date */
  srand(time(NULL));

  while (reader.hasNext()) {
    item = reader.nextRecord();

    if (item.L_RETURNFLAG == 'R') {
      res_writer.write(item);
    }
  }
  reader.close();
  res_writer.close();

  //############################ Delete Previous File
  //####################################
  // check if the input is a primary table and not an intermediate results
  if (!bm->getSM().isInTableCatalog(full_input_name) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(full_input_name);
  }
}

void Query10Node2::run(deceve::storage::BufferManager* bm,
                       const std::string& left_file,
                       const std::string& right_file,
                       const std::string& simple_output_name) {
  //  COUT << "Query10Node1 Execution (Join <ORDER vs LINEITEM>)" << "\n";
  //#################################### Construct full paths for left and right

  std::string left;

  if (left_file_path == deceve::storage::INTERMEDIATE_PATH) {
    left = constructInputTmpFullPath(leftNode->node_output_name);
  } else {
    left = constructFullPath(left_file);
  }

  std::string right;
  if (right_file_path == deceve::storage::INTERMEDIATE_PATH) {
    right = constructInputTmpFullPath(rightNode->node_output_name);
  } else {
    right = constructFullPath(right_file);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_order left_extractor(1);
  column_extractor_lineitem right_extractor(1);
  // q10_combinator_order_lineitem combinator;

  q10_converter_PR_LINEITEM pr_right_converter;
  q10_extractor_PR_LINEITEM pr_right_extractor(0);

  q10_converter_PR_ORDER pr_left_converter;
  q10_extractor_PR_ORDER pr_left_extractor(0);

  q10_combinator_pr_order_pr_lineitem pr_final_combinator;

  // q10_combinator_order_pr_lineitem pr_combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  //  joiner =
  //      new deceve::bama::GraceJoin<ORDER, LINEITEM, column_extractor_order,
  //                                  column_extractor_lineitem,
  //                                  q10_combinator_order_lineitem,
  //                                  std::less<int>,
  //                                  comparatorQ102Left, comparatorQ102Right>(
  //          bm, left, right, output_full_path, left_extractor,
  //          right_extractor,
  //          combinator, partitions, joinMode);

  //  joiner = new deceve::bama::GraceJoinConvert<ORDER, LINEITEM,
  //  Q10_PR_LINEITEM, column_extractor_order,
  //      column_extractor_lineitem, q10_converter_PR_LINEITEM,
  //      q10_extractor_PR_LINEITEM, q10_combinator_order_pr_lineitem,
  //      std::less<int>, comparatorQ102Left, comparatorQ102Right>(bm, left,
  //      right, output_full_path, left_extractor,
  //                                                               right_extractor,
  //                                                               pr_right_converter,
  //                                                               pr_right_extractor,
  //                                                               pr_combinator,
  //                                                               partitions,
  //                                                               joinMode,
  //                                                               deceve::storage::RIGHT_PROJECT);

  joiner = new deceve::bama::GraceJoinConvertBoth<
      ORDER, Q10_PR_ORDER, LINEITEM, Q10_PR_LINEITEM, column_extractor_order,
      column_extractor_lineitem, q10_converter_PR_ORDER,
      q10_converter_PR_LINEITEM, q10_extractor_PR_ORDER,
      q10_extractor_PR_LINEITEM, q10_combinator_pr_order_pr_lineitem,
      std::less<int>, comparatorQ102Left, comparatorQ102Right>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      pr_left_converter, pr_right_converter, pr_left_extractor,
      pr_right_extractor, pr_final_combinator, partitions, joinMode,
      deceve::storage::BOTH_PROJECT);

  joiner->setJoinTableKeys(left_tk, right_tk);


  //#################################### Decide to persist or use persisted
  // results ##################################

  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  if (check_PersistResult_flag) {
    bm->getSM().deleteFromPersistedTmpFileSet(tk);

    int tmp_result_id = joiner->getResultId();
    if (tmp_result_id == 0) {
      left_tv.num_files = joiner->getNumberOfPartitions();
      right_tv.num_files = joiner->getNumberOfPartitions();

      left_tv.output_name = left;
      right_tv.output_name = right;

      left_tv.full_output_path = joiner->getRunNamePrefixOfPersistentFile();
      right_tv.full_output_path = joiner->getRunNamePrefixOfPersistentFile();

      bm->getSM().insertInPersistedFileCatalog(
          std::make_pair(left_tk, left_tv));
      bm->getSM().insertInPersistedFileCatalog(
          std::make_pair(right_tk, right_tv));

      if (!bm->getSM().isContainedInPersistentStatistics(left_tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(left_tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();

        bm->getSM().insertToPersistentStatistics(std::make_pair(left_tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(left_tk);
      }

      if (!bm->getSM().isContainedInPersistentStatistics(right_tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(right_tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();

        bm->getSM().insertToPersistentStatistics(std::make_pair(right_tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(right_tk);
      }

      bm->getSM().deleteFromPersistedTmpFileSet(left_tk);
      bm->getSM().deleteFromPersistedTmpFileSet(right_tk);
    } else if (tmp_result_id == 1) {
      left_tv.num_files = joiner->getNumberOfPartitions();
      left_tv.output_name = left;
      left_tv.full_output_path = joiner->getRunNamePrefixOfPersistentFile();

      bm->getSM().insertInPersistedFileCatalog(
          std::make_pair(left_tk, left_tv));

      if (!bm->getSM().isContainedInPersistentStatistics(left_tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(left_tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();

        bm->getSM().insertToPersistentStatistics(std::make_pair(left_tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(left_tk);
      }

      bm->getSM().deleteFromPersistedTmpFileSet(left_tk);

    } else if (tmp_result_id == 2) {
      right_tv.num_files = joiner->getNumberOfPartitions();
      right_tv.output_name = right;
      right_tv.full_output_path = joiner->getRunNamePrefixOfPersistentFile();
      bm->getSM().insertInPersistedFileCatalog(
          std::make_pair(right_tk, right_tv));

      if (!bm->getSM().isContainedInPersistentStatistics(right_tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(right_tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();

        bm->getSM().insertToPersistentStatistics(std::make_pair(right_tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(right_tk);
      }

      bm->getSM().deleteFromPersistedTmpFileSet(right_tk);
    }
  }
  delete joiner;

  //############################ Add infomation about files to Statistics
  // Keeper
  //####################################
  deletePreviousFilesJoin(bm, left, right);

  bm->removeFile(output_full_path);

}

void Query10Node3::run(deceve::storage::BufferManager* bm,
                       const std::string& left_file,
                       const std::string& right_file,
                       const std::string& simple_output_name) {
  //  COUT << "Query9Node6 Execution (Join <RESULT vs CUSTOMER>)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;

  if (left_file_path == deceve::storage::INTERMEDIATE_PATH) {
    left = constructInputTmpFullPath(leftNode->node_output_name);
  } else {
    left = constructFullPath(left_file);
  }

  std::string right;
  if (right_file_path == deceve::storage::INTERMEDIATE_PATH) {
    right = constructInputTmpFullPath(rightNode->node_output_name);
  } else {
    right = constructFullPath(right_file);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  q10_join2_extractor_result left_extractor(0);
  column_extractor_customer right_extractor(0);
  q10_combinator_result_customer combinator;

  q10_converter_PR_CUSTOMER_SORT pr_right_converter;
  q10_extractor_PR_CUSTOMER_SORT pr_right_extractor(0);
  // q10_combinator_result_pr_sort_customer pr_combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  joiner = new deceve::bama::MergeSortJoin<
      Q10_JOIN1_STRUCT_ORDER_LINEITEM, CUSTOMER, q10_join2_extractor_result,
      column_extractor_customer, q10_combinator_result_customer>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      combinator, 1000, joinMode, deceve::storage::LEFT_FILE_ONLY);

  //  joiner = new
  //  deceve::bama::MergeSortJoinConvertRight<Q10_JOIN1_STRUCT_ORDER_LINEITEM,
  //  CUSTOMER, Q10_PR_CUSTOMER_SORT,
  //      q10_join2_extractor_result, column_extractor_customer,
  //      q10_extractor_PR_CUSTOMER_SORT,
  //      q10_converter_PR_CUSTOMER_SORT,
  //      q10_combinator_result_pr_sort_customer>(bm, left, right,
  //      output_full_path,
  //                                                                              left_extractor, right_extractor,
  //                                                                              pr_right_extractor, pr_right_converter,
  //                                                                              pr_combinator, 1000, joinMode,
  //                                                                              deceve::storage::LEFT_FILE_ONLY);

  //#################################### Decide to persist or use persisted
  // results ##################################

  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  if (check_PersistResult_flag) {
    bm->getSM().deleteFromPersistedTmpFileSet(tk);

    int tmp_result_id = joiner->getResultId();
    if (tmp_result_id == 0) {
      left_tv.num_files = joiner->getNumberOfPartitions();
      right_tv.num_files = joiner->getNumberOfPartitions();

      left_tv.output_name = left;
      right_tv.output_name = right;

      left_tv.full_output_path = joiner->getRunNamePrefixOfPersistentFile();
      right_tv.full_output_path = joiner->getRunNamePrefixOfPersistentFile();
      bm->getSM().insertInPersistedFileCatalog(
          std::make_pair(left_tk, left_tv));
      bm->getSM().insertInPersistedFileCatalog(
          std::make_pair(right_tk, right_tv));

      if (!bm->getSM().isContainedInPersistentStatistics(left_tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(left_tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();

        bm->getSM().insertToPersistentStatistics(std::make_pair(left_tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(left_tk);
      }

      if (!bm->getSM().isContainedInPersistentStatistics(right_tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(right_tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();

        bm->getSM().insertToPersistentStatistics(std::make_pair(right_tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(right_tk);
      }

      bm->getSM().deleteFromPersistedTmpFileSet(left_tk);
      bm->getSM().deleteFromPersistedTmpFileSet(right_tk);
    } else if (tmp_result_id == 1) {
      left_tv.num_files = joiner->getNumberOfPartitions();
      left_tv.output_name = left;
      left_tv.full_output_path = joiner->getRunNamePrefixOfPersistentFile();

      bm->getSM().insertInPersistedFileCatalog(
          std::make_pair(left_tk, left_tv));

      if (!bm->getSM().isContainedInPersistentStatistics(left_tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(left_tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();

        bm->getSM().insertToPersistentStatistics(std::make_pair(left_tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(left_tk);
      }

      bm->getSM().deleteFromPersistedTmpFileSet(left_tk);

    } else if (tmp_result_id == 2) {
      right_tv.num_files = joiner->getNumberOfPartitions();
      right_tv.output_name = right;
      right_tv.full_output_path = joiner->getRunNamePrefixOfPersistentFile();
      bm->getSM().insertInPersistedFileCatalog(
          std::make_pair(right_tk, right_tv));

      if (!bm->getSM().isContainedInPersistentStatistics(right_tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(right_tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();

        bm->getSM().insertToPersistentStatistics(std::make_pair(right_tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(right_tk);
      }

      bm->getSM().deleteFromPersistedTmpFileSet(right_tk);
    }
  }
  delete joiner;

  //############################ Delete Previous File
  //####################################
  if (!bm->getSM().isInTableCatalog(left) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(left);
  }

  if (!bm->getSM().isInTableCatalog(right) &&
      !bm->getSM().isInPersistedFileCatalog(rightNode->tk)) {
    bm->removeFile(right);
  }
}

void Query10Node4::run(deceve::storage::BufferManager* bm,
                       const std::string& left_file,
                       const std::string& right_file,
                       const std::string& simple_output_name) {
  // COUT << "Query10Node4 Execution (Join <RESULT vs NATION>)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;

  if (left_file_path == deceve::storage::INTERMEDIATE_PATH) {
    left = constructInputTmpFullPath(leftNode->node_output_name);
  } else {
    left = constructFullPath(left_file);
  }

  std::string right;
  if (right_file_path == deceve::storage::INTERMEDIATE_PATH) {
    right = constructInputTmpFullPath(rightNode->node_output_name);
  } else {
    right = constructFullPath(right_file);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  q10_join3_extractor_result left_extractor(0);
  column_extractor_nation right_extractor(1);
  q10_combinator_result_nation combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  joiner = new deceve::bama::MergeSortJoin<
      Q10_JOIN2_STRUCT_RESULT_CUSTOMER, NATION, q10_join3_extractor_result,
      column_extractor_nation, q10_combinator_result_nation>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      combinator, POOLSIZE, joinMode, deceve::storage::LEFT_FILE_ONLY);

  //#################################### Decide to persist or use persisted
  // results ##################################

  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  if (check_PersistResult_flag) {
    bm->getSM().deleteFromPersistedTmpFileSet(tk);

    int tmp_result_id = joiner->getResultId();
    if (tmp_result_id == 0) {
      left_tv.num_files = joiner->getNumberOfPartitions();
      right_tv.num_files = joiner->getNumberOfPartitions();

      left_tv.output_name = left;
      right_tv.output_name = right;

      left_tv.full_output_path = joiner->getRunNamePrefixOfPersistentFile();
      right_tv.full_output_path = joiner->getRunNamePrefixOfPersistentFile();

      bm->getSM().insertInPersistedFileCatalog(
          std::make_pair(left_tk, left_tv));
      bm->getSM().insertInPersistedFileCatalog(
          std::make_pair(right_tk, right_tv));

      if (!bm->getSM().isContainedInPersistentStatistics(left_tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(left_tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();

        bm->getSM().insertToPersistentStatistics(std::make_pair(left_tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(left_tk);
      }

      if (!bm->getSM().isContainedInPersistentStatistics(right_tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(right_tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();

        bm->getSM().insertToPersistentStatistics(std::make_pair(right_tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(right_tk);
      }

      bm->getSM().deleteFromPersistedTmpFileSet(left_tk);
      bm->getSM().deleteFromPersistedTmpFileSet(right_tk);
    } else if (tmp_result_id == 1) {
      left_tv.num_files = joiner->getNumberOfPartitions();
      left_tv.output_name = left;
      left_tv.full_output_path = joiner->getRunNamePrefixOfPersistentFile();

      bm->getSM().insertInPersistedFileCatalog(
          std::make_pair(left_tk, left_tv));

      if (!bm->getSM().isContainedInPersistentStatistics(left_tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(left_tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();

        bm->getSM().insertToPersistentStatistics(std::make_pair(left_tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(left_tk);
      }

      bm->getSM().deleteFromPersistedTmpFileSet(left_tk);

    } else if (tmp_result_id == 2) {
      right_tv.num_files = joiner->getNumberOfPartitions();
      right_tv.output_name = right;
      right_tv.full_output_path = joiner->getRunNamePrefixOfPersistentFile();
      bm->getSM().insertInPersistedFileCatalog(
          std::make_pair(right_tk, right_tv));

      if (!bm->getSM().isContainedInPersistentStatistics(right_tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(right_tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();

        bm->getSM().insertToPersistentStatistics(std::make_pair(right_tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(right_tk);
      }

      bm->getSM().deleteFromPersistedTmpFileSet(right_tk);
    }
  }
  delete joiner;
  //
  //############################ Delete Previous File
  //####################################
  if (!bm->getSM().isInTableCatalog(left) &&
      !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
    bm->removeFile(left);
  }

  if (!bm->getSM().isInTableCatalog(right) &&
      !bm->getSM().isInPersistedFileCatalog(rightNode->tk)) {
    bm->removeFile(right);
  }

  bm->removeFile(output_full_path);
}
}  // namespace queries
}  // namespace deceve
