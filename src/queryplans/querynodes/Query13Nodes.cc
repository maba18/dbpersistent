/*
 * Query13Nodes.cc
 *
 *  Created on: 8 Oct 2015
 *      Author: mike
 */

#include "Query13Nodes.hh"
namespace deceve {
namespace queries {
void Query13Node1::run(deceve::storage::BufferManager* bm,
                       const std::string& left_file,
                       const std::string& right_file,
                       const std::string& simple_output_name) {
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_customer left_extractor(0);
  column_extractor_order right_extractor(0);
  // q13_join1_combinator_customer_order combinator;

  q13_converter_PR_ORDER_SORT pr_right_converter;
  q13_extractor_PR_ORDER_SORT pr_right_extractor(0);
  q13_combinator_customer_pr_sort_order pr_combinator;
  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  // <LINEITEM, PART, column_extractor_lineitem,
  // column_extractor_part, q14_combinator_part_line, std::less<int>,
  // comparatorQ142Left, deceve::bama::DefaultComparator<PART> >

  //  joiner = new deceve::bama::MergeSortJoin<
  //      CUSTOMER, ORDER, column_extractor_customer, column_extractor_order,
  //      q13_join1_combinator_customer_order>(
  //      bm, left, right, output_full_path, left_extractor, right_extractor,
  //      combinator, partitions, joinMode, deceve::storage::RIGHT_FILE_ONLY);

  joiner = new deceve::bama::MergeSortJoinConvertRight<
      CUSTOMER, ORDER, Q13_PR_ORDER_SORT, column_extractor_customer,
      column_extractor_order, q13_extractor_PR_ORDER_SORT,
      q13_converter_PR_ORDER_SORT, q13_combinator_customer_pr_sort_order>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      pr_right_extractor, pr_right_converter, pr_combinator, partitions,
      joinMode, deceve::storage::RIGHT_FILE_ONLY);

  joiner->setJoinTableKeys(left_tk, right_tk);

  //    joiner = new deceve::bama::GraceJoin<
  //        CUSTOMER, ORDER, column_extractor_customer, column_extractor_order,
  //        q13_join1_combinator_customer_order>(
  //        bm, left, right, output_full_path, left_extractor, right_extractor,
  //        combinator, partitions, joinMode);

  //#################################### Decide to persist or use persisted
  // results ##################################
  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
}

void Query13Node2::run(deceve::storage::BufferManager* bm,
                       const std::string& input_name,
                       const std::string& simple_output_name) {
  //####################### Sorting Template
  //####################################

  //  COUT
  //      << "******************** -Query1Node3 Execution (Sort)-
  //      ********************"
  //      << "\n";
  std::string output_full_path;
  /*
   * EXECUTE USING PERSISTED FILES
   */
  if (check_usePersistedResult_flag) {
    node_output_name = information_about_persisted_files.output_name;
    bm->getSM().incrementPersistentStatisticsHits(tk);
  } else {
    //####################### Construct Filenames
    //####################################
    std::string full_input_name;

    if (input_file_path == deceve::storage::PRIMARY_PATH) {
      full_input_name = constructFullPath(input_name);
    } else {
      full_input_name = constructInputTmpFullPath(node_input_name);
    }

    output_full_path = constructOutputTmpFullPath(simple_output_name);

    //################################ OPERATION
    //#####################################
    q13_extractor_final_result extractor(0);

    deceve::storage::FileMode sorterMode = deceve::storage::PRIMARY;
    if (check_PersistResult_flag) {
      sorterMode = deceve::storage::INTERMEDIATE;
    }

    sorter = new deceve::bama::ReplacementSort<QUERY13_STRUCT_CUSTOMER_ORDER,
                                               q13_extractor_final_result>(
        bm, full_input_name, output_full_path, extractor, POOLSIZE, sorterMode);

    // EXECUTE AND PERSIST RESULTS
    if (check_PersistResult_flag) {
      sorter->persistResult();
      sorter->sort();

      if (!bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }

      // REMOVE FROM TMPFILESET
      bm->getSM().deleteFromPersistedTmpFileSet(tk);

      // ADD TO PERSISTED FILE CATALOG
      tv.num_files = sorter->getNumberOfRuns();
      tv.output_name = simple_output_name;
      tv.full_output_path = output_full_path;
      tv.pins = 0;
      bm->getSM().insertInPersistedFileCatalog(std::make_pair(tk, tv));
    } else {
      // SIMPLY EXECUTE
      sorter->sort();

      if (tk.version == deceve::storage::SORT &&
          !bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }
    }

    delete sorter;
    //############################ Delete Previous File
    //####################################
    if (!bm->getSM().isInTableCatalog(full_input_name) &&
        !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
      bm->removeFile(full_input_name);
    }
  }

  bm->removeFile(output_full_path);
}
}
}
