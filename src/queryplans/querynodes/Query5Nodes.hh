/*
 * Query5Nodes.hh
 *
 *  Created on: Mar 13, 2015
 *      Author: maba18
 */

#ifndef QUERY5NODES_HH_
#define QUERY5NODES_HH_

// Include General Functions
#include "../definitions.hh"
#include "../../utils/global.hh"

// Include base class
#include "../NodeTree.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

// Include Algorithm Classes
#include "../../algorithms/replacementsort.hh"
#include "../../algorithms/gracejoin.hh"

#include "../../algorithms/gracejoinConvertRight.hh"

namespace deceve {
namespace queries {

struct comparatorQ54Right {
  comparatorQ54Right() {
    srand(time(NULL));
    int randomYear = 1993 + rand() % 5;
    date d1{randomYear, 1, 1};
    date d2{randomYear + 1, 1, 1};
  }

  bool operator()(ORDER item) {
    date tmpdate(item.O_ORDERDATE);
    if (d1 <= tmpdate && tmpdate < d2) {
      return true;
    }
    return false;
  }

  date d1;
  date d2;
};

class Query5Node1 : public NodeTree {
 public:
  Query5Node1(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    // name field
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

//    node2 = new Query5Node2(bm, NATION_PATH, "nothing");
class Query5Node2 : public NodeTree {
 public:
  Query5Node2(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    tk.version = deceve::storage::HASHJOIN;

    //		left_tk.table_name = "nation";
    //		right_tk.table_name = "region";
    //		left_tk.field = "N_REGIONKEY";
    //		right_tk.field = "R_REGIONKEY";
    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::OTHER_TABLENAME;
    right_tk.table_name = deceve::storage::OTHER_TABLENAME;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::OTHER_FIELD;

    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::GraceJoin<NATION, REGION, column_extractor_nation,
                          column_extractor_region,
                          q5_join1_combinator_nation_region> *joiner;
};

// node3 = new Query5Node3(bm, "nothing", CUSTOMER_PATH);  // RIGHT AUXILIARY
class Query5Node3 : public NodeTree {
 public:
  Query5Node3(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
    tk.version = deceve::storage::HASHJOIN;

    left_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::NATION;
    left_tk.field = deceve::storage::OTHER_FIELD;

    right_tk.version = deceve::storage::HASHJOIN;
    right_tk.table_name = deceve::storage::CUSTOMER;
    right_tk.field = deceve::storage::C_NATIONKEY;

    leftFileDetails(1, sizeof(Q5_JOIN1_STRUCT_REGION_NATION),
                    sizeof(Q5_JOIN1_STRUCT_REGION_NATION));
    rightFileDetails(1, sizeof(CUSTOMER), sizeof(Q5_PR_CUSTOMER));
    outputFileDetails(0, sizeof(Q5_JOIN2_STRUCT_RESULT1_CUSTOMER));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  //  deceve::bama::GraceJoin<Q5_JOIN1_STRUCT_REGION_NATION, CUSTOMER,
  //  q5_join2_extractor_result_of_1,
  //      column_extractor_customer, q5_join2_combinator_result_of_1_customer>
  //      *joiner1;

  deceve::bama::GraceJoinConvertRight<
      Q5_JOIN1_STRUCT_REGION_NATION, CUSTOMER, Q5_PR_CUSTOMER,
      q5_join2_extractor_result_of_1, column_extractor_customer,
      q5_converter_PR_CUSTOMER, q5_extractor_PR_CUSTOMER,
      q5_combinator_other_pr_customer> *joiner;
};

class Query5Node4 : public NodeTree {
 public:
  Query5Node4(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    //		tk.field = "O_ORDERDATE";
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

//    node5 = new Query5Node5(bm, "nothing", ORDERS_PATH);
class Query5Node5 : public NodeTree {
 public:
  Query5Node5(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
    tk.version = deceve::storage::HASHJOIN;
    //		left_tk.table_name = "result_left_2";
    //		right_tk.table_name = "orders_filtered";
    //		left_tk.field = "C_CUSTKEY";
    //		right_tk.field = "O_CUSTKEY";

    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::OTHER_TABLENAME;
    right_tk.table_name = deceve::storage::ORDERS;
    left_tk.field = deceve::storage::C_CUSTKEY;
    right_tk.field = deceve::storage::OTHER_FIELD;

    leftFileDetails(1, sizeof(Q5_JOIN2_STRUCT_RESULT1_CUSTOMER),
                    sizeof(Q5_JOIN2_STRUCT_RESULT1_CUSTOMER), 0.001);
    rightFileDetails(1, sizeof(ORDER), sizeof(Q5_PR_ORDER));
    outputFileDetails(0, sizeof(Q5_JOIN2_STRUCT_RESULT1_CUSTOMER));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  //  deceve::bama::GraceJoin<Q5_JOIN2_STRUCT_RESULT1_CUSTOMER, ORDER,
  //  q5_join3_extractor_result_of_2,
  //      column_extractor_order, q5_join3_combinator_result_of_2_order,
  //      std::less<int>,
  //      deceve::bama::DefaultComparator<Q5_JOIN2_STRUCT_RESULT1_CUSTOMER>,
  //      comparatorQ54Right> *joiner;

  deceve::bama::GraceJoinConvertRight<
      Q5_JOIN2_STRUCT_RESULT1_CUSTOMER, ORDER, Q5_PR_ORDER,
      q5_join3_extractor_result_of_2, column_extractor_order,
      q5_converter_PR_ORDER, q5_extractor_PR_ORDER,
      q5_combinator_other_pr_order> *joiner;
};

// Query5Node6(bm, "nothing", LINE_ITEM_PATH);  // RIGHT AUXILIARY, PROJECTED
class Query5Node6 : public NodeTree {
 public:
  Query5Node6(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;
    //		left_tk.table_name = "result_left_3";
    //		right_tk.table_name = "lineitem";
    //		left_tk.field = "O_ORDERKEY";
    //		right_tk.field = "L_ORDERKEY";
    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::HASHJOIN;
    left_tk.table_name = deceve::storage::OTHER_TABLENAME;
    right_tk.table_name = deceve::storage::LINEITEM;
    left_tk.field = deceve::storage::O_ORDERKEY;
    right_tk.field = deceve::storage::L_ORDERKEY;
    //    right_tk.field = bm->getStorageManager().getRandomOrderKeyField(0);

#ifdef UNIFIED
    right_tk.projected_field = deceve::storage::LINEITEM_UNIFIED_ORDER_KEY;
#else
    right_tk.projected_field = deceve::storage::LINEITEM_Q5;
#endif


    leftFileDetails(0, sizeof(Q5_JOIN3_STRUCT_RESULT2_ORDER),
                    sizeof(Q5_JOIN3_STRUCT_RESULT2_ORDER), 0.001);
    rightFileDetails(1, sizeof(LINEITEM), sizeof(Q5_PR_LINEITEM));
    outputFileDetails(0, sizeof(Q5_JOIN4_STRUCT_RESULT3_ORDER));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::GraceJoinConvertRight<
      Q5_JOIN3_STRUCT_RESULT2_ORDER, LINEITEM, Q5_PR_LINEITEM,
      q5_join4_extractor_result_of_3, column_extractor_lineitem,
      q5_converter_PR_LINEITEM, q5_extractor_PR_LINEITEM,
      q5_combinator_part_pr_lineitem> *joiner;

  //  deceve::bama::GraceJoinConvert<PART, LINEITEM, Q9_PR_LINEITEM,
  //  column_extractor_part, column_extractor_lineitem,
  //      q9_converter_PR_LINEITEM, q9_extractor_PR_LINEITEM,
  //      q9_combinator_part_pr_lineitem, std::less<int>,
  //      comparatorQ91Left, deceve::bama::DefaultComparator<LINEITEM> >
  //      *joiner;
};

class Query5Node7 : public NodeTree {
 public:
  Query5Node7(deceve::storage::BufferManager *bmr, const std::string &l_name,
              const std::string &r_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    tk.version = deceve::storage::HASHJOIN;
    //		left_tk.table_name = "result_left_4";
    //		right_tk.table_name = "supplier";
    //		left_tk.field = "L_SUPPKEY";
    //		right_tk.field = "S_SUPPKEY";
    left_tk.version = deceve::storage::OTHER;
    // SUPPLIER_USAGE
    //    right_tk.version = deceve::storage::HASHJOIN;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::OTHER_TABLENAME;
    right_tk.table_name = deceve::storage::SUPPLIER;
    left_tk.field = deceve::storage::L_SUPPKEY;
    right_tk.field = deceve::storage::S_SUPPKEY;

    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::GraceJoin<Q5_JOIN4_STRUCT_RESULT3_ORDER, SUPPLIER,
                          q5_join5_extractor_result_of_4,
                          column_extractor_supplier,
                          q5_join5_combinator_result_of_4_supplier> *joiner;
};

class Query5Node8 : public NodeTree {
 public:
  Query5Node8(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    //		tk.table_name = "group";
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query5Node9 : public NodeTree {
 public:
  Query5Node9(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    //    tk.table_name = "lineitem_group_sort";
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    //    tk.field = "q1_column_extractor";
    tk.field = deceve::storage::OTHER_FIELD;
    sorter = NULL;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);

 private:
  deceve::bama::ReplacementSort<Q5_JOIN5_STRUCT_RESULT4_SUPPLIER,
                                q5_join5_extractor_result_of_5> *sorter;
};
}
} /* namespace deceve */

#endif /* QUERY5NODES_HH_ */
