/*
 * Query18Nodes.hh
 *
 *  Created on: 14 Oct 2015
 *      Author: mike
 */

#ifndef QUERYPLANS_QUERYNODES_QUERY18NODES_HH_
#define QUERYPLANS_QUERYNODES_QUERY18NODES_HH_

// Include General Functions
#include "../definitions.hh"
#include "../../utils/global.hh"

#include <sstream>

// Include base class
#include "../NodeTree.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

// Include Algorithm Classes
#include "../../algorithms/replacementsort.hh"
#include "../../algorithms/gracejoin.hh"
#include "../../algorithms/gracejoinConvertRight.hh"
#include "../../algorithms/gracejoinConvertRight.hh"
#include "../../algorithms/partitionMapReduce.hh"
#include "../../algorithms/gracejoinMapReduce.hh"
#include "../../storage/identity.hh"

#include "../../algorithms/transformer.hh"

namespace deceve {
namespace queries {

struct comparatorQ181Left {
  comparatorQ181Left() {
    //    containers = {"STANDARD ANODIZED TIN",  "SMALL BURNISHED NICKEL",
    //                  "MEDIUM PLATED BRASS",    "LARGE POLISHED STEEL",
    //                  "ECONOMY BRUSHED COPPER", "PROMO"};

    containers1 = {"SM", "LG", "MED", "JUMBO", "WRAP"};

    containers2 = {"CASE", "BOX", "BAG", "JAR", "PKG", "PACK", "CAN", "DRUM"};

    srand(time(NULL));
    int N = 1 + rand() % 5;
    int M = 1 + rand() % 5;
    std::stringstream s;
    s << "BRAND#" << N << M;
    brand = s.str();
    randomIndex = rand() % containers1.size();

    std::stringstream c;
    c << containers1[randomIndex] << " ";
    randomIndex = rand() % containers2.size();
    c << containers2[randomIndex];
    container = c.str();
  }

  bool operator()(PART item) {
    std::string containerTuple = string(item.P_CONTAINER);
    std::string brandnameTuple = string(item.P_BRAND);

    if (containerTuple == container && brandnameTuple == brand) {
      return true;
    }
    return false;
  }

  std::vector<std::string> containers1;
  std::vector<std::string> containers2;
  std::string brand;
  std::string container;
  int randomIndex;
};

class Query18Node0 : public NodeTree {
 public:
  double fRandomDouble(double fMin, double fMax) {
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
  }

  Query18Node0(deceve::storage::BufferManager *bmr,
               const std::string &input_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
    tk.table_name = deceve::storage::LINEITEM;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;

    outputFileDetails(0.15, sizeof(int));
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query18Node1 : public NodeTree {
 public:
  Query18Node1(deceve::storage::BufferManager *bmr, const std::string &l_name,
               const std::string &r_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;

    left_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::LINEITEM;
    left_tk.field = deceve::storage::OTHER_FIELD;

    right_tk.version = deceve::storage::HASHJOIN;
    right_tk.table_name = deceve::storage::ORDERS;
    right_tk.field = deceve::storage::O_ORDERKEY;

#ifdef UNIFIED
    right_tk.projected_field = deceve::storage::ORDER_UNIFIED_ORDER_KEY;
#else
    right_tk.projected_field = deceve::storage::ORDER_Q18;
#endif

    leftFileDetails(1, sizeof(int), sizeof(int));
    rightFileDetails(1, sizeof(ORDER), sizeof(Q18_PR_ORDER));
    outputFileDetails(0, sizeof(Q18_JOIN1_STRUCT_INT_ORDER));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::GraceJoinConvertRight<
      int, ORDER, Q18_PR_ORDER, q18_extractor_int, column_extractor_order,
      q18_converter_PR_ORDER, q18_extractor_PR_ORDER,
      q18_combinator_int_pr_orders> *joiner;

  //  deceve::bama::GraceJoinConvertRight<
  //      Q18_JOIN2_STRUCT_CUSTOMER_RESULT, LINEITEM, Q18_PR_LINEITEM,
  //      q18_extractor_join2_result, column_extractor_lineitem,
  //      q18_converter_PR_LINEITEM, q18_extractor_PR_LINEITEM,
  //      q18_combinator_part_pr_lineitem> *joiner;
};

class Query18Node2 : public NodeTree {
 public:
  Query18Node2(deceve::storage::BufferManager *bmr, const std::string &l_name,
               const std::string &r_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
    tk.version = deceve::storage::HASHJOIN;

    left_tk.version = deceve::storage::HASHJOIN;
    left_tk.table_name = deceve::storage::CUSTOMER;
    left_tk.field = deceve::storage::C_CUSTKEY;

    right_tk.version = deceve::storage::OTHER;
    right_tk.table_name = deceve::storage::ORDERS;
    right_tk.field = deceve::storage::OTHER_FIELD;

    leftFileDetails(1, sizeof(CUSTOMER), sizeof(CUSTOMER));
    rightFileDetails(0, sizeof(Q18_JOIN1_STRUCT_INT_ORDER),
                     sizeof(Q18_JOIN1_STRUCT_INT_ORDER));
    outputFileDetails(0, sizeof(Q18_JOIN1_STRUCT_INT_ORDER));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::GraceJoin<CUSTOMER, Q18_JOIN1_STRUCT_INT_ORDER,
                          column_extractor_customer, q18_extractor_join1_result,
                          q18_combinator_join2_customer_result> *joiner;
};

class Query18Node3 : public NodeTree {
 public:
  Query18Node3(deceve::storage::BufferManager *bmr, const std::string &l_name,
               const std::string &r_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
    tk.version = deceve::storage::HASHJOIN;

    left_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::CUSTOMER;
    left_tk.field = deceve::storage::OTHER_FIELD;

    right_tk.version = deceve::storage::HASHJOIN;
    right_tk.table_name = deceve::storage::LINEITEM;
    right_tk.field = deceve::storage::L_ORDERKEY;

//    right_tk.field = bm->getStorageManager().getRandomOrderKeyField(1);

#ifdef UNIFIED
    right_tk.projected_field = deceve::storage::LINEITEM_UNIFIED_ORDER_KEY;
#else
    right_tk.projected_field = deceve::storage::LINEITEM_Q18;
#endif

    leftFileDetails(0, sizeof(Q18_JOIN2_STRUCT_CUSTOMER_RESULT),
                    sizeof(Q18_JOIN2_STRUCT_CUSTOMER_RESULT));
    rightFileDetails(1, sizeof(LINEITEM), sizeof(Q18_PR_LINEITEM));
    outputFileDetails(0, sizeof(Q18_JOIN3_STRUCT_RESULT_LINEITEM));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::GraceJoinConvertRight<
      Q18_JOIN2_STRUCT_CUSTOMER_RESULT, LINEITEM, Q18_PR_LINEITEM,
      q18_extractor_join2_result, column_extractor_lineitem,
      q18_converter_PR_LINEITEM, q18_extractor_PR_LINEITEM,
      q18_combinator_part_pr_lineitem> *joiner;
};

class Query18Node4 : public NodeTree {
 public:
  Query18Node4(deceve::storage::BufferManager *bmr,
               const std::string &input_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    //    tk.table_name = "result_group_sort";
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;
    sorter = NULL;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);

 private:
  deceve::bama::ReplacementSort<Q18_JOIN3_STRUCT_RESULT_LINEITEM,
                                q18_extractor_join3_result> *sorter;
};
}
} /* namespace deceve */

#endif /* QUERYPLANS_QUERYNODES_QUERY18NODES_HH_ */
