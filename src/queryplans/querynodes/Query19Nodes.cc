/*
 * Query19Nodes.cc
 *
 *  Created on: 14 Oct 2015
 *      Author: mike
 */

#include "Query19Nodes.hh"

namespace deceve {
namespace queries {

void Query19Node1::run(deceve::storage::BufferManager* bm,
                       const std::string& left_file,
                       const std::string& right_file,
                       const std::string& simple_output_name) {
  //  COUT << "Query5Node2 Execution (Join)" << "\n";
  //#################################### Construct full paths for left and
  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //  std::cout << "output_full_path: " << output_full_path << std::endl;

  column_extractor_part left_extractor(0);
  column_extractor_lineitem right_extractor(0);
  // my_combinator_part_lineitem combinator;

  q19_extractor_PR_PART pr_left_extractor(0);
  q19_converter_PR_PART pr_left_converter;

  q19_extractor_PR_LINEITEM pr_right_extractor(0);
  q19_converter_PR_LINEITEM pr_right_converter;

  q19_combinator_pr_part_pr_lineitem pr_final_combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  //  joiner = new deceve::bama::GraceJoin<
  //      PART, LINEITEM, column_extractor_part, column_extractor_lineitem,
  //      my_combinator_part_lineitem, std::less<int>, comparatorQ191Left,
  //      deceve::bama::DefaultComparator<LINEITEM> >(
  //      bm, left, right, output_full_path, left_extractor, right_extractor,
  //      combinator, partitions, joinMode);

  joiner = new deceve::bama::GraceJoinConvertBoth<
      PART, Q19_PR_PART, LINEITEM, Q19_PR_LINEITEM, column_extractor_part,
      column_extractor_lineitem, q19_converter_PR_PART,
      q19_converter_PR_LINEITEM, q19_extractor_PR_PART,
      q19_extractor_PR_LINEITEM, q19_combinator_pr_part_pr_lineitem,
      std::less<int>, comparatorQ191Left, comparatorQ191Right>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      pr_left_converter, pr_right_converter, pr_left_extractor,
      pr_right_extractor, pr_final_combinator, partitions, joinMode,
      deceve::storage::BOTH_PROJECT);

  joiner->setStreamFlag(true);
  joiner->setJoinTableKeys(left_tk, right_tk);
  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }

  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);

  bm->removeFile(output_full_path);
}
}  // namespace queries
}  // namespace deceve
