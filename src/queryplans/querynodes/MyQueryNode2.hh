/*
 * MyQueryNode2.hh
 *
 *  Created on: Jan 20, 2015
 *      Author: maba18
 */

#ifndef MYQUERYNODE2_HH_
#define MYQUERYNODE2_HH_

// Include General Functions
#include "../definitions.hh"
#include "../../utils/global.hh"

// Include base class
#include "../NodeTree.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"

// Include Algorithm Classes
#include "../../algorithms/replacementsort.hh"
#include "../../algorithms/replacementsortConvert.hh"

namespace deceve {
namespace queries {

class MyQueryNode2 : public NodeTree {
 public:
  MyQueryNode2(deceve::storage::BufferManager *bmr,
               const std::string &input_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode), sorter() {
    //		tk.table_name = "lineitem";
    //		tk.version = deceve::storage::SORT;
    //		tk.field = "L_EXTENDEDPRICE";
    tk.table_name = deceve::storage::LINEITEM;
    tk.version = deceve::storage::SORT;
    tk.field = deceve::storage::L_EXTENDEDPRICE;

    if (input_name == "nothing") {
      input_file_mode = deceve::storage::INTERMEDIATE;
    };
  }

  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);

 private:
  // test test test test
  deceve::bama::ReplacementSortConvert<
      LINEITEM, Q2_PR_LINEITEM_SORT, column_extractor_lineitem,
      q2_extractor_PR_LINEITEM_SORT, q2_converter_PR_LINEITEM_SORT> *sorter;
};

class MyQueryNode3 : public NodeTree {
 public:
  MyQueryNode3(deceve::storage::BufferManager *bmr,
               const std::string &input_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode), sorter() {
    //		tk.table_name = "part";
    tk.table_name = deceve::storage::PART;
    tk.version = deceve::storage::SORT;
    //		tk.field = "L_EXTENDEDPRICE";
    tk.field = deceve::storage::L_EXTENDEDPRICE;

    if (input_name == "nothing") {
      input_file_mode = deceve::storage::INTERMEDIATE;
    };
  }

  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);

 private:
  deceve::bama::ReplacementSort<PART, column_extractor_part> *sorter;
};
}
}

#endif /* MYQUERYNODE2_HH_ */
