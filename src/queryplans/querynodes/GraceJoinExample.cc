/*
 * QueryTestJoin2.cc
 *
 *  Created on: Jan 23, 2025
 *      Author: maba28
 */

#include "GraceJoinExample.hh"

#include "../NodeTree.hh"
#include "../definitions.hh"
#include "../../utils/global.hh"
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

namespace deceve {
namespace queries {

void GraceJoinExample::run(deceve::storage::BufferManager* bm, const std::string& left_file,
                           const std::string& right_file, const std::string& simple_output_name) {
  std::cout << "GRACE Execution (Join)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  COUT << " Left file: " << left << "\n";
  COUT << "File size: " << bm->getFileSizeFromCatalog(left) << "\n";
  COUT << " Right file: " << right << "\n";
  COUT << "File size: " << bm->getFileSizeFromCatalog(right) << "\n";

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_order left_extractor(1);
  column_extractor_lineitem right_extractor(1);
  q10_combinator_order_lineitem combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  joiner = new deceve::bama::GraceJoin<ORDER, LINEITEM, column_extractor_order, column_extractor_lineitem,
      q10_combinator_order_lineitem>(bm, left, right, output_full_path, left_extractor, right_extractor, combinator,
                                     partitions, joinMode);

  //  joiner = new deceve::bama::HashJoin<PART, LINEITEM, column_extractor_part,
  //  column_extractor_lineitem,
  //      my_combinator_part_lineitem>(bm, left, right, output_full_path,
  //      left_extractor, right_extractor, combinator,
  //                                   partitions, joinMode);

  //  HashJoin(deceve::storage::BufferManager* bm, const std::string& l,
  //           const std::string& r, const std::string& o,
  //           const LeftExtract& le = LeftExtract(),
  //           const RightExtract& re = RightExtract(),
  //           const Combine& c = Combine(), size_t np = 20,
  //           deceve::storage::FileMode fmode = deceve::storage::PRIMARY)

  //#################################### Decide to persist or use persisted
  // results ##################################
  // test
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file, key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file, information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file, result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file, result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile = joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles, runNamePrefixOfPersistentFile);
  }
  delete joiner;

  std::cout << "NUMBER OF PARTITIONS FROM HASH JOIN" << numOfPartitionFiles << std::endl;
  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);

  bm->removeFile(output_full_path);
}

void MergeJoinExample::run(deceve::storage::BufferManager* bm, const std::string& left_file,
                           const std::string& right_file, const std::string& simple_output_name) {
  //#################################### Construct full paths for left and right
  // file ######################################

  std::cout << "MERGEJOIN Execution (Join)" << "\n";

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);


  column_extractor_part left_extractor(0);
  column_extractor_lineitem right_extractor(0);
  my_combinator_part_lineitem combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################


  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  // joiner = new deceve::bama::HashJoin<LINEITEM, PART,
  // 		column_extractor_lineitem, q14_column_extractor_part,
  // 		q14_combinator_part_line>(bm, left, right, output_full_path,
  // 		left_extractor, right_extractor, combinator, partitions,
  // joinMode);

  joiner = new deceve::bama::MergeSortJoin<PART, LINEITEM, column_extractor_part, column_extractor_lineitem,
      my_combinator_part_lineitem>(bm, left, right, output_full_path, left_extractor, right_extractor, combinator,
                                   partitions, joinMode, deceve::storage::RIGHT_FILE_ONLY);

  //#################################### Decide to persist or use persisted
   // results ##################################
   if (check_PersistResult_flag) {
     if (result_id == 0) {
       joiner->persistResult(key_about_left_persisted_file,
                             key_about_right_persisted_file, result_id);
     } else if (result_id == 1) {
       joiner->persistResult(key_about_left_persisted_file, result_id);
     } else {
       joiner->persistResult(key_about_right_persisted_file, result_id);
     }
   }
   if (check_usePersistedResult_flag) {
     if (result_id == 0) {
       joiner->usePersistedResult(information_about_left_persisted_file,
                                  information_about_right_persisted_file,
                                  result_id);
       // Update Statistics
       bm->getSM().incrementPersistentStatisticsHits(left_tk);
       bm->getSM().incrementPersistentStatisticsHits(right_tk);

     } else if (result_id == 1) {
       joiner->usePersistedResult(information_about_left_persisted_file,
                                  result_id);
       // Update Statistics
       bm->getSM().incrementPersistentStatisticsHits(left_tk);

     } else if (result_id == 2) {
       joiner->usePersistedResult(information_about_right_persisted_file,
                                  result_id);
       // Update Statistics
       bm->getSM().incrementPersistentStatisticsHits(right_tk);
     }
   }

   //#################################### Execute Join
   //##################################
   joiner->join();
   //#################################### Add information to file persistent
   // catalog ##################################

   unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
   std::string runNamePrefixOfPersistentFile =
       joiner->getRunNamePrefixOfPersistentFile();
   int tmp_result_id = joiner->getResultId();

   if (check_PersistResult_flag) {
     addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                        runNamePrefixOfPersistentFile);
   }
   delete joiner;

   //############################ Add infomation about files to Statistics Keeper
   //####################################
   addToStatistics(bm, tmp_result_id, left_tk, right_tk);

   //############################ Delete Previous Files
   //####################################
   deletePreviousFilesJoin(bm, left, right);

   bm->removeFile(output_full_path);


}

void HashJoinExample::run(deceve::storage::BufferManager* bm, const std::string& left_file,
                          const std::string& right_file, const std::string& simple_output_name) {
  std::cout << "HASHJOIN Execution (Join)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  COUT << " Left file: " << left << "\n";
  COUT << "File size: " << bm->getFileSizeFromCatalog(left) << "\n";
  COUT << " Right file: " << right << "\n";
  COUT << "File size: " << bm->getFileSizeFromCatalog(right) << "\n";

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_part left_extractor(0);
  column_extractor_lineitem right_extractor(0);
  my_combinator_part_lineitem combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  //  joiner = new deceve::bama::GraceJoin<PART, LINEITEM,
  //  column_extractor_part,
  //                                       column_extractor_lineitem,
  //                                       my_combinator_part_lineitem>(
  //      bm, left, right, output_full_path, left_extractor, right_extractor,
  //      combinator, partitions, joinMode);

  joiner = new deceve::bama::HashJoin<PART, LINEITEM, column_extractor_part, column_extractor_lineitem,
      my_combinator_part_lineitem>(bm, left, right, output_full_path, left_extractor, right_extractor, combinator,
                                   partitions, joinMode);

  //  HashJoin(deceve::storage::BufferManager* bm, const std::string& l,
  //           const std::string& r, const std::string& o,
  //           const LeftExtract& le = LeftExtract(),
  //           const RightExtract& re = RightExtract(),
  //           const Combine& c = Combine(), size_t np = 20,
  //           deceve::storage::FileMode fmode = deceve::storage::PRIMARY)

  //#################################### Decide to persist or use persisted
  // results ##################################
  // test
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file, key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }
  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file, information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file, result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file, result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile = joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles, runNamePrefixOfPersistentFile);
  }
  delete joiner;

  std::cout << "NUMBER OF PARTITIONS FROM HASH JOIN" << numOfPartitionFiles << std::endl;
  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);

  //  bm->removeFile(output_full_path);
}

void NestedLoopExample::run(deceve::storage::BufferManager* bm, const std::string& left_file,
                            const std::string& right_file, const std::string& simple_output_name) {
  std::cout << "NESTED LOOP Execution (Join)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_part left_extractor(0);
  column_extractor_lineitem right_extractor(0);
  my_combinator_part_lineitem combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  // size_t partitions = 1;

  //  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;
  //
  //  if (check_PersistResult_flag) {
  //    joinMode = deceve::storage::INTERMEDIATE;
  //  }

  joiner = new deceve::bama::NestedLoopsJoin<PART, LINEITEM, column_extractor_part, column_extractor_lineitem,
      my_combinator_part_lineitem>(bm, left, right, output_full_path, left_extractor, right_extractor, combinator);

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  delete joiner;

  //####################################
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
  //  bm->removeFile(output_full_path);
}
}
}
