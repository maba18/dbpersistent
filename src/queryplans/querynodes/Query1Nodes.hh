/*
 * Query1Nodes.hh
 *
 *  Created on: 11 Mar 2015
 *      Author: michail
 */

#ifndef QUERY1NODES_HH_
#define QUERY1NODES_HH_

// Include General Functions
#include "../definitions.hh"
#include "../../utils/global.hh"

// Include base class
#include "../NodeTree.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

// Include Algorithm Classes
#include "../../algorithms/replacementsort.hh"

namespace deceve {
namespace queries {

class Query1Node1 : public NodeTree {
 public:
  Query1Node1(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::LINEITEM;
    tk.version = deceve::storage::SELECT;
    tk.field = deceve::storage::OTHER_FIELD;

    // cost_estimation variables
    outputFileDetails(0, sizeof(LINEITEM));

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query1Node2 : public NodeTree {
 public:
  Query1Node2(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    //		tk.table_name = "lineitem_group";
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query1Node3 : public NodeTree {
 public:
  Query1Node3(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    //		tk.table_name = "lineitem_group_sort";
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    //		tk.field = "q1_column_extractor";
    tk.field = deceve::storage::OTHER_FIELD;
    sorter = NULL;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);

 private:
  deceve::bama::ReplacementSort<q1_struct, q1_column_extractor> *sorter;
};

class Query1Node4 : public NodeTree {
 public:
  Query1Node4(deceve::storage::BufferManager *bmr,
              const std::string &input_name, NodeTree *lNode = NULL,
              NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    // tk.table_name = "lineitem_group_sort_select";
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::SELECT;
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};


}
} /* namespace deceve */

#endif /* QUERY1NODES_HH_ */
