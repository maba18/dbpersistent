/*
 * Query13Nodes.hh
 *
 *  Created on: 8 Oct 2015
 *      Author: mike
 */

#ifndef QUERY13NODES_HH_
#define QUERY13NODES_HH_

#include <iostream>
#include <string>
#include <regex>

// Include General Functions
#include "../definitions.hh"
#include "../../utils/global.hh"

// Include base class
#include "../NodeTree.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

// Include Algorithm Classes
#include "../../algorithms/replacementsort.hh"
#include "../../algorithms/gracejoin.hh"
#include "../../algorithms/mergesortjoin.hh"
#include "../../algorithms/mergesortjoinConvertRight.hh"
#include "../../algorithms/mergesortjoinConvertBoth.hh"

namespace deceve {
namespace queries {

struct comparatorQ131Right {
  comparatorQ131Right() {
    srand(time(NULL));

    int randomIndex1 = rand() % 4;
    int randomIndex2 = rand() % 4;

    std::string words1[4] = {"special", "pending", "unusual", "express"};
    std::string words2[4] = {"packages", "requests", "accounts", "deposits"};

    word1 = words1[randomIndex1];
    word2 = words2[randomIndex2];
    std::string all = "(.*)";
    std::string expr = all + word1 + all + word2 + all;
    txt_regex = std::regex(expr);
  }

  bool operator()(ORDER item) {
    std::string o_comment(item.O_COMMENT);
    if (std::regex_match(o_comment, txt_regex)) {
      return true;
    }
    return false;
  }

  std::string word1;
  std::string word2;
  std::regex txt_regex;
};

class Query13Node1 : public NodeTree {
 public:
  Query13Node1(deceve::storage::BufferManager *bmr, const std::string &l_name,
               const std::string &r_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;

    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::SORT;

    left_tk.table_name = deceve::storage::CUSTOMER;
    right_tk.table_name = deceve::storage::ORDERS;

    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::O_CUSTKEY;

    right_tk.projected_field = deceve::storage::ORDER_Q13_SORTED;

    leftFileDetails(0, sizeof(CUSTOMER), sizeof(CUSTOMER));
    rightFileDetails(1, sizeof(ORDER), sizeof(Q13_PR_ORDER_SORT));
    outputFileDetails(0.98, sizeof(QUERY13_STRUCT_CUSTOMER_ORDER));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  //    deceve::bama::MergeSortJoin<CUSTOMER, ORDER, column_extractor_customer,
  //                                column_extractor_order,
  //                               q13_join1_combinator_customer_order> *joiner;

  deceve::bama::MergeSortJoinConvertRight<
      CUSTOMER, ORDER, Q13_PR_ORDER_SORT, column_extractor_customer,
      column_extractor_order, q13_extractor_PR_ORDER_SORT,
      q13_converter_PR_ORDER_SORT,
      q13_combinator_customer_pr_sort_order> *joiner;

  //  deceve::bama::GraceJoin<CUSTOMER, ORDER, column_extractor_customer,
  //                          column_extractor_order,
  //                          q13_join1_combinator_customer_order> *joiner;
};

// class Query13Node1 : public NodeTree {
// public:
//  Query13Node1(deceve::storage::BufferManager *bmr, const std::string &l_name,
//               const std::string &r_name, NodeTree *lNode = NULL,
//               NodeTree *rNode = NULL)
//      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
//    tk.version = deceve::storage::HASHJOIN;
//    left_tk.table_name = deceve::storage::CUSTOMER;
//    right_tk.table_name = deceve::storage::ORDERS;
//    left_tk.version = deceve::storage::HASHJOIN;
//    right_tk.version = deceve::storage::OTHER;
//    //    left_tk.field = "L_PARTKEY";
//    //    right_tk.field = "P_PARTKEY";
//    left_tk.field = deceve::storage::C_CUSTKEY;
//    right_tk.field = deceve::storage::OTHER_FIELD;
//
//    if (l_name == "nothing") {
//      left_file_path = deceve::storage::INTERMEDIATE_PATH;
//    };
//    if (r_name == "nothing") {
//      right_file_path = deceve::storage::INTERMEDIATE_PATH;
//    }
//  }
//
//  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
//           const std::string &right_file,
//           const std::string &simple_output_name);
//
// private:
//  deceve::bama::GraceJoin<
//      CUSTOMER, ORDER, column_extractor_customer, column_extractor_order,
//      q13_join1_combinator_customer_order, std::less<int>,
//      deceve::bama::DefaultComparator<CUSTOMER>, comparatorQ131Right> *joiner;
//};

class Query13Node2 : public NodeTree {
 public:
  Query13Node2(deceve::storage::BufferManager *bmr,
               const std::string &input_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    //    tk.table_name = "lineitem_group_sort";
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    //    tk.field = "q1_column_extractor";
    tk.field = deceve::storage::OTHER_FIELD;
    sorter = NULL;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);

 private:
  deceve::bama::ReplacementSort<QUERY13_STRUCT_CUSTOMER_ORDER,
                                q13_extractor_final_result> *sorter;
};
}
}

#endif /* QUERY13NODES_HH_ */
