/*
 * Query16Nodes.hh
 *
 *  Created on: 12 Oct 2015
 *      Author: mike
 */

#ifndef QUERY16NODES_HH_
#define QUERY16NODES_HH_

#include <sstream>
#include <unordered_map>

// Include General Functions
#include "../definitions.hh"
#include "../../utils/global.hh"

// Include base class
#include "../NodeTree.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

// Include Algorithm Classes
#include "../../algorithms/gracejoin.hh"
#include "../../algorithms/gracejoinConvertBoth.hh"

#include "../../storage/identity.hh"

namespace deceve {
namespace queries {

struct comparatorQ161Left {
  comparatorQ161Left() {
    // Construct random type string
    srand(time(NULL));
    types1 = {"STANDARD", "SMALL", "MEDIUM", "LARGE", "ECONOMY", "PROMO"};
    types2 = {"ANODIZED", "BURNISHED", "PLATED", "POLISHED", "BRUSHED"};
    types3 = {"TIN", "NICKEL", "BRASS", "STEEL", "COPPER"};

    std::stringstream concatenatedTypes;
    randomIndex = rand() % types1.size();
    concatenatedTypes << types1[randomIndex] << " ";
    randomIndex = rand() % types2.size();
    concatenatedTypes << types2[randomIndex] << " ";
    randomIndex = rand() % types3.size();
    concatenatedTypes << types2[randomIndex] << " ";

    type = concatenatedTypes.str();

    int N = 1 + rand() % 5;
    int M = 1 + rand() % 5;
    std::stringstream concatenatedBrand;
    concatenatedBrand << "BRAND#" << N << M;

    // Construct Brand
    brand = concatenatedBrand.str();

    for (int i = 0; i < 10; i++) {
      sizes.insert(1 + rand() % 50);
    }
  }

  bool operator()(PART item) {
    std::string typeTuple = string(item.P_TYPE);
    std::string brandnameTuple = string(item.P_BRAND);
    int sizeTuple = item.P_SIZE;

    //    std::cout << typeTuple << " " << brandnameTuple << " " << sizeTuple
    //              << " vs " << sizeTuple << " " << brand << " " << std::endl;
    //    for (auto it : sizes) {
    //      std::cout << it << " ";
    //    }
    if (typeTuple != container && brandnameTuple != brand &&
        sizes.find(sizeTuple) != sizes.end()) {
      //      std::cout << "true" << std::endl;
      return true;
    }
    return false;
  }

  std::vector<std::string> containers1;
  std::vector<std::string> containers2;
  std::vector<std::string> types1;
  std::vector<std::string> types2;
  std::vector<std::string> types3;
  std::string brand;
  std::string container;
  std::string type;
  int randomIndex;
  std::unordered_set<int> sizes;
};

class Query16Node1 : public NodeTree {
 public:
  Query16Node1(deceve::storage::BufferManager *bmr, const std::string &l_name,
               const std::string &r_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    tk.version = deceve::storage::HASHJOIN;

    left_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::PART;
    left_tk.field = deceve::storage::OTHER_FIELD;

    right_tk.version = deceve::storage::HASHJOIN;
    right_tk.table_name = deceve::storage::PARTSUPP;
    right_tk.field = deceve::storage::PS_PARTKEY;

#ifdef UNIFIED
    right_tk.projected_field = deceve::storage::PARTSUPP_UNIFIED_PARTKEY;
#else
    right_tk.projected_field = deceve::storage::PARTSUPP_Q16;
#endif

    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    leftFileDetails(0.2, sizeof(PART), sizeof(Q16_PR_PART));
    rightFileDetails(1, sizeof(PARTSUPP), sizeof(Q16_PR_PARTSUPP));
    outputFileDetails(0.18, sizeof(Q16_JOIN1_STRUCT_PART_PARTSUPP));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  //  deceve::bama::GraceJoin<PART, PARTSUPP, column_extractor_part,
  //  column_extractor_partsupp,
  //      q16_join1_combinator_part_partsupp, std::less<int>,
  //      comparatorQ161Left, deceve::bama::DefaultComparator<PARTSUPP> >
  //      *joiner1;

  deceve::bama::GraceJoinConvertBoth<
      PART, Q16_PR_PART, PARTSUPP, Q16_PR_PARTSUPP, column_extractor_part,
      column_extractor_partsupp, q16_converter_PR_PART,
      q16_converter_PR_PARTSUPP, q16_extractor_PR_PART,
      q16_extractor_PR_PARTSUPP, q16_join1_combinator_pr_part_pr_partsupp,
      std::less<int>, comparatorQ161Left,
      deceve::bama::DefaultComparator<PARTSUPP>> *joiner;
};
}
}
#endif /* QUERY16NODES_HH_ */
