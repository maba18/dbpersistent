/*
 * QueryTestJoin1.hh
 *
 *  Created on: Jan 23, 2025
 *      Author: maba28
 */

#ifndef QUERYTESTJOIN2_HH_
#define QUERYTESTJOIN2_HH_

// Include General Functions
#include "../definitions.hh"
#include "../../utils/global.hh"

// Include base class
#include "../NodeTree.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"

// Include Algorithm Classes
#include "../../algorithms/gracejoin.hh"
#include "../../algorithms/mergesortjoin.hh"
#include "../../algorithms/hashjoin.hh"
#include "../../algorithms/nestedloopsjoin.hh"

namespace deceve {
namespace queries {

struct comparatorQ201Left {
  comparatorQ201Left() {
    types = {"STANDARD ANODIZED TIN",  "SMALL BURNISHED NICKEL",
             "MEDIUM PLATED BRASS",    "LARGE POLISHED STEEL",
             "ECONOMY BRUSHED COPPER", "PROMO"};

    srand(time(NULL));
    randomIndex = rand() % 6;
  }

  bool operator()(PART item) {
    std::string type = string(item.P_TYPE);
    if (type == types[randomIndex]) {
      return true;
    }
    return false;
  }

  std::vector<std::string> types;
  int randomIndex;
};

class GraceJoinExample : public NodeTree {
 public:
  GraceJoinExample(deceve::storage::BufferManager *bmr,
                   const std::string &l_name, const std::string &r_name,
                   NodeTree *lNode = NULL, NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    tk.version = deceve::storage::HASHJOIN;
    //		left_tk.table_name = "result_left_3";
    //		right_tk.table_name = "lineitem";
    //		left_tk.field = "O_ORDERKEY";
    //		right_tk.field = "L_ORDERKEY";

    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::HASHJOIN;
    left_tk.table_name = deceve::storage::ORDERS;
    right_tk.table_name = deceve::storage::LINEITEM;
    left_tk.field = deceve::storage::O_ORDERKEY;
    right_tk.field = deceve::storage::L_ORDERKEY;

    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::GraceJoin<ORDER, LINEITEM, column_extractor_order,
                          column_extractor_lineitem,
                          q10_combinator_order_lineitem> *joiner;

  //  deceve::bama::HashJoin<PART, LINEITEM, column_extractor_part,
  //                         column_extractor_lineitem,
  //                         my_combinator_part_lineitem> *joiner;

  //  HashJoin(deceve::storage::BufferManager* bm, const std::string& l,
  //           const std::string& r, const std::string& o,
  //           const LeftExtract& le = LeftExtract(),
  //           const RightExtract& re = RightExtract(),
  //           const Combine& c = Combine(), size_t np = 20,
  //           deceve::storage::FileMode fmode = deceve::storage::PRIMARY)
};

class MergeJoinExample : public NodeTree {
 public:
  MergeJoinExample(deceve::storage::BufferManager *bmr,
                   const std::string &l_name, const std::string &r_name,
                   NodeTree *lNode = NULL, NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    tk.version = deceve::storage::HASHJOIN;
    //		left_tk.table_name = "part";
    //		right_tk.table_name = "lineitem";
    //		left_tk.field = "P_PARTKEY";
    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::SORT;
    left_tk.table_name = deceve::storage::OTHER_TABLENAME;
    right_tk.table_name = deceve::storage::LINEITEM;
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::L_PARTKEY;

    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::MergeSortJoin<PART, LINEITEM, column_extractor_part,
                              column_extractor_lineitem,
                              my_combinator_part_lineitem> *joiner;
};

class HashJoinExample : public NodeTree {
 public:
  HashJoinExample(deceve::storage::BufferManager *bmr,
                  const std::string &l_name, const std::string &r_name,
                  NodeTree *lNode = NULL, NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    tk.version = deceve::storage::HASHJOIN;
    //    left_tk.table_name = "result_left_3";
    //    right_tk.table_name = "lineitem";
    //    left_tk.field = "O_ORDERKEY";
    //    right_tk.field = "L_ORDERKEY";

    left_tk.version = deceve::storage::HASHJOIN;
    right_tk.version = deceve::storage::HASHJOIN;
    left_tk.table_name = deceve::storage::PART;
    right_tk.table_name = deceve::storage::PARTSUPP;
    left_tk.field = deceve::storage::P_PARTKEY;
    right_tk.field = deceve::storage::PS_PARTKEY;

    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  //  deceve::bama::GraceJoin<PART, LINEITEM, column_extractor_part,
  //                          column_extractor_lineitem,
  //                          my_combinator_part_lineitem> *joiner;

  deceve::bama::HashJoin<PART, LINEITEM, column_extractor_part,
                         column_extractor_lineitem,
                         my_combinator_part_lineitem> *joiner;

  //  HashJoin(deceve::storage::BufferManager* bm, const std::string& l,
  //           const std::string& r, const std::string& o,
  //           const LeftExtract& le = LeftExtract(),
  //           const RightExtract& re = RightExtract(),
  //           const Combine& c = Combine(), size_t np = 20,
  //           deceve::storage::FileMode fmode = deceve::storage::PRIMARY)
};

class NestedLoopExample : public NodeTree {
 public:
  NestedLoopExample(deceve::storage::BufferManager *bmr,
                    const std::string &l_name, const std::string &r_name,
                    NodeTree *lNode = NULL, NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    tk.version = deceve::storage::HASHJOIN;
    //    left_tk.table_name = "result_left_3";
    //    right_tk.table_name = "lineitem";
    //    left_tk.field = "O_ORDERKEY";
    //    right_tk.field = "L_ORDERKEY";

    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    left_tk.table_name = deceve::storage::PART;
    right_tk.table_name = deceve::storage::PARTSUPP;
    left_tk.field = deceve::storage::P_PARTKEY;
    right_tk.field = deceve::storage::PS_PARTKEY;

    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::NestedLoopsJoin<PART, LINEITEM, column_extractor_part,
                                column_extractor_lineitem,
                                my_combinator_part_lineitem> *joiner;
};
}
}

#endif /* QUERYTESTJOIN2_HH_ */
