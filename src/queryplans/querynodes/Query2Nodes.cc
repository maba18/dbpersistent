/*
 * Query2Nodes.cc
 *
 *  Created on: Apr 29, 2015
 *      Author: maba18
 */

#include "Query2Nodes.hh"

#include <string>

namespace deceve {
namespace queries {

void Query2Node1::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  //   std::cout << "Query2Node1" << std::endl;
  //  COUT << "Query5Node2 Execution (Join)" << "\n";
  //#################################### Construct full paths for left and
  // right
  // file ######################################
  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //  std::cout << "output_full_path: " << output_full_path << std::endl;

  column_extractor_supplier left_extractor(0);
  column_extractor_partsupp right_extractor(1);
  //  q2_join1_combinator_supplier_partsupp combinator;

  //  q9_combinator_part_line combinator;
  q2_converter_PR_PARTSUPP pr_right_converter;
  q2_extractor_PR_PARTSUPP pr_right_extractor(1);
  q2_combinator_supplier_pr_partsupp pr_combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  //  joiner = new deceve::bama::GraceJoin<
  //      SUPPLIER, PARTSUPP, column_extractor_supplier,
  //      column_extractor_partsupp,
  //            q2_join1_combinator_supplier_partsupp, std::less<int>,
  //            deceve::bama::DefaultComparator<SUPPLIER>,
  //            deceve::bama::DefaultComparator<PARTSUPP> >(bm, left, right,
  //            output_full_path,
  //                                             left_extractor,
  //                                             right_extractor,
  //                                             combinator,partitions,
  //                                             joinMode);

  joiner = new deceve::bama::GraceJoinConvertRight<
      SUPPLIER, PARTSUPP, Q2_PR_PARTSUPP, column_extractor_supplier,
      column_extractor_partsupp, q2_converter_PR_PARTSUPP,
      q2_extractor_PR_PARTSUPP, q2_combinator_supplier_pr_partsupp,
      std::less<int>, deceve::bama::DefaultComparator<SUPPLIER>,
      deceve::bama::DefaultComparator<PARTSUPP>>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      pr_right_converter, pr_right_extractor, pr_combinator, partitions,
      joinMode, deceve::storage::RIGHT_PROJECT);

  joiner->setJoinTableKeys(left_tk, right_tk);


  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }

  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
}

void Query2Node2::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  //   std::cout << "Query2Node2" << std::endl;
  //  COUT << "Query5Node2 Execution (Join)" << "\n";
  //#################################### Construct full paths for left and
  // right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //  std::cout << "output_full_path: " << output_full_path << std::endl;

  column_extractor_nation left_extractor(0);
  q2_join1_extractor_result right_extractor(0);
  q2_join2_combinator_nation_result combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  //  size_t partitions = 1;

  //  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;
  //
  //  if (check_PersistResult_flag) {
  //    joinMode = deceve::storage::INTERMEDIATE;
  //  }

  joiner = new deceve::bama::NestedLoopsJoin<
      NATION, Q2_JOIN1_STRUCT_SUPPLIER_PARTSUPP, column_extractor_nation,
      q2_join1_extractor_result, q2_join2_combinator_nation_result>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      combinator);

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  delete joiner;

  deletePreviousFilesJoin(bm, left, right);
  // std::cout << "deletePreviousFilesJoin" << std::endl;

  //  scan<Q2_JOIN2_STRUCT_SUPPLIER_RESULT>(output_full_path);
  //  bm->removeFile(output_full_path);
  // std::cout<<"output_full_path: "<<output_full_path<<std::endl;
  // std::cout<<"left: "<<left<<std::endl;
  // std::cout<<"right: "<<right<<std::endl;
}

void Query2Node3RegionSelection::run(deceve::storage::BufferManager* bm,
                                     const std::string& input_name,
                                     const std::string& simple_output_name) {
  //   std::cout << "Query2Node3Select" << std::endl;
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<REGION> reader(bm, full_input_name,
                                      bm->getFileMode(full_input_name));

  deceve::bama::Writer<REGION> res_writer(bm, output_full_path,
                                          deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  REGION item;
  srand(time(NULL));
  std::string region_names[] = {"AFRICA", "AMERICA", "ASIA", "EUROPE",
                                "MIDDLE EAST"};

  /* Generate random date and subtract it from the current date */
  std::string random_region_name = region_names[rand() % 5];

  // std::cout << "RANDOM REGION SELECTED: " << random_region_name << std::endl;
  int counter = 0;
  while (reader.hasNext()) {
    item = reader.nextRecord();
    counter++;
    if (strcmp(item.R_NAME, random_region_name.c_str()) == 0) {
      res_writer.write(item);
    }
  }
  reader.close();
  res_writer.close();
  // std::cout<<"output_full_path: "<<output_full_path<<std::endl;
}

void Query2Node3::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  //   std::cout << "Query2Node3" << std::endl;
  //  COUT << "Query5Node2 Execution (Join)" << "\n";
  //#################################### Construct full paths for left and
  // right
  // file ######################################

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //  std::cout << "output_full_path: " << output_full_path << std::endl;

  column_extractor_region left_extractor(0);
  q2_join2_extractor_result right_extractor(0);
  q2_join3_combinator_region_result combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  //  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  joiner = new deceve::bama::HashJoin<
      REGION, Q2_JOIN2_STRUCT_SUPPLIER_RESULT, column_extractor_region,
      q2_join2_extractor_result, q2_join3_combinator_region_result>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      combinator, joinMode);

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  delete joiner;

  deletePreviousFilesJoin(bm, left, right);
  // std::cout << "Left: " << left << std::endl;
  // std::cout << "Right: " << right << std::endl;
  // // std::cout << "deletePreviousFilesJoin" << std::endl;

  //  scan<Q2_JOIN3_STRUCT_NATION_RESULT>(output_full_path);
  // DO IT EXPLICITLY
  //  bm->removeFile(left);

  //    bm->removeFile(output_full_path);
  // std::cout<<"output_full_path: "<<output_full_path<<std::endl;
  // std::cout<<"left: "<<left<<std::endl;
  // std::cout<<"right: "<<right<<std::endl;
}

void Query2Node4::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  //  std::cout << "Query2Node4 Execution (Join <PART vd LINEITEM>)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################
  // std::cout << "Query2Node4" << std::endl;
  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_partsupp left_extractor(0);
  column_extractor_part right_extractor(0);
  // q2_join4_combinator_partsupp_part combinator;

  q2_converter_PR_PARTSUPP_NODE4 pr_left_converter;
  q2_extractor_PR_PARTSUPP_NODE4 pr_left_extractor(0);

  q2_converter_PR_PART pr_right_converter;
  q2_extractor_PR_PART pr_right_extractor(0);

  q2_combinator_pr_partsupp_node4_pr_part pr_final_combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  //  joiner = new deceve::bama::GraceJoin<
  //      PARTSUPP, PART, column_extractor_partsupp, column_extractor_part,
  //      q2_join4_combinator_partsupp_part, std::less<int>,
  //      deceve::bama::DefaultComparator<PARTSUPP>, comparatorQ24Right>(
  //      bm, left, right, output_full_path, left_extractor, right_extractor,
  //      combinator, partitions, joinMode);

  joiner = new deceve::bama::GraceJoinConvertBoth<
      PARTSUPP, Q2_PR_PARTSUPP_NODE4, PART, Q2_PR_PART,
      column_extractor_partsupp, column_extractor_part,
      q2_converter_PR_PARTSUPP_NODE4, q2_converter_PR_PART,
      q2_extractor_PR_PARTSUPP_NODE4, q2_extractor_PR_PART,
      q2_combinator_pr_partsupp_node4_pr_part, std::less<int>,
      deceve::bama::DefaultComparator<PARTSUPP>, comparatorQ24Right>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      pr_left_converter, pr_right_converter, pr_left_extractor,
      pr_right_extractor, pr_final_combinator, partitions, joinMode,
      deceve::storage::LEFT_PROJECT);

  joiner->setJoinTableKeys(left_tk, right_tk);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }

  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
}

void Query2Node5::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  //  std::cout << "Query2Node5 Execution (Join <PART vd LINEITEM>)" << "\n";
  //#################################### Construct full paths for left and right
  // file ######################################

  // std::cout << "Query2Node5" << std::endl;

  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  column_extractor_supplier left_extractor(1);
  q2_join4_extractor_result right_extractor(0);
  q2_join5_combinator_supplier_result combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  joiner = new deceve::bama::GraceJoin<
      SUPPLIER, Q2_JOIN4_STRUCT_PARTSUPP_PART, column_extractor_supplier,
      q2_join4_extractor_result, q2_join5_combinator_supplier_result,
      std::less<int>, deceve::bama::DefaultComparator<SUPPLIER>,
      deceve::bama::DefaultComparator<Q2_JOIN4_STRUCT_PARTSUPP_PART>>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      combinator, partitions, joinMode);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }

  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);
}

void Query2Node6::run(deceve::storage::BufferManager* bm,
                      const std::string& left_file,
                      const std::string& right_file,
                      const std::string& simple_output_name) {
  //  std::cout << "Query2Node6" << std::endl;
  //  COUT << "Query5Node2 Execution (Join)" << "\n";
  //#################################### Construct full paths for left and
  // right
  // file ######################################
  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //  std::cout << "output_full_path: " << output_full_path << std::endl;

  q2_join5_extractor_result left_extractor(1);
  q2_join3_extractor_result right_extractor(1);
  q2_join6_combinator_result_result combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################
  //  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;
  //
  //  if (check_PersistResult_flag) {
  //    joinMode = deceve::storage::INTERMEDIATE;
  //  }

  joiner = new deceve::bama::HashJoin<
      Q2_JOIN5_STRUCT_SUPPLIER_RESULT, Q2_JOIN3_STRUCT_NATION_RESULT,
      q2_join5_extractor_result, q2_join3_extractor_result,
      q2_join6_combinator_result_result>(bm, left, right, output_full_path,
                                         left_extractor, right_extractor,
                                         combinator);

  //  joiner = new deceve::bama::NestedLoopsJoin<
  //      Q2_JOIN5_STRUCT_SUPPLIER_RESULT, Q2_JOIN3_STRUCT_NATION_RESULT,
  //      q2_join5_extractor_result, q2_join3_extractor_result,
  //      q2_join6_combinator_result_result>(bm, left, right, output_full_path,
  //                                         left_extractor, right_extractor,
  //                                         combinator);

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  delete joiner;

  deletePreviousFilesJoin(bm, left, right);
  // std::cout << "deletePreviousFilesJoin" << std::endl;
  //  scan<Q2_JOIN1_STRUCT_SUPPLIER_PARTSUPP>(output_full_path);
  // std::cout<<"output_full_path: "<<output_full_path<<std::endl;
  // std::cout<<"left: "<<left<<std::endl;
  // std::cout<<"right: "<<right<<std::endl;
  bm->removeFile(output_full_path);
}

}  // namespace queries
}  // namespace deceve
