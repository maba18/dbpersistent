/*
 * Query10Nodes.hh
 *
 *  Created on: 20 Mar 2015
 *      Author: michail
 */

#ifndef QUERY10NODES_HH_
#define QUERY10NODES_HH_

// Include General Functions
#include "../definitions.hh"
#include "../../utils/global.hh"

// Include base class
#include "../NodeTree.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

// Include Algorithm Classes
#include "../../algorithms/gracejoin.hh"
#include "../../algorithms/gracejoinConvertRight.hh"
#include "../../algorithms/gracejoinConvertBoth.hh"
#include "../../algorithms/mergesortjoin.hh"
#include "../../algorithms/mergesortjoinConvertRight.hh"

namespace deceve {
namespace queries {

struct comparatorQ102Left {
  comparatorQ102Left() {
    srand(time(NULL));

    int randomYear = 1993 + rand() % 3;
    int randomMonth = rand() % 12 + 1;
    if (randomYear == 1995) randomMonth = 1;
    if (randomYear == 1993 && randomMonth == 1) randomMonth = 2;
    d1 = {randomYear, randomMonth, 1};
    //    d2= {randomYear, randomMonth + 3, 1};
    d2 = d1.plus(3);
  }

  bool operator()(ORDER item) {
    date tmpdate(item.O_ORDERDATE);
    //    std::cout << "ORDER: Check d1: " << d1 << " < " << item.O_ORDERDATE <<
    //    " - "
    //              << tmpdate << " item.O_ORDERDATE:" << std::endl;
    //    std::cout << "ORDER: Check O_ORDERDATE: " << tmpdate << " < " << d2 <<
    //    " d2"
    //              << std::endl;
    if (d1 <= tmpdate && tmpdate <= d2) {
      //      std::cout << "True" << std::endl;
      return true;
    }
    //    std::cout << "False" << std::endl;
    return false;
  }

  date d1;
  date d2;
};

struct comparatorQ102Right {
  bool operator()(LINEITEM item) {
    //    std::cout << "LINEITEM CHECK item: " << item.L_RETURNFLAG << " with R"
    //              << std::endl;
    if (item.L_RETURNFLAG == 'R') {
      //      std::cout << "True" << std::endl;
      return true;
    }
    //    std::cout << "False" << std::endl;
    return false;
  }
};

class Query10Node0 : public NodeTree {
 public:
  Query10Node0(deceve::storage::BufferManager *bmr,
               const std::string &input_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    // tk.table_name = "orders";
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    //		tk.field = "L_ORDERDATE";
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query10Node1 : public NodeTree {
 public:
  Query10Node1(deceve::storage::BufferManager *bmr,
               const std::string &input_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    //		tk.table_name = "lineitem";
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query10Node2 : public NodeTree {
 public:
  Query10Node2(deceve::storage::BufferManager *bmr, const std::string &l_name,
               const std::string &r_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;
    //		left_tk.table_name = "result_order";
    //		right_tk.table_name = "result_lineitem";
    left_tk.table_name = deceve::storage::ORDERS;
    right_tk.table_name = deceve::storage::LINEITEM;
    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    //		left_tk.field = "O_ORDERKEY";
    //		right_tk.field = "L_ORDERKEY";
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::OTHER_FIELD;

    //    leftWriteFactor = 0.3;
    //    leftTupleSize = sizeof(ORDER);
    //    leftTupleSizeProjected = sizeof(Q10_PR_ORDER);
    //
    //    rightWriteFactor = 0.33;
    //    rightTupleSize = sizeof(LINEITEM);
    //    rightTupleSizeProjected = sizeof(Q10_PR_LINEITEM);
    //
    //    outputWriteFactor = 0.011;
    //    outputTupleSize = sizeof(Q10_JOIN1_STRUCT_ORDER_LINEITEM);

    leftFileDetails(0.04, sizeof(ORDER), sizeof(Q10_PR_ORDER));
    rightFileDetails(0.24, sizeof(LINEITEM), sizeof(Q10_PR_LINEITEM));
    outputFileDetails(0.019, sizeof(Q10_JOIN1_STRUCT_ORDER_LINEITEM));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  //  deceve::bama::GraceJoin<ORDER, LINEITEM, column_extractor_order,
  //  column_extractor_lineitem,
  //      q10_combinator_order_lineitem, std::less<int>, comparatorQ102Left,
  //      comparatorQ102Right> *joiner;

  //  deceve::bama::GraceJoinConvert<ORDER, LINEITEM, Q10_PR_LINEITEM,
  //  column_extractor_order, column_extractor_lineitem,
  //      q10_converter_PR_LINEITEM, q10_extractor_PR_LINEITEM,
  //      q10_combinator_order_pr_lineitem, std::less<int>,
  //      comparatorQ102Left, comparatorQ102Right> *joiner;

  deceve::bama::GraceJoinConvertBoth<
      ORDER, Q10_PR_ORDER, LINEITEM, Q10_PR_LINEITEM, column_extractor_order,
      column_extractor_lineitem, q10_converter_PR_ORDER,
      q10_converter_PR_LINEITEM, q10_extractor_PR_ORDER,
      q10_extractor_PR_LINEITEM, q10_combinator_pr_order_pr_lineitem,
      std::less<int>, comparatorQ102Left, comparatorQ102Right> *joiner;
};

class Query10Node3 : public NodeTree {
 public:
  Query10Node3(deceve::storage::BufferManager *bmr, const std::string &l_name,
               const std::string &r_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;
    //		left_tk.table_name = "result";
    //		right_tk.table_name = "result_customer";
    left_tk.table_name = deceve::storage::OTHER_TABLENAME;
    left_tk.version = deceve::storage::OTHER;
    left_tk.field = deceve::storage::OTHER_FIELD;

    right_tk.table_name = deceve::storage::CUSTOMER;
    right_tk.version = deceve::storage::OTHER;
    right_tk.field = deceve::storage::OTHER_FIELD;
    //		left_tk.field = "O_CUSTKEY";
    //		right_tk.field = "C_CUSTKEY";

    //    leftWriteFactor = 1;
    //    leftTupleSize = sizeof(Q10_JOIN1_STRUCT_ORDER_LINEITEM);
    //    leftTupleSizeProjected = sizeof(Q10_JOIN1_STRUCT_ORDER_LINEITEM);
    //    manualNumberOfLeftTuples = 0.011;

    leftFileDetails(1, sizeof(Q10_JOIN1_STRUCT_ORDER_LINEITEM),
                    sizeof(Q10_JOIN1_STRUCT_ORDER_LINEITEM), 0.019);

    rightFileDetails(1, sizeof(CUSTOMER), sizeof(CUSTOMER));

    outputFileDetails(0.64, sizeof(Q10_JOIN2_STRUCT_RESULT_CUSTOMER));

    //    rightWriteFactor = 1;
    //    rightTupleSize = sizeof(CUSTOMER);
    //    rightTupleSizeProjected = sizeof(CUSTOMER);

    //    outputWriteFactor = 0.2;
    //    outputTupleSize = sizeof(Q10_JOIN2_STRUCT_RESULT_CUSTOMER);
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::MergeSortJoin<
      Q10_JOIN1_STRUCT_ORDER_LINEITEM, CUSTOMER, q10_join2_extractor_result,
      column_extractor_customer, q10_combinator_result_customer> *joiner;

  //  deceve::bama::MergeSortJoinConvertRight<Q10_JOIN1_STRUCT_ORDER_LINEITEM,
  //  CUSTOMER, Q10_PR_CUSTOMER_SORT , q10_join2_extractor_result,
  //      column_extractor_customer, q10_extractor_PR_CUSTOMER_SORT,
  //      q10_converter_PR_CUSTOMER_SORT,
  //      q10_combinator_result_pr_sort_customer> *joiner;
};
//

class Query10Node4 : public NodeTree {
 public:
  Query10Node4(deceve::storage::BufferManager *bmr, const std::string &l_name,
               const std::string &r_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;
    //		left_tk.table_name = "result";
    //		right_tk.table_name = "result_nation";
    left_tk.table_name = deceve::storage::OTHER_TABLENAME;
    right_tk.table_name = deceve::storage::NATION;
    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;
    //		left_tk.field = "C_NATIONKEY";
    //		right_tk.field = "N_NATIONKEY";
    left_tk.field = deceve::storage::OTHER_FIELD;
    right_tk.field = deceve::storage::OTHER_FIELD;

    leftFileDetails(0, sizeof(Q10_JOIN2_STRUCT_RESULT_CUSTOMER),
                    sizeof(Q10_JOIN2_STRUCT_RESULT_CUSTOMER), 0.019);

    rightFileDetails(1, sizeof(NATION), sizeof(NATION));

    outputFileDetails(0, sizeof(Q10_JOIN3_STRUCT_RESULT_NATION));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::MergeSortJoin<
      Q10_JOIN2_STRUCT_RESULT_CUSTOMER, NATION, q10_join3_extractor_result,
      column_extractor_nation, q10_combinator_result_nation> *joiner;
};
}
}
#endif /* QUERY10NODES_HH_ */
