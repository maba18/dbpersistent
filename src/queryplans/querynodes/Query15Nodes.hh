/*
 * Query15Nodes.hh
 *
 *  Created on: 13 Mar 2015
 *      Author: michail
 */

#ifndef QUERY15NODES_HH_
#define QUERY15NODES_HH_

// Include General Functions
#include "../definitions.hh"
#include "../../utils/global.hh"
#include <map>

// Include base class
#include "../NodeTree.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

// Include Algorithm Classes
#include "../../algorithms/replacementsort.hh"
#include "../../algorithms/gracejoin.hh"
#include "../../algorithms/mergesortjoin.hh"

namespace deceve {
namespace queries {

class Query15Node1 : public NodeTree {
 public:
  Query15Node1(deceve::storage::BufferManager *bmr,
               const std::string &input_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
    //		tk.table_name = "lineitem";
    tk.table_name = deceve::storage::LINEITEM;
    tk.version = deceve::storage::SELECT;
    //		tk.field = "L_SHIPDATE";
    tk.field = deceve::storage::OTHER_FIELD;


    // Cost estimates
    outputWriteFactor = 0.038;
    outputTupleSize = sizeof(q15_struct);
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query15Node2 : public NodeTree {
 public:
  Query15Node2(deceve::storage::BufferManager *bmr,
               const std::string &input_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    //		tk.table_name = "revenue";
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    //		tk.field = "total_revenue";
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);

  double q15_find_max_revenue(deceve::storage::BufferManager *bm,
                              const std::string &input_name);
};

class Query15Node3 : public NodeTree {
 public:
  Query15Node3(deceve::storage::BufferManager *bmr, const std::string &l_name,
               const std::string &r_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;
    //		left_tk.table_name = "supplier";
    //		right_tk.table_name = "revenue";

    left_tk.table_name = deceve::storage::SUPPLIER;
    right_tk.table_name = deceve::storage::OTHER_TABLENAME;

    // SUPPLIER_USAGE
    // left_tk.version = deceve::storage::SORT;
    left_tk.version = deceve::storage::OTHER;

    right_tk.version = deceve::storage::OTHER;
    //		left_tk.field = "S_SUPPKEY";
    //		right_tk.field = "SUPPLIER_NO";
    left_tk.field = deceve::storage::S_SUPPKEY;
    right_tk.field = deceve::storage::OTHER_FIELD;


    // Cost estimates
    leftFileDetails(1, sizeof(SUPPLIER), sizeof(SUPPLIER));
    rightFileDetails(0, sizeof(q15_struct), sizeof(q15_struct));
    outputFileDetails(0, sizeof(q15_struct_final_output));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::MergeSortJoin<SUPPLIER, q15_struct, column_extractor_supplier,
                              q15_column_extractor_revenue,
                              q15_combinator_supplier_revenue> *joiner;
};

class Query15Node4 : public NodeTree {
 public:
  Query15Node4(deceve::storage::BufferManager *bmr,
               const std::string &input_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    //		tk.table_name = "lineitem_group_sort";
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;
    sorter = NULL;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);

 private:
  deceve::bama::ReplacementSort<q15_struct_final_output,
                                q15_column_extractor_final_struct> *sorter;
};
}
} /* namespace deceve */

#endif /* QUERY15NODES_HH_ */
