/*
 * Query11Nodes.hh
 *
 *  Created on: Apr 20, 2015
 *      Author: maba18
 */

#ifndef QUERYPLANS_QUERYNODES_QUERY11NODES_HH_
#define QUERYPLANS_QUERYNODES_QUERY11NODES_HH_

// Include General Functions
#include "../definitions.hh"
#include "../../utils/global.hh"

// Include base class
#include "../NodeTree.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

// Include Algorithm Classes
#include "../../algorithms/replacementsort.hh"
#include "../../algorithms/gracejoin.hh"
#include "../../algorithms/mergesortjoin.hh"
#include "../../algorithms/gracejoinConvertLeft.hh"
#include "../../algorithms/gracejoinConvertBoth.hh"

namespace deceve {
namespace queries {

class Query11Node0 : public NodeTree {
 public:
  Query11Node0(deceve::storage::BufferManager *bmr,
               const std::string &input_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    // name field
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query11Node1 : public NodeTree {
 public:
  Query11Node1(deceve::storage::BufferManager *bmr, const std::string &l_name,
               const std::string &r_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;

    // SUPPLIER_USAGE
    //    left_tk.version = deceve::storage::HASHJOIN;
    left_tk.version = deceve::storage::OTHER;
    right_tk.version = deceve::storage::OTHER;

    left_tk.table_name = deceve::storage::SUPPLIER;
    right_tk.table_name = deceve::storage::NATION;

    left_tk.field = deceve::storage::S_NATIONKEY;
    right_tk.field = deceve::storage::OTHER_FIELD;

    leftFileDetails(1, sizeof(SUPPLIER), sizeof(SUPPLIER));
    rightFileDetails(1, sizeof(NATION), sizeof(NATION));
    outputFileDetails(1, sizeof(Q11_JOIN1_STRUCT_SUPPLIER_NATION));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  deceve::bama::GraceJoin<SUPPLIER, NATION, column_extractor_supplier,
                          column_extractor_nation,
                          q11_join1_combinator_supplier_nation> *joiner;
};

class Query11Node2 : public NodeTree {
 public:
  Query11Node2(deceve::storage::BufferManager *bmr, const std::string &l_name,
               const std::string &r_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;

    left_tk.version = deceve::storage::HASHJOIN;
    right_tk.version = deceve::storage::OTHER;

    left_tk.table_name = deceve::storage::PARTSUPP;
    right_tk.table_name = deceve::storage::OTHER_TABLENAME;

    left_tk.field = deceve::storage::PS_SUPPKEY;
    right_tk.field = deceve::storage::OTHER_FIELD;

    left_tk.projected_field = deceve::storage::PARTSUPP_Q11;

    leftFileDetails(1, sizeof(PARTSUPP), sizeof(Q11_PR_PARSTSUPP));
    rightFileDetails(1, sizeof(Q11_JOIN1_STRUCT_SUPPLIER_NATION),
                     sizeof(Q11_JOIN1_STRUCT_SUPPLIER_NATION));
    outputFileDetails(0, sizeof(Q11_JOIN2_STRUCT_PARTSUPP_RESULT));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  //  deceve::bama::GraceJoin<PARTSUPP, Q11_JOIN1_STRUCT_SUPPLIER_NATION,
  //  column_extractor_partsupp,
  //      q11_join2_extractor_result, q11_join2_combinator_partsupp_result>
  //      *joiner;

  //  deceve::bama::GraceJoinConvertLeft<
  //      PARTSUPP, Q11_JOIN1_STRUCT_SUPPLIER_NATION, Q11_PR_PARSTSUPP,
  //      column_extractor_partsupp, q11_join2_extractor_result,
  //      q11_converter_PR_PARSTSUPP, q11_extractor_PR_PARTSUPP,
  //      q11_combinator_pr_supplier_other> *joiner;

  deceve::bama::GraceJoinConvertBoth<
      PARTSUPP, Q11_PR_PARSTSUPP, Q11_JOIN1_STRUCT_SUPPLIER_NATION,
      Q11_JOIN1_STRUCT_SUPPLIER_NATION, column_extractor_partsupp,
      q11_join2_extractor_result, q11_converter_PR_PARSTSUPP,
      q11_converter_JOIN1_STRUCT_SUPPLIER_NATION, q11_extractor_PR_PARTSUPP,
      q11_join2_extractor_result, q11_combinator_pr_supplier_other> *joiner;

  //  column_extractor_partsupp left_extractor(1);
  //  q11_join2_extractor_result right_extractor(0);
  //  //q11_join2_combinator_partsupp_result combinator;
  //
  //  q11_converter_PR_PARSTSUPP pr_converter;
  //  q11_extractor_PR_PARTSUPP pr_extractor(0);
  //  q11_combinator_pr_supplier_other pr_combinator;
};

class Query11Node3 : public NodeTree {
 public:
  Query11Node3(deceve::storage::BufferManager *bmr,
               const std::string &input_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    // name field
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query11Node4 : public NodeTree {
 public:
  Query11Node4(deceve::storage::BufferManager *bmr,
               const std::string &input_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    //    tk.table_name = "result_group_sort";
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    //    tk.field = "q9_n8_column_extractor";
    tk.field = deceve::storage::OTHER_FIELD;
    sorter = NULL;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);

 private:
  deceve::bama::ReplacementSort<Q11_RESULT3_STRUCT_RESULT,
                                q11_joinLast_extractor_result> *sorter;
};
}
}

#endif /* QUERYPLANS_QUERYNODES_QUERY11NODES_HH_ */
