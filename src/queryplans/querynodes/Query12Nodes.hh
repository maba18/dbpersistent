/*
 * Query14Nodes.hh
 *
 *  Created on: Mar 12, 2015
 *      Author: maba18
 */

#ifndef QUERY12NODES_HH_
#define QUERY12NODES_HH_

// Include General Functions
#include "../definitions.hh"
#include "../../utils/global.hh"

// Include base class
#include "../NodeTree.hh"

// Include Storage Classes
#include "../../storage/BufferManager.hh"
#include "../../storage/readwriters.hh"

// Include Algorithm Classes
#include "../../algorithms/replacementsort.hh"
#include "../../algorithms/gracejoin.hh"
#include "../../algorithms/gracejoinConvertBoth.hh"

namespace deceve {
namespace queries {

struct comparatorQ121Left {
  comparatorQ121Left() {
    srand(time(NULL));
    std::tie(shipMode1, shipMode2) = query12RandomGetShipModes();
    int randomYear = 1993 + rand() % 5;
    d1 = {randomYear, 1, 1};
    d2 = d1.plus(12);
  }

  bool operator()(LINEITEM item) {
    date l_shipdate(item.L_SHIPDATE);
    date l_commitdate(item.L_COMMITDATE);
    date l_receiptdate(item.L_RECEIPTDATE);
    std::string shipMode = std::string(item.L_SHIPMODE);

    if (shipMode != shipMode1 && shipMode != shipMode2) {
      return false;
    }

    if (!(l_commitdate < l_receiptdate)) {
      return false;
    }

    if (l_receiptdate >= d1 && l_receiptdate < d2) {
      return true;
    }
    return false;
  }

  std::tuple<std::string, std::string> query12RandomGetShipModes() {
    srand(time(NULL));

    std::vector<std::string> shipmodes = {"REG AIR", "AIR",  "RAIL", "SHIP",
                                          "TRUCK",   "MAIL", "FOB"};

    int randomShipMode1 = 0;
    int randomShipMode2 = 0;

    randomShipMode1 = (rand() % 100) % 7;

    do {
      randomShipMode2 = (rand() % 100) % 7;
    } while (randomShipMode2 == randomShipMode1);

    //    std::cout << randomShipMode1 << "" << randomShipMode2 << std::endl;

    return std::make_tuple(shipmodes[randomShipMode1],
                           shipmodes[randomShipMode2]);
  }

  date d1;
  date d2;
  std::string shipMode1;
  std::string shipMode2;
};

class Query12Node1 : public NodeTree {
 public:
  Query12Node1(deceve::storage::BufferManager *bmr, const std::string &l_name,
               const std::string &r_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, l_name, r_name, lNode, rNode), joiner() {
    if (l_name == "nothing") {
      left_file_path = deceve::storage::INTERMEDIATE_PATH;
    };
    if (r_name == "nothing") {
      right_file_path = deceve::storage::INTERMEDIATE_PATH;
    }

    tk.version = deceve::storage::HASHJOIN;
    left_tk.table_name = deceve::storage::LINEITEM;
    left_tk.version = deceve::storage::OTHER;
    left_tk.field = deceve::storage::OTHER_FIELD;

    right_tk.table_name = deceve::storage::ORDERS;
    right_tk.version = deceve::storage::HASHJOIN;
    right_tk.field = deceve::storage::O_ORDERKEY;

#ifdef UNIFIED
    right_tk.projected_field = deceve::storage::ORDER_UNIFIED_ORDER_KEY;
#else
    right_tk.projected_field = deceve::storage::ORDER_Q12;
#endif

    leftFileDetails(0.027, sizeof(LINEITEM), sizeof(Q12_PR_LINEITEM));
    rightFileDetails(1, sizeof(ORDER), sizeof(Q12_PR_ORDER));
    outputFileDetails(0.027, sizeof(Q12_JOIN1_STRUCT_LINEITEM_ORDER));
  }

  void run(deceve::storage::BufferManager *bm, const std::string &left_file,
           const std::string &right_file,
           const std::string &simple_output_name);

 private:
  //  deceve::bama::GraceJoin<LINEITEM, ORDER, column_extractor_lineitem,
  //                          column_extractor_order,
  //                          q12_combinator_lineitem_order,
  //                          std::less<int>, comparatorQ121Left,
  //                          deceve::bama::DefaultComparator<ORDER>> *joiner;
  //
  //  deceve::bama::GraceJoinConvertBoth<
  //      ORDER, Q10_PR_ORDER, LINEITEM, Q10_PR_LINEITEM,
  //      column_extractor_order,
  //      column_extractor_lineitem, q10_converter_PR_ORDER,
  //      q10_converter_PR_LINEITEM, q10_extractor_PR_ORDER,
  //      q10_extractor_PR_LINEITEM, q10_combinator_pr_order_pr_lineitem,
  //      std::less<int>, comparatorQ102Left, comparatorQ102Right> *joiner1;

  deceve::bama::GraceJoinConvertBoth<
      LINEITEM, Q12_PR_LINEITEM, ORDER, Q12_PR_ORDER, column_extractor_lineitem,
      column_extractor_order, q12_converter_PR_LINEITEM, q12_converter_PR_ORDER,
      q12_extractor_PR_LINEITEM, q12_extractor_PR_ORDER,
      q12_combinator_pr_lineitem_pr_order, std::less<int>, comparatorQ121Left,
      deceve::bama::DefaultComparator<ORDER>> *joiner;
};

class Query12Node2 : public NodeTree {
 public:
  Query12Node2(deceve::storage::BufferManager *bmr,
               const std::string &input_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    // tk.table_name = "lineitem_group_sort_select";
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    tk.field = deceve::storage::OTHER_FIELD;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);
};

class Query12Node3 : public NodeTree {
 public:
  Query12Node3(deceve::storage::BufferManager *bmr,
               const std::string &input_name, NodeTree *lNode = NULL,
               NodeTree *rNode = NULL)
      : NodeTree(bmr, input_name, lNode, rNode) {
    //    tk.table_name = "lineitem_group_sort";
    tk.table_name = deceve::storage::OTHER_TABLENAME;
    tk.version = deceve::storage::OTHER;
    //    tk.field = "q1_column_extractor";
    tk.field = deceve::storage::OTHER_FIELD;
    sorter = NULL;

    if (input_name == "nothing") {
      input_file_path = deceve::storage::INTERMEDIATE_PATH;
    }
  }
  void run(deceve::storage::BufferManager *bm, const std::string &input_name,
           const std::string &simple_output_name);

 private:
  deceve::bama::ReplacementSort<Q12_JOIN1_STRUCT_LINEITEM_ORDER,
                                q12_extractor_final_result> *sorter;
};
}
} /* namespace deceve */

#endif /* QUERY12NODES_HH_ */
