/*
 * Query18Nodes.cc
 *
 *  Created on: 14 Oct 2015
 *      Author: mike
 */

#include "Query18Nodes.hh"

namespace deceve {
namespace queries {

void Query18Node0::run(deceve::storage::BufferManager* bm,
                       const std::string& input_name,
                       const std::string& simple_output_name) {
  //####################### Selection Template
  //####################################

  //  COUT << "******************** -Query1Node1 Execution (Select)- "
  //          "********************"
  //       << "\n";
  //####################### Construct Filenames
  //###################################
  std::string full_input_name;

  if (input_file_path == deceve::storage::PRIMARY_PATH) {
    full_input_name = constructFullPath(input_name);
  } else {
    full_input_name = constructInputTmpFullPath(node_input_name);
  }

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //####################### Declare Readers Writers
  //###################################

  deceve::bama::Reader<LINEITEM> reader(bm, full_input_name,
                                        bm->getFileMode(full_input_name));

  deceve::bama::Writer<int> res_writer(bm, output_full_path,
                                       deceve::storage::PRIMARY);

  //################################ OPERATION
  //########################################

  LINEITEM item;

  //################################ OPERATION1
  //#####################################
  std::map<int, double> memoryMap;

  //  [312..315]
  double randomQuantity;
  srand(time(NULL));
  randomQuantity = fRandomDouble(152, 155);
  // std::cout << "randomQuantity: " << randomQuantity << std::endl;
  int counter = 0;
  while (reader.hasNext()) {
    counter++;
    item = reader.nextRecord();
    int order_key = item.L_ORDERKEY;
    double quantity = item.L_QUANTITY;
    //    std::cout << quantity << std::endl;
    auto it = memoryMap.find(order_key);
    if (it == memoryMap.end()) {
      memoryMap.insert(std::make_pair(order_key, quantity));
    } else {
      it->second += quantity;
    }
  }

  for (auto it = memoryMap.begin(); it != memoryMap.end(); ++it) {
    if (it->second > randomQuantity) {
      res_writer.write(it->second);
    }
  }

  reader.close();
  res_writer.close();
}

void Query18Node1::run(deceve::storage::BufferManager* bm,
                       const std::string& left_file,
                       const std::string& right_file,
                       const std::string& simple_output_name) {
  // std::cout << "Query18Node1" << std::endl;
  //  COUT << "Query5Node2 Execution (Join)" << "\n";
  //#################################### Construct full paths for left and
  // right
  // file ######################################
  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //  std::cout << "output_full_path: " << output_full_path << std::endl;

  //  q18_extractor_int left_extractor(0);
  //  column_extractor_order right_extractor(1);
  //  q18_combinator_int_orders combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  q18_extractor_int left_extractor(0);
  column_extractor_order right_extractor(1);
  //  q18_combinator_join3_result_lineitem combinator;
  q18_converter_PR_ORDER pr_converter;
  q18_extractor_PR_ORDER pr_extractor(0);
  q18_combinator_int_pr_orders pr_combinator;

  //  joiner =
  //      new deceve::bama::GraceJoin<int, ORDER, q18_extractor_int,
  //      column_extractor_order, q18_combinator_int_orders>(
  //          bm, left, right, output_full_path, left_extractor,
  //          right_extractor, combinator, partitions, joinMode);

  joiner = new deceve::bama::GraceJoinConvertRight<
      int, ORDER, Q18_PR_ORDER, q18_extractor_int, column_extractor_order,
      q18_converter_PR_ORDER, q18_extractor_PR_ORDER,
      q18_combinator_int_pr_orders>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      pr_converter, pr_extractor, pr_combinator, partitions, joinMode,
      deceve::storage::RIGHT_PROJECT);

  joiner->setJoinTableKeys(left_tk, right_tk);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }

  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);

  //  bm->removeFile(output_full_path);
}

void Query18Node2::run(deceve::storage::BufferManager* bm,
                       const std::string& left_file,
                       const std::string& right_file,
                       const std::string& simple_output_name) {
  //  std::cout << "Query18Node2" << std::endl;
  //  COUT << "Query5Node2 Execution (Join)" << "\n";
  //#################################### Construct full paths for left and
  // right
  // file ######################################
  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  //  std::cout << "output_full_path: " << output_full_path << std::endl;

  column_extractor_customer left_extractor(0);
  q18_extractor_join1_result right_extractor(0);
  q18_combinator_join2_customer_result combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  joiner = new deceve::bama::GraceJoin<
      CUSTOMER, Q18_JOIN1_STRUCT_INT_ORDER, column_extractor_customer,
      q18_extractor_join1_result, q18_combinator_join2_customer_result>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      combinator, partitions, joinMode);

  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }

  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);

  //  bm->removeFile(output_full_path);
}

void Query18Node3::run(deceve::storage::BufferManager* bm,
                       const std::string& left_file,
                       const std::string& right_file,
                       const std::string& simple_output_name) {
  // std::cout << "Query18Node3" << std::endl;
  //  COUT << "Query5Node2 Execution (Join)" << "\n";
  //#################################### Construct full paths for left and
  // right
  // file ######################################
  std::string left;
  buildLeftFilePath(left_file, left);

  std::string right;
  buildRightFilePath(right_file, right);

  std::string output_full_path = constructOutputTmpFullPath(simple_output_name);

  // std::cout << "left" << left << std::endl;
  // std::cout << "right" << right << std::endl;
  // std::cout << "output_full_path: " << output_full_path << std::endl;

  q18_extractor_join2_result left_extractor(0);
  column_extractor_lineitem right_extractor(1);
  //  q18_combinator_join3_result_lineitem combinator;
  q18_converter_PR_LINEITEM pr_converter;
  q18_extractor_PR_LINEITEM pr_extractor(0);
  q18_combinator_part_pr_lineitem pr_combinator;

  //#################################### Calculate number of partitions and
  // initialize joiner ##################################

  // not used
  size_t partitions = 1;

  deceve::storage::FileMode joinMode = deceve::storage::PRIMARY;

  if (check_PersistResult_flag) {
    joinMode = deceve::storage::INTERMEDIATE;
  }

  // std::cout << "Before join" << std::endl;

  joiner = new deceve::bama::GraceJoinConvertRight<
      Q18_JOIN2_STRUCT_CUSTOMER_RESULT, LINEITEM, Q18_PR_LINEITEM,
      q18_extractor_join2_result, column_extractor_lineitem,
      q18_converter_PR_LINEITEM, q18_extractor_PR_LINEITEM,
      q18_combinator_part_pr_lineitem>(
      bm, left, right, output_full_path, left_extractor, right_extractor,
      pr_converter, pr_extractor, pr_combinator, partitions, joinMode,
      deceve::storage::RIGHT_PROJECT);

  joiner->setJoinTableKeys(left_tk, right_tk);


  //#################################### Decide to persist or use persisted
  // results ##################################
  if (check_PersistResult_flag) {
    if (result_id == 0) {
      joiner->persistResult(key_about_left_persisted_file,
                            key_about_right_persisted_file, result_id);
    } else if (result_id == 1) {
      joiner->persistResult(key_about_left_persisted_file, result_id);
    } else {
      joiner->persistResult(key_about_right_persisted_file, result_id);
    }
  }

  if (check_usePersistedResult_flag) {
    if (result_id == 0) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);
      bm->getSM().incrementPersistentStatisticsHits(right_tk);

    } else if (result_id == 1) {
      joiner->usePersistedResult(information_about_left_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(left_tk);

    } else if (result_id == 2) {
      joiner->usePersistedResult(information_about_right_persisted_file,
                                 result_id);
      // Update Statistics
      bm->getSM().incrementPersistentStatisticsHits(right_tk);
    }
  }

  //#################################### Execute Join
  //##################################
  joiner->join();
  //#################################### Add information to file persistent
  // catalog ##################################

  unsigned int numOfPartitionFiles = joiner->getNumberOfPartitions();
  std::string runNamePrefixOfPersistentFile =
      joiner->getRunNamePrefixOfPersistentFile();
  int tmp_result_id = joiner->getResultId();

  if (check_PersistResult_flag) {
    addFilesToCatalogs(bm, tmp_result_id, left, right, numOfPartitionFiles,
                       runNamePrefixOfPersistentFile);
  }
  delete joiner;

  //############################ Add infomation about files to Statistics Keeper
  //####################################
  addToStatistics(bm, tmp_result_id, left_tk, right_tk);
  //############################ Delete Previous Files
  //####################################
  deletePreviousFilesJoin(bm, left, right);

  bm->removeFile(output_full_path);
  //  bm->removeFile(output_full_path);
}

void Query18Node4::run(deceve::storage::BufferManager* bm,
                       const std::string& input_name,
                       const std::string& simple_output_name) {
  //####################### Sorting Template
  //####################################

  //  COUT
  //      << "******************** -Query8Node11 Execution (Sort)-
  //      ********************"
  //      << "\n";
  std::string output_full_path;
  /*
   * EXECUTE USING PERSISTED FILES
   */
  if (check_usePersistedResult_flag) {
    node_output_name = information_about_persisted_files.output_name;
    bm->getSM().incrementPersistentStatisticsHits(tk);
    // Inform next operator to unpin file on the persisted tmp catalog

  } else {
    //####################### Construct Filenames
    //####################################
    std::string full_input_name;

    if (input_file_path == deceve::storage::PRIMARY_PATH) {
      full_input_name = constructFullPath(input_name);
    } else {
      full_input_name = constructInputTmpFullPath(node_input_name);
    }

    output_full_path = constructOutputTmpFullPath(simple_output_name);

    //################################ OPERATION
    //#####################################
    q18_extractor_join3_result extractor(0);

    deceve::storage::FileMode sorterMode = deceve::storage::PRIMARY;
    if (check_PersistResult_flag) {
      sorterMode = deceve::storage::INTERMEDIATE;
    }

    sorter = new deceve::bama::ReplacementSort<Q18_JOIN3_STRUCT_RESULT_LINEITEM,
                                               q18_extractor_join3_result>(
        bm, full_input_name, output_full_path, extractor, POOLSIZE, sorterMode);

    // EXECUTE AND PERSIST RESULTS
    if (check_PersistResult_flag) {
      sorter->persistResult();
      sorter->sort();

      if (!bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(
            std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }

      // REMOVE FROM TMPFILESET
      bm->getSM().deleteFromPersistedTmpFileSet(tk);

      // ADD TO PERSISTED FILE CATALOG
      tv.num_files = sorter->getNumberOfRuns();
      tv.output_name = simple_output_name;
      tv.full_output_path = output_full_path;
      tv.pins = 1;
      bm->getSM().insertInPersistedFileCatalog(
          std::make_pair(tk, tv));
    } else {
      sorter->sort();

      if (!bm->getSM().isContainedInPersistentStatistics(tk)) {
        // Add information to persistent_file_statistics
        deceve::storage::table_statistics ts;
        ts.references = 1;
        ts.hits = 0;
        ts.cost = bm->calculateOperatorCost(tk);
        ts.timestamp = bm->getSM().getNextPersistentTime();
        bm->getSM().insertToPersistentStatistics(
            std::make_pair(tk, ts));
      } else {
        // Update information to persistent_file_statistics
        bm->getSM().incrementPersistentStatisticsReferences(tk);
      }
    }

    delete sorter;
    //############################ Delete Previous File
    //####################################
    if (!bm->getSM().isInTableCatalog(full_input_name) &&
        !bm->getSM().isInPersistedFileCatalog(leftNode->tk)) {
      bm->removeFile(full_input_name);
    }
  }

  //  scan<Q18_JOIN3_STRUCT_RESULT_LINEITEM>(output_full_path);
  bm->removeFile(output_full_path);
  //  std::this_thread::sleep_for(std::chrono::seconds(1));
  //  sleep(std::chrono::seconds(2));
}
}
} /* namespace deceve */
