#!/bin/bash


echo "Start generating graphs"
# find numbers of graphs to be merged
count=$(ls input/*graph* | wc -l)
# echo $count
lastIndex=$count


columns="input/columns.txt"
test=""
for i in `seq 1 $lastIndex`;
        do
                test="$test input/graph$i.txt"
        done 
echo "entoli $columns input/*graph*"

pwd

touch info.dat

ls input/*graph*

# rm input/old/*graph*
# cp input/*graph* input/old/

# mv input/graph13.txt input/agraph1.txt
# mv input/graph14.txt input/agraph2.txt
# mv input/graph15.txt input/agraph3.txt
# mv input/graph18.txt input/agraph4.txt
# mv input/graph19.txt input/agraph5.txt


paste $columns input/*graph* > info.dat


sed -n -e '1,1p' -e '2,2p' info.dat > info1.dat
sed -n -e '1,1p' -e '3,3p' info.dat > info2.dat
sed -n -e '1,1p' -e '4,4p' info.dat > info3.dat
sed -n -e '1,1p' -e '5,5p' info.dat > info4.dat

./generateSingleGraph.sh


############ re-run script

# sleep 5

# mv input/agraph1.txt input/bgraph1.txt
# mv input/graph16.txt input/bgraph2.txt
# mv input/graph17.txt input/bgraph3.txt
# mv input/graph20.txt input/bgraph4.txt
# mv input/graph21.txt input/bgraph5.txt

# paste $columns input/*bgraph* > info.dat


# sed -n -e '1,1p' -e '2,2p' info.dat > info1.dat
# sed -n -e '1,1p' -e '3,3p' info.dat > info2.dat
# sed -n -e '1,1p' -e '4,4p' info.dat > info3.dat
# sed -n -e '1,1p' -e '5,5p' info.dat > info4.dat


# ./generateSingleGraph.sh