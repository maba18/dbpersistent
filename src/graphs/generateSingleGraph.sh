#!/bin/bash

gnuplot generatePlot.gnu

uuid=$(uuidgen)
echo $uuid

now=$(date +"%T")
next=$(date +"%m%d%y")
next=$next-$(date +"%H%M%S")
echo "Current time : $next"

outputFolderName=output/$next
mkdir $outputFolderName

filename=$outputFolderName/bar.png

mv bar.png $filename

for i in `seq 1 4`;
        do
                mv bar$i.png $outputFolderName/bar$i.png
        done 


echo $outputFolderName

cp info.dat $outputFolderName



# if [ "$(uname)" == "Darwin" ]
# then

# if [[ $1 -eq 1 ]] ; then
#  	echo "file was generated $outputFolderName/bar.png"
# else
#  	open $outputFolderName/bar.png
# fi


# elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]
# then

# echo $1
# if [[ $1 -eq 1 ]] ; then
#  	echo "file was generated $outputFolderName/bar.png"
# else
#  	eog $outputFolderName/bar.png
# fi


# elif [ -n "$COMSPEC" -a -x "$COMSPEC" ]
# then 
#   echo $0: this script does not support Windows \:\(
# fi