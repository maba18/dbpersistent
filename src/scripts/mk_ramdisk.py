#!/usr/bin/python

import os
import platform
import glob
import optparse
import subprocess

def main():
    usage = "usage %prog [options]"
    parser = optparse.OptionParser(usage)

    parser.add_option("-n", "--name", type="string", dest="name",
                      action="store", default="ramdisk",
                      help="name of RAM disk")
    parser.add_option("-s", "--size", type="int", dest="size",
                      action="store", default="1024",
                      help="size of RAM disk in megabytes")

    options, args = parser.parse_args()
    
    system = platform.system()
    if system == "Linux":
        size = options.size * 1024 * 1024
        command = "mkdir -p /mnt/" + options.name
        print "running %s" % command
        subprocess.call(command, shell=True)        
        command = "mount -t ramfs -o size=" + str(size) + " ramfs "
        command += "/mnt/" + options.name
        print "running %s" % command
        subprocess.call(command, shell=True)
        command = "chmod a+rwx /mnt/" + options.name
        print "running %s" % command
        subprocess.call(command, shell=True)
    elif system == "Darwin":
        size = options.size * 2048
        command = "diskutil erasevolume HFS+ \"" + options.name + "\" "
        command += "`hdiutil attach -nomount ram://" + str(size) + "`"
        print "running %s" % command
        subprocess.call(command, shell=True)
    else:
        print "Unsupported operating system: %s" % system

if __name__ == "__main__":
    main()
