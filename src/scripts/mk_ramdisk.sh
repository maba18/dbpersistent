#!/bin/sh
SIZE=$((1024*1024/2))
if [ $# -ge 1 ]; then
	SIZE=$1
fi
OS=${OSTYPE//[0-9.]/}
diskutil erasevolume HFS+ "ramdisk" `hdiutil attach -nomount ram://$SIZE`
