#!/bin/bash


if [ "$(uname)" == "Darwin" ]
then

sudo rm -r /Volumes/ramdisk/src

sudo diskutil unmount /Volumes/ramdisk


elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]
then

sudo rm -r /mnt/ramdisk/src

sudo umount /mnt/ramdisk


elif [ -n "$COMSPEC" -a -x "$COMSPEC" ]
then 
  echo $0: this script does not support Windows \:\(
fi
