#!/bin/bash


rm visualisation/pattern/*json
rm visualisation/pattern/*php

rm runs/dbfiles/test.table

cd runs

# Delete temporary files if execution fails
rm *_join_*
rm dbfiles/tmp/*
rm dbfiles/*.table_*
rm dbfiles/*.table.*
rm dbfiles/*persistent*



# echo "$@"

STRING="./startDatabase $@"

echo $STRING
eval $STRING

#if execution fails, remove generated intermediate files
# and run again
while [[ $? -eq 139 ]]; do

rm *_join_*
rm dbfiles/tmp/*
rm dbfiles/*.table_*
rm dbfiles/*.table.*
rm dbfiles/*persistent*

echo $STRING
eval $STRING

done

echo "---------------- dbfiles folder content ----------------"
ls -al -h dbfiles
echo "---------------- dbfiles/tmp folder content ----------------"
ls -al -h dbfiles/tmp
cd ..


cd visualisation/pattern/

# use this if you want to track the bufferpool state
# ./generateIndexPhpFiles.sh

cd ../..
