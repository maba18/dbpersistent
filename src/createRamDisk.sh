#!/bin/bash

# if [ "$(uname)" == "Darwin" ]; then
#     # Do something under Mac OS X platform 
#     echo MAC      
# elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
#     # Do something under Linux platform
# elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
#     # Do something under Windows NT platform
# fi



if [ "$(uname)" == "Darwin" ]
then

/Volumes/ramdisk

diskutil erasevolume HFS+ 'ramdisk' `hdiutil attach -nomount ram://8388608`

mkdir -p /Volumes/ramdisk/src/runs/dbfiles/tmp

cp -r runs/dbfiles/ /Volumes/ramdisk/src/runs/dbfiles/

cp runs/startDatabase /Volumes/ramdisk/src/runs/

cp runs/queries.txt /Volumes/ramdisk/src/runs/

cp justRun.sh /Volumes/ramdisk/src/

cp -r visualisation /Volumes/ramdisk/src

elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]
then

 # mkdir /mnt/ramdisk

 # mount -t tmpfs -o size=5120m,mode=777 tmpfs /mnt/ramdisk

disk=ramdisk

rm /mnt/$disk/src/runs/startDatabase

mkdir -p /mnt/$disk/src/runs/dbfiles/tmp

cp -r runs/dbfiles/ /mnt/$disk/src/runs/

cp runs/startDatabase /mnt/$disk/src/runs/

cp justRun.sh /mnt/$disk/src/

cp -r visualisation /mnt/$disk/src/

cp fullExperiments.sh /mnt/$disk/src/

cp runs/queries.txt /mnt/$disk/src/runs/

elif [ -n "$COMSPEC" -a -x "$COMSPEC" ]
then 
  echo $0: this script does not support Windows \:\(
fi






