#!/bin/bash

#Case1
pool=$1
budget=$2
hist=$3
thre=$4
reorder=$5
depth=$6
nquer=$7
clever=0
alg=0
q=100
part=1

segment=0.05


if [[ $1 -eq 0 ]] ; then
    echo "parameter 1 not given, default value 50000 is given"
    pool=50000
fi

if [[ $2 -eq 0 ]] ; then
    echo "parameter 2 not given, default value 100000 is given"
    budget=100000
fi

if [[ $3 -eq "-1" ]] ; then
    echo "parameter 3 not given, default value 0 is given"
    hist=0
fi

if [[ $4 -eq 0 ]] ; then
    echo "parameter 4 not given, default value 1 is given"
    thre=4
fi

if [[ $5 -eq "-1" ]] ; then
    echo "parameter 5 not given, default value 0 is given"
    reorder=0
fi

if [[ $6 -eq 0 ]] ; then
    echo "depth 6 not given, default value 10 is given"
    depth=10
fi
if [[ $7 -eq 0 ]] ; then
    echo "depth 7 not given, default value 10 is given"
    nquer=500
fi




ramdiskPath="/mnt/ramdisk/src"
projectPath="/home/mike/Dropbox/Phd/pbm2015/s1250553/src"

# Change this according to the platform the script should be ran
version=MAC

if [ "$(uname)" == "Darwin" ]
then
version=MAC

elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]
then
version=RAM


elif [ -n "$COMSPEC" -a -x "$COMSPEC" ]
then 
  echo $0: this script does not support Windows \:\(
fi

if [ $version == "MAC" ]
then
  	echo "mac"
  	
  	ramdiskPath="/Volumes/ramdisk/src"
	projectPath="/Users/mike/Dropbox/Phd/pbm2015/s1250553/src"
else
	if [ $version == "RAM" ]
	then
		echo "unix"
		ramdiskPath="/mnt/ramdisk/src"
		projectPath="/home/mike/Dropbox/Phd/pbm2015/s1250553/src"
	else
		echo "pmfs"
		ramdiskPath="/mnt/pmfs/src"
		projectPath="/home/andchat/mike/s1250553/src"
	fi
fi


ramdiskPathExe="$ramdiskPath/runs/"
projectPathExe="$projectPath/runs/startDatabase"
resultsPath="$projectPath/safeExperimentResults/executionTime.txt"
resultsPathExcel="$projectPath/safeExperimentResults/executionTimeExcel.txt"
resultsPathExcelWrites="$projectPath/safeExperimentResults/executionTimeExcelWrites.txt"

echo "-------------------------- PATH DETAILS ---------------------"
echo $ramdiskPath
echo $ramdiskPathExe
echo $projectPath
echo $projectPathExe
echo $resultsPath
echo $resultsPathExcel
echo "-------------------------- PARAMETER DETAILS ---------------------"
echo "start experiments"
echo "poolSize: $pool"
echo "budget: $budget"
echo "hist: $hist"
echo "thre: $thre"
echo "reorder: $reorder"
echo "depth: $depth"
echo "nqueries: $nquer"




copyExecutable(){
	rm "$ramdiskPathExe/startDatabase"
	cp $projectPathExe $ramdiskPathExe
}


goToRamdisk(){
	cd $ramdiskPath
}

goToProject(){
	cd $projectPath
}

copyGraph(){
	echo "copyGraph"
	#find index that the new file should have
	filesCounter=$(ls $projectPath/graphs/input/* | wc -l)
	# cp "$ramdiskPath/runs/graph.txt" "$projectPath/graphs/input/graph$filesCounter.txt"
	newFilename="$projectPath/graphs/input/graph""$(echo -ne $filesCounter).txt"


	# echo $newFilename


	cp $ramdiskPath/runs/graph.txt $newFilename

	echo "$ramdiskPath/runs/graph.txt"
	echo $newFilename

 	tail -32 runs/executionTime.txt >> $resultsPath;
 	tail -2 runs/executionTimeExcel.txt >> $resultsPathExcel;
 	tail -2 runs/executionTimeExcelWrites.txt >> $resultsPathExcelWrites;
 	tail -2 runs/executionTimeExcel.txt

	# cp $projectPathExe $ramdiskPathExe
}

goToGraphFolder(){
	cd $projectPath/graphs
	ls -al -h
}

removeGraphs(){
	$(rm $projectPath/graphs/input/*graph*)
}

generateGraph(){
	ls
	cd graphs
	cp "$projectPath/graphs/input/*graph*" input/old
	./script.sh 1
	sleep 10
	ls input/*graph*
}



# generateGraph

# echo "exit"
# exit

# echo "exit"


# gg(){
# 	#find index that the new file should have
# 	filesCounter=$(ls $projectPath/graphs/input/graph* | wc -l)
# 	# cp "$ramdiskPath/runs/graph.txt" "$projectPath/graphs/input/graph$filesCounter.txt"
# 	echo "$projectPath/graphs/input/graph""$(echo -ne $filesCounter).txt"

# }


# gg

# sleep 200

# echo "goToRamdisk"
# goToRamdisk
# ls -al -h runs
# pwd
# echo "goToProject"
# goToProject
# pwd




# # -------------------- Ranked Based ------------------------------

removeGraphs

# sed -i -- 's/DDUMMY1/DLRUTEST/g' Makefile
# sed -i -- 's/DCLEANTEST/DDUMMY2/g' Makefile

# make -j 4 clean
# make -j 4 runs/startDatabase


# echo "Create ramdisk"
# ./ramDiskScript.sh


# goToRamdisk


# Run without optimisation simple algorithms


# reorder=0
# clever=0
# alg=1
# ./justRun.sh --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q  --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
# copyGraph

# sleep 10

# # Clever algorithms
# clever=1
# alg=1
# ./justRun.sh --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q  --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
# copyGraph

# sleep 10

# # Clever algorithms
# clever=1
# alg=1
# reorder=1
# ./justRun.sh --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q  --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
# reorder=0
# copyGraph

# sleep 10


# clever=0
# alg=4
# ./justRun.sh --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q  --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
# copyGraph

# sleep 10

# clever=1
# alg=4
# ./justRun.sh --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q  --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
# copyGraph

# sleep 10

# clever=1
# reorder=1
# alg=4
# ./justRun.sh --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q  --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
# reorder=0
# copyGraph

# sleep 10


# # # -------------------- CLEAN WITH TWO LISTS------------------------------




echo "START CLRU WITH TWO LISTS"


 # sed -i -- 's/DLRUTEST/DDUMMY1/g' Makefile
 # sed -i -- 's/DDUMMY2/DCLEANTEST/g' Makefile
 # sed -i -- 's/DCLEANTEST2/DCLEANTEST/g' Makefile




 goToProject


 sed -i -- 's/DAUXILIARY_MERGED/DPLACEHOLDER/g' Makefile


 # make clean 
 # make -j 4 runs/startDatabase

 echo "copy new Executable startDatabase"
 copyExecutable
 goToRamdisk



#RUN FOR PESSIMISTIC COST 
costFuture=0

sleep 5
clever=0
alg=2
./justRun.sh --costCalculator=$costFuture --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q  --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
 copyGraph


sleep 5
clever=1
alg=2
./justRun.sh --costCalculator=$costFuture --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q  --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
 copyGraph


sleep 5
clever=1
alg=3
./justRun.sh --costCalculator=$costFuture --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q  --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
 copyGraph


# # #RUN FOR COST BASED ON QUEUE-ANALYSIS 
# costFuture=1

# sleep 5

# clever=1
# alg=2
# ./justRun.sh --costCalculator=$costFuture --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q  --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
#  copyGraph

# sleep 5

# clever=1
# alg=3
# ./justRun.sh --costCalculator=$costFuture --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q  --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
#  copyGraph

# sleep 5


# goToProject
make clean
#RUN FOR DIFFERENT ALGORITHM3
sed -i -- 's/DPLACEHOLDER/DAUXILIARY_MERGED/g' Makefile
make -j 4 runs/startDatabase
copyExecutable
goToRamdisk

# #RUN FOR PESSIMISTIC COST 
# costFuture=0
# algorithm3Case=1

# clever=1
# alg=3
# ./justRun.sh --costCalculator=$costFuture --algorithmCase=$algorithm3Case --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q  --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
#  copyGraph

# costFuture=0
# algorithm3Case=2

# clever=1
# alg=3
# ./justRun.sh --costCalculator=$costFuture --algorithmCase=$algorithm3Case --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q  --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
#  copyGraph


# #RUN FOR QUEUE ANALYSIS COST 
# costFuture=1
# algorithm3Case=1

# clever=1
# alg=3
# ./justRun.sh --costCalculator=$costFuture --algorithmCase=$algorithm3Case --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q  --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
#  copyGraph

# costFuture=1
# algorithm3Case=2

# clever=1
# alg=3
# ./justRun.sh --costCalculator=$costFuture --algorithmCase=$algorithm3Case --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q  --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
#  copyGraph



# clever=1
# alg=2
# reorder=1
# ./justRun.sh --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q  --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
# reorder=0
# copyGraph



# # -------------------- My Replacement algorithms ------------------------------

# goToProject

#  sed -i -- 's/DLRUTEST/DDUMMY1/g' Makefile
#  sed -i -- 's/DDUMMY2/DCLEANTEST2/g' Makefile
#  sed -i -- 's/DCLEANTEST/DCLEANTEST2/g' Makefile

#  make -j 4 clean
#  make -j 4 runs/startDatabase

# # echo "create randisk: First Time"
# copyExecutable
# goToRamdisk

# sleep 5

# # #Run Without Recycling Ranked Based

# clever=1
# alg=3
# ./justRun.sh --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
# copyGraph

# sleep 5

# clever=1
# alg=3
# reorder=1
# ./justRun.sh --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
# reorder=0
# copyGraph

# sleep 5

# clever=1
# alg=7
# ./justRun.sh --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
# copyGraph

# sleep 10

# clever=1
# alg=7
# reorder=1
# ./justRun.sh --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
# reorder=0
# copyGraph

# sleep 10

# segment=0.1
# clever=1
# alg=8
# ./justRun.sh --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
# copyGraph

# sleep 10

# segment=0.1
# clever=1
# reorder=1
# alg=8
# ./justRun.sh --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
# reorder=0
# copyGraph

# sleep 10


# segment=0.3
# clever=1
# alg=8
# ./justRun.sh --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
# copyGraph

# sleep 10


# segment=0.3
# clever=1
# reorder=1
# alg=8
# ./justRun.sh --reorder=$reorder --pool_size=$pool --threads=$thre --budgetPermanent=$budget  --clever=$clever --algorithm=$alg --query=$q --nqueries=$nquer --history=$hist --segment=$segment --partitions=$part --depth=$depth
# reorder=0
# copyGraph

# sleep 10





# # -------------------- LRU and CLRU ------------------------------
# #Run Without optimisations LRU and CLEAN-LRU

goToProject

# sed -i -- 's/DLRUTEST/DDUMMY1/g' Makefile
# sed -i -- 's/DCLEANTEST/DDUMMY2/g' Makefile

generateGraph

goToProject
