#!/bin/bash


rm visualisation/pattern/*json
rm visualisation/pattern/*php
rm runs/dbfiles/test.table


cd runs

rm startDatabase

# Delete temporary files if execution fails
rm *_join_*
rm dbfiles/tmp/*
rm dbfiles/*.table_*
rm dbfiles/*.table.*
rm dbfiles/*persistent*

#compile file
cd ..
make -j 4 runs/startDatabase


STRING="./startDatabase $@"

echo $STRING


#run file
cd runs
eval $STRING
# ./startDatabase
echo "---------------- dbfiles folder content ----------------"
ls -al -h dbfiles
echo "---------------- dbfiles/tmp folder content ----------------"
ls -al -h dbfiles/tmp
cd ..

cd visualisation/pattern

# use this if you want to track the bufferpool state
# ./generateIndexPhpFiles.sh

cd ..
cd ..