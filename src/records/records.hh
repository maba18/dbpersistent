/*
 * records.hh
 *
 *  Created on: 18 Dec 2014
 *      Author: michail
 */

#ifndef S1250553_SRC_RECORDS_RECORDS_HH_
#define S1250553_SRC_RECORDS_RECORDS_HH_

#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>

#include <iostream>
#include "stdlib.h"
#include "stdio.h"
#include <fstream>
#include <string>

using namespace std;
using namespace boost;

struct PART {
  double P_RETAILPRICE;
  int P_PARTKEY;
  int P_SIZE;
  char P_NAME[56];
  char P_MFGR[26];
  char P_BRAND[11];
  char P_TYPE[26];
  char P_CONTAINER[11];
  char P_COMMENT[24];
  friend std::ostream& operator<<(std::ostream& o, const PART& r);
};

//Done
struct SUPPLIER {
  double S_ACCTBAL;
  int S_NATIONKEY;
  int S_SUPPKEY;
  char S_NAME[26];
  char S_ADDRESS[41];
  char S_PHONE[16];
  char S_COMMENT[102];
  friend std::ostream& operator<<(std::ostream& o, const SUPPLIER& r);
};

//Done
struct PARTSUPP {
  double PS_SUPPLYCOST;
  int PS_PARTKEY;
  int PS_SUPPKEY;
  int PS_AVAILQTY;
  char PS_COMMENT[199];
  friend std::ostream& operator<<(std::ostream& o, const PARTSUPP& r);
};

//Done
struct CUSTOMER {
  double C_ACCTBAL;
  int C_CUSTKEY;
  int C_NATIONKEY;
  char C_NAME[26];
  char C_ADDRESS[41];
  char C_PHONE[16];
  char C_MKTSEGMENT[11];
  char C_COMMENT[118];
  friend std::ostream& operator<<(std::ostream& o, const CUSTOMER& r);
};

struct ORDER {
  char O_COMMENT[80];
  char O_ORDERPRIORITY[16];
  char O_CLERK[16];
  char O_ORDERDATE[11];
  double O_TOTALPRICE;
  int O_SHIPPRIORITY;
  int O_ORDERKEY;
  int O_CUSTKEY;
  char O_ORDERSTATUS;
  friend std::ostream& operator<<(std::ostream& o, const ORDER& r);
};

struct LINEITEM {
  char L_COMMENT[45];
  char L_SHIPINSTRUCT[26];
  char L_SHIPDATE[11];
  char L_COMMITDATE[11];
  char L_RECEIPTDATE[11];
  char L_SHIPMODE[11];
  double L_QUANTITY;
  double L_EXTENDEDPRICE;
  double L_DISCOUNT;
  double L_TAX;
  int L_ORDERKEY;
  int L_PARTKEY;
  int L_SUPPKEY;
  int L_LINENUMBER;
  char L_RETURNFLAG;
  char L_LINESTATUS;
  friend std::ostream& operator<<(std::ostream& o, const LINEITEM& r);
};

//Done nation struct
struct NATION {
  char N_COMMENT[153];
  char N_NAME[26];
  int N_NATIONKEY;
  int N_REGIONKEY;
  friend std::ostream& operator<<(std::ostream& o, const NATION& r);
};

//Done
struct REGION {
  char R_COMMENT[153];
  char R_NAME[26];
  int R_REGIONKEY;
  friend std::ostream& operator<<(std::ostream& o, const REGION& r);
};


#endif /* S1250553_SRC_RECORDS_RECORDS_HH_ */
