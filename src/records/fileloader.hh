/*
 * fileloader.hh
 *
 *  Created on: Dec 18, 2014
 *      Author: maba18
 */

#ifndef FILELOADER_HH_
#define FILELOADER_HH_

#include <iostream>
#include <string>
 #include "../storage/BufferManager.hh"
#include "../storage/readwriters.hh"

//load files
class FileLoader {

public:


	FileLoader(size_t num_or_records) :
			cardinality(num_or_records), factor(1) {
	};
	FileLoader(size_t num_or_records, float fact) :
			cardinality(num_or_records), factor(fact) {
	}
	FileLoader(const std::string& fn, int num_or_records, float fact) :
			filename(fn), cardinality(num_or_records), factor(fact) {
	}
	;

	void loadFile(deceve::storage::BufferManager *bm, const std::string& fn, const std::string& fo);
	void loadRegionTable(deceve::storage::BufferManager *bm, const std::string& fo);
	void loadNationTable(deceve::storage::BufferManager *bm, const std::string& fo);
	void loadSupplierTable(deceve::storage::BufferManager *bm, const std::string& fo);
	void loadCustomerTable(deceve::storage::BufferManager *bm, const std::string& fo);
	void loadPartsuppTable(deceve::storage::BufferManager *bm, const std::string& fo);
	void loadLineItemTable(deceve::storage::BufferManager *bm, const std::string& fo);
	void loadOrdersTable(deceve::storage::BufferManager *bm, const std::string& fo);
	void loadPartTable(deceve::storage::BufferManager *bm, const std::string& fo);

private:
	std::string filename;
	size_t cardinality;
	float factor;

};

#endif /* FILELOADER_HH_ */
