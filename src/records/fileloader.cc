/*
 * fileloader.cc
 *
 *  Created on: Dec 18, 2014
 *      Author: maba18
 */

// reading a text file
#include <iostream>
#include "stdlib.h"
#include "stdio.h"
#include <fstream>
#include <string>
#include "fileloader.hh"
#include "records.hh"
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include <boost/filesystem.hpp>

#include "../storage/BTree.hh"
#include "../storage/Record.hh"

using namespace std;
using namespace boost;

// load files
void FileLoader::loadFile(deceve::storage::BufferManager* bm, const std::string& fn, const std::string& fo) {
  if (boost::filesystem::exists(fo)) {
    std::cout << "File" << fo << " exists so remove it and load it again!" << "\n";
    boost::filesystem::remove(fo);
  }

  filename = fn;
  if (fn == "../files/region.tbl") {
    loadRegionTable(bm, fo);
  } else if (fn == "../files/nation.tbl") {
    loadNationTable(bm, fo);
  } else if (fn == "../files/supplier.tbl") {
    loadSupplierTable(bm, fo);
  } else if (fn == "../files/customer.tbl") {
    loadCustomerTable(bm, fo);
  } else if (fn == "../files/partsupp.tbl") {
    loadPartsuppTable(bm, fo);
  } else if (fn == "../files/lineitem.tbl" || fn == "../files/lineitems.tbl") {
    loadLineItemTable(bm, fo);
  } else if (fn == "../files/orders.tbl") {
    loadOrdersTable(bm, fo);
  } else if (fn == "../files/part.tbl") {
    loadPartTable(bm, fo);
  }
}

void FileLoader::loadRegionTable(deceve::storage::BufferManager* bm, const std::string& outputFile) {
  string line;
  ifstream myfile(filename.c_str());
  REGION region;
  // deceve::storage::BufferManager bm(100 * deceve::storage::PAGE_SIZE_PERSISTENT);

  deceve::bama::Writer<REGION> writer(bm, outputFile, deceve::storage::PRIMARY);
  // int regionSize = sizeof region;
  // std::cout<<regionSize<<std::endl;
  int tuplesInPage = deceve::storage::PAGE_SIZE_PERSISTENT / sizeof region;
  std::cout << "Region tuplesInPage: " << tuplesInPage << std::endl;

  size_t count_rec = 1;
  if (myfile.is_open()) {
    while (getline(myfile, line) && count_rec < tuplesInPage * cardinality) {
      count_rec++;
      char_separator<char> sep("|");
      tokenizer<char_separator<char> > tokens(line, sep);
      vector<string> fields;
      BOOST_FOREACH (const string& t, tokens) {
        fields.push_back(t);
      }

      region.R_REGIONKEY = atoi(fields[0].c_str());
      ::memcpy(&region.R_NAME[0], fields[1].c_str(), 26);
      ::memcpy(&region.R_COMMENT[0], fields[2].c_str(), 153);
      writer.write(region);
    }
    myfile.close();
    writer.close();
  } else {
    std::cout << "Unable to open file: " << filename;
  }

  std::cout << "NUMBER OF TUPLES: " << count_rec << std::endl;
  // Read the generated file
  //	int counter = 0;
  //	deceve::bama::Reader<REGION> reader(&bm, outputFile);
  //	std::cout << "Start Reader";
  //	std::cout << "(R_REGIONKEY, R_NAME, R_COMMENT)" << "\n";
  //	while (reader.hasNext()) {
  //		std::cout << counter++ << " " << reader.nextRecord();
  //	}
  //	std::cout << "End Reader" << "\n";
  //	reader.close();
}

void FileLoader::loadNationTable(deceve::storage::BufferManager* bm, const std::string& outputFile) {
  string line;
  ifstream myfile(filename.c_str());

  NATION nation;
  // deceve::storage::BufferManager bm(100 * deceve::storage::PAGE_SIZE_PERSISTENT);

  deceve::bama::Writer<NATION> writer(bm, outputFile, deceve::storage::PRIMARY);

  int tuplesInPage = deceve::storage::PAGE_SIZE_PERSISTENT / sizeof nation;
  std::cout << "Nation tuplesInPage: " << tuplesInPage << std::endl;

  size_t count_rec = 1;
  if (myfile.is_open()) {
    while (getline(myfile, line) && count_rec < tuplesInPage * cardinality) {
      count_rec++;
      char_separator<char> sep("|");
      tokenizer<char_separator<char> > tokens(line, sep);
      vector<string> fields;
      BOOST_FOREACH (const string& t, tokens) {
        fields.push_back(t);
      }

      nation.N_NATIONKEY = atoi(fields[0].c_str());
      ::memcpy(&nation.N_NAME[0], fields[1].c_str(), 26);
      nation.N_REGIONKEY = atoi(fields[2].c_str());
      ::memcpy(&nation.N_COMMENT[0], fields[3].c_str(), 153);
      writer.write(nation);
    }
    myfile.close();
    writer.close();
  } else {
    std::cout << "Unable to open file: " << filename;
  }

  std::cout << "NUMBER OF TUPLES: " << count_rec << std::endl;
  //	int counter = 0;
  //	deceve::bama::Reader<NATION> reader(&bm, outputFile);
  //	std::cout << "Start Reader for: " << outputFile << "\n";
  //	std::cout << "(N_NATIONKEY, N_NAME, N_REGIONKEY, R_COMMENT)" << "\n";
  //	while (reader.hasNext()) {
  //		std::cout << counter++ << " " << reader.nextRecord();
  //	}
  //	std::cout << "End Reader" << "\n";
  //	reader.close();
}

void FileLoader::loadSupplierTable(deceve::storage::BufferManager* bm, const std::string& outputFile) {
  string line;
  ifstream myfile(filename.c_str());

  SUPPLIER record;
  // deceve::storage::BufferManager bm(100 * deceve::storage::PAGE_SIZE_PERSISTENT);

  deceve::bama::Writer<SUPPLIER> writer(bm, outputFile, deceve::storage::PRIMARY);

  int tuplesInPage = deceve::storage::PAGE_SIZE_PERSISTENT / sizeof record;
  std::cout << "SUPPLIER tuplesInPage: " << tuplesInPage << std::endl;

  size_t count_rec = 1;
  if (myfile.is_open()) {
    while (getline(myfile, line) && count_rec < tuplesInPage * cardinality) {
      count_rec++;
      char_separator<char> sep("|");
      tokenizer<char_separator<char> > tokens(line, sep);
      vector<string> fields;
      BOOST_FOREACH (const string& t, tokens) {
        fields.push_back(t);
      }

      record.S_SUPPKEY = atoi(fields[0].c_str());
      ::memcpy(&record.S_NAME[0], fields[1].c_str(), 26);
      ::memcpy(&record.S_ADDRESS[0], fields[2].c_str(), 41);
      record.S_NATIONKEY = atoi(fields[3].c_str());
      ::memcpy(&record.S_PHONE[0], fields[4].c_str(), 16);
      record.S_ACCTBAL = atof(fields[5].c_str());
      ::memcpy(&record.S_COMMENT[0], fields[6].c_str(), 102);

      writer.write(record);
    }
    myfile.close();
    writer.close();
  } else {
    std::cout << "Unable to open file: " << filename;
  }
  std::cout<<"NUMBER OF TUPLES: "<<count_rec<<std::endl;
}

// void FileLoader::loadSupplierTable(deceve::storage::BufferManager *bm,
//                                   const std::string& outputFile) {
//
//  //
//  //  unsigned long val = 0;
//  //
//  //  std::cout << "Start inserting items" << std::endl;
//  //  for (int times = 0; times < 10000; times++) {
//  //    val = rand() % numeric_limits<long>::max();
//  //    testTree.insert(
//  //        deceve::storage::makeRecord<deceve::storage::MyKey,
//  //            deceve::storage::MyPayload>(deceve::storage::makeKey(times),
//  // deceve::storage::makePayload(val)));
//  //  }
//  //
//  //  int total = 0;
//  //    for (MyBTree::iterator it2 = testTree.begin(); it2 != testTree.end();)
//  {
//  //      cout << " Key: " << it2->key << "\t Payload: " << it2->payload <<
//  endl;
//  //      it2++;
//  //      total++;
//  //    }
//  //
//  //    auto it = testTree.find(deceve::storage::makeKey(9553));
//  //
//  //    if (it != testTree.end()) {
//  //      std::cout << it->payload << std::endl;
//  //    }
//  //
//  //    cout << "Total records in b-tree: " << total << endl;
//
//  string line;
//  ifstream myfile(filename.c_str());
//
//  SUPPLIER record;
//  // deceve::storage::BufferManager bm(100 * deceve::storage::PAGE_SIZE_PERSISTENT);
//
//  typedef deceve::storage::BTree<deceve::storage::MyKey, SUPPLIER>
//  SupplierBTree;
//  typedef SupplierBTree::record_type MyRecord;
//
//  SupplierBTree testTree(bm, outputFile);
//
//  size_t count_rec = 1;
//  if (myfile.is_open()) {
//    while (getline(myfile, line) && count_rec < cardinality) {
//      count_rec++;
//      char_separator<char> sep("|");
//      tokenizer<char_separator<char> > tokens(line, sep);
//      vector<string> fields;
//      BOOST_FOREACH (const string& t, tokens){
//      fields.push_back(t);
//    }
//
//      record.S_SUPPKEY = atoi(fields[0].c_str());
//      ::memcpy(&record.S_NAME[0], fields[1].c_str(), 26);
//      ::memcpy(&record.S_ADDRESS[0], fields[2].c_str(), 41);
//      record.S_NATIONKEY = atoi(fields[3].c_str());
//      ::memcpy(&record.S_PHONE[0], fields[4].c_str(), 16);
//      record.S_ACCTBAL = atof(fields[5].c_str());
//      ::memcpy(&record.S_COMMENT[0], fields[6].c_str(), 102);
//
//      testTree.insert(
//          deceve::storage::makeRecord<deceve::storage::MyKey, SUPPLIER>(
//              deceve::storage::makeKey(record.S_SUPPKEY), record));
//
//    }
//    myfile.close();
////    writer.close();
//  } else
//    std::cout << "Unable to open file: " << filename;
//
//  int total = 0;
//  for (SupplierBTree::iterator it2 = testTree.begin(); it2 != testTree.end();)
//  {
//    cout << " Key: " << it2->key << "\t Payload: " << it2->payload << "\n";
//    it2++;
//    total++;
//  }
//
//}

void FileLoader::loadCustomerTable(deceve::storage::BufferManager* bm, const std::string& outputFile) {
  string line;
  ifstream myfile(filename.c_str());

  CUSTOMER record;
  // deceve::storage::BufferManager bm(100 * deceve::storage::PAGE_SIZE_PERSISTENT);

  deceve::bama::Writer<CUSTOMER> writer(bm, outputFile, deceve::storage::PRIMARY);

  int tuplesInPage = deceve::storage::PAGE_SIZE_PERSISTENT / sizeof record;
  std::cout << "CUSTOMER tuplesInPage: " << tuplesInPage << std::endl;

  size_t count_rec = 1;
  if (myfile.is_open()) {
    while (getline(myfile, line) && count_rec < tuplesInPage * cardinality) {
      count_rec++;
      char_separator<char> sep("|");
      tokenizer<char_separator<char> > tokens(line, sep);
      vector<string> fields;
      BOOST_FOREACH (const string& t, tokens) {
        fields.push_back(t);
      }

      record.C_CUSTKEY = atoi(fields[0].c_str());
      ::memcpy(&record.C_NAME, fields[1].c_str(), 26);
      ::memcpy(&record.C_ADDRESS, fields[2].c_str(), 41);
      record.C_NATIONKEY = atoi(fields[3].c_str());
      ::memcpy(&record.C_PHONE, fields[4].c_str(), 16);
      record.C_ACCTBAL = atof(fields[5].c_str());
      ::memcpy(&record.C_MKTSEGMENT, fields[6].c_str(), 11);
      ::memcpy(&record.C_COMMENT, fields[7].c_str(), 118);

      writer.write(record);
    }
    myfile.close();
    writer.close();
  } else {
    std::cout << "Unable to open file: " << filename;
  }
  std::cout << "NUMBER OF TUPLES: " << count_rec << std::endl;

  //	int counter = 0;
  //	deceve::bama::Reader<CUSTOMER> reader(&bm, outputFile);
  //	std::cout << "Start Reader for: " << outputFile << "\n";
  //	std::cout
  //			<< "(C_CUSTKEY, C_CUSTKEY[25], C_ADDRESS[40], C_NATIONKEY,
  //C_PHONE[15], C_ACCTBAL, C_MKTSEGMENT[10], C_COMMENT[117] )"
  //			<< "\n";
  //
  //	while (reader.hasNext()) {
  //		std::cout << counter++ << " " << reader.nextRecord();
  //	}
  //	std::cout << "End Reader" << "\n";
  //	reader.close();
}

void FileLoader::loadPartsuppTable(deceve::storage::BufferManager* bm, const std::string& outputFile) {
  string line;
  ifstream myfile(filename.c_str());

  PARTSUPP record;
  // deceve::storage::BufferManager bm(100 * deceve::storage::PAGE_SIZE_PERSISTENT);

  deceve::bama::Writer<PARTSUPP> writer(bm, outputFile, deceve::storage::PRIMARY);

  int tuplesInPage = deceve::storage::PAGE_SIZE_PERSISTENT / sizeof record;
  std::cout << "PARTSUPP tuplesInPage: " << tuplesInPage << std::endl;

  size_t count_rec = 1;
  if (myfile.is_open()) {
    while (getline(myfile, line) && count_rec < tuplesInPage * cardinality) {
      count_rec++;
      char_separator<char> sep("|");
      tokenizer<char_separator<char> > tokens(line, sep);
      vector<string> fields;
      BOOST_FOREACH (const string& t, tokens) {
        fields.push_back(t);
      }

      record.PS_PARTKEY = atoi(fields[0].c_str());
      record.PS_SUPPKEY = atoi(fields[1].c_str());
      record.PS_AVAILQTY = atoi(fields[2].c_str());
      record.PS_SUPPLYCOST = atof(fields[3].c_str());
      ::memcpy(&record.PS_COMMENT, fields[4].c_str(), 199);

      writer.write(record);
    }
    myfile.close();
    writer.close();
  } else {
    std::cout << "Unable to open file: " << filename;
  }
  std::cout << "NUMBER OF TUPLES: " << count_rec << std::endl;
  //	int counter = 0;
  //	deceve::bama::Reader<PARTSUPP> reader(&bm, outputFile);
  //	std::cout << "Start Reader for: " << outputFile << "\n";
  //	std::cout
  //			<< "(PS_PARTKEY, PS_SUPPKEY, PS_AVAILQTY, PS_SUPPLYCOST,
  //PS_COMMENT )";
  //	std::cout << "\n";
  //
  //	while (reader.hasNext()) {
  //		std::cout << counter++ << " " << reader.nextRecord();
  //	}
  //	std::cout << "End Reader" << "\n";
  //	reader.close();
}

void FileLoader::loadLineItemTable(deceve::storage::BufferManager* bm, const std::string& outputFile) {
  string line;
  ifstream myfile(filename.c_str());

  LINEITEM record;
  // deceve::storage::BufferManager bm(100 * deceve::storage::PAGE_SIZE_PERSISTENT);

  deceve::bama::Writer<LINEITEM> writer(bm, outputFile, deceve::storage::PRIMARY);
  int tuplesInPage = deceve::storage::PAGE_SIZE_PERSISTENT / sizeof record;
  std::cout << "LINEITEM tuplesInPage: " << tuplesInPage << std::endl;

  size_t count_rec = 1;
  if (myfile.is_open()) {
    while (getline(myfile, line) && count_rec < tuplesInPage * cardinality) {
      count_rec++;
      char_separator<char> sep("|");
      tokenizer<char_separator<char> > tokens(line, sep);
      vector<string> fields;
      BOOST_FOREACH (const string& t, tokens) {
        fields.push_back(t);
      }

      record.L_ORDERKEY = atoi(fields[0].c_str());
      record.L_PARTKEY = atoi(fields[1].c_str());
      record.L_SUPPKEY = atoi(fields[2].c_str());
      record.L_LINENUMBER = atoi(fields[3].c_str());
      record.L_QUANTITY = atof(fields[4].c_str());
      record.L_EXTENDEDPRICE = atof(fields[5].c_str());
      record.L_DISCOUNT = atof(fields[6].c_str());
      record.L_TAX = atof(fields[7].c_str());
      ::memcpy(&record.L_RETURNFLAG, fields[8].c_str(), 1);
      ::memcpy(&record.L_LINESTATUS, fields[9].c_str(), 1);
      ::memcpy(&record.L_SHIPDATE[0], fields[10].c_str(), 11);
      ::memcpy(&record.L_COMMITDATE[0], fields[11].c_str(), 11);
      ::memcpy(&record.L_RECEIPTDATE[0], fields[12].c_str(), 11);
      ::memcpy(&record.L_SHIPINSTRUCT[0], fields[13].c_str(), 26);
      ::memcpy(&record.L_SHIPMODE[0], fields[14].c_str(), 11);
      ::memcpy(&record.L_COMMENT[0], fields[15].c_str(), 46);

      writer.write(record);
    }
    myfile.close();
    writer.close();
  } else {
    std::cout << "Unable to open file: " << filename;
  }
  std::cout << "NUMBER OF TUPLES: " << count_rec << std::endl;
}

void FileLoader::loadOrdersTable(deceve::storage::BufferManager* bm, const std::string& outputFile) {
  string line;
  ifstream myfile(filename.c_str());

  ORDER record;
  // deceve::storage::BufferManager bm(100 * deceve::storage::PAGE_SIZE_PERSISTENT);

  deceve::bama::Writer<ORDER> writer(bm, outputFile, deceve::storage::PRIMARY);

  int tuplesInPage = deceve::storage::PAGE_SIZE_PERSISTENT / sizeof record;
  std::cout << "ORDER tuplesInPage: " << tuplesInPage << std::endl;

  size_t count_rec = 1;
  if (myfile.is_open()) {
    while (getline(myfile, line) && count_rec < tuplesInPage * cardinality) {
      count_rec++;
      char_separator<char> sep("|");
      tokenizer<char_separator<char> > tokens(line, sep);
      vector<string> fields;
      BOOST_FOREACH (const string& t, tokens) {
        fields.push_back(t);
      }

      record.O_ORDERKEY = atoi(fields[0].c_str());
      record.O_CUSTKEY = atoi(fields[1].c_str());
      ::memcpy(&record.O_ORDERSTATUS, fields[2].c_str(), 1);
      record.O_TOTALPRICE = atof(fields[3].c_str());
      ::memcpy(&record.O_ORDERDATE[0], fields[4].c_str(), 11);
      ::memcpy(&record.O_ORDERPRIORITY[0], fields[5].c_str(), 16);
      ::memcpy(&record.O_CLERK[0], fields[6].c_str(), 16);
      record.O_SHIPPRIORITY = atoi(fields[7].c_str());
      ::memcpy(&record.O_COMMENT[0], fields[8].c_str(), 80);

      writer.write(record);
    }
    myfile.close();
    writer.close();
  } else {
    std::cout << "Unable to open file: " << filename;
  }
  std::cout << "NUMBER OF TUPLES: " << count_rec << std::endl;
}

void FileLoader::loadPartTable(deceve::storage::BufferManager* bm, const std::string& outputFile) {
  string line;
  ifstream myfile(filename.c_str());

  PART record;
  // deceve::storage::BufferManager bm(100 * deceve::storage::PAGE_SIZE_PERSISTENT);

  deceve::bama::Writer<PART> writer(bm, outputFile, deceve::storage::PRIMARY);

  int tuplesInPage = deceve::storage::PAGE_SIZE_PERSISTENT / sizeof record;
  std::cout << "PART tuplesInPage: " << tuplesInPage << std::endl;

  size_t count_rec = 1;
  if (myfile.is_open()) {
    while (getline(myfile, line) && count_rec < tuplesInPage * cardinality) {
      count_rec++;
      char_separator<char> sep("|");
      tokenizer<char_separator<char> > tokens(line, sep);
      vector<string> fields;
      BOOST_FOREACH (const string& t, tokens) {
        fields.push_back(t);
      }

      record.P_PARTKEY = atoi(fields[0].c_str());
      ::memcpy(&record.P_NAME[0], fields[1].c_str(), 56);
      ::memcpy(&record.P_MFGR[0], fields[2].c_str(), 26);
      ::memcpy(&record.P_BRAND[0], fields[3].c_str(), 11);
      ::memcpy(&record.P_TYPE[0], fields[4].c_str(), 26);
      record.P_SIZE = atoi(fields[5].c_str());
      ::memcpy(&record.P_CONTAINER[0], fields[6].c_str(), 11);
      record.P_RETAILPRICE = atof(fields[7].c_str());
      ::memcpy(&record.P_COMMENT[0], fields[8].c_str(), 24);

      writer.write(record);
    }
    myfile.close();
    writer.close();
  } else {
    std::cout << "Unable to open file: " << filename;
  }
  std::cout << "NUMBER OF TUPLES: " << count_rec << std::endl;
}
