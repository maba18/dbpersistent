/*
 * records.cc
 *
 *  Created on: Dec 18, 2014
 *      Author: maba18
 */

#include <iostream>
#include <string>
#include "records.hh"

std::ostream& operator<<(std::ostream& o, const NATION& r) {
	o << "[";
	o << r.N_NATIONKEY << " ,";
	o << " " << r.N_NAME << " ,";
	o << " " << r.N_REGIONKEY << " ,";
	o << " " << r.N_COMMENT << " ]" << "\n";
	return o;
}

std::ostream& operator<<(std::ostream& o, const REGION& r) {
	o << "[";
	o << " " << r.R_REGIONKEY << " ,";
	o << " " << r.R_NAME << " ,";
	o << " " << r.R_COMMENT << " ]" << "\n";
	return o;
}

std::ostream& operator<<(std::ostream& o, const SUPPLIER& r) {
	o << "[";
	o << " " << r.S_SUPPKEY << " ,";
	o << " " << r.S_NAME << " ,";
	o << " " << r.S_ADDRESS << " ,";
	o << " " << r.S_NATIONKEY << " ,";
	o << " " << r.S_PHONE << " ,";
	o << " " << r.S_ACCTBAL << " ,";
	o << " " << r.S_COMMENT << " ]" << "\n";
	return o;
}

std::ostream& operator<<(std::ostream& o, const CUSTOMER& r) {
	o << "[";
	o << " " << r.C_CUSTKEY << ",";
	o << " " << r.C_NAME << ",";
	o << " " << r.C_ADDRESS << ",";
	o << " " << r.C_NATIONKEY << ",";
	o << " " << r.C_PHONE << ",";
	o << " " << r.C_ACCTBAL << ",";
	o << " " << r.C_MKTSEGMENT << ",";
	o << " " << r.C_COMMENT << "]" << "\n";
	return o;
}
std::ostream& operator<<(std::ostream& o, const PARTSUPP& r) {
	o << "[";
	o << " " << r.PS_PARTKEY << " ,";
	o << " " << r.PS_SUPPKEY << " ,";
	o << " " << r.PS_AVAILQTY << " ,";
	o << " " << r.PS_AVAILQTY << " ,";
	o << " " << r.PS_COMMENT << " ]" << "\n";
	return o;
}
// print lineitem
std::ostream& operator<<(std::ostream& o, const LINEITEM& r) {
	o << "[";
	o << " " << r.L_ORDERKEY << ",";
	o << " " << r.L_PARTKEY << ",";
	o << " " << r.L_SUPPKEY << ",";
	o << " " << r.L_LINENUMBER << ",";
	o << " " << r.L_QUANTITY << ",";
	o << " " << r.L_EXTENDEDPRICE << ",";
	o << " " << r.L_DISCOUNT << ",";
	o << " " << r.L_TAX << ",";
	o << " " << r.L_RETURNFLAG << ",";
	o << " " << r.L_LINESTATUS << ",";
	o << " " << r.L_SHIPDATE << ",";
	o << " " << r.L_COMMITDATE << ",";
	o << " " << r.L_RECEIPTDATE << ",";
	o << " " << r.L_SHIPINSTRUCT << ",";
	o << " " << r.L_SHIPMODE << ",";
	o << " " << r.L_COMMENT << "]" << "\n";
	return o;
}

std::ostream& operator<<(std::ostream& o, const ORDER& r) {
	o << "[";
	o << "" << r.O_ORDERKEY << "";
	o << "|" << r.O_CUSTKEY << "";
	o << "|" << r.O_ORDERSTATUS << "";
	o << "|" << r.O_TOTALPRICE << "";
	o << "|" << r.O_ORDERDATE << "";
	o << "|" << r.O_ORDERPRIORITY << "";
	o << "|" << r.O_CLERK << "";
	o << "|" << r.O_SHIPPRIORITY << "";
	o << "|" << r.O_COMMENT << "]" << "\n";
	return o;

}

std::ostream& operator<<(std::ostream& o, const PART& r) {
	o << "[";
	o << "" << r.P_PARTKEY << "";
	o << "|" << r.P_NAME << "";
	o << "|" << r.P_MFGR << "";
	o << "|" << r.P_BRAND << "";
	o << "|" << r.P_TYPE << "";
	o << "|" << r.P_SIZE << "";
	o << "|" << r.P_CONTAINER << "";
	o << "|" << r.P_RETAILPRICE << "";
	o << "|" << r.P_COMMENT << "]" << "\n";
	return o;

}

