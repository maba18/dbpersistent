#!/bin/bash

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
srcDir="$(dirname "$currentDir")"

echo $currentDir
echo $srcDir



if [ "$(uname)" == "Darwin" ]; then
    # Do something under Mac OS X platform  
    echo "####################################---DOWNLOAD DBGEN for mac-os-x ---########################################";

    wget https://dl.dropboxusercontent.com/u/29393464/phdPublic/tpch_mac.zip

    echo "####################################---UNZIP tpch_mac.zip---####################################";
    unzip tpch_mac.zip

    echo "####################################---BUILD DBGEN---####################################";
    cd tpch_mac/dbgen

    make all
    echo "####################################---RUN dbgen with default values---####################################";
    ./dbgen      
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    echo "####################################---DOWNLOAD DBGEN---########################################";
    wget https://dl.dropboxusercontent.com/u/29393464/phdPublic/tpch_2_17_0.zip

    echo "####################################---UNZIP tpch_2_17_0.zip---####################################";
    unzip tpch_2_17_0.zip

    echo "####################################---BUILD DBGEN---####################################";
    cd tpch_2_17_0/dbgen

    # Do something under Linux platform
    echo "####################################---Linux---####################################";
    mv makefile.linux Makefile
    make all
    echo "####################################---RUN dbgen with default values---####################################";
    ./dbgen
    
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
    # Do something under Windows NT platform
    echo "Other";
fi


echo "####################################---Copy *.tbl file to proper folder (called files) ---####################################";
mkdir $srcDir/files/
mv *.tbl $srcDir/files/
cd $srcDir

echo "####################################---Compile File Loader ---####################################";

make -j 4 runs/fileload
cd runs
mkdir -p dbfiles/tmp

echo "####################################---Load files to database ---####################################";
./loadScaledFiles.sh

echo "####################################---Start running queries ---####################################";

 cd $srcDir
# ./compileAndRun.sh
make -j 4 runs/startDatabase

cd tpchWorkloadFiles

if [ "$(uname)" == "Darwin" ]; then

    rm tpch_mac.zip

    rm -r tpch_mac

    rm -r __MACOSX

elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then

    rm tpch_2_17_0.zip

    rm -r tpch_2_17_0

elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
    # Do something under Windows NT platform
    echo "Other";
fi

