#ifndef STARTDATABASE_HH_
#define STARTDATABASE_HH_

// Include c
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>

// Include Boost Functions (as c libraries)
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

// Include c++
#include <chrono>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>

// include STL containers
#include <deque>
#include <queue>
#include <vector>

#include <atomic>
#include <condition_variable>
#include <mutex>
#include <thread>

// Include threading functions
#include "ThreadPool.hh"

// Include General Functions
#include "../storage/io.hh"
#include "../utils/defs.hh"
#include "../utils/global.hh"
#include "../utils/types.hh"
#include "../utils/util.hh"

// Include Storage Classes
#include "../storage/BufferManager.hh"
#include "../storage/IterableQueue.hh"

// Include Storage Classes
#include "../queryplans/NodeTree.hh"
#include "../queryplans/PoolNode.hh"
#include "../queryplans/QGenerator.hh"
#include "../queryplans/QueryManager.hh"
#include "../queryplans/QueueManager.hh"

#include <chrono>

//// Include Query Classes
//#include "../queryplans/queries/MyQuery.hh"
//#include "../queryplans/queries/MyQueryJoin.hh"
//#include "../queryplans/queries/MyQuery1.hh"
//#include "../queryplans/queries/MyQuery2.hh"
//#include "../queryplans/queries/MyQuery3.hh"
//#include "../queryplans/queries/MyQuery4.hh"
//#include "../queryplans/queries/MyQuery5.hh"
//#include "../queryplans/queries/MyQuery6.hh"
//#include "../queryplans/queries/MyQuery7.hh"
//#include "../queryplans/queries/MyQuery8.hh"
//#include "../queryplans/queries/MyQuery9.hh"
//#include "../queryplans/queries/MyQuery10.hh"
//#include "../queryplans/queries/MyQuery11.hh"
//#include "../queryplans/queries/MyQuery12.hh"
//#include "../queryplans/queries/MyQuery13.hh"
//#include "../queryplans/queries/MyQuery14.hh"
//#include "../queryplans/queries/MyQuery15.hh"
//#include "../queryplans/queries/MyQuery16.hh"
//#include "../queryplans/queries/MyQuery17.hh"
//#include "../queryplans/queries/MyQuery18.hh"
//#include "../queryplans/queries/MyQuery19.hh"
//#include "../queryplans/QOperator.hh"

using namespace std;
using namespace boost;

namespace po = boost::program_options;
namespace db = deceve::bama;
namespace dq = deceve::queries;

// Global variables declaration
// Number of workers
ThreadPool *threadPool;

std::atomic<int> *queueDepth;

// Execution Parameters
double tps;
// double counterOfQueriesExecuted;
size_t  nQueries, cleverFlag, historyFlag;

// Variables for tracking queries runtimes
ofstream detailedStatisticsFile, executionTimeFile;
uint64_t startOperatorTime, endOperatorTime;

// Task queue implementation
iterable_queue<deceve::queries::QOperator *> taskQueue;

// std::queue<int> randomNumbers;
// std::queue<int> randomPauses;

// std::vector<int> ids = {1,  2,  3,  4,  5,  6,  7,  8,  9,  10,
//                        11, 12, 13, 14, 15, 16, 17, 18, 19, 20};

// std::vector<std::vector<int> > groups = {{8, 9, 14, 17, 12},
//                                         {5, 18},
//                                         {7, 13},
//                                         {2, 9, 16},
//                                         {1, 3, 4, 6, 10, 11, 15, 19}};

int check = 0;

std::mutex fileLock;
// use monitor for counting the number of threads that are currently running
std::atomic<int> monitor{0};
std::atomic<int> counterOfQueriesExecuted;
std::mutex printMutex;

static int dirtyStatisticsCounter{0};
std::mutex threadQueueMutex;
bool running = {false};

int query_task_execution(deceve::queries::QOperator *qop,
                         deceve::storage::BufferManager *bm) {
  (void)bm;
  monitor++;
  startOperatorTime = db::rdtsc();
  qop->run();
  endOperatorTime = db::rdtsc();
  monitor--;
  (*queueDepth)++;

  std::lock_guard<std::mutex> fileLocker(fileLock);
  counterOfQueriesExecuted--;

  //  if (dirtyStatisticsCounter > 0) {
  // @HOT for visualising results
  // bm->getBufferPool().printDirtyPagesForAllDataStructures();
  //  }
  dirtyStatisticsCounter++;

  delete qop;

  return 1;
}

void producer(deceve::queries::QueueManager *queueManager,
              deceve::queries::QGenerator *qGenerator) {
  while (1) {
    if ((*queueDepth) < 1) continue;
    std::this_thread::sleep_for(std::chrono::nanoseconds(1000));

    if (qGenerator->areThereMoreQueriesLeft()) {
      deceve::queries::QOperator *randQuery = qGenerator->getNextQuery();
      std::lock_guard<std::mutex> locker(threadQueueMutex);
      queueManager->addQuery(randQuery);
      (*queueDepth)--;
    } else {
      break;
    }
  }
}

void consumer(deceve::storage::BufferManager *bm,
              deceve::queries::QueueManager *queueManager,
              deceve::queries::QGenerator *qGenerator) {
  while (1) {
    std::this_thread::sleep_for(std::chrono::nanoseconds(1));

    if (threadPool->getNumberOfTasks() == 0 && counterOfQueriesExecuted == 0) {
      std::cout << "Consumer finished, no other queries are produced \n";
      break;
    }

    if (queueManager->size() != 0 && threadPool->getNumberOfTasks() == 0 &&
        monitor.load() < static_cast<int>(threadPool->getNumberOfWorkers())) {
      std::lock_guard<std::mutex> locker(threadQueueMutex);

      if (queueManager->size() != 0 && threadPool->getNumberOfTasks() == 0 &&
          monitor.load() < static_cast<int>(threadPool->getNumberOfWorkers())) {
        // Check when a re-order in the queue should appear
        if (bm->getSM().getReorderFlag() == 1) {
          if (bm->getSM().reorderCounter != 0 &&
              bm->getSM().reorderCounter % (bm->getSM().getQueueDepth() -
                                            bm->getSM().getNumberOfThreads()) ==
                  0) {
            std::lock_guard<std::mutex> lockAnalyser(
                bm->getSM().queryOperatorsMutex);
            //
            std::cout << "------- REORDER START ------------ " << std::endl;

            deceve::queries::QOperator *dummyQuery =
                qGenerator->getDummyQuery();
            //            fakeQuery->setQueryIndex(&taskQueue);
            dummyQuery->setQueryIndex(queueManager->getQueue());
            dummyQuery->setQueueAsInput(cleverFlag);
            dummyQuery->analyseQueries();
            dummyQuery->reorderQueries();
            bm->getSM().clearPersistedFutureFileReferences();
            //       bm->getStorageManager().printInternalScores();
            std::cout << "------- REORDER END ------------ " << std::endl;
            delete dummyQuery;
          }
        }

        deceve::queries::QOperator *queryToBeExecuted =
            queueManager->getFirstQuery();

        queryToBeExecuted->setQueryIndex(queueManager->getQueue());
        queryToBeExecuted->setQueueAsInput(cleverFlag);
        if (cleverFlag) {
          queryToBeExecuted->rewriteQueryPlan();
        }

        threadPool->enqueue(&query_task_execution, queryToBeExecuted, bm);
      }
    }
  }
}

// Function definition
deceve::storage::HistoryMode getHistoryEnum(size_t history);

void initialiseVariables(size_t read_delay, size_t write_delay,
                         size_t budgetPermanent,
                         int algorithm, deceve::storage::BufferManager *bm,
                         size_t history, double segment, size_t partitions,
                         size_t number_of_threads, int queueDepthParameter,
                         int reorder, int order) {
  bm->bufferReadDelay = read_delay;
  bm->bufferWriteDelay = write_delay;
  bm->getSM().setStorageReadDelay(read_delay);
  bm->getSM().setStorageWriteDelay(write_delay);
  bm->getSM().setPhysicalReadNumber(1);
  bm->getSM().setPhysicalWriteNumber(1);
  bm->getSM().setBudgetPermanentSize(budgetPermanent);
  bm->getBufferPool().setAlgorithm(algorithm);
  counterOfQueriesExecuted = nQueries;
  bm->getSM().setHistoryFlag(getHistoryEnum(history));
  bm->getBufferPool().setSegmentPercentage(segment);
  bm->getSM().setAmountOfPartitions(partitions);
  bm->getSM().setNumberOfThreads(number_of_threads);
  bm->getSM().setQueueDepth(queueDepthParameter);
  bm->getSM().setReorderFlag(reorder);
  bm->getSM().setOrderFlag(order);
}

deceve::storage::HistoryMode getHistoryEnum(size_t history) {
  if (history == 0) {
    return deceve::storage::PAST_FUTURE;
  } else if (history == 1) {
    return deceve::storage::PAST_ONLY;
  } else if (history == 2) {
    return deceve::storage::FUTURE_ONLY;
  }

  return deceve::storage::PAST_FUTURE;
}

void addAnalyticalDetailsAboutWorkloadExecution(const std::string& inputName, size_t budget,  int algorithm,
                          int reorderFlag) {
  // ################## Create files for storing statistics about executions
  // ####################################################
  std::cout << "Start program"
            << "\n";
  std::cout << "POOLSIZE: " << deceve::queries::POOLSIZE << "\n";
  std::cout << "Budget: " << budget << "\n";

  // This file contains more detailed information about the time each individual
  // query needs
  detailedStatisticsFile.open(inputName, ios::out | ios::app);
  detailedStatisticsFile << "----------Execution BEGIN--------------"
                         << "\n";
  detailedStatisticsFile << "-------------- ALGORITHM (" << algorithm
                         << ") clever: (" << cleverFlag << ") reorder:("
                         << reorderFlag << ") ----------------" << std::endl
                         << " ";
  detailedStatisticsFile << "\n";
  detailedStatisticsFile << "POOLSIZE: " << deceve::queries::POOLSIZE << "\n";
  detailedStatisticsFile << "Budget: " << budget << "\n";
  detailedStatisticsFile << "Number of queries: " << nQueries << "\n";
  detailedStatisticsFile << "Algorithm: " << algorithm << "\n";

  // This file contains total statistics
  executionTimeFile.open("executionTimeExcel.txt", ios::out | ios::app);
  executionTimeFile << "\n-------------- ALGORITHM (" << algorithm
                    << ") clever: (" << cleverFlag << ") reorder:("
                    << reorderFlag << ") ----------------" << std::endl
                    << " ";
  executionTimeFile << "pool: " << deceve::queries::POOLSIZE;

  if (algorithm == 0) {
    detailedStatisticsFile << budget << " 0:LRU(" << cleverFlag << ")   ";
    executionTimeFile << budget << " 0:LRU(" << cleverFlag << ")   ";
  } else if (algorithm == 1) {
    detailedStatisticsFile << budget << " 1:SLRU(" << cleverFlag << ")";
    executionTimeFile << budget << " 1:SLRU(" << cleverFlag << ")";
  } else if (algorithm == 2) {
    detailedStatisticsFile << budget << " 2:CLRU-TWO(" << cleverFlag << ")";
    executionTimeFile << budget << " 2:FLRU(" << cleverFlag << ")";
  } else if (algorithm == 3) {
    detailedStatisticsFile << budget << " 3:CRU(" << cleverFlag << ")";
    executionTimeFile << budget << " 3:CRU(" << cleverFlag << ")   ";
  } else if (algorithm == 4) {
    detailedStatisticsFile << budget << " 4:SCRU(" << cleverFlag << ")";
    executionTimeFile << budget << " 4:SCRU(" << cleverFlag << ")";
  } else if (algorithm == 5) {
    detailedStatisticsFile << budget << " 5:FCRU(" << cleverFlag << ")";
    executionTimeFile << budget << " 5:FCRU(" << cleverFlag << ")";
  } else if (algorithm == 6) {
    detailedStatisticsFile << budget << " 6:Ranking(" << cleverFlag << ")";
    executionTimeFile << budget << " 6:Ranking(" << cleverFlag << ")";
  } else if (algorithm == 7) {
    detailedStatisticsFile << budget << " 7:CleanRanking(" << cleverFlag << ")";
    executionTimeFile << budget << " 7:CleanRanking(" << cleverFlag << ")";
  } else if (algorithm == 9) {
    detailedStatisticsFile << budget << " 9:CleanOne(" << cleverFlag << ")";
    executionTimeFile << budget << " 9:CleanOne(" << cleverFlag << ")";
  } else if (algorithm == 10) {
    detailedStatisticsFile << budget << " 10:LRUOne(" << cleverFlag << ")";
    executionTimeFile << budget << " 10:LRUOne(" << cleverFlag << ")";
  } else if (algorithm == 15) {
    detailedStatisticsFile << budget << " 15:RandRank(" << cleverFlag << ")";
    executionTimeFile << budget << " 15:RandRank(" << cleverFlag << ")";
  } else {
    detailedStatisticsFile << budget << " LRU(" << cleverFlag << ")   ";
    executionTimeFile << budget << " LRU(" << cleverFlag << ")   ";
  }

  //  executionTimeFile.open("executionTimeExcel.txt", ios::out | ios::app);

  detailedStatisticsFile << "qId (all)"
                         << "pool (" << deceve::queries::POOLSIZE << ")"
                         << "nqueries (" << nQueries << ")"
                         << " ";

  detailedStatisticsFile.close();
  executionTimeFile.close();
}

#endif /* STARTDATABASE_HH_ */
