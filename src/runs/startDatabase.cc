/*******************************************************************************
 * Copyright (c) 2015 Michail Basios and Stratis Viglas.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michail Basios - initial API and implementation
 *     Stratis Viglas - initial API and implementation
 *******************************************************************************/
#include "startDatabase.hh"
#include "../utils/statistics.hh"

int main(int argc, char** argv) {
  size_t read_delay, write_delay, availableDataStructureBudget, dump;
  size_t clock_wait_time, partitions, numberOfThreads;
  unsigned long pool_size;
  uint64_t start, end;
  double segmentPercentage;  // experimental
  int replacementAlgorithm, queueDepthParameter, reorderFlag, orderFlag;
  int algorithm3Case;
  int costCalculatorCase;
  std::string workload;

  po::options_description desc("Arguments");
  desc.add_options()("help", "produce help message")(
      "read-delay", po::value<size_t>(&read_delay)->default_value(10),
      "persistent memory read delay (ns)")(
      "write-delay", po::value<size_t>(&write_delay)->default_value(150),
      "persistent memory write delay (ns)")(
      "pool_size", po::value<unsigned long>(&pool_size)->default_value(25000),
      "size of the buffer pool")(
      "budgetPermanent",
      po::value<size_t>(&availableDataStructureBudget)->default_value(50000),
      "size of availableDataStructureBudget in pages")(
      "algorithm", po::value<int>(&replacementAlgorithm)->default_value(3),
      " replacementAlgorithm: case: 2, hybrid Clean-LRU , case: 3, "
      "Clean-LRU-Ranked")(
      "algorithmCase", po::value<int>(&algorithm3Case)->default_value(0),
      " case0 = algorithm 3 (default rankings), case = 1 -> algorithm3Case1 "
      "(keep data in DRAM), case = 2 -> "
      "algorithm3Case2 (write data in NVRAM)")(
      "costCalculator", po::value<int>(&costCalculatorCase)->default_value(0),
      " case 0 = calculate cost based on pessimistic approach, case = 1 -> "
      "calculate cost by analysing the current state of the queue ")(
      "clock", po::value<size_t>(&clock_wait_time)->default_value(5),
      "clock wait time for estimating speed")(
      "workload", po::value<std::string>(&workload)->default_value("FROM_FILE"),
      "case1: FROM_FILE, case2: RANDOMLY, case3: SKEWED")(
      "clever", po::value<size_t>(&cleverFlag)->default_value(1),
      "if 1 the system is going to reuse results otherwise "
      "it will follow normal execution")(
      "history", po::value<size_t>(&historyFlag)->default_value(2),
      "0: PAST_FUTURE, 1:PAST_ONLY 2:FUTURE_ONLY")(
      "nqueries", po::value<size_t>(&nQueries)->default_value(100),
      "nqueries to be executed")(
      "threads", po::value<size_t>(&numberOfThreads)->default_value(1),
      "number of threads the system has")(
      "reorder", po::value<int>(&reorderFlag)->default_value(0),
      "reorder flag: 0 for no reordering and 1 for reordering queries")(
      "depth", po::value<int>(&queueDepthParameter)->default_value(5),
      "number of users sending concurrently queries to the system")(
      "order", po::value<int>(&orderFlag)->default_value(0),
      "order flag: 0 merging future scores, 1 for not merging, used in "
      "calculating scores")("dump", po::value<size_t>(&dump)->default_value(0),
                            "print pages and files of bufferpool")(
      "segment", po::value<double>(&segmentPercentage)->default_value(0.1),
      "amount of memory percentage used by slider to move. Experimental: "
      "NOT-USED right now")("partitions",
                            po::value<size_t>(&partitions)->default_value(1),
                            "number of extra partitions, when deciding the "
                            "number of partitions for a hash-join operation");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    std::cerr << desc << "\n";
    return 1;
  }

  /*****************************************
   *
   *              Calculate Delays (read-writes)
   *
   *****************************************/
  std::cout << "Estimating clock speed (wait " << clock_wait_time << "s)...";
  std::cout.flush();
  tps = deceve::bama::ticks_per_second(clock_wait_time);
  std::cout << " done"
            << "\n"
            << "clock speed is " << std::fixed << std::setprecision(9)
            << (size_t)tps << " ticks per second"
            << "\n";

  std::cout << "read delay (" << read_delay << "ns) is ";
  std::cout.flush();
  read_delay = (size_t)(::pow(10, -9) * read_delay * tps);
  std::cout << std::fixed << std::setprecision(9) << read_delay << " ticks"
            << "\n";
  db::set_read_delay(read_delay);

  std::cout << "write delay (" << write_delay << "ns) is ";
  std::cout.flush();
  write_delay = (size_t)(::pow(10, -9) * write_delay * tps);
  std::cout << std::fixed << std::setprecision(9) << write_delay << " ticks"
            << "\n";
  db::set_write_delay(write_delay);

  std::cout << "read_delay: " << read_delay << std::endl;
  std::cout << "write_delay: " << write_delay << std::endl;

  // ----------------------

  /*****************************************
   *
   *   Initialize parameters
   *
   *****************************************/
  deceve::queries::POOLSIZE = pool_size;
  deceve::queries::QueryManager qm;
  deceve::storage::BufferManager bm(deceve::queries::POOLSIZE *
                                    deceve::storage::PAGE_SIZE_PERSISTENT);
  deceve::queries::QueueManager queueManager;

  /*****************************************
   *
   *   Generate workload
   *
   *****************************************/
  deceve::queries::QGenerator* queryGenerator;

  if (workload == "FROM_FILE") {
    queryGenerator = new deceve::queries::QGenerator(
        &bm, &qm, nQueries, deceve::queries::FROM_FILE);
  } else if (workload == "RANDOMLY") {
    queryGenerator = new deceve::queries::QGenerator(&bm, &qm, nQueries,
                                                     deceve::queries::RANDOMLY);
  } else {
    queryGenerator = new deceve::queries::QGenerator(&bm, &qm, nQueries,
                                                     deceve::queries::SKEWED);
  }

  nQueries = queryGenerator->getTotalNumberOfQueries();

  //  bm.getStorageManager().setReorderFlag(5);

  initialiseVariables(read_delay, write_delay, availableDataStructureBudget,
                      replacementAlgorithm, &bm, historyFlag, segmentPercentage,
                      partitions, numberOfThreads, queueDepthParameter,
                      reorderFlag, orderFlag);

  // set algorithm3Case
  bm.getBufferPool().algorithm3Case = algorithm3Case;
  // set how to calculate cost
  bm.costCalculatorCase = costCalculatorCase;

  if (algorithm3Case != 0) {
#ifndef AUXILIARY_MERGED
    std::cerr << "ERROR: (you run: algorithm3 with cases) remember to use "
                 "-DAUXILIARY_MERGED when compile"
              << std::endl;
    return 0;
#endif
  } else {
#ifdef AUXILIARY_MERGED
    std::cerr << "ERROR: (you run: algorithm3) remember you should not use "
                 "-DAUXILIARY_MERGED when compile"
              << std::endl;
    return 0;
#endif
  }

  /******************************************
   *
   *   Run Workload
   *
   *****************************************/
  auto startChronoTime = chrono::steady_clock::now();
  start = db::rdtsc();

  threadPool = new ThreadPool(numberOfThreads);
  std::cout << "Threadpool number of workers: "
            << threadPool->getNumberOfWorkers() << std::endl;
  queueDepth = new std::atomic<int>(queueDepthParameter);
  std::thread thread2(consumer, &bm, &queueManager, queryGenerator);

  //  producer(&bm, &qm, &queueManager);
  producer(&queueManager, queryGenerator);
  // thread1.join();

  thread2.join();

  end = db::rdtsc();
  auto endChronoTime = chrono::steady_clock::now();

  auto totalChronoTime = endChronoTime - startChronoTime;

  size_t cost = (size_t)(
      bm.getSM().getPhysicalReadsNumber() * bm.getSM().getStorageReadDelay() +
      bm.getSM().getPhysicalWritesNumber() * bm.getSM().getStorageWriteDelay());

  std::cout << "QUERIES FINISHED" << std::endl;

  /*****************************************
   *
   *   Export Details about workload execution
   *
   *****************************************/
  deceve::statistics::printRuntimeDetailsConsole(dump, bm);
  bm.getSM().printAllFiles();

  /*******************************************
   *
   *   EXPORT ANALYTICAL DETAILS ABOUT WORKLOAD EXECUTION
   *   OUTPUT_FILE -> analyticalDetails.txt
   *
   *****************************************/
  std::string analyticalDetailsFile = "analyticalDetails.txt";
  // #### Create files for storing statistics about executions ###
  addAnalyticalDetailsAboutWorkloadExecution(analyticalDetailsFile,
                                             availableDataStructureBudget,
                                             replacementAlgorithm, reorderFlag);

  detailedStatisticsFile.open(analyticalDetailsFile, ios::out | ios::app);

  detailedStatisticsFile << " Total program execution time (secs): "
                         << (end - start) / tps << "\n";

  detailedStatisticsFile << " Total cost in reads*r + writes*w: " << cost
                         << "\n";
  bm.getBufferPool().printHitRatio(&detailedStatisticsFile);

  detailedStatisticsFile << "filesPersistedCostFuture: "
                         << bm.getSM().filesPersistedCostFuture << std::endl;
  detailedStatisticsFile << "filesPersistedCounterFuture: "
                         << bm.getSM().filesPersistedCounterFuture << std::endl;
  detailedStatisticsFile << "filesPersistedCostPast: "
                         << bm.getSM().filesPersistedCostPast << std::endl;
  detailedStatisticsFile << "filesPersistedCounterPast: "
                         << bm.getSM().filesPersistedCounterPast << std::endl;

  detailedStatisticsFile << "---------- Execution END --------------"
                         << "\n";
  detailedStatisticsFile.close();

  bm.getSM().printSharingStatistics(analyticalDetailsFile);
  // ################# END ##############################

  /***************  ***   ***********************
   *
   *   EXPORT GATHERED STATISTICS ABOUT WORKLOAD EXECUTION
   *   OUTPUT_FILE -> runtimeDetails.txt
   *
   *****************************************/

  ofstream runtimeDetails;
  runtimeDetails.open("runtimeDetails.txt", ios::out | ios::app);
  runtimeDetails << std::endl
                 << "-------------- ALGORITHM (" << replacementAlgorithm
                 << ") clever: (" << cleverFlag << ") reorder:(" << reorderFlag
                 << ") ----------------" << std::endl;
  runtimeDetails << " algo: " << bm.getBufferPool().getAlgorithm() << " ";
  runtimeDetails << " pool: " << bm.getBufferPool().getPoolSize() << " ";
  runtimeDetails << " budg: " << availableDataStructureBudget << " ";
  runtimeDetails << " t_t: " << (end - start) / tps << " ";
  runtimeDetails << " cost: " << cost << " ";
  runtimeDetails << "t_r: " << bm.getSM().getPhysicalReadsNumber() << " ";
  runtimeDetails << "t_w: " << bm.getSM().getPhysicalWritesNumber() << " ";
  runtimeDetails << "p_r: " << bm.getSM().getPrimaryPhysicalReadsNumber()
                 << " ";
  runtimeDetails << "i_r: " << bm.getSM().getIntermediatePhysicalReadsNumber()
                 << " ";
  runtimeDetails << "a_r: "
                 << bm.getSM().getAuxiliaryPhysicalPhysicalReadsNumber() << " ";
  runtimeDetails << "p_w: " << bm.getSM().getPrimaryPhysicalWritesNumber()
                 << " ";
  runtimeDetails << "i_w: " << bm.getSM().getIntermediatePhysicalWritesNumber()
                 << " ";
  runtimeDetails << "a_w: " << bm.getSM().getAuxiliaryPhysicalWritesNumber()
                 << " ";
  runtimeDetails << "p_hit: " << bm.getBufferPool().getPrimaryHitRatio() << " ";
  runtimeDetails << "i_hit: " << bm.getBufferPool().getIntermediateHitRatio()
                 << " ";
  runtimeDetails << "a_hit: " << bm.getBufferPool().getAuxiliaryHitRatio()
                 << " ";
  runtimeDetails << "t_hit: " << bm.getBufferPool().getTotalHitRatio()
                 << " history" << historyFlag << " ";
  runtimeDetails << "threads: " << bm.getSM().getNumberOfThreads() << " ";
  runtimeDetails << "queue-depth: " << bm.getSM().getQueueDepth() << " ";
  runtimeDetails << "reorder: " << bm.getSM().getReorderFlag() << std::endl
                 << std::endl;
  runtimeDetails.close();

  //  bm.getSM().printWritePredictions();

  //  bm.getSM().printNameConverters();

  //  DEBUG(bm.getSM().monitorConverter);
  //  DEBUG(bm.getSM().monitorKeys);

  //  bm.getStatsManager().printMonitorMap();

  //  ofstream statsFile;
  //  statsFile.open("stats.txt", ios::out);
  //
  //  statsFile << " Final number of queries executed: "
  //            << counterOfQueriesExecuted.load() << std::endl;
  //
  //  statsFile << "totalChronoTime: ";
  //  statsFile << chrono::duration<double, milli>(totalChronoTime).count();
  //  statsFile << std::endl;
  //
  //  // readCalls
  //  statsFile
  //      << "totalReadChronoTime: "
  //      << chrono::duration<double,
  //      milli>(bm.getSM().totalReadChronoTime).count()
  //      << " ms" << endl;
  //  statsFile << "numberOfReadCalls: " << bm.getSM().totalReadChronoCounter
  //            << std::endl;
  //  statsFile << "average readCall: "
  //            << chrono::duration<double,
  //            milli>(bm.getSM().totalReadChronoTime)
  //                       .count() /
  //                   bm.getSM().totalReadChronoCounter
  //            << std::endl;
  //
  //  // writeCalls
  //  statsFile << "totalWriteChronoTime: "
  //            << chrono::duration<double,
  //            milli>(bm.getSM().totalWriteChronoTime)
  //                   .count()
  //            << " ms" << endl;
  //  statsFile << "numberOfWriteCalls: " << bm.getSM().totalWriteChronoCounter
  //            << std::endl;
  //  statsFile << "average writeCall: "
  //            << chrono::duration<double,
  //            milli>(bm.getSM().totalWriteChronoTime)
  //                       .count() /
  //                   bm.getSM().totalWriteChronoCounter
  //            << std::endl;
  //
  //  statsFile << "getTotalRunTimeForStorageReadingPages: "
  //            << bm.getSM().getTotalRunTimeForStorageReadingPages() / tps
  //            << std::endl;
  //
  //  statsFile << "getTotalRunTimeForStorageWritingPages: "
  //            << bm.getSM().getTotalRunTimeForStorageWritingPages() / tps
  //            << std::endl;
  //
  //  statsFile << "filesPersistedCostFuture: "
  //            << bm.getSM().filesPersistedCostFuture << std::endl;
  //  statsFile << "filesPersistedCounterFuture: "
  //            << bm.getSM().filesPersistedCounterFuture << std::endl;
  //  statsFile << "filesPersistedCostPast: " <<
  //  bm.getSM().filesPersistedCostPast
  //            << std::endl;
  //  statsFile << "filesPersistedCounterPast: "
  //            << bm.getSM().filesPersistedCounterPast << std::endl;
  //
  //  statsFile << "Error counter: " << bm.getSM().getErrorCounter() <<
  //  std::endl;
  //
  //  statsFile.close();

  /***************  ***   ***********************
   *
   *   EXPORT STATISTICS TO FILE USED TO GENERATE GRAPHS
   *   OUTPUT_FILE -> graph.txt
   *
   *****************************************/
  fstream graphFile;
  graphFile.open("graph.txt", ios::out);
  graphFile << bm.getAlgorithmToken(replacementAlgorithm) << "(" << cleverFlag
            << "," << reorderFlag << ") " << std::endl;
  graphFile << (end - start) / tps << " " << std::endl;
  graphFile << cost << " " << std::endl;
  graphFile << bm.getSM().getPhysicalReadsNumber() << " " << std::endl;
  graphFile << bm.getSM().getPhysicalWritesNumber() << " " << std::endl;
  graphFile.close();

  delete threadPool;
  delete queueDepth;

  std::cout << "Total execution time"
            << " t_t (cycles): " << (end - start) << std::endl;

  std::cout << "totalChronoTime (secs): ";
  std::cout << chrono::duration<double, milli>(totalChronoTime).count() / 1000.0
            << std::endl;

  return 0;
}
