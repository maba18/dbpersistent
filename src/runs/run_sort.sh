#!/bin/sh

BIN_ROOT=$1
RES_ROOT=$2
CARDINALITY=$3

# first start with ram disk

SCRATCH=$RES_ROOT/ramdisk
mkdir $SCRATCH
make clean
make
$BIN_ROOT/mk_ramdisk.py --name=ramdisk --size=2048
$BIN_ROOT/sort_memory_run.py --times=3 \
    --exec-dir=$BIN_ROOT --data-dir=$SCRATCH \
    --output-dir=$SCRATCH --read-delay=10 --write-delay=150 \
    --start=$CARDINALITY --end=$CARDINALITY --increment=$CARDINALITY \
    --points=15 --key=0 &> $SCRATCH/log.txt
umount /mnt/ramdisk
rm /mnt/ramdisk

# PMFS
mkdir -p /mnt/pmfs
mount -t pmfs -o physaddr=0x10000000,init=2G none /mnt/pmfs
$BIN_ROOT/sort_memory_run.py --times=3 \
    --exec-dir=$BIN_ROOT --data-dir=$SCRATCH \
    --output-dir=$SCRATCH --read-delay=10 --write-delay=150 \
    --start=$CARDINALITY --end=$CARDINALITY --increment=$CARDINALITY \
    --points=15 --key=0 &> $SCRATCH/log.txt
umount /mnt/pmfs
rm /mnt/pmfs

# memfile

SCRATCH=$RES_ROOT/memory
mkdir $SCRATCH
make clean
make CXXFLAGS=-DMEMORY
$BIN_ROOT/sort_memory_run.py --times=3 \
    --exec-dir=$BIN_ROOT --data-dir=$SCRATCH \
    --output-dir=$SCRATCH --read-delay=10 --write-delay=150 \
    --start=$CARDINALITY --end=$CARDINALITY --increment=$CARDINALITY \
    --points=15 --key=0 &> $SCRATCH/log.txt 

# vector

SCRATCH=$RES_ROOT/vector
mkdir $SCRATCH
make clean
make CXXFLAGS=-DVECTOR
$BIN_ROOT/sort_memory_run.py --times=3 \
    --exec-dir=$BIN_ROOT --data-dir=$SCRATCH \
    --output-dir=$SCRATCH --read-delay=10 --write-delay=150 \
    --start=$CARDINALITY --end=$CARDINALITY --increment=$CARDINALITY \
    --points=15 --key=0 &> $SCRATCH/log.txt 

