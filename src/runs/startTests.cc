#define CATCH_CONFIG_MAIN

#include "startDatabase.hh"

#include <tuple>

// Include c
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <time.h>

// Include Boost Functions (as c libraries)
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

// Include c++
#include <cstdio>
#include <fstream>
#include <cstring>
#include <iomanip>
#include <string>
#include <iostream>
#include <chrono>

// include STL containers
#include <vector>
#include <queue>
#include <deque>

#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>

#include "test.hh"
#include "catch.hpp"

// Include threading functions
#include "ThreadPool.hh"

// Include General Functions
#include "../utils/defs.hh"
#include "../utils/util.hh"
#include "../storage/io.hh"
#include "../utils/types.hh"
#include "../utils/global.hh"

// Include Storage Classes
#include "../storage/BufferManager.hh"
#include "../storage/IterableQueue.hh"
#include "../storage/readwriters.hh"

// Include Storage Classes
#include "../queryplans/NodeTree.hh"
#include "../queryplans/PoolNode.hh"
#include "../queryplans/QueryManager.hh"
#include "../queryplans/QueueManager.hh"
#include "../queryplans/QGenerator.hh"

// Include Query Classes
#include "../queryplans/queries/MyQuery.hh"
#include "../queryplans/queries/MyQueryJoin.hh"
#include "../queryplans/queries/MyQuery1.hh"
#include "../queryplans/queries/MyQuery2.hh"
#include "../queryplans/queries/MyQuery3.hh"
#include "../queryplans/queries/MyQuery4.hh"
#include "../queryplans/queries/MyQuery5.hh"
#include "../queryplans/queries/MyQuery6.hh"
#include "../queryplans/queries/MyQuery7.hh"
#include "../queryplans/queries/MyQuery8.hh"
#include "../queryplans/queries/MyQuery9.hh"
#include "../queryplans/queries/MyQuery10.hh"
#include "../queryplans/queries/MyQuery11.hh"
#include "../queryplans/queries/MyQuery12.hh"
#include "../queryplans/queries/MyQuery13.hh"
#include "../queryplans/queries/MyQuery14.hh"
#include "../queryplans/queries/MyQuery15.hh"
#include "../queryplans/queries/MyQuery16.hh"
#include "../queryplans/queries/MyQuery17.hh"
#include "../queryplans/queries/MyQuery18.hh"
#include "../queryplans/queries/MyQuery19.hh"
#include "../queryplans/QOperator.hh"

#include "../storage/BTree.hh"

namespace po = boost::program_options;
using namespace std;
using namespace boost;

namespace db = deceve::bama;
namespace dq = deceve::queries;

// TEST_CASE("Check table_key structs", "[table_key_test]") {
//  deceve::storage::BufferManager bm(
//      10000 * deceve::storage::PAGE_SIZE_PERSISTENT, 1);
//
//  SECTION("Test merge keys 1") {
//    sto::table_key tk1;
//    sto::table_key tk2;
//
//    std::cout << tk1 << std::endl;
//    std::cout << tk2 << std::endl;
//
//    REQUIRE(tk1 == tk2);
//
//    sto::table_key tk3 = bm.getSM().mergeKeys(tk1, tk2);
//    std::cout << "result tk2: " << tk3 << std::endl;
//
//    REQUIRE(tk3.attributes[0] == sto::ALL_ATTRIBUTES);
//  }
//
//  SECTION("Test merge keys 2") {
//    sto::table_key tk1;
//    sto::table_key tk2;
//
//    tk1.table_name = sto::LINEITEM;
//    tk1.field = sto::L_PARTKEY;
//    tk1.version = sto::HASHJOIN;
//    tk1.attributes = {sto::L_ORDERDATE, sto::L_PARTKEY, sto::L_SHIPDATE};
//
//    tk2.table_name = sto::LINEITEM;
//    tk2.field = sto::L_PARTKEY;
//    tk2.version = sto::HASHJOIN;
//    tk2.attributes = {sto::L_PARTKEY, sto::L_SUPPKEY};
//
//    std::cout << tk1 << std::endl;
//    std::cout << tk2 << std::endl;
//
//    REQUIRE(tk1 == tk2);
//
//    sto::table_key tk3 = bm.getSM().mergeKeys(tk1, tk2);
//    std::cout << "result tk2: " << tk3 << std::endl;
//
//    std::vector<sto::FIELD> manualResult = {sto::L_ORDERDATE, sto::L_PARTKEY,
//                                            sto::L_SHIPDATE, sto::L_SUPPKEY};
//
//    for (size_t i = 0; i < tk3.attributes.size(); i++) {
//      REQUIRE(tk3.attributes[i] == manualResult[i]);
//    }
//  }
//}

// ################# QOperator Functions testing ####################

TEST_CASE("QOperator::doesQueryContainDS(tk)", "[doesQueryContainDS]") {
  // Variables for tracking queries runtimes
  ofstream detailedStatisticsFile;
  ofstream executionTimeFile;
  uint64_t startOperatorTime, endOperatorTime;
  size_t read_delay = 10;
  size_t write_delay = 150;
  unsigned long pool_size = 20000;
  size_t budgetPermanent = 100000;
  size_t dump = 0;
  uint64_t start, end;
  int algorithm = 2;
  size_t clock_wait_time = 1;
  size_t qRate = 1;
  size_t tempQueryId = 10;
  size_t nQueries = 100;
  size_t windowSize = 5;
  double segmentPercentage = 0.1;
  size_t partitions = 1;
  size_t numberOfThreads = 1;
  int queueDepthParameter = 10;
  int reorderFlag = 0;
  int orderFlag = 0;
  historyFlag = 0;
  deceve::queries::POOLSIZE = pool_size;

  /*****************************************
   *
   *              Calculate Delays (read-writes)
   *
   *****************************************/
  std::cout << "estimating clock speed (wait " << clock_wait_time << "s)...";
  std::cout.flush();
  tps = deceve::bama::ticks_per_second(clock_wait_time);
  std::cout << " done"
            << "\n"
            << "clock speed is " << std::fixed << std::setprecision(9)
            << (size_t)tps << " ticks per second"
            << "\n";

  std::cout << "read delay (" << read_delay << "ns) is ";
  std::cout.flush();
  read_delay = (size_t)(::pow(10, -9) * read_delay * tps);
  std::cout << std::fixed << std::setprecision(9) << read_delay << " ticks"
            << "\n";
  db::set_read_delay(read_delay);

  std::cout << "write delay (" << write_delay << "ns) is ";
  std::cout.flush();
  write_delay = (size_t)(::pow(10, -9) * write_delay * tps);
  std::cout << std::fixed << std::setprecision(9) << write_delay << " ticks"
            << "\n";
  db::set_write_delay(write_delay);

  std::cout << "read_delay: " << read_delay << std::endl;
  std::cout << "write_delay: " << write_delay << std::endl;

  deceve::queries::POOLSIZE = 20000;
  deceve::queries::QueryManager qm;
  deceve::storage::BufferManager bm(
      deceve::queries::POOLSIZE * deceve::storage::PAGE_SIZE_PERSISTENT);
  deceve::queries::QueueManager queueManager;

  //  bm.getStorageManager().setReorderFlag(5);

  initialiseVariables(read_delay, write_delay, budgetPermanent,
                      algorithm, &bm, historyFlag, segmentPercentage,
                      partitions, numberOfThreads, queueDepthParameter,
                      reorderFlag, orderFlag);

  // #### Create files for storing statistics about executions ###
  addAnalyticalDetailsAboutWorkloadExecution(budgetPermanent, algorithm,
                       reorderFlag);

  //  deceve::storage::BTree<int, LINEITEM> bTreeLineitem(&bm, "testTree.bin");
  //  for (int times = 0; times < 100; times++) {
  //    std::cout << "inserting record... " << times << "\n";
  //
  //    LINEITEM lineitem;
  //    bTreeLineitem.insert(
  //        deceve::storage::makeRecord<int, LINEITEM>(times, lineitem));
  //    std::cout << "inserted"
  //              << "\n";
  //  }

  //  deceve::queries::QueryManager qm;
  //  deceve::storage::BufferManager bm(1000 * 1024, 1);
  //
  //  deceve::queries::QOperator *randQuery = getQueryWithId(&bm, &qm, 5);
  //  deceve::storage::table_key right_tk;
  //
  //  right_tk.version = deceve::storage::HASHJOIN;
  //  right_tk.table_name = deceve::storage::LINEITEM;
  //  right_tk.field = deceve::storage::L_ORDERKEY;
  //  right_tk.projected_field = deceve::storage::LINEITEM_Q5;
  //
  //  bool isContained = randQuery->doesQueryContainDS(right_tk);
  //
  //  REQUIRE(isContained == true);
  //
  //  SECTION("test read and write delays") {
  //    int iterations = 100000;
  //    start = db::rdtsc();
  //    for (int i = 0; i < iterations; i++) {
  //      deceve::bama::delay(read_delay *
  //                          (deceve::bama::get_pagesize() /
  //                          db::cacheline_size));
  //    }
  //    end = db::rdtsc();
  //
  //    uint64_t totalReadDelay = end - start;
  //
  //    // ----------------------
  //    start = db::rdtsc();
  //    for (int i = 0; i < iterations; i++) {
  //      deceve::bama::delay(write_delay *
  //                          (deceve::bama::get_pagesize() /
  //                          db::cacheline_size));
  //    }
  //    end = db::rdtsc();
  //    uint64_t totalWriteDelay = end - start;
  //
  //    std::cout << "totalReadDelay  delay:  " << totalReadDelay << std::endl;
  //    std::cout << "totalWriteDelay delay: " << totalWriteDelay << std::endl;
  //    std::cout << "totalReadDelay  delay: (sec):  " << totalReadDelay / tps
  //              << std::endl;
  //    std::cout << "totalWriteDelay delay: (sec):  " << totalWriteDelay / tps
  //              << std::endl;
  //
  //    REQUIRE(totalWriteDelay > totalReadDelay);
  //  }

  SECTION("test write delays on pages") {
    std::string inputFile =
        deceve::queries::constructInputTmpFullPath("test1.table");

    deceve::bama::Writer<int> writer1(&bm, inputFile, deceve::storage::PRIMARY);

    int numTuples = deceve::storage::PAGE_SIZE_PERSISTENT / sizeof(int);
    std::cout << "numTuples: " << 10 * numTuples << std::endl;

    std::chrono::duration<double> writeTime = {};
    std::chrono::duration<double> readTime = {};
    std::chrono::duration<double> deleteTime = {};

    auto start = chrono::steady_clock::now();
    for (int i = 0; i < 250000 * 1022; i++) {
      writer1.write(i);
    }
    writer1.close();
    auto end = chrono::steady_clock::now();

    writeTime = end - start;

    start = chrono::steady_clock::now();
    deceve::bama::Reader<int> reader1(&bm, inputFile);
    while (reader1.hasNext()) {
      int x = reader1.nextRecord();
    }
    reader1.close();
    end = chrono::steady_clock::now();
    readTime = end - start;

    start = chrono::steady_clock::now();
    bm.removeFile(inputFile);
    end = chrono::steady_clock::now();
    deleteTime = end - start;

    std::cout << "writeTime: "
              << std::chrono::duration<double, milli>(writeTime).count()
              << std::endl;
    std::cout << "readTime: "
              << std::chrono::duration<double, milli>(readTime).count()
              << std::endl;
    std::cout << "deleteTime: "
              << std::chrono::duration<double, milli>(deleteTime).count()
              << std::endl;

    bm.bmrWritePageTimer.printTimes();
    bm.bmrReadPageTimer.printTimes();
    bm.bmrPinPageTimer.printTimes();
    bm.bmrReleasePageTimer.printTimes();
    bm.getBufferPool().timerFetchFromPrimaryMode.printTimes();
    bm.getBufferPool().timerPhysicalReadPageClean.printTimes();
    bm.getBufferPool().timerEvictPageTwoLists.printTimes();


  }

  REQUIRE(1 == 1);
}

// TEST_CASE("QGenerator", "[qGenerator]") {
//  deceve::queries::POOLSIZE = 1;
//  deceve::queries::QueryManager qm;
//  deceve::storage::BufferManager bm(1, 1);
//
//  SECTION("generate queries from file") {
//    deceve::queries::QGenerator generator(&bm, &qm, 10,
//                                          deceve::queries::FROM_FILE);
//    generator.generateQueries();
//  }
//
//  SECTION("generate queries randomly") {
//    deceve::queries::QGenerator generator(&bm, &qm, 50,
//                                          deceve::queries::RANDOMLY);
//    generator.generateQueries();
//  }
//
//  SECTION("generate queries randomly skewed") {
//    deceve::queries::QGenerator generator(&bm, &qm, 50,
//                                          deceve::queries::SKEWED);
//    generator.generateQueries();
//  }
//
//  REQUIRE(1 == 1);
//}
