/*
 * loadfilemain.cc
 *
 *  Created on: Dec 18, 2014
 *      Author: maba18
 */

#include <string>
#include <iostream>
#include "stdlib.h"
#include "stdio.h"
#include <iomanip>
#include <cstdio>
#include <sys/types.h>
#include <fstream>
#include <cstring>
#include "../storage/BufferManager.hh"
#include "../queryplans/queries.hh"
#include "../utils/defs.hh"
#include "../utils/util.hh"
#include "../storage/io.hh"
#include "../utils/types.hh"
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

namespace po = boost::program_options;
using namespace std;
using namespace boost;

namespace db = deceve::bama;




class Executor{
public:
	std::queue<deceve::queries::pool_node>& getQueries(){
		return all_queries;
	}
private:
	std::queue<deceve::queries::pool_node> all_queries;
};


std::vector<deceve::queries::NodeTree> getOperators(
		deceve::storage::BufferManager *bm) {

	std::string tmpFile1 = "test";
	std::cout << "query 10" << "\n";
	std::string lineitem_file = deceve::queries::LINE_ITEM_PATH;

	deceve::queries::MyQuery_Select_LineItem_Node node1(bm, lineitem_file,
			lineitem_file);

	deceve::queries::MyQuery_Sort_Node node2(bm, "nothing", tmpFile1);

	std::vector<deceve::queries::NodeTree> operators;
	operators.push_back(node1);
	operators.push_back(node2);


	operators[1].leftNode= &operators[0];

	return operators;
}

int main(int argc, char** argv) {

	std::string queries;
	size_t task_size;
	size_t read_delay;
	size_t write_delay;
	uint64_t start, end;
	unsigned long pool_size;
	size_t clock_wait_time;

	po::options_description desc("Arguments");
	desc.add_options()("help", "produce help message")("queries",
			po::value<std::string>(&queries)->default_value("all"),
			"Which queries to run")("task_size",
			po::value<size_t>(&task_size)->default_value(10),
			"number of queries in the task queue")("read-delay",
			po::value<size_t>(&read_delay)->default_value(50),
			"persistent memory read delay (ns)")("write-delay",
			po::value<size_t>(&write_delay)->default_value(500),
			"persistent memory write delay (ns)")("pool_size",
			po::value<unsigned long>(&pool_size)->default_value(512),
			"size of the buffer pool")("clock",
			po::value<size_t>(&clock_wait_time)->default_value(1),
			"clock wait time for estimating speed");

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if (vm.count("help")) {
		std::cerr << desc << "\n";
		return 1;
	}

	std::cout << "POOLSIZE: " << deceve::queries::POOLSIZE << "\n";
	deceve::storage::BufferManager bm(
			deceve::queries::POOLSIZE * deceve::storage::PAGE_SIZE);

	std::cout << "estimating clock speed (wait " << clock_wait_time << "s)...";
	std::cout.flush();
	double tps = deceve::bama::ticks_per_second(clock_wait_time);
	std::cout << " done" << "\n" << "clock speed is " << std::fixed
			<< std::setprecision(9) << (size_t) tps << " ticks per second"
			<< "\n";

	std::cout << "read delay (" << read_delay << "ns) is ";
	std::cout.flush();
	read_delay = (size_t) (::pow(10, -9) * read_delay * tps);
	std::cout << std::fixed << std::setprecision(9) << read_delay << " ticks"
			<< "\n";
	db::set_read_delay(read_delay);

	std::cout << "write delay (" << write_delay << "ns) is ";
	std::cout.flush();
	write_delay = (size_t) (::pow(10, -9) * write_delay * tps);
	std::cout << std::fixed << std::setprecision(9) << write_delay << " ticks"
			<< "\n";
	db::set_write_delay(write_delay);

	start = db::rdtsc();

	std::string tmpFile1 = "test";
	std::cout << "query 10" << "\n";
	std::string lineitem_file = deceve::queries::LINE_ITEM_PATH;


	deceve::queries::MyQuery q10(&bm);

	std::vector<deceve::queries::NodeTree *> operators ;

		operators.push_back(
			new deceve::queries::MyQuery_Select_LineItem_Node(&bm,
					lineitem_file, lineitem_file));
	operators.push_back(
			new deceve::queries::MyQuery_Sort_Node(&bm, "nothing", tmpFile1));
	operators[1]->leftNode = operators[0];

	q10.rootNode = operators[1];


	deceve::queries::pool_node p1;
	p1.qoperator = &q10;
	p1.tnodes = operators;

	Executor executor;

	std::queue<deceve::queries::pool_node>  all_queries = executor.getQueries();

	all_queries.push(p1);

	while (!all_queries.empty()) {

		deceve::queries::executeQueryPlan(
				all_queries.front().qoperator->rootNode);

		all_queries.pop();
	}



//	deceve::queries::printQueryPlan(q10.rootNode);
//	deceve::queries::executeQueryPlan(q10.rootNode);

	std::vector<deceve::storage::AttrValue> fields;
	deceve::storage::AttrValue a("ID", "12", deceve::storage::EQ_OP);
	fields.push_back(a);

	std::vector<deceve::storage::AttrValue> fields1;
	deceve::storage::AttrValue a1("ID", "12", deceve::storage::EQ_OP);
	fields1.push_back(a1);

	deceve::storage::Details det1("sort", fields);
	deceve::storage::Details det2("sort", fields);

	deceve::storage::FileStatus f1("test.table", det1);
	deceve::storage::FileStatus f2("test.table", det2);

	if (f1 == f2) {
		std::cout << "Equal" << "\n";
	} else {
		std::cout << "Not Equal" << "\n";
	}

	bm.getSM().currentFilePlans.push_back(f1);
	bm.getSM().currentFilePlans.push_back(f2);

	for (std::vector<deceve::storage::FileStatus>::iterator it =
			bm.getSM().currentFilePlans.begin();
			it != bm.getSM().currentFilePlans.end(); ++it) {
		std::cout << *it << "\n";

	}

//q1.persistResultToDRAM();

//	std::vector<QOperator *> random_queries;
//
//	for (int i = 0; i < 10; i++) {
//		random_queries.push_back(new Query1(&bm));
//	}
//
//	for (std::vector<QOperator *>::iterator it = random_queries.begin();
//			it != random_queries.end(); ++it) {
//		(*it)->execute();
//	}

//	query1(&bm);
//	query6(&bm);
//	query14(&bm);
//	query15(&bm);
	end = db::rdtsc();

	std::cout << "Time: " << end - start << "\n";

	bm.getSM().printFiles(deceve::storage::PRIMARY);
	bm.getSM().printFiles(deceve::storage::INTERMEDIATE);
	bm.getSM().printFiles(deceve::storage::AUXILIARY);

//	std::cout << "---------------- Bufferpool Status ----------------"
//			<< "\n";
//	bm.getBufferPool().printPages(deceve::storage::PRIMARY);
//	bm.getBufferPool().printPages(deceve::storage::INTERMEDIATE);
//	bm.getBufferPool().printPages(deceve::storage::AUXILIARY);
//	bm.getBufferPool().printPages(deceve::storage::FREE);

//	bm.getBufferPool().printHitRatio();

//	query3(&bm);

	return 0;
}

