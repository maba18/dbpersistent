#!/usr/bin/python

import os
import glob
import optparse
import commands


def main():
    usage = "usage %prog [options]"
    parser = optparse.OptionParser(usage)
    parser.add_option("-n", "--number-of-times", type="int", dest="number",
                      action="store", default="5",
                      help="number of times to execute a run")
    parser.add_option("-d", "--directory", type="string", dest="directory",
                      action="store", default="data",
                      help="directory with data files")
    parser.add_option("-s", "--suffix", type="string", dest="suffix",
                      action="store", default="bin",
                      help="suffix of data files to sort")
    parser.add_option("-b", "--buffers", type="int", dest="buffers",
                      action="store", default="200",
                      help="number of 4kB buffers to use")
    parser.add_option("-v", "--verbose", dest="verbose",
                      action="store_true",
                      help="be verbose throughout execution")
    parser.add_option("-o", "--output", type="string", dest="output",
                      action="store", default="output.dat",
                      help="prefix of file to store output timings")
    parser.add_option("-r", "--ratio", type="float", dest="ratio",
                      action="store", default="1.0",
                      help="ratio of reads over writes")

    # parse the options
    options, args = parser.parse_args()
    # save the current path
    current_path = os.getcwd()
    # open the output file
    outfile = open(os.path.join(current_path, options.output), 'w')
    # switch to the designated path
    os.chdir(options.directory)
    for file in glob.glob("*." + options.suffix):
        if (options.verbose): print "processing " + file
        result = file
        for algorithm in ['hybes', 'em']:
            # construct the command to execute
            command = "run --" + algorithm + " --input=" + file
            command = command + " --output=" + file + ".out"
            command = command + " --purge --buffers=" + str(options.buffers)
            command = command + " --times=" + str(options.number)
            command = command + " --ratio=" + str(options.ratio)
            if (options.verbose):
                command = command + " --verbose"
                print command
            # run the command and parse the output
            output = commands.getoutput(command)
            if (options.verbose): print output
            split = output.split("average time: ")
            result = result + "\t" + split[1]
        outfile.write(result + "\n")

    outfile.close()
    if (options.verbose): print "going back to " + current_path
    os.chdir(current_path)
    if (options.verbose): print "done!"
    
if __name__ == "__main__":
    main()

