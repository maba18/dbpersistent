#include "../hybes.hh"
#include "../algorithms/replacementsort.hh"
#include "../utils/costs.hh"
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <iostream>
#include <algorithm>
#include <ctime>
#include <sys/time.h>

namespace bpo = boost::program_options;
namespace bfs = boost::filesystem;

int main(int ac, char** av) {
  std::string input;
  std::string output;
  unsigned int times;
  unsigned int buffers;
  double ratio;
  deceve::cost cst;

  bpo::options_description desc("Allowed options");
  desc.add_options()("help", "produce help message")("verbose",
                                                     "produce verbose output")(
      "hybes", "use HyBES (overrides other algorithms)")("em",
                                                         "use external sort")(
      "input", bpo::value<std::string>(&input)->default_value("input.bin"),
      "input file")(
      "output", bpo::value<std::string>(&output)->default_value("output.bin"),
      "output file")(
      "times", bpo::value<unsigned int>(&times)->default_value(3),
      "times to repeat run")("purge", "purge output after all runs")(
      "ratio", bpo::value<double>(&ratio)->default_value(1.0),
      "number of times read is faster than write")(
      "buffers", bpo::value<unsigned int>(&buffers)->default_value(1024),
      "number of 4kB buffers to be used for sorting");
  bpo::variables_map vm;
  bpo::store(bpo::parse_command_line(ac, av, desc), vm);
  bpo::notify(vm);

  if (vm.count("help")) {
    COUT << desc << "\n";
    return 1;
  }

  if (!bdb::fs::exists(input)) {
    std::cerr << "input file " << input << " does not exist."
              << "\n";
  }
  if (vm.count("ratio")) {
    cst.write = (unsigned int)(ratio * cst.read);
  }

  if (vm.count("verbose")) {
    COUT << "running " << times << " times"
         << "\n";
    COUT << "  input: " << input << ", output: " << output << "\n";
    COUT << "  buffers: " << buffers << "\n";
    COUT << "  read/write: " << ratio << "\n";
  }

  timeval begin, end;
  double time = 0.0;
  double runs[times];
  for (unsigned int i = 0; i < times; i++) {
    if (bdb::fs::exists(output)) {
      bdb::fs::remove_all(output);
    }
    if (vm.count("hybes")) {
      ::gettimeofday(&begin, 0);
      deceve::HybeSort<unsigned long, unsigned long> sorter(input, output,
                                                            buffers, &cst);
      sorter.sort();
      ::gettimeofday(&end, 0);
      runs[i] = end.tv_sec - begin.tv_sec +
                (end.tv_usec - begin.tv_usec) / 1000000.0f;
      if (vm.count("verbose")) {
        COUT << "  run " << i << " done in " << runs[i] << "s"
             << "\n";
      }
      // time += run;
    } else {
      ::gettimeofday(&begin, 0);
      deceve::MergeSort<unsigned long, unsigned long> sorter(input, output,
                                                             buffers);
      sorter.sort();
      ::gettimeofday(&end, 0);
      runs[i] = end.tv_sec - begin.tv_sec +
                (end.tv_usec - begin.tv_usec) / 1000000.0f;
      if (vm.count("verbose")) {
        COUT << "  run " << i << " done in " << runs[i] << "s"
             << "\n";
      }
      // time += run;
    }
  }

  if (vm.count("purge") && bdb::fs::exists(output)) {
    bdb::fs::remove_all(output);
  }

  if (vm.count("hybes")) {
    COUT << "bucket sort ";
  } else {
    COUT << "merge sort ";
  }

  std::sort(runs, runs + times);
  for (unsigned int i = 1; i < times - 1; i++) time += runs[i];
  COUT << " average time: " << (time / (times - 2)) << "\n";
}
