/*******************************************************************************
 * Copyright (c) 2015 Michail Basios and Stratis Viglas.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michail Basios - initial API and implementation
 *     Stratis Viglas - initial API and implementation
 *******************************************************************************/

// C headers
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <time.h>

// Boost headers
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

// C++ headers
#include <iomanip>
#include <iostream>
#include <string>
#include <cstdio>
#include <fstream>
#include <cstring>
#include <vector>
#include <ctime>
#include <thread>
#include <atomic>
#include <mutex>

// Include General Functions
#include "../utils/defs.hh"
#include "../utils/util.hh"
#include "../storage/io.hh"
#include "../utils/types.hh"
#include "../utils/global.hh"

// Include Storage Classes
#include "../storage/BufferManager.hh"

// Include Storage Classes
#include "../queryplans/NodeTree.hh"
#include "../queryplans/PoolNode.hh"
#include "../queryplans/QOperator.hh"
#include "../queryplans/QueryManager.hh"

#include "../records/fileloader.hh"
#include "../storage/readwriters.hh"
#include "../records/records.hh"

// Include Boost Thread
// #include <boost/thread.hpp>
// #include <boost/date_time.hpp>

#include "ThreadPool.hh"

namespace po = boost::program_options;
using namespace std;
using namespace boost;

namespace db = deceve::bama;
namespace dq = deceve::queries;

int main(int argc, char** argv) {
  std::string queries;
  size_t task_size;
  size_t read_delay;
  size_t write_delay;
  uint64_t start, end;
  size_t budget;
  size_t dump;
  size_t algorithm;
  size_t queryId;
  size_t nqueries;

  unsigned long pool_size;
  size_t clock_wait_time;
  // Persist or not results
  size_t cleverFlag;

  std::string file;
  size_t cardinality;
  float factor;

  po::options_description desc("Arguments");
  desc.add_options()("help", "produce help message")(
      "queries", po::value<std::string>(&queries)->default_value("all"),
      "Which queries to run")("task_size",
                              po::value<size_t>(&task_size)->default_value(10),
                              "number of queries in the task queue")(
      "read-delay", po::value<size_t>(&read_delay)->default_value(50),
      "persistent memory read delay (ns)")(
      "write-delay", po::value<size_t>(&write_delay)->default_value(500),
      "persistent memory write delay (ns)")(
      "pool_size", po::value<unsigned long>(&pool_size)->default_value(30000),
      "size of the buffer pool")(
      "budget", po::value<size_t>(&budget)->default_value(13000),
      "size of the persistent budget")(
      "dump", po::value<size_t>(&dump)->default_value(0),
      "print pages and files of bufferpool")(
      "algorithm", po::value<size_t>(&algorithm)->default_value(1),
      " 0 - LRU , 1 - CRU ")(
      "clock", po::value<size_t>(&clock_wait_time)->default_value(1),
      "clock wait time for estimating speed")(
      "clever", po::value<size_t>(&cleverFlag)->default_value(0),
      "if 1 the system is going to persist results otherwise it will follow "
      "normal execution")("queryid",
                          po::value<size_t>(&queryId)->default_value(21),
                          "queryId to be executed")(
      "nqueries", po::value<size_t>(&nqueries)->default_value(1),
      "nqueries to be executed")(
      "file", po::value<std::string>(&file)->default_value("all"),
      "Load all files")("cardinality",
                        po::value<size_t>(&cardinality)->default_value(30000),
                        "number of items per table")(
      "factor", po::value<float>(&factor)->default_value(1),
      "factor between smaller and bigger tables");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    std::cerr << desc << "\n";
    return 1;
  }

  std::cout << "estimating clock speed (wait " << clock_wait_time << "s)...";
  std::cout.flush();
  double tps = deceve::bama::ticks_per_second(clock_wait_time);
  std::cout << " done"
            << "\n"
            << "clock speed is " << std::fixed << std::setprecision(9)
            << (size_t)tps << " ticks per second"
            << "\n";

  std::cout << "read delay (" << read_delay << "ns) is ";
  std::cout.flush();
  read_delay = (size_t)(::pow(10, -9) * read_delay * tps);
  std::cout << std::fixed << std::setprecision(9) << read_delay << " ticks"
            << "\n";
  db::set_read_delay(read_delay);

  std::cout << "write delay (" << write_delay << "ns) is ";
  std::cout.flush();
  write_delay = (size_t)(::pow(10, -9) * write_delay * tps);
  std::cout << std::fixed << std::setprecision(9) << write_delay << " ticks"
            << "\n";
  db::set_write_delay(write_delay);

  srand(time(NULL));
  deceve::queries::POOLSIZE = pool_size;
  std::cout << "POOLSIZE IS: " << deceve::queries::POOLSIZE << "\n";
  deceve::queries::QueryManager qm;
  deceve::storage::BufferManager bm(
      deceve::queries::POOLSIZE * deceve::storage::PAGE_SIZE_PERSISTENT);

  bm.bufferReadDelay = read_delay;
  bm.bufferWriteDelay = write_delay;
  bm.getSM().setStorageReadDelay(read_delay);
  bm.getSM().setStorageWriteDelay(write_delay);
  bm.getSM().setBudgetPermanentSize(budget);
  bm.getBufferPool().setAlgorithm(algorithm);

  start = db::rdtsc();

  clock_t begin1 = clock();

  std::cout << "cardinality: " << cardinality << "\n";
  std::cout << "factor: " << factor << "\n";

  if (file == "all") {
    std::cout << "Load all database files"
              << "\n";
    FileLoader fl(cardinality, factor);
    fl.loadFile(&bm, "../files/region.tbl", "dbfiles/region.table");
    fl.loadFile(&bm, "../files/nation.tbl", "dbfiles/nation.table");
    fl.loadFile(&bm, "../files/supplier.tbl", "dbfiles/supplier.table");
    fl.loadFile(&bm, "../files/partsupp.tbl", "dbfiles/partsupp.table");
    fl.loadFile(&bm, "../files/part.tbl", "dbfiles/part.table");
    fl.loadFile(&bm, "../files/customer.tbl", "dbfiles/customer.table");
    fl.loadFile(&bm, "../files/lineitem.tbl", "dbfiles/lineitem.table");
    fl.loadFile(&bm, "../files/orders.tbl", "dbfiles/orders.table");
  } else {
    std::cout << "Load ../files/" + file + ".tbl -> dbfiles/" + file + ".table"
              << "\n";
    FileLoader fl(cardinality, factor);
    fl.loadFile(&bm, "../files/" + file + ".tbl", "dbfiles/" + file + ".table");
  }

  clock_t end1 = clock();
  double elapsed_secs = static_cast<double>(end1 - begin1) / CLOCKS_PER_SEC;

  end = db::rdtsc();

  std::cout << "Final Execution Time is: " << (end - start) / 1000000UL << "\n";

  std::cout << "elapsed_secs: " << elapsed_secs << "\n";

  if (dump == 1) {
    std::cout << "PRIMARY PAGES: "
              << "\n";
    bm.getBufferPool().printPagesByTime(deceve::storage::PRIMARY);
    std::cout << "INTERMEDIATE PAGES: "
              << "\n";
    bm.getBufferPool().printPagesByTime(deceve::storage::INTERMEDIATE);
    std::cout << "FREE PAGES: "
              << "\n";
    bm.getBufferPool().printPagesByTime(deceve::storage::FREE);
    std::cout << "FREE_INTERMEDIATE PAGES: "
              << "\n";
    bm.getBufferPool().printPagesByTime(deceve::storage::FREE_INTERMEDIATE);
  }

  bm.getSM().printFiles(deceve::storage::PRIMARY);
  bm.getSM().printFiles(deceve::storage::INTERMEDIATE);
  bm.getBufferPool().printHitRatio();
  bm.getSM().printPersistentFileCatalog();
  bm.getSM().printPesistentFileStatistics();

  return 0;
}
