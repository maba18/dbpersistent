/*******************************************************************************
 * Copyright (c) 2015 Michail Basios and Stratis Viglas.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michail Basios - initial API and implementation
 *     Stratis Viglas - initial API and implementation
 *******************************************************************************/
// C headers
#include <sys/types.h>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

// C++ headers
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdio>

// Other headers
#include "../data/wisc_extractor.hh"
#include "../algorithms/gracejoin.hh"
#include "../data/wisc.hh"

namespace db = deceve::bama;
namespace po = boost::program_options;

int main(int argc, char** argv) {
  std::string left;
  std::string right;
  std::string output;
  size_t left_key;
  size_t right_key;
  size_t runs;
  float ratio;
  size_t min_memory;
  size_t max_memory;
  std::string stats_file_name;
  size_t read_delay;
  size_t write_delay;
  size_t points;

  po::options_description desc("Arguments");
  desc.add_options()("help", "produce help message")(
      "left", po::value<std::string>(&left)->default_value("left.dat"),
      "Wisconsin benchmark left input file")(
      "right", po::value<std::string>(&right)->default_value("right.dat"),
      "Wisconsin benchmark right input file")(
      "output", po::value<std::string>(&output)->default_value("out.dat"),
      "output file")("left-key", po::value<size_t>(&left_key)->default_value(0),
                     "left join key")(
      "right-key", po::value<size_t>(&right_key)->default_value(0),
      "right join key")("runs", po::value<size_t>(&runs)->default_value(5),
                        "number of runs")(
      "ratio", po::value<float>(&ratio)->default_value(3.0),
      "write/read ratio")(
      "min-memory", po::value<size_t>(&min_memory),
      "minimum amount of core memory to be used for sorting in bytes; "
      "default: 1% of left input file size")(
      "max-memory", po::value<size_t>(&max_memory),
      "maximum amount of core memory to be used for sorting in bytes; "
      "default: 15% of left input file size")(
      "stats-file",
      po::value<std::string>(&stats_file_name)->default_value("stats.txt"),
      "output stats file (default: stats.txt)")(
      "points", po::value<size_t>(&points)->default_value(20),
      "number of measurement points (memory sizes to test)")(
      "read-delay", po::value<size_t>(&read_delay)->default_value(50),
      "persistent memory read delay (ns)")(
      "write-delay", po::value<size_t>(&write_delay)->default_value(500),
      "persistent memory write delay (ns)");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    std::cerr << desc << "\n";
    return 1;
  }

  std::cout << "estimating clock speed (wait 10s)...";
  std::cout.flush();
  double tps = deceve::bama::ticks_per_second(10);
  std::cout << " done"
            << "\n"
            << "clock speed is " << std::fixed << std::setprecision(9)
            << (size_t)tps << " ticks per second"
            << "\n";

  std::cout << "read delay (" << read_delay << "ns) is ";
  std::cout.flush();
  read_delay = (size_t)(::pow(10, -9) * read_delay * tps);
  std::cout << std::fixed << std::setprecision(9) << read_delay << " ticks"
            << "\n";
  db::set_read_delay(read_delay);

  std::cout << "write delay (" << write_delay << "ns) is ";
  std::cout.flush();
  write_delay = (size_t)(::pow(10, -9) * write_delay * tps);
  std::cout << std::fixed << std::setprecision(9) << write_delay << " ticks"
            << "\n";
  db::set_write_delay(write_delay);

  // boost::filesystem::path left_path(left);
  // boost::filesystem::path right_path(right);
  // boost::filesystem::path output_path(output);

  if (!vm.count("min-memory")) {
    min_memory = 0.01 * boost::filesystem::file_size(left);
  }

  if (!vm.count("max-memory")) {
    max_memory = 0.15 * boost::filesystem::file_size(left);
  }

  if (min_memory > max_memory) {
    std::cout << "minimum memory greater than maximum memory; "
              << "adjusting maximum to 4x minimum"
              << "\n";
    max_memory = 4 * min_memory;
  }
  std::cout << "minimum memory: " << min_memory
            << ", maximum memory: " << max_memory << "\n";

  std::ofstream stats_stream(vm.count("stats-file") ? stats_file_name.c_str()
                                                    : "stats.txt");

  stats_stream << "# memory reads writes cost min-ticks min-time "
               << "max-ticks max-time med-ticks med-time "
               << "avg-ticks avg-time"
               << "\n";

  db::fs::init();
  size_t increment = (max_memory - min_memory) / (points - 1);
  for (size_t i = 0; i < points; i++) {
    size_t memory =
        (i == points - 1 ? max_memory : min_memory + i * increment) /
        db::get_pagesize();

    unsigned int pool_size = memory;

    deceve::storage::BufferManager bm(pool_size * deceve::bama::get_pagesize());

    size_t partitions =
        (size_t)(1.2 * (bm.getFileSize(left) / (memory * db::get_pagesize())));

    std::cout << "partitions: " << partitions << "\n";
    std::cout << "iteration " << (i + 1)
              << ", memory: " << memory * db::get_pagesize()
              << ", poolsize: " << memory;
    std::cout.flush();
    uint64_t* run_times = new uint64_t[runs];
    uint64_t start, end;
    size_t reads = 0, writes = 0;
    column_extractor left_extractor(left_key);
    column_extractor right_extractor(right_key);
    simple_combinator combinator;
    std::cout << ", run: ";
    std::cout.flush();

    for (size_t i = 0; i < runs; i++) {
      std::cout << (i + 1) << " ";
      std::cout.flush();
      // check why we are copying
      /*
      boost::filesystem::path
          left_tmp_path(std::string(left_path.string()).append(".tmp"));
      boost::filesystem::copy_file(left_path, left_tmp_path);
      boost::filesystem::path
          right_tmp_path(std::string(right_path.string()).append(".tmp"));
      boost::filesystem::copy_file(right_path, right_tmp_path);
      */
      db::GraceJoin<db::wisc_t, db::wisc_t, column_extractor, column_extractor,
                    simple_combinator> joiner(&bm, left, right, output,
                                              left_extractor, right_extractor,
                                              combinator, partitions);
      db::reset_reads();
      db::reset_writes();
      start = db::rdtsc();
      joiner.join();
      end = db::rdtsc();
      if (i == 0) {
        reads = db::reads;
        writes = db::writes;
      }
      run_times[i] = (end - start);
      bm.removeFile(output);
      // db::fs::remove(output);
      // db::fs::remove(left);
      // db::fs::remove(right);
    }

    std::cout << "\n";
    std::sort(run_times, run_times + runs);
    uint64_t sum = 0;
    for (size_t i = 0; i < runs; i++) sum += run_times[i];
    stats_stream << (memory * db::get_pagesize()) << " " << reads << " "
                 << writes << " " << (size_t)(reads + ratio * writes) << " "
                 << run_times[0] << " " << std::fixed << std::setprecision(3)
                 << static_cast<double>(run_times[0] / tps) << " "
                 << run_times[runs - 1] << " " << std::fixed
                 << std::setprecision(3)
                 << static_cast<double>(run_times[runs - 1] / tps) << " "
                 << run_times[runs / 2] << " " << std::fixed
                 << std::setprecision(3)
                 << static_cast<double>(run_times[runs / 2] / tps) << " "
                 << (size_t)(static_cast<double>(sum) / runs) << " "
                 << std::fixed << std::setprecision(3)
                 << (static_cast<double>(sum) / (runs * tps)) << " "
                 << "\n";
  }
  db::fs::shutdown();
}
