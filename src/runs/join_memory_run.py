#!/usr/bin/python

import os
import glob
import optparse
import subprocess
import sys

def execute(command):
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    # Poll process for new output until finished
    while True:
        nextline = process.stdout.readline()
        if nextline == '' and process.poll() != None:
            break
        sys.stdout.write(nextline)
        sys.stdout.flush()

    output = process.communicate()[0]
    exitCode = process.returncode

    if (exitCode == 0):
        return output
    else:
        return exitCode
        #raise ProcessException(command, exitCode, output)

def main():
    usage = "usage %prog [options]"
    parser = optparse.OptionParser(usage)

    parser.add_option("-s", "--start", type="long", dest="start",
                      action="store", default="1000000",
                      help="starting left input cardinality")
    parser.add_option("-e", "--end", type="long", dest="end",
                      action="store", default="10000000",
                      help="ending left input cardinality")
    parser.add_option("-t", "--increment", type="long", dest="increment",
                      action="store", default="1000000",
                      help="left input increment")
    parser.add_option("-c", "--right-ratio", type="int", dest="right_ratio",
                      action="store", default="1",
                      help="right input cardinality ratio over left input "
                      + "cardinality")
    parser.add_option("-i", "--right-input-points", type="int",
                      dest="right_input_points", action="store", default="10",
                      help="number of right input increments per "
                      + "left input size")
    parser.add_option("-a", "--left-key", type="int", dest="left_key",
                      action="store", default="0",
                      help="index of left input join attribute")
    parser.add_option("-b", "--right-key", type="int", dest="right_key",
                      action="store", default="0",
                      help="index of right input join attribute")
    parser.add_option("-l", "--delay", type="int", dest="delay",
                      action="store", default="150",
                      help="persistent memory delay in nanoseconds")
    parser.add_option("-M", "--memory-points", type="int", dest="points",
                      action="store", default="20",
                      help="number of memory points per join run")
    parser.add_option("-x", "--exec-dir", type="string", dest="execdir",
                      action="store", default=".",
                      help="directory containing executables")
    parser.add_option("-d", "--data-dir", type="string", dest="datadir",
                      action="store", default=".",
                      help="directory where data is to be placed")
    parser.add_option("-o", "--output-dir", type="string", dest="outputdir",
                      action="store", default=".",
                      help="directory where output statistics will be placed")
    parser.add_option("-R", "--write-read-ratio", type="float",
                      dest="write_read_ratio", action="store", #default="2",
                      help="write to read ratio for computation")
    parser.add_option("-n", "--times", type="int", dest="number_of_runs",
                      action="store", default="5",
                      help="number of times to execute each run")
    parser.add_option("-r", "--read-delay", type="int", dest="read_delay",
                      action="store", default="50",
                      help="read delay in nanoseconds")
    parser.add_option("-w", "--write-delay", type="int", dest="write_delay",
                      action="store", default="500",
                      help="write delay in nanoseconds")

    # parse the options
    options, args = parser.parse_args()

    cwd = os.getcwd()
    os.chdir(options.datadir)

    if (options.write_read_ratio == None):
        options.write_read_ratio = 0.8 * options.write_delay

    command = "rm ./left.dat ./right.dat"
    print "running %s" % command
    execute(command)
    for left_cardinality in range(options.start,
                                  options.end+options.increment,
                                  options.increment):
        command = os.path.join(options.execdir, "generator")
        command += " --file ./left.dat"
        command += " --cardinality " + str(left_cardinality)
        print "running %s" % command
        execute(command)
        for right_point in range(1, options.right_input_points+1, 1):
            command = os.path.join(options.execdir, "generator")
            command += " --file ./right.dat"
            right_cardinality = left_cardinality * options.right_ratio * right_point
            command += " --cardinality " + str(right_cardinality)
            print "running %s" % command
            execute(command)
            for algorithm in ['grace', 'hash', 'hybrid',
                              'nestedloops', 'lazy', 'segment']:
                command = os.path.join(options.execdir, algorithm + "_join")
                command += " --left ./left.dat --right ./right.dat"
                command += " --left-key " + str(options.left_key)
                command += " --right-key " + str(options.right_key)
                command += " --stats-file "
                command += os.path.join(options.outputdir,
                                        "stats-" + algorithm
                                        + "-" + str(left_cardinality)
                                        + "-" + str(right_cardinality)
                                        + ".txt")
                command += " --read-delay " + str(options.read_delay)
                command += " --write-delay " + str(options.write_delay)
                command += " --ratio " + str(options.write_read_ratio)
                command += " --points " + str(options.points)
                command += " --runs " + str(options.number_of_runs)
                print "running %s" % command
                execute(command)
            command = "rm ./right.dat"
            print "running %s" % command
            execute(command)

        command = "rm ./left.dat"
        print "running %s" % command
        execute(command)

    os.chdir(cwd)
            
if __name__ == "__main__":
    main()
