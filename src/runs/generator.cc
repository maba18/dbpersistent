/*******************************************************************************
 * Copyright (c) 2015 Michail Basios and Stratis Viglas.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Michail Basios - initial API and implementation
 *     Stratis Viglas - initial API and implementation
 *******************************************************************************/
#include <sys/types.h>
#include <boost/program_options.hpp>
#include <boost/thread.hpp>
#include <boost/date_time.hpp>

#include <iostream>
#include <sstream>

#include "../data/wisc.hh"
#include "../data/wisc_extractor.hh"
#include "../storage/BufferManager.hh"
#include "../storage/readwriters.hh"

namespace db = deceve::bama;
namespace po = boost::program_options;

void testSimpleQuery() {}

void createFileWithWriter(deceve::storage::BufferManager* bm) {
  deceve::bama::Writer<int> wf(bm, "lala.bin");

  for (int i = 0; i < 600; i++) {
    wf.write(i);
  }
  wf.flush();
  wf.close();
}

int main(int argc, char** argv) {
  std::string file;
  size_t cardinality;
  float skew;
  size_t skew_values;
  size_t pool_size;

  po::options_description desc("Arguments");
  desc.add_options()("help", "produce help message")(
      "file", po::value<std::string>(&file)->default_value("wisc.dat"),
      "Wisconsin benchmark output file")(
      "cardinality", po::value<size_t>(&cardinality)->default_value(50000),
      "relation cardinality")("skew", po::value<float>(&skew)->default_value(0),
                              "skew percentage")(
      "skew-values", po::value<size_t>(&skew_values)->default_value(0),
      "number of skewed values")(
      "pool-size", po::value<size_t>(&pool_size)->default_value(100),
      "the pool size")("scan", "scan file contents with backtrack and skip")(
      "scan-only", "scan file contents only")(
      "write-read", "write-close-write-close-read-close")("create",
                                                          "create new file")(
      "thread", "create a new file through a different thread");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    std::cerr << desc << "\n";
    return 1;
  }

  if (skew > 0.99 || skew < 0) {
    std::cerr << "skew must be between 0 and 0.99"
              << "\n";
    return 1;
  }

  if (skew_values > skew * cardinality) {
    std::cerr << "number of skewed values greater than skew percentage"
              << "\n";
    return 1;
  }

  deceve::storage::BufferManager bm(pool_size * deceve::storage::PAGE_SIZE);

  if (!vm.count("scan") && !vm.count("scan-only")) {
    std::cout << "--------------------------- Create ------------------------"
              << "\n";
    std::cout << "Start Writing: " << cardinality << " records: "
              << "\n";
    db::wisc_generator generator;
    generator.set_length(cardinality);
    generator.set_skew(skew);
    generator.set_number_of_skewed_keys(skew_values);
    generator.create_file(&bm, file);
    bm.getSM().printAllFiles();
    std::cout << " " << cardinality << " records written to " << file << "\n";
  }

  if (vm.count("scan")) {
    std::cout << "--------------------------- Scan ------------------------"
              << "\n";
    std::cout << "Start scan of the current file"
              << "\n";
    db::Reader<db::wisc_t> reader(&bm, file);
    size_t count = 0;

    while (reader.hasNext()) {
      if (count == 10) {
        reader.mark();
      }
      std::cout << (count++) << ": " << reader.nextRecord() << "\n";
    }

    std::cout << "Rollback"
              << "\n";
    reader.rollback();
    count = 10;
    while (reader.hasNext()) {
      std::cout << (count++) << ": " << reader.nextRecord() << "\n";
    }
    reader.close();
  }

  if (vm.count("scan-only")) {
    std::cout << "Start scan of the file"
              << "\n";
    db::Reader<db::wisc_t> reader(&bm, file);
    size_t count = 0;
    while (reader.hasNext()) {
      db::wisc_t rec = reader.nextRecord();
      std::cout << (count++) << ": " << rec << "\n";
    }
    reader.close();
    std::cout << count << " records were read from " << file << "\n";
    // std::cout << "Final state of bufferPool" << "\n";
  }

  if (vm.count("write-read")) {
    std::cout
        << "--------------------------- Write-read ------------------------"
        << "\n";
    std::cout << "Write: " << cardinality << " more records: "
              << "\n";

    db::wisc_generator generator;
    generator.set_length(cardinality);
    generator.set_skew(skew);
    generator.set_number_of_skewed_keys(skew_values);
    generator.create_file(&bm, file);
    std::cout << cardinality << " records written to " << file << "\n";

    std::cout << "Start scan of whole the file"
              << "\n";

    db::Reader<db::wisc_t> reader(&bm, file);
    size_t count = 0;

    while (reader.hasNext()) {
      db::wisc_t rec = reader.nextRecord();
      std::cout << (count++) << ": " << rec << "\n";
    }
    reader.close();

    std::cout << count << " records were read from " << file << "\n";
    std::cout << "Final state of bufferPool"
              << "\n";
  }
}
