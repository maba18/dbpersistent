#include "../storage/readwriters.hh"
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/random.hpp>

namespace bpo = boost::program_options;
namespace bfs = boost::filesystem;

typedef boost::mt19937 base_generator_type;
typedef deceve::bama::Writer<unsigned long, unsigned long, 4096> MyWriter;
typedef MyWriter::record_type MyRecord;

int main(int ac, char** av) {
  float skew;
  unsigned long records;
  unsigned long max;
  std::string outfile;

  bpo::options_description desc("Allowed options");
  desc.add_options()("help", "produce help message")(
      "skew", bpo::value<float>(&skew)->default_value(0.15), "set skew")(
      "records", bpo::value<unsigned long>(&records)->default_value(100000),
      "set number of records")(
      "max", bpo::value<unsigned long>(&max)->default_value(100000),
      "set maximum value")(
      "outfile", bpo::value<std::string>(&outfile)->default_value("out.bin"),
      "set output file");

  bpo::variables_map vm;
  bpo::store(bpo::parse_command_line(ac, av, desc), vm);
  bpo::notify(vm);

  if (vm.count("help")) {
    std::cout << desc << "\n";
    return 1;
  }

  if (bdb::fs::exists(outfile)) {
    std::cerr << outfile << " exists; will delete"
              << "\n";
    bdb::fs::remove_all(outfile);
  }

  MyWriter writer(outfile);

  base_generator_type uni_generator(1975u);
  boost::uniform_real<> key_dist(0, 1);
  boost::uniform_real<> target_dist(0, 1);
  boost::uniform_real<> payload_dist(0, 1);
  boost::variate_generator<base_generator_type&, boost::uniform_real<> > key(
      uni_generator, key_dist);
  boost::variate_generator<base_generator_type&, boost::uniform_real<> > target(
      uni_generator, target_dist);
  boost::variate_generator<base_generator_type&, boost::uniform_real<> >
      payload(uni_generator, payload_dist);
  unsigned long start = (1 - skew) * max;
  for (unsigned long i = 0; i < records; i++) {
    double tvalue = target();
    unsigned long kvalue;
    if (tvalue < skew)
      kvalue = (unsigned long)(start * key());
    else
      kvalue = (unsigned long)((max - start) * key()) + start;

    writer.write(deceve::makeRecord(kvalue, (unsigned long)(max * payload())));
  }

  return 1;
}
