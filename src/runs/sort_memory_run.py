#!/usr/bin/python

import os
import glob
import optparse
import subprocess
import sys

def execute(command):
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    # Poll process for new output until finished
    while True:
        nextline = process.stdout.readline()
        if nextline == '' and process.poll() != None:
            break
        sys.stdout.write(nextline)
        sys.stdout.flush()

    output = process.communicate()[0]
    exitCode = process.returncode

    if (exitCode == 0):
        return output
    else:
        return exitCode
        #raise ProcessException(command, exitCode, output)

def main():
    usage = "usage %prog [options]"
    parser = optparse.OptionParser(usage)

    parser.add_option("-s", "--start", type="long", dest="start",
                      action="store", default="1000000",
                      help="starting input cardinality")
    parser.add_option("-e", "--end", type="long", dest="end",
                      action="store", default="10000000",
                      help="ending input cardinality")
    parser.add_option("-i", "--increment", type="long", dest="increment",
                      action="store", default="1000000",
                      help="cardinality increment")
    parser.add_option("-l", "--delay", type="int", dest="delay",
                      action="store", default="150",
                      help="persistent memory delay in nanoseconds")
    parser.add_option("-p", "--points", type="int", dest="points",
                      action="store", default="20",
                      help="number of points per algorithm run")
    parser.add_option("-k", "--key", type="int", dest="key",
                      action="store", default="0",
                      help="index of sort attribute")
    parser.add_option("-x", "--exec-dir", type="string", dest="execdir",
                      action="store", default=".",
                      help="directory containing executables")
    parser.add_option("-d", "--data-dir", type="string", dest="datadir",
                      action="store", default=".",
                      help="directory where data is to be placed")
    parser.add_option("-o", "--output-dir", type="string", dest="outputdir",
                      action="store", default=".",
                      help="directory where output statistics will be placed")
    parser.add_option("-R", "--write-read-ratio", type="float",
                      dest="write_read_ratio", 
                      help="write to read ratio for computation")
    parser.add_option("-n", "--times", type="int", dest="number_of_runs",
                      action="store", default="5",
                      help="number of times to execute each run")
    parser.add_option("-r", "--read-delay", type="int", dest="read_delay",
                      action="store", default="50",
                      help="read delay in nanoseconds")
    parser.add_option("-w", "--write-delay", type="int", dest="write_delay",
                      action="store", default="500",
                      help="write delay in nanoseconds")

    # parse the options
    options, args = parser.parse_args()

    cwd = os.getcwd()
    os.chdir(options.datadir)

    if (options.write_read_ratio == None):
        options.write_read_ratio = 0.8*options.write_delay

    #if os.path.isfile("./input.dat"):
    command = "rm ./input.dat"
    print "running %s" % command
    output = execute(command)    
    for cardinality in range(options.start,
                             options.end+options.increment,
                             options.increment):
        command = os.path.join(options.execdir, "generator")
        command += " --file ./input.dat"
        command += " --create --cardinality " + str(cardinality)
        print "running %s" % command
        output = execute(command)
        for algorithm in ['merge', 'segment', 'lazy', 'hybrid']:
            command = os.path.join(options.execdir, algorithm + "_sort")
            command += " --input ./input.dat"
            command += " --stats-file "
            command += os.path.join(options.outputdir,
                                    "stats-" + algorithm + "-"
                                    + str(cardinality) + ".txt")
            command += " --read-delay " + str(options.read_delay)
            command += " --write-delay " + str(options.write_delay)
            command += " --points " + str(options.points)
            command += " --runs " + str(options.number_of_runs)
            command += " --ratio " + str(options.write_read_ratio)
            print "running %s" % command
            output = execute(command)
            
        command = "rm ./input.dat"
        print "running %s" % command
        output = execute(command)
        
    os.chdir(cwd)
            
if __name__ == "__main__":
    main()
