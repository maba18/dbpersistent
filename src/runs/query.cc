/*
 * query.cc
 *
 *  Created on: 4 Dec 2014
 *      Author: michail
 */

#include <iostream>
#include <sstream>
#include <sys/types.h>
#include <boost/program_options.hpp>
#include "../data/wisc.hh"
#include "../data/wisc_extractor.hh"
#include "../storage/BufferManager.hh"
#include "../storage/readwriters.hh"
#include "../storage/identity.hh"
#include "../operators/select.hh"
#include "../algorithms/replacementsort.hh"
#include "../algebra/operators.hh"
#include <boost/thread.hpp>
#include <boost/date_time.hpp>
#include "../parse/parser.hh"
#include <boost/algorithm/string/predicate.hpp>
#include <boost/lexical_cast.hpp>
#include "../utils/types.hh"
#include <boost/variant.hpp>

#include <cstring>

namespace db = deceve::bama;
namespace ds = deceve::storage;
namespace da = deceve::algebra;
namespace po = boost::program_options;

typedef da::variable<column_extractor> var_def;

//typedef da::var_val_Qualification<var_def,
//      std::equal_to<column_extractor::key_type> > var_val_def;

typedef da::var_var_Qualification<var_def> var_var_def;

size_t size_of_var(const std::string &type) {
	if (type == "int") {
		return 4;
	}

	if (boost::starts_with(type, "c")) {
		char st[type.length()];
		::strcpy(st, type.c_str());
		return boost::lexical_cast<int>(st + 1);
	}

	else {
		return 4;
	}
}

struct rec_type {
	char data[20];
};

class CreateStatement {

public:
	CreateStatement(deceve::storage::BufferManager *buffer,
			const std::string fname,
			const std::vector<deceve::storage::AttributeInfo>& attrs) :
			bm(buffer), relName(fname), attributes(attrs) {
	}

	void execute() {

//		rec_type record;
//		int offset = 0;
		size_t record_size = 0;
		for (std::vector<deceve::storage::AttributeInfo>::iterator it =
				attributes.begin(); it != attributes.end(); ++it) {
			record_size += it->attr_length;
		}

		std::cout << "record_size is: " << record_size << "\n";

		deceve::bama::Writer<rec_type> createFile(bm, relName);

		bm->getSM().printAllFiles();
		createFile.close();
	}

private:
	deceve::storage::BufferManager *bm;
	std::string relName;
	std::vector<deceve::storage::AttributeInfo> attributes;
};

class InsertStatement {

public:
	InsertStatement(deceve::storage::BufferManager *buffer,
			const std::string fname) :
			bm(buffer), relName(fname) {
	}

	void insert(const rec_type & rec) {
		deceve::bama::Writer<rec_type> writer(bm, relName,
				deceve::storage::PRIMARY);
		writer.write(rec);
		writer.close();
	}

private:
	deceve::storage::BufferManager *bm;
	std::string relName;

};

void extractAttributeAndValues(
		std::vector<deceve::storage::AttrValue> &attributesAndNames,
		const std::string query) {

	std::cout << "Extract AttributesAndValues from query: " << query
			<< "\n";
	// parse node and extract proper values
	deceve::storage::AttrValue a1("int", "a");
	attributesAndNames.push_back(a1);
	deceve::storage::AttrValue a2("c6", "b");
	attributesAndNames.push_back(a2);
}

void insertIntoAttrCatalog(deceve::storage::BufferManager *bm,
		const std::string & relationName,
		const std::vector<deceve::storage::AttrValue> & attributesAndNames,
		std::vector<ds::AttributeInfo> & attributes) {

	int counter = 0;
	int offset = 0;
	for (std::vector<deceve::storage::AttrValue>::const_iterator it =
			attributesAndNames.begin(); it != attributesAndNames.end(); ++it) {

		ds::AttributeInfo ainfo;
		ainfo.attr_length = size_of_var(it->attrName);
		ainfo.attr_name = it->value;
		ainfo.attr_type = it->attrName;
		ainfo.index = counter++;
		ainfo.offset = offset;
		offset += size_of_var(it->attrName);
		attributes.push_back(ainfo);
	}

	bm->getSM().insertIntoAttributeCatalog(relationName,
			attributes);

}

size_t getRecordSize(
		const std::vector<deceve::storage::AttributeInfo> &attributes) {
	return attributes.back().offset + attributes.back().attr_length;
}

size_t getRecordSize(deceve::storage::BufferManager *bm,
		const std::string &relName) {
	std::vector<deceve::storage::AttributeInfo> attributes =
			bm->getSM().getAttributes(relName);
	return attributes.back().offset + attributes.back().attr_length;
}

size_t getAttributeOffset(deceve::storage::BufferManager *bm,
		const std::string &relName, const std::string &attrName) {
	std::vector<deceve::storage::AttributeInfo> attributes =
			bm->getSM().getAttributes(relName);

	for (std::vector<deceve::storage::AttributeInfo>::iterator it =
			attributes.begin(); it != attributes.end(); ++it) {
		if (it->attr_name == attrName) {
			return it->offset;
		}
	}
	return 0;
}

std::vector<std::string> extractAttrAndValues(std::string query) {

	std::cout << "ExtractAttrAndValues from query: " << query << "\n";

	//assume columns are given in the correct order
	//need to change this probably later

//std::vector<deceve::storage::AttrAndValue> attrAndValues;

//	std::vector<std::string> attributesNames;
//	attributesNames.push_back("a");
//	attributesNames.push_back("b");

	std::vector<std::string> values;
	values.push_back("12");
	values.push_back("12345");
	return values;

}

std::vector<std::string> extractRelationNames(std::string query) {
	std::cout << "Extract RelationName: " << query << "\n";

	std::vector<std::string> relationNames;
	relationNames.push_back("t");
	return relationNames;
}

void insertExtractedDataToRecord(deceve::storage::BufferManager *bm,
		std::string fname, std::vector<std::string>& values,
		rec_type & record) {
	int counter = 0;
//	int offset = 0;

	//Get information about the attributes of the relation
	std::vector<deceve::storage::AttributeInfo> tmpAttributes =
			bm->getSM().getAttributes(fname);
//		size_t rec_size = getRecordSize(bm, fname);

	for (std::vector<std::string>::iterator it = values.begin();
			it != values.end(); ++it) {

		std::string sss = *it;

		if (tmpAttributes[counter].attr_type == "int") {
			int tempValue;
			tempValue = atoi(sss.c_str());
			memcpy(record.data + tmpAttributes[counter].offset, &tempValue,
					tmpAttributes[counter].attr_length);

		} else {
			memcpy(record.data + tmpAttributes[counter].offset, sss.c_str(),
					tmpAttributes[counter].attr_length);
		}
		counter++;
	}
}


	////--------------------- Select Query ---------------------

void extractConditions(const std::string& query, std::vector<deceve::storage::Condition>& conditions ){



	deceve::storage::Condition c1;

	deceve::storage::RelAttr ra1;
	ra1.attr_name="a";
	ra1.rel_name="t";
	c1.lhsAttr=ra1;
	c1.bRhsIsAttr=0;
	deceve::storage::Value valright;




}

	ds::Value v1;








int main() {

	//A set of queries that the system should be able to execute
	// Q1:	CREATE TABLE tablename ( a int, b c8 );
	// Q2:  INSERT INTO tablename (a, b) VALUES (1, 'testttt');
	// Q3: SELECT * from tablename;
	// Q4: SELECT tablename.a from tablename;

	deceve::storage::BufferManager bm(100 * deceve::storage::PAGE_SIZE);

//------------------- Create Query --------------------

	std::string query1 = "create table t (int a, c6 b)";

	//if(query_type="create")

	std::string relationName = "t";

	//The parser exports all attributes names and types and saves them to the following vector
	std::vector<deceve::storage::AttrValue> attributesAndNames;

	//This function should be executed from the parser.
	//Remember that attributesAndNames is passed by reference and contain all extracted AttrValues
	extractAttributeAndValues(attributesAndNames, query1);

	//Save all information about attributes to attributeCatalog
	std::vector<ds::AttributeInfo> attributes;
	insertIntoAttrCatalog(&bm, relationName, attributesAndNames, attributes);

	CreateStatement cs(&bm, relationName, attributes);
	cs.execute();

	std::cout << "Record size: " << getRecordSize(attributes) << "\n";
	std::cout << "Record size: " << getRecordSize(&bm, relationName)
			<< "\n";

	bm.getSM().printAttrCatalog();

//--------------------- Insert Query ---------------------

	std::string query2 = "insert into table t1 (a , b) values(12, '12345')";

	std::vector<std::string> values;
	//assume this is returned from the parser
	values = extractAttrAndValues(query2);
	//get relation name
	std::string fname = extractRelationNames(query2)[0];

	rec_type record;
	insertExtractedDataToRecord(&bm, fname, values, record);

	InsertStatement is(&bm, fname);
	is.insert(record);

	////--------------------- Select Query ---------------------
	std::string query3 = "select t.a from t where t.a=123 and t.b='12345'";

	std::vector<std::string> relationNames= extractRelationNames(query3);
	std::vector<deceve::storage::Condition> conditions;

	extractConditions(query3 ,conditions);
































	//      bool result= d;
	//      bool d= doStuff(3,4, std::less<int>());
	//
	//      std::cout<<"Result: "<<d;

	//  variableVariableQualification< variable<column_extractor>, std::equal_to<column_extractor::key_type> > vv(var1,var2);
	//  da::variableValueQualification<da::variable<column_extractor>,
	//          std::equal_to<column_extractor::key_type> > qualification(var1, 0);

	//  deceve::bama::Selection<db::wisc_t, column_extractor, std::equal_to<unsigned int> >
	//  selection(&bm, filename, var.extractor);
	//  selection.execute();
	//
	//  deceve::bama::Selection1<db::wisc_t, variable_def, std::equal_to<unsigned int> >
	//  selection(&bm, filename, var);
	//  selection.execute();

	//  unsigned int column_id=3;
	//  column_extractor column(column_id);
	//  var_def var(filename, column);
	//  var_val_def var_val_qualification(var, 0);
	//
	//  deceve::bama::Selection2<db::wisc_t, var_val_def > selection2(
	//          &bm, filename, var_val_qualification);
	//  selection2.execute();

	//      std::string test="1";
	//      int result;
	//
	//      std::istringstream(test) >> result;
	//
	//      std::cout<<result;
	//
	//  deceve::bama::Writer<int> createFile(&bm, "testIntFile", deceve::storage::PRIMARY);
	//
	//  for(int i=0; i<10000; i++){
	//      createFile.write(i);
	//  }
	//  createFile.close();
	//
	//  deceve::bama::Reader<int> readFile(&bm, "testIntFile" );
	//
	//  while(readFile.hasNext()){
	//
	//      std::cout<<readFile.nextRecord()<<"\n";
	//  }
	//
	//  readFile.close();
	//
	//
	//
	//  column_extractor col1(3);
	//  column_extractor col2(4);
	//
	//  var_def variable1(filename, col1);
	//  var_def variable2(filename, col2);
	//
	//  var_var_def var_var_qualification(variable1, variable2 );
	//
	//  deceve::bama::Selection<db::wisc_t, var_var_def> select(&bm, filename, var_var_qualification);

	//  select.execute();

	//
	//  std::string output = selection2();

	//  std::cout << "output" << "\n";
	//
	//  db::ReplacementSort<db::wisc_t, column_extractor, std::less<unsigned int> > sorter(
	//          &bm, output, "final", extractor1, 100);
	//
	//  sorter.sort();

	//  while(reader.hasNext()){
	//
	//      db::wisc_t rec=reader.nextRecord();
	//      if(rec.serial==29356){
	//          std::cout<<rec<<"\n";
	//      }
	//
	//  }
	//
	//      reader.close();

	return 0;
}

