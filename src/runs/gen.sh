#!/bin/sh
for i in 0.1 0.2 0.3 0.4 0.5; 
do 
	out=sk"$i".bin; 
	echo $out; 
	gendist --skew="$i" --records=$1 --max=$2 --outfile="$out"; 
done
