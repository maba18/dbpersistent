#!/bin/sh

numOfPagesForRelation=$1

if [ $# -eq 0 ]; then
    numOfPagesForRelation=300000
fi

lineitem=$numOfPagesForRelation

orders=$(awk "BEGIN {print 0.249 * $lineitem; exit}")
customer=$(awk "BEGIN {print 0.037 * $lineitem; exit}")
part=$(awk "BEGIN {print 0.039 * $lineitem; exit}")
partsupp=$(awk "BEGIN {print 0.199 * $lineitem; exit}")
supplier=$(awk "BEGIN {print 0.0024 * $lineitem; exit}")
nation=5
region=1

echo "sizes in pages:"
echo "lineitem $lineitem"
echo "orders $orders"
echo "customer $customer"
echo "part $part"
echo "partsupp $partsupp"
echo "supplier $supplier"
echo "nation $nation"
echo "region $region"


./fileloadmain --file=lineitem --cardinality=$lineitem
./fileloadmain --file=orders --cardinality=$orders
./fileloadmain --file=customer --cardinality=$customer
./fileloadmain --file=part --cardinality=$part
./fileloadmain --file=partsupp --cardinality=$partsupp
./fileloadmain --file=supplier --cardinality=$supplier
./fileloadmain --file=nation --cardinality=$nation
./fileloadmain --file=region --cardinality=$region
