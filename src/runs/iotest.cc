#include "../utils/require.hh"
#include "../storage/file.hh"
#include <boost/program_options.hpp>
#include <fcntl.h>  // O_RDWR, O_CREAT
#include <unistd.h>
#include <cmath>    // ceil
#include <cstdlib>  // posix_memalign
#include <cstring>  // memset
#include <iostream>
#include <iomanip>

namespace bpo = boost::program_options;

const size_t _1GB = 1024 * 1024 * 1024;
const size_t _100MB = 100 * 1024 * 1024;

int open_file(const std::string&);
void close_file(int);
double ticks_per_second();
double ticks_to_seconds(double, double);
void create_file(const std::string&, size_t);
double read_seq(const std::string&, size_t, unsigned int);
double write_seq(const std::string&, size_t, unsigned int);
double read_rnd(const std::string&, size_t, unsigned int);
double write_rnd(const std::string&, size_t, unsigned int);

static __inline__ uint64_t rdtsc();

namespace deceve {

static inline unsigned char* aligned_new(size_t size) {
  unsigned char* var;
#ifdef MEMALIGN
  if (::posix_memalign((void**)&var, 512, size)) return 0;
#else
  if (!(var = (unsigned char*)malloc(size))) return 0;
#endif
  ::memset(var, 0, size);
  // std::cout << "allocated " << (void*) var << "\n";
  return var;
}

static inline void aligned_delete(unsigned char* p) {
  // std::cout << "deleting " << (void*) p << "\n";
  free(p);
  // delete [] p;
}

}

int main(int ac, char* av[]) {
  std::string type;
  std::string pattern;
  size_t size;
  std::string dir;
  unsigned int reps;
  double tps;

  bpo::options_description desc("Allowed options");
  desc.add_options()("help", "produce help message")(
      "type", bpo::value<std::string>(&type)->default_value("rw"),
      "type of I/O to test (r|w|rw)")(
      "pattern", bpo::value<std::string>(&pattern)->default_value("sr"),
      "set I/O pattern to test (s|r|sr)")(
      "size", bpo::value<size_t>(&size)->default_value(_1GB),
      "size of file to test")(
      "directory", bpo::value<std::string>(&dir)->default_value("/dev"),
      "directory to perform the test in")(
      "reps", bpo::value<unsigned int>(&reps)->default_value(100),
      "number of repetitions of the requested operation");
  // parse the command line
  bpo::variables_map vm;
  bpo::store(bpo::parse_command_line(ac, av, desc), vm);
  bpo::notify(vm);
  // check for help
  if (vm.count("help")) {
    std::cout << desc << "\n";
    return 1;
  }
  // sanity check the file size
  deceve::require(size >= _1GB, "File size must be at least 1GB.");
  // sanity check the options
  deceve::require(type == "r" || type == "w" || type == "rw",
                  "Type must be 'r', 'w', or 'rw'.");
  deceve::require(pattern == "s" || pattern == "r" || pattern == "sr",
                  "Pattern must be 's', 'r', or 'sr'.");
  std::stringstream s;
  s << dir << "/tmp." << ::getpid() << std::ends;
  dir = s.str();
  if (size % _100MB) {
    size = size - size % _100MB;
    std::cout << "Adjusting file size to " << size << "\n";
  }
  std::cout << "Getting clock ticks per second (wait 10s): ";
  std::cout.flush();
  tps = ticks_per_second();
  std::cout << "resolution is " << std::fixed << std::setprecision(9) << tps
            << " ticks per second."
            << "\n";
  ::srand(::time(NULL));
  // create the file and populate it
  std::cout << "Creating file " << dir << "...";
  std::cout.flush();
  create_file(dir, size);

  std::cout << " done"
            << "\n";

  std::cout << "Performing " << reps << " iterations for " << type << "/"
            << pattern << " (this might take a while)"
            << "\n";
  double average;
  if (type == "r" || type == "rw") {
    if (pattern == "s" || pattern == "sr") {
      average = read_seq(dir, size, reps);
      std::cout << "read/sequential" << std::fixed << std::setprecision(12)
                << " average is " << average << " ticks, "
                << ticks_to_seconds(average, tps) << "s per 4kB."
                << "\n";
    }
    if (pattern == "r" || pattern == "sr") {
      average = read_rnd(dir, size, reps);
      std::cout << "read/random" << std::fixed << std::setprecision(12)
                << " average is " << average << " ticks, "
                << ticks_to_seconds(average, tps) << "s per 4kB."
                << "\n";
    }
  }
  if (type == "w" || type == "rw") {
    if (pattern == "s" || pattern == "sr") {
      average = write_seq(dir, size, reps);
      std::cout << "write/sequential" << std::fixed << std::setprecision(12)
                << " average is " << average << " ticks, "
                << ticks_to_seconds(average, tps) << "s per 4kB."
                << "\n";
    }
    if (pattern == "r" || pattern == "sr") {
      average = write_rnd(dir, size, reps);
      std::cout << "write/random" << std::fixed << std::setprecision(12)
                << " average is " << average << " ticks, "
                << ticks_to_seconds(average, tps) << "s per 4kB."
                << "\n";
    }
  }
  deceve::require(::remove(dir.c_str()) == 0, "Could not delete file.");
}

int open_file(const std::string& filename) {
  int descriptor;
#ifdef LINUX
#ifdef IODIRECT
  deceve::require((descriptor = ::open(filename.c_str(),
                                       O_DIRECT | O_RDWR | O_CREAT, 0644)),
                  "could not open file.");
#endif
#ifndef IODIRECT
  descriptor = ::open(filename.c_str(), O_RDWR | O_CREAT, 0644);
  deceve::require(descriptor != -1, "could not open file.");
#endif
#endif
#ifdef DARWIN
  deceve::require(
      (descriptor = ::open(filename.c_str(), O_RDWR | O_CREAT, 0644)),
      "could not open file.");
#ifdef IODIRECT
  if (descriptor != -1)
    deceve::require((fcntl(descriptor, F_NOCACHE, 1) >= 0),
                    "could not set direct flags");
#endif
#endif
  return descriptor;
}

static __inline__ uint64_t rdtsc() {
  uint32_t lo, hi;
  /* We cannot use "=A", since this would use %rax on x86_64 */
  __asm__ __volatile__("rdtsc" : "=a"(lo), "=d"(hi));
  return (uint64_t)hi << 32 | lo;
}

double ticks_to_seconds(double t, double tps) { return t / tps; }

double ticks_per_second() {
  uint64_t ticks = 0;
  uint64_t start = 0, end = 0;
  for (int i = 0; i < 10; i++) {
    start = rdtsc();
    sleep(1);
    ticks += (rdtsc() - start);
  }
  return ((double)ticks) / 10;
}

void close_file(int descriptor) {
  deceve::require(::close(descriptor) != -1, "Could not close file.");
}

void create_file(const std::string& file, size_t size) {
  int descriptor;
  deceve::require((descriptor = ::open(file.c_str(), O_RDWR | O_CREAT, 0644)),
                  "could not open file.");
  unsigned long pos = 0;
  unsigned char* buf = deceve::aligned_new(_100MB);

  int pageSize = ::getpagesize();
   std::cout << "page size is " << pageSize << "\n";

  ::memset(buf, 0, _100MB);
  while (pos < size) {
    ::lseek(descriptor, pos, SEEK_SET);
    ::write(descriptor, buf, _100MB);
    pos += _100MB;
  }
  deceve::aligned_delete(buf);
  deceve::require(::close(descriptor) != -1, "could not close file.");
}

double read_seq(const std::string& file, size_t size, unsigned int reps) {
  // open file
  int fd = open_file(file);
  // std::cout << "file opened" << "\n";
  int bs;
  int ps = ::getpagesize();
   std::cout << "page size is " << ps << "\n";
  long pos;
  unsigned char* buf;
  unsigned long blocks = 0;
  uint64_t ticks = 0;
  uint64_t start = 0, end = 0;
  for (unsigned int i = 0; i < reps; i++) {
    // pick a number between 10 times the page size and the maximum
    // block size of 100MB
    bs = ::rand() % ((_100MB / ps) - 10) + 10;
    std::cout << "block size is " << bs << "\n";
    buf = deceve::aligned_new(bs * ps);
    pos = (long)(((double)::rand() / RAND_MAX) * (size - _100MB));
    pos = pos - (pos % ps);
    // std::cout << "position is " << pos << "\n";
    start = rdtsc();
    ::lseek(fd, pos, SEEK_SET);
    ::read(fd, buf, bs * ps);
    end = rdtsc();
    ticks += end - start;
    blocks += bs;
    deceve::aligned_delete(buf);
  }

  ::sleep(5);
  // cleanup
  close_file(fd);
  return ticks / blocks;
}

double write_seq(const std::string& file, size_t size, unsigned int reps) {
  // open file
  int fd = open_file(file);
  int bs;
  int ps = ::getpagesize();
  long pos;
  unsigned char* buf;
  uint64_t ticks = 0;
  uint64_t start = 0, end = 0;
  unsigned long blocks = 0;
  for (unsigned int i = 0; i < reps; i++) {
    // pick a number between 10 times the page size and the maximum
    // block size of 100MB
    bs = ::rand() % ((_100MB / ps) - 10) + 10;
    buf = deceve::aligned_new(bs * ps);
    ::memset(buf, '#', bs * ps);
    pos = (long)(((double)::rand() / RAND_MAX) * (size - _100MB));
    pos = pos - (pos % ps);
    start = rdtsc();
    ::lseek(fd, pos, SEEK_SET);
    ::write(fd, buf, bs * ps);
    end = rdtsc();
    ticks += (end - start);
    blocks += bs;
    deceve::aligned_delete(buf);
  }
  // cleanup
  close_file(fd);
  return ticks / blocks;
}

double read_rnd(const std::string& file, size_t size, unsigned int reps) {
  // open file
  int fd = open_file(file);
  int ps = ::getpagesize();
  long pos;
  unsigned char* buf = deceve::aligned_new(ps);
  ;
  unsigned long blocks = 0;
  uint64_t ticks = 0;
  uint64_t start = 0, end = 0;
  for (unsigned int i = 0; i < reps; i++) {
    pos = (long)(((double)::rand() / RAND_MAX) * (size - ps));
    pos = pos - (pos % ps);
    start = rdtsc();
    ::lseek(fd, pos, SEEK_SET);
    ::read(fd, buf, ps);
    end = rdtsc();
    ticks += (end - start);
  }
  // cleanup
  close_file(fd);
  return ticks / reps;
}

double write_rnd(const std::string& file, size_t size, unsigned int reps) {
  // open file
  int fd = open_file(file);
  int ps = ::getpagesize();
  long pos;
  unsigned char* buf = deceve::aligned_new(ps);
  ;
  uint64_t ticks = 0;
  uint64_t start = 0, end = 0;
  unsigned long blocks = 0;
  for (unsigned int i = 0; i < reps; i++) {
    pos = (long)(((double)::rand() / RAND_MAX) * (size - ps));
    pos = pos - (pos % ps);
    start = rdtsc();
    ::lseek(fd, pos, SEEK_SET);
    ::write(fd, buf, ps);
    end = rdtsc();
    ticks += (end - start);
  }
  // cleanup
  close_file(fd);
  return ticks / reps;
}
