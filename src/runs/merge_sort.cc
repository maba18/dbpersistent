#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdio>
#include <sys/types.h>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include "../data/wisc_extractor.hh"
#include "../storage/io.hh"
#include "../algorithms/replacementsort.hh"
#include "../data/wisc.hh"
#include "../utils/defs.hh"
#include "../utils/util.hh"

namespace db = deceve::bama;
namespace po = boost::program_options;

int main(int argc, char** argv) {
	std::string input;
	std::string output;
	size_t key;
	size_t runs;
	float ratio;
	size_t min_memory;
	size_t max_memory;
	std::string stats_file_name;
	size_t read_delay;
	size_t write_delay;
	size_t points;

	po::options_description desc("Arguments");
	desc.add_options()("help", "produce help message")("input",
			po::value<std::string>(&input)->default_value("in.dat"),
			"Wisconsin benchmark input file")("output",
			po::value<std::string>(&output)->default_value("out.dat"),
			"output file")("key", po::value<size_t>(&key)->default_value(0),
			"sort key")("runs", po::value<size_t>(&runs)->default_value(5),
			"number of runs")("ratio",
			po::value<float>(&ratio)->default_value(3.0), "write/read ratio")(
			"min-memory", po::value<size_t>(&min_memory),
			"minimum amount of core memory to be used for sorting in bytes; "
					"default: 1% of input file size")("max-memory",
			po::value<size_t>(&max_memory),
			"maximum amount of core memory to be used for sorting in bytes; "
					"default: 15% of input file size")("stats-file",
			po::value<std::string>(&stats_file_name)->default_value(
					"stats.txt"), "output stats file (default: stats.txt)")(
			"points", po::value<size_t>(&points)->default_value(20),
			"number of measurement points (memory sizes to test)")("read-delay",
			po::value<size_t>(&read_delay)->default_value(50),
			"persistent memory read delay (ns)")("write-delay",
			po::value<size_t>(&write_delay)->default_value(500),
			"persistent memory write delay (ns)");

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if (vm.count("help")) {
		std::cerr << desc << "\n";
		return 1;
	}

	std::cout << "estimating clock speed (wait 10s)...";
	std::cout.flush();
	double tps = deceve::bama::ticks_per_second(10);
	std::cout << " done" << "\n" << "clock speed is " << std::fixed
			<< std::setprecision(9) << (size_t) tps << " ticks per second"
			<< "\n";

	std::cout << "read delay (" << read_delay << "ns) is ";
	std::cout.flush();
	read_delay = (size_t) (::pow(10, -9) * read_delay * tps);
	std::cout << std::fixed << std::setprecision(9) << read_delay << " ticks"
			<< "\n";
	db::set_read_delay(read_delay);

	std::cout << "write delay (" << write_delay << "ns) is ";
	std::cout.flush();
	write_delay = (size_t) (::pow(10, -9) * write_delay * tps);
	std::cout << std::fixed << std::setprecision(9) << write_delay << " ticks"
			<< "\n";
	db::set_write_delay(write_delay);

	//boost::filesystem::path input_path(input);
	//boost::filesystem::path output_path(output);

	if (!vm.count("min-memory")) {
		min_memory = 0.01 * boost::filesystem::file_size(input);
		//min_memory = 0.01 *
		//    boost::filesystem::file_size(input) / db::get_pagesize();
	}

	if (!vm.count("max-memory")) {
		max_memory = 0.15 * boost::filesystem::file_size(input);
		//max_memory = 0.15 *
		//    boost::filesystem::file_size(input) / db::get_pagesize();
	}

	if (min_memory > max_memory) {
		std::cout << "minimum memory greater than maximum memory; "
				<< "adjusting maximum to 4x minimum" << "\n";
		max_memory = 4 * min_memory;
	}
	std::cout << "minimum memory: " << min_memory << ", maximum memory: "
			<< max_memory << "\n";

	std::ofstream stats_stream(stats_file_name.c_str());

	stats_stream << "# memory reads writes cost min-ticks min-time "
			<< "max-ticks max-time med-ticks med-time " << "avg-ticks avg-time"
			<< "\n";


	db::fs::init();
	size_t increment = (max_memory - min_memory) / (points - 1);
	for (size_t j = 0; j < points; j++) {
		size_t memory = (
				j == points - 1 ? max_memory : min_memory + j * increment)
				/ db::get_pagesize();
		std::cout << "iteration " << (j + 1) << ", memory: "
				<< memory * db::get_pagesize() << ", poolsize: "<<memory;
		std::cout.flush();
		uint64_t* run_times = new uint64_t[runs];
		uint64_t start, end;
		uint64_t reads = 0, writes = 0;
		column_extractor extractor(key);
		std::cout << ", run: ";
		std::cout.flush();


		unsigned int pool_size=memory;
		deceve::storage::BufferManager bm(pool_size * deceve::bama::get_pagesize());


		for (size_t i = 0; i < runs; i++) {

			std::cout << (i + 1) << " ";
			std::cout.flush();

			db::ReplacementSort<db::wisc_t, column_extractor, std::less<unsigned int> > sorter(&bm, input,
					output, extractor, memory);
			db::reset_reads();
			db::reset_writes();
			start = db::rdtsc();
			sorter.sort();
			end = db::rdtsc();


			if (i == 0) {
				//reads = db::reads;
				reads = bm.getSM().getRead_cost_real();
				//writes = db::writes;
				writes = bm.getSM().getWrite_cost_real();
			}
			run_times[i] = (end - start);
//
			std::cout<<"Print Primary"<<"\n";
			bm.getBufferPool().printPages(deceve::storage::PRIMARY);
			std::cout<<"Print Intermediate"<<"\n";
			bm.getBufferPool().printPages(deceve::storage::INTERMEDIATE);
//			std::cout<<"Print by Dirtiness"<<"\n";
//			bm.getBufferPool().printPagesByDirtiness();
			bm.getSM().printAllFiles();
		//	bm.getBufferPool().testMPIndex();
			bm.removeFile(output);

		}

		std::cout << "\n";
		std::sort(run_times, run_times + runs);
		uint64_t sum = 0;
		for (size_t i = 0; i < runs; i++)
			sum += run_times[i];
		stats_stream << (memory * db::get_pagesize()) << " " << reads << " "
				<< writes << " " << (size_t) (reads + ratio * writes) << " "
				<< run_times[0] << " " << std::fixed << std::setprecision(3)
				<< (double) run_times[0] / tps << " " << run_times[runs - 1]
				<< " " << std::fixed << std::setprecision(3)
				<< (double) run_times[runs - 1] / tps << " "
				<< run_times[runs / 2] << " " << std::fixed
				<< std::setprecision(3) << (double) run_times[runs / 2] / tps
				<< " " << (size_t) ((double) sum / runs) << " " << std::fixed
				<< std::setprecision(3) << ((double) sum / (runs * tps)) << " "
				<< "\n";
	}
db::fs::shutdown();
}
