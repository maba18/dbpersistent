## INSTALL AND RUN

#####First time:#####
The first time in order to build the project type: `./buildMe.sh`

##### Other times ##### 
In order to build and run the workloads type: `./compileAndRun.sh` 

In order to run the workload without building it again type: `./justRun.sh`


## FLAGS (default values)

### Program execution flags
* -pool_size=100,000, size of bufferpool in pages
* -budgetPermanent=100,000, size of the data structure store in pages
* -algorithm=3, clean ranked-based replacement algorithm
* -algorithmCase=0, if algorithm=3 then this flag is used to determine the ranking of data-structure pages
* -threads=1, number of threads
* -reorder=0, if reorder=1 then queue-reordering is happening

### Compilation flags for the MAKEFILE
* -DMULTI_THREADED, flag for adding locks when multiple threads are used
* -DCLEANTEST2, flag for using hybrid-clean-first and ranked-clean-first algorithms
* -DAUXILIARY_MERGED, flag for using two different versions of ranked-clean-first algorithm. In the first version, data structure pages are given the lowest priority (write pages in NVRAM as soon as possible), whereas in the second they are given the highest priority (keep pages in DRAM as much as possible). This flag is used with -algorithmCase=3 during execution.


## How to choose what workload to run?

Option 1(default): Add manually the queries (1-19) in runs/queries.txt file. One query per line:

sample format of runs/queries.txt:  
6 
17  
6 
17
17
17

#####Flag to be used:#####
-workload=FROM_FILE


Option 2: Generate randomly queries from (1-19):
#####Flag to be used:#####

-nQueries=100 // define number of queries to be generated
-workload=RANDOMLY


## RESULTS ##

For analytic statistics about the workload execution see: `runs/analyticalDetails.txt`
For less statistics about the workload execution see: `runs/runtimeDetails.txt`
